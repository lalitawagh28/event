@extends('layouts.master')
@section('page_title', __('voyager::generic.viewing').' Sales Report Commission')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 mb-0">
                <h1 class="page-title">
                    <i class="voyager-plus"></i> Sales Report Commission
                </h1>
            </div>

        </div>
    </div>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">




                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-bordered">
                            {{-- <div class="panel"> --}}


                            <div class="panel-body">
                                <form action="{{ route('voyager.agents.get_sales_report_by_commission_data') }}" method="POST"
                                    role="form">
                                    <!-- CSRF TOKEN -->
                                    {{ csrf_field() }}
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="event">Event <span class="text-danger">*</span> </label>
                                        <select class="form-control select2" multiple id="events" name="events[]" required>
                                            <option  value="all">{{ __('admin.select_event_all') }}</option>
                                            @foreach ($events as $event)
                                                <option value="{{ $event->id }}" @if(old('event',request()->event) == $event->id ) selected @endif>{{ $event->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="from_date">From Date</label>
                                        <input type="date" class="form-control" id="from_date" name="from_date" value="{{ old('from_date',request()->from_date)}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="to_date">To Date</label>
                                        <input type="date" class="form-control" id="to_date" name="to_date" value="{{ old('to_date',request()->to_date)}}">
                                    </div>


                                    <button type="submit" class="btn btn-primary pull-right save">
                                        {{ __('voyager::generic.submit') }}
                                    </button>
                                </form>

                                @if (!is_null(@$bookings))
                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                        <th>Event Name</th>
                                        <th>Ticket Category</th>
                                        <th>Ticket Sub Category</th>
                                        <th>Voucher Code</th>
                                        <th title="Sales Amount = Original Price - Agent Commission - User discount"><span style="color:red;pointer-events: auto;cursor: pointer;">?</span> Sales</th>
                                        <th>No Of Ticket</th>
                                        <th>Agent Commission Earned</th>
                                    </thead>
                                    <tbody>
                                        @php
                                        $totalPrices = 0;
                                        $totalTickets = 0;
                                        @endphp
                                        @foreach ($selectedEvent as $ticket)
                                            @if($ticket->sub_category)
                                                @forEach($ticket->sub_categories as $subCat)

                                                    @foreach(App\Models\AgentTicket::where('agent_id',auth()->user()->id)->where('sub_category_id',$subCat->id)->get() as $agent_ticket)
                                                        @php
                                                            $totalTicket = 0;
                                                            $totalPrice = 0;
                                                            $commission = 0;
                                                            $price=0;
                                                        @endphp
                                                        @foreach ($bookings->where('agent_id',$agent_ticket->id) as $booking)
                                                            @php
                                                                $totalTicket += $booking->quantity;
                                                                $price = $booking->price - $booking?->agent_ticket?->commission - $booking?->agent_ticket?->discount;
                                                                $totalPrice += $price;
                                                                $commission += $agent_ticket->commission;
                                                                $totalPrices += $price;
                                                                $totalTickets += $booking?->quantity;
                                                            @endphp
                                                        @endforeach
                                                        @if($totalTicket > 0)
                                                            <tr>
                                                                <td>{{ $ticket->event->title }}</td>
                                                                <td>{{ $ticket->title }}</td>
                                                                <td>{{ $subCat->title }}</td>
                                                                <td>@if( !empty(trim($agent_ticket?->coupon_code) )){{ $agent_ticket?->coupon_code }} @else {{ $agent_ticket?->code }} @endif</td>
                                                                <td>£{{ number_format($price, 2) }}</td>
                                                                <td>{{ $totalTicket }}</td>
                                                                <td>{{ $commission }}</td>
                                                            </tr>
                                                        @endif
                                                    @endForEach
                                                @endForEach
                                            @else

                                                @foreach (App\Models\AgentTicket::where('agent_id',auth()->user()->id)->where('ticket_id',$ticket->id)->get() as $agent_ticket)
                                                        @php
                                                            $totalTicket = 0;
                                                            $totalPrice = 0;
                                                            $commission = 0;
                                                            $price=0;
                                                        @endphp

                                                        @foreach ($bookings->where('agent_id',$agent_ticket->id) as $booking)
                                                            @php
                                                                $totalTicket += $booking->quantity;
                                                                $price = $booking->price - $booking?->agent_ticket?->commission - $booking?->agent_ticket?->discount;
                                                                $totalPrice += $price;
                                                                $commission += $agent_ticket->commission;
                                                                $totalPrices += $price;
                                                                $totalTickets += $booking?->quantity;
                                                            @endphp
                                                        @endforeach
                                                        @if($totalTicket > 0)
                                                            <tr>
                                                                <td>{{ $ticket->event->title }}</td>
                                                                <td>{{ $ticket->title }}</td>
                                                                <td> NA </td>
                                                                <td>@if( !empty(trim($agent_ticket?->coupon_code) )){{ $agent_ticket?->coupon_code }} @else {{ $agent_ticket?->code }} @endif</td>
                                                                <td>£{{ number_format($price, 2) }}</td>
                                                                <td>{{ $totalTicket }}</td>
                                                                <td>{{ $commission }}</td>
                                                            </tr>
                                                        @endif
                                                @endforeach
                                            @endif
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4"></td>

                                            <td style="white-space: nowrap;"><strong>Total: £{{ number_format($totalPrices, 2) }}</strong></td>
                                            <td style="white-space: nowrap;"><strong>Total Tickets: {{ $totalTickets }}</strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <form method="POST" action="{{ route('events.agents.exportCommisionSale') }}">
                                    @csrf
                                    <input type="hidden" name="from_date" value="{{ request()->from_date }}">
                                    <input type="hidden" name="to_date" value="{{ request()->to_date }}">
                                    <input type="hidden" name="event_ids" value="{{  implode(',',$eventsId) }}">
                                    <button type="submit" class="btn btn-primary pull-right" >Export Excel</button>
                                </form>
                            @endif
                            </div>


                        </div>
                    </div>
                </div>




            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .page-content -->



@stop




@section('javascript')
<script>
    $(document).ready(function(){
        @if(@$eventIds)
            $('#events').val({!! json_encode($eventIds) !!}).trigger('change');
        @endif
    });
    $('#to_date').on('change',function(){
        if($('#from_date').val() == ''){
            alert("From Date is required");
            $('#to_date').val('');
        } else {
            if($('#from_date').val() >  $('#to_date').val()){
                alert("To Date is greater than From Date");
                $('#to_date').val('');
            }
        }

    });
</script>
@endsection
