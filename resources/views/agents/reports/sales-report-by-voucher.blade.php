@extends('layouts.master')
@section('page_title', __('voyager::generic.viewing') . ' Sales Report By Voucher')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 mb-0">
                <h1 class="page-title">
                    <i class="voyager-plus"></i> Sales Report By Voucher
                </h1>
            </div>

        </div>
    </div>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">




                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-bordered">
                            {{-- <div class="panel"> --}}


                            <div class="panel-body">
                                <form action="{{ route('voyager.agents.get_sales_report_by_voucher_data') }}" method="POST"
                                    role="form">
                                    <!-- CSRF TOKEN -->
                                    {{ csrf_field() }}
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="event">Event <span class="text-danger">*</span></label>
                                        <select class="form-control select2" id="event" name="event"
                                            onchange="getVoucherCode(this)" required>
                                            <option class="disabled" value="">{{ __('admin.select_event') }}</option>
                                            @foreach ($events as $event)
                                                <option value="{{ $event->id }}"
                                                    @if (old('event',request()->event) == $event->id) selected @endif>{{ $event->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="voucher">Voucher <span class="text-danger">*</span></label>
                                        <select class="form-control select2" id="voucher" name="voucher[]" multiple required>

                                            @if(!empty($vouchers))
                                            <option value="Select All" @if(in_array('Select All',request()->voucher)) selected @endif>Select ALL</option>
                                            @foreach ($vouchers as $voucher)
                                            @if($voucher->coupon_code != '')
                                            <option value="{{ $voucher->coupon_code }}" @if(in_array($voucher->coupon_code,request()->voucher)) selected @endif>

                                                    {{ $voucher->coupon_code }} (Discount {{ $voucher->discount }} GBP)

                                            </option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="from_date">From Date</label>
                                        <input type="date" class="form-control" id="from_date" name="from_date"
                                            value="{{ old('from_date',request()->from_date) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="to_date">To Date</label>
                                        <input type="date" class="form-control" id="to_date" name="to_date"
                                            value="{{ old('to_date',request()->to_date) }}">
                                    </div>


                                    <button type="submit" class="btn btn-primary pull-right save">
                                        {{ __('voyager::generic.submit') }}
                                    </button>
                                </form>

                                @if (!is_null(@$bookings))
                                    <table id="dataTable" class="table table-hover">
                                        <thead>
                                            <th>Ticket Category</th>
                                            <th>Ticket Sub Category</th>
                                            <th>Voucher Code</th>
                                            <th title="Sales Amount = Original Price - Agent Commission - User discount"><span style="color:red;pointer-events: auto;cursor: pointer;">?</span> Sales</th>
                                            <th>No Of Ticket</th>
                                        </thead>
                                        <tbody>
                                            @php
                                            $totalPrices = 0;
                                            $totalTickets = 0;
                                            @endphp
                                            @foreach ($selectedEvent as $ticket)
                                            @if($ticket->sub_category)
                                                @forEach ($ticket->sub_categories as $subCat)
                                                    @php
                                                        $totalTicket = 0;
                                                        $totalPrice = 0;
                                                        $price=0;
                                                    @endphp
                                                    @foreach ($bookings->where('sub_category_id',$subCat->id) as $booking)
                                                            @php
                                                                $totalTicket += $booking->quantity;
                                                                $price = $booking->price - $booking?->commission - $booking?->discount;
                                                                $totalPrice += $price;
                                                                $totalPrices += $price;
                                                                $totalTickets += $booking?->quantity;
                                                            @endphp
                                                    @endforeach
                                                    @if($totalTicket > 0)
                                                        <tr>
                                                            <td>{{ $ticket->title }}</td>
                                                            <td>{{ $subCat->title }}</td>
                                                            <td>{{ $booking->promocode }}</td>
                                                            <td>£{{ number_format($price, 2) }}</td>
                                                            <td>{{ $totalTicket }}</td>
                                                        </tr>
                                                    @endif
                                                @endforEach
                                            @else
                                                @php
                                                    $totalTicket = 0;
                                                    $totalPrice = 0;
                                                    $price=0;
                                                @endphp
                                                @foreach ($bookings->where('ticket_id',$ticket->id) as $booking)
                                                    @php
                                                        $totalTicket += $booking->quantity;
                                                        $price = $booking->price - $booking?->commission - $booking?->discount;
                                                        $totalPrice += $price;
                                                        $totalPrices += $price;
                                                        $totalTickets += $booking?->quantity;
                                                    @endphp
                                                @endforeach
                                                @if($totalTicket > 0)
                                                <tr>
                                                    <td>{{ $ticket->title }}</td>
                                                    <td>NA</td>
                                                    <td>{{ $booking->promocode }}</td>
                                                    <td>£{{ number_format($price, 2) }}</td>
                                                    <td>{{ $totalTicket }}</td>
                                                </tr>
                                                @endif
                                            @endif
                                            @endforeach

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2"></td>

                                                <td><strong>Total: £{{ number_format($totalPrices, 2) }}</strong></td>
                                                <td><strong>Total Tickets: {{ $totalTickets }}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <form method="POST" action="{{ route('events.agents.exportVoucherSale') }}">
                                        @csrf
                                        <input type="hidden" name="from_date" value="{{ request()->from_date }}">
                                        <input type="hidden" name="to_date" value="{{ request()->to_date }}">
                                        <input type="hidden" name="event_id" value="{{  request()->event }}">
                                        <input type="hidden" name="voucher_ids" value="{{  implode(',',$vouchersId) }}">
                                        <button type="submit" class="btn btn-primary pull-right" >Export Excel</button>
                                    </form>
                                @endif
                            </div>


                        </div>
                    </div>
                </div>




            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .page-content -->



@stop
@section('javascript')
    <script>
        $(document).ready(function(){
            @if(@$voucherIds)
                $('#voucher').val({!! json_encode($voucherIds) !!}).trigger('change');
            @endif
        });
        $(function() {
            $.ajaxSetup({
                headers: {
                    "X-CSRFToken": getCookie("csrftoken")
                }
            });
        });
        $('#to_date').on('change',function(){
            if($('#from_date').val() == ''){
                alert("From Date is required");
                $('#to_date').val('');
            } else {
                if($('#from_date').val() >  $('#to_date').val()){
                    alert("To Date is greater than From Date");
                    $('#to_date').val('');
                }
            }

        });
        function getVoucherCode(the) {
            var eventId = $(the).val();

            $.ajax({
                url: '{{ route('voyager.agents.get_voucher_code_by_event') }}',
                type: "POST",
                data: {
                    'eventId': eventId
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(result) {
                    var vouchers = result.vouchers;
                    var SelectAll = [];
                    var html = '<option  value="Select All">Select All</option>';
                    for (let x in vouchers) {
                        console.log(vouchers[x]['code']);
                        if(vouchers[x]['coupon_code'] != '')
                        {
                            var voucherCode = vouchers[x]['coupon_code'];

                            html += '<option  value="' + vouchers[x]['coupon_code'] + '">' + voucherCode +' (Discount '+ vouchers[x]['discount'] +' GBP)' +
                            '</option>';

                            SelectAll.push(vouchers[x]['coupon_code']);
                        }


                    }

                    $("#voucher").html(html);
                }
            });
        }
    </script>
@stop
