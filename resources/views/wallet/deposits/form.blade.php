

@extends('eventmie::layouts.app')

{{-- Page title --}}
@section('title')
@endsection



@section('content')
<router-view></router-view>
@endsection

@section('javascript')
<script>
    var wallets  =  {!! json_encode($wallets, JSON_HEX_TAG) !!};
</script>
<script type="text/javascript" src="{{ asset('js/wallet_v1.0.js') }}"></script>
@stop