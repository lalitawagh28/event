@extends('voyager::master')
@section('page_title', __('voyager::generic.viewing') . ' Sales Report By Agent')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 mb-0">
                <h1 class="page-title">
                    <i class="voyager-plus"></i> Sales Report Commission
                </h1>
            </div>

        </div>
    </div>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                @if (auth()->user()->hasRole('admin'))
                                    @php
                                        $route = route('voyager.get_sales_report_commission_data');
                                    @endphp
                                @else
                                    @php
                                        $route = route('voyager.organizer.get_sales_report_commission_data');
                                    @endphp
                                @endif
                                <form action="{{ $route }}" method="POST" role="form">
                                    <!-- CSRF TOKEN -->
                                    {{ csrf_field() }}
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="agent_id">Event <span class="text-danger">*</span></label>
                                        <select class="form-control select2" id="event" name="event"
                                            onchange="getAgents(this)" required>
                                            <option class="disabled" value="">{{ __('admin.select_event') }}</option>
                                            @foreach ($events as $event)
                                                <option value="{{ $event->id }}"
                                                    @if (old('event', request()->event) == $event->id) selected @endif>{{ $event->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="agent_id">Agents <span class="text-danger">*</span></label>
                                        <select class="form-control select2" id="agents" multiple name="agents[]" required>
                                            <option value="all">Select All Agent</option>
                                            @if (!empty($agents))
                                                @foreach ($agents as $agent)
                                                    <option value="{{ $agent->agent->id }}"
                                                        @if ($agent->agent->id == $agent_id) selected @endif>
                                                        {{ $agent->agent->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="event_id">From Date</label>
                                        <input type="date" class="form-control" id="from_date" name="from_date"
                                            value="{{ old('from_date', request()->from_date) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="agent_name">To Date</label>
                                        <input type="date" class="form-control" id="to_date" name="to_date"
                                            value="{{ old('to_date', request()->to_date) }}">
                                    </div>


                                    <button type="submit" class="btn btn-primary pull-right save">
                                        {{ __('voyager::generic.submit') }}
                                    </button>
                                </form>

                                <div style="margin-top:80px;overflow-y: auto;height: 600px;">
                                    @if (!is_null(@$selectedEvent))
                                        <table id="dataTable" class="table table-hover">
                                            <thead>
                                                <th>Event Title</th>
                                                <th>Ticket Category</th>
                                                <th>Ticket Sub Category</th>
                                                <th>Agent Name</th>
                                                <th>Voucher Code</th>
                                                <th
                                                    title="Sales Amount = Original Price - Agent Commission - User discount">
                                                    <span style="color:red;pointer-events: auto;cursor: pointer;">?</span>
                                                    Sales
                                                </th>
                                                <th>No Of Ticket</th>
                                                <th>Agent Commission Earned</th>
                                            </thead>
                                            <tbody>
                                                @php
                                                $totalPrices = 0;
                                                $totalTickets = 0;
                                                @endphp
                                                @foreach ($selectedEvent as $ticket)
                                                @if($ticket->sub_category)
                                                    @forEach($ticket->sub_categories as $subCat)
                                                        @forEach($agids as $aid)
                                                        @php
                                                            $aids = App\Models\AgentTicket::where('agent_id',$aid)->where('sub_category_id',$subCat->id)->pluck('id')->toArray();
                                                            $agt = App\Models\Agent::find($aid);
                                                        @endphp

                                                        @foreach (App\Models\AgentTicket::whereIn('id',$aids)->where('sub_category_id',$subCat->id)->get() as $key => $agent_ticket)
                                                            @php
                                                                $totalTicket = 0;
                                                                $totalPrice = 0;
                                                                $commission = 0;
                                                                $price = 0;
                                                                $booking = null;
                                                                $agent = null;

                                                            @endphp
                                                            @empty($agent_ticket)
                                                            @else
                                                                @if ($agent_ticket->id)
                                                                    @php
                                                                        $agent = \App\Models\AgentTicket::
                                                                            whereId($agent_ticket->id)
                                                                            ->first();
                                                                    @endphp
                                                                @else
                                                                    @php
                                                                        $agent = null;
                                                                    @endphp
                                                                @endif

                                                                @if (!is_null($from_date) && !is_null($to_date))
                                                                    @php
                                                                        $from_date = date('Y-m-d', strtotime($from_date));
                                                                        $to_date = date('Y-m-d', strtotime('+1 day', strtotime($to_date)));

                                                                        $booking = \App\Models\Booking::whereEventId($event_id)
                                                                            ->whereBetween('bookings.created_at', [$from_date, $to_date])
                                                                            ->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id')
                                                                            ->where('bookings.agent_id', $agent?->id)
                                                                            ->where('bookings.promocode', $agent?->coupon_code)
                                                                            ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', 'agent_tickets.commission', 'agent_tickets.discount', DB::raw(' COALESCE(SUM(bookings.price),0) as tot_net_price'), DB::raw(' COALESCE(SUM(bookings.quantity),0) as tot_quantity'), DB::raw('COALESCE(SUM(agent_tickets.commission),0) as tot_commission'), DB::raw('COALESCE(SUM(agent_tickets.discount),0) as tot_discount'))
                                                                            ->first();

                                                                        $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;

                                                                    @endphp
                                                                @else
                                                                    @php

                                                                        $booking = \App\Models\Booking::whereEventId($event_id)
                                                                            ->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id')
                                                                            ->where('bookings.agent_id', $agent?->id)
                                                                            ->where('bookings.promocode', $agent?->coupon_code)
                                                                            ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', 'agent_tickets.commission', 'agent_tickets.discount', DB::raw(' COALESCE(SUM(bookings.price),0) as tot_net_price'), DB::raw(' COALESCE(SUM(bookings.quantity),0) as tot_quantity'), DB::raw('COALESCE(SUM(agent_tickets.commission),0) as tot_commission'), DB::raw('COALESCE(SUM(agent_tickets.discount),0) as tot_discount'))
                                                                            ->first();
                                                                        $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                                                                        $totalPrices += $price;
                                                                        $totalTickets += $booking?->tot_quantity;
                                                                    @endphp
                                                                @endif
                                                                @if( $booking?->tot_quantity > 0)
                                                                    <tr>
                                                                        <td>{{ $ticket->event->title }}</td>
                                                                        <td>{{ $ticket->title }}</td>
                                                                        <td>{{ $subCat->title }}</td>
                                                                        <td> {{  $agt->email }} </td>
                                                                        <td>@if( !empty(trim($agent?->coupon_code) )){{ $agent?->coupon_code }} @else {{ $agent?->code }} @endif</td>
                                                                        <td>£{{ number_format($price, 2) }}</td>
                                                                        <td>{{ $booking?->tot_quantity }}</td>
                                                                        <td>{{ $booking?->tot_commission }}</td>
                                                                    </tr>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                        @endforEach
                                                    @endforEach
                                                @else
                                                    @forEach($agids as $aid)
                                                    @php
                                                        $aids = App\Models\AgentTicket::where('agent_id',$aid)->where('ticket_id',$ticket->id)->pluck('id')->toArray();
                                                        $agt = App\Models\Agent::find($aid);
                                                    @endphp


                                                    @foreach (App\Models\AgentTicket::whereIn('id',$aids)->where('ticket_id',$ticket->id)->get() as $key => $agent_ticket)
                                                        @php
                                                            $totalTicket = 0;
                                                            $totalPrice = 0;
                                                            $commission = 0;
                                                            $price = 0;
                                                            $booking = null;
                                                            $agent = null;

                                                        @endphp
                                                        @empty($agent_ticket)
                                                        @else
                                                            @if ($agent_ticket->id)
                                                                @php
                                                                    $agent = \App\Models\AgentTicket::
                                                                        whereId($agent_ticket->id)
                                                                        ->first();
                                                                @endphp
                                                            @else
                                                                @php
                                                                    $agent = null;
                                                                @endphp
                                                            @endif

                                                            @if (!is_null($from_date) && !is_null($to_date))
                                                                @php
                                                                    $from_date = date('Y-m-d', strtotime($from_date));
                                                                    $to_date = date('Y-m-d', strtotime('+1 day', strtotime($to_date)));

                                                                    $booking = \App\Models\Booking::whereEventId($event_id)
                                                                        ->whereBetween('bookings.created_at', [$from_date, $to_date])
                                                                        ->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id')
                                                                        ->where('bookings.agent_id', $agent?->id)
                                                                        ->where('bookings.promocode', $agent?->coupon_code)
                                                                        ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', 'agent_tickets.commission', 'agent_tickets.discount', DB::raw(' COALESCE(SUM(bookings.price),0) as tot_net_price'), DB::raw(' COALESCE(SUM(bookings.quantity),0) as tot_quantity'), DB::raw('COALESCE(SUM(agent_tickets.commission),0) as tot_commission'), DB::raw('COALESCE(SUM(agent_tickets.discount),0) as tot_discount'))
                                                                        ->first();

                                                                    $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                                                                @endphp
                                                            @else
                                                                @php

                                                                    $booking = \App\Models\Booking::whereEventId($event_id)
                                                                        ->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id')
                                                                        ->where('bookings.agent_id', $agent?->id)
                                                                        ->where('bookings.promocode', $agent?->coupon_code)
                                                                        ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', 'agent_tickets.commission', 'agent_tickets.discount', DB::raw(' COALESCE(SUM(bookings.price),0) as tot_net_price'), DB::raw(' COALESCE(SUM(bookings.quantity),0) as tot_quantity'), DB::raw('COALESCE(SUM(agent_tickets.commission),0) as tot_commission'), DB::raw('COALESCE(SUM(agent_tickets.discount),0) as tot_discount'))
                                                                        ->first();
                                                                    $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                                                                    $totalPrices += $price;
                                                                    $totalTickets += $booking?->tot_quantity;
                                                                @endphp
                                                            @endif
                                                            @if( $booking?->tot_quantity > 0)
                                                            <tr>
                                                                <td>{{ $ticket->event->title }}</td>
                                                                <td>{{ $ticket->title }}</td>
                                                                <td>NA</td>
                                                                <td> {{  $agt->email }} </td>
                                                                <td>@if( !empty(trim($agent?->coupon_code) )){{ $agent?->coupon_code }} @else {{ $agent?->code }} @endif</td>
                                                                <td>£{{ number_format($price, 2) }}</td>
                                                                <td>{{ $booking?->tot_quantity }}</td>
                                                                <td>{{ $booking?->tot_commission }}</td>
                                                            </tr>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                    @endforEach
                                            @endif
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5"></td>

                                                <td style="white-space: nowrap;"><strong>Total: £{{ number_format($totalPrices, 2) }}</strong></td>
                                                <td style="white-space: nowrap;"><strong>Total Tickets: {{ $totalTickets }}</strong></td>
                                            </tr>
                                        </tfoot>
                                        </table>
                                        <form method="POST" action="{{ route('events.exportCommisionSale') }}">
                                            @csrf
                                            <input type="hidden" name="from_date" value="{{ request()->from_date }}">
                                            <input type="hidden" name="to_date" value="{{ request()->to_date }}">
                                            <input type="hidden" name="event_id" value="{{  request()->event }}">
                                            <input type="hidden" name="agent_ids" value="{{  implode(',',$agids) }}">
                                            <button type="submit" class="btn btn-primary pull-right" >Export Excel</button>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

@section('javascript')
    <script>
        $(document).ready(function(){
            @if(@$agentid)
                $('#agents').val({!! json_encode($agentid) !!}).trigger('change');
            @endif
        });
        $(function() {
            $.ajaxSetup({
                headers: {
                    "X-CSRFToken": getCookie("csrftoken")
                }
            });
        });
        $('#to_date').on('change',function(){
            if($('#from_date').val() == ''){
                alert("From Date is required");
                $('#to_date').val('');
            } else {
                if($('#from_date').val() >  $('#to_date').val()){
                    alert("To Date is greater than From Date");
                    $('#to_date').val('');
                }
            }

        });
        function getAgents(the) {
            var eventId = $(the).val();

            $.ajax({
                url: '{{ route('voyager.admin.get_agents_by_event') }}',
                type: "POST",
                data: {
                    'eventId': eventId
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(result) {
                    console.log(result.agents);
                    var agents = result.agents;
                    var html = '<option  value="all">Select All Agent</option>';
                    for (let x in agents) {
                        console.log(agents[x]['agent']['name']);
                        html += '<option  value="' + agents[x]['agent']['id'] + '">' + agents[x]['agent'][
                                'name'
                            ] +
                            '</option>';
                    }
                    $("#agents").html(html);
                    $("#agents").trigger('change');
                }
            });
        }
    </script>
@stop
