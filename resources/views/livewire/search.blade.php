<div>
    <div class="col-sm-12 col-md-12">
    <input type="text" wire:model="searchTerm" class="form-control mb-5" placeholder="Search Users...">
    </div>
    
    @foreach ($users as $user)
    <div class="col-sm-4 col-md-3">
        <input type="radio" name="user_id" required value="{{ $user->id }}"> {{ $user->name}}
    </div>
    @endforeach
</div>
