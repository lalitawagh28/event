@extends('layouts.app-report')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->getTranslatedAttribute('display_name_plural'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->getTranslatedAttribute('display_name_plural') }}
        </h1>

        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if ($isServerSide)
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <div class="col-2">
                                        <select id="search_key" name="key">
                                            <option value="transaction_id" @if('transaction_id' == @$search?->key) {{ 'selected' }}@endif>{{ 'Transaction ID' }}</option>
                                            <option value="order_number" @if('order_number' == @$search?->key) {{ 'selected' }}@endif>{{ 'Order No' }}</option>
                                            <option value="events.title" @if('events.title' == @$search?->key) {{ 'selected' }}@endif>{{ 'Event Title' }}</option>
                                            <option value="bookings.status" @if('bookings.status' == @$search?->key) {{ 'selected' }}@endif>{{ 'Status' }}</option>
                                        </select>
                                    </div>
                                    <div class="col-2">
                                        <select id="filter" name="filter">
                                            <option value="contains" @if($search->filter == "contains"){{ 'selected' }}@endif>{{ __('voyager::generic.contains') }}</option>
                                            <option value="equals" @if($search->filter == "equals"){{ 'selected' }}@endif>=</option>
                                        </select>
                                    </div>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                @if (Request::has('sort_order') && Request::has('order_by'))
                                    <input type="hidden" name="sort_order" value="{{ Request::get('sort_order') }}">
                                    <input type="hidden" name="order_by" value="{{ Request::get('order_by') }}">
                                @endif
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        @if($showCheckboxColumn)
                                            <th>
                                                <input type="checkbox" class="select_all">
                                            </th>
                                        @endif
                                        <th>
                                        {{ 'Total Tickets' }}
                                        </th>
                                        <th>
                                        {{ 'Transaction Id' }}
                                        </th>
                                        @foreach($dataType->browseRows as $row)
                                        <th>
                                            @if ($isServerSide)
                                                <a href="{{ $row->sortByUrl($orderBy, $sortOrder) }}">
                                            @endif
                                            {{ $row->getTranslatedAttribute('display_name') }}
                                            @if ($isServerSide)
                                                @if ($row->isCurrentSortField($orderBy))
                                                    @if ($sortOrder == 'asc')
                                                        <i class="voyager-angle-up pull-right"></i>
                                                    @else
                                                        <i class="voyager-angle-down pull-right"></i>
                                                    @endif
                                                @endif
                                                </a>
                                            @endif
                                        </th>
                                        @endforeach
                                        <th> {{ __('eventmie-pro::em.expired') }} </th>
                                        @if(Session::get('default_org_user') != auth()->user()->email)
                                        <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataTypeContent as $data)

                                    <tr>
                                            <td> {{ $data->event->tickets->sum('quantity')}} </td>
                                            <td><a style="text-decoration:underline" href="{{ route('admin.getTransactionDetails',[$data->id])}}" data-remote="false" data-toggle="modal" data-target="#transaction_modal" class="show_transaction_modal">
                                                        <div>{{ $data->transaction_id }}</div>
                                                    </a></td>
                                        @if($showCheckboxColumn)
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                                            </td>
                                        @endif
                                        @foreach($dataType->browseRows as $row)
                                            @php
                                            if ($data->{$row->field.'_browse'}) {
                                                $data->{$row->field} = $data->{$row->field.'_browse'};
                                            }
                                            @endphp
                                            <td>
                                                @if (isset($row->details->view))
                                                    @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $data->{$row->field}, 'action' => 'browse'])
                                                @elseif($row->type == 'image')
                                                    <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @elseif($row->type == 'relationship')
                                                    @include('voyager::formfields.relationship', ['view' => 'browse','options' => $row->details])
                                                @elseif($row->type == 'select_multiple')
                                                    @if(property_exists($row->details, 'relationship'))

                                                        @foreach($data->{$row->field} as $item)
                                                            {{ $item->{$row->field} }}
                                                        @endforeach

                                                    @elseif(property_exists($row->details, 'options'))
                                                        @if (!empty(json_decode($data->{$row->field})))
                                                            @foreach(json_decode($data->{$row->field}) as $item)
                                                                @if (@$row->details->options->{$item})
                                                                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            {{ __('voyager::generic.none') }}
                                                        @endif
                                                    @endif

                                                    @elseif($row->type == 'multiple_checkbox' && property_exists($row->details, 'options'))
                                                        @if (@count(json_decode($data->{$row->field})) > 0)
                                                            @foreach(json_decode($data->{$row->field}) as $item)
                                                                @if (@$row->details->options->{$item})
                                                                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            {{ __('voyager::generic.none') }}
                                                        @endif

                                                @elseif(($row->type == 'select_dropdown' || $row->type == 'radio_btn') && property_exists($row->details, 'options'))

                                                    @if($row->getTranslatedAttribute('display_name') == 'Booking Cancel')
                                                        @if($data->{$row->field} == 1)
                                                            {{ __('voyager::generic.pending') }}
                                                        @elseif($data->{$row->field} == 2)
                                                            {{ __('voyager::generic.approved') }}
                                                        @elseif($data->{$row->field} == 3)
                                                            {{ __('voyager::generic.refunded') }}
                                                        @else
                                                            {{ __('voyager::generic.no') }} {{ __('voyager::generic.cancellation') }}
                                                        @endif
                                                    @endif


                                                    @if($row->getTranslatedAttribute('display_name') == 'Status')
                                                        @if($data->{$row->field} == 1)
                                                            {{ __('voyager::generic.enabled') }}
                                                        @else
                                                            {{ __('voyager::generic.disabled') }}
                                                        @endif
                                                    @endif

                                                    @if($row->field == 'checked_in')
                                                        @if($data->{$row->field} == 1)
                                                            {{ __('voyager::generic.yes') }}
                                                        @else
                                                            {{ __('voyager::generic.no') }}
                                                        @endif
                                                    @endif

                                                    @if($row->field == 'payment_type')
                                                        @if($data->{$row->field} == 'offline')
                                                            {{ __('voyager::generic.offline') }}
                                                        @else
                                                        {{ __('voyager::generic.online') }}
                                                        @endif
                                                    @endif

                                                    @if($row->field == 'is_paid')
                                                        @if($data->{$row->field} == 1)
                                                            {{ __('voyager::generic.yes') }}
                                                        @else
                                                            {{ __('voyager::generic.no') }}
                                                        @endif
                                                    @endif


                                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                    @if ( property_exists($row->details, 'format') && !is_null($data->{$row->field}) )
                                                        {{ \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($row->details->format) }}
                                                    @else
                                                        {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'checkbox')
                                                    @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                                        @if($data->{$row->field})
                                                            <span class="label label-info">{{ $row->details->on }}</span>
                                                        @else
                                                            <span class="label label-primary">{{ $row->details->off }}</span>
                                                        @endif
                                                    @else
                                                    {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'color')
                                                    <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                @elseif($row->type == 'text')
                                                    <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'text_area')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    @if(json_decode($data->{$row->field}) !== null)
                                                        @foreach(json_decode($data->{$row->field}) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}" target="_blank">
                                                            {{ __('voyager::generic.download') }}
                                                        </a>
                                                    @endif
                                                @elseif($row->type == 'rich_text_box')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div>{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                @elseif($row->type == 'coordinates')
                                                    @include('voyager::partials.coordinates-static-image')
                                                @elseif($row->type == 'multiple_images')
                                                    @php $images = json_decode($data->{$row->field}); @endphp
                                                    @if($images)
                                                        @php $images = array_slice($images, 0, 3); @endphp
                                                        @foreach($images as $image)
                                                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                                        @endforeach
                                                    @endif
                                                @elseif($row->type == 'media_picker')
                                                    @php
                                                        if (is_array($data->{$row->field})) {
                                                            $files = $data->{$row->field};
                                                        } else {
                                                            $files = json_decode($data->{$row->field});
                                                        }
                                                    @endphp
                                                    @if ($files)
                                                        @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                            @foreach (array_slice($files, 0, 3) as $file)
                                                            <img src="@if( !filter_var($file, FILTER_VALIDATE_URL)){{ Voyager::image( $file ) }}@else{{ $file }}@endif" style="width:50px">
                                                            @endforeach
                                                        @else
                                                            <ul>
                                                            @foreach (array_slice($files, 0, 3) as $file)
                                                                <li>{{ $file }}</li>
                                                            @endforeach
                                                            </ul>
                                                        @endif
                                                        @if (count($files) > 3)
                                                            {{ __('voyager::media.files_more', ['count' => (count($files) - 3)]) }}
                                                        @endif
                                                    @elseif (is_array($files) && count($files) == 0)
                                                        {{ trans_choice('voyager::media.files', 0) }}
                                                    @elseif ($data->{$row->field} != '')
                                                        @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                            <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:50px">
                                                        @else
                                                            {{ $data->{$row->field} }}
                                                        @endif
                                                    @else
                                                        {{ trans_choice('voyager::media.files', 0) }}
                                                    @endif
                                                @else
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <span>{{ $data->{$row->field} }}</span>
                                                @endif
                                            </td>
                                        @endforeach

                                        <td>

                                            {{

                                            (\Carbon\Carbon::parse($data->event_end_date.' '.$data->event_end_time)->timezone(setting('regional.timezone_default')) <= \Carbon\Carbon::now()->timezone(setting('regional.timezone_default'))) ?  __('voyager::generic.yes') : __('voyager::generic.no')

                                            }}

                                        </td>
                                        @if(Session::get('default_org_user') != auth()->user()->email)
                                        <td class="no-sort no-click" id="bread-actions">

                                            {{-- delete event from frontend --}}
                                            <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-sm btn-danger delete pull-right" onclick='openDeleteModal("{{ $data->id }}")'>
                                                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                                            </a>

                                            <a href="{{ route('voyager.bookings.edit',[$data->id])}}" class="btn btn-sm btn-info edit pull-right" data-customer-id="{{$data->customer_id}}" data-booking-id="{{$data->id}}" data-event-id="{{$data->event_id}}" data-ticket-id="{{$data->ticket_id}}" data-remote="false" data-toggle="modal" data-target="#booking_modal">
                                                <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.edit') }}</span>
                                            </a>

                                            <a href="{{ route('eventmie.obookings_organiser_bookings_show',[$data->id])}}" class="btn btn-sm btn-warning view pull-right">
                                                <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.view') }}</span>
                                            </a>

                                            <a href="{{ route('eventmie.downloads_index',[$data->id, $data->order_number])}}" class="btn btn-sm btn-success download pull-right">
                                                <i class="voyager-download"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.download') }}</span>
                                            </a>

                                            {{-- Single delete modal --}}
                                            <div class="modal modal-danger fade" tabindex="-1" id="delete_modal{{ $data->id }}" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <form action="{{ route('eventmie.obookings_organiser_booking_delete',[$data->id]) }}" id="delete_form" method="GET">
                                                                {{ csrf_field() }}
                                                                <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                                                            </form>
                                                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if ($isServerSide)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends([
                                    's' => $search->value,
                                    'filter' => $search->filter,
                                    'key' => $search->key,
                                    'order_by' => $orderBy,
                                    'sort_order' => $sortOrder,
                                    'showSoftDeleted' => $showSoftDeleted,
                                ])->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="transaction_modal" class="modal fade right">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div id="booking_modal" class="modal modal-mask">
            <div class="modal-dialog modal-container">
                <div class="modal-content lgx-modal-box">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="title">{{ __('admin.update_booking_status') }} </h3>
                    </div>
                    <div class="alert alert-danger error_alert" role="alert" style="display: none">
                        <ul></ul>
                    </div>
                    <form  id="booking_form" method="POST" enctype="multipart/form-data">
                        <input type="hidden" class="form-control lgxname"  id="book_customer_id"  name="customer_id">
                        <input type="hidden" class="form-control lgxname"  id="book_event_id"  name="event_id" >
                        <input type="hidden" class="form-control lgxname"  id="book_booking_id" name="booking_id" >
                        <input type="hidden" class="form-control lgxname"  id="book_ticket_id" name="ticket_id">

                        <div class="modal-body">
                            <div class="form-group">
                                <label><strong>{{ __('admin.booking_cancellation') }}</strong></label>
                                <select  class="form-control"  name="booking_cancel">
                                    <option  value="0">{{ __('admin.no_cancellation') }} </option>
                                    <option  value="1">{{ __('admin.cancellation_pending') }} </option>
                                    <option  value="2">{{ __('admin.cancellation_approved') }} </option>
                                    <option  value="3">{{ __('admin.amount_refunded') }} </option>
                                </select>

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    <label><strong>{{ __('admin.booking_status') }}</strong></label>
                                </div>
                                <div class="col-md-9">
                                    <label class="radio-inline">
                                        <input type="radio" value="1" name="status" > {{ __('admin.enable') }}
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" value="0" name="status"> {{ __('admin.disable') }}
                                    </label>
                                </div>

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    <label><strong>{{ __('admin.booking_paid') }} </strong></label>
                                </div>
                                <div class="col-md-9">
                                    <label class="radio-inline">
                                        <input type="radio" value="1" name="is_paid" > {{ __('admin.yes') }}
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" value="0" name="is_paid"> {{ __('admin.no') }}
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn lgx-btn lgx-btn-red"><i class="fas fa-sd-card"></i> {{ __('admin.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@stop

@section('css')
<style>
    th {
        white-space: nowrap;
    }
</style>
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
         $("#transaction_modal").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-body").load(link.attr("href"));
        });
        $("#booking_modal").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find("#book_customer_id").val($(link).data("customer-id"));
            $(this).find("#book_event_id").val($(link).data("event-id"));
            $(this).find("#book_booking_id").val($(link).data("booking-id"));
            $(this).find("#book_ticket_id").val($(link).data("ticket-id"));
        });
        $(document).ready(function () {
            $(document).on('submit', '#booking_form', function(event){
                var info = $('.error_alert');
                event.preventDefault();
                var data = new FormData($('#booking_form')[0]);
                $.ajax({
                    url: "{{route('eventmie.obookings_organiser_bookings_edit')}}",
                    type: "POST",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    error: function(response) {
                        let resp = JSON.parse(response.responseText)
                        if(resp.errors)
                        {
                            $.each(resp.errors, function(index, error){
                                info.find('ul').append(error);
                            });
                            info.slideDown();
                        }
                    }
                }).done(function(response) {
                    info.hide().find('ul').empty();
                    if(response.errors)
                    {
                        $.each(response.errors, function(index, error){
                            info.find('ul').append(error);
                        });
                        info.slideDown();
                    }
                    else if(response.status){
                        info.hide().find('ul').empty();
                        showNotification('success', 'Booking updated Successfully');
                        $('#booking_modal .close').click();
                    }

                });
            });
            @if (!$dataType->server_side)
                var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => $orderColumn,
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
            @else
                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
            //send email
            send_email();
        });


        var deleteFormAction;
        function openDeleteModal(id) {
            $('#delete_modal'+id).modal('show');
        }

        @if($usesSoftDeletes)
            @php
                $params = [
                    's' => $search->value,
                    'filter' => $search->filter,
                    'key' => $search->key,
                    'order_by' => $orderBy,
                    'sort_order' => $sortOrder,
                ];
            @endphp
            $(function() {
                $('#show_soft_deletes').change(function() {
                    if ($(this).prop('checked')) {
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 1]), true)) }}"></a>');
                    }else{
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 0]), true)) }}"></a>');
                    }

                    $('#redir')[0].click();
                })
            })
        @endif
        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });

        // send notification after successful booking
        function send_email(){
            var is_success       = {!! json_encode($is_success, JSON_HEX_TAG) !!};
            var send_email_route = {!! json_encode(route('eventmie.send_email'), JSON_HEX_TAG) !!};

            if(is_success <= 0)
                return false;

            var msg = "@lang('eventmie-pro::em.booking_success')<br> @lang('eventmie-pro::em.booking_mail')";
            showLoaderNotification(msg);

            $.ajax({
                type: 'GET',
                url: send_email_route,
                dataType: "JSON", // data type expected from server
                success: function (data) {
                    // hide loader
                    if(data.status > 0){
                        Swal.close();
                        var msg = "<p>@lang('eventmie-pro::em.booking_success')</p>"+
                                "<p>@lang('eventmie-pro::em.booking_email_sent')</p>";

                        showNotification('success', msg);
                    }
                },
                error: function(error) {
                    showNotification('error', 'failed');
                }
            });
        }

        // show loader with notification
        function showLoaderNotification(message) {

            Swal.fire ({
                title: message,
                allowEscapeKey: false,
                allowOutsideClick: false,
                showConfirmButton: false,
                onBeforeOpen: () => {
                  Swal.showLoading ()
                }
            })
        }


        // show notification

        function showNotification(type, message) {

            const Toast = Swal.mixin({
                toast: true,
                position: 'top-right',
                showConfirmButton: false,
                timer: 4000,
                customClass: {
                    container: 'custom-swal-container',
                    popup: 'custom-swal-popup custom-swal-popup-'+type,
                    header: 'custom-swal-header',
                    title: 'custom-swal-title',
                    closeButton: 'custom-swal-close-button',
                    image: 'custom-swal-image',
                    content: 'custom-swal-content',
                    input: 'custom-swal-input',
                    actions: 'custom-swal-actions',
                    confirmButton: 'custom-swal-confirm-button',
                    cancelButton: 'custom-swal-cancel-button',
                    footer: 'custom-swal-footer'
                }
            });
            Toast.fire({
                type: type,
                html: message
            })
        }

    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@stop
