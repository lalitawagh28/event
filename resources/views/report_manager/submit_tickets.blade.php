@extends('layouts.app-report')

@section('page_title', __('voyager::generic.viewing') . ' ' . __('admin.submit_tickets'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-plus"></i> {{ __('admin.submit_tickets') }}
        </h1>
        <a href="javascript:;" class="btn btn-success btn-add-new" id="bulk_upload_btn">
            <i class="voyager-upload"></i> <span>{{ __('admin.bulk_upload') }}</span>
        </a>
    </div>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">

                <form
                    action="@if (@$agent_ticket->id) {{ route('voyager.report-manager.ticket_update', $agent_ticket->id) }}@else{{ route('voyager.report-manager.ticket_store') }} @endif"
                    method="POST" role="form">
                    <!-- CSRF TOKEN -->
                    {{ csrf_field() }}
                    @if (@$agent_ticket->id)
                        <input type="hidden" id="agent_ticket_id" value="{{ $agent_ticket->id }}" name="id">
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-bordered">
                                {{-- <div class="panel"> --}}
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="agent_id">{{ __('admin.agent_id') }}</label>
                                        <input type="text" class="form-control" id="agent_id" name="agent_id"
                                            value="{{ $agent->id }}" placeholder="{{ __('admin.enter_agent_id') }}"
                                            value="{{ old('agent_id', $agent_ticket->agent_id ?? '') }}" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="event_id">{{ __('admin.event_name') }}</label>
                                        <select class="form-control select2" id="event_id" name="event_id">
                                            <option class="disabled" value="">{{ __('admin.select_event') }}</option>
                                            @foreach ($events as $event)
                                                <option value="{{ $event->id }}"
                                                    {{ $event->id == old('event_id', $agent_ticket->ticket->event_id ?? '') ? 'selected' : '' }}>
                                                    {{ $event->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="agent_name">{{ __('admin.agent_name') }}</label>
                                        <input type="text" class="form-control" id="agent_id" name="agent_name"
                                            value="{{ $agent->name }}" placeholder="{{ __('admin.enter_agent_name') }}"
                                            value="{{ old('agent_id', $agent_ticket->agent_id ?? '') }}" readonly>
                                    </div>
                                    <table id="ticket_add" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th class="white-space" style="padding:10px; white-space: nowrap;">
                                                    {{ __('admin.ticket_category') }}</th>
                                                <th class="white-space" style="padding:10px; white-space: nowrap;">
                                                    {{ __('admin.sub_category') }}
                                                </th>
                                                <th class="white-space" style="padding:10px; white-space: nowrap;">
                                                    {{ __('admin.type') }}</th>
                                                <th class="white-space" style="padding:10px; white-space: nowrap;">
                                                    {{ __('admin.number_of_tickets') }}</th>
                                                <th class="white-space" style="padding:10px; white-space: nowrap;">
                                                    {{ __('admin.agent_commission') }}</th>
                                                <th class="white-space">{{ __('admin.price') }}</th>
                                                <th class="white-space" style="padding:10px; white-space: nowrap;">
                                                    {{ __('admin.coupon_code') }}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (old('ticket_id', $agent_ticket->ticket_id ?? []))
                                                @foreach (old('ticket_id', []) as $key => $ticket)
                                                    <tr @if ($loop->first) id="row_add" @endif>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            <input type="text" readonly class="ticket_name form-control"
                                                                name="ticket_name" value="">
                                                            <input type="hidden" class="ticket_id form-control"
                                                                name="ticket_id[]" value="">
                                                        </td>

                                                        <td class="sub_category white-space"></td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            <select style="width: auto;" class="coupon_type form-control"
                                                                name="coupon_type[]">
                                                                <option value="general" selected>
                                                                    {{ __('admin.general') }}</option>
                                                                <option value="student">{{ __('admin.student') }}</option>
                                                            </select>
                                                        </td>
                                                        <td class="white-space" style="padding:10px; white-space: nowrap;">
                                                            <input type="text" class="quantity form-control"
                                                                name="quantity[]" value=""
                                                                placeholder="{{ __('admin.enter_no_tickets') }}"
                                                                value="">
                                                            <span style="margin-top:5px;display:block;"
                                                                class="available_tickets" class="text-primary"></span>
                                                        </td>

                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            <input type="text" class="commission form-control"
                                                                name="commission[]"
                                                                placeholder="{{ __('admin.enter_agent_commission') }}"
                                                                value="">
                                                            <span style="margin-top:5px;display:block;"
                                                                class="original_price" class="text-primary"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            <input type="text" class="price form-control"
                                                                name="price[]"
                                                                placeholder="{{ __('admin.enter_price') }}"
                                                                value="" readonly>
                                                        </td>

                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            <input type="text"class="coupon_code form-control"
                                                                name="coupon_code[]"
                                                                placeholder="{{ __('admin.enter_coupon_code') }}"
                                                                value="" readonly>
                                                        </td>
                                                    </tr>
                                                @endforEach
                                            @else
                                                <tr id="row_add">
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <input type="text" readonly class="ticket_name form-control"
                                                            name="ticket_name" value="">
                                                        <input type="hidden" class="ticket_id form-control"
                                                            name="ticket_id[]" value="">
                                                    </td>

                                                    <td style="padding:10px;vertical-align: top; white-space: nowrap;"
                                                        class="sub_category white-space">

                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <select class="coupon_type form-control" name="coupon_type[]">
                                                            <option value="general" selected>{{ __('admin.general') }}
                                                            </option>
                                                            <option value="student">{{ __('admin.student') }}</option>
                                                        </select>
                                                    </td>
                                                    <td class="white-space" style="padding:10px; white-space: nowrap;">
                                                        <input type="text" class="quantity form-control"
                                                            name="quantity[]" value="0"
                                                            placeholder="{{ __('admin.enter_no_tickets') }}"
                                                            value="">
                                                        <span style="margin-top:5px;display:block;"
                                                            class="available_tickets" class="text-primary"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <input type="text" class="commission form-control"
                                                            value="0.00" name="commission[]"
                                                            placeholder="{{ __('admin.enter_agent_commission') }}"
                                                            value="">
                                                        <span style="margin-top:5px;display:block;" class="original_price"
                                                            class="text-primary"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <input type="text" class="price form-control" value="0.00"
                                                            name="price[]" placeholder="{{ __('admin.enter_price') }}"
                                                            value="" readonly>
                                                    </td>

                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <input type="text"class="coupon_code form-control"
                                                            name="coupon_code[]"
                                                            placeholder="{{ __('admin.enter_coupon_code') }}"
                                                            value="" readonly>
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    @if (!@$agent_ticket)
                                        <div class="form-group">
                                            <label for="common_code">{{ __('admin.coupon_code') }}</label>
                                            <input type="text" class="coupon_code form-control" id="coupon_code"
                                                name="common_code" placeholder="{{ __('admin.enter_coupon_code') }}"
                                                value="{{ old('common_code', @$agent_ticket->code) }}" readonly>
                                            <span style="margin-top:5px;display:inline-block;"
                                                class="text-primary">{{ __('admin.auto_generate_message') }} </span> <span
                                                class="text-primary" id="edit_coupon"> <i class="voyager-edit"></i> <i
                                                    style="display:none" class="voyager-brush"></i> </span>
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary pull-right save">
                                        {{ __('voyager::generic.save') }}
                                    </button>
                                </div>


                            </div>
                        </div>
                    </div>


                </form>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .page-content -->

    <div class="modal modal-danger fade" tabindex="-1" id="bulk_upload_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('voyager.report-manager.upload_event_allocation') }}" enctype='multipart/form-data'
                    id="bulk_upload_form" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">
                            <i class="voyager-upload"></i> {{ __('admin.upload_excel') }} <span>
                        </h4>
                    </div>
                    <div class="modal-body" id="bulk_upload_modal_body">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="event_id">{{ __('admin.download_event_template') }}</label>
                                <select class="form-control select2" id="event_upload_id" name="event_upload_id">
                                    <option class="disabled" value="">{{ __('admin.select_event') }}</option>
                                    @foreach ($events as $event)
                                        <option value="{{ $event->id }}">{{ $event->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <a class="hidden" id="download_excel_link" href=""><i class="voyager-download"></i>
                                {{ __('admin.download_template') }} <span></a>
                            <div class="form-group">
                                <input type="file" name="import_csv" id="bulk_upload_file" value=""
                                    class="hidden">
                                <input type="hidden" name="agent_id" value="{{ $agent->id }}">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer" class="hidden">
                        <input type="submit" class="btn btn-danger pull-right"
                            value="{{ __('admin.upload_agent_allocation') }} ">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                            {{ __('voyager::generic.cancel') }}
                        </button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop

@section('javascript')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

    <script>
        var agent_id = "{{ old('agent_id', $agent_ticket->agent_id ?? '') }}";
        var download_link = "{{ route('voyager.report-manager.download_template', ['id' => '?', 'agent_id' => $agent->id]) }}";
        $(document).ready(function() {
            $('#event_upload_id').on('change', function() {
                var id = $(this).val();

                if (id) {
                    $('#download_excel_link').attr('href', download_link.replace('?', id));
                    $('#download_excel_link').removeClass('hidden');
                    $('#bulk_upload_file').removeClass('hidden');
                } else {
                    $('#download_excel_link').addClass('hidden');
                    $('#bulk_upload_file').addClass('hidden');
                }

            });
            $('#event_id').trigger("change");
            $('#add_button').on('click', function(e) {
                e.preventDefault();
                $('#row_add').clone().insertAfter($('#ticket_add tbody tr:last')).removeAttr('id').find(
                    '.delete_row').removeAttr('disabled');
                $('.delete_row').on('click', function(e) {
                    e.preventDefault();
                    $(this).closest('tr').remove();
                });
            });
        });
        $('#edit_coupon').on('click', function() {
            if ($('.coupon_code').prop('readonly')) {
                $('.coupon_code').prop('readonly', false);
                $('#edit_coupon .voyager-edit').hide();
                $('#edit_coupon .voyager-brush').show();
            } else {
                $('.coupon_code').val('');
                $('.coupon_code').prop('readonly', true);
                $('#edit_coupon .voyager-edit').show();
                $('#edit_coupon .voyager-brush').hide();
            }

        });
        $('#coupon_code').on('keyup', function() {
            $('.coupon_code').val($(this).val());
            $('.coupon_code').each(function() {
                setInputFilter($(this).get(0), function(value) {
                    return /[a-zA-Z0-9&\-]{5,25}$/.test(
                        value); // Allow digits and '.' only, using a RegExp.
                }, "Only 6 to 25 characters  are allowed");
            });


        })
        $('body').on('keyup', '#ticket_add tbody tr .quantity', function() {
            var tr = $(this).closest('tr');

            if ($(this).val() && $(this).val() != '-') {
                if ($('#event_id').val() == '' || $(tr).find('.ticket_name').val() == '') return false;
                var value = Number($(this).val())
                $(this).val(value);

                tickets = Number($(tr).find(".ticket_id").data("available")) - Number($(this).val());
                price = Number($(tr).find(".ticket_id").data("price"));
                old_price = Number($(tr).find(".ticket_id").data("old-price"));
                if (tickets < 0) {
                    $(this).val(0);
                    tickets = Number($(tr).find(".ticket_id").data("available"))
                }
                $(tr).find('.available_tickets').html('Available Ticket: ' + tickets)
                if (old_price) {
                    $(tr).find('.original_price').html('Sales Price: ' + price);
                } else {
                    $(tr).find('.original_price').html('Original Price: ' + price);
                }
            } else {
                if ($('#event_id').val() == '' || $(tr).find('.ticket_name').val() == '') return false;
                tickets = Number($(tr).find(".ticket_id").data("available"));
                price = Number($(tr).find(".ticket_id").data("price"));
                old_price = Number($(tr).find(".ticket_id").data("old-price"));
                $(tr).find('.available_tickets').html('Available Ticket: ' + tickets)
                if (old_price) {
                    $(tr).find('.original_price').html('Sales Price: ' + price);
                } else {
                    $(tr).find('.original_price').html('Original Price: ' + price);
                }
            }
        });
        $('body').on('keyup', '#ticket_add tbody tr .commission', function() {
            let value = String(parseFloat($(this).val()));
            var commission = Number($(this).val());
            var tr = $(this).closest('tr');
            var price = $(tr).find(".ticket_id").data("price")
            if (commission > price) {
                $(tr).find('.price').val('0.0');
                $(this).val(price);
            } else {
                if (commission && price) {
                    price = Number(price);
                    data = price - commission;
                    $(tr).find('.price').val(data)
                } else {
                    if(commission == 0){
                        $(tr).find('.price').val(price)
                    } else {
                        $(tr).find('.price').val("0.00")
                    }

                }
            }
        });
        $('body').on('change', '#ticket_add tbody tr .ticket_id', function() {
            var tr = $(this).closest('tr');
            $(tr).find('.commission').trigger("keyup");
            tickets = Number($("#ticket_id").data("available")) - Number($(tr).find('.quantity').val());
            price = Number($(tr).find(".ticket_id").data("price"));
            old_price = Number($(tr).find(".ticket_id").data("old-price"));
            $(tr).find('.available_tickets').html('Available Ticket: ' + tickets)
            if (old_price) {
                $(tr).find('.original_price').html('Sales Price: ' + price);
            } else {
                $(tr).find('.original_price').html('Original Price: ' + price);
            }

        });

        $('body').on('change', '#event_id', function() {
            var event_id = $(this).val();

            var agent_ticket_id = $('#agent_ticket_id').val();
            if (!agent_ticket_id) {
                agent_ticket_id = 0;
            }
            if (event_id) {
                console.log(event_id);
                $.ajax({
                    url: "/organizer/get-tickets/" + event_id + "/" + agent_ticket_id,
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        $('#ticket_add tbody tr').not("#row_add").remove();


                        $(response.tickets).each(function(index, item) {
                            if (index == 0) {
                                var row = $('#row_add');


                                $(row).find('.ticket_id').attr('data-available', item.available)
                                    .attr('data-price', item.price).attr('data-old-price', item
                                        .old_price)
                                $(row).find('.ticket_name').val(item.title);
                                if (item.sub_category) {
                                    $(row).find('.sub_category').empty();
                                    $(row).find('.sub_category').append(
                                        '<input type="text" name="sub_category_name" readonly class="sub_caregory_name form-control"  value="' +
                                        item.sub_category +
                                        '"/><input type="hidden" name="sub_category_id[' +
                                        index + ']"    value="' + item.sub_category_id +
                                        '"/>')
                                }
                                $(row).find('.ticket_id').val(item.id).trigger('change');
                                $(row).find('.ticket_name').val(item.title)
                                $(row).find('.available_tickets').html('Available Ticket: ' +
                                    item.available);
                                if (item.old_price) {
                                    $(row).find('.original_price').html('Sales Price: ' + item
                                        .price)
                                } else {
                                    $(row).find('.original_price').html('Original Price: ' +
                                        item.price)
                                }
                                $(row).find('.price').val(item.price);
                            } else {
                                var row = $('#row_add').clone().insertAfter($(
                                    '#ticket_add tbody tr:last')).removeAttr('id');

                                $(row).find('.ticket_id').attr('data-available', item.available)
                                    .attr('data-price', item.price).attr('data-old-price', item
                                        .old_price)
                                $(row).find('.delete_row').removeAttr('disabled');
                                $(row).find('.ticket_id').val(item.id).trigger('change');
                                $(row).find('.ticket_name').val(item.title);
                                if (item.sub_category) {
                                    $(row).find('.sub_category').empty();
                                    $(row).find('.sub_category').append(
                                        '<input type="text" name="sub_category_name" readonly class="sub_caregory_name form-control"  value="' +
                                        item.sub_category +
                                        '"/><input type="hidden" name="sub_category_id[' +
                                        index + ']"    value="' + item.sub_category_id +
                                        '"/>')
                                }
                                $(row).find('.ticket_name').val(item.title)
                                $(row).find('.available_tickets').html('Available Ticket: ' +
                                    item.available);
                                if (item.old_price) {
                                    $(row).find('.original_price').html('Sales Price: ' + item
                                        .price)
                                } else {
                                    $(row).find('.original_price').html('Original Price: ' +
                                        item.price)
                                }
                                $(row).find('.price').val(item.price);
                            }
                        });
                        $('.coupon_code').each(function() {
                            setInputFilter($(this).get(0), function(value) {
                                return /[a-zA-Z0-9&\-]{5,25}$/.test(
                                    value
                                ); // Allow digits and '.' only, using a RegExp.
                            }, "Only 5 to 25 characters  are allowed");
                        });
                        // $('#row_add').find('select').prop('readonly', true);
                        // $('#row_add').find('select').select2();

                        // if(agent_id != ''){
                        //     agent_id ='';
                        // }
                    },
                    error: function(xhr, status, errorThrown) {
                        xhr.status;
                        alert('Failed to get Tickets!');
                    }
                });
            }
        });

        /********** End Relationship Functionality **********/

        function setInputFilter(textbox, inputFilter, errMsg) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "focusout"].forEach(function(event) {
                textbox.addEventListener(event, function(e) {
                    const key = e.key; // const {key} = event; ES6+
                    if (["keyup", "mouseup", "input", "focusout"].indexOf(e.type) >= 0) {
                        const pattern = /[a-zA-Z0-9\-]{0,25}$/;
                        if (!pattern.test(this.value)) {
                            this.value = this.oldValue;
                            return;
                        }
                    }
                    if (["input", "focusout"].indexOf(e.type) >= 0) {
                        if (this.value.length < 5) {
                            this.setCustomValidity(errMsg);
                            this.reportValidity();
                        }
                    }
                    if (inputFilter(this.value)) {
                        // Accepted value.
                        if (["keydown", "mousedown", "focusout"].indexOf(e.type) >= 0) {
                            this.classList.remove("input-error");
                            this.setCustomValidity("");

                        }
                    }
                    if (key === "Backspace" || key === "Delete") {
                        if (this.value.length == 1 && this.oldValue == this.value) {
                            this.oldValue = '';
                            this.value = '';
                        }
                    }
                    if (this.value.length > 25) {
                        this.setCustomValidity(errMsg);
                        this.reportValidity();
                        this.value = this.oldValue;
                    }
                    this.oldValue = this.value;
                });
            });
        }
        setInputFilter(document.getElementById("coupon_code"), function(value) {
            return /[a-zA-Z0-9&\-]{5,25}$/.test(value); // Allow digits and '.' only, using a RegExp.
        }, "Only 5 to 25 characters  are allowed");

        var $bulkUploadBtn = $('#bulk_upload_btn');
        var $bulkUploadModal = $('#bulk_upload_modal');
        $bulkUploadBtn.on('click', function(e) {
            $bulkUploadModal.modal('show');
        });
    </script>
@stop
