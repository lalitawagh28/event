@extends('layouts.app-report')
@section('page_title', __('voyager::generic.viewing').' Sales Report By Agent')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 mb-0">
                <h1 class="page-title">
                    <i class="voyager-plus"></i> Sales Report By Agent
                </h1>
            </div>

        </div>
    </div>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">




                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-bordered">
                            {{-- <div class="panel"> --}}


                            <div class="panel-body">
                                <form action="{{ route('voyager.report-manager.get_sales_report_by_agent_data') }}" method="POST"
                                    role="form">
                                    <!-- CSRF TOKEN -->
                                    {{ csrf_field() }}
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="agent_id">Event <span class="text-danger">*</span></label>
                                        <select class="form-control select2" id="event" name="event"  onchange="getAgents(this)" required>
                                            <option class="disabled" value="">{{ __('admin.select_event') }}</option>
                                            @foreach ($events as $event)
                                                <option value="{{ $event->id }}" @if(old('event',request()->event) == $event->id ) selected @endif>{{ $event->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="agent_id">Agents <span class="text-danger">*</span> </label>
                                        <select class="form-control select2" id="agents" multiple name="agents[]" required>
                                            <option value="all">Select All Agent</option>
                                            @if(!empty($agents))
                                            @foreach ($agents as $agent)
                                                <option value="{{ $agent->agent->id }}" @if($agent->agent->id == request()->agent) selected @endif>{{ $agent->agent->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="event_id">From Date</label>
                                        <input type="date" class="form-control" id="from_date" name="from_date" value="{{ old('from_date',request()->from_date)}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="agent_name">To Date</label>
                                        <input type="date" class="form-control" id="to_date" name="to_date" value="{{ old('to_date',request()->to_date)}}">
                                    </div>


                                    <button type="submit" class="btn btn-primary pull-right save">
                                        {{ __('voyager::generic.submit') }}
                                    </button>
                                </form>

                                @if (!is_null(@$bookings))
                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                        <th>Event Name</th>
                                        <th>Ticket Category</th>
                                        <th>Ticket Sub Category</th>
                                        <th>Agent Name</th>
                                        <th title="Sales Amount = Original Price - Agent Commission - User discount"><span style="color:red;pointer-events: auto;">?</span> Sales</th>
                                        <th>No Of Ticket</th>
                                    </thead>
                                    <tbody>
                                        @php
                                        $totalPrices = 0;
                                        $totalTickets = 0;
                                        @endphp
                                        @foreach ($selectedEvent as $ticket)
                                         @if($ticket->sub_category)
                                            @forEach($ticket->sub_categories as $subCat)
                                            @forEach($agids as $aid)
                                            @php
                                                $totalTicket = 0;
                                                $totalPrice = 0;
                                                $price=0;
                                                $aids = App\Models\AgentTicket::where('agent_id',$aid)->where('sub_category_id',$subCat->id)->pluck('id')->toArray();
                                                $agent = App\Models\Agent::find($aid);
                                            @endphp
                                            @if(!is_null(request()->from_date) && !is_null(request()->to_date))
                                                @php
                                                $booking  = \App\Models\Booking::whereEventId(request()->event)->whereBetween('bookings.created_at', [date('Y-m-d', strtotime(request()->from_date)), date('Y-m-d', strtotime('+1 day', strtotime(request()->to_date)))])
                                                ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                                                ->whereIn('agent_tickets.id', @$aids ?? [])
                                                ->where('bookings.sub_category_id',$subCat->id)
                                                ->select('bookings.*','agent_tickets.id','agent_tickets.agent_id',DB::raw("SUM(bookings.price) as tot_net_price"),
                                                    DB::raw(" COALESCE(SUM(bookings.quantity),0) as tot_quantity"),
                                                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount"))
                                                ->first();
                                                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                                                @endphp
                                            @else
                                                @php
                                                $booking  = \App\Models\Booking::whereEventId(request()->event)
                                                ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                                                ->whereIn('agent_tickets.id', @$aids ?? [])
                                                ->where('bookings.sub_category_id',$subCat->id)
                                                ->select('bookings.*','agent_tickets.id','agent_tickets.agent_id',DB::raw("SUM(bookings.price) as tot_net_price"),
                                                    DB::raw(" COALESCE(SUM(bookings.quantity),0) as tot_quantity"),
                                                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount"))
                                                ->first();
                                                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                                                $totalPrices += $price;
                                                $totalTickets += $booking?->tot_quantity;
                                                @endphp
                                            @endif

                                                @if($booking?->tot_quantity > 0)
                                                 <tr>
                                                    <td>{{ $ticket->event->title }}</td>
                                                    <td>{{ $ticket->title }}</td>
                                                    <td>{{ $subCat->title }}</td>
                                                    <td> {{  $agent->email }} </td>
                                                    <td>£{{ number_format($price, 2) }}</td>
                                                    <td>{{ $booking?->tot_quantity }}</td>
                                                </tr>
                                                @endif
                                            @endforEach
                                            @endforEach
                                         @else
                                         @forEach($agids as $aid)
                                         @php
                                             $totalTicket = 0;
                                             $totalPrice = 0;
                                             $price=0;
                                             $aids = App\Models\AgentTicket::where('agent_id',$aid)->where('ticket_id',$ticket->id)->pluck('id')->toArray();
                                             $agent = App\Models\Agent::find($aid);
                                         @endphp
                                            @if(!is_null(request()->from_date) && !is_null(request()->to_date))
                                                @php
                                                $booking  = \App\Models\Booking::whereEventId(request()->event)->whereBetween('bookings.created_at', [date('Y-m-d', strtotime(request()->from_date)), date('Y-m-d', strtotime('+1 day', strtotime(request()->to_date)))])
                                                ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                                                ->whereIn('agent_tickets.id', @$aids ?? [])
                                                ->where('bookings.ticket_id',$ticket->id)
                                                ->select('bookings.*','agent_tickets.id','agent_tickets.agent_id',DB::raw("SUM(bookings.price) as tot_net_price"),
                                                DB::raw(" COALESCE(SUM(bookings.quantity),0) as tot_quantity"),
                                                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount"))
                                                ->first();
                                                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                                                @endphp
                                            @else
                                                @php
                                                $booking  = \App\Models\Booking::whereEventId(request()->event)
                                                ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                                                ->whereIn('agent_tickets.id', @$aids ?? [])
                                                ->where('bookings.ticket_id',$ticket->id)
                                                ->select('bookings.*','agent_tickets.id','agent_tickets.agent_id',DB::raw("SUM(bookings.price) as tot_net_price"),
                                                    DB::raw(" COALESCE(SUM(bookings.quantity),0) as tot_quantity"),
                                                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount"))
                                                ->first();
                                                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                                                $totalPrices += $price;
                                                $totalTickets += $booking?->tot_quantity;
                                                @endphp
                                            @endif

                                            @if($booking?->tot_quantity > 0)
                                            <tr>
                                                <td>{{ $ticket->event->title }} </td>
                                                <td>{{ $ticket->title }}</td>
                                                <td>NA</td>
                                                <td> {{  $agent->email }} </td>
                                                <td>£{{ number_format($price, 2) }}</td>
                                                <td>{{ $booking?->tot_quantity }}</td>
                                            </tr>
                                            @endif
                                            @endforEach
                                        @endif
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4"></td>

                                            <td><strong>Total: £{{ number_format($totalPrices, 2) }}</strong></td>
                                            <td><strong>Total Tickets: {{ $totalTickets }}</strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <form method="POST" action="{{ route('events.exportAgentSale') }}">
                                    @csrf
                                    <input type="hidden" name="from_date" value="{{ request()->from_date }}">
                                    <input type="hidden" name="to_date" value="{{ request()->to_date }}">
                                    <input type="hidden" name="event_id" value="{{  request()->event }}">
                                    <input type="hidden" name="agent_ids" value="{{  implode(',',$agids) }}">
                                    <button type="submit" class="btn btn-primary pull-right" >Export Excel</button>
                                </form>
                            @endif
                            </div>


                        </div>
                    </div>
                </div>




            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .page-content -->



@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            @if(@$agentIds)
                $('#agents').val({!! json_encode($agentIds) !!}).trigger('change');
            @endif
        });
        $(function() {
            $.ajaxSetup({
                headers: {
                    "X-CSRFToken": getCookie("csrftoken")
                }
            });
        });

        function getAgents(the) {
            var eventId = $(the).val();

            $.ajax({
                url: '{{ route('voyager.report-manager.get_agents_by_event') }}',
                type: "POST",
                data: {
                    'eventId': eventId
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(result) {
                    console.log(result.agents);
                    var agents = result.agents;
                    var html = '<option value="all">Select All Agent</option>';
                    for (let x in agents) {
                        console.log(agents[x]['agent']['name']);
                        html += '<option  value="' + agents[x]['agent']['id'] + '">' + agents[x]['agent']['name'] +
                            '</option>';
                    }
                    $("#agents").html(html);
                    $("#agents").trigger('change');
                }
            });
        }
        $('#to_date').on('change',function(){
            if($('#from_date').val() == ''){
                alert("From Date is required");
                $('#to_date').val('');
            } else {
                if($('#from_date').val() >  $('#to_date').val()){
                    alert("To Date is greater than From Date");
                    $('#to_date').val('');
                }
            }

        });
    </script>


@stop
