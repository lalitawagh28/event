@extends('eventmie::layouts.app')

{{-- @section('title', $event->title)
@section('meta_title', $event->meta_title)
@section('meta_keywords', $event->meta_keywords)
@section('meta_description', $event->meta_description)
@section('meta_image', '/storage/'.$event['thumbnail'])
@section('meta_url', url()->current()) --}}

    
@section('content')




<!--ABOUT-->
<section>
    <div id="lgx-about" class="lgx-about">
        <div class="mt-30 mb-50 mt-mobile-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 m-auto">
                        <div class="">
                            <div class="lgx-banner-info-circle lgx-info-circle">
                                <div class="lgx-registration-form">
                                   
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="card mb-3">
                                                    <h3 class="title px-5">
                                                        Event Information
                                                    </h3>
                                
                                                    <div class="card-body px-5">
                                                        <p><strong class="pb-3"> Event Title: </strong> {{ $booking->event_title}}</p>
                                                        <p><strong class="pb-3"> Total Ticket Quantity : </strong> {{ $qty}}</p>
                                                        <p><strong class="pb-3"> Total Ticket Price : </strong> {{ $price}}</p>
                                                    
                                                        @php
                                                        $wallet_url = (config('app.env') == 'production') ? "https://events.dhigna.com/update-password" : "https://eventsuat.dhigna.com/update-password";
                                                        @endphp
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            <ticket-transfer 
                                                        :booking="{{ json_encode($booking, JSON_HEX_APOS) }}" 
                                                        :wallets="{{ json_encode($wallets, JSON_HEX_APOS) }}"
                                                        :wallet_url="{{ json_encode($wallet_url, JSON_HEX_APOS) }}"
                                                        :price="{{ json_encode($price, JSON_HEX_APOS) }}"
                                                        :title="{{ json_encode($booking->event_title, JSON_HEX_APOS) }}"
                                                    >
                                                    </ticket-transfer>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="lgx-about-content-area">
                            <div class="lgx-heading">
                                <h2 class="heading"></h2>
                                
                            </div>
                           
                        </div>
                    </div>

                </div>
                <br><br>
               
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
<!--ABOUT END-->
{{-- CUSTOM --}}
{{-- Seating Chart Image --}}



<!--GOOGLE MAP END-->

@endsection

@section('javascript')

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var stripe_publishable_key   = {!! json_encode(setting('apps.stripe_public_key')) !!};
    
    var stripe_secret_key        = {!! json_encode( @$extra['stripe_secret_key']  )  !!};
    

</script>

<script type="text/javascript" src="{{ asset('js/transfer_ticket.js') }}"></script>
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>

@endsection
