@extends('eventmie::layouts.app')

{{-- @section('title', $event->title)
@section('meta_title', $event->meta_title)
@section('meta_keywords', $event->meta_keywords)
@section('meta_description', $event->meta_description)
@section('meta_image', '/storage/'.$event['thumbnail'])
@section('meta_url', url()->current()) --}}

    
@section('content')




<!--ABOUT-->
<section>
    <div id="lgx-about" class="lgx-about">
        <div class="mt-30 mb-50 mt-mobile-0">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-8 m-auto">
                        <div class="">
                            <div class="lgx-banner-info-circle lgx-info-circle">
                                <div class="lgx-registration-form">
                                    <form method="POST" action="{{ route('ticketTransferRequest') }}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12 my-3">
                                            <h3 class="title">Method</h3>
                                            </div>
                                           
                                            <div class="col-12 col-sm-6 col-lg-4">
                                                <div class="lgx-event p-5 font-weight-bold">
                                                    <input type="radio" name="type" value="free" checked> Free
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-lg-4">
                                                <div class="lgx-event p-5 font-weight-bold">
                                                    <input type="radio" name="type" value="paid"> Paid
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <h3 class="title py-0 px-5">
                                                      Users
                                                    </h3>
                                                    <div class="card-body">
                                                       
                                                        @livewire('search', ['users' => $users,'walletUsers' => $walletUsers])
                                                        <input type="hidden" name="booking_id" value="{{ implode(',',$bookingIds) }}">
                                                        <input type="hidden" name="order_id" value="{{ request()->route('orderId') }}">
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-right my-4">
                                            <input type="submit" name="submit" class="lgx-scroll lgx-btn lgx-btn-sm">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="lgx-about-content-area">
                            <div class="lgx-heading">
                                <h2 class="heading"></h2>
                                
                            </div>
                           
                        </div>
                    </div>

                </div>
                <br><br>
               
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
<!--ABOUT END-->
{{-- CUSTOM --}}
{{-- Seating Chart Image --}}



<!--GOOGLE MAP END-->

@endsection

