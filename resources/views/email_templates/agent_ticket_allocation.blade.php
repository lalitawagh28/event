<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width" />
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="�x-apple-disable-message-reformatting�" />
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
      <style>
        * {
          font-family: sans-serif !important;
        }
      </style>
    <![endif]-->
    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
    <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->
    <!-- Web Font / @font-face : END -->
    <!-- CSS Reset -->
    <style>
      /* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
      html,
      body {
        margin: 0 auto !important;
        padding: 0 !important;
        height: 100% !important;
        width: 100% !important;
      }

      /* What it does: Stops email clients resizing small text. */
      * {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }

      /* What it does: Centers email on Android 4.4 */
      div[style*="margin: 16px 0"] {
        margin: 0 !important;
      }

      /* What it does: Stops Outlook from adding extra spacing to tables. */
      table,
      td {
        mso-table-lspace: 0pt !important;
        mso-table-rspace: 0pt !important;
      }

      /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
      table {
        border-spacing: 0 !important;
        border-collapse: collapse !important;
        table-layout: fixed !important;
        margin: 0 auto !important;
      }
      table table table {
        table-layout: auto;
      }

      /* What it does: Uses a better rendering method when resizing images in IE. */
      img {
        -ms-interpolation-mode: bicubic;
      }

      /* What it does: A work-around for iOS meddling in triggered links. */
      .mobile-link--footer a,
      a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: underline !important;
      }
    </style>
    <!-- Progressive Enhancements -->
    <style>
      /* What it does: Hover styles for buttons */
      .button-td,
      .button-a {
        transition: all 100ms ease-in;
      }
      .button-td:hover,
      .button-a:hover {
        background: #6e9a39 !important;
        border-color: #6e9a39 !important;
      }
      a {
        color: #666666;
      }
      /* Media Queries */
      @media screen and (max-width: 480px) {
        /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
        .fluid,
        .fluid-centered {
          width: 100% !important;
          max-width: 100% !important;
          height: auto !important;
          margin-left: auto !important;
          margin-right: auto !important;
        }
        /* And center justify these ones. */
        .fluid-centered {
          margin-left: auto !important;
          margin-right: auto !important;
        }

        /* What it does: Forces table cells into full-width rows. */
        .stack-column,
        .stack-column-center {
          display: block !important;
          width: 100% !important;
          max-width: 100% !important;
          direction: ltr !important;
        }
        /* And center justify these ones. */
        .stack-column-center {
          text-align: center !important;
        }

        /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
        .center-on-narrow {
          text-align: center !important;
          display: block !important;
          margin-left: auto !important;
          margin-right: auto !important;
          float: none !important;
        }
        table.center-on-narrow {
          display: inline-block !important;
        }
      }
    </style>
  </head>
  <body width="100%" bgcolor="#ffffff" style="margin: 0">
    <center style="width: 100%; background: #ffffff">
      <!-- Visually Hidden Preheader Text : BEGIN -->
      <div
        style="
          display: none;
          font-size: 1px;
          line-height: 1px;
          max-height: 0px;
          max-width: 0px;
          opacity: 0;
          overflow: hidden;
          mso-hide: all;
          font-family: sans-serif;
        "
      >
        (Optional) This text will appear in the inbox preview, but not the email
        body.
      </div>
      <!-- Visually Hidden Preheader Text : END -->
      <!--    
            Set the email width. Defined in two places:
            1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
            2. MSO tags for Desktop Windows Outlook enforce a 680px width.
            Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.  
        -->
      <div style="max-width: 70%; margin: auto; padding: 0 30px">
        <!--[if mso]>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
            <td>
            <![endif]-->
        <!-- Email Body : BEGIN -->
        <br/>
        <br/>
        <table
          role="presentation"
          cellspacing="0"
          cellpadding="0"
          border="0"
          align="center"
          width="100%"
          style="max-width: 6000px"
        >
          <!-- Hero Image, Flush : BEGIN -->

          <!-- Hero Image, Flush : END -->
          <!-- 1 Column Text + Button : BEGIN -->
          <tr>
            <td
              colspan="2"
              style="
                padding: 10px 0px 0;
                text-align: left;
                font-family: sans-serif;
                font-size: 14px;
                mso-height-rule: exactly;
                line-height: 20px;
                color: #222222;
              "
            >
            @php
              if($user->organization_logo) {
                $logo = asset('storage/'.$user->organization_logo);
              } else {
                $logo = asset('images/logo.png');
              }

            @endphp
            {{-- <img src="{{ $logo  }}" width="150" height="" alt="alt_text" border="0" align="center" class="fluid" style="
            width: 100%;
            max-width: 150px;
            background: #fff;
            font-family: sans-serif;
            font-size: 15px;
            mso-height-rule: exactly;
            line-height: 20px;
            color: #555555;
            margin-bottom: 10px;
          "> --}}
              <p style="font-size: 15px">
                <span><strong>Dear  {{  $user->name }},</strong></span
                ><br />
                We hope this message finds you well. We are pleased to inform you that tickets for the Following events have been successfully allocated to you: 
              </p>
            </td>
          </tr>

          <tr>
            <td colspan="2" style="font-family: sans-serif">
              
              <table
                class=""
                cellspacing="0"
                cellpadding="0"
                border="0"
                align="left"
                width="100%"
                style="max-width: 6000px"
              >
                <thead>
                  <tr style="border-bottom: 2px solid #333">
                    <th class="text-left" style="text-align: left; padding: 0 0 15px">Event Name</th>
                    <th class="text-left" style="text-align: left; padding: 0 0 15px">Date</th>
                    <th class="text-left" style="text-align: left; padding: 0 0 15px">Tickets Allocated</th>
                    <th class="text-left" style="text-align: left; padding: 0 0 15px">Tickets Category</th>
                    {{-- <th class="text-left"tyle="text-align: left; padding: 0 0 15px">Venue</th> --}}
                      
                  </tr>
                </thead>
                <tbody>
                  @forEach($agent_tickets as $agent_ticket)
                  <tr style="border-bottom: 2px solid #a5a5a5">
                    <td class="" style="text-align: left; padding: 15px 0">
                      {{  $event->title }}
                    </td>
                    <td class="" style="text-align: left; padding: 15px 0">
                        {{  $event->start_date }}
                    </td>
                    <td class="" style="text-align: left; padding: 15px 0">
                      {{  $agent_ticket->quantity }}
                    </td>
                    <td class="" style="text-align: left; padding: 15px 0">
                      {{  $agent_ticket->ticket->title }}  {{ $agent_ticket->sub_category?->title ? "(". $agent_ticket->sub_category?->title .")" : ""}}
                    </td>
                    {{-- <td class="" style="text-align: left; padding: 15px 0">
                        {{ $event->venues?->first()?->title  }}, {{ $event->venues?->first()?->country->country_name  }}, {{ $event->venues?->first()?->zipcode  }}
                    </td> --}}
                  </tr>
                  @endforEach
                </tbody>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="2">
              <br/>
              <br/>
              <h2 style="color: #222222;font-size: 18px;font-weight: normal;margin-top: 0;padding-bottom: 0px;font-weight:bold;">Important Notes: </h2>
              <ul>
                  <li> <p  style="font-family: sans-serif;font-size: 16px;line-height: 1.2;">Please ensure that you have all necessary Event-related information.</p></li>
                  <li> <p  style="font-family: sans-serif;font-size: 16px;line-height: 1.2;">Tickets are non-transferable and non -refundable. </p></li>
              </ul>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <p  style="font-family: sans-serif;font-size: 16px;line-height: 1.5;">
                For further help or clarification, feel free to contact our support team<a href="mailto:support@dhigna.com">[support@dhigna.com]</a>
              </p>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <p  style="font-family: sans-serif;font-size: 16px;line-height: 1.5;">
                Thank you for your cooperation, and we wish you a successful Event! 
              </p>
            </td>
          </tr>
        </table>

        <table
          style="padding: 50px 0; display: inline-block"
          cellspacing="0"
          cellpadding="0"
          border="0"
          align="center"
          width="100%"
        >
          <tr>
            <td
              style="
                width: 40%;
                text-align: left;
                font-family: sans-serif;
                font-size: 16px;
                mso-height-rule: exactly;
                line-height: 20px;
                color: #222222;
              "
            >
              <h1
                style="
                  color: #222222;
                  font-size: 16px;
                  font-weight: normal;
                  margin-top: 0;
                  padding-bottom: 0px;
                "
              >
                Best Regards,
              </h1>
              <img
                    src="{{ $logo }}"
                    width="150"
                    height=""
                    alt="alt_text"
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      width: 100%;
                      max-width: 150px;
                      background: #fff;
                      font-family: sans-serif;
                      font-size: 15px;
                      mso-height-rule: exactly;
                      line-height: 20px;
                      color: #555555;
                      margin-bottom: 10px;
                    "
                  />
                  <br />
              <div>
                <p style="margin-top: 0;margin-bottom: 10px;line-height: 1.5;font-size: 14px;">
                  {{ $event->user->name }} <br />
                  {{ $event->user->email }} <br />
                  {{ $event->user->phone }}
                </p>
              </div>
            </td>
            <td
              style="
                width: 33%;
                text-align: left;
                font-family: sans-serif;
                font-size: 16px;
                mso-height-rule: exactly;
                line-height: 20px;
                color: #222222;
              "
            >
              <h1
                style="
                  color: #222222;
                  font-size: 20px;
                  font-weight: 600;
                  margin-top: 0;
                  padding-bottom: 20px;
                "
              ></h1>
              <div></div>
            </td>
            <td
              style="
                width: 33.33%;
                text-align: left;
                font-family: sans-serif;
                font-size: 16px;
                mso-height-rule: exactly;
                line-height: 20px;
                color: #222222;
                padding-left: 60px;
              "
            >
             
            </td>
          </tr>
        </table>
      </div>
    </center>
  </body>
</html>
