<div class="d-flex p-15 align-items-center flex-row justify-content-between border-bottom">             
    <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.event_id')}}
    </h4>
    <span class="text-dark text_span"><a class="text-dark" href="javascript:void(0);">{{ $event->id }} </a></span>
</div>
<div class="border-bottom">  
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.event_name') }}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.ticket_categories')}}
        </h4>
    </div>
    @php
        $tickets = $event->tickets()->selectRaw('title, (price * quantity) as lot_price')->get();
    @endphp
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{ $event->title }}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        @foreach($tickets as $ticket)
            <div>Ticket : {{ $ticket->title }}</div>
        @endforeach
        </h4>
    </div>
</div>

<div class="border-bottom">  
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.event_id')}}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.commission')}}
        </h4>
    </div>
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{ $event->id }}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        </h4>
    </div>
</div>

<div class="border-bottom">  
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.ticket_price')}}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.event_location')}}
        </h4>
    </div>
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        @foreach($tickets as $ticket)
            <div>Ticket : {{ $ticket->title }} - {{ $ticket->lot_price }}</div>
        @endforeach
        </h4>
        <h4 class="font-weight-bold text-dark text_header" style="width:300px">
            {!! @$event->venues?->first()?->location() !!}
        </h4>
    </div>
</div>

<div class="border-bottom">  
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.event_date_time')}}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.total_tickets')}}
        </h4>
    </div>
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        Date : {{
            \Carbon\Carbon::parse($event->start_date)->format('dS F, Y');
                    }}
        <br>
        Time : {{
        \Carbon\Carbon::parse($event->start_time)->format('g:ia');
        }}  {{ $event->country?->country_code}}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
            @foreach($tickets as $ticket)
                <div>Ticket : {{ $ticket->title }} - {{ $ticket->quantity }}</div>
            @endforeach
        </h4>
    </div>
</div>

<div class="border-bottom">  
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.total_capacity')}}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.total_lot')}}
        </h4>
    </div>
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{ $event->tickets->sum('customer_limit'); }}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        {{ $event->tickets->sum('quantity'); }}
        </h4>
    </div>
</div>