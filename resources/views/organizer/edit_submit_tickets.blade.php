@extends('layouts.app')

@section('page_title', __('voyager::generic.viewing').' '. __('admin.submit_tickets'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-plus"></i> {{ __('admin.submit_tickets') }}
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">

                <form action="@if(@$agent_ticket->id){{ route('voyager.organizer.ticket_update', $agent_ticket->id) }}@else{{ route('voyager.organizer.ticket_store') }}@endif"
                      method="POST" role="form">
                    <!-- CSRF TOKEN -->
                    {{ csrf_field() }}
                    @if(@$agent_ticket->id)
                        <input type="hidden" id="agent_ticket_id" value="{{ $agent_ticket->id }}" name="id">
                    @endif
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-bordered">
                            {{-- <div class="panel"> --}}
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="agent_id">{{ __('admin.agent_id') }}</label>
                                        <input type="text" class="form-control" id="agent_id" name="agent_id" value="{{ $agent->id}}" placeholder="{{ __('admin.enter_agent_id') }}"
                                            value="{{ old('agent_id', $agent_ticket->agent_id ?? '') }}" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="event_id">{{ __('admin.event_name') }}</label>
                                        <select class="form-control select2" id="event_id" name="event_id">
                                            <option class="disabled" value="">{{ __('admin.select_event') }}</option>
                                            @foreach ($events as $event)
                                            <option value="{{ $event->id }}"
                                            {{ ($event->id ==  old('event_id', $agent_ticket->ticket->event_id ?? '') ? 'selected' : '') }}>{{ $event->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="agent_name">{{ __('admin.agent_name') }}</label>
                                        <input type="text" class="form-control" id="agent_id" name="agent_name" value="{{ $agent->name}}" placeholder="{{ __('admin.enter_agent_name') }}"
                                            value="{{ old('agent_id', $agent_ticket->agent_id ?? '') }}" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="ticket_id">{{ __('admin.ticket_category') }}</label>
                                        <input type="text" readonly class="ticket_name form-control" name="ticket_name"
                                                value="{{$agent_ticket->ticket->title}}">
                                            <input type="hidden" class="ticket_id form-control" data-id="{{ old('ticket_id', $agent_ticket->ticket_id ?? '') }}" id="ticket_id" name="ticket_id" value="{{$agent_ticket->ticket_id}}"
                                            value="">
                                    </div>
                                    @if($agent_ticket->sub_category_id)
                                    <div class="form-group">
                                        <label for="sub_category_id">{{ __('admin.sub_category') }}</label>
                                        <input type="text" readonly class="sub_category_name form-control" name="sub_category_id"
                                                value="{{ $agent_ticket->sub_category->title }}">
                                            <input type="hidden" class="sub_category_id form-control" data-sc-id="{{ old('sub_category_id', $agent_ticket->sub_category_id ?? '') }}"  id="sub_category_id" name="sub_category_id" value="{{$agent_ticket->sub_category_id}}"
                                            value="">
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="coupon_type">{{ __('admin.type') }}</label>
                                        <select class="form-control select2" id="coupon_type" name="coupon_type">
                                            <option value="general" @if($agent_ticket->type == 'general') selected @endif>General</option>
                                            <option value="student" @if($agent_ticket->type == 'student') selected @endif>Student</option>
                                        </select>
                                                            </div>
                                    <div class="form-group">
                                        <label for="commission">{{ __('admin.agent_commission') }} </label>
                                        <input type="text" class="form-control" id="commission"  min="0" max="100" name="commission"  placeholder="{{ __('admin.enter_agent_commission') }}"
                                            value="{{ old('commission', @$agent_ticket->commission ?? '0') }}" >
                                    </div>
                                    @if(!@$agent_ticket)
                                    <div class="form-group">
                                        <label for="price">{{ __('admin.coupon_code') }}</label>
                                        <input type="text" class="form-control" id="coupon_code"  name="coupon_code" placeholder="{{ __('admin.enter_coupon_code') }}"
                                            value="{{ old('coupon_code', @$agent_ticket->code) }}" readonly>  
                                        <span style="margin-top:5px;display:inline-block;"  class="text-primary">{{__('admin.auto_generate_message')}} </span> <span class="text-primary" id="edit_coupon"> <i class="voyager-edit"></i> <i style="display:none" class="voyager-brush"></i> </span>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="price">{{ __('admin.price') }}</label>
                                        <input type="text" class="form-control" id="price" name="price" placeholder="{{ __('admin.enter_price') }}"
                                            value="{{ old('price', $agent_ticket->price ?? '0.00') }}" readonly>
                                        <span style="margin-top:5px;display:block;" id="original_price" class="text-primary"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="quantity">{{ __('admin.allocate_tickets') }}</label>
                                        <input type="text" class="form-control" id="quantity" name="quantity"  placeholder="{{ __('admin.enter_no_tickets') }}"
                                            value="{{ old('quantity', $agent_ticket->quantity ?? '') }}" >
                                        <span style="margin-top:5px;display:block;" id="available_tickets" class="text-primary"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="price">{{ __('admin.voucher_code') }}</label>
                                        <input type="text" class="form-control" id="coupon_code" name="coupon_code" placeholder="{{ __('admin.enter_coupon_code') }}"
                                            value="{{ old('coupon_code', $agent_ticket->coupon_code  ? $agent_ticket->coupon_code : $agent_ticket->code) }}" readonly>  
                                        <span style="margin-top:5px;display:inline-block;"  class="text-primary">{{__('admin.auto_generate_message')}} </span> <span class="text-primary" id="edit_coupon"> <i class="voyager-edit"></i> <i style="display:none" class="voyager-brush"></i> </span>
                                    </div>
                                    <div class="form-group">
                                        <label for="price">{{ __('admin.valid_from') }}</label>
                                        <input type="date" class="form-control" id="valid_from" name="valid_from" placeholder="{{ __('admin.enter_valid_from') }}"
                                            value="{{ old('valid_from', $agent_ticket->valid_from ) }}" onchange="changeFromDate(this)">  
                                            <span style="margin-top:5px;display:block;" id="error_valid_from" class="text-primary"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="price">{{ __('admin.valid_to') }}</label>
                                        <input type="date" class="form-control" id="valid_to" name="valid_to" placeholder="{{ __('admin.enter_valid_to') }}"
                                            value="{{ old('valid_from', $agent_ticket->valid_to) }}">  
                                            <span style="margin-top:5px;display:block;" id="error_valid_to" class="text-primary"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="quantity">{{ __('admin.user_discount') }}</label>
                                        <input type="text" class="form-control" id="user_commission" name="user_commission"  placeholder="{{ __('admin.enter_user_discount') }}"
                                            value="{{ old('quantity', $agent_ticket->discount ?? '') }}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="quantity">{{ __('admin.max_uses') }}</label>
                                        <input type="text" class="form-control" id="max_uses" name="max_uses"  placeholder="{{ __('admin.enter_max_uses') }}"
                                            value="{{ old('max_uses', $agent_ticket->max_uses ?? '') }}" >
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right save">
                                        {{ __('voyager::generic.save') }}
                                    </button>
                                </div>
                                
                    
                            </div>
                        </div>
                    </div>


                </form>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .page-content -->



@stop

@section('javascript')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

    <script>
         function changeFromDate(obj){
                    $('#valid_to').attr('min',obj.value);
            }
            $(document).ready(function() {
                var today = new Date().toISOString().split('T')[0];
                $("#valid_from").attr('min', today);
                $("#valid_to").attr('min', today);
            });
            $(document).ready(function () {
                $('#event_id').trigger("change");
            });
            $('#edit_coupon').on('click',function () {
                
                if($('#coupon_code').prop('readonly')) {
                    $('#coupon_code').prop('readonly', false);
                    $('#edit_coupon .voyager-edit').hide();
                    $('#edit_coupon .voyager-brush').show();
                } else {
                    $('#coupon_code').val('');
                    $('#coupon_code').prop('readonly', true);
                    $('#edit_coupon .voyager-edit').show();
                    $('#edit_coupon .voyager-brush').hide();
                }
                
            });
            $('#quantity').on('keyup',function() {
                if($(this).val() && $(this).val() != '-') {
                    var value = Number($(this).val())
                    $(this).val(value);

                    tickets = Number($("#ticket_id").data("available")) - Number($(this).val());
                    price = Number($("#ticket_id").data("price"));
                    old_price = Number($("#ticket_id").data("old-price"));
                    if(tickets < 0) {
                        $(this).val(0);
                        tickets = Number($("#ticket_id").data("available"))
                    }
                    $('#available_tickets').html('Available Ticket: ' + tickets)
                    if(old_price){
                        $('#original_price').html('Sales Price: ' + price)
                    } else {
                        $('#original_price').html('Original Price: ' + price)
                    }
                } else {
                    tickets = Number($("#ticket_id").data("available"))
                    $('#available_tickets').html('Available Ticket: ' + tickets)
                    price = Number($("#ticket_id").data("price"));
                    if(old_price){
                        $('#original_price').html('Sales Price: ' + price)
                    } else {
                        $('#original_price').html('Original Price: ' + price)
                    }
                }
            });
            $('#commission').on('keyup',function() {
                let value = String(parseFloat($(this).val()));
                var commission = Number($(this).val());
                var price = $("#ticket_id").data("price")
                if(commission > price ){
                    $('#price').val('0.0');
                    $(this).val(price);
                } else {
                    if(commission &&  price){
                        price = Number( price);
                        data= price - commission;
                        $('#price').val(data)
                    }
                }
            });
            $('#ticket_id').on('change',function() {
                $('#commission').trigger("keyup");
                 tickets = Number($("#ticket_id").data("available"))- Number($('#quantity').val());
                 price = Number($("#ticket_id").data("price"));
                 old_price = Number($("#ticket_id").data("old-price"));
                $('#available_tickets').html('Available Ticket: ' + tickets)
                if(old_price){
                    $('#original_price').html('Sales Price: ' + price)
                } else {
                    $('#original_price').html('Original Price: ' + price)
                }
            });
            
            $('#event_id').on('change',function() {
                var event_id = $(this).val();
                var agent_ticket_id = $('#agent_ticket_id').val();
                if(!agent_ticket_id) {
                    agent_ticket_id = 0;
                }
                if(event_id){
                    $.ajax({
                        url: "/organizer/get-tickets/"+event_id + "/" + agent_ticket_id,
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function(response) {
                            ticket_id = $('#ticket_id').data('id');
                            sub_category_id = $('#sub_category_id').data('sc-id');
                            $(response.tickets).each(function(index,item){
                            
                                if(item.id == ticket_id){
                                    $('#available_tickets').html('Available Ticket: ' + item.available);
                                    if(item.old_price){
                                        $('#original_price').html('Sales Price: ' + item.price)
                                    } else {
                                        $('#original_price').html('Original Price: ' + item.price)
                                    }
                                
                                    if(sub_category_id && item.sub_category_id == sub_category_id){
                                        $('#ticket_id').attr('data-available',item.available).attr('data-price',item.price).attr('data-old-price',item.old_price);
                                    }
                                    if(!sub_category_id && ticket_id && item.id == ticket_id){
                                        $('#ticket_id').attr('data-available',item.available).attr('data-price',item.price).attr('data-old-price',item.old_price);
                                    }
                                } 
                            });
                            if(ticket_id){
                                $("#ticket_id").val(ticket_id);
                                $('#ticket_id').trigger("change");
                            }
                            

                        },
                        error: function(xhr, status, errorThrown) {
                            xhr.status;
                            alert('Failed to get Tickets!');
                        }
                    });
                }
            });
            
        /********** End Relationship Functionality **********/

        function setInputFilter(textbox, inputFilter, errMsg) {
  [ "input", "keydown", "keyup", "mousedown", "mouseup", "focusout" ].forEach(function(event) {
    textbox.addEventListener(event, function(e) {
        const key = e.key; // const {key} = event; ES6+
        if ([ "keyup", "mouseup", "input","focusout" ].indexOf(e.type) >= 0){
            const pattern = /[a-zA-Z0-9\-]{0,25}$/;
            if(!pattern.test(this.value)){
                this.value = this.oldValue;
                return;
            } 
        }
        if ([ "input","focusout" ].indexOf(e.type) >= 0){
            if(this.value.length < 6){
                this.setCustomValidity(errMsg);
                this.reportValidity();
            }
        }
        if (inputFilter(this.value)) {
            // Accepted value.
            if ([ "keydown", "mousedown", "focusout" ].indexOf(e.type) >= 0){
                this.classList.remove("input-error");
                this.setCustomValidity("");
                
            }
        }
        if (key === "Backspace" || key === "Delete") {
            if(this.value.length == 1 && this.oldValue == this.value){
                this.oldValue = '';
                this.value = '';
            }
        }  
        if(this.value.length > 25){
            this.setCustomValidity(errMsg);
            this.reportValidity();
            this.value = this.oldValue;
        }
        this.oldValue = this.value;
    });
  });
}
// setInputFilter(document.getElementById("coupon_code"), function(value) {
//   return /[a-zA-Z0-9&\-]{6,25}$/.test(value); // Allow digits and '.' only, using a RegExp.
// }, "Only 6 to 25 characters  are allowed");





    </script>
@stop

