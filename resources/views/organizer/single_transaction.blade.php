<div class="d-flex p-15 align-items-center flex-row justify-content-between border-bottom">             
    <h4 class="font-weight-bold text-dark text_header">
    {{__('admin.transaction_id')}}
    </h4>
    <span class="text-dark text_span"><a class="text-dark" href="javascript:void(0);">{{ $booking->transaction_id }} </a></span>
</div>
<div class="border-bottom">  
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.event_name')}}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.ticket_categories')}}
        </h4>
    </div>
    @php
        $tickets = $booking->event->tickets()->selectRaw('title, (price * quantity) as lot_price')->get();
    @endphp
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{ $booking->event->title }}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        @foreach($tickets as $ticket)
            <div>Ticket : {{ $ticket->title }}</div>
        @endforeach
        </h4>
    </div>
</div>

<div class="border-bottom">  
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.venue')}}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
        {{__('admin.date_time')}}
        </h4>
    </div>
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
        {!! @$booking->event->venues?->first()?->location() !!}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">
         {{
            \Carbon\Carbon::parse($booking->event->start_date)->format('dS F, Y, g:ia');
                    }}
          {{ $booking->event->country?->country_code}}
        </h4>
    </div>
</div>

<div class="border-bottom">  
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
            {{__('admin.account_transfer_type')}}
        </h4>
        <h4 class="font-weight-bold text-dark text_header">

        </h4>
    </div>
    <div class="d-flex p-15 align-items-center flex-row justify-content-between">             
        <h4 class="font-weight-bold text-dark text_header">
            @php
            $is_paypal = 1;
            if(empty(setting('apps.paypal_secret')) || empty(setting('apps.paypal_client_id')))
                $is_paypal = 0;
            
            if($is_paypal) {
                $payment_method = 'Paypal';
            } else {
                $payment_method = 'Stripe';
            }
            @endphp
            {{ $payment_method }}
        </h4>
        <h4 class="font-weight-bold text-dark text_header" style="width:300px">
        <a href="{{ route('invoice',[$booking->id])}}" class="btn btn-sm btn-success edit pull-right">
                <span class="hidden-xs hidden-sm">{{ __('admin.download') }}</span>
        </a>
        </h4>
    </div>
</div>