{{-- Organizer dashboard --}}
@extends('layouts.app')

@section('page_title')
    @lang('eventmie-pro::em.dashboard')
@endsection
@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 mb-0">
                <h1 class="page-title">
                    <i class="voyager-boat"></i> {{ __('voyager::generic.Dashboard') }}
                </h1>
            </div>
            <div class="col-xs-6 mb-0">
                <div class="dropdown pull-right custom-dashboard-dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        {{ __('voyager::generic.Notifications') }}
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenu1">
                        
                        <li role="separator" class="divider"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
<div class="page-content browse container-fluid custom-dashboard">
    <div class="row statistics">

        <div class="col-md-2">
            <div class="box">
                <i class="voyager-people text-center"></i>
                <div class="info">
                    <h3>{{ @$total_customers }}</h3> <p>{{ __('voyager::generic.Customers') }} </p>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-2">
            <div class="box">
                <i class="voyager-company text-center"></i>
                <div class="info">
                    <h3>{{ @$total_organizers }}</h3> <p>{{ __('voyager::generic.Organisers') }}</p>
                </div>
            </div>
        </div> -->
        <div class="col-md-2">
            <div class="box">
                <i class="voyager-calendar text-center"></i>
                <div class="info">
                    <h3>{{ @$total_events}}</h3> <p>{{ __('voyager::generic.Events') }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="box">
                <i class="voyager-ticket text-center"></i>
                <div class="info">
                    <h3>{{ @$total_bookings}}</h3> <p>{{ __('voyager::generic.Bookings') }}</p>
                </div>
            </div>
        </div>

    </div>
    <div class="panel panel-bordered">
        <div class="panel-body">
                
            <div class="row">
                <div class="col-md-12">
                    <h3>{{ __('voyager::generic.Top 10 Selling Events') }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {!! $eventsChart->container() !!}
                </div>
            </div>
            
        </div>
    </div>
</div>
@stop

@section('javascript')



<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

{!! $eventsChart->script() !!}

@stop