@extends('layouts.app')
@section('page_title', __('voyager::generic.viewing') . ' Sales Report By Voucher')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 mb-0">
                <h1 class="page-title">
                    <i class="voyager-plus"></i> Sales Report By Voucher
                </h1>
            </div>

        </div>
    </div>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-bordered">
                            {{-- <div class="panel"> --}}


                            <div class="panel-body">
                                <form action="{{ route('voyager.organizer.get_sales_report_by_voucher_data') }}" method="POST"
                                    role="form">
                                    <!-- CSRF TOKEN -->
                                    {{ csrf_field() }}
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="agent_id">Event <span class="text-danger">*</span></label>
                                        <select class="form-control select2" id="event" name="event"
                                            onchange="getVoucherCode(this)" required>
                                            <option class="disabled" value="">{{ __('admin.select_event') }}</option>
                                            @foreach ($events as $event)
                                                <option value="{{ $event->id }}"
                                                    @if (old('event',request()->event) == $event->id) selected @endif>{{ $event->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @php
                                        $selectCodes = [];
                                    @endphp
                                    @if(!empty($vouchers))
                                        @foreach ($vouchers as $voucher)
                                            @if($voucher->coupon_code != '')
                                                @php
                                                    array_push($selectCodes,$voucher->coupon_code);
                                                @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <input type="hidden" name="selectAll" id="selectAll" value="{{ implode(',',$selectCodes) }}">

                                    <div class="form-group">
                                        <label for="agent_id">Voucher <span class="text-danger">*</span></label>
                                        <select class="form-control select2" id="voucher" name="voucher[]" multiple required>

                                            @if(!empty($vouchers))
                                            <option value="Select All" @if(in_array('Select All',request()->voucher)) selected @endif>Select ALL</option>
                                            @foreach ($vouchers as $voucher)
                                            @if($voucher->coupon_code != '')
                                            <option value="{{ $voucher->coupon_code }}" @if(in_array($voucher->coupon_code,request()->voucher)) selected @endif>

                                                    {{ $voucher->coupon_code }} (Discount {{ $voucher->discount }} GBP)

                                            </option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="from_date">From Date</label>
                                        <input type="date" class="form-control" id="from_date" name="from_date"
                                            value="{{ old('from_date',request()->from_date) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="to_date">To Date</label>
                                        <input type="date" class="form-control"  name="to_date" value="{{ old('to_date',request()->to_date) }}"
                                           >
                                    </div>

                                    {{-- <div class="form-group">
                                        <label for="to_date">To Date</label>
                                        <input type="date" class="form-control"
                                           >
                                    </div> --}}


                                    <button type="submit" class="btn btn-primary pull-right save">
                                        {{ __('voyager::generic.submit') }}
                                    </button>
                                </form>
                                <div class="col-md-12">


                                @if (!is_null(@$bookings))
                                    <div class="html-content">
                                        <h4>Event Title: @foreach ($events as $event)
                                                @if (old('event',request()->event) == $event->id) {{ $event->title }} </h4> @endif
                                            @endforeach

                                        <table id="dataTable" class="table table-hover ">
                                            <thead>
                                                <th>Ticket Category</th>
                                                <th>Ticket SubCategory</th>
                                                <th>Voucher Code</th>
                                                <th title="Sales Amount = Original Price - Agent Commission - User discount"><span style="color:red;pointer-events: auto;cursor: pointer;">?</span> Sales</th>
                                                <th>No Of Ticket</th>
                                            </thead>
                                            <tbody>
                                                @php
                                                $totalPrices = 0;
                                                $totalTickets = 0;
                                                @endphp
                                                @if(in_array('Select All',request()->input('voucher')))
                                                    @php
                                                        $voucherData = explode(',',request()->input('selectAll'));
                                                    @endphp
                                                @else
                                                    @php
                                                        $voucherData = request()->input('voucher');
                                                    @endphp
                                                @endif
                                                @if (!is_null($from_date) && !is_null($to_date))
                                                    @php
                                                        $from_date = date('Y-m-d', strtotime($from_date));
                                                        $to_date = date('Y-m-d', strtotime('+1 day', strtotime($to_date)));



                                                            $booking = \App\Models\Booking::whereEventId($event_id)
                                                            ->whereBetween('bookings.created_at', [$from_date, $to_date])
                                                            ->whereIn('bookings.promocode', $voucherData)
                                                            ->get();

                                                        $finalArray = array();
                                                        foreach($booking as $bookingValue){
                                                            $bookingDetails = \App\Models\Booking::whereEventId($event_id)->leftJoin('agent_tickets', 'bookings.agent_id', '=', 'agent_tickets.id')->where('bookings.ticket_id',$bookingValue->ticket_id)
                                                            ->whereIn('bookings.promocode', $voucherData)
                                                            ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', 'agent_tickets.commission', 'agent_tickets.discount', DB::raw('SUM(bookings.price) as tot_net_price'), DB::raw('SUM(bookings.quantity) as tot_quantity'), DB::raw('SUM(agent_tickets.commission) as tot_commission'), DB::raw('SUM(agent_tickets.discount) as tot_discount'))
                                                            ->groupBy('bookings.promocode')->get();

                                                            foreach($bookingDetails as $bookingDetailsVlaue)
                                                            {
                                                                if(!in_array($bookingDetailsVlaue,$finalArray))
                                                                {
                                                                    array_push($finalArray,$bookingDetailsVlaue);
                                                                }


                                                            }

                                                        }

                                                    @endphp
                                                @else
                                                    @php

                                                        $booking = \App\Models\Booking::whereEventId($event_id)
                                                            ->whereIn('bookings.promocode', $voucherData)
                                                            ->get();

                                                        $finalArray = array();
                                                        foreach($booking as $bookingValue){
                                                            $bookingDetails = \App\Models\Booking::whereEventId($event_id)->leftJoin('agent_tickets', 'bookings.agent_id', '=', 'agent_tickets.id')->where('bookings.ticket_id',$bookingValue->ticket_id)
                                                            ->whereIn('bookings.promocode', $voucherData)
                                                            ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', 'agent_tickets.commission', 'agent_tickets.discount', DB::raw('SUM(bookings.price) as tot_net_price'), DB::raw('SUM(bookings.quantity) as tot_quantity'), DB::raw('SUM(agent_tickets.commission) as tot_commission'), DB::raw('SUM(agent_tickets.discount) as tot_discount'))
                                                            ->groupBy('bookings.promocode')->get();

                                                            foreach($bookingDetails as $bookingDetailsVlaue)
                                                            {
                                                                if(!in_array($bookingDetailsVlaue,$finalArray))
                                                                {
                                                                    array_push($finalArray,$bookingDetailsVlaue);
                                                                }


                                                            }

                                                        }

                                                    @endphp
                                                @endif

                                                    @foreach ($finalArray as  $bookingVal)
                                                        @php
                                                           $ticket = \Classiebit\Eventmie\Models\Ticket::whereId($bookingVal->ticket_id)->first();
                                                           $agentTicket = \App\Models\AgentTicket::whereCouponCode($bookingVal->promocode)->first();
                                                           $price = $bookingVal->tot_net_price - $bookingVal?->tot_commission - $bookingVal?->tot_discount;
                                                           $ticketSubcategory = \App\Models\TicketSubCategory::whereId($bookingVal->sub_category_id)->first();
                                                           $totalPrices += $price;
                                                           $totalTickets += $bookingVal->tot_quantity;

                                                        @endphp
                                                        @if(!is_null($agentTicket) && $bookingVal->tot_quantity > 0)
                                                        <tr>
                                                            <td>{{ $ticket?->title }}</td>
                                                            <td>@if(!is_null($ticketSubcategory?->title)){{ $ticketSubcategory?->title }} @else  - @endif</td>
                                                            <td>{{ $bookingVal->promocode }}</td>
                                                            <td>£{{ number_format($price, 2) }}</td>
                                                            <td>{{ $bookingVal->tot_quantity }}</td>
                                                        </tr>
                                                        @endif
                                                    @endforeach

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3"></td>

                                                    <td><strong>Total: £{{ number_format($totalPrices, 2) }}</strong></td>
                                                    <td><strong>Total Tickets: {{ $totalTickets }}</strong></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <form method="POST" action="{{ route('events.exportVoucherSale') }}">
                                        @csrf
                                        <input type="hidden" name="from_date" value="{{ $from_date }}">
                                        <input type="hidden" name="selectAllExcel" id="selectAllExcel" value="{{ implode(',',$selectCodes) }}">
                                        <input type="hidden" name="to_date" value="{{ $to_date }}">
                                        <input type="hidden" name="voucherData" value=" {{ implode(',',request()->voucher)}}">
                                        <input type="hidden" name="event_id" value="{{ $event_id }}">
                                        <button type="submit" class="btn btn-primary pull-right" >Export Excel</button>
                                    </form>


                                    {{-- <button id="cmd" class="btn btn-primary pull-right" onclick="CreatePDFfromHTML()">Export</button> --}}
                                @endif
                                </div>
                            </div>


                        </div>
                    </div>
                </div>




            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .page-content -->



@stop
@section('javascript')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script>


        function getVoucherCode(the) {
            var eventId = $(the).val();

            $.ajax({
                url: '{{ route('voyager.organizer.get_voucher_code_by_event') }}',
                type: "POST",
                data: {
                    'eventId': eventId
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(result) {
                    var vouchers = result.vouchers;
                    var SelectAll = [];
                    var html = '<option  value="Select All">Select All</option>';
                    for (let x in vouchers) {
                        console.log(vouchers[x]['code']);
                        if(vouchers[x]['coupon_code'] != '')
                        {
                            var voucherCode = vouchers[x]['coupon_code'];

                            html += '<option  value="' + vouchers[x]['coupon_code'] + '">' + voucherCode +' (Discount '+ vouchers[x]['discount'] +' GBP)' +
                            '</option>';

                            SelectAll.push(vouchers[x]['coupon_code']);
                        }


                    }
                    $('#selectAll').val(SelectAll);
                    $('#selectAllExcel').val(SelectAll);

                    $("#voucher").html(html);
                }
            });
        }
        $('#to_date').on('change',function(){
            if($('#from_date').val() == ''){
                alert("From Date is required");
                $('#to_date').val('');
            } else {
                if($('#from_date').val() >  $('#to_date').val()){
                    alert("To Date is greater than From Date");
                    $('#to_date').val('');
                }
            }

        });
        function CreatePDFfromHTML() {
            var HTML_Width = $(".html-content").width();
            var HTML_Height = $(".html-content").height();
            var top_left_margin = 15;
            var PDF_Width = HTML_Width + (top_left_margin * 2);
            var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
            var canvas_image_width = HTML_Width;
            var canvas_image_height = HTML_Height;

            var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

            html2canvas($(".html-content")[0]).then(function (canvas) {
                var imgData = canvas.toDataURL("image/jpeg", 1.0);
                var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
                pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
                for (var i = 1; i <= totalPDFPages; i++) {
                    pdf.addPage(PDF_Width, PDF_Height);
                    //pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
                    pdf.addImage(imgData, 'PNG', 10, 10, (canvas_image_width*.62), (canvas_image_height*.62),undefined,'FAST');
                }
                pdf.save("SalesVoucherCodeReport.pdf");
                // $(".html-content").hide();
            });
        }
    </script>
@stop
