@extends('layouts.app')
@section('page_title', __('voyager::generic.viewing').' Sales Report By Event')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 mb-0">
                <h1 class="page-title">
                    <i class="voyager-plus"></i> Sales Report By Event
                </h1>
            </div>

        </div>
    </div>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">




                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-bordered">
                            {{-- <div class="panel"> --}}


                            <div class="panel-body">
                                <form action="{{ route('voyager.organizer.get_sales_report_by_event_data') }}" method="POST"
                                    role="form">
                                    <!-- CSRF TOKEN -->
                                    {{ csrf_field() }}
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="agent_id">Event <span class="text-danger">*</span></label>
                                        <select class="form-control select2" multiple id="events" name="events[]" required>
                                            <option  value="all">{{ __('admin.select_event_all') }}</option>
                                            @foreach ($events as $event)
                                                <option value="{{ $event->id }}" @if(old('event',request()->event) == $event->id ) selected @endif>{{ $event->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="event_id">From Date</label>
                                        <input type="date" class="form-control" id="from_date" name="from_date" value="{{ old('from_date',request()->from_date)}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="agent_name">To Date</label>
                                        <input type="date" class="form-control" id="to_date" name="to_date" value="{{ old('to_date',request()->to_date)}}">
                                    </div>


                                    <button type="submit" class="btn btn-primary pull-right save">
                                        {{ __('voyager::generic.submit') }}
                                    </button>
                                </form>

                                @if (!is_null(@$bookings))
                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                        <th>Event Name</th>
                                        <th>Ticket Category</th>
                                        <th>Ticket Sub Category</th>
                                        <th title="Sales Amount = Original Price - Agent Commission - User discount"><span style="color:red;pointer-events: auto;cursor: pointer;">?</span> Sales</th>
                                        <th>No Of Ticket</th>
                                    </thead>
                                    <tbody>
                                        @php
                                        $totalPrices = 0;
                                        $totalTickets = 0;
                                        @endphp
                                        @foreach ($selectedEvent as $ticket)
                                        @if($ticket->sub_category)
                                        @forEach($ticket->sub_categories as $subCat)
                                            @php
                                                $totalTicket = 0;
                                                $totalPrice = 0;
                                                $price=0;
                                            @endphp

                                            @if(!is_null(request()->from_date) && !is_null(request()->to_date))
                                                @php

                                                $booking  = \App\Models\Booking::where('bookings.sub_category_id',$subCat->id)->whereIn('event_id',$eventsId)
                                                    ->select('bookings.*',
                                                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                                                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),

                                                    )->whereBetween('bookings.created_at', [date('Y-m-d', strtotime(request()->from_date)), date('Y-m-d', strtotime('+1 day', strtotime(request()->to_date)))])->first();

                                                $amount = \App\Models\Booking::where('bookings.sub_category_id',$subCat->id)->whereIn('event_id',$eventsId)
                                                ->whereBetween('bookings.created_at', [date('Y-m-d', strtotime(request()->from_date)), date('Y-m-d', strtotime('+1 day', strtotime(request()->to_date)))])
                                                ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')->select('bookings.*',
                                                        DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                                        DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                                                    )->first();


                                                $price = $booking->tot_net_price - $amount?->tot_commission - $amount?->tot_discount;

                                                @endphp
                                            @else
                                                @php

                                                $booking  = \App\Models\Booking::where('bookings.sub_category_id',$subCat->id)->whereIn('event_id',$eventsId)
                                                    ->select('bookings.*',
                                                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                                                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),

                                                    )->first();
                                                $amount = \App\Models\Booking::where('bookings.sub_category_id',$subCat->id)->whereIn('event_id',$eventsId)
                                                ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')->select('bookings.*',
                                                        DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                                        DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                                                    )->first();

                                                $price = $booking->tot_net_price - $amount?->tot_commission - $amount?->tot_discount;
                                                $totalPrices += $price;
                                                $totalTickets += $booking?->tot_quantity;
                                                @endphp
                                            @endif
                                            @if($booking?->tot_quantity > 0)

                                            <tr>
                                                <td>{{ $ticket->event->title }}</td>
                                                <td>{{ $ticket->title }}</td>
                                                <td>{{ $subCat->title }}</td>
                                                <td>£{{ number_format($price, 2) }}</td>
                                                <td>{{ $booking?->tot_quantity }}</td>
                                            </tr>
                                            @endif
                                        @endforeach
                                        @else
                                            @php
                                                $totalTicket = 0;
                                                $totalPrice = 0;
                                            @endphp

                                            @if(!is_null(request()->from_date) && !is_null(request()->to_date))
                                                @php

                                                $booking  = \App\Models\Booking::where('bookings.ticket_id',$ticket->id)->whereIn('event_id',$eventsId)
                                                    ->select('bookings.*',
                                                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                                                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),

                                                    )->whereBetween('bookings.created_at', [date('Y-m-d', strtotime(request()->from_date)), date('Y-m-d', strtotime('+1 day', strtotime(request()->to_date)))])->first();

                                                $amount = \App\Models\Booking::where('bookings.ticket_id',$ticket->id)->whereIn('event_id',$eventsId)
                                                ->whereBetween('bookings.created_at', [date('Y-m-d', strtotime(request()->from_date)), date('Y-m-d', strtotime('+1 day', strtotime(request()->to_date)))])
                                                ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')->select('bookings.*',
                                                        DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                                        DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                                                    )->first();


                                                $price = $booking->tot_net_price - $amount?->tot_commission - $amount?->tot_discount;

                                                @endphp
                                            @else
                                                @php

                                                $booking  = \App\Models\Booking::where('bookings.ticket_id',$ticket->id)->whereIn('event_id',$eventsId)
                                                    ->select('bookings.*',
                                                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                                                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),

                                                    )->first();
                                                $amount = \App\Models\Booking::where('bookings.ticket_id',$ticket->id)->whereIn('event_id',$eventsId)
                                                ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')->select('bookings.*',
                                                        DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                                        DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                                                    )->first();

                                                $price = $booking->tot_net_price - $amount?->tot_commission - $amount?->tot_discount;
                                                $totalPrices += $price;
                                                $totalTickets += $booking?->tot_quantity;
                                                @endphp
                                            @endif

                                            @if($booking?->tot_quantity > 0)
                                            <tr>
                                                <td>{{ $ticket->event->title }}</td>
                                                <td>{{ $ticket->title }}</td>
                                                <td> NA </td>
                                                <td>£{{ number_format($price, 2) }}</td>
                                                <td>{{ $booking?->tot_quantity }}</td>
                                            </tr>
                                            @endif
                                        @endif
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"></td>

                                            <td><strong>Total: £{{ number_format($totalPrices, 2) }}</strong></td>
                                            <td><strong>Total Tickets: {{ $totalTickets }}</strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <form method="POST" action="{{ route('events.exportEventSale') }}">
                                    @csrf
                                    <input type="hidden" name="from_date" value="{{ request()->from_date }}">
                                    <input type="hidden" name="to_date" value="{{ request()->to_date }}">
                                    <input type="hidden" name="event_ids" value="{{  implode(',',$eventsId) }}">
                                    <button type="submit" class="btn btn-primary pull-right" >Export Excel</button>
                                </form>
                            @endif
                            </div>


                        </div>
                    </div>
                </div>




            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .page-content -->



@stop

@section('javascript')
<script>
    $(document).ready(function(){
        @if(@$eventIds)
            $('#events').val({!! json_encode($eventIds) !!}).trigger('change');
        @endif
    });
    $('#to_date').on('change',function(){
        if($('#from_date').val() == ''){
            alert("From Date is required");
            $('#to_date').val('');
        } else {
            if($('#from_date').val() >  $('#to_date').val()){
                alert("To Date is greater than From Date");
                $('#to_date').val('');
            }
        }

    })
</script>
@endsection
