@extends('layouts.app')

@section('page_title', __('voyager::generic.viewing') . ' ' . __('admin.allocate_tickets'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-plus"></i> {{ __('admin.allocate_tickets') }}
        </h1>
    </div>
@stop

@section('head')
    <style>
        #ticket_add .select2 {
            display: inline;
        }


        span.select2-dropdown {
            display: table;
        }
    </style>
@stop

@section('content')
    <div class="page-content container-fluid" id="voyagerBreadEditAdd">
        <div class="row">
            <div class="col-md-12">

                <form action="{{ route('voyager.organizer.bulk_ticket_update') }}" method="POST" role="form">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-bordered">
                                {{-- <div class="panel"> --}}
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="panel-body">
                                    <button type="button" class="btn btn-success btn-add-new" id="add_row"> <i
                                            class="voyager-plus"></i></button>
                                    <button type="button" class="btn btn-success btn-add-new" id="delete_row"> <span
                                            class="glyphicon glyphicon-minus-sign"></span></button>
                                    <div style="overflow-x:auto;">
                                        <table id="ticket_add" style="width:190%">
                                            <thead>
                                                <tr>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.event') }} <span class="text-danger">*</span></th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.ticket_category') }}  <span class="text-danger">*</span>
                                                    </th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.type') }}  <span class="text-danger">*</span></th>
                                                    {{-- <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        Code Value</th> --}}
                                                    {{-- <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.overall_commission') }}</th> --}}
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.agents') }}  <span class="text-danger">*</span></th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.agent_commission') }} </th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.user_discount') }}</th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.voucher_code') }}  <span class="text-danger">*</span></th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.no_of_tickets') }}  <span class="text-danger">*</span></th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.no_of_uses') }}</th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.valid_from') }}</th>
                                                    <th class="white-space"
                                                        style="padding:10px; white-space: nowrap;font-weight:bold;">
                                                        {{ __('admin.valid_to') }}</th>

                                                </tr>
                                            </thead>
                                            @if (old('event_id',[]))
                                                @foreach (old('event_id', []) as $key => $evt)
                                                    <tr @if ($loop->first) id="row_add" @endif>
                                                        <td style="padding:10px;vertical-align: top;width:300px;">
                                                            <select class="form-control select2 event_id" style="width:250px;"
                                                                name="event_id[]">
                                                                <option class="disabled" value="">
                                                                    {{ __('admin.select_event') }}</option>
                                                                @foreach ($events as $event)
                                                                    @if($evt == $event->id)
                                                                    <option value="{{ $event->id }}" selected>
                                                                        {{ $event->title }}
                                                                    </option>
                                                                    @else
                                                                    <option value="{{ $event->id }}">
                                                                        {{ $event->title }}
                                                                    </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            <span style="margin-top:5px;display:block;"
                                                                class="event_error text-danger"></span>
                                                        </td>
                                                        <td style="padding:10px;vertical-align: top;width:300px;">
                                                            @php
                                                            $ticket_id = old('ticket_id',[]);
                                                            $selected_event = App\Models\Event::find($evt);
                                                            @endphp
                                                            <select class="form-control select2 ticket_id" style="width:250px;"
                                                                multiple="multiple" name="ticket_id[{{ $key }}][]">
                                                                <option value="all"  @if(in_array('all',$ticket_id[$key])) selected @endif>Select All</option>
                                                                @foreach($selected_event->tickets as $ticket)
                                                                    @if($ticket->sub_category)
                                                                        @foreach($ticket->sub_categories as $sub_category)
                                                                            <option value="{{ $ticket->id.','.$sub_category->id.'('.$sub_category->price.')' }}" @if(in_array($ticket->id.','.$sub_category->id.'('.$sub_category->price.')',@$ticket_id[$key])) selected @endif>{{ $ticket->title.' '.$sub_category->title.'('.$sub_category->price.')'}} ({{ $sub_category->avaliableTickets() }})</option>
                                                                        @endforeach
                                                                    @else
                                                                        <option value="{{ $ticket->id }}" @if(in_array($ticket->id ,@$ticket_id[$key])) selected @endif>{{ $ticket->title.'('.$ticket->price.')'}} ({{ $ticket->avaliableTickets() }})</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            <span style="margin-top:5px;display:block;"
                                                                class="ticket_id_error text-danger"></span>
                                                        </td>
                                                        <td style="padding:10px;vertical-align: top;width:300px;">
                                                            <select class="form-control select2 type" multiple="multiple"
                                                                style="width:250px;" multiple="multiple" name="type[{{$key}}][]">
                                                                @php
                                                                    $type = old('type',[]);
                                                                @endphp
                                                                @if(old('type',[]))
                                                                <option value="general" @if(in_array('general',@$type[$key])) selected @endif>General</option>
                                                                <option value="student" @if(in_array('student',@$type[$key]))  selected @endif>Student</option>
                                                                @else
                                                                    <option value="general" selected>General</option>
                                                                    <option value="student">Student</option>
                                                                @endif
                                                            </select>
                                                            <span style="margin-top:5px;display:block;"
                                                                class="type_error text-danger"></span>
                                                        </td>
                                                        {{-- <td style="padding:10px;vertical-align: top;width:300px;">
                                                            <select class="form-control select2 code_value" style="width:250px;"
                                                                name="code_value[]">
                                                                <option value="fixed" selected>Fixed</option>
                                                                <option value="percent">Percent</option>
                                                            </select>
                                                            <span style="margin-top:5px;display:block;"
                                                                class="code_value_error text-danger"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            <input type="text" class="overall_commission form-control"
                                                                value="0.00" name="overall_commission[]"
                                                                placeholder="{{ __('admin.enter_overall_commission') }}"
                                                                value="" onkeypress="return isNumberKey(this, event);">
                                                            <span style="margin-top:5px;display:block;"
                                                                class="overall_commission_error text-danger"></span>
                                                        </td> --}}
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top;width:350px;">
                                                            @php
                                                            $ags = old('agents',[]);
                                                            @endphp
                                                            <select class="form-control select2 agent" name="agents[]">
                                                                <option class="disabled" value="">
                                                                    {{ __('admin.select_agent') }}</option>
                                                                @foreach ($agents as $agent)
                                                                    @if(@$ags[$key] == $agent->id)
                                                                        <option value="{{ $agent->id }}" selected>
                                                                            {{ $agent->name }} ( {{ $agent->email }})
                                                                        </option>
                                                                    @else
                                                                        <option value="{{ $agent->id }}">
                                                                            {{ $agent->name }} ( {{ $agent->email }})
                                                                        </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            <span style="margin-top:5px;display:block;"
                                                                class="agent_error  text-danger"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; width:300px;white-space: nowrap;">
                                                            @php
                                                            $agent_commission = old('agent_commission',[]);
                                                            @endphp
                                                            <input type="text" class="agent_commission form-control"
                                                                style="width:250px;"  name="agent_commission[]"
                                                                placeholder="{{ __('admin.enter_agent_commission') }}"
                                                                @if(@$agent_commission[$key]) value="{{ $agent_commission[$key] }}" @else value="" @endif onkeypress="return isNumberKey(this, event,true);">
                                                            <span style="margin-top:5px;display:block;"
                                                                class="agent_commission_error text-danger"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            @php
                                                            $user_discount = old('user_discount',[]);
                                                            @endphp
                                                            <input type="text" class="user_discount form-control"
                                                                 name="user_discount[]" 
                                                                placeholder="{{ __('admin.user_discount') }}" 
                                                                @if(@$user_discount[$key]) value="{{ $user_discount[$key] }}" @else value="" @endif   onkeypress="return isNumberKey(this, event,true);">
                                                            <span style="margin-top:5px;display:block;"
                                                                class="user_discount_error text-danger"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;width:200px;">
                                                            @php
                                                             $voucher_code = old('voucher_code',[]);
                                                            @endphp
                                                            <input type="text" class="voucher_code form-control"
                                                                name="voucher_code[]"
                                                                placeholder="{{ __('admin.enter_voucher_code') }}"
                                                                @if(@$voucher_code[$key]) value="{{ $voucher_code[$key] }}" @else value="" @endif  />
                                                            <span style="margin-top:5px;display:block;"
                                                                class="voucher_code_error text-danger"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            @php
                                                             $quantity = old('quantity',[]);
                                                            @endphp
                                                            <input type="text" class="quantity form-control" name="quantity[]"
                                                                placeholder="{{ __('admin.enter_no_of_tickets') }}"
                                                                @if(@$quantity[$key]) value="{{ $quantity[$key] }}" @else value="" @endif  onkeypress="return isNumberKey(this, event,false);" />
                                                            <span style="margin-top:5px;display:block;"
                                                                class="quantity_error text-danger"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                            @php
                                                                $max_uses = old('max_uses',[]);
                                                            @endphp
                                                            <input type="text" class="max_uses form-control" name="max_uses[]"
                                                                placeholder="{{ __('admin.enter_no_of_uses') }}" 
                                                                @if(@$max_uses[$key]) value="{{ $max_uses[$key] }}" @else value="" @endif
                                                                onkeypress="return isNumberKey(this, event,false);" />
                                                            <span style="margin-top:5px;display:block;"
                                                                class="max_uses_error text-danger"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;width:200px;">
                                                            @php
                                                                $valid_from = old('valid_from',[]);
                                                            @endphp
                                                            <input type="date" class="valid_from form-control"
                                                                name="valid_from[]"
                                                                placeholder="{{ __('admin.enter_valid_from') }}"
                                                                @if(@$valid_from[$key]) value="{{ $valid_from[$key] }}" @else value="" @endif onchange="changeFromDate(this)"/>
                                                            <span style="margin-top:5px;display:block;"
                                                                class="valid_from_error text-danger"></span>
                                                        </td>
                                                        <td class="white-space"
                                                            style="padding:10px;vertical-align: top; white-space: nowrap;width:200px;">
                                                            @php
                                                                $valid_to = old('valid_to',[]);
                                                            @endphp
                                                            <input type="date" class="valid_to form-control" form-control"
                                                                name="valid_to[]"
                                                                placeholder="{{ __('admin.enter_valid_to"') }}"
                                                                @if(@$valid_to[$key]) value="{{ $valid_to[$key] }}" @else value="" @endif />
                                                            <span style="margin-top:5px;display:block;"
                                                                class="valid_to_error text-danger"></span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr id="row_add">
                                                    <td style="padding:10px;vertical-align: top;width:300px;">
                                                        <select class="form-control select2 event_id" style="width:250px;"
                                                            name="event_id[]">
                                                            <option class="disabled" value="">
                                                                {{ __('admin.select_event') }}</option>
                                                            @foreach ($events as $event)
                                                                <option value="{{ $event->id }}">
                                                                    {{ $event->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <span style="margin-top:5px;display:block;"
                                                            class="event_error text-danger"></span>
                                                    </td>
                                                    <td style="padding:10px;vertical-align: top;width:300px;">
                                                        <select class="form-control select2 ticket_id" style="width:250px;"
                                                            multiple="multiple" name="ticket_id[]">
                                                        </select>
                                                        <span style="margin-top:5px;display:block;"
                                                            class="ticket_id_error text-danger"></span>
                                                    </td>
                                                    <td style="padding:10px;vertical-align: top;width:300px;">
                                                        <select class="form-control select2 type" multiple="multiple"
                                                            style="width:250px;" multiple="multiple" name="type[]">
                                                            <option value="general" selected>General</option>
                                                            <option value="student">Student</option>
                                                        </select>
                                                        <span style="margin-top:5px;display:block;"
                                                            class="type_error text-danger"></span>
                                                    </td>
                                                    {{-- <td style="padding:10px;vertical-align: top;width:300px;">
                                                        <select class="form-control select2 code_value" style="width:250px;"
                                                            name="code_value[]">
                                                            <option value="fixed" selected>Fixed</option>
                                                            <option value="percent">Percent</option>
                                                        </select>
                                                        <span style="margin-top:5px;display:block;"
                                                            class="code_value_error text-danger"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <input type="text" class="overall_commission form-control"
                                                            value="0.00" name="overall_commission[]"
                                                            placeholder="{{ __('admin.enter_overall_commission') }}"
                                                            value="" onkeypress="return isNumberKey(this, event);">
                                                        <span style="margin-top:5px;display:block;"
                                                            class="overall_commission_error text-danger"></span>
                                                    </td> --}}
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top;width:350px;">
                                                        <select class="form-control select2 agent" name="agents[]">
                                                            <option class="disabled" value="">
                                                                {{ __('admin.select_agent') }}</option>
                                                            @foreach ($agents as $agent)
                                                                <option value="{{ $agent->id }}">
                                                                    {{ $agent->name }} ( {{ $agent->email }})
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <span style="margin-top:5px;display:block;"
                                                            class="agent_error  text-danger"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; width:300px;white-space: nowrap;">
                                                        <input type="text" class="agent_commission form-control"
                                                            style="width:250px;" value="0.00" name="agent_commission[]"
                                                            placeholder="{{ __('admin.enter_agent_commission') }}"
                                                            value="" onkeypress="return isNumberKey(this, event,true);">
                                                        <span style="margin-top:5px;display:block;"
                                                            class="agent_commission_error text-danger"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <input type="text" class="user_discount form-control"
                                                            value="0.00" name="user_discount[]" 
                                                            placeholder="{{ __('admin.user_discount') }}" value=""  onkeypress="return isNumberKey(this, event,true);">
                                                        <span style="margin-top:5px;display:block;"
                                                            class="user_discount_error text-danger"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;width:200px;">
                                                        <input type="text" class="voucher_code form-control"
                                                            name="voucher_code[]"
                                                            placeholder="{{ __('admin.enter_voucher_code') }}"
                                                            value="" />
                                                        <span style="margin-top:5px;display:block;"
                                                            class="voucher_code_error text-danger"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <input type="text" class="quantity form-control" name="quantity[]"
                                                            placeholder="{{ __('admin.enter_no_of_tickets') }}"
                                                            value="" onkeypress="return isNumberKey(this, event,false);" />
                                                        <span style="margin-top:5px;display:block;"
                                                            class="quantity_error text-danger"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;">
                                                        <input type="text" class="max_uses form-control" name="max_uses[]"
                                                            placeholder="{{ __('admin.enter_no_of_uses') }}" value=""
                                                            onkeypress="return isNumberKey(this, event,false);" />
                                                        <span style="margin-top:5px;display:block;"
                                                            class="max_uses_error text-danger"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;width:200px;">
                                                        <input type="date" class="valid_from form-control"
                                                            name="valid_from[]"
                                                            placeholder="{{ __('admin.enter_valid_from') }}"
                                                            value="" onchange="changeFromDate(this)"/>
                                                        <span style="margin-top:5px;display:block;"
                                                            class="valid_from_error text-danger"></span>
                                                    </td>
                                                    <td class="white-space"
                                                        style="padding:10px;vertical-align: top; white-space: nowrap;width:200px;">
                                                        <input type="date" class="valid_to form-control" form-control"
                                                            name="valid_to[]"
                                                            placeholder="{{ __('admin.enter_[]"') }}"
                                                            value="" />
                                                        <span style="margin-top:5px;display:block;"
                                                            class="valid_to_error text-danger"></span>
                                                    </td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-add-new">Submit</button>
                                </div>
                            </div>
                </form>
            @endsection

            @section('javascript')
                <script>
                     $(document).ready(function() {
                        var today = new Date().toISOString().split('T')[0];
                        $(".valid_from").attr('min', today);
                        $(".valid_to").attr('min', today);
                    });
                    function isNumberKey(txt, evt,stat) {
                        var charCode = (evt.which) ? evt.which : evt.keyCode;
                        console.log(stat,'asdasd');
                        if (stat && charCode == 37) {
                            if(txt.value.indexOf('%') === -1) {
                                return true;
                            } else {
                                return false;
                            }
                            
                        }
                        if (charCode == 46) {
                            //Check if the text already contains the . character
                            if (txt.value.indexOf('.') === -1) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            if (charCode > 31 &&
                                (charCode < 48 || charCode > 57))
                                return false;
                        }
                        if(stat){
                            if(  txt.value.indexOf('%') === -1) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                        
                        
                    }
                    $(document).ready(function() {
                        $(document).on('change', '.overall_commission', function() {
                            var tr = $(this).closest('tr');
                            var agent_commission = $(tr).find('.agent_commission');
                            agent_commission.val('0.00');
                            var user_discount = $(tr).find('.user_discount');
                            user_discount.val($(this).val());
                        });
                        $(document)
                            .on('click', 'form button[type=submit]', function(e) {
                                isValid = true;
                                $('#ticket_add tbody tr').each(function() {
                                    var tr = this;
                                    var event_id = $(tr).find('.event_id');
                                    if (!event_id.val()) {
                                        $(tr).find('.event_error').html('Event is required');
                                        isValid = false;
                                    } else {
                                        $(tr).find('.event_error').html('');
                                    }
                                    var ticket_id = $(tr).find('.ticket_id');
                                    // var ticket_count = $(tr).find('.ticket_count');
                                    // ticket_count.val(ticket_id.val().length);
                                    if (!ticket_id.val().length) {
                                        $(tr).find('.ticket_id_error').html('Ticket is required');
                                        isValid = false;
                                    } else {
                                        $(tr).find('.ticket_id_error').html('');
                                    }
                                    var type = $(tr).find('.type');
                                    if (!type.val()) {
                                        $(tr).find('.type_error').html('Type is required');
                                        isValid = false;
                                    } else {
                                        $(tr).find('.type_error').html('');
                                    }

                                    // var code_value = $(tr).find('.code_value');
                                    // if (!code_value.val()) {
                                    //     $(tr).find('.code_value_error').html('Code Value is required');
                                    //     isValid = false;
                                    // } else {
                                    //     $(tr).find('.code_value_error').html('');
                                    // }

                                    // var overall_commission = $(tr).find('.overall_commission');
                                    // if (Number(overall_commission.val()) == 0) {
                                    //     $(tr).find('.overall_commission_error').html(
                                    //         'Overall Commission is required');
                                    //     isValid = false;
                                    // } else {
                                    //     $(tr).find('.overall_commission_error').html('');
                                    // }

                                    var agent = $(tr).find('.agent');
                                    if (!agent.val()) {
                                        $(tr).find('.agent_error').html('Agent is required');
                                        isValid = false;
                                    } else {
                                        $(tr).find('.agent_error').html('');
                                    }
                                    var agent_commission = $(tr).find('.agent_commission');
                                    if (agent_commission.val() == '') {
                                        $(tr).find('.agent_commission_error').html('Agent Commission is required');
                                        isValid = false;
                                    } else {
                                        $(tr).find('.agent_commission_error').html('');
                                    }


                                    var voucher_code = $(tr).find('.voucher_code');
                                    if (!voucher_code.val() || voucher_code.val().length < 5) {
                                        $(tr).find('.voucher_code_error').html(
                                            'Voucher Code is required and min 5 chracters');
                                        isValid = false;
                                    } else {
                                        $(tr).find('.voucher_code_error').html('');
                                    }

                                    var quantity = $(tr).find('.quantity');

                                    if (!quantity.val() || Number(quantity.val()) < 1) {
                                        $(tr).find('.quantity_error').html('No of Tickets is required');
                                        isValid = false;
                                    } else {
                                        $(tr).find('.quantity_error').html('');
                                    }

                                    var max_uses = $(tr).find('.max_uses');
                                    if (!max_uses.val() || max_uses.val() < 1) {
                                        $(tr).find('.max_uses_error').html('Maximum Uses per User is required');
                                        isValid = false;
                                    } else {
                                        $(tr).find('.max_uses_error').html('');
                                    }
                                })
                                if (!isValid) {
                                    e.preventDefault(); //prevent the default action
                                }
                            });
                        $('body').on('click', '#add_row', function() {
                            $('#row_add .select2').each(function() {
                                if ($(this).hasClass("select2-hidden-accessible")) {
                                    $(this).select2('destroy');
                                }
                            })
                            var tr = $('#row_add').clone();
                            tr.insertAfter($('#ticket_add tbody tr:last')).removeAttr('id');

                            $('#row_add .select2').select2();
                            // console.log(tr);
                            var event_id = $(tr).find('.event_id');
                            event_id.val('');
                            $('body').find('.event_error').html('');
                            var ticket_id = $(tr).find('.ticket_id');
                            ticket_id.empty();
                            $('body').find('.ticket_id_error').html('');
                            var type = $(tr).find('.type');
                            type.val('general');
                            $('body').find('.type_error').html('');
                            // var code_value = $(tr).find('.code_value');
                            // code_value.val('fixed');
                            // $('body').find('.code_value_error').html('');
                            // var overall_commission = $(tr).find('.overall_commission');
                            // overall_commission.val('0.00');
                            // $('body').find('.overall_commission_error').html('');
                            var agent = $(tr).find('.agent');
                            agent.val('');
                            $('body').find('.agent_error').html('');
                            var agent_commission = $(tr).find('.agent_commission');
                            agent_commission.val('0.00');
                            $('body').find('.agent_commission_error').html('');
                            var user_discount = $(tr).find('.user_discount');
                            user_discount.val('0.00');
                            var voucher_code = $(tr).find('.voucher_code');
                            voucher_code.val('');
                            $('body').find('.voucher_code_error').html('');
                            var quantity = $(tr).find('.quantity');
                            quantity.val('');
                            $('body').find('.quantity_error').html('');
                            var max_uses = $(tr).find('.max_uses');
                            max_uses.val('');
                            $('body').find('.max_uses_error').html('');
                            tr.find('.select2').select2();
                        })
                        $('body').on('click', '#delete_row', function() {
                            if ($('#ticket_add tbody tr').length > 1) {
                                $('#ticket_add tbody tr:last').remove();
                            }
                        })


                        $('body').on('keyup', '.agent_commission', function() {

                            var tr = $(this).closest('tr');
                            var event_id = $(tr).find('.event_id');

                            if (!event_id.val()) {
                                $(tr).find('.event_error').html('Event is required');
                                $(this).val(0);
                                return false;
                            } else {
                                $(tr).find('.event_error').html('');
                            }

                            var ticket_id = $(tr).find('.ticket_id');
                            if (!ticket_id.val().length) {
                                $(tr).find('.ticket_id_error').html('Ticket is required');
                                $(this).val(0);
                                return false;
                            } else {
                                $(tr).find('.ticket_id_error').html('');
                            }
                            var type = $(tr).find('.type');
                            if (!type.val()) {
                                $(tr).find('.type_error').html('Type is required');
                                $(this).val(0);
                                return false;
                            } else {
                                $(tr).find('.type_error').html('');
                            }

                            // var code_value = $(tr).find('.code_value');
                            // if (!type.val()) {
                            //     $(tr).find('.code_value_error').html('Code value is required');
                            //     $(this).val(0);
                            //     return false;
                            // } else {
                            //     $(tr).find('.code_value_error').html('');
                            // }
                            // var overall_commission = $(tr).find('.overall_commission');
                            // if (Number(overall_commission.val()) == 0) {
                            //     $(tr).find('.overall_commission_error').html('Overall Commission is required');
                            //     $(this).val(0);
                            //     return false;
                            // } else {
                            //     $(tr).find('.overall_commission_error').html('');
                            // }

                            var agent = $(tr).find('.agent');
                            if (!agent.val()) {
                                $(tr).find('.agent_error').html('Agent is required');
                                $(this).val(0);
                                return false;
                            } else {
                                $(tr).find('.agent_error').html('');
                            }

                            // agent_commission = $(this).val();
                            // if (parseFloat(agent_commission) > parseFloat(overall_commission.val())) {
                            //     $(tr).find('.agent_commission_error').html(
                            //         'Agent Commission greater than Overall Commission');
                            //     $(this).val(0);
                            //     return false;
                            // } else {
                            //     $(tr).find('.agent_commission_error').html('');
                            // }
                            // var user_discount = $(tr).find('.user_discount');
                            // if (parseFloat(agent_commission) >= 0) {
                            //     discount = parseFloat(overall_commission.val()) - parseFloat(agent_commission);
                            //     user_discount.val(discount.toFixed(2));
                            // } else {
                            //     user_discount.val('0.00');
                            // }

                        });
                        $('body').on('change', '.event_id', function() {
                            var event_id = $(this).val();
                            var tr = $(this).closest('tr');
                            var ticket_id = $(tr).find('.ticket_id');
                            var type = $(tr).find('.type');
                            ticket_id.attr('disabled', true);
                            if (event_id) {
                                $.ajax({
                                    url: "/organizer/get-tickets/" + event_id + "/0",
                                    type: "POST",
                                    data: {
                                        "_token": "{{ csrf_token() }}"
                                    },
                                    success: function(response) {
                                        ticket_id.empty();
                                        let index = $('#ticket_add tbody tr').length - 1;
                                        ticket_id.attr('name', `ticket_id[${index}][]`);
                                        ticket_id.attr('data-available')
                                        type.attr('name', `type[${index}][]`);
                                        ticket_id.append(new Option('Select All', 'all'));
                                        $(response.tickets).each(function(index, item) {
                                            if (item.sub_category_id) {
                                                ticket_id.append(new Option(item.title + ' ' + item
                                                    .sub_category + '(' + item.price + ')' +
                                                    ' (' + item.available + ')', item.id +
                                                    ',' + item.sub_category_id + '(' + item
                                                    .price + ')'));
                                            } else {
                                                ticket_id.append(new Option(item.title + '(' + item
                                                    .price + ')' + ' (' + item.available +
                                                    ')', item.id));
                                            }
                                        });
                                        ticket_id.attr('disabled', false);
                                    }
                                });
                            }
                        });
                    });
                    function changeFromDate(obj){
                        $(obj).closest('tr').find('.valid_to').attr('min',obj.value);
                    }
                </script>
            @endsection
