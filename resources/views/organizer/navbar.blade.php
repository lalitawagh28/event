<nav class="navbar navbar-default navbar-fixed-top navbar-top" style="background-color:white">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="hamburger btn-link">
                <span class="hamburger-inner"></span>
            </button>
            @section('breadcrumbs')
            <ol class="breadcrumb hidden-xs">
                @php
                $segments = array_filter(explode('/', str_replace(str_replace('/dashboard','',route('voyager.organizer.dashboard')), '', Request::url())));
                $url = route('voyager.organizer.dashboard');
                @endphp
                @if(count($segments) == 0)
                    <li class="active"><i class="voyager-boat"></i> {{ __('voyager::generic.dashboard') }}</li>
                @else
                    <li class="active">
                        <a href="{{ route('voyager.organizer.dashboard')}}"><i class="voyager-boat"></i> {{ __('voyager::generic.dashboard') }}</a>
                    </li>
                    @php
                        $prev = '';
                    @endphp
                    @foreach ($segments as $segment)
                        @php
                        if($segment == 'submit_tickets') {
                            $url = str_replace('/dashboard','',$url);
                            $url .= '/agents_avaliable';
                            $segment = 'Agents Available';
                            $prev = $segment;
                        } elseif($segment == 'edit_submit_tickets') {
                            $url = str_replace('/dashboard','',$url);
                            $url .= '/agents_allocated';
                            $segment = 'Agents Allocated';
                            $prev = $segment;
                        } else {
                            $url .= '/'.$segment;
                        }

                        
                        @endphp
                        @if ($loop->last)
                            @if($prev == 'Agents Available')
                                <li>{{ ucfirst(urldecode("Submit Tickets")) }}</li>
                            @elseif($prev == 'Agents Allocated')
                                <li>{{ ucfirst(urldecode("Edit Submit Tickets")) }}</li>
                            @else
                                <li>{{ ucwords(urldecode(str_replace('-',' ',$segment))) }}</li>
                            @endif
                        @else
                            <li>
                                <a href="{{ $url }}">{{ ucfirst(urldecode($segment)) }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ol>
            @show
        </div>
        @php
            $user = \Illuminate\Support\Facades\Auth::user();
            $clientDetails = \Illuminate\Support\Facades\DB::table('oauth_clients')->whereUserId($user?->getKey())->first();

            
            if(config('app.env') == 'production')
            {
                if(!is_null($clientDetails))
                {
                    $url = 'https://dhigna.com/user-portal-detail/'.$clientDetails?->id.'/'.$clientDetails?->secret;
                }else {
                    $url = '/';
                }

            }else {
                if(!is_null($clientDetails))
                {
                    $url = 'https://uat.dhigna.com/user-portal-detail/'.$clientDetails?->id.'/'.$clientDetails?->secret;
                }else {
                    $url = '/';
                }
            }

        @endphp

        <ul class="nav navbar-nav @if (__('voyager::generic.is_rtl') == 'true') navbar-left @else navbar-right @endif">
            <li style="margin-top:20px">
                <div class="ml-5" style="display:inline-block">
                    <span>{{__('admin.logged_organizer')}}</span>
                </div>
            </li>
            <li class="dropdown profile">
                <a href="#" class="dropdown-toggle text-right" data-toggle="dropdown" role="button"
                   aria-expanded="false"><img src="{{ $user_avatar }}" class="profile-img"> <span
                            class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-animated">
                    <li class="profile-img">
                        <img src="{{ $user_avatar }}" class="profile-img">
                        <div class="profile-body">
                            <h5>{{ Auth::user()->name }}</h5>
                            <h6>{{ Auth::user()->email }}</h6>
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="dropdown-item" href="{{ $url }}"><i class="voyager-categories"></i> User Portal</a>
                    </li>
                    <?php $nav_items = config('voyager.dashboard.navbar_items'); ?>
                    @if(is_array($nav_items) && !empty($nav_items))
                    @foreach($nav_items as $name => $item)
                    <li {!! isset($item['classes']) && !empty($item['classes']) ? 'class="'.$item['classes'].'"' : '' !!}>
                        @if(isset($item['route']) && $item['route'] == 'voyager.logout')
                        <form action="{{ route('eventmie.logout') }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-block">
                                @if(isset($item['icon_class']) && !empty($item['icon_class']))
                                <i class="{!! $item['icon_class'] !!}"></i>
                                @endif
                                {{__($name)}}
                            </button>
                        </form>
                        @else
                            @if(isset($item['route']) && $item['route'] == 'voyager.profile')
                            <a href="{{ Route::has('eventmie.profile') ? route('eventmie.profile')  : '#' }}" {!! isset($item['target_blank']) && $item['target_blank'] ? 'target="_blank"' : '' !!}>
                                @if(isset($item['icon_class']) && !empty($item['icon_class']))
                                <i class="{!! $item['icon_class'] !!}"></i>
                                @endif
                                {{__($name)}}
                            </a>
                            @else
                            <a href="{{ isset($item['route']) && Route::has($item['route']) ? route($item['route']) : (isset($item['route']) ? $item['route'] : '#') }}" {!! isset($item['target_blank']) && $item['target_blank'] ? 'target="_blank"' : '' !!}>
                                @if(isset($item['icon_class']) && !empty($item['icon_class']))
                                <i class="{!! $item['icon_class'] !!}"></i>
                                @endif
                                {{__($name)}}
                            </a>
                            @endif
                        @endif
                    </li>
                    @endforeach
                    @endif
                </ul>
            </li>
        </ul>
    </div>
</nav>
