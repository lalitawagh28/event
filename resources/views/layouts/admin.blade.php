<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NJW4NR8');</script>
<!-- End Google Tag Manager --> 
    
    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="/dist/css/app.css" />
    <link rel="stylesheet" href="/dist/css/support.css" />

    @include('eventmie::layouts.meta')

    @include('eventmie::layouts.favicon')

    @yield('stylesheet')
</head>


<body class="home" {!! is_rtl() ? 'dir="rtl"' : '' !!}>
    <div class="grid grid-cols-12 gap-4 mt-4">
        <div
            class="col-span-12 md:col-span-12 lg:col-span-12 xxl:col-span-12 h-full flex flex-col sm:flex-row dark:border-dark-5">
            
            <div class="flex items-center w-full">
                
                <a href="{{ config('app.url') }}" class="-intro-x flex items-center pt-0 mr-auto">
                    <img alt="" class="w-40" src=" /dist/img/logo.png">
                </a>
                <a href="{{ config('app.url') }}" style="float:right">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px">    <path d="M 12 2.0996094 L 1 12 L 4 12 L 4 21 L 10 21 L 10 14 L 14 14 L 14 21 L 20 21 L 20 12 L 23 12 L 12 2.0996094 z"/></svg>
                </a>

            </div>
            
        </div>
    </div>
<!-- Google Tag Manager (noscript) -->
<!-- End Google Tag Manager (noscript) -->

    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
        your browser</a> to improve your experience.</p>
    <![endif]-->

    {{-- Ziggy directive --}}
    
    {{-- Main wrapper --}}
    <div class="lgx-container" id="eventmie_app">
    

        <section class="main-wrapper">
        
            {{-- page content --}}
            @yield('content')

            
        </section>


    </div>
    <!--Main wrapper end-->

    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=[" your-google-map-api"]&libraries=places"></script>
    <script src="/dist/js/app.js"></script>

    {{-- Page specific javascript --}}
    @yield('javascript')
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://unpkg.com/alpinejs@3.2.2/dist/cdn.min.js" defer></script>
</body>
</html>
