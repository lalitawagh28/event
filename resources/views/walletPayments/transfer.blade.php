
@extends('eventmie::layouts.app')
<style>
    <style>
    .ti-form-label {
        margin-bottom: .5rem;
        display: block;

        line-height: 1.25rem;
        font-weight: 300;
        margin-bottom: 5px;
        color: #000;
        font-size: 18px !important;
    }
    .ti-form-input {
        display: block;
        width: 100%;
        align-items: center;
        padding: .75rem 1rem;
        line-height: 1.25rem;
        margin-right: 10px;
        border-radius: 8px;
        border: 1px solid #999;
    }

    .profile_save {
        padding: 8px 236px 12px 236px;
        align-items: center;
        flex-shrink: 0;
        border-radius: 8px;
        background: #FF5C02;
        color: white;
    }
    .profile_info_user {
        margin-top: 10px;
    }
    .profile_form_title {
        color: #000;
        font-size: 23px;
        font-weight: 600;
        margin-bottom: 20px;
    }
    .profile_forms .card {
    border-radius: 14px;
}

.card h3 {
    border-bottom: 1px solid #333;
    padding-bottom: 5px;
    display: block;
}
.deposit_money_button2{
        background-color: #FF5C02;
        color: white;
        padding: 8 54px;
        border-radius: 5px;
    }

    .deposit_slienttag{
        font-size:10px;
        line-height: 15px;

    }
    .payout{
        display: flex;
        align-items: center;
    }


    .overlay {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 100%;
    background-color: rgb(0, 0, 0);
    background-color: rgb(255 255 255 / 1);
    overflow-x: hidden;
    transition: 0.5s;
    -webkit-box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
    -moz-box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
    box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
}

.closebtn{
    font-size: 25px;
    color: black;
    text-align: right;
    margin-left: 283px;
}
.text-header{
    border-bottom: 1px solid rgb(93, 87, 87);
    display: flex;
    align-items: center;
    padding: 5px;
}
.deposit_slienttags{
    font-size:10px;
}
.profile_info_userss{
    display:flex;
}
.otp_label{
    width:100px;
}
.otp_input{
    width:307px;
}
.resend_otp{
    border-radius: 5px;
    border: 2px solid #FF5C02;
    color: #FF5C02;
    padding: 3 20px;
}
.submit_otp{
    background-color: #FF5C02;
    color: white;
    padding: 3 20px;
    border-radius: 5px;
}
.deposit_money_button{
    text-align: right;
}
.overlay {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 100%;
    background-color: rgb(0, 0, 0);
    background-color: rgb(255 255 255 / 1);
    overflow-x: hidden;
    transition: 0.5s;
    -webkit-box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
    -moz-box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
    box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
}

.closebtn{
    font-size: 25px;
    color: black;
    text-align: right;
    margin-left: 243px;
}
.text-header{
    border-bottom: 1px solid rgb(93, 87, 87);
    display: flex;
    align-items: center;
    padding: 5px;
}
.transfer_slienttags{
    font-size:10px;
}
.profile_info_userss{
    display:flex;
}
.otp_label{
    width:80px;
}
.otp_input{
    width:307px;
}
.resend_otp{
    border-radius: 5px;
    border: 2px solid #FF5C02;
    color: #FF5C02;
    padding: 3 20px;
}
.submit_otp{
    background-color: #FF5C02;
    color: white;
    padding: 3 20px;
    border-radius: 5px;
}



</style>
{{-- @extends('eventmie::layouts.app') --}}
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 m-auto">
            <div class="profile_forms clearfix">
                <div class="card my-5 p-4">
                    <div class="card-header">
                        <h3 class="profile_form_title border-b pb-2">Transfer</h3>
                    </div>
                    <div class="card-body">
                            <span id ="universal_error" class="text-danger"></span>
                            <span id ="remaining_balance_error" class="text-danger"></span>
                        {{-- <form action ="{{route("storeFormData")}}" method="POST" enctype="multipart/form-data"> --}}
                            <form id = "transferForm"  action = "" method="POST"  enctype="multipart/form-data">

                            @csrf
                            @method('post')
                            <div class="col-md-12 m-auto">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user">
                                                <label for="currency" class="col-sm-4 col-form-label ti-form-label">Transfer Form <span class="text-danger">*</span></label>
                                                <div class="input-container col-sm-8">
                                                    <select id="currency" name ="currency" class="ti-form-input" placeholder="Select Currency">
                                                        <option value="" disabled selected>Select Transfer Form </option>
                                                        @foreach($walletList['data']['wallets'] as $wallet)
                                                            <option value="{{ $wallet['ledger']['id'] }}">{{ $wallet['ledger']['name'] }}</option>
                                                        @endforeach
                                                    </select>
                                                    @php
                                                        // dd($currency['code']);
                                                    @endphp
                                                </div>
                                                <span id = "currency_error" class="col-sm-8 offset-sm-4 text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user">
                                                <label for="balance" class="col-sm-4 col-form-label ti-form-label">Balance</label>
                                                <div class="input-container col-sm-8">
                                                    <input name = "balance" type="text" id="balance" class="ti-form-input" value="" readonly>
                                                    {{-- value="{{ $walletList['data']['wallets'][0]['balance'] }}" --}}
                                                </div>
                                                @error('balance')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user payout">
                                                <label for="beneficiary"  class="col-sm-4 col-form-label ti-form-label">Beneficiary <span class="text-danger">*</span></label>
                                                <div class="input-container col-sm-8 ">
                                                    <input type="text" name = "beneficiary" id="beneficiary" class="ti-form-input" value = '{{$userName}}' readonly>
                                                    {{-- value="{{ $walletList['data']['wallets'][0]['name'] }}" --}}
                                                </div>
                                                @error('beneficiary')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user">
                                                <label for="mobile" class="col-sm-4 col-form-label ti-form-label">Mobile</label>
                                                <div class="input-container col-sm-8">
                                                    <input name = 'mobile' type="text" id="mobile" class="ti-form-input" placeholder="" value = '{{$pNumber}}' readonly>
                                                </div>
                                                <span id = "mobile_error" class="offset-sm-4 text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user">
                                                <label for="amount_to_pay" class="col-sm-4 col-form-label ti-form-label">Amount to Pay <span class="text-danger">*</span></label>
                                                <div class="input-container col-sm-8">
                                                    <input name = "amount_to_pay" type="text" id="amount_to_pay" class="ti-form-input" placeholder="">
                                                </div>
                                                <span id = "amount_to_pay_error" class="col-sm-8 offset-sm-4 text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user">
                                                <label for="remaining_balance" class="col-sm-4 col-form-label ti-form-label">Remaining</label>
                                                <div class="input-container col-sm-8">
                                                    <input name = "remaining_balance" type="text" id="remaining_balance" class="ti-form-input" placeholder="0.00" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user">
                                                <label for="transfer_to" class="col-sm-4 col-form-label ti-form-label">Transfer To<span class="text-danger">*</span></label>
                                                <div class="input-container col-sm-8 ">
                                                    <select id="transfer_to" name = "transfer_to" class="ti-form-input" placeholder="Select Deposit To">
                                                        <option value="" disabled selected>Transfer To </option>
                                                        @foreach($walletList['data']['wallets'] as $wallet)
                                                            <option value="{{ $wallet['ledger']['id'] }}">{{ $wallet['ledger']['name'] }}</option>
                                                        @endforeach

                                                        {{-- @foreach($walletList['data']['currencies'] as $currency)
                                                            <option value="{{ $currency['code'] }}">{{ $currency['name'] }}</option>
                                                        @endforeach --}}
                                                    </select>
                                                </div>
                                                <span id = "transfer_to_error" class="col-sm-8 offset-sm-4 text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Reference <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <input name = "Reference" type="text" id="input-label" class="ti-form-input"
                                            placeholder="">
                                    </div>
                                        <span id = "reference_error" class="col-sm-8 offset-sm-4 text-danger "></span>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user">
                                                <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Note </label>
                                                <div class="input-container col-sm-8">
                                                    <input name = "Note"type="text" id="input-label" class="ti-form-input"
                                                        placeholder="Naveen">
                                                          <!-- Display exchange fee message -->
                                                          <div class="response-message">
                                                            <p class="exchange-fee-message"></p>
                                                        </div>
                                                        {{-- <span class="deposit_slienttag pt-3">Ex.Fees: 0.20, Ex Rate: 1 INR = 1.00 GBP</span><br>
                                                        <span class="deposit_slienttag">Debit:10.00 INR , Credit: 9.80 GBP</span> --}}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="profile_info_user">
                                                <label for="attachment" class="col-sm-4 col-form-label ti-form-label">Attachment </label>
                                                <div class="input-container col-sm-8">
                                                    <input name = "Attachment" type="file" id="attachment" class="ti-form-input">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right mt-3">
                                        <div class="deposit_next_button mt-4 mr-4">
                                            <button id="deposit_money_button2" type="submit" class="deposit_money_button2" style="float:right;" >Submit</button>
                                            {{-- <button id="deposit_money_button2" type="submit" class="deposit_money_button2" style="float:right;" >Submit</button> --}}

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- off canvas of otp-->
 <div id="myNavotp" class="overlay" style="">

    <div class="overlay-content" id="otp">
        <div class="text-block">
            <div id = "#responceMessage"></div>
            <div class="text-header">
                <h5>OTP Verification</h5>
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav1()">&times;</a>
            </div>
            <div class="content_main_overlay mt-3">
                <div class="col-md-12">
                    <form id = "" action="" method="POST">
                        <div class="form-group">
                            <div class="profile_info_userss">
                                <label for="input-label" class="otp_label col-form-label ti-form-label">OTP<span
                                        class="text-danger">*</span></label>
                                <div>
                                    <input type="text" id="otpInput" class="otp_input ti-form-input"
                                        placeholder=""/>
                                        <div id="responceMessage">
                                            <!-- Response messages will be displayed here -->
                                        </div>
                                        <span id = "otp_error" class=" text-danger"></span><br>
                                        <input type="hidden" id="beneficiary_id" class="otp_input ti-form-input"
                                        placeholder="" name="beneficiary_id"/>
                                    <span class="deposit_slienttags pt-3">Please Check
                                        OTP Sent to your Mobile . It will Expire in 10
                                        Minutes</span>
                                </div>
                                {{-- <span id = "otp_error" class="offset-sm-4 text-danger"></span> --}}
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-3  mt-2 ">
            <div class="deposit_money_button ">
                <button id ="resendOtp"  class="resend_otp">Resend OTP</button>
                <button id = "verifyotpform" type="submit" class="submit_otp">Submit</button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- off canvas of otp end-->
@endsection

@section('javascript')

{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}
<script>
    $(document).ready(function(){
        function fetchExchangeRate() {
                // Capture input values
                var tranferFrom = $('#currency').val();
                var amountToPay = $('#amount_to_pay').val();
                var tranferTo = $('#transfer_to').val();
                console.log("exchange rate calling");
                // Prepare data to send
                var data = {
                    'currency': tranferFrom,
                    'amount_to_pay': amountToPay,
                    'transfer_to': tranferTo,
                    'transaction_type': "deposit",
                };

                // Send AJAX request
                $.ajax({
                    type: 'POST',
                    url: "{{ route('exchangeRate') }}",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    }, // URL to your server endpoint
                    data: data,
                    success: function(response) {
                        if (response && response.data && response.data.exchange_fee_message) {
                            $('.exchange-fee-message').text(response.data.exchange_fee_message);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error(error);
                    }
                });
            }

            // Attach change event listeners to input fields
            $('#currency, #amount_to_pay, #transfer_to').change(fetchExchangeRate);

            // Initial fetch when page loads
            fetchExchangeRate();

        // Event listener for the dropdown menu
        $('#currency').change(function(){
            // Get the selected currency code
            var selectedCurrency = $(this).val();

            // Loop through the wallets data to find the corresponding balance and update fields
            @foreach($walletList['data']['wallets'] as $wallet)
                if ("{{ $wallet['ledger']['id'] }}" === selectedCurrency) {
                    // Update the balance field with the corresponding balance
                    $('#balance').val("{{ $wallet['balance'] }}");

                    // Get the amount to pay value
                    var amountToPay = parseFloat($('#amount_to_pay').val());
                    // Calculate the remaining balance
                    var remainingBalance = parseFloat("{{ $wallet['balance'] }}") - amountToPay;
                    // Update the remaining balance field
                    $('#remaining_balance').val(remainingBalance.toFixed(2));
                }
            @endforeach
        });

        // Event listener for the amount to pay field
        $('#amount_to_pay').keyup(function(){
            // Get the total balance
            var totalBalance = parseFloat($('#balance').val());
            // Get the amount to pay
            var amountToPay = parseFloat($(this).val());

            // Calculate the remaining balance
            var remainingBalance = totalBalance - amountToPay;

            // Update the remaining balance field
            $('#remaining_balance').val(remainingBalance.toFixed(2));
        });
//ajax
function openNav1() {
            document.getElementById("myNavotp").style.left = "55%";
        }
        function openOtpVerificationModel() {
            // Open the OTP verification model
            openNav1();
        }

        $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//validations

function validateForm() {
        var isValid = true;

        // Validate Transfer Form
        var currency = $('#currency').val();
        if (currency === null || currency === '') {
            isValid = false;
            $('#currency_error').text('Transfer Form is required');
        }else {
            $('#currency_error').text('');
        }

        // Validate Amount to Pay
        var amountToPay = $('#amount_to_pay').val();

        if (amountToPay === '') {
            isValid = false;
            $('#amount_to_pay_error').text('Amount to Pay is required');
        }else {
            $('#amount_to_pay_error').text('');
        }
        var amountToPayValue = parseFloat(amountToPay);
        if (isNaN(amountToPayValue)) {
            // If it's not a valid number
            isValid = false;
            $('#amount_to_pay_error').text('Please enter a valid number for Amount to Pay');
        } else if (amountToPayValue <= 0) {
            // If the value is zero or negative
            isValid = false;
            $('#amount_to_pay_error').text('Amount to Pay must be greater than zero');
        }else {
            $('#amount_to_pay_error').text('');
        }
        // Validate Transfer To
        var transferTo = $('#transfer_to').val();

        if (transferTo === null || transferTo === '') {
            isValid = false;
            $('#transfer_to_error').text('Transfer To is required');
        } else if (transferTo === currency) {
            isValid = false;
            $('#universal_error').text('Transaction cannot be processed with the same wallet');
        } else {
            $('#universal_error').text('');
            $('#transfer_to_error').text('');
        }


        // Validate Reference
        var reference = $('#input-label').val();
        if (reference === '') {
            isValid = false;
            $('#reference_error').text('Reference is required');
        } else {
            $('#reference_error').text('');
        }

        // Validate Beneficiary
        var beneficiary = $('#beneficiary').val();
        if (beneficiary === '') {
            isValid = false;
            $('#beneficiary_error').text('Beneficiary is required');
        }
        else {
            $('#beneficiary_error').text('');
        }
        var mobile = $('#mobile').val();
        if (mobile === '') {
            isValid = false;
            $('#mobile_error').text('Mobile is required');
        } else {
            $('#mobile_error').text('');
        }

        var remainingBalance = parseFloat($('#remaining_balance').val());
        if (remainingBalance < 0) {
            isValid = false;
            $('#remaining_balance_error').text('Insufficient balance in the account.');
        }else {
            $('#remaining_balance_error').text('');
        }

        return isValid;
    }
var transactionId; // Declare transactionId globally
var formData;
$('#transferForm').submit(function(e) {
    e.preventDefault(); // Prevent the default form submission
if (validateForm()) {


    // Collect form data
     formData = new FormData($(this)[0]);

    // Send AJAX request to storeFormData route
    $.ajax({
        url: "{{ route('storeFormData') }}",
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
            // Handle success response
            console.log(response);
            transactionId = response.data.transaction.id;
            console.log("Transaction ID:", transactionId);
            // Open the OTP verification modal
            openOtpVerificationModel();
        },
        error: function(xhr, status, error) {
            // Handle error response
            console.error(xhr.responseText);
        }
    });
}
});
// otp validations
function validateOTPForm() {
    // Validate OTP Input
    var otpValue = $('#otpInput').val().trim();

    if (otpValue === '') {
        $('#responceMessage').html('<div class="text-danger">Please enter the OTP</div>');
        return false;
    } else if (otpValue.length !== 6 || !(/^\d+$/.test(otpValue))) {

        $('#responceMessage').html('<div class="text-danger">OTP should be exactly 6 digits</div>');
        return false;
    } else {
        $('#responceMessage').text('');
        return true;
    }
}

$('#verifyotpform').click(function(e) {
    e.preventDefault(); // Prevent the default form submission

    if (validateOTPForm()) {

        var otpValue = $('#otpInput').val();
        console.log(transactionId);
        console.log(otpValue);
        // Collect form data
        // var formData = new FormData($(this)[0]);

        // Send AJAX request to verifyTransaction route
        $.ajax({
            url: "{{ route('verifyTransaction') }}",
            method: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
            otpValue: otpValue,
            transactionId: transactionId,
            // Add any other data you need to send
            },
            success: function(response) {

                if (response.transaction && response.transaction.status) {

                    $('#responceMessage').html('<div class="alert alert-success">Transaction successfully verified!</div>');
                }
                if (response.message && response.errors && response.errors.code) {
                    var errorMessage = response.errors.code[0]; // Extract the first error message from the 'code' array
                    $('#responceMessage').html('<div class="alert alert-danger">' + errorMessage + '</div>');
                }
                console.log(response);
            },
           error: function(xhr, status, error) {
        // Handle error response
                var errorMessage = xhr.responseJSON && xhr.responseJSON.message ? xhr.responseJSON.message : 'An error occurred while processing your request.';
                console.error(errorMessage);
                $('#responceMessage').html('<div class="alert alert-danger">'  + errorMessage + '</div>');
            }
        });
    }
});


$('#resendOtp').click(function(e) {
    e.preventDefault(); // Prevent the default form submission
    // var otpValue = $('#otpInput').val();
    console.log(transactionId);
    // console.log(otpValue);
    // Collect form data
    // var formData = new FormData($(this)[0]);

    // Send AJAX request to verifyTransaction route
    $.ajax({
        url: "{{ route('resendOtp') }}",
        method: "POST",
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
        // otpValue: otpValue,
        transactionId: transactionId,
        // Add any other data you need to send
    },

        success: function(response) {

            if (response.status === "success") {
                $('#responceMessage').html('<div class="alert alert-success">' + response.message + '</div>');
            } else {
                $('#responceMessage').html('<div class="alert alert-danger">An error occurred while resending OTP.</div>');
            }
            // Handle success response
            console.log('hello this is resend otp');

            console.log(response);

            // You can handle the response as needed
        },
        error: function(xhr, status, error) {
            // Handle error response
            console.error(xhr.responseText);
            $('#responceMessage').html('<div class="alert alert-danger">An error occurred while resending OTP.</div>');
        }
    });

});
        // Function to close the OTP verification form
        function closeNav() {
            document.getElementById("transfer_nav").style.left = "100%";
        }
        //verify otp
    });




</script>



@endsection
