@php
   $event = isset($bookings[0]) ? $bookings[0]->event : null;
   $order = isset($bookings[0]) ? $bookings[0]->orders->first() : null;
   $books = $order ? $order->bookings : null;
   $ticketIds = $books ? $books->pluck('ticket_id')->unique()->toArray() : [];

@endphp

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta charset="utf-8" />
      <!-- utf-8 works for most cases -->
      <meta name="viewport" content="width=device-width" />
      <!-- Forcing initial-scale shouldn't be necessary -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!-- Use the latest (edge) version of IE rendering engine -->
      <meta name="�x-apple-disable-message-reformatting�" />
      <!-- Disable auto-scale in iOS 10 Mail entirely -->
      <title></title>
      <!-- The title tag shows in email notifications, like Android 4.4. -->
      <!-- Web Font / @font-face : BEGIN -->
      <!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
      <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
      <!--[if mso]>
      <style>
         * {
         font-family: sans-serif !important;
         }
      </style>
      <![endif]-->
      <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
      <!--[if !mso]><!-->
      <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
      <!--<![endif]-->
      <!-- Web Font / @font-face : END -->
      <!-- CSS Reset -->
      <style>
         /* What it does: Remove spaces around the email design added by some email clients. */
         /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
         html,
         body {
         margin: 0 auto !important;
         padding: 0 !important;
         height: 100% !important;
         width: 100% !important;
         }
         /* What it does: Stops email clients resizing small text. */
         * {
         -ms-text-size-adjust: 100%;
         -webkit-text-size-adjust: 100%;
         }
         /* What it does: Centers email on Android 4.4 */
         div[style*="margin: 16px 0"] {
         margin: 0 !important;
         }
         /* What it does: Stops Outlook from adding extra spacing to tables. */
         table,
         td {
         mso-table-lspace: 0pt !important;
         mso-table-rspace: 0pt !important;
         }
         /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
         table {
         border-spacing: 0 !important;
         border-collapse: collapse !important;
         table-layout: fixed !important;
         margin: 0 auto !important;
         }
         table table table {
         table-layout: auto;
         }
         /* What it does: Uses a better rendering method when resizing images in IE. */
         img {
         -ms-interpolation-mode: bicubic;
         }
         /* What it does: A work-around for iOS meddling in triggered links. */
         .mobile-link--footer a,
         a[x-apple-data-detectors] {
         color: inherit !important;
         text-decoration: underline !important;
         }
      </style>
      <!-- Progressive Enhancements -->
      <style>
         /* What it does: Hover styles for buttons */
         .button-td,
         .button-a {
         transition: all 100ms ease-in;
         }
         .button-td:hover,
         .button-a:hover {
         background: #6e9a39 !important;
         border-color: #6e9a39 !important;
         }
         a {
         color: #666666;
         }
         /* Media Queries */
         @media screen and (max-width: 480px) {
         /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
         .fluid,
         .fluid-centered {
         width: 100% !important;
         max-width: 100% !important;
         height: auto !important;
         margin-left: auto !important;
         margin-right: auto !important;
         }
         /* And center justify these ones. */
         .fluid-centered {
         margin-left: auto !important;
         margin-right: auto !important;
         }
         /* What it does: Forces table cells into full-width rows. */
         .stack-column,
         .stack-column-center {
         display: block !important;
         width: 100% !important;
         max-width: 100% !important;
         direction: ltr !important;
         }
         /* And center justify these ones. */
         .stack-column-center {
         text-align: center !important;
         }
         /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
         .center-on-narrow {
         text-align: center !important;
         display: block !important;
         margin-left: auto !important;
         margin-right: auto !important;
         float: none !important;
         }
         table.center-on-narrow {
         display: inline-block !important;
         }
         }
         .display_inline {
          display: inline-block;
         }
      </style>
   </head>
   <body width="100%" bgcolor="#ffffff" style="margin: 0">
      <center style="width: 100%; background: #ffffff;margin-top:10px;">
         <!-- Visually Hidden Preheader Text : BEGIN -->
         <div
            style="
            display: none;
            font-size: 1px;
            line-height: 1px;
            max-height: 0px;
            max-width: 0px;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            font-family: sans-serif;
            "
            >
            (Optional) This text will appear in the inbox preview, but not the email
            body.
         </div>
         <!-- Visually Hidden Preheader Text : END -->
         <!--
            Set the email width. Defined in two places:
            1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
            2. MSO tags for Desktop Windows Outlook enforce a 680px width.
            Note: The Fluid and Responsive templates have a different width (600px). The hybrid grid is more "fragile", and I've found that 680px is a good width. Change with caution.
            -->
         <div style="max-width: 800px; margin: auto; padding: 0 30px">
            <!--[if mso]>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
               <tr>
                  <td>

                     <![endif]-->
                     <!-- Email Body : BEGIN -->
                     <table
                        role="presentation"
                        cellspacing="0"
                        cellpadding="0"
                        border="0"
                        align="center"
                        width="100%"
                        style="max-width: 6000px"
                        >
                        <!-- Hero Image, Flush : BEGIN -->
                        <tr>
                           <td style="70%">
                              <h1
                                 style="
                                 font-size: 1.875rem;
                                 line-height: 2.25rem;
                                 font-weight: bold;
                                 text-transform: uppercase;
                                 font-family: sans-serif;
                                 "
                                 >
                                 Invoice
                              </h1>
                           </td>
                           <td bgcolor="#ffffff" style="width:40%">
                              <img
                                @if($event->invoice_logo)
                                @php
                                $content = \Illuminate\Support\Facades\Storage::disk('azure')->get($event->invoice_logo);
                                $imageData = base64_encode($content);
                                $src = 'data:image/png;base64,' . $imageData;
                                @endphp
                                src="{{ $src }}"
                                @else
                                src="{{ asset('images/logo.png')}}"
                                @endif
                                 width="200"
                                 height=""
                                 alt="alt_text"
                                 border="0"
                                 align="center"
                                 class="fluid"
                                 style="
                                 width: 100%;
                                 max-width: 200px;
                                 background: #fff;
                                 font-family: sans-serif;
                                 font-size: 15px;
                                 mso-height-rule: exactly;
                                 line-height: 20px;
                                 color: #555555;
                                 max-height: 80px;
                                 padding-left:8px;
                                 "
                                 />
                           </td>
                        </tr>
                        <!-- Hero Image, Flush : END -->
                        <!-- 1 Column Text + Button : BEGIN -->
                        <tr>
                           <td
                              style="
                              padding: 10px 0px 0;
                              text-align: left;
                              font-family: sans-serif;
                              font-size: 16px;
                              mso-height-rule: exactly;
                              line-height: 20px;
                              color: #222222;
                              vertical-align:top;
                              "
                              >
                              <h1
                                 style="
                                 color: #222222;
                                 font-size: 18px;
                                 font-weight: 600;
                                 margin-top: 0;
                                 "
                                 >
                                 Event Details
                              </h1>
                              <div>
                                 <div style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                    <span style="width: 120px;font-size: 14px; display: inline-block;font-weight: bold;">Event Name:</span>
                                       <span style="font-weight: normal;font-size: 14px; display: inline-block;">
                                    {{  $event->title }}
                                       </span>
                                 </div>
                                 @if($event->organised_by)
                                 <div style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                  <strong style="width: 120px; font-size: 14px; display: inline-block;font-weight: bold;"> Organized By:</strong>
                                  <span style="font-weight: normal;font-size: 14px; display: inline-block;">
                                  {{  $event->organised_by }}
                                  </span>
                                </div>
                                @endif
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                    <span style="width: 120px; font-size: 14px; display: inline-block;font-weight: bold;">Date:</span>
                                       <span style="font-weight: normal;font-size: 14px; display: inline-block;">
                                    {{  \Carbon\Carbon::parse($bookings[0]->event_start_date)->format('l, d F Y') }}
                                       </span>
                                 </p>
                                 <p style="margin-top: 0;font-size: 14px;margin-bottom: 5px;">
                                    <span style="width: 120px; font-size: 14px; display: inline-block;font-weight: bold;">Time:</span>
                                    <span style="font-weight: normal;font-size: 14px; display: inline-block;">  {{  \Carbon\Carbon::parse($bookings[0]->event_start_time)->format('H:i') }} GMT
                                    </span>
                                 </p>
                                 <p style="margin-top: 0;margin-bottom: 5px;font-size: 14px;vertical-align:top;width:100%;">
                                  <span style="width: 120px;display: inline-block;font-weight: bold;vertical-align:top;font-size: 14px;">Venue:</span>
                                  <span style="word-wrap: break-word;display:inline-block;width:300px;line-height:22px;font-size: 14px;margin-top:3px;">{{ $event->venues?->first()?->title  }}, {{ $event->venues?->first()?->country->country_name  }}, {{ $event->venues?->first()?->zipcode  }}.</span>
                                </p>
                                {{-- @php
                                 $order = $bookings[0]->orders?->first();
                                @endphp
                                @if($order->address_type == 1 || $order->address_type == 3)
                                 @if($order->delivery_address)
                                 <div style="margin-top: 0;margin-bottom: 5px;font-size: 14px;vertical-align:top;width:100%;">
                                    <span style="width: 120px;display: inline-block;font-weight: bold;vertical-align:top;font-size: 14px;">Deliver Address:</span>
                                    <span style="word-wrap: break-word;display:inline-block;width:300px;line-height:22px;font-size: 14px;margin-top:3px;">{{ $order->delivery_address  }}.</span>
                                 </d>
                                 @endif
                               @else
                                 @if($order->collection_point)
                                 <div style="margin-top: 0;margin-bottom: 5px;font-size: 14px;vertical-align:top;width:100%;">
                                    <span style="width: 120px;display: inline-block;font-weight: bold;vertical-align:top;font-size: 14px;">Collection Point:</span>
                                    <span style="word-wrap: break-word;display:inline-block;width:300px;line-height:22px;font-size: 14px;margin-top:3px;">{{ $order->collection_point  }}.</span>
                                 </div>
                                 @endif
                               @endif --}}
                              </div>
                           </td>
                           <td
                              style="
                              padding: 10px 0px 0 10px;
                              text-align: left;
                              font-family: sans-serif;
                              font-size: 16px;
                              mso-height-rule: exactly;
                              line-height: 20px;
                              color: #222222;
                              vertical-align:top;
                              "
                              >
                              <h1
                                 style="
                                 color: #222222;
                                 font-size: 18px;
                                 font-weight: 600;
                                 margin-top: 0;
                                 "
                                 >
                                 Customer Details
                              </h1>
                              <div>
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                    <span style="width: 120px; font-size: 14px; display: inline-block;font-weight: bold;">Customer Name:</span>
                                     <span style="font-weight: normal;font-size: 14px;display: inline-block;">{{  $bookings[0]->customer_name }}</span>
                                 </p>
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                      <span style="width: 120px; font-size: 14px; display: inline-block;font-weight: bold;">Contact Number:</span>
                                       <span style="font-weight: normal;font-size: 14px;display: inline-block;">
                                    @if(@$bookings[0]->customer->phone )
                                       @if (preg_match("/^(?:0)\d+$/", @$bookings[0]->customer->phone))
                                          {{  @$bookings[0]->customer->phone }}
                                       @else
                                          +44 {{  @$bookings[0]->customer->phone }}
                                       @endif
                                    @else
                                       {{-- @if (preg_match("/^(?:07)\d+$/", $bookings[0]->attendee?->phone))
                                          {{  @$bookings[0]->attendee?->phone }}
                                       @else
                                          +44 - {{  @$bookings[0]->attendee?->phone }}
                                       @endif --}}
                                    @endif
                                  </span>
                                 </p>
                                 <p style="margin-top: 0;margin-bottom: 5px;font-size: 14px;">
                                      <span style="width: 120px; font-size: 14px; display: inline-block;font-weight: bold;">Order ID:</span>
                                       <span style="font-weight: normal;font-size: 14px;display: inline-block;">
                                       {{  $order->id }}
                                      </span>
                                 </p>

                                 <p style="margin-top: 0;margin-bottom: 5px;font-size: 14px;width:100%;">
                                   <span style="width: 120px;display: inline-block;font-weight: bold;vertical-align:top;font-size: 14px;">Order Date:</span>
                                   <span style="word-wrap: break-word;display:inline-block;width:140px;line-height:22px;margin-top:3px;font-size: 14px;">{{ \Carbon\Carbon::parse($bookings[0]->created_date)->format('l, d F Y') }}</span>
                                 </p>
                                 @php
                                 $order = $bookings[0]->orders?->first();
                                @endphp
                                @if($order->address_type == 1 || $order->address_type == 3)
                                 @if($order->delivery_address)
                                 <div style="margin-top: 0;margin-bottom: 5px;font-size: 14px;vertical-align:top;width:100%;">
                                    <span style="width: 120px;display: inline-block;font-weight: bold;vertical-align:top;font-size: 14px;">Delivery Address:</span>
                                    <span style="word-wrap: break-word;display:inline-block;width:140px;line-height:22px;margin-top:3px;font-size: 14px;padding-top:4px;"><p>{{ $order->delivery_address  }}.</p></span>
                                 </div>
                                 @endif
                               @else
                                 @if($order->collection_point)
                                 <div style="margin-top: 0;margin-bottom: 5px;font-size: 14px;vertical-align:top;width:100%;">
                                    <span style="width: 120px;display: inline-block;font-weight: bold;vertical-align:top;font-size: 14px;">Collection Point:</span>
                                    <span style="word-wrap: break-word;display:inline-block;width:140px;line-height:22px;margin-top:3px;font-size: 14px;padding-top:4px;"><p>{{ $order->collection_point  }}.</p></span>
                                 </div>
                                 @endif
                               @endif
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="2" style="font-family: sans-serif">
                              <h2
                                 style="
                                 font-size: 1.1rem;
                                 line-height: 2.25rem;
                                 font-weight: bold;
                                 text-transform: uppercase;
                                 font-family: sans-serif;
                                 padding-top: 35px;
                                 "
                                 >
                                 DESCRIPTION
                              </h2>
                              <table
                                 class=""
                                 cellspacing="0"
                                 cellpadding="0"
                                 border="0"
                                 align="left"
                                 width="100%"
                                 style="max-width: 6000px"
                                 >
                                 <thead>
                                    <tr style="border-bottom: 2px solid #333">
                                       <th
                                          class="text-left"
                                          style="text-align: left; padding: 0 0 3px;font-size: 15px;"
                                          >
                                          Event Name
                                       </th>
                                       <th
                                          class="text-left"
                                          style="text-align: left; padding: 0 0 3px;font-size: 15px;"
                                          >
                                          Ticket Category
                                       </th>
                                       <th
                                          class="text-left"
                                          style="text-align: left; padding: 0 0 3px;font-size: 15px;"
                                          >
                                          Ticket Price
                                       </th>
                                       <th
                                          class="text-center"
                                          style="text-align:center; padding: 0 0 3px;font-size: 15px;"
                                          >
                                          Qty
                                       </th>
                                       <th
                                          class="text-center"
                                          style="text-align:right; padding: 0 0 3px;font-size: 15px;"
                                          >
                                          Total
                                       </th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @forEach($ticketIds as $key => $ticketId)
                                    @if($books->where('ticket_id',$ticketId)->whereNotNull('sub_category_id')->count())
                                       @php
                                       $sub_category_ids = $books->where('ticket_id',$ticketId)->pluck('sub_category_id')->unique()->toArray();
                                       @endphp
                                       @forEach($sub_category_ids as $i => $sub_category_id)
                                          @php
                                          $loop_data = $books->where('ticket_id',$ticketId)->where('sub_category_id',$sub_category_id);
                                          $data_sub_first = $loop_data->first();
                                          @endphp
                                             <tr style="border-bottom: 2px solid #a5a5a5">
                                                <td class="" style="text-align: left; padding: 15px 10px 15px 0px;font-size: 15px;">
                                                {{   $event->title }}
                                                </td>
                                                <td class="" style="text-align: left; padding: 15px 0;font-size: 15px;">
                                                {{   $data_sub_first->ticket_title }}  {{   $data_sub_first->sub_category_id ?  '(' . $data_sub_first?->subcategory?->title . ' )'  : ''}}
                                                </td>
                                                <td class="" style="text-align: left; padding: 15px 0;font-size: 15px;">
                                                   £{{ number_format((float)$data_sub_first->price, 2, '.', '') }}
                                                </td>
                                                <td class="" style="text-align: center; padding: 15px 0;font-size: 15px;">
                                                   {{ $loop_data->count() }}
                                                </td>
                                                <td class="" style="text-align: right; padding: 15px 0;font-size: 15px;">
                                                   £{{ number_format((float)$loop_data->sum('price'), 2, '.', '')  }}
                                                </td>
                                             </tr>
                                       @endforEach
                                    @else
                                    @php
                                    $loop_data = $books->where('ticket_id',$ticketId);
                                    @endphp
                                    <tr style="border-bottom: 2px solid #a5a5a5">
                                       <td class="" style="text-align: left; padding: 15px 10px 15px 0px;font-size: 15px;">
                                        {{   $event->title }}
                                       </td>
                                       <td class="" style="text-align: left; padding: 15px 0;font-size: 15px;">
                                        {{   $books[$key]->ticket_title }}  {{   $books[$key]->sub_category_id ?  '(' . $books[$key]?->subcategory?->title . ' )'  : ''}}
                                       </td>
                                       <td class="" style="text-align: left; padding: 15px 0;font-size: 15px;">
                                          £{{ number_format((float)$books[$key]->price, 2, '.', '') }}
                                       </td>
                                       <td class="" style="text-align: center; padding: 15px 0;font-size: 15px;">
                                          {{ $loop_data->count() }}
                                       </td>
                                       <td class="" style="text-align: right; padding: 15px 0;font-size: 15px;">
                                          £{{ number_format((float)$loop_data->sum('price'), 2, '.', '')  }}
                                       </td>
                                    </tr>
                                    @endif
                                    @endforEach

                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td></td>
                           <td
                              colspan=""
                              style="
                              padding: 15px 0px 0 0px;
                              text-align: right;
                              font-family: sans-serif;
                              font-size: 16px;
                              mso-height-rule: exactly;
                              line-height: 20px;
                              color: #222222;
                              "
                              >
                              <div style="width: 250px;
                                 text-align: left;
                                 float: right;">
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 16px;">
                                    <strong style="width: 145px; display: inline-block">Gross Total</strong>
                                    <span style="text-align: right;display: inline-block;width:100px">£{{ number_format((float)$order->price, 2, '.', '') }}</span>
                                 </p>
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 16px;">
                                    <strong style="width: 185px; display: inline-block">Processing Fees (+)</strong>
                                    <span style="text-align: right;display: inline-block;width:60px">£{{  number_format((float)$order->tax, 2, '.', '') }}</span>
                                 </p>
                                 @if($books->sum('delivery_charge') > 0)
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 16px;">
                                  <strong style="width: 145px; display: inline-block">Delivery Fees (+)</strong>
                                  <span style="text-align: right;display: inline-block;width:100px">£{{ number_format(round($books->sum('delivery_charge')), 2, '.', '') }}</span>
                                </p>
                                @endif
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 16px;">
                                    <strong style="width: 145px; display: inline-block">Discounts(-)</strong>
                                       <span style="text-align: right;display: inline-block;width:100px">£{{ number_format((float)$order->promocode_reward, 2, '.', '') ?? 0.00 }}</span>
                                 </p>
                                 <p
                                    style="
                                    margin-top: 0;
                                    margin-bottom: 10px;
                                    border-bottom: 2px solid #a5a5a5;
                                    border-top: 2px solid #a5a5a5;
                                    padding: 10px 0;font-size: 16px;
                                    "
                                    >
                                    <strong style="width: 145px; display: inline-block"
                                       >Net Total
                                    </strong>
                                    <span style="text-align: right;display: inline-block;width:100px"> £{{ number_format((float)$order->net_price, 2, '.', '') }}</span>
                                 </p>
                              </div>
                           </td>
                        </tr>
                        <!-- 1 Column Text + Button : BEGIN -->
                     </table>
                     <table  style="padding: 180px 0px 50px 0;" cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
                        <tr>
                           <td
                              style="
                              width: 40%;
                              text-align: left;
                              font-family: sans-serif;
                              font-size: 16px;
                              mso-height-rule: exactly;
                              line-height: 20px;
                              color: #222222;
                              vertical-align: top;
                              "
                              >
                              <h1
                                 style="
                                 color: #222222;
                                 font-size: 20px;
                                 font-weight: 600;
                                 margin-top: 0;
                                 padding-bottom: 0px;
                                 "
                                 >
                                 Company Address
                              </h1>
                              <div>
                                @if($bookings[0]->organizer->support_address)
                                 <p
                                    style="
                                    margin-top: 0;
                                    margin-bottom: 10px;
                                    line-height: 1.5;
                                    font-size: 16px;
                                    padding-right:10px;
                                    "
                                    >
                                    {{  $bookings[0]->organizer->support_address }}
                                 </p>
                                 @else
                                 <p
                                    style="
                                    margin-top: 0;
                                    margin-bottom: 10px;
                                    line-height: 1.5;
                                    font-size: 16px;
                                    padding-right:10px;
                                    "
                                    >
                                    @php
                                    $subject = setting('contact.address');
                                    $pos = strrpos($subject, ',');

                                    if($pos !== false)
                                    {
                                    $subject = substr_replace($subject, ',<br>', $pos, strlen(','));
                                    }
                                      @endphp
                                    {{  $subject }}
                                 </p>
                                 @endif
                              </div>
                           </td>
                           <td
                              style="
                              width: 33%;
                              text-align: left;
                              font-family: sans-serif;
                              font-size: 16px;
                              mso-height-rule: exactly;
                              line-height: 20px;
                              color: #222222;
                              vertical-align: top;
                              "
                              >
                              <h1
                                 style="
                                 color: #222222;
                                 font-size: 20px;P
                                 font-weight: 600;
                                 margin-top: 0;
                                 padding-bottom: 0px;
                                 "
                                 >
                                 Payment Details
                              </h1>
                              <div>
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                    <span style="width: 100px; display: inline-block">Payment Type:</span>
                                    @if($bookings[0]->payment_type == 'online')
                                    <span style="width: 100px; display: inline-block">{{  $bookings[0]->transaction?->payment_gateway ?   (($bookings[0]->transaction?->payment_gateway !=  'Wallet' ) ? __('admin.pay_by_card') : __('admin.wallet')) :  ucfirst($bookings[0]->payment_type) }} </span>
                                    @else
                                    <span style="width: 100px; display: inline-block">{{  ucfirst($bookings[0]->payment_type) }} </span>
                                    @endif
                                 </p>
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                    <span style="width: 100px; display: inline-block">Payment Processed BY:</span>
                                    <span style="width: 100px; display: inline-block">{{  $event->payment_processed }} </span>
                                 </p>
                              </div>
                           </td>
                           <td
                              style="
                              width: 33.33%;
                              text-align: left;
                              font-family: sans-serif;
                              font-size: 16px;
                              mso-height-rule: exactly;
                              line-height: 20px;
                              color: #222222;
                              padding-left: 45px;
                              vertical-align: top;
                              "
                              >
                              <h1
                                 style="
                                 color: #222222;
                                 font-size: 18px;
                                 font-weight: 600;
                                 margin-top: 0;
                                 padding-bottom: 0px;
                                 "
                                 >
                                 Support
                              </h1>
                              <div>
                                @if($bookings[0]->organizer->support_email && $bookings[0]->organizer->support_phone)
                                <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                 <strong style="width: 20px; display: inline-block;margin-right:5px;"
                                    ><img
                                 src="{{ asset('images/callicon.png')}}"
                                 width="16px";
                                 height="16px";
                                 alt="alt_text"
                                 border="0"
                                 align="center"
                                 class="fluid"
                                 style="
                                 width: 100%;
                                 max-width: 16px;
                                 background: #fff;
                                 font-family: sans-serif;
                                 font-size: 15px;
                                 mso-height-rule: exactly;
                                 line-height: 20px;
                                 color: #555555;
                                 "
                                 /></strong
                                    >
                                   {{  $bookings[0]->organizer->support_phone }}
                              </p>
                              <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                 <strong style="width: 16px; display: inline-block;margin-right:5px;"
                                    ><img
                                 src="{{ asset('images/mailicon.png')}}"
                                 width="16px";
                                 height="16px";
                                 alt="alt_text"
                                 border="0"
                                 align="center"
                                 class="fluid"
                                 style="
                                 width: 100%;
                                 max-width: 16px;
                                 background: #fff;
                                 font-family: sans-serif;
                                 font-size: 15px;
                                 mso-height-rule: exactly;
                                 line-height: 20px;
                                 color: #555555;
                                 "
                                 /></strong
                                    >
                                    {{ $bookings[0]->organizer->support_email}}
                              </p>
                                @else
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                    <img
                                    src="{{ asset('images/logo.png')}}"
                                  width="80"
                                  height=""
                                  alt="alt_text"
                                  border="0"
                                  align="center"
                                  class="fluid"
                                  style="
                                    width: 100%;
                                    max-width: 120px;
                                  "
                                />
                                  </p>
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                    <strong style="width: 20px; display: inline-block;margin-right:5px;"
                                       ><img
                                    src="{{ asset('images/callicon.png')}}"
                                    width="16px";
                                    height="16px";
                                    alt="alt_text"
                                    border="0"
                                    align="center"
                                    class="fluid"
                                    style="
                                    width: 100%;
                                    max-width: 16px;
                                    background: #fff;
                                    font-family: sans-serif;
                                    font-size: 15px;
                                    mso-height-rule: exactly;
                                    line-height: 20px;
                                    color: #555555;
                                    "
                                    /></strong
                                       >
                                      {{  setting('contact.phone') }}
                                 </p>
                                 <p style="margin-top: 0; margin-bottom: 5px;font-size: 14px;">
                                    <strong style="width: 16px; display: inline-block;margin-right:5px;"
                                       ><img
                                    src="{{ asset('images/mailicon.png')}}"
                                    width="16px";
                                    height="16px";
                                    alt="alt_text"
                                    border="0"
                                    align="center"
                                    class="fluid"
                                    style="
                                    width: 100%;
                                    max-width: 16px;
                                    background: #fff;
                                    font-family: sans-serif;
                                    font-size: 15px;
                                    mso-height-rule: exactly;
                                    line-height: 20px;
                                    color: #555555;
                                    "
                                    /></strong
                                       >
                                       {{  setting('contact.email') }}
                                 </p>
                                 @endif
                              </div>
                           </td>
                        </tr>
                     </table>
                     <table
                        role="presentation"
                        cellspacing="0"
                        cellpadding="0"
                        border="0"
                        align="center"
                        width="100%"
                        style="max-width: 6000px; font-family: sans-serif"
                        >
                        <tr>
                           <td>
                              <h1
                                 style="
                                 color: #222222;
                                 font-size: 18px;
                                 font-weight: 600;
                                 margin: 0;
                                 padding-bottom: 0px;
                                 "
                                 >
                                 Terms & Conditions*
                              </h1>
                               {!! setting('booking.terms_condition')  !!}
                           </td>
                        </tr>
                     </table>
                     <!-- Email Footer : END -->
                     <!--[if mso]>
                  </td>
               </tr>
            </table>
            <![endif]-->
         </div>
      </center>
   </body>
</html>
