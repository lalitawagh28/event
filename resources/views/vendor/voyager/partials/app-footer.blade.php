<footer class="app-footer">
    <div class="site-footer-right">
        {!! __('voyager::theme.footer_copyright') !!} <a href="https://events.dhigna.com/" target="_blank">Events Dhigna Group</a>
    </div>
</footer>
