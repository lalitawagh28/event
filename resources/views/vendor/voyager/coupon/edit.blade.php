@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@stop

@section('page_title', __('voyager::generic.edit'))

@section('page_header')
    <h1 class="page-title">
        {{ __('voyager::generic.edit') }}
        <i class="voyager-tag"></i> Event Coupon
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" class="form-edit-add" action="{{ route('coupon.update',$coupon->id) }}" method="POST"
                        enctype="multipart/form-data">
                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        @method('PUT')
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Agent Name <small class="text-danger h5">*</small></label>
                                <input type="text" name="agent_name" class="form-control" id="exampleFormControlInput1"
                                    placeholder="Aurora Phantom" value="{{$coupon->agent_name}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Coupon Type <small class="text-danger h5">*</small></label>
                                <select class="form-control couponType" name="code_type" id="exampleFormControlSelect1">
                                    <option value="">Select Type</option>
                                    <option value="free" {{ $coupon->code_type === 'free' ? 'selected' : '' }}>Free</option>
                                    <option value="paid" {{ $coupon->code_type === 'paid' ? 'selected' : '' }}>Paid</option>
                                </select>
                            </div>
                            <div class="form-group" id="coupon_discount" @if ($coupon->code_type == 'free') style="display: none" @endif>
                                <label for="exampleFormControlInput2">Discount <small class="text-danger h5">*</small></label>
                                <input type="text" name="discount" class="form-control" id="exampleFormControlInput2"
                                    placeholder="e.g. 100.00" value="{{$coupon->discount}}">
                            </div>
                            <div class="form-group" id="coupon_value" @if ($coupon->code_type == 'free') style="display: none" @endif>
                                <label for="exampleFormControlSelect1">Value <small class="text-danger h5">*</small></label>
                                <select class="form-control" name="code_value" id="exampleFormControlSelect1">
                                    <option value="">Select value</option>
                                    <option value="fixed" {{ $coupon->code_value === 'fixed' ? 'selected' : '' }}>Fixed</option>
                                    <option value="percentage" {{ $coupon->code_value === 'percentage' ? 'selected' : '' }}>Percentage</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput">Max Uses</label>
                                <input type="text" name="max_uses" class="form-control" id="exampleFormControlInput"
                                    placeholder="1" value="{{$coupon->max_uses}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput3">Quantity <small class="text-danger h5">*</small></label>
                                <input type="text" name="quantity" class="form-control" id="exampleFormControlInput2"
                                    placeholder="10" value="{{$coupon->quantity}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect2">Status <small class="text-danger h5">*</small></label>
                                <select class="form-control" name="is_active" id="exampleFormControlSelect2">
                                    <option value="">Select Status</option>
                                    <option value="1" {{ $coupon->is_active === 1 ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ $coupon->is_active === 0 ? 'selected' : '' }}>Disabled</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Valid From <small class="text-danger h5">*</small></label>
                                        <input type="date" name="valid_from" class="form-control"
                                            placeholder="2023-01-01" id="start_date" value="{{$coupon->valid_from}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Valid To <small class="text-danger h5">*</small></label>
                                        <input type="date" name="valid_to" class="form-control" placeholder="2023-12-01"
                                            id="end_date" value="{{$coupon->valid_to}}">
                                    </div>
                                    <span id="error_message" class="text-danger"></span>
                                </div>
                            </div>
                        </div><!-- panel-body -->
                        <div class="panel-footer">
                        @section('submit-buttons')
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        @stop
                        @yield('submit-buttons')
                    </form>
                </div>
                    
            </div>
        </div>
    </div>
@stop

@section('javascript')
<script>
    $('document').ready(function() {
        $('.toggleswitch').bootstrapToggle();

        //Init datepicker for date fields if data-datepicker attribute defined
        //or if browser does not handle date inputs
        $('.form-group input[type=date]').each(function(idx, elt) {
            if (elt.hasAttribute('data-datepicker')) {
                elt.type = 'text';
                $(elt).datetimepicker($(elt).data('datepicker'));
            } else if (elt.type != 'date') {
                elt.type = 'text';
                $(elt).datetimepicker({
                    format: 'L',
                    extraFormats: ['YYYY-MM-DD']
                }).datetimepicker($(elt).data('datepicker'));
            }
        });

        $('#start_date, #end_date').on('change', function() {
            var startDate = new Date($('#start_date').val());
            var endDate = new Date($('#end_date').val());
            if (startDate > endDate) {
                $('#error_message').text('End date should be greater than or equal to start date');
            } else {
                $('#error_message').text('');
            }
        });

        $('.couponType').change(function (e) {
            let isFree=$(this).val();
            if (isFree=='paid') {
                $('#coupon_discount').css({'display':'block'});
                $('#coupon_value').css({'display':'block'});
            }else{
                $('#coupon_discount').css({'display':'none'});
                $('#coupon_value').css({'display':'none'});
            }
        });
    });
</script>
@stop