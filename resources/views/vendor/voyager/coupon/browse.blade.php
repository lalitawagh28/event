@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing') . ' ' . 'Coupon')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-tag"></i> Event Coupon
        </h1>
        <a href="{{ route('coupon.create') }}" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
        </a>
        <a href="javascript:;" class="btn btn-danger btn-add-new" id="bulk_delete_btn">
            <i class="voyager-trash"></i> <span>{{ __('voyager::generic.bulk_delete') }}</span>
        </a>
        @include('voyager::multilingual.language-selector')
    </div>
@stop
@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @elseif(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="dt-not-orderable">
                                            <input type="checkbox" class="select_all">
                                        </th>
                                        <th>Agent Name</th>
                                        <th>Code</th>
                                        <th>Coupon Value</th>
                                        <th>Discount</th>
                                        <th>Quantity</th>
                                        <th>Max Uses</th>
                                        <th>Valid From</th>
                                        <th>Valid To</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($coupons as $coupon)
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $coupon->id }}"
                                                    value="{{ $coupon->id }}">
                                            </td>
                                            <td>{{ $coupon->agent_name }} </td>
                                            <td>{{ $coupon->code }} </td>
                                            @if ($coupon->discount==0)
                                                <td colspan="2" class="text-center"><span class="badge badge-pill badge-success">Free Coupon</span></td>
                                            @else
                                                <td>{{ $coupon->code_value }} </td>
                                                <td>&euro; {{ $coupon->discount }} </td>
                                            @endif
                                            <td>{{ $coupon->quantity }} </td>
                                            <td>{{ $coupon->max_uses }} </td>
                                            <td>{{ $coupon->valid_from }} </td>
                                            <td>{{ $coupon->valid_to }} </td>
                                            <td>
                                                @if ($coupon->is_active == 1)
                                                    <span class="badge badge-pill badge-success">Active</span>
                                                @else
                                                    <span class="badge badge-pill badge-warning">Disabled</span>
                                                @endif
                                            <td>
                                                <a href="{{ route('coupon.edit', $coupon->id) }}"
                                                    class="btn btn-sm btn-primary" target="_self"
                                                    rel="noopener noreferrer"><i class="voyager-edit"></i>
                                                    Edit</a>

                                                <a href="javascript:;" title="{{ __('voyager::generic.delete') }}"
                                                    class="btn btn-danger delete" data-id="{{ $coupon->id }}"
                                                    id="delete-{{ $coupon->id }}"
                                                    data-action="{{ route('coupon.destroy', $coupon->id) }}"">
                                                    <i class="voyager-trash"></i> <span
                                                        class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i>
                        {{ __('voyager::generic.delete_question') }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                            value="{{ __('voyager::generic.delete_confirm') }} ">
                    </form>
                    <button type="button" class="btn btn-default pull-right"
                        data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- Bulk delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="bulk_delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.are_you_sure_delete') }} <span
                            id="bulk_delete_count"></span> <span id="bulk_delete_display_name"></span>?
                    </h4>
                </div>
                <div class="modal-body" id="bulk_delete_modal_body">
                </div>
                <div class="modal-footer">
                    <form action="#" id="bulk_delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="hidden" name="ids" id="bulk_delete_input" value="">
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                            value="{{ __('voyager::generic.bulk_delete_confirm') }} ">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                        {{ __('voyager::generic.cancel') }}
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('javascript')
    <script>
        $('document').ready(function() {
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
        });

        var deleteFormAction;
        $('.delete').on('click', function(e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/) ?
                deleteFormAction.replace(/([0-9]+$)/, $(this).data('id')) :
                deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

        // Bulk delete selectors
        var $bulkDeleteBtn = $('#bulk_delete_btn');
        var $bulkDeleteModal = $('#bulk_delete_modal');
        var $bulkDeleteCount = $('#bulk_delete_count');
        var $bulkDeleteDisplayName = $('#bulk_delete_display_name');
        var $bulkDeleteInput = $('#bulk_delete_input');
        var $bulkFormInput = $('#bulk_delete_form');
        // Reposition modal to prevent z-index issues
        $bulkDeleteModal.appendTo('body');

        $bulkDeleteBtn.on('click', function(e) {
            var ids = [];
            var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
            var count = $checkedBoxes.length;

            if (count) {
                var $bulkFormAction = $bulkFormInput.prop('action', $('.delete').data('action'));
                // Reset input value
                $bulkDeleteInput.val('');
                // Deletion info
                var displayName = count > 1 ? 'coupons' : 'coupon';
                displayName = displayName.toLowerCase();
                $bulkDeleteCount.html(count);
                $bulkDeleteDisplayName.html(displayName);
                // Gather IDs
                $.each($checkedBoxes, function() {
                    var value = $(this).val();
                    ids.push(value);
                })
                // Set input value
                $bulkDeleteInput.val(ids);
                // Show modal
                $bulkDeleteModal.modal('show');
            } else {
                // No row selected
                toastr.warning('{{ __('voyager::generic.bulk_delete_nothing') }}');
            }
        });
    </script>
@stop
