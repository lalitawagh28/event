

@extends('eventmie::layouts.app')

{{-- @section('title', $event->title)
@section('meta_title', $event->meta_title)
@section('meta_keywords', $event->meta_keywords)
@section('meta_description', $event->meta_description)
@section('meta_image', '/storage/'.$event['thumbnail'])
@section('meta_url', url()->current()) --}}

    
@section('content')

<!--ABOUT-->
<section>
    <div id="lgx-about" class="lgx-about">
        <div class="mt-30 mb-50 mt-mobile-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 m-auto">
                        <div class="">
                            <div class="lgx-banner-info-circle lgx-info-circle">
                                <div class="lgx-registration-form">
                                   
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                            
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card mb-3">
                                                    <h3 class="title px-5">
                                                        Authorization Request
                                                    </h3>
                                                    <div class="card-body px-5">
                                                        <p><strong>{{ $client->name }}</strong> is requesting permission to access your account.</p>
                                                         <!-- Scope List -->
                                                            @if (count($scopes) > 0)
                                                            <div class="scopes">
                                                                    <p><strong>This application will be able to:</strong></p>

                                                                    <ul>
                                                                        @foreach ($scopes as $scope)
                                                                            <li>{{ $scope->description }}</li>
                                                                        @endforeach
                                                                    </ul>
                                                            </div>
                                                        @endif

                                                        <div class="row ">
                                                            <!-- Authorize Button -->
                                                            <form method="post" action="{{ route('passport.authorizations.approve') }}">
                                                                @csrf

                                                                <input type="hidden" name="state" value="{{ $request->state }}">
                                                                <input type="hidden" name="client_id" value="{{ $client->getKey() }}">
                                                                <input type="hidden" name="auth_token" value="{{ $authToken }}">
                                                                <button type="submit" class="btn btn-success btn-approve m-3">Authorize</button>
                                                            </form>

                                                            <!-- Cancel Button -->
                                                            <form method="post" action="{{ route('passport.authorizations.deny') }}">
                                                                @csrf
                                                                @method('DELETE')

                                                                <input type="hidden" name="state" value="{{ $request->state }}">
                                                                <input type="hidden" name="client_id" value="{{ $client->getKey() }}">
                                                                <input type="hidden" name="auth_token" value="{{ $authToken }}">
                                                                <button class="btn btn-danger m-3">Cancel</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="lgx-about-content-area">
                            <div class="lgx-heading">
                                <h2 class="heading"></h2>
                                
                            </div>
                           
                        </div>
                    </div>

                </div>
                <br><br>
               
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
<!--ABOUT END-->
{{-- CUSTOM --}}
{{-- Seating Chart Image --}}



<!--GOOGLE MAP END-->

@endsection


