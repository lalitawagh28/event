@extends('eventmie::layouts.app')
@section('stylesheet')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
    integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
<style>
    .carousel-wrapper {
  width: 1000px;
  margin: auto;
  position: relative;
  text-align: center;
  font-family: sans-serif;
}
.owl-carousel .owl-nav {
  overflow: hidden;
  height: 0px;
}
.owl-theme .owl-dots .owl-dot.active span,
.owl-theme .owl-dots .owl-dot:hover span {
  background: #5110e9;
}

.owl-carousel .item {
  text-align: center;
}
.owl-carousel .nav-button {
  height: 50px;
  width: 25px;
  cursor: pointer;
  position: absolute;
  top: 110px !important;
}
.owl-carousel .owl-prev.disabled,
.owl-carousel .owl-next.disabled {
  pointer-events: none;
  opacity: 0.25;
}
.owl-carousel .owl-prev {
  left: -35px;
}
.owl-carousel .owl-next {
  right: -35px;
}
.owl-theme .owl-nav [class*=owl-] {
  color: #ffffff;
  font-size: 39px;
  background: #000000;
  border-radius: 3px;
  display: flex;
  align-items:center;
}
.owl-carousel .prev-carousel:hover {
  background-position: 0px -53px;
}
.owl-carousel .next-carousel:hover {
  background-position: -24px -53px;
}
</style>
@endsection
@section('title') @lang('eventmie-pro::em.home') @endsection

@section('content')

    <!--Banner slider start-->
    <section>
        <div class="lgx-slider welcome-slider">
            <div class="lgx-banner-style">
                <div class="lgx-inner">
                    <div id="lgx-main-slider" class="owl-carousel lgx-slider-navbottom">
                        <!--Vue slider-->
                        @guest
                            <banner-slider :banners="{{ json_encode($banners, JSON_HEX_APOS) }}"
                                :is_logged="{{ 0 }}" :is_customer="{{ 0 }}"
                                :is_organiser="{{ 0 }}" :is_admin="{{ 0 }}"
                                :is_multi_vendor="{{ setting('multi-vendor.multi_vendor') ? 1 : 0 }}"
                                :demo_mode="{{ config('voyager.demo_mode') }}" :check_session="{{ 1 }}"
                                :s_host="{{ json_encode($_SERVER['REMOTE_ADDR'], JSON_HEX_TAG) }}"></banner-slider>
                        @else
                            <banner-slider :banners="{{ json_encode($banners, JSON_HEX_APOS) }}"
                                :is_logged="{{ 1 }}"
                                :is_customer="{{ Auth::user()->hasRole('customer') ? 1 : 0 }}"
                                :is_organiser="{{ Auth::user()->hasRole('organiser') ? 1 : 0 }}"
                                :is_admin="{{ Auth::user()->hasRole('admin') ? 1 : 0 }}"
                                :is_multi_vendor="{{ setting('multi-vendor.multi_vendor') ? 1 : 0 }}"
                                :demo_mode="{{ config('voyager.demo_mode') }}" :check_session="{{ 1 }}"
                                :s_host="{{ json_encode($_SERVER['REMOTE_ADDR'], JSON_HEX_TAG) }}"></banner-slider>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if (auth()->user()?->type == 'student')
        <div class="student_badge mt-5 px-5 mx-5">
            <div class="students_baj">
                <div class="students_badge_items">
                    <div class="gif_left">
                        <img src="{{ asset('images/gif_left.gif') }}" width="50px">
                    </div>
                    <div class="middle_content">
                        <img src="{{ asset('images/Vector.png') }}" width="20px" height="20px">
                        <h4 class="text-white middile_text">Special 10% Discount for Students</h4>

                    </div>
                    <div class="copy_logo">
                        <img src="{{ asset('images/copy_icon.png') }}" width="20px" height="20px">
                        <p class="text-white copy_logo_text"><u>RBS-Student-S1</u></p>
                    </div>
                    <div class="gif_left">
                        <img src="{{ asset('images/gif_right.gif') }}" width="50px">
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!--Event Upcoming Start-->
    @if (!empty($upcomming_events))
        <div class="bbb_viewed">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col">
                        <div class="bbb_main_container">
                            <div class="bbb_viewed_title_container">
                                <h3 class="bbb_viewed_title">Recommended Events <a
                                        href="{{ route('eventmie.events_index') }}"
                                        class="seeall float-right text-right">See all ></a></h3>

                            </div>
                            {{-- <div class="bbb_viewed_slider_container">
                                <div class="owl-carousel owl-theme bbb_viewed_slider">
                                    <event-listing :events="{{ json_encode($upcomming_events, JSON_HEX_APOS) }}"
                                        :currency="{{ json_encode($currency, JSON_HEX_APOS) }}"
                                        :date_format="{{ json_encode(
                                            [
                                                'vue_date_format' => format_js_date(),
                                                'vue_time_format' => format_js_time(),
                                            ],
                                            JSON_HEX_APOS,
                                        ) }}">
                                    </event-listing>
                                </div>
                            </div> --}}
                            <div class="bbb_viewed_slider_container">
                                <div class="owl-carousel owl-theme bbb_viewed_slider">
                                    @foreach ($upcomming_events as $event)
                                    <div class="owl-item">
                                        <div class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                            <a href="{{ route('eventmie.events_show', $event->slug) }}">
                                            <div class="bbb_viewed_image border"><img src="{{ $event->thumbnail}}" class="bbb_viewed_imagees" alt=""></div>
                                            <div class="bbb_viewed_content ">
                                                <div class="event_date">
                                                    <img src="{{ asset('images/calender_icon.png') }}"  class="calender_icon ">
                                                    {{-- <span class="date bbb_viewed_name_events_list">{{ date('d M Y', strtotime($event->start_date)) }}</span> --}}
                                                    <span class="date bbb_viewed_name_events_list"><time-zone  
                                                                :datetime="{{ json_encode($event->start_date . ' ' . $event->start_time, JSON_HEX_APOS) }}"
                                                                :format="{{ json_encode('YYYY-MM-DD HH:mm:ss', JSON_HEX_APOS) }}"
                                                                :timezone="{{ json_encode($event->time_zone, JSON_HEX_APOS) }}"
                                                                :display_format="{{ json_encode('DD MMM YYYY', JSON_HEX_APOS) }}"
                                                                :show_zonename="{{ json_encode(false, JSON_HEX_APOS) }}"></time-zone></span>
                                                </div>
                                                <div class="bbb_viewed_name_events_list">{{ $event->title}}</div>
                                                <span class="bbb_viewed_smallname">{{ $event->category_name }}</span>
                                            </div>
                                            </a>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!--Event Upcoming END-->
    <!--Event Categories Start-->
    @if (!empty($categories))
        <div class="bbb_viewed">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col">
                        <div class="bbb_main_container">
                            <div class="bbb_viewed_title_container">
                                <h3 class="bbb_viewed_title">Browse Events by Category</h3>
                                <span class="events_text"> Live Events for all your entertainment needs<span><a
                                            href="{{ route('eventmie.events_index') }}"
                                            class="seeall float-right text-right">See all ></a>
                            </div>
                            {{-- <div class="bbb_viewed_slider_container">
                                <div class="owl-carousel owl-theme bbb_viewed_slider">
                                    <event-category
                                        :categories="{{ json_encode($categories, JSON_HEX_APOS) }}"></event-category>

                                </div>
                            </div> --}}

                            <div class="bbb_viewed_slider_container">
                                <div class="owl-carousel owl-theme bbb_viewed_slider">
                                    @foreach ($categories as $key => $category)

                                    <div class="owl-items">
                                        <div class="bbb_item{{ $key%5 }}">
                                            <a href="/events?category={{ $category['name']}}">
                                            <div class="relative">
                                                <p class="bbb_info">{{ $category['name'] }}</p>
                                                {{-- <span class="silent-text">{{ $category['category_count'] }}+ Events</span> --}}
                                                <div class="bb-imgpart">
                                                    <img src="/storage/{{$category['thumb']}}" class="bbb_img1"
                                                            :alt="'category.name'">
                                                </div>
                                            </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!--Event Categories END-->
    <!--Event Top-selling Start-->
    @if (!empty($top_selling_events))
        <div class="bbb_viewed">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col">
                        <div class="bbb_main_container">
                            <div class="bbb_viewed_title_container">
                                <h3 class="bbb_viewed_title">Lastest Events <a href="{{ route('eventmie.events_index') }}"
                                        class="seeall float-right text-right">See all ></a></h3>
                            </div>
                            {{-- <div class="bbb_viewed_slider_container">
                                <div class="owl-carousel owl-theme bbb_viewed_slider">
                                    <event-listing :show_tags="false"
                                        :events="{{ json_encode($top_selling_events, JSON_HEX_APOS) }}"
                                        :currency="{{ json_encode($currency, JSON_HEX_APOS) }}"
                                        :date_format="{{ json_encode(
                                            [
                                                'vue_date_format' => format_js_date(),
                                                'vue_time_format' => format_js_time(),
                                            ],
                                            JSON_HEX_APOS,
                                        ) }}">
                                    </event-listing>
                                </div>
                            </div> --}}
                            <div class="bbb_viewed_slider_container">
                                <div class="owl-carousel owl-theme bbb_viewed_slider">
                                    @foreach ($top_selling_events as $event)
                                    <div class="owl-item">
                                        <div class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                            <a href="{{ route('eventmie.events_show', $event->slug) }}">
                                            <div class="bbb_viewed_image border"><img src="{{ $event->thumbnail}}" class="bbb_viewed_imagees" alt=""></div>
                                            <div class="bbb_viewed_content ">
                                                <div class="event_date">
                                                    <img src="{{ asset('images/calender_icon.png') }}"  class="calender_icon ">
                                                    {{-- <span class="date bbb_viewed_name_events_list">{{ date('d M Y', strtotime($event->start_date)) }}</span> --}}
                                                    <span class="date bbb_viewed_name_events_list"><time-zone  
                                                        :datetime="{{ json_encode($event->start_date . ' ' . $event->start_time, JSON_HEX_APOS) }}"
                                                        :format="{{ json_encode('YYYY-MM-DD HH:mm:ss', JSON_HEX_APOS) }}"
                                                        :timezone="{{ json_encode( $event->time_zone, JSON_HEX_APOS) }}"
                                                        :display_format="{{ json_encode('DD MMM YYYY', JSON_HEX_APOS) }}"
                                                        :show_zonename="{{ json_encode(false, JSON_HEX_APOS) }}"></time-zone></span>
                                                </div>
                                                <div class="bbb_viewed_name_events_list">{{ $event->title}}</div>
                                                <span class="bbb_viewed_smallname">{{ $event->category_name }}</span>
                                            </div>
                                            </a>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!--Event Top-selling END-->

@endsection


@section('javascript')
    <script type="text/javascript">
        var google_map_key = {!! json_encode(setting('apps.google_map_key')) !!};
    </script>
    <script type="text/javascript" src="{{ asset('js/welcome_v1.8.js?v='.filemtime(public_path('js/welcome_v1.8.js'))) }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script> <!-- Owl Carousel JS -->
    <script>
        $(document).ready(function() {
            // Owl Carousel initialization
            $('.bbb_viewed_slider').owlCarousel({
                loop: false,
                margin: 0,
                autoplay: false,
                responsiveClass:true,
                nav: true,
                dots: false,
                navText: ["<div class='nav-button owl-prev'>‹</div>", "<div class='nav-button owl-next'>›</div>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    575: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    991: {
                        items: 3
                    },
                    1199: {
                        items: 3
                    },
                    1400: {
                        items: 4
                    }
                }
            });

            $('.bbb_viewed_prev').on('click', function() {
                $('.bbb_viewed_slider').trigger('prev.owl.carousel');
            });

            $('.bbb_viewed_next').on('click', function() {
                $('.bbb_viewed_slider').trigger('next.owl.carousel');
            });
        });
    </script>

@stop
