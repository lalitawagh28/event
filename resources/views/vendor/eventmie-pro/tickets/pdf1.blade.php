<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width" />
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="�x-apple-disable-message-reformatting�" />
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
      <style>
        * {
          font-family: sans-serif !important;
        }
      </style>
    <![endif]-->
    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
    <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->
    <!-- Web Font / @font-face : END -->
    <!-- CSS Reset -->
    <style>
      /* @page { size: auto; size: A4 portrait; } */
      @media screen and (max-width: 600px) {
        .perform {
          padding: inherit !important;
        }
        ul {
          font-size: 16px !important;
        }
      }
    </style>
  </head>

  <body width="100%" style="margin: 0">
    <center style="width: 100%;">
      <!-- Visually Hidden Preheader Text : BEGIN -->
      <div
        style="
          display: none;
          font-size: 1px;
          line-height: 1px;
          max-height: 0px;
          max-width: 0px;
          opacity: 0;
          overflow: hidden;
          mso-hide: all;
          font-family: 'Varela Round', sans-serif;
        "
      >
        (Optional) This text will appear in the inbox preview, but not the email
        body.
      </div>
      <!-- Visually Hidden Preheader Text : END -->

      <div
        style="
          max-width: 100%;
          margin: auto;
          position: relative;
          min-height: 442px;
          overflow:hidden;
        "
      >
      <div style="position: relative;width:100%;height:100%;position:absolute;z-index:-1;top:0;left:0;">
    <img
    src="{{  asset('images/bg-banner.png') }}"
    style="width:100%;height:442px;object-fit: cover;"/>
      </div>
        <!-- Email Body : BEGIN -->
        <table
          role="presentation"
          cellspacing="0"
          cellpadding="0"
          border="0"
          align="center"
          width="100%"
          style="max-width: 6000px"
        >
          <!-- Hero Image, Flush : BEGIN -->
          <tr>
            <td style="width: 70%">
              <div>
                <a href="https://www.dhigna.com/"
                  ><img
                    @if($booking->event->ticket_logo)
                    @php
                    $content = \Illuminate\Support\Facades\Storage::disk('azure')->get($booking->event->ticket_logo);
                    $imageData = base64_encode($content);
                    $src = 'data:image/png;base64,' . $imageData;
                    @endphp
                    src="{{ $src }}"
                    @else
                    src="{{ asset('images/logo1.png')}}"
                    @endif
                    alt="logo"
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      max-width: 85%;
                      width: 240px;
                      margin: 25px auto 0 20px;
                      text-align: left;
                      display: inline-block;
                      max-height: 80px;
                    "
                /></a>
                <div
                  style="
                    text-align: right;
                    float: right;
                    color: #6feb3a;
                    font-size: 24px;
                    text-transform: uppercase;
                    padding-top: 42px;
                    padding-right: 42px;
                    font-family: monospace;
                    font-weight: 300;
                  "
                >
                  your booking is confirmed!
                  <center>
                  <img
                    src="{{ asset('images/checkicon.png')}}"
                    alt=""
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      max-width: 85%;
                      width: 37px;
                      height: 38px;
                      text-align: center;
                      float: none;
                      display: block;
                      margin: 15px auto;
                    "
                  />
                  </center>
                </div>
              </div>

              <div style="padding: 46px 38px 20px">
                <h2
                  style="
                    color: #ff8235;
                    font-size: 40px;
                    font-family: monospace;
                    margin-bottom: 22px;
                  "
                >
                  @php
                     //preg_match('/^.{1,40}\b/s', $event->title,$match)
                  @endphp
                  {{ $event->title }}
                </h2>
                <div>
                  <table
                    style="
                      width:100%;
                      margin-top: 12px;
                    "
                  >
                  <tr>
                    <td>
                    <span
                      style="
                        background-color: #ff8235;
                        padding: 10px 32px 8px 10px;
                        text-align: left;
                        border: none;
                        margin-right: 0px;
                        color: #000;
                        width: 135px;
                        display: inline-block;
                        font-size: 18px;
                        font-family: monospace;
                        font-weight:bold;
                      "
                      >Date & Time</span
                    >
                    </td>
                    <td style="padding-left:10px; width:100%;">
                    <span
                      style="
                        background-color: white;
                        padding: 10px 32px 8px 10px;
                        width:90%;
                        font-size: 18px;
                        font-family: monospace;
                        display:block;
                      "
                      >{{ \Carbon\Carbon::parse($booking['event_start_date'] . ' ' . $booking['event_start_time'])->format('d, F Y ') . ' at ' . \Carbon\Carbon::parse($booking['event_start_date'] . ' ' . $booking['event_start_time'])->format('H:i')}} GMT</span
                    >
                    </td>
                    </tr>

                    <tr>
                        <td
                    style="
                      margin-bottom: 0;
                      padding-top: 12px;
                    "
                  >
                    <span
                      style="
                        background-color: #ff8235;
                        padding: 10px 32px 8px 10px;
                        text-align: left;
                        border: none;
                        margin-right: 0px;
                        color: #000;
                        width: 135px;
                        display: inline-block;
                        font-size: 18px;
                        font-family: monospace;
                        font-weight:bold;
                      "
                      >Venue</span
                    >
                </td>
                <td style="padding-left:10px;  padding-top: 12px;">
                    <span
                      style="
                        background-color: white;
                        padding: 10px 32px 8px 10px;
                        width:90%;
                        font-size: 18px;
                        font-family: monospace;
                        display:block;
                      ">
                         @if($event->venue)  
                         @php
                           preg_match('/^.{1,60}\b/s', $event->venue,$match)
                         @endphp  
                       @else
                         @php
                           preg_match('/^.{1,60}\b/s', $event->venues?->first()?->city . ' ' . $event->venues?->first()?->country->country_name . ' ' .$event->venues?->first()?->zipcode,$match)
                         @endphp 
                       @endif
                      {{  $match[0]  }} </span
                    >
                </td>
            </tr>
            @if($event->guest_star)
            <tr>
                <td
                    style="

                      margin-bottom: 0;
                      padding-top: 12px;
                    "
                  >
                    <span
                      style="
                        background-color: #ff8235;
                        padding: 10px 32px 8px 10px;
                        text-align: left;
                        border: none;
                        margin-right: 0px;
                        color: #000;
                        width: 135px;
                        display: inline-block;
                        font-size: 18px;
                        font-family: monospace;
                        font-weight:bold;
                      "
                      >Artist
                    </span>
                </td>
                    <td style="padding-left:10px;  padding-top: 12px;">
                    <span
                      style="
                        background-color: white;
                        padding: 10px 32px 8px 10px;
                        width:90%;
                        font-size: 18px;
                        font-family: monospace;
                        display:block;
                      "
                      >{{  $event->guest_star }}</span
                    >
                </td>
                    </tr>
                    @endif
                </table>
                </div>
              </div>
            </td>
            <td
              style="
                width: 30%;
                border-width: 2px;
                border-style: dashed;
                border-color: #fff;
                border-right: 2px;
                border-top: none;
                border-bottom: none;
                vertical-align: top;
                padding: 40px 20px;
                color: #fff;
              "
            >
              <div style="display:flex:flex-direction:column;align-items:stretch">
                <div>
              <div
                style="
                  min-height: 100px;
                  justify-content: flex-start;
                  display: flex;
                  align-items: flex-start;
                "
              >
                <span style="display:inline-block;">
                  <img
                  src="{{ asset('storage/qrcodes/'.$booking['customer_id'].'/'.$booking['id'].'-'.$booking['order_number'].'.png') }}"
                    alt="barcode"
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      max-width: 85%;
                      width: 102px;
                      margin: 0;
                      text-align: left;
                      display:inline-block;
                    "
                  />
                </span>
                <span
                  style="
                    text-align: left;
                    color: #fff;
                    font-family: monospace;
                    font-size: 24px;
                    word-wrap: break-word;
                    padding-right: 50px;
                    margin-top:0px;
                    display:inline-block;
                    font-weight:bold;
                    vertical-align:top;
                    
                  "
                >
                  Booking ID <br />{{ $booking['order_number'] }}
                </span>
              </div>
              <div style="padding-top: 20px;">
                <p
                  style="
                    font-family: monospace;
                    font-size: 20px;
                    margin: 10px 0;
                    font-weight:bold;
                    position:relative;
                  "
                >
                  <img
                    src="{{ asset('images/icon1.png')}}"
                    alt=""
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      max-width: 85%;
                      width: 20px;
                      height: 20px;
                      margin-right: 10px;
                      text-align: left;
                      float: left;
                    "
                  />
                  {{ @$booking->attendee?->name }}
                </p>
                {{-- <p
                  style="
                    font-family: monospace;
                    font-size: 20px;
                    margin: 10px 0;
                  "
                >
                  <img
                    src="{{ asset('images/icon2.png')}}"
                    alt=""
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      max-width: 85%;
                      width: 20px;
                      height: 20px;
                      margin-right: 10px;
                      text-align: left;
                      float: left;
                    "
                  />
                  @if($booking->attendee->phone)
                  @if (preg_match("/^(?:0)\d+$/", @$booking->attendee->phone))
                    {{  @$booking->attendee->phone }}
                  @else
                    +44  {{  @$booking->attendee->phone }}
                  @endif

                  @else
                     +44 - {{  @$booking->attendee?->phone }}
                  @endif
                </p> --}}

                {{-- <p
                  style="
                    font-family: monospace;
                    font-size: 20px;
                    margin: 10px 0;
                  "
                >
                  <img
                    src="{{ asset('images/icon3.png')}}"
                    alt=""
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      max-width: 85%;
                      width: 20px;
                      height: 20px;
                      margin-right: 10px;
                      text-align: left;
                      float: left;
                    "
                  />
                 @if(!empty($booking->orders?->first()?->delivery_address))
                 {!! $booking->orders?->first()?->delivery_address !!}
                 @elseif(!empty($booking->orders?->first()?->collection_point))
                 {!! $booking->orders?->first()?->collection_point !!}
                 @elseif(!empty( $booking->attendee?->address))
                 {!! $booking->attendee?->address !!}
                 @else
                  {!! $booking->customer->address !!}
                  @endif
                </p> --}}
                @if($booking->ticket->gates->count())
                <p
                  style="
                    font-family: monospace;
                    font-size: 20px;
                    margin: 10px 0;
                    font-weight:bold;
                  "
                >
                  <img
                    src="{{ asset('images/icon4.png')}}"
                    alt=""
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      max-width: 85%;
                      width: 20px;
                      height: 20px;
                      margin-right: 10px;
                      text-align: left;
                      float: left;
                    "
                  />
                  Entrance: {{ implode(',',$booking->ticket->gates->pluck('name')->toArray()) }}
                </p>
                @endif
                <p
                  style="
                    font-family: monospace;
                    font-size: 20px;
                    margin: 10px 0;
                    font-weight:bold;
                  "
                >
                  <img
                    src="{{ asset('images/icon5.png')}}"
                    alt=""
                    border="0"
                    align="center"
                    class="fluid"
                    style="
                      max-width: 85%;
                      width: 20px;
                      height: 20px;
                      margin-right: 10px;
                      text-align: left;
                      float: left;
                    "
                  />
                  Block: {{   $booking->ticket->title }}  {{   $booking->sub_category_id ?  '(' . $booking->subcategory->title . ' )'  : ''}} {{ $booking->price }}{{ $booking->currency }}
                </p>

              </div>
            </div>
            <div class="power_by">
              <p style="
              font-family: monospace;
              font-size: 20px;
              margin: 10px 0;
              font-weight:bold;
              text-align:right;
              bottom:0px;
              position:relative;
              right:10px;
              bottom:10px;
              color:white;
            ">Powered by <img
                  src="{{ asset('images/logo1.png')}}"
                  alt="logo"
                  border="0"
                  align="center"
                  class="fluid"
                  style="
                    max-width: 85%;

                    width:100px;
                    margin: 0px;
                    text-align: left;
                    display: inline-block;
                  "
              /></p>
            </div>
          </div>
            </td>
          </tr>

          <!-- Hero Image, Flush : END -->
          <!-- 1 Column Text + Button : BEGIN -->

          <!-- 1 Column Text + Button : BEGIN -->
        </table>

        <!-- Email Footer : END -->
        <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
      </div>
    </center>
  </body>
</html>
