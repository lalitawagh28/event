@extends('eventmie::layouts.app')

@section('title', $event->title)
@section('meta_title', $event->meta_title)
@section('meta_keywords', $event->meta_keywords)
@section('meta_description', $event->meta_description)
@section('meta_image', '/storage/' . $event['thumbnail'])
@section('meta_url', url()->current())


@php
    $earlier = new DateTime($event['start_date']);
    $later = new DateTime($event['end_date']);

    $days_count = $later->diff($earlier)->format('%a') + 1; //3
@endphp
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
    integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('css/profile.css') }}" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
</script>
<style>
    .modal-open .modal {
        height: 400px;
        margin-left: 459px;
        margin-top: 100px
    }

    .modal_icon {
        text-align: right;
        gap: 2px;
    }

    .modal-content {
        height: 34vh !important;
        display: block !important;
        flex-direction: column;
        justify-content: space-between;
    }

    .modal_icon {
        text-align: left !important;
        gap: 2px;
    }

    .ban_img {
        object-fit: inherit !important;
    }
</style>
<style>
    .carousel-wrapper {
  width: 1000px;
  margin: auto;
  position: relative;
  text-align: center;
  font-family: sans-serif;
}
.owl-carousel .owl-nav {
  overflow: hidden;
  height: 0px;
}
.owl-theme .owl-dots .owl-dot.active span,
.owl-theme .owl-dots .owl-dot:hover span {
  background: #5110e9;
}

.owl-carousel .item {
  text-align: center;
}
.owl-carousel .nav-button {
  height: 50px;
  width: 25px;
  cursor: pointer;
  position: absolute;
  top: 110px !important;
}
.owl-carousel .owl-prev.disabled,
.owl-carousel .owl-next.disabled {
  pointer-events: none;
  opacity: 0.25;
}
.owl-carousel .owl-prev {
  left: -35px;
}
.owl-carousel .owl-next {
  right: -35px;
}
.owl-theme .owl-nav [class*=owl-] {
  color: #ffffff;
  font-size: 39px;
  background: #000000;
  border-radius: 3px;
}
.owl-carousel .prev-carousel:hover {
  background-position: 0px -53px;
}
.owl-carousel .next-carousel:hover {
  background-position: -24px -53px;
}
</style>

@section('content')
    <div class="banner">
        <div class="col-sm-12">
        <div class="row">
            <div class="banner_img col-lg-7 col-sm-6 col-md-6 p-0">
                <img src='{{ $event['poster'] }}' class="ban_img">
            </div>
            <div class="overlay_div col-lg-5 col-sm-6 col-md-6 p-0">
                <div class="ban_info">
                    <event-social :url_current="{{ json_encode(url()->current(), JSON_HEX_APOS) }}"
                        :event_title="{{ json_encode(urlencode($event->title), JSON_HEX_APOS) }}"></event-social>
                    <div class="ticket-details">
                        <p class="ticket_info"><span class="label_width">Event Name :</span><span
                                class="input">{{ $event['title'] }}<span></p>
                        <p class="ticket_info"><span class="label_width">Event Category :</span><span
                                class="input">{{ $event->categories->count()? join(',',collect($event->categories)->map(function ($v) {return $v->name;})->toArray()) . '': '| ' }}
                                <span></p>
                        @if (!is_null($event['guest_star']))
                            <p class="ticket_info"><span class="label_width">Artist :</span><span
                                    class="input">{{ $event['guest_star'] }}<span></p>
                        @endif
                        <p class="ticket_info"><span class="label_width">Date :</span>
                                <span class="input">
                                <time-zone  
                                :datetime="{{ json_encode($event->start_date . ' ' . $event->start_time, JSON_HEX_APOS) }}"
                                :format="{{ json_encode('YYYY-MM-DD HH:mm:ss', JSON_HEX_APOS) }}"
                                :timezone="{{ json_encode( $event->time_zone, JSON_HEX_APOS) }}"
                                :display_format="{{ json_encode('DD MMM YYYY', JSON_HEX_APOS) }}"
                                :show_zonename="{{ json_encode(false, JSON_HEX_APOS) }}"></time-zone> 
                                {{-- {{ date('d M Y', strtotime($event['start_date'])) }} --}}
                                (@if ($days_count == 0)
                                    1 @else{{ $days_count }}
                                @endif days) <span></p>
                        <p class="ticket_info"><span class="label_width">Time :</span>
                                <span class="input">
                                {{-- {{ Carbon\Carbon::parse($event->start_date . ' ' . $event->start_time)->format('H:i') }} --}}
                                <time-zone  
                                :datetime="{{ json_encode($event->start_date . ' ' . $event->start_time, JSON_HEX_APOS) }}"
                                :format="{{ json_encode('YYYY-MM-DD HH:mm:ss', JSON_HEX_APOS) }}"
                                :timezone="{{ json_encode( $event->time_zone, JSON_HEX_APOS) }}"
                                :display_format="{{ json_encode('hh:mm', JSON_HEX_APOS) }}"
                                :show_zonename="{{ json_encode(true, JSON_HEX_APOS) }}"></time-zone> 
                                onwards<span></p>
                        <p class="ticket_info"><span class="label_width">Venue :</span><span
                                class="input">{{ $event['venue'] }}<span></p>
                    </div>
                    <div class="ban_buttons">
                        <event-calander :event_start_date_time="{{ json_encode($event->start_date . ' '. $event->start_time, JSON_HEX_APOS) }}"
                            :event_end_date_time="{{ json_encode($event->end_date . ' '. $event->end_time, JSON_HEX_APOS) }}" :time_zone="{{ json_encode($event->time_zone, JSON_HEX_APOS) }}">
                        </event-calander>
                        <booking-button :event="{{ json_encode($event, JSON_HEX_APOS) }}"></booking-button>
                        {{-- @if(Carbon\Carbon::parse($event->end_date . ' '. $event->end_time)->gt(Carbon\Carbon::now()))
                        <div class="date_btn" >
                            <a href="{{ route('eventmie.events_checkout', [$event->slug]) }}"
                                class="event_btn btn mr-md-auto">Book Tickets</a>
                        </div>
                        @endif
                        @if(Carbon\Carbon::parse($event->end_date . ' '. $event->end_time)->lt(Carbon\Carbon::now()))
                        <div class="date_btn">
                            <span class="event_btn btn mr-md-auto text-white ">Event Ended</span>
                        </div>
                        @endif --}}
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="tab" style="clear:both">
        <div class="wrapper">
            <div class="tabs-wrapper">

                <ul  class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Events Details</a></li>
                    <li><a data-toggle="tab" href="#menu1">Terms & Conditions</a></li>
                    <li><a data-toggle="tab" href="#faq">FAQs</a></li>
                    <li><a data-toggle="tab" href="#menu2">Get Directions</a></li>

                </ul>

            </div>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="tab_info">
                        <p>{!! $event['description'] !!}</p>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="tab_info">
                        <ul class="bullet">
                            <li><a target="_blank" href="{{ route('events.getEventInfo', ['id' => $event['id'],'page' => 'terms']) }}">Terms & Conditions</a></li>
                            <li><a target="_blank" href="{{ route('events.getEventInfo', ['id' => $event['id'],'page' => 'privacy']) }}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <div class="tab_info">
                        <div class="row">
                            <div class="col-md-6">

                                @if ($event->latitude && $event->longitude)
                                    <event-map :latitude="{{ json_encode(floatval($event->latitude), JSON_HEX_APOS) }}"
                                        :longitude="{{ json_encode(floatval($event->longitude), JSON_HEX_APOS) }}">
                                    </event-map>
                                @endif
                            </div>
                            {{-- <div class="map">

                 </div> --}}
                            <div class="col-md-6">
                                <div class="map_info">
                                    {{ $event['address'] }} {{ $event['city'] }} {{ $event['state'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="faq" class="tab-pane fade">
                    <div class="tab_info">
                        <p>{!! $event['faq_info'] !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if (!empty($upcomming_events))
        <div class="bbb_viewed">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col">
                        <div class="bbb_main_container">
                            <div class="bbb_viewed_title_container">
                                <h3 class="bbb_viewed_title">Recommended Events <a
                                        href="{{ route('eventmie.events_index') }}"
                                        class="seeall float-right text-right">See all ></a></h3>

                            </div>
                            <div class="bbb_viewed_slider_container">
                                <div class="owl-carousel owl-theme bbb_viewed_slider">
                                    @foreach ($upcomming_events as $event)
                                    <div class="owl-item">
                                        <div class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                            <a href="{{ route('eventmie.events_show', $event->slug) }}">
                                            <div class="bbb_viewed_image border"><img src="{{ $event->thumbnail}}" class="bbb_viewed_imagees" alt=""></div>
                                            <div class="bbb_viewed_content ">
                                                <div class="event_date">
                                                    <img src="{{ asset('images/calender_icon.png') }}"  class="calender_icon ">
                                                    <span class="date bbb_viewed_name_events_list">{{ date('d M Y', strtotime($event->start_date)) }}</span>
                                                </div>
                                                <div class="bbb_viewed_name_events_list">{{ $event->title}}</div>
                                                <span class="bbb_viewed_smallname">{{ $event->category_name }}</span>
                                            </div>
                                            </a>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('javascript')
    <script type="text/javascript">
        var google_map_key = {!! json_encode($google_map_key) !!};

        var stripe_publishable_key = {!! json_encode(setting('apps.stripe_public_key')) !!};

        var stripe_secret_key = {!! json_encode($extra['stripe_secret_key']) !!};

        var is_stripe = {!! json_encode($extra['is_stripe']) !!};

        var is_authorize_net = {!! json_encode($extra['is_authorize_net']) !!};

        var is_bitpay = {!! json_encode($extra['is_bitpay']) !!};

        var is_stripe_direct = {!! json_encode($extra['is_stripe_direct']) !!};

        var is_twilio = {!! json_encode($extra['is_twilio']) !!};

        var default_payment_method = {!! json_encode($extra['default_payment_method']) !!};

        var sale_tickets = {!! json_encode($extra['sale_tickets']) !!};

        var is_pay_stack = {!! json_encode($extra['is_pay_stack']) !!};

        var is_razorpay = {!! json_encode($extra['is_razorpay']) !!};

        var is_paytm = {!! json_encode($extra['is_paytm']) !!};
    </script>

    <script src="https://cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/events_show_v1.9.js') }}?ver={{ filemtime(public_path('js/events_show_v1.9.js')) }}"></script>

    {{-- CUSTOM --}}
    <script type="text/javascript" src="https://js.stripe.com/v3/"></script>

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{ setting('apps.google_map_key') }}&callback=initMap"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.23.0/axios.min.js"
        integrity="sha512-Idr7xVNnMWCsgBQscTSCivBNWWH30oo/tzYORviOCrLKmBaRxRflm2miNhTFJNVmXvCtzgms5nlJF4az2hiGnA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script type="text/javascript">
        var latitude = {!! json_encode($event->latitude) !!};
        var longitude = {!! json_encode($event->longitude) !!};
        var venue = {!! json_encode($event->venue) !!};

        $('.nav-tabs li').on('click', function(e) {
            e.preventDefault();
            $('.nav-tabs li').removeClass('active');
            $(this).addClass('active');
            $('.tab-pane').removeClass('active').removeClass('in');
            $($(this).find('a').attr('href')).addClass('active').addClass('in');
        });

        function initMap() {

            var markerArray = [];

            // Instantiate a directions service.
            var directionsService = new google.maps.DirectionsService;

            // Create a map and center it on Manhattan.
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: {
                    lat: parseFloat(latitude),
                    lng: parseFloat(longitude)
                }
            });

            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(latitude),
                    lng: parseFloat(longitude)
                },
                title: venue,
            });

            // To add the marker to the map, call setMap();
            marker.setMap(map);

            // Create a renderer for directions and bind it to the map.
            var directionsDisplay = new google.maps.DirectionsRenderer({
                map: map
            });

            // Instantiate an info window to hold step text.
            var stepDisplay = new google.maps.InfoWindow;

            // Listen to change events from the start and end lists.
            var onChangeHandler = function() {
                // get current location latlngs
                getUserLocationLatLong(directionsDisplay, directionsService, markerArray, stepDisplay, map);
            };
            document.getElementById('get_directions').addEventListener('click', onChangeHandler);

        }

        let infoWindow;

        function getUserLocationLatLong(directionsDisplay, directionsService, markerArray, stepDisplay, map) {

            console.log('heyy');
            // map = new google.maps.Map(document.getElementById("map"), {
            //     zoom: 13,
            //     center: {lat:  parseFloat(latitude), lng:  parseFloat(longitude)}
            // });
            infoWindow = new google.maps.InfoWindow();

            infoWindow = new google.maps.InfoWindow();

            infoWindow = new google.maps.InfoWindow();

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    position => {
                        const pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        calculateAndDisplayRoute(directionsDisplay, directionsService, markerArray, stepDisplay, map,
                            position.coords.latitude, position.coords.longitude);
                    },
                    () => {
                        alert('Error-1');
                    }
                );
            } else {
                // Browser doesn't support Geolocation
                alert('Browser doesnt support Geolocation');
            }

            console.log('heyy');
            console.log('heyy');
        }



        function calculateAndDisplayRoute(directionsDisplay, directionsService, markerArray, stepDisplay, map, cur_lat,
            cur_lng) {
            // First, remove any existing markers from the map.
            for (var i = 0; i < markerArray.length; i++) {
                markerArray[i].setMap(null);
            }

            directionsService.route({
                origin: new google.maps.LatLng(parseFloat(cur_lat), parseFloat(cur_lng)),
                destination: new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)),
                travelMode: 'DRIVING',
                drivingOptions: {
                    departureTime: new Date( /* now, or future date */ ),
                    trafficModel: google.maps.TrafficModel.BEST_GUESS
                },
            }, function(response, status) {
                console.log(response);
                if (status === 'OK') {
                    document.getElementById('warnings-panel').innerHTML =
                        '<b>' + response.routes[0].warnings + '</b>';
                    directionsDisplay.setDirections(response);
                    showSteps(response, markerArray, stepDisplay, map);
                } else {
                    window.alert(trans('em.fail_directions'));
                }
            });
        }

        function showSteps(directionResult, markerArray, stepDisplay, map) {
            var myRoute = directionResult.routes[0].legs[0];
            for (var i = 0; i < myRoute.steps.length; i++) {
                var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
                marker.setMap(map);
                marker.setPosition(myRoute.steps[i].start_location);
                attachInstructionText(
                    stepDisplay, marker, myRoute.steps[i].instructions, map);
            }
        }

        function attachInstructionText(stepDisplay, marker, text, map) {
            google.maps.event.addListener(marker, 'click', function() {
                stepDisplay.setContent(text);
                stepDisplay.open(map, marker);
            });
        }



        function open() {
            document.getElementById("review_modal").style.display = "block";
        }
        $('body').click(function() {
            $(".modal-wrapper").hide();
        });
    </script>
    {{-- CUSTOM  --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script> <!-- Owl Carousel JS -->

    <script>
        $(document).ready(function() {
            // Owl Carousel initialization
            $('.bbb_viewed_slider').owlCarousel({
                loop: false,
                margin: 0,
                autoplay: false,
                responsiveClass:true,
                nav: true,
                dots: false,
                navText: ["<div class='nav-button owl-prev'>‹</div>", "<div class='nav-button owl-next'>›</div>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    575: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    991: {
                        items: 3
                    },
                    1199: {
                        items: 3
                    },
                    1400: {
                        items: 4
                    }
                }
            });

            $('.bbb_viewed_prev').on('click', function() {
                $('.bbb_viewed_slider').trigger('prev.owl.carousel');
            });

            $('.bbb_viewed_next').on('click', function() {
                $('.bbb_viewed_slider').trigger('next.owl.carousel');
            });
        });
    </script>
@stop
