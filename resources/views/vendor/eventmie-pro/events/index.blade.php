@extends('eventmie::layouts.app')

{{-- Page title --}}
@section('title')
    @lang('eventmie-pro::em.events')
@endsection
@section('stylesheet')

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<style>

.calander_thumb {
    position: relative;
    padding: 0;
}

.calander_thumb img {
    position: absolute;
    height: 130px;
    width:100%;
}

.calander_thumb .calander-thumTitle {
    position: absolute;
    z-index: 1;
    color: #fff;
    width: 100%;
    word-wrap: break-word;
    flex-direction: column;
    padding: 8px;
    top: 0;
}

.calander_thumb .calander-thumTitle h3 {
    color: #fff;
    font-size: 16px;
    word-wrap: break-word;
    word-break: break-all;
    line-height: 1.3;
    margin-top: 10px;
}
body .fc-daygrid-event{white-space: inherit;}
.calander_thumb .calander-thumTitle p {
    color: #fff;
    font-size: 12px;
}
</style>
@endsection
@section('content')


        <router-view :date_format="{{ json_encode([
            'vue_date_format' => format_js_date(),
            'vue_time_format' => format_js_time()
        ], JSON_HEX_APOS) }}">
        </router-view>

@endsection

@section('javascript')

{{-- {{ dd($events_calendar)}} --}}
<script>
    var path           = {!! json_encode($path, JSON_HEX_TAG) !!};
    var events  =  {!! json_encode($events_calendar, JSON_HEX_TAG) !!};
</script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.css">
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.js"></script>
<script type="text/javascript" src="{{ asset('js/events_listing_v1.8.js') }}?ver={{ filemtime(public_path('js/events_listing_v1.8.js')) }}"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@stop
