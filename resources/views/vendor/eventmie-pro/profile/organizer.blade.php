@extends('eventmie::layouts.app')

<link rel="stylesheet" href="{{ asset('css/profile.css') }}" />
@section('content')

<div class="profile_main">

    <div class="col-xl-4">
        <!-- Profile picture card-->
        <div class="card mb-4 mb-xl-0">
            <div class="card"> <img class="card-img-top" src="https://i.imgur.com/K7A78We.jpg" alt="Card image cap">
                <div class="card-body little-profile text-center">
                    <div class="pro-img"><img  alt="user" @if(!is_null($user->avatar)) src="{{ asset('storage/'.$user->avatar)}}" @else src="https://i.imgur.com/8RKXAIV.jpg" @endif  id="preview-image-before-upload"></div>
                    <div class="card_info">
                        <h3 class="card_name">{{$user->name}}</h3>
                        <span class="card_mail">{{$user->email}}</span>
                    </div>

                </div>
            </div>
        </div>
        <div class="profile_organizer"><a href="{{ route('eventmie.profile') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Details</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>

        </div>
        <div class="profile_support"><a href="{{ route('eventmie.profile-support') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Support</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-support') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_setting"><a href="{{ route('eventmie.profile-organizer') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text">Organizer</h4>
            <a class="profile_icon" href="{{ route('eventmie.profile-organizer') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_bank"><a href="{{ route('eventmie.profile-bank') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Bank</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-bank') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_seller"><a  href="{{ route('eventmie.profile-seller') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Seller</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-seller') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_stripe"><a href="{{ route('eventmie.profile-stripe') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Stripe</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-stripe') }}"><i class="fa fa-angle-right"
                    ariahidden="true"></i></a></a>
        </div>
        <div class="profile_mailchimp"><a href="{{ route('eventmie.profile-mailchimp') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Mail Chimp</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-mailchimp') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_resetpin"><a href="{{ route('eventmie.resetPin')}}"  >
            <img src="{{ asset('images/reset_icon.png') }}">
            <h4 class="profile_text1">Reset Pin</h4>
            <a class="profile_icon1" href="{{ route('eventmie.resetPin')}}"  ><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
        <div class="profile_logout"><a href="{{ route('eventmie.logout') }}">
            <img src="{{ asset('images/logout_icon.png') }}">
            <h4 class="profile_text2">Logout</h4>
            <a class="profile_icon2" href="{{ route('eventmie.logout') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
    </div>
    <div class="col-xl-5">
        <div class="profile_form">
            {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}

            @if (session('status'))

                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
            <form class="form-horizontal" action="{{ route('eventmie.updateAuthUser')}}" method="post" enctype="multipart/form-data">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="name" value="{{ $user->name }}">
                <input type="hidden" name="email" value="{{ $user->email }}">
                <input type="hidden" name="phone" value="{{ $user->phone }}">
                <input type="hidden" name="organization_data" value="1">
                <input type="hidden" name="route_name" value="{{ url()->full() }}">

                <p class="profile_form_title">@lang('eventmie-pro::em.org_info')</p>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.organization') @lang('eventmie-pro::em.name') <span class="text-danger">*</span></label>
                    <input type="text" id="second-input" class="ti-form-input" name="organisation"
                        placeholder="Organization Name" value="{{ old('organisation',$user->organisation) }}">
                    @if ($errors->has('organisation'))
                    <span role="alert" style="color:red">{{ $errors->first('organisation') }}</span>
                    @endif
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.description')</label>
                    <textarea type="text" id="second-input" class="ti-form-input form-control" name="org_description">{!! old('org_description',$user->org_description) !!}</textarea>
                    @if ($errors->has('org_description'))
                    <span role="alert" style="color:red">{{ $errors->first('org_description') }}</span>
                    @endif
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">Facebook</label>
                    <input type="text" id="second-input" class="ti-form-input" name="org_facebook" placeholder="e.g. www.facebook.com/YourPage" value="{{ old('org_facebook',$user->org_facebook) }}">
                    @if ($errors->has('org_facebook'))
                    <span role="alert" style="color:red">{{ $errors->first('org_facebook') }}</span>
                    @endif
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">Instagram</label>
                    <input type="text" id="second-input" class="ti-form-input" name="org_instagram" placeholder="e.g. www.instagram.com/YourPage" value="{{ old('org_instagram',$user->org_instagram) }}">
                    @if ($errors->has('org_instagram'))
                    <span role="alert" style="color:red">{{ $errors->first('org_instagram') }}</span>
                    @endif
                </div>

                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">YouTube</label>
                    <input type="text" id="second-input" class="ti-form-input" name="org_youtube" placeholder="e.g. www.youtube.com/channel/YourChannel" value="{{ old('org_youtube',$user->org_youtube) }}">
                    @if ($errors->has('org_youtube'))
                    <span role="alert" style="color:red">{{ $errors->first('org_youtube') }}</span>
                    @endif
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.website')</label>
                    <input type="text" id="second-input" class="ti-form-input" name="org_twitter" placeholder="e.g. www.yourwebsite.com" value="{{ old('org_twitter',$user->org_twitter) }}">
                    @if ($errors->has('org_twitter'))
                    <span role="alert" style="color:red">{{ $errors->first('org_twitter') }}</span>
                    @endif
                </div>

                <div class="save_button">
                    <button class="btn lgx-btn w-60" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ eventmie_asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ eventmie_asset('js/bootstrap.min.js') }}"></script>
{{-- CUSTOM --}}
<script type="text/javascript">

    $(document).ready(function (e) {


       $('#avatar').change(function(){

        let reader = new FileReader();

        reader.onload = (e) => {

          $('#preview-image-before-upload').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);

       });

       $('#seller_signature').change(function(){

                let reader = new FileReader();

                reader.onload = (e) => {

                  $('#preview-image-signature').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);

               });

    });
</script>
{{-- CUSTOM --}}
@stop
