@extends('eventmie::layouts.app')

<link rel="stylesheet" href="{{ asset('css/profile.css') }}" />
@section('content')

<div class="profile_main">

    <div class="col-xl-4">
        <!-- Profile picture card-->
        <div class="card mb-4 mb-xl-0">
            <div class="card"> <img class="card-img-top" src="https://i.imgur.com/K7A78We.jpg" alt="Card image cap">
                <div class="card-body little-profile text-center">
                    <div class="pro-img"><img  alt="user" @if(!is_null($user->avatar)) src="{{ asset('storage/'.$user->avatar)}}" @else src="https://i.imgur.com/8RKXAIV.jpg" @endif  id="preview-image-before-upload"></div>
                    <div class="card_info">
                        <h3 class="card_name">{{$user->name}}</h3>
                        <span class="card_mail">{{$user->email}}</span>
                    </div>

                </div>
            </div>
        </div>
        <div class="profile_support"><a href="{{ route('eventmie.profile') }}">
            <img src="{{ asset('images/icon_profile.png') }}" >
            <h4 class="profile_text1">Profile Details</h4>
            <a class="profile_icon" href="{{ route('eventmie.profile') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
        <div class="profile_setting"><a href="{{ route('eventmie.profile-support') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text">Profile Support</h4>
            <a class="profile_icon" href="{{ route('eventmie.profile-support') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_resetpin"><a href="{{ route('eventmie.resetPin')}}"  >
            <img src="{{ asset('images/reset_icon.png') }}">
            <h4 class="profile_text1">Reset Pin</h4>
            <a class="profile_icon1" href="{{ route('eventmie.resetPin')}}"  ><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
        <div class="profile_logout"><a href="{{ route('eventmie.logout') }}">
            <img src="{{ asset('images/logout_icon.png') }}">
            <h4 class="profile_text2">Logout</h4>
            <a class="profile_icon2" href="{{ route('eventmie.logout') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
    </div>
    <div class="col-xl-5">
        <div class="profile_form">
            {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}

            @if (session('status'))

                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
            <form class="form-horizontal" action="{{ route('eventmie.updateSupport')}}" method="post" enctype="multipart/form-data">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="name" value="{{ $user->name }}">
                <input type="hidden" name="email" value="{{ $user->email }}">
                <input type="hidden" name="phone" value="{{ $user->phone }}">

                <p class="profile_form_title">Support</p>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">Spport Email <span class="text-danger">*</span></label>
                    <input type="email" id="second-input" class="ti-form-input" name="support_email"
                        placeholder="support@gmail.com" value="{{ old('support_email',$user->support_email) }}">
                    @if ($errors->has('support_email'))
                    <span role="alert" style="color:red">{{ $errors->first('support_email') }}</span>
                    @endif
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">Support Mobile Number <span class="text-danger">*</span></label>
                    <input type="text" id="second-input" class="ti-form-input" name="support_phone"
                        placeholder="Support Mobile number" value="{{ old('support_email',$user->support_phone) }}">
                    @if ($errors->has('support_phone'))
                    <span role="alert" style="color:red">{{ $errors->first('support_phone') }}</span>
                    @endif
                </div>

                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">Support Address <span class="text-danger">*</span></label>
                    <input type="text" id="second-input" class="ti-form-input" name="support_address"
                        placeholder="Support Address" value="{{ old('support_address',$user->support_address) }}">
                    @if ($errors->has('support_address'))
                    <span role="alert" style="color:red">{{ $errors->first('support_address') }}</span>
                    @endif
                </div>
                <div class="save_button">
                    <button class="btn lgx-btn w-60" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ eventmie_asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ eventmie_asset('js/bootstrap.min.js') }}"></script>
{{-- CUSTOM --}}
<script type="text/javascript">

    $(document).ready(function (e) {


       $('#avatar').change(function(){

        let reader = new FileReader();

        reader.onload = (e) => {

          $('#preview-image-before-upload').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);

       });

       $('#seller_signature').change(function(){

                let reader = new FileReader();

                reader.onload = (e) => {

                  $('#preview-image-signature').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);

               });

    });
</script>
{{-- CUSTOM --}}
@stop
