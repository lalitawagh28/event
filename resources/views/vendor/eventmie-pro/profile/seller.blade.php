@extends('eventmie::layouts.app')

<link rel="stylesheet" href="{{ asset('css/profile.css') }}" />
@section('content')

<div class="profile_main">

    <div class="col-xl-4">
        <!-- Profile picture card-->
        <div class="card mb-4 mb-xl-0">
            <div class="card"> <img class="card-img-top" src="https://i.imgur.com/K7A78We.jpg" alt="Card image cap">
                <div class="card-body little-profile text-center">
                    <div class="pro-img"><img  alt="user" @if(!is_null($user->avatar)) src="{{ asset('storage/'.$user->avatar)}}" @else src="https://i.imgur.com/8RKXAIV.jpg" @endif  id="preview-image-before-upload"></div>
                    <div class="card_info">
                        <h3 class="card_name">{{$user->name}}</h3>
                        <span class="card_mail">{{$user->email}}</span>
                    </div>

                </div>
            </div>
        </div>
        <div class="profile_seller"><a href="{{ route('eventmie.profile') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Details</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>

        </div>
        <div class="profile_support"><a href="{{ route('eventmie.profile-support') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Support</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-support') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_organizer"><a href="{{ route('eventmie.profile-organizer') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Organizer</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-organizer') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_bank"><a  href="{{ route('eventmie.profile-bank') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Bank</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-bank') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_setting"><a href="{{ route('eventmie.profile-seller') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text">Profile Seller</h4>
            <a class="profile_icon" href="{{ route('eventmie.profile-seller') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_stripe"><a href="{{ route('eventmie.profile-stripe') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Stripe</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-stripe') }}"><i class="fa fa-angle-right"
                    ariahidden="true"></i></a></a>
        </div>
        <div class="profile_mailchimp"><a href="{{ route('eventmie.profile-mailchimp') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Mail Chimp</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-mailchimp') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_resetpin"><a href="{{ route('eventmie.resetPin')}}"  >
            <img src="{{ asset('images/reset_icon.png') }}">
            <h4 class="profile_text1">Reset Pin</h4>
            <a class="profile_icon1" href="{{ route('eventmie.resetPin')}}"  ><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
        <div class="profile_logout"><a href="{{ route('eventmie.logout') }}">
            <img src="{{ asset('images/logout_icon.png') }}">
            <h4 class="profile_text2">Logout</h4>
            <a class="profile_icon2" href="{{ route('eventmie.logout') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
    </div>
    <div class="col-xl-5">
        <div class="profile_form">
            {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}

            @if (session('status'))

                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
            <form class="form-horizontal" action="{{ route('eventmie.updateAuthUser')}}" method="post" enctype="multipart/form-data">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="name" value="{{ $user->name }}">
                <input type="hidden" name="email" value="{{ $user->email }}">
                <input type="hidden" name="phone" value="{{ $user->phone }}">
                <input type="hidden" name="seller_data" value="1">
                <input type="hidden" name="route_name" value="{{ url()->full() }}">

                <p class="profile_form_title">@lang('eventmie-pro::em.seller_info')</p>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.seller_name') <span class="text-danger">*</span></label>
                    <input type="text" id="second-input" class="ti-form-input" name="seller_name"
                        placeholder="Steve Carl" value="{{ old('seller_name',$user->seller_name) }}">
                    @if ($errors->has('seller_name'))
                    <span role="alert" style="color:red">{{ $errors->first('seller_name') }}</span>
                    @endif
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.seller_info') <span class="text-danger">*</span></label>
                    <textarea type="text" id="second-input" class="ti-form-input form-control" name="seller_info">{!! old('seller_info',$user->seller_info) !!}</textarea>
                    @if ($errors->has('seller_info'))
                    <span role="alert" style="color:red">{{ $errors->first('seller_info') }}</span>
                    @endif
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.seller_tax_info') <span class="text-danger">*</span></label>
                    <textarea type="text" id="second-input" class="ti-form-input form-control" name="seller_tax_info">{!! old('seller_tax_info',$user->seller_tax_info) !!}</textarea>
                    @if ($errors->has('seller_tax_info'))
                    <span role="alert" style="color:red">{{ $errors->first('seller_tax_info') }}</span>
                    @endif
                </div>
                 <div class="profile_info_user">
                    <img id="preview-image-signature" src="{{ asset('storage/'.$user->seller_signature)}}" alt="profile-pic" style="max-height: 128px;border-radius: 50%;">
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.seller_signature') <span class="text-danger">*</span></label>
                    <input type="file" id="second-input" class="ti-form-input" name="seller_signature" value="{{ old('seller_signature',$user->seller_signature) }}">
                    @if ($errors->has('seller_signature'))
                    <span role="alert" style="color:red">{{ $errors->first('seller_signature') }}</span>
                    @endif
                </div>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.seller_note') <span class="text-danger">*</span></label>
                    <textarea type="text" id="second-input" class="ti-form-input form-control" name="seller_note">{!! old('seller_note',$user->seller_note) !!}</textarea>
                    @if ($errors->has('seller_note'))
                    <span role="alert" style="color:red">{{ $errors->first('seller_note') }}</span>
                    @endif
                </div>
                <div class="save_button">
                    <button class="btn lgx-btn w-60" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ eventmie_asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ eventmie_asset('js/bootstrap.min.js') }}"></script>
{{-- CUSTOM --}}
<script type="text/javascript">

    $(document).ready(function (e) {


       $('#avatar').change(function(){

        let reader = new FileReader();

        reader.onload = (e) => {

          $('#preview-image-before-upload').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);

       });

       $('#seller_signature').change(function(){

                let reader = new FileReader();

                reader.onload = (e) => {

                  $('#preview-image-signature').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);

               });

    });
</script>
{{-- CUSTOM --}}
@stop
