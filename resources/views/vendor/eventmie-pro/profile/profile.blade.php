@extends('eventmie::layouts.app')

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
    integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

<link rel="stylesheet" href="{{ asset('css/profile.css') }}" />
@section('content')

    <div class="profile_main">

        <div class="col-xl-4">
            <!-- Profile picture card-->
            <div class="card mb-4 mb-xl-0">
                <div class="card"> <img class="card-img-top" src="https://i.imgur.com/K7A78We.jpg" alt="Card image cap">
                    <div class="card-body little-profile text-center">
                        <div class="pro-img"><img alt="user"
                                @if (!is_null($user->avatar)) src="{{ asset('storage/' . $user->avatar) }}" @else src="https://i.imgur.com/8RKXAIV.jpg" @endif
                                id="preview-image-before-upload"></div>
                        <div class="card_info">
                            <h3 class="card_name">{{ $user->name }}</h3>
                            <span class="card_mail">{{ $user->email }}</span>
                        </div>

                    </div>
                </div>
            </div>
            <div class="profile_setting"><a href="{{ route('eventmie.profile') }}">
                <img src="{{ asset('images/icon_profile.png') }}">
                <h4 class="profile_text">Profile Details</h4>
                <a class="profile_icon" href="{{ route('eventmie.profile') }}"><i class="fa fa-angle-right"
                        aria-hidden="true"></i></a></a>

            </div>
            @if(\Auth::user()->hasRole('organiser'))
            <div class="profile_support"><a href="{{ route('eventmie.profile-support') }}">
                <img src="{{ asset('images/icon_profile.png') }}">
                <h4 class="profile_text1">Profile Support</h4>
                <a class="profile_icon1" href="{{ route('eventmie.profile-support') }}"><i class="fa fa-angle-right"
                        aria-hidden="true"></i></a></a>
            </div>
            <div class="profile_organizer"><a  href="{{ route('eventmie.profile-organizer') }}">
                <img src="{{ asset('images/icon_profile.png') }}">
                <h4 class="profile_text1">Organizer</h4>
                <a class="profile_icon1" href="{{ route('eventmie.profile-organizer') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>
            </div>
            <div class="profile_bank"><a href="{{ route('eventmie.profile-bank') }}">
                <img src="{{ asset('images/icon_profile.png') }}">
                <h4 class="profile_text1">Profile Bank</h4>
                <a class="profile_icon1" href="{{ route('eventmie.profile-bank') }}"><i class="fa fa-angle-right"
                        aria-hidden="true"></i></a></a>
            </div>
            <div class="profile_seller"><a href="{{ route('eventmie.profile-seller') }}">
                <img src="{{ asset('images/icon_profile.png') }}">
                <h4 class="profile_text1">Profile Seller</h4>
                <a class="profile_icon1" href="{{ route('eventmie.profile-seller') }}"><i class="fa fa-angle-right"
                        aria-hidden="true"></i></a></a>
            </div>
            <div class="profile_stripe"><a href="{{ route('eventmie.profile-stripe') }}">
                <img src="{{ asset('images/icon_profile.png') }}">
                <h4 class="profile_text1">Stripe</h4>
                <a class="profile_icon1" href="{{ route('eventmie.profile-stripe') }}"><i class="fa fa-angle-right"
                        aria-hidden="true"></i></a></a>
            </div>
            <div class="profile_mailchimp"><a href="{{ route('eventmie.profile-mailchimp') }}">
                <img src="{{ asset('images/icon_profile.png') }}">
                <h4 class="profile_text1">Mail Chimp</h4>
                <a class="profile_icon1" href="{{ route('eventmie.profile-mailchimp') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>
            </div>
            @endif
            <div class="profile_resetpin"><a href="{{ route('eventmie.resetPin') }}">
                <img src="{{ asset('images/reset_icon.png') }}">
                <h4 class="profile_text1">Reset Pin</h4>
                <a class="profile_icon1" href="{{ route('eventmie.resetPin') }}"><i class="fa fa-angle-right"
                        aria-hidden="true"></i></a></a>

            </div>
            <div class="profile_logout"><a href="{{ route('eventmie.logout') }}">
                <img src="{{ asset('images/logout_icon.png') }}">
                <h4 class="profile_text2">Logout</h4>
                <a class="profile_icon2" href="{{ route('eventmie.logout') }}"><i class="fa fa-angle-right"
                        aria-hidden="true"></i></a></a>

            </div>
        </div>
        <div class="col-xl-5">
            <div class="profile_form">
                {{-- @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif --}}

                @if (session('status'))

                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                <form class="form-horizontal" action="{{ route('eventmie.updateAuthUser') }}" method="post"
                    enctype="multipart/form-data">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <p class="profile_form_title">Profile Details</p>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">Name <span class="text-danger">*</span></label>
                        <input type="text" id="second-input" class="ti-form-input" name="name"
                            placeholder="Name" value="{{ $user->name }}">
                        @if ($errors->has('name'))
                        <span role="alert" style="color:red">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">Email <span class="text-danger">*</span></label>
                        <input type="email" id="second-input" class="ti-form-input" name="email"
                            placeholder="email@gmail.com" value="{{ $user->email }}">
                        @if ($errors->has('email'))
                        <span role="alert" style="color:red">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">Mobile Number <span class="text-danger">*</span></label>
                        <input type="text" id="second-input" class="ti-form-input" name="phone"
                            placeholder="Mobile number" value="{{ old('phone',$user->phone)}}">
                        @if ($errors->has('phone'))
                        <span role="alert" style="color:red">{{ $errors->first('phone') }}</span>
                        @endif
                    </div>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.address')</label>
                        <input  class="ti-form-input" name="address" type="text"  value="{{$user->address}}" >
                        @if ($errors->has('address'))
                            <div class="help text-danger">{{ $errors->first('address') }}</div>
                        @endif
                    </div>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.taxpayer_number')</label>
                        <input class="ti-form-input" name="taxpayer_number" type="text"  value="{{$user->taxpayer_number}}" >
                        @if ($errors->has('taxpayer_number'))
                            <div class="help text-danger">{{ $errors->first('taxpayer_number') }}</div>
                        @endif
                    </div>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">Avtar</label>
                        <input type="file" class="ti-form-input" id="avatar" name="avatar">
                        @if ($errors->has('avatar'))
                        <span role="alert" style="color:red">{{ $errors->first('avatar') }}</span>
                        @endif
                    </div>
                    @if(\Auth::user()->hasRole('organiser'))
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">Organization Logo</label>
                        <input type="file" class="ti-form-input" id="organization_logo" name="organization_logo">
                        @if ($errors->has('organization_logo'))
                        <span role="alert" style="color:red">{{ $errors->first('organization_logo') }}</span>
                        @endif
                    </div>
                    @if($user->organization_logo)
                    <div class="profile_info_user">
                        <img id="preview-image-signature" src="{{ asset('storage/'.$user->organization_logo)}}" alt="profile-pic" style="max-height: 128px;">
                    </div>
                    @endif
                    @endif
                    <div class="save_button">
                        <button class="btn lgx-btn w-60" type="submit">Save</button>
                    </div>
                </form>
                 {{-- if logged in user is customer and multi-vendor mode is enabled --}}
                 @if(Auth::user()->hasRole('customer'))
                    @if(setting('multi-vendor.multi_vendor'))

                        @if((setting('multi-vendor.manually_approve_organizer') && empty($user->organisation)) || !setting('multi-vendor.manually_approve_organizer'))

                        <div class="profile_info_user">
                                <label for="input-label" class="ti-form-label host-text">@lang('eventmie-pro::em.want_to_create_host')</label>
                                <button type="button" class="lgx-btn lgx-btn-black lgx-btn-sm" id="show_modal"><i class="fas fa-person-booth"></i> @lang('eventmie-pro::em.become_organiser')</button>
                            </div>
                        @endif
                    @endif

                    @if(setting('multi-vendor.manually_approve_organizer') && !empty($user->organisation))
                        <div class="alert alert-info" role="alert">
                            <strong>@lang('eventmie-pro::em.become_organiser_notification')</strong>
                        </div>
                    @endif
                @endif

                <!-- Modal -->

                <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content" style="height:600px">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">@lang('eventmie-pro::em.become_organiser')</h4>
                                <button type="button" id="show_mymodal_close" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-info">
                                    <h4>@lang('eventmie-pro::em.info')</h4>
                                    <ul>
                                        <li>@lang('eventmie-pro::em.organiser_note_1')</li>
                                        <li>@lang('eventmie-pro::em.organiser_note_2')</li>
                                        <li>@lang('eventmie-pro::em.organiser_note_3')</li>
                                        <li>@lang('eventmie-pro::em.organiser_note_4')</li>
                                    </ul>
                                </div>
                                <form class="form-horizontal" action="{{ route('eventmie.updateAuthUserRole')}}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="role_id" value="3">

                                    <div class="form-group row">
                                        <label class="col-md-3">@lang('eventmie-pro::em.organization')</label>
                                        <div class="col-md-9">
                                            <input class="form-control" name="organisation" type="text" placeholder="@lang('eventmie-pro::em.brand_identity')" value="{{$user->organisation}}">

                                            @if ($errors->has('organisation'))
                                                <div class="error">{{ $errors->first('organisation') }}</div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="lgx-btn"><i class="fas fa-sd-card"></i> @lang('eventmie-pro::em.submit')</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ eventmie_asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ eventmie_asset('js/bootstrap.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
    {{-- CUSTOM --}}
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('#show_mymodal_close').click(function(){
                $('#myModal1').hide()
                $('#myModal1').removeClass('in')
            });
            $('#show_modal').click(function(){
                $('#myModal1').show()
                $('#myModal1').addClass('in')
            })
            $('#avatar').change(function() {

                let reader = new FileReader();

                reader.onload = (e) => {

                    $('#preview-image-before-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);

            });

            $('#seller_signature').change(function() {

                let reader = new FileReader();

                reader.onload = (e) => {

                    $('#preview-image-signature').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);

            });

        });
    </script>
    {{-- CUSTOM --}}
@stop
