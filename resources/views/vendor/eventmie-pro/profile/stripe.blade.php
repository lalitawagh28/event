@extends('eventmie::layouts.app')

<link rel="stylesheet" href="{{ asset('css/profile.css') }}" />
@section('content')

<div class="profile_main">

    <div class="col-xl-4">
        <!-- Profile picture card-->
        <div class="card mb-4 mb-xl-0">
            <div class="card"> <img class="card-img-top" src="https://i.imgur.com/K7A78We.jpg" alt="Card image cap">
                <div class="card-body little-profile text-center">
                    <div class="pro-img"><img  alt="user" @if(!is_null($user->avatar)) src="{{ asset('storage/'.$user->avatar)}}" @else src="https://i.imgur.com/8RKXAIV.jpg" @endif  id="preview-image-before-upload"></div>
                    <div class="card_info">
                        <h3 class="card_name">{{$user->name}}</h3>
                        <span class="card_mail">{{$user->email}}</span>
                    </div>

                </div>
            </div>
        </div>
        <div class="profile_stripe"><a href="{{ route('eventmie.profile') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Details</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>

        </div>
        <div class="profile_support"><a href="{{ route('eventmie.profile-support') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Support</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-support') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_organizer"><a href="{{ route('eventmie.profile-organizer') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Organizer</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-organizer') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_bank"><a href="{{ route('eventmie.profile-bank') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Bank</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-bank') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_seller"><a href="{{ route('eventmie.profile-seller') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Profile Seller</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-seller') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_setting"><a href="{{ route('eventmie.profile-stripe') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text">Stripe</h4>
            <a class="profile_icon" href="{{ route('eventmie.profile-stripe') }}"><i class="fa fa-angle-right"
                    aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_mailchimp"><a href="{{ route('eventmie.profile-mailchimp') }}">
            <img src="{{ asset('images/icon_profile.png') }}">
            <h4 class="profile_text1">Mail Chimp</h4>
            <a class="profile_icon1" href="{{ route('eventmie.profile-mailchimp') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>
        </div>
        <div class="profile_resetpin"><a href="{{ route('eventmie.resetPin')}}"  >
            <img src="{{ asset('images/reset_icon.png') }}">
            <h4 class="profile_text1">Reset Pin</h4>
            <a class="profile_icon1" href="{{ route('eventmie.resetPin')}}"  ><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
        <div class="profile_logout"><a href="{{ route('eventmie.logout') }}">
            <img src="{{ asset('images/logout_icon.png') }}">
            <h4 class="profile_text2">Logout</h4>
            <a class="profile_icon2" href="{{ route('eventmie.logout') }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></a>

        </div>
    </div>
    <div class="col-xl-5">
        <div class="profile_form">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (session('status'))

                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
            <form class="form-horizontal" action="{{ route('eventmie.updateAuthUser')}}" method="post" enctype="multipart/form-data">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="name" value="{{ $user->name }}">
                <input type="hidden" name="email" value="{{ $user->email }}">
                <input type="hidden" name="phone" value="{{ $user->phone }}">
                <input type="hidden" name="stripe_data" value="1">
                <input type="hidden" name="route_name" value="{{ url()->full() }}">

                <p class="profile_form_title">Stripe Details</p>
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.stripe_account') @lang('eventmie-pro::em.country')</label>
                    <select name="country" class="form-control" id="country">
                        <option value="AU" {{$user->country == 'AU' ?  'selected' : ''}} >Australia</option>
                        <option value="AT" {{$user->country == 'AT' ?  'selected' : ''}} >Austria</option>
                        <option value="BE" {{$user->country == 'BE' ?  'selected' : ''}} >Belgium</option>
                        <option value="BR" {{$user->country == 'BR' ?  'selected' : ''}} >Brazil</option>
                        <option value="BG" {{$user->country == 'BG' ?  'selected' : ''}} >Bulgaria</option>
                        <option value="CA" {{$user->country == 'CA' ?  'selected' : ''}} >Canada</option>
                        <option value="CY" {{$user->country == 'CY' ?  'selected' : ''}} >Cyprus</option>
                        <option value="CZ" {{$user->country == 'CZ' ?  'selected' : ''}} >Czech Republic</option>
                        <option value="DK" {{$user->country == 'DK' ?  'selected' : ''}} >Denmark</option>
                        <option value="EE" {{$user->country == 'EE' ?  'selected' : ''}} >Estonia</option>
                        <option value="FI" {{$user->country == 'FI' ?  'selected' : ''}} >Finland</option>
                        <option value="FR" {{$user->country == 'FR' ?  'selected' : ''}} >France</option>
                        <option value="DE" {{$user->country == 'DE' ?  'selected' : ''}} >Germany</option>
                        <option value="GR" {{$user->country == 'GR' ?  'selected' : ''}} >Greece</option>
                        <option value="HK" {{$user->country == 'HR' ?  'selected' : ''}} >Hong Kong</option>
                        <option value="HU" {{$user->country == 'HU' ?  'selected' : ''}} >Hungary</option>
                        <option value="IN" {{$user->country == 'IN' ?  'selected' : ''}} >India</option>
                        <option value="IE" {{$user->country == 'IE' ?  'selected' : ''}} >Ireland</option>
                        <option value="IT" {{$user->country == 'IT' ?  'selected' : ''}} >Italy</option>
                        <option value="JP" {{$user->country == 'JP' ?  'selected' : ''}} >Japan</option>
                        <option value="LV" {{$user->country == 'LV' ?  'selected' : ''}} >Latvia</option>
                        <option value="LT" {{$user->country == 'LT' ?  'selected' : ''}} >Lithuania</option>
                        <option value="LU" {{$user->country == 'LU' ?  'selected' : ''}} >Luxembourg</option>
                        <option value="MY" {{$user->country == 'MV' ?  'selected' : ''}} >Malaysia</option>
                        <option value="MT" {{$user->country == 'MT' ?  'selected' : ''}} >Malta</option>
                        <option value="MX" {{$user->country == 'MX' ?  'selected' : ''}} >Mexico</option>
                        <option value="NL" {{$user->country == 'NL' ?  'selected' : ''}} >Netherlands</option>
                        <option value="NZ" {{$user->country == 'NZ' ?  'selected' : ''}} >New Zealand</option>
                        <option value="NO" {{$user->country == 'NO' ?  'selected' : ''}} >Norway</option>
                        <option value="PL" {{$user->country == 'PL' ?  'selected' : ''}} >Poland</option>
                        <option value="PT" {{$user->country == 'PT' ?  'selected' : ''}} >Portugal</option>
                        <option value="RO" {{$user->country == 'RO' ?  'selected' : ''}} >Romania</option>
                        <option value="SG" {{$user->country == 'SG' ?  'selected' : ''}} >Singapore</option>
                        <option value="SK" {{$user->country == 'SK' ?  'selected' : ''}} >Slovakia</option>
                        <option value="SI" {{$user->country == 'SI' ?  'selected' : ''}} >Slovenia</option>
                        <option value="ES" {{$user->country == 'ES' ?  'selected' : ''}} >Spain</option>
                        <option value="SE" {{$user->country == 'SE' ?  'selected' : ''}} >Sweden</option>
                        <option value="CH" {{$user->country == 'CH' ?  'selected' : ''}} >Switzerland</option>
                        <option value="AE" {{$user->country == 'AE' ?  'selected' : ''}} >United Arab Emirates</option>
                        <option value="GB" {{$user->country == 'GB' ?  'selected' : ''}} >United Kingdom</option>
                        <option value="US" {{$user->country == 'US' ?  'selected' : ''}} >United States</option>
                        <option value="GI" {{$user->country == 'GI' ?  'selected' : ''}} >Gibraltar</option>
                        <option value="ID" {{$user->country == 'ID' ?  'selected' : ''}} >Indonesia</option>
                        <option value="LI" {{$user->country == 'LI' ?  'selected' : ''}} >Liechtenstein</option>
                        <option value="PH" {{$user->country == 'PH' ?  'selected' : ''}} >Philippines</option>
                        <option value="TH" {{$user->country == 'TH' ?  'selected' : ''}} >Thailand</option>
                    </select>
                </div>
                @if(empty(\Auth::user()->stripe_account_id))
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.stripe_account')</label>
                    <button type="button" id="stripe_button" class="lgx-btn lgx-btn-black btn-lg"><i class="fab fa-stripe"></i> @lang('eventmie-pro::em.connect_stripe')</button>
                </div>
                @else
                <div class="profile_info_user">
                    <label for="input-label" class="ti-form-label">@lang('eventmie-pro::em.stripe_account')</label>
                    <button type="button" class="btn btn-success btn-lg disabled" disabled><i class="fab fa-stripe"></i> Stripe Connected <i class="fas fa-check-circle"></i></button>
                    <p class="help-block">NOTE: Your Stripe account must have at least one of the following capabilities enabled: <strong>transfers</strong> or <strong>legacy_payments</strong> to start receiving payouts. <a target="_blank" href="https://stripe.com/docs/connect/required-verification-information#US-full-individual--card_payments|transfers">Visit here for more info</a></p>
                </div>
                @endif
                <div class="save_button">
                    <button class="btn lgx-btn w-60" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection

@section('javascript')

<script type="text/javascript">

    function submitForm(e) {
        e.preventDefault();
        console.log(e);
    }
    $(document).ready(function (e) {

        $('#stripe_button').click(function(){
            var country = document.getElementById("country").value;
            console.log(country);

            window.location.href = route('connect_stripe', {'country' : country});

        });




    });
</script>
{{-- CUSTOM --}}
@stop
