@extends('layouts.admin')
@section('content')
    <!--top registration section-->
    <!--Membership registration UI-->
    <div class="intro-y grid grid-cols-12 gap-0 mt-5">
        <div class="intro-y col-span-12 md:col-span-12 text-center py-3">
            <h2 class="text-lg font-medium text-3xl py-4">{{ __('admin.signup') }} </h2>
        </div>
    </div>
    <div class="intro-y flex flex-row justify-center mt-5 space-x-24 mr-5 ml-5">
        <!-- BEGIN: Blog Layout -->
        <div class="mb-2 basis-1/4 sm:basis-full">
            <div
                class="h-[150px] before:block before:absolute before:w-full before:h-full before:top-0 before:left-0 before:z-10 text-center before:from-black/90 before:to-black/10 image-fit">
                <a href="{{ route('agent.register.personal_detail') }}" class="block font-medium text-xl mt-3 relative z-10">
                    <img alt="" class="w-full" src=" /dist/img/signup-img1.png">
                    <div class="absolute bottom-0 text-white px-5 pb-6 w-full"> {{ __('admin.register_agent') }} 
                    </div>
                </a>
            </div>
        </div>
        <div class="mb-2 basis-1/4 sm:basis-full">
            <div
                class="h-[150px] before:block before:absolute before:w-full before:h-full before:top-0 before:left-0 before:z-10 text-center before:from-black/90 before:to-black/10 image-fit">
                <a href="{{ route('admin.register.personal_detail') }}" class="block font-medium text-xl mt-3 relative z-10">
                    <img alt="" class="w-full" src=" /dist/img/signup-img2.png">
                    <div class="absolute bottom-0 text-white px-5 pb-6 w-full">{{ __('admin.register_organizer') }}
                    </div>
                </a>
            </div>
        </div>
        <!--Membership registration UI-->
    </div>
@endsection
