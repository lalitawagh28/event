@extends('eventmie::layouts.app')
<style>


    .deposit_money_button{
        display: flex;
        gap: 20px;
        justify-content: end;
        margin-top: 24px;
    }
    .deposit_money_button3{
        border-radius: 5px;
        border: 2px solid #FF5C02;
        color: #FF5C02;
        padding: 5 24px;

    }
    .deposit_money_button4{
        background-color: #FF5C02;
        color: white;
        padding: 6 24px;
        border-radius: 5px;
    }
    .profile_forms .card {
    border-radius: 14px;
}


    .profile_info_user {
        margin-top: 10px;
    }
    .deposit_card_text{
        margin-left:13px;
        margin-bottom: 20px;
       color: #000;
        font-size: 13px;
        font-weight: 500;
    }
    .card_details{
        display: flex;
        justify-content: space-between;
       align-items: center;

    }
    .card_info input{
        border: none;
    }
    .header_payment{
        display: flex;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid #333;

    }
    .success_info{
        text-align: center;
    }
    .success_info p{
        font-size: 15px;
        font-weight: 600;
        line-height: 30px;
    }


</style>


@section('content')


<div class="row">
    <div class="col-md-4 m-auto">

        <div class="profile_forms clearfix">
            <div class="card my-5 p-4">
            <div class="card-header">
                <div class="header_payment">
                    <h3 class="profile_form_title border-b pb-2">Deposits</h3>
                    <a><i class="fa fa-times" aria-hidden="true" style="color: #000"></i></a>
                </div>
            </div>
            <div class="card-body">
               <div class="success_info">
                  <img src="{{ asset('images/success_img.png') }}" width="100px">
                  <p class="success_title">Success</p>
                  <p>Deposit Completed Successfully</p>
                  <p>Transcation Amount $21</p>
                  <p>Fee: $1.00 + $ 0.81(Stripe Fee)= $1.81</p>
                  <p>Deposit Amount $1.81</p>
               </div>
               <div class="col-md-12 text-center mt-3 ">
                <div class="my-5 ">
                    <a class="deposit_money_button3 mr-2">Print</a>
                    <a class="deposit_money_button4">Deposit Money Again</a>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>

</div>

@endsection
@section('javascript')
@stop
