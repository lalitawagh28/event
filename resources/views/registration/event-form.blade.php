<html lang="en" class="light">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}" />
    {{-- <link rel="stylesheet" href="/dist/css/support.css" /> --}}
    <!-- END: CSS Assets-->
</head>

<body>
    <!--top registration section-->
    <div class="grid grid-cols-12 gap-4 mt-4">
        <div
            class="col-span-12 md:col-span-12 lg:col-span-12 xxl:col-span-12 h-full flex flex-col sm:flex-row dark:border-dark-5">
            <div class="flex items-center w-full">
                <a href="{{ config('app.url') }}" class="-intro-x flex items-center pt-0 mr-auto">
                    <img alt="" class="w-40" src=" /dist/img/logo.png">
                </a>

            </div>
        </div>
    </div>
    <!--top registration section-->
    <!--Membership registration UI-->
    <div class="intro-y grid grid-cols-12 gap-6 mt-5">
        <div class="col-span-12 lg:col-span-12 md:col-span-12">
            <div class="box pb-3">
                <div class="btn-primary flex mb-6 p-3 rounded-md" role="alert">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item text-dark"><a href="#">
                                <i data-lucide="home" class="dark:text-slate-500"></i>
                            </a></li>
                        <li class="breadcrumb-item text-dark" aria-current="page">My Event</li>
                        <li class="breadcrumb-item text-dark" aria-current="page">Manage</li>
                    </ol>
                </div>

                <div class="px-10 py-2">
                    <form>
                        <div class="mb-4">
                            <label for="regular-form-1" class="form-label">Categories</label>
                            <div class="sm:w-full">
                                <select data-search="true" id="eventCategories" class="tom-select form-control">
                                    <option value="1" selected="selected">Sports</option>
                                    <option value="2">Music</option>
                                    <option value="3">Entertiment</option>
                                    <option value="4">Seminar</option>
                                </select>
                            </div>
                        </div>

                        <div class="mb-4">
                            <label for="eventName" class="form-label">Event Name</label>
                            <input id="eventName" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="mb-4">
                            <label for="eventID" class="form-label">Event ID</label>
                            <input id="eventID" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="mb-4">
                            <label for="eventURL" class="form-label">Event URL</label>
                            <input id="eventURL" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="mb-4">
                            <label for="regular-form-1" class="form-label">Event Date/Timings</label>
                            <div class="relative">
                                <div
                                    class="absolute rounded-l right-0 w-10 h-full flex items-center justify-center bg-slate-100 border text-slate-500 dark:bg-darkmode-700 dark:border-darkmode-800 dark:text-slate-400">
                                    <i data-lucide="calendar" class="w-4 h-4"></i>
                                </div> <input type="text" class="datepicker form-control pl-3"
                                    data-single-mode="true">
                            </div>
                        </div>
                        <div class="mb-4">
                            <label for="eventLocation" class="form-label">Event Location</label>
                            <input id="eventLocation" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="mb-4">
                            <label for="totalCapacity" class="form-label">Total Capacity</label>
                            <input id="totalCapacity" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="mb-4">
                            <label for="totalTickets" class="form-label">Total Tickets</label>
                            <input id="totalTickets" type="text" class="form-control" placeholder="">
                        </div>
                        <div class="mb-4">
                            <label for="ticketsLot" class="form-label">Tickets Lot</label>
                            <input id="ticketsLot" type="text" class="form-control" placeholder="">
                        </div>
                    </form>

                    <div class="flex justify-center text-center mt-3">
                        <div class="flex justify-center text-center mt-0">
                            <div class="form-inline float-right mb-3">
                                <a href="#" class="btn btn-elevated-primary w-32 mr-1">
                                    Submit</a>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>






    <!-- BEGIN: JS Assets-->
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=[" your-google-map-api"]&libraries=places"></script>
    <script src="{{ asset('dist/js/app.js') }}"></script>
    <!-- END: JS Assets-->




</body>

</html>
