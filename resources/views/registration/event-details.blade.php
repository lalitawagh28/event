@extends('eventmie::layouts.app')

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
    integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script> <!-- Owl Carousel JS -->

<style>
    .modal-open .modal {
        height: 400px;
        margin-left: 459px;
        margin-top: 100px
    }

    .modal_icon {
        text-align: right;
        gap: 2px;
    }
</style>


@section('content')
    {{-- <div class="banner">
  <div class="banner_img">
      <div class="overlay_div">
           <div class="ban_info">
                  <div class="share_icon"><i class="fa fa-share-alt" style="font-size:28px;color:rgb(51, 255, 0)" data-toggle="modal" data-target="#exampleModalCenter"></i></div>
                  <!-- Modal -->
                        <div class="modal fade share_modal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title text-black" id="exampleModalCenterTitle">Share</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                      <div class="modal_icon">
                                         <img src="{{ asset('images/whattsup_icon.png') }}">
                                         <img src="{{ asset('images/twitter_icon.png') }}">
                                         <img src="{{ asset('images/facebook_icon.png') }}">
                                         <img src="{{ asset('images/linkdin_icon.png') }}">
                                         <img src="{{ asset('images/insta_icon.png') }}">
                                      </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="ticket-details">
                            <p class="ticket_info"><span class="label_width">Event Name :</span><span class="input">Once Upon a time<span></p>
                            <p class="ticket_info"><span class="label_width">Event Type :</span><span class="input">Musical Concert<span></p>
                            <p class="ticket_info"><span class="label_width">Guest Star :</span><span class="input">Anirudh<span></p>
                            <p class="ticket_info"><span class="label_width">Date :</span><span class="input">7th Nov 2023 (2days)<span></p>
                            <p class="ticket_info"><span class="label_width">Time :</span><span class="input">04:00 PM onwards<span></p>
                            <p class="ticket_info"><span class="label_width">Venue :</span><span class="input">Bhartiya City, London<span></p>
                        </div>
                        <div class="ban_buttons">
                            <div class="date_btn">
                            <a href="#" class="event_btn btn mr-md-auto">Feb 23rd 2023</a>
                            <a href="#" class="event_btn btn mr-md-auto">Book Tickets</a>
                        </div>

                 </div>
           </div>
    </div>
  </div>
</div> --}}
    <div class="banner">
        <div class="row">
            <div class="banner_img col col-lg-8">
            </div>
            <div class="col col-lg-4">
                <div class="ban_info">
                    <div class="share_icon"><i class="fa fa-share-alt" style="font-size:28px;color:rgb(51, 255, 0)"></i>
                    </div>
                    <div class="ticket-details">
                        <p class="ticket_info"><span class="label_width">Event Name :</span><span class="input">Once Upon a
                                time<span></p>
                        <p class="ticket_info"><span class="label_width">Event Type :</span><span class="input">Musical
                                Concert<span></p>
                        <p class="ticket_info"><span class="label_width">Guest Star :</span><span
                                class="input">Anirudh<span></p>
                        <p class="ticket_info"><span class="label_width">Date :</span><span class="input">7th Nov 2023
                                (2days)<span></p>
                        <p class="ticket_info"><span class="label_width">Time :</span><span class="input">04:00 PM
                                onwards<span></p>
                        <p class="ticket_info"><span class="label_width">Venue :</span><span class="input">Bhartiya City,
                                London<span></p>
                    </div>
                    <div class="ban_buttons">
                        <div class="date_btn">
                            <a href="#" class="event_btn btn mr-md-auto">Feb 23rd 2023</a>
                            <a href="#" class="event_btn btn mr-md-auto">Book Tickets</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab">
        <div class="wrapper">
            <div class="tabs-wrapper">

                <ul id="scroller" class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Events Details</a></li>
                    <li><a data-toggle="tab" href="#menu1">Terms & Conditions</a></li>
                    <li><a data-toggle="tab" href="#menu2">Get Directions</a></li>
                </ul>

            </div>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="tab_info">
                        <p>An event designed with the same spirit of Vh1 Supersonic and coming to London for the very first
                            time, we hope Vh1 Supersonic Main Stage is one of the most thrilling experiences for all you
                            music lovers, of all genres. An event designed with the same spirit of Vh1 Supersonic and coming
                            to London for the very first time, we hope Vh1 Supersonic Main Stage is one of the most
                            thrilling experiences for all you music lovers, of all genres. An event designed with the same
                            spirit of Vh1 Supersonic and coming to London for the very first time, we hope Vh1 Supersonic
                            Main Stage is one of the most thrilling experiences for all you music lovers, of all genres.</p>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="tab_info">
                        <ul class="bullet">
                            <li>Above 18+ are allowed</li>
                            <li>Every Attended should bring their personal ID cards</li>
                            <li>For your own saftey,Wear face mask</li>
                        </ul>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <div class="tab3_info">
                        <div class="map">
                            <iframe
                                src=
                    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3506.2233913121413!2d77.4051603706222!3d28.50292593193056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce626851f7009%3A0x621185133cfd1ad1!2sGeeksforGeeks!5e0!3m2!1sen!2sin!4v1585040658255!5m2!1sen!2sin"
                                width="400" height="200" frameborder="0" style="border:0;" allowfullscreen=""
                                aria-hidden="false" tabindex="0">
                            </iframe>
                        </div>
                        <div class="map_info">
                            Bhartiya City,London United Kingdom
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="bbb_viewed">
        <div class="">
            <div class="row">
                <div class="col">
                    <div class="bbb_main_container">
                        <div class="bbb_viewed_title_container">
                            <h3 class="bbb_viewed_title">Recommended Events</h3>
                            <div class="bbb_viewed_nav_container">
                                <a class="seeall">See all ></a>
                            </div>
                        </div>
                        <div class="bbb_viewed_slider_container">
                            <div class="owl-carousel owl-theme bbb_viewed_slider">
                                <div class="owl-item">
                                    <div
                                        class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                        <div class="bbb_viewed_image border"><img src="{{ asset('images/img1.png') }}"
                                                class="bbb_viewed_imagees" alt=""></div>
                                        <div class="bbb_viewed_content ">
                                            <div class="event_date">
                                                <img src="{{ asset('images/calender_icon.png') }}" class="calender_icon ">
                                                <span class="date">03 Nov 2023</span>
                                            </div>
                                            <div class="bbb_viewed_name"><a href="#" class="bbb_viewed_name">Alkatel
                                                    Phone</a></div>
                                            <span class="bbb_viewed_smallname">concrete</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div
                                        class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                        <div class="bbb_viewed_image border"><img src="{{ asset('images/img2.png') }}"
                                                class="bbb_viewed_imagees" alt=""></div>
                                        <div class="bbb_viewed_content ">
                                            <div class="event_date">
                                                <img src="{{ asset('images/calender_icon.png') }}" class="calender_icon ">
                                                <span class="date">03 Nov 2023</span>
                                            </div>
                                            <div class="bbb_viewed_name"><a href="#"
                                                    class="bbb_viewed_name">Alkatel Phone</a></div>
                                            <span class="bbb_viewed_smallname">concrete</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div
                                        class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                        <div class="bbb_viewed_image border"><img src="{{ asset('images/img3.png') }}"
                                                class="bbb_viewed_imagees" alt=""></div>
                                        <div class="bbb_viewed_content ">
                                            <div class="event_date">
                                                <img src="{{ asset('images/calender_icon.png') }}" class="calender_icon ">
                                                <span class="date">03 Nov 2023</span>
                                            </div>
                                            <div class="bbb_viewed_name"><a href="#"
                                                    class="bbb_viewed_name">Alkatel Phone</a></div>
                                            <span class="bbb_viewed_smallname">concrete</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div
                                        class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                        <div class="bbb_viewed_image border"><img src="{{ asset('images/img4.png') }}"
                                                class="bbb_viewed_imagees" alt=""></div>
                                        <div class="bbb_viewed_content ">
                                            <div class="event_date">
                                                <img src="{{ asset('images/calender_icon.png') }}" class="calender_icon ">
                                                <span class="date">03 Nov 2023</span>
                                            </div>
                                            <div class="bbb_viewed_name"><a href="#"
                                                    class="bbb_viewed_name">Alkatel Phone</a></div>
                                            <span class="bbb_viewed_smallname">concrete</span>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> <!-- Load jQuery before other scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script> <!-- Owl Carousel JS -->
    <script>
        $(document).ready(function() {
            // Owl Carousel initialization
            $('.bbb_viewed_slider').owlCarousel({
                loop: true,
                margin: 10,
                autoplay: true,
                autoWidth: true,
                autoplayTimeout: 6000,
                nav: false,
                dots: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    575: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    991: {
                        items: 4
                    },
                    1199: {
                        items: 4
                    }
                }
            });

            $('.bbb_viewed_prev').on('click', function() {
                $('.bbb_viewed_slider').trigger('prev.owl.carousel');
            });

            $('.bbb_viewed_next').on('click', function() {
                $('.bbb_viewed_slider').trigger('next.owl.carousel');
            });
        });
    </script>

@stop
