@extends('eventmie::layouts.app')
<style>
    .events {
        display: flex;
    }

    .book {
        margin-left: 800px;
        margin-top: 60px;
        align-items: center;
        border-radius: 5px;
        background: #FF5C02;
        display: inline-flex;
        height: 52px;
        padding: 12px 24px;
        justify-content: center;
        align-items: center;
        text: white;
    }

    .ticket {
        color: #F6F6F6
    }



    .main {
        padding: 5PX;
        margin: 40px;
        border-radius: 20px;
        background: #F6F6F6;
        box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
    }

    .hi {
        align-items: center;
        margin: 15px;
        margin-top: 50px;

    }

    .menu {
        display: flex;
        justify-content: space-between;
        margin-top: 30px;
    }

    .menu-heading {
        margin: 0;
        margin-left: 62px;
    }


    .fc {
        border: 2px solid #000;

    }

    .fc-day {

        padding: 5px;

    }


    .fc-toolbar-center {
        margin: 0;

    }

    .fc-header-toolbar {
        justify-content: space-between;

    }

    .listBtn {
        padding-left: 80px;
        padding-right: 80px;
        border-top-left-radius: 25px;
        border-bottom-left-radius: 25px;
        margin-left: 50px;
        background-color: #FF5C02;
        color: #F6F6F6;

        margin-right: 0;
        border: 0.2px solid black;
    }

    .calenderBtn {
        padding-left: 50px;
        padding-right: 50px;
        border-top-right-radius: 25px;
        border-bottom-right-radius: 25px;
        margin-right: 50px;
        margin-left: -5px;
        border: 0.2px solid black;

    }

    .eventlist_content {
        padding-right: 20px;
        display: flex;
        background-color: #F6F6F6;
        align-items: center;
        border-radius: 20px;
        background: #F6F6F6;
        box-shadow: 0px 0px 1px 0px rgba(0, 0, 0, 0.25);

    }

    .bookticket_btn {
        display: block;
        margin-left: auto;
        margin-right: 20px;
        border-radius: 5px;
        background: #FF5C02;
        height: 52px;
        padding: 12px 24px;
        justify-content: center;



    }

    .bookticket_btn a {
        color: #F6F6F6;
    }

    .hooo {
        display: flex;
        justify-content: space-between;
    }

    .event_content {
        justify-content: space-between;
        padding: 30px;
    }

    .music_event {
        color: #000;
        font-weight: 600;
        font-size: 25px;
    }

    .filter {
        padding-right: 60px;
        font-size: 30px;
        color: #000;

    }

    .modal.fade.right:not(.in) .modal-dialog {
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
    }

    .modal.right .modal-dialog {
        position: fixed;
        height: 100%;
        width: 100%;
        margin-right: 0;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
    }

    .modal-content {
        height: 100vh;
        box-sizing: border-box;
        margin-top: 0;
        width: 100%;
    }

    .kalyan .modal-dialog {
        padding: 0;
        margin: 0;
        border-radius: 0;
        border: 0;
        box-shadow: transparent;
        width: 100%;
        min-width: 450px;
        position: fixed;
        right: 0;
        height: 100%;
        max-width: 75rem;
        /* width: 500px !important; */
        left: auto;
    }

    .row {
        display: flex;
        justify-content: space-between
    }

    .ticket_count {
        width: 100%;

    }

    .row1 {
        padding: 10px;
    }

    .modal-body {
        display: flex;
        flex-direction: column;
        row-gap: 10px;
    }

    .buttons {
        display: flex;
        justify-content: center;
        column-gap: 20px;
        margin-bottom: 20px;
    }

    .resetButton,
    .continueButton {
        border: 1px solid #FF5C02;
        border-radius: 6px;
        padding: 0 20px;
    }

    .resetButton {
        color: #FF5C02;
        background-color: white;
    }

    .continueButton {
        color: white;
        background-color: #FF5C02;
    }

    .modal-content {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }

    .calander_thumb {
    position: relative;
    padding: 0;
}

.calander_thumb img {
    position: absolute;
    min-height: 135px;
}

.calander_thumb .calander-thumTitle {
    position: absolute;
    z-index: 1;
    color: #fff;
    width: 100%;
    word-wrap: break-word;
    flex-direction: column;
    padding: 8px;
    top: 0;
}

.calander_thumb .calander-thumTitle h3 {
    color: #fff;
    font-size: 16px;
    word-wrap: break-word;
    word-break: break-all;
    line-height: 1.3;
    margin-top: 10px;
}
body .fc-daygrid-event{white-space: inherit;}
.calander_thumb .calander-thumTitle p {
    color: #fff;
    font-size: 12px;
}
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.css">
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.js"></script>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                start: 'prev',
                center: 'title',
                end: 'next'
            },
            customButtons: {
                customTodayButton: {
                    text: 'Today',
                    click: function() {
                        calendar.gotoDate(new Date());
                    }
                }
            },
            events:
                [{
    title: 'Blue Event',
    start: '2023-11-14',
    description: 'Lorem ipsum lorem ipsum',
    class: 'blue main'
}],

eventContent: { html: '<div class="calander_thumb"><div class="calander-thumTitle"><h3>The Music Events of heights</h3><p>Fri, 1st Nov. 06:30 to 11:00 PM</p></div><img src="{{ asset('images/banner_img1.png') }}"></div>' },



        });
        calendar.render();
        $('.fc-prev-button, .fc-next-button').css('margin', '0 200px');
        $('.fc-day').css('border', '2px solid #000');
        $('.fc-daygrid ').css('border', '2px solid #000');
    });
</script>


@section('title')
    @lang('eventmie-pro::em.home')
@endsection
@section('content')
    <div class="menu">
        <div>
            <h3 class="menu-heading">Events list</h3>
        </div>
        <div class="side">
            <a id="listBtn" class="listBtn py-2" onclick="openList()"><i class="fa fa-list-ul" aria-hidden="true"></i>
                List</a>
            <a id="calenderBtn" class="calenderBtn py-2" onclick="openCalender()"><i class="fa fa-calendar"
                    aria-hidden="true"></i> Calendar</a>
            <a class="filter" style="cursor: pointer"><i class="fa fa-filter py-2" aria-hidden="true" data-toggle="modal"
                    data-target="#myModal"></i></a>
        </div>
        <div id="myModal" class="modal fade right kalyan in">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row1">
                            <!-- First row with the first two columns -->
                            <div class="col-md-6">
                                <p>Search Event</p>
                                <input type="text" class="form-input form-control"
                                    placeholder="Type Event Name/Venue/City">
                            </div>
                            <div class="col-md-6">
                                <p>Search Category</p>
                                <select class="ticket_count form-control">
                                    <option value="0">Select Event Category</option>
                                    <option value="1">Music Concert</option>
                                    <option value="2">Sports</option>
                                    <option value="3">Entertainment</option>
                                    <option value="4">Business</option>
                                </select>
                            </div>
                        </div>
                        <div class="row1">
                            <!-- First row with the first two columns -->
                            <div class="col-md-6">
                                <p>City</p>
                                <select class="ticket_count form-control">
                                    <option value="0">Select Your City</option>
                                    <option value="1">London</option>
                                    <option value="2">Uk</option>
                                    <option value="3">OxFord</option>
                                    <option value="4">Business</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <p>Price</p>
                                <select class="ticket_count form-control">
                                    <option value="0">Any Price</option>
                                    <option value="1">Free</option>
                                    <option value="2">Paid</option>
                                    <option value="3">Any</option>
                                    <option value="4">Business</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            <Span style="margin-left:20px">Date</Span>
                        </div>
                    </div>
                    <div class="buttons">
                        <button class="resetButton">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                            <span>Reset Filters</span>
                        </button>
                        <button class="continueButton">Continue</button>
                    </div>

                </div>
            </div>
        </div>

    </div>
    {{-- list start --}}
    <div id="List" class="tabcontent">
        <div class="eventlist_content">
            <div class="eventlist_img">
                <img src="{{ asset('images/event/club.png') }}" alt="userbanner-img" class="">
            </div>
            <div class="event_content">
                <p class="music_event">The Musical Events Of Heights</p>
                <span>Fri, 3rd Nov to 7th Nov at 06:30 to 11:00 pm</span></br>
                <span>Bharatiya City, London.</span>
            </div>
            <div class="bookticket_btn">
                <a>Book Tickets</a>
            </div>
        </div>
        <div class="eventlist_content">
            <div class="eventlist_img">
                <img src="{{ asset('images/event/2.png') }}" alt="userbanner-img" class="">
            </div>
            <div class="event_content">
                <p class="music_event">The Musical Events Of Heights</p>
                <span>Fri, 3rd Nov to 7th Nov at 06:30 to 11:00 pm</span></br>
                <span>Bharatiya City, London.</span>
            </div>
            <div class="bookticket_btn">
                <a>Book Tickets</a>
            </div>
        </div>
        <div class="eventlist_content">
            <div class="eventlist_img">
                <img src="{{ asset('images/event/3.png') }}" alt="userbanner-img" class="">
            </div>
            <div class="event_content">
                <p class="music_event">The Musical Events Of Heights</p>
                <span>Fri, 3rd Nov to 7th Nov at 06:30 to 11:00 pm</span></br>
                <span>Bharatiya City, London.</span>
            </div>
            <div class="bookticket_btn">
                <a>Book Tickets</a>
            </div>
        </div>
        <div class="eventlist_content">
            <div class="eventlist_img">
                <img src="{{ asset('images/event/4.png') }}" alt="userbanner-img" class="">
            </div>
            <div class="event_content">
                <p class="music_event">The Musical Events Of Heights</p>
                <span>Fri, 3rd Nov to 7th Nov at 06:30 to 11:00 pm</span></br>
                <span>Bharatiya City, London.</span>
            </div>
            <div class="bookticket_btn">
                <a>Book Tickets</a>
            </div>
        </div>
        <div class="eventlist_content">
            <div class="eventlist_img">
                <img src="{{ asset('images/event/5.png') }}" alt="userbanner-img" class="">
            </div>
            <div class="event_content">
                <p class="music_event">The Musical Events Of Heights</p>
                <span>Fri, 3rd Nov to 7th Nov at 06:30 to 11:00 pm</span></br>
                <span>Bharatiya City, London.</span>
            </div>
            <div class="bookticket_btn">
                <a>Book Tickets</a>
            </div>
        </div>
        <div class="eventlist_content">
            <div class="eventlist_img">
                <img src="{{ asset('images/event/4.png') }}" alt="userbanner-img" class="">
            </div>
            <div class="event_content">
                <p class="music_event">The Musical Events Of Heights</p>
                <span>Fri, 3rd Nov to 7th Nov at 06:30 to 11:00 pm</span></br>
                <span>Bharatiya City, London.</span>
            </div>
            <div class="bookticket_btn">
                <a>Book Tickets</a>
            </div>
        </div>
        <div class="eventlist_content">
            <div class="eventlist_img">
                <img src="{{ asset('images/event/club.png') }}" alt="userbanner-img" class="">
            </div>
            <div class="event_content">
                <p class="music_event">The Musical Events Of Heights</p>
                <span>Fri, 3rd Nov to 7th Nov at 06:30 to 11:00 pm</span></br>
                <span>Bharatiya City, London.</span>
            </div>
            <div class="bookticket_btn">
                <a>Book Tickets</a>
            </div>
        </div>



    </div>
    {{-- list end --}}
    {{-- calendar start --}}
    <div class="tabcontent" id="Paris" style="display:none;">
        <div id="calendar" style="padding:40px"></div>
    </div>
    {{-- <div id="calendar" style="padding:20px"></div> --}}

    {{-- calnedar end --}}

    <script>
        function openList() {
            var list = document.getElementById("List");
            var paris = document.getElementById("Paris");
            var listBtnEle = document.getElementById("listBtn");
            var calenderBtnEle = document.getElementById("calenderBtn");
            list.style.display = "block";
            paris.style.display = "none";
            listBtnEle.style.backgroundColor = "FF5C02";
            listBtnEle.style.color = "F6F6F6";
            listBtnEle.style.borderWidth = "0";
            calenderBtnEle.style.backgroundColor = "white";
            calenderBtnEle.style.color = "black";

            calenderBtnEle.style.borderWidth = "2px";
        }

        function openCalender() {
            var list = document.getElementById("List");
            var paris = document.getElementById("Paris");
            var calenderBtnEle = document.getElementById("calenderBtn");
            var listBtnEle = document.getElementById("listBtn");
            list.style.display = "none";
            paris.style.display = "block";
            calenderBtnEle.style.backgroundColor = "FF5C02";
            calenderBtnEle.style.color = "F6F6F6";
            calenderBtnEle.style.borderWidth = "0";
            listBtnEle.style.background = "white";
            listBtnEle.style.color = "black";
            listBtnEle.style.borderWidth = "2px";

        }
    </script>
@endsection
