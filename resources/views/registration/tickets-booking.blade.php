@extends('eventmie::layouts.app')
<style>
    .ticket_banner {
        position: relative;
        text-align: center;
    }

    .ticket_banner_img {
        position: relative;
        display: inline-block;
    }

    .ban_img {
        display: block;
        max-width: 100%;
        /* Ensure the image doesn't exceed its container */
    }

    .ticket_banner_content {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        color: white;
        /* Adjust the color as needed */
        z-index: 1;
    }

    .event_title {
        color: #FFF;
        font-family: Segoe UI;
        font-size: 35px;
        font-style: normal;
        font-weight: 600;
        line-height: normal;
    }

    .ticket_banner_img {
        background: rgba(8, 13, 64, 0.60);
        backdrop-filter: blur(2.5px);
    }

    .ban_img {
        background: rgba(8, 13, 64, 0.60);
        backdrop-filter: blur(2.5px);
    }

    .event_subtitle {
        color: #FFF;
        font-size: 15px;
        font-weight: 300;
    }

    .tickets_selection {
        display: flex;
        width: 90%;
        margin: 30px auto;
        gap: 20px;
    }

    .ticket_left_content {
        width: 60%;
        text-align: center;
    }

    .gold_ticket,
    .premium_ticket,
    .vip_ticket {
        display: flex;
    align-items: center;
    border-radius: 10px;
    background: #3D517F;
    padding: 12px 20px;
    margin-bottom: 10px;
    justify-content: space-between;
    }

    .ticket_type {
        margin-top: 30px;
        margin-bottom: 30px;
    }

    .ticket_price {
        background-color: #FFF;
        padding: 0px 15px;
        color: black;
        border-radius: 4px;
        padding-top: 5px;

    }

    .plan_type {
        color: #FFF;
    padding-left: 10px;
    width: 100px;
    font-size: 28px;
    text-align: start;
    margin: 0;
    font-weight: normal;
    padding: 4px 0;
    }

    .number_of_tickets {
        padding-right: 10px;
    }

    .ticket_count {
        width: 50px;
        text-align: center;
    }

    .plan_img {
        width: 60%;
        margin-bottom: 20px;
    }

    .ticket_form {
        flex-shrink: 0;
        border-radius: 24px;
        border: 1px solid #959595;
        margin-top: 0px;
        overflow: hidden;
    }

    .ticket_details {
        align-items: center;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-left: 30px;
        background: #3D517F;

    }

    .search_by_postodee,
    .adress_dropdown {
        width: 100%;
        height: 42px;
        padding: 5px;
        border-radius: 5px;
        border: 1px solid #000;
        margin-top: 10px;
    }

    .ticket_form_info {
        padding: 15px;
        border-bottom: 1px solid #BFBFBF;
    }

    .details_info {
        display: flex;
        justify-content: space-between;
        margin-bottom: 17px;
    }

    .ticket_price_details {
        padding: 15px;
    }

    .apply {
        background-color: #FF5C02;
        padding: 3px 9px;
        border: 2px solid #FF5C02;
        color: #FFF;
    }

    .details_info1 {
        display: flex;
        justify-content: space-between;
        border-top: 1px solid #BFBFBF;
        border-bottom: 1px solid #BFBFBF;
        padding: 15px;
    }

    .payments {
        padding: 15px;
        border-bottom: 1px solid #BFBFBF;
    }

    .payment_type {
        margin-bottom: 10px;
        margin-top: 5px;
        display: flex;
        align-items: center;
    }

    .addmoney {
        background-color: #FF5C02;
        padding: 5px 10px;
        margin-left: 10px;
        border-radius: 5px;
        color: #FFF;

    }

    .tickets_buttons {
        display: flex;
        padding-top: 20px;
        padding-bottom: 20px;
        gap: 30px;
        text-align: center;
        justify-content: center;
    }

    .btn_1 {
        padding: 5px 10px;
        color: #FF5C02;
        border-radius: 5px;
        border: 2px solid #FF5C02;
    }

    .btn_2 {
        background-color: #FF5C02;
        padding: 5px 12px;
        border-radius: 5px;
        color: #FFF;

    }

    .ticket_right_content {
        margin-bottom: 20px;
        width: 40%;
    }

    .head_text_info {

        font-weight: 600;
        color: #000;
        padding-right: 20px;
        margin-top: 4px;
        font-size: 14px;
    }

    .ticket_label {
        color: #000;
        font-size: 14px;
        font-weight: 500;
    }

    .ticket_label1 {
        color: #000;
        font-size: 19px;
        font-weight: 500;
    }

    .price_input {
        font-size: 16px;
        font-weight: 500;
        color: #000;
    }

    .price_input1 {
        font-size: 20px;
        font-weight: 500;
        color: #000;
    }

    .radio_label {
        margin-top: 2px;
    }

    input[type=checkbox],
    input[type=radio] {
        margin: 4px 0 0;
        accent-color: #ff5c02;
        margin: 0 10px 0 0 !important;
        width: 19px;
        height: 19px
    }

    .attende_details {
        border-radius: 10px;
        border: 1px solid #9A9A9A;
        flex-shrink: 0;
        padding: 20px;
        margin-bottom: 15px;
    }

    .attende_title {
        border-radius: 7px;
        background: #CDCDCD;
        padding: 10px;
        color: #000;
        padding-left: 14px;
        text-align: start;
        font-size: 16px;
        font-weight: 500;
    }

    .attendee_form {
        padding-top: 20px;
    }

    .row {
        padding-bottom: 15px;
    }

    .form-input {
        stroke-width: 1px;
        stroke: #707070;
        border-radius: 8px;

        border: 2px solid #e4e4e4;

        box-shadow: none;
        color: #151414;
        display: block;
        font-size: 14px;
        height: 44px;

        padding: 1rem 2rem;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        width: 100%;
    }

    .attendee_checkbox {
        text-align: start;

    }

    .ticket_deliverd_type {
        display: flex;
    }

    .relative {
        position: relative;
    }

    .search_box {
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        margin-right: 10px;
        padding: 10px;
    }

    .form-input {
        padding: 1rem 3rem;
        align-items: center;
    }

    .search_box1 {
        position: absolute;
        top: 67%;
        transform: translateY(-50%);
        margin-right: 10px;
        padding: 10px;
    }

    .search_by_postodee1 {
        width: 100%;
        height: 42px;
        padding: 5px;
        border-radius: 5px;
        border: 1px solid #000;
        margin-top: 10px;
        padding-left: 33px;
    }

    .ticket_type_select {
        display: flex;
        align-items: center;
    }

    .walletBalance strong {
        color: #ff5c02;
    }

    .walletBalance {
        padding-left: 30px;
    }

    .condition-wrap label {
        font-size: 13px;
        font-weight: normal;
        line-height: 1.3;
    }

    .condition-wrap .custom-control {
        display: flex;
        align-items: start;
    }

    .condition-wrap {
        padding: 15px;
    }
    .plan_types_right{
        display: flex;
        justify-content: space-between;
    }
    .plan_type_subtitle{
        font-size: 10px;
        padding-top: -10px;
        padding-left: 10px;
        text-align: start;
    }
    .plan_types_left{
        display: flex;
        align-items: center;
    }
    .plan_add{
        color: rgb(51, 215, 51);
        font-size: 20px;
        font-weight: 500;
    }
    .number_of_tickets,.sub_category{
        padding-right: 20px;

    }
    .sub_category_select,.ticket_count{
        padding: 8px 15px;
        background-color: #FFF;
        border-radius: 4px;
    }
    ..sub_category_select{
        padding: 5px 20px;
    }
    .plan_delete{
        margin-left: 21px;
        color: #FFF;
    }
</style>


@section('content')
    <div class="ticket_banner">
        <div class="ticket_banner_img">
            <img src="{{ asset('images/banner_img1.png') }}" class="ban_img">
        </div>
        <div class="ticket_banner_content">
            <h4 class="event_title">Once Upon a Time | Anirudh Ravichandran<h4>
                    <span class="event_subtitle"><i class="fa fa-music" aria-hidden="true"></i> Musical Concert | <i
                            class="fa fa-calendar" aria-hidden="true"></i> 3 Nov,2023 | <i class="fa fa-clock-o"
                            aria-hidden="true"></i> 09:30 PM GMT | <i class="fa fa-map-marker" aria-hidden="true"></i>
                        Bhartiya City, London</span>
        </div>
    </div>
    <div class="tickets_selection">


        <div class="ticket_left_content">
            <div class="ticket_img">
                <img src="{{ asset('images/image17.png') }}" class="plan_img">
            </div>
            <div class="ticket_type">
                <div class="gold_ticket">
                    <div class="plan_types_left">
                        <div class="plan_type1 mr-5">
                            <h4 class="plan_type">Gold</h4>
                            <p style="color: #FFF" class="plan_type_subtitle">(Processing Fee 2.50 GBP)</p>
                        </div>
                        <div class="plan_type1">
                            <h4><a class="plan_add">+Add</a></h4>
                        </div>
                        <div class="plan_type1">
                            <a class="plan_delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="plan_types_right">
                        <div class="sub_category">
                            <select class="sub_category_select" placeholder="Sub Category">
                                <option value="Sub Category">Sub Category</option>
                                <option value="A1">A1</option>
                                <option value="A2">A2</option>
                            </select>
                        </div>
                        <div class="number_of_tickets">
                            <select class="ticket_count">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <p class="ticket_price">32.00 GBP</p>
                    </div>
                </div>
                <div class="attende_details">
                    <div class="attende1">
                        <p class="attende_title">1Attendee
                        <p>
                        <div class="attendee_form">
                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class=" form-input" placeholder="Input 1">
                                    </div>
                                    <div class="col-md-6 ">
                                        <input type="text" class=" form-input" placeholder="Input 2">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class=" form-input" placeholder="Input 1">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class=" form-input" placeholder="Input 2">
                                    </div>
                                </div>
                                <div class="attendee_checkbox">
                                    <input type="checkbox" id="vegan" name="sameasabove" checked>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="attende1">
                        <p class="attende_title">2Attendee
                        <p>
                        <div class="attendee_form">
                            <form class="p-0 m-0">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class=" form-input" placeholder="Input 1">
                                    </div>
                                    <div class="col-md-6 ">
                                        <input type="text" class=" form-input" placeholder="Input 2">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="relative">
                                            <input type="text" id="hs-leading-icon" name="hs-leading-icon"
                                                class="form-input pl-10 ti-form-input ltr:pl-11 rtl:pr-11 focus:z-10"
                                                placeholder="Search by Postcode">
                                            <div
                                                class="search_box absolute inset-y-0 ltr:left-0 rtl:right-0 flex items-center pointer-events-none z-20 ltr:pl-4 rtl:pr-4">
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class=" form-input" placeholder="Input 2">
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
                <div class="gold_ticket">
                    <div class="plan_types_left">
                        <div class="plan_type1 mr-5">
                            <h4 class="plan_type">Vip</h4>
                            <p style="color: #FFF" class="plan_type_subtitle">(Processing Fee 2.50 GBP)</p>
                        </div>
                        <div class="plan_type1">
                            <h4><a class="plan_add">+Add</a></h4>
                        </div>
                        <div class="plan_type1">
                            <a class="plan_delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="plan_types_right">
                        <div class="sub_category">
                            <select class="sub_category_select" placeholder="Sub Category">
                                <option value="Sub Category">Sub Category</option>
                                <option value="A1">A1</option>
                                <option value="A2">A2</option>
                            </select>
                        </div>
                        <div class="number_of_tickets">
                            <select class="ticket_count">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <p class="ticket_price">32.00 GBP</p>
                    </div>
                </div>
                <div class="gold_ticket">
                    <div class="plan_types_left">
                        <div class="plan_type1 mr-5">
                            <h4 class="plan_type">Premium</h4>
                            <p style="color: #FFF" class="plan_type_subtitle">(Processing Fee 2.50 GBP)</p>
                        </div>
                        <div class="plan_type1">
                            <h4><a class="plan_add">+Add</a></h4>
                        </div>
                        <div class="plan_type1">
                            <a class="plan_delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="plan_types_right">
                        <div class="sub_category">
                            <select class="sub_category_select" placeholder="Sub Category">
                                <option value="Sub Category">Sub Category</option>
                                <option value="A1">A1</option>
                                <option value="A2">A2</option>
                            </select>
                        </div>
                        <div class="number_of_tickets">
                            <select class="ticket_count">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <p class="ticket_price">32.00 GBP</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="ticket_right_content">
            <div class="ticket_form">
                <div class="ticket_details">
                    <h3 style="color:#FFF;">Ticket Details</h3>
                </div>
                <div class="ticket_form_info">
                    <div class="ticket_deliverd_type">
                        <div class="ticket_type_select">
                            <input type="radio" id="ticketbyadress" name="" value="">
                            <label for="" class="head_text_info">Ticket Delivery Address</label>
                        </div>
                        <div class="ticket_type_select">
                            <input type="radio" id="ticket_point" name="" value="">
                            <label for="" class="head_text_info">Ticket Collection Point</label>
                        </div>
                    </div>
                    <div>

                        <div class="relative">
                            <input type="text" id="hs-leading-icon" name="hs-leading-icon"
                                class="search_by_postodee1 pl-10 ti-form-input ltr:pl-11 rtl:pr-11 focus:z-10"
                                placeholder="Search by Postcode">
                            <div
                                class="search_box1 absolute inset-y-0 ltr:left-0 rtl:right-0 flex items-center pointer-events-none z-20 ltr:pl-4 rtl:pr-4">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                    <div>
                        <select class="adress_dropdown" placeholder="Select Your address">
                            <option value="1">Southanpton</option>
                            <option value="2">Bristol</option>
                            <option value="3">London</option>
                        </select>
                    </div>
                </div>
                <div class="ticket_price_details">
                    <div class="details_info">
                        <p class="ticket_label">Total No. of Tickets</p>
                        <p class="price_input">00</p>
                    </div>
                    <div class="details_info">
                        <p class="ticket_label">Total Amount</p>
                        <p class="price_input">00</p>
                    </div>
                    <div class="details_info">
                        <p class="ticket_label">Processing Fee</p>
                        <p class="price_input">00</p>
                    </div>
                    <div class="details_info">
                        <p class="ticket_label">Ticket Delivery Fee</p>
                        <p class="price_input">00</p>
                    </div>
                    <div class="details_info">
                        <p class="ticket_label">Have a Promo Code</p>
                        <div>
                            <input type="text" name="" id="promocode" placeholder="Enter promo code"><a
                                class="apply">Apply</a>
                        </div>
                    </div>
                </div>
                <div class="ticket_price_details1">
                    <div class="details_info1">
                        <p class="ticket_label1">Total Order</p>
                        <p class="price_input1">00</p>
                    </div>
                </div>
                <div class="payments">
                    <div class="payment_type">
                        <input type="radio" id="paybycard" name="" value="">
                        <label for="" class="radio_label">Pay by Card</label>
                    </div>
                    <div class="paymentmain">
                        <div class="payment_type">
                            <input type="radio" id="paybywallet" name="" value="">
                            <label for="" class="radio_label">Pay by Dhigna Wallet</label>
                            <a class="addmoney">Add Money</a>
                        </div>
                        <div class="walletBalance">Your wallet Balance <strong>32.50GBP</strong></div>
                    </div>
                    <div class="payment_type">
                        <input type="radio" id="paybycard" name="" value="">
                        <label for="" class="radio_label">Pay by Bank Account Transfer</label>
                    </div>
                </div>
                <div class="condition-wrap">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing1">
                        <label class="custom-control-label" for="customControlAutosizing">
                            The Order is Subject to Dhigna Events <a href="">Terms & Conditions</a> and <a
                                href="">Privacy Policy.</a>
                        </label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing2">
                        <label class="custom-control-label" for="customControlAutosizing">Amount is non-refundable and
                            non-transable</label>
                    </div>
                </div>
                <div class="tickets_buttons">
                    <a class="btn_1">cancel</a>
                    <a class="btn_2">Proceed to Pay</a>
                </div>


            </div>
        </div>
    </div>
@endsection
@section('javascript')
@stop
