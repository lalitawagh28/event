<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">


    <style>
        .main-table {
            margin: 20px;
        }

        table.table {
            border: none;
        }

        .table-header {
            background: #949191a2;
        }

        .table-body th,
        .table-body td {
            border-bottom: none;
        }

        .table-body {
            background-color: #D4D4D4;
        }

        .booking-btn {
            background: #FF5C02;
            padding: 5px 10px;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            display: inline-block;
            margin: 5px;
        }
    </style>
</head>

<body>
    <div class="main-table">
        <table class="table">
            <thead class="table-header">
                <tr>
                    <th scope="col">Order ID</th>
                    <th scope="col">Event</th>
                    <th scope="col">Total Tickets</th>
                    <th scope="col">Order Total</th>
                    <th scope="col">Dicounts</th>
                    <th scope="col">Booked On</th>
                    <th scope="col">Payment</th>
                    <th scope="col">Checked In</th>
                    <th scope="col">Status</th>
                    <th scope="col">Delivey Address</th>
                    <th scope="col">Expired</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody class="table-body">
                <tr>
                    <td>#876</td>
                    <td>Night Spirts<br />
                        Timings: 04 Nov,2023 02:02 AM-11:00 AM (IST)
                    </td>
                    <td>1</td>
                    <td>107.50GBP</td>
                    <td>0GBP</td>
                    <td>03 Nov 2023</td>
                    <td>Online</td>
                    <td>No</td>
                    <td>Enabled</td>
                    <td></td>
                    <td>No</td>
                    <td class="booking-btn">Bookings</td>
                </tr>
                
            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>

</body>

</html>
