@extends('eventmie::layouts.app')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
    integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('css/profile.css') }}" />

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
</script>

@section('content')


    <div class="profile_main ">



        <div class="col-xl-4">
            <!-- Profile picture card-->
            <div class="card mb-4 mb-xl-0">
                <div class="card"> <img class="card-img-top" src="https://i.imgur.com/K7A78We.jpg" alt="Card image cap">
                    <div class="card-body little-profile text-center">
                        <div class="pro-img"><img src="https://i.imgur.com/8RKXAIV.jpg" alt="user"></div>
                        <div class="card_info">
                            <h3 class="card_name">Brad Macullam</h3>
                            <span class="card_mail">amit@gmail.com<span>
                        </div>

                    </div>
                </div>
            </div>
            <div class="profile_setting">
                <img src="{{ asset('images/icon_profile.png') }}">
                <h4 class="profile_text">Profile Details</h4>
                <a class="profile_icon"><i class="fa fa-angle-right" aria-hidden="true"></i></a>

            </div>
            <div class="profile_resetpin">
                <img src="{{ asset('images/reset_icon.png') }}">
                <h4 class="profile_text1">Reset Pin</h4>
                <a class="profile_icon1"><i class="fa fa-angle-right" aria-hidden="true"></i></a>

            </div>
            <div class="profile_logout">
                <img src="{{ asset('images/logout_icon.png') }}">
                <h4 class="profile_text2">Logout</h4>
                <a class="profile_icon2"><i class="fa fa-angle-right" aria-hidden="true"></i></a>

            </div>
        </div>
        <div class="col-xl-5">
            <div class="profile_form">
                <form>

                    <p class="profile_form_title">Profile Details</p>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">Name</label>
                        <div class="input-container">
                            <input type="text" id="input-label" class="ti-form-input" placeholder="Naveen kumar">
                            <input type="text" id="second-input" class="ti-form-input" placeholder="Annabattula">
                        </div>

                    </div>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">Email</label>
                        <input type="email" id="second-input" class="ti-form-input" placeholder="naveen@gmail.com">
                    </div>
                    <div class="profile_info_user">
                        <label for="input-label" class="ti-form-label">Mobile Number</label>
                        <input type="text" id="second-input" class="ti-form-input" placeholder="Mobile number">
                    </div>
                    <div class="save_button">
                        <a class="profile_save">Save</a>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
@section('javascript')
@stop
