<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <title>Dhigna Events -faq-Mobile</title>
    <style>
        .faq_data{
            font-size: 14pt;
            line-height: 23px;
            font-family: Calibri, Calibri_EmbeddedFont, Calibri_MSFontService, sans-serif;
        }
        .faq_info{
            margin-bottom: 25px;
            color: black;
        }
        h5 {
            font-size: 1.1rem;
            font-weight: 700;
        }
    </style>
  </head>
  <body>
    <div class="container">
        <div class="faq_info">
             <h5>How can I register for the event? </h5>
             <p class="faq_data">In home page click on register and click on customer register complete the registration steps i.e.  Personal information, mobile verification, email verification then dashboard will display. </p>
        </div>
        <div class="faq_info">
            <h5>Is there a registration fee? </h5>
            <p class="faq_data">No! You can sign up for free </p>
        </div>
        <div class="faq_info">
            <h5>How can I reset my pin?</h5>
            <p class="faq_data">In login page click on forgot password and give valid email address, email notification sent to the given mail address in that mail reset password link will there click on that link and reset the pin</p>
        </div>
        <div class="faq_info">
            <h5>How can I scan my ticket?</h5>
            <p class="faq_data">You can check In attendees using your smartphone or tablet’s camera to scan the attendee’s QR codes on their tickets.</p>
        </div>
        <div class="faq_info">
            <h5>How do I download my event tickets? </h5>
            <p class="faq_data">When your tickets are ready to download, you will receive an email from us with the link to download them. After clicking on the link, you can view the tickets and print them or download and save them as a PDF. Please note that only one copy of the barcode will be accepted at the venue. </p>
        </div>
        <div class="faq_info">
            <h5>What Is the cancellation policy?</h5>
            <p class="faq_data">No! Once tickets are booked you can’t cancel the tickets  </p>
        </div>
        <div class="faq_info">
            <h5>If the transaction is unsuccessful and money is debited, how will I get my refund? </h5>
            <p class="faq_data">In the case of a transaction failure, the ticket amount gets auto-refunded to your original mode of payment within 5 - 7 business days. In case it doesn’t, you can reach out to our team at support@dhigna.com, and we will look into it.</p>
        </div>
        <div class="faq_info">
            <h5>How do I get my refund?</h5>
            <p class="faq_data">Unfortunately, tickets are non-refundable. </p>
        </div>
        <div class="faq_info">
            <h5>Is there parking available? </h5>
            <p class="faq_data">Yes</p>
        </div>
        <div class="faq_info">
            <h5>Can I bring a child to an event?</h5>
            <p class="faq_data">However, keep in mind that they will need their own ticket as most concerts don't allow kids in. It will be indicated on the event website and when purchasing tickets children under a specific age are welcome to attend events for free. A photo ID is often required to enter events that do have age limitations, which are indicated on the event website and when you finish your purchase. </p>
        </div>
    </div>
  </body>
</html>
