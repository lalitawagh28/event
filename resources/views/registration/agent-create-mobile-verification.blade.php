@extends('layouts.admin')
@section('content')
    <!--top registration section-->
    <!--Membership registration UI-->
    <div class="grid grid-cols-8 mt-8">
        <div class="p-5 rounded-l24 complete-step blog col-start-2 col-span-4 sm:col-span-12 md:col-span-12 md:col-start-2 lg:col-start-2 xl:col-start-2 lg:col-span-4 xl:col-span-4">
            <h1 class="text-2xl ml-3 font-bold"> {{ __('admin_register.agent_mobile_verification') }} </h1>
        </div>
    </div>
    <div class="grid grid-cols-8 gap-4 mt-8">
        <div class="h-full xl:block md:hidden lg:hidden"></div>
        <div class="p-5 rounded-l24 complete-step blog col-span-2 sm:col-span-12 md:col-span-12 lg:col-span-2 xl:col-span-2 color-lgray">
            <div class="chat px-3 py-2 m-0 text-gray-700">
                <div class="px-0 sm:px-0 step-bdr">
                    <div class="intro-x flex items-center complete-step block">
                        <button class="w-10 h-10 rounded-full btn btn-primary">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M5 13l4 4L19 7"></path>
                            </svg>
                        </button>
                        <div class="font-medium ml-3">{{ __('admin_register.personal_detail') }}</div>
                    </div>
                    <div class="intro-x flex items-center sm:mt-4 block">
                        <button class="w-10 h-10 rounded-full btn btn-primary">

                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M5 13l4 4L19 7"></path>
                            </svg>
                        </button>
                        <div class="font-medium ml-3">{{ __('admin_register.mobile_otp') }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="-ml-4 rounded-r24 blog col-span-4 sm:col-span-12 md:col-span-12 lg:col-span-4 xl:col-span-4 box">
            <div class="col-span-5">
                <div class="intro-y col-span-12 lg:col-span-8 xxl:col-span-9">
                    <div class="tab-content">
                        <div id="chats" class="tab-pane active" role="tabpanel" aria-labelledby="chats-tab">
                            <div class="pr-1">
                                <div class="intro-y col-span-12 lg:col-span-12">
                                    <div class="intro-y box mt-0">
                                        <div
                                            class="flex sm:flex-row items-center p-3 border-b border-gray-200 dark:border-dark-5">
                                            <h2 class="font-medium text-base">{{ __('admin_register.mobile_verification') }}</h2>
                                            <!-- BEGIN: Custom Tooltip Content -->
                                        </div>
                                        <div id="horizontal-form" class="p-5" x-data="{ show: @if (old('phone')) true @else false @endif }">
                                            <div class="p-3 mt-3 text-center">

                                                <div class="text-center mt-3">
                                                    <div class="font-medium text-lg">{{ __('admin_register.verify_mobile') }}</div>
                                                    <div class="text-gray-600 mt-1">{{ __('admin_register.verify_mobile_text1') }}<br> {{ __('admin_register.verify_mobile_text2') }} </div>
                                                </div>
                                                <form action="{{ route('agent_create.register.changeMobileNumber',$user->id) }}" method="post" enctype="">
                                                    @csrf
                                                    @method('post')
                                                    <div
                                                        class="dark:bg-darkmode-400 dark:border-darkmode-400 bg-white py-3 rounded text-center mt-5">
                                                        <h1 class="text-2xl font-bold">OTP Verification</h1>
                                                        <div class="w-full mx-auto mt-0">
                                                            <div class="flex items-baseline sm:w-6/12 mx-auto">
                                                                <div class="pos__ticket__item-name w-full mt-3" style="display:flex;align-items: center;justify-content: center;gap:10px;">
                                                                <span class="font-normal text-2xl"
                                                                    x-show="!show">{{ $user->getPhoneWithCountryCode() }}
                                                                </span>

                                                                    <div x-show="show">
                                                                        <span>{{ __('admin_register.enter_mobile') }}</span>
                                                                        <div class="w-full pb-3">
                                                                            <div class="input-group mt-2">
                                                                                <input type="number" class="form-control"
                                                                                    id="phone" name="phone"
                                                                                    onKeyPress="if(this.value.length==11) return false;return onlyNumberKey(event);"
                                                                                    required>
                                                                                <span class="block text-center text-theme-6 mt-2"
                                                                                    id="mobile_error"></span>

                                                                                <div id="input-group-price" class="input-group-text"
                                                                                    style="padding: 0;">
                                                                                    <button type="submit"
                                                                                        class="btn btn-elevated-primary mt-0 w-32">
                                                                                        {{ __('admin_register.update_number') }}
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                            @error('phone')
                                                                                <span
                                                                                    class="block text-theme-6 mt-2">{{ $message }}</span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <button class="ml-0" @click="show = !show"  style="margin-top:10px;"
                                                                        :aria-expanded="show ? 'true' : 'false'"
                                                                        :class="{ 'active': show }" type="button"
                                                                        aria-expanded="false">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                            width="24" height="24"
                                                                            viewBox="0 0 24 24" fill="none"
                                                                            stroke="currentColor" stroke-width="1.5"
                                                                            stroke-linecap="round"
                                                                            stroke-linejoin="round"
                                                                            class="feather feather-edit block mx-auto">
                                                                            <path
                                                                                d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7">
                                                                            </path>
                                                                            <path
                                                                                d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z">
                                                                            </path>
                                                                        </svg>
                                                                    </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                                    <div class="p-0 mt-0">
                                                        <form action="{{ route('agent_create.register.mobileOtpVerification',$user->id) }}" method="post" enctype="">
                                                            @csrf
                                                            @method('post')
                                                            <div id="otp"
                                                                class="flex flex-row justify-center text-center px-2 mt-5"
                                                                x-show="!show">
                                                                <input
                                                                    class="p-0 m-1 form-focus  border-2 border-theme-1 dark:border-theme-1 h-8 w-8 text-center form-control rounded"
                                                                    type="number" id="first" maxlength="1" onkeypress="preventNonNumericalInput(event)">
                                                                <input
                                                                    class="p-0 m-1 form-focus  border-2 border-theme-1 dark:border-theme-1 h-8 w-8 text-center form-control rounded"
                                                                    type="number" id="second" maxlength="1" onkeypress="preventNonNumericalInput(event)">
                                                                <input
                                                                    class="p-0 m-1 form-focus  border-2 border-theme-1 dark:border-theme-1 h-8 w-8 text-center form-control rounded"
                                                                    type="number" id="third" maxlength="1" onkeypress="preventNonNumericalInput(event)">
                                                                <input
                                                                    class="p-0 m-1 form-focus  border-2 border-theme-1 dark:border-theme-1 h-8 w-8 text-center form-control rounded"
                                                                    type="number" id="fourth" maxlength="1" onkeypress="preventNonNumericalInput(event)">
                                                                <input
                                                                    class="p-0 m-1 form-focus  border-2 border-theme-1 dark:border-theme-1 h-8 w-8 text-center form-control rounded"
                                                                    type="number" id="fifth" maxlength="1" onkeypress="preventNonNumericalInput(event)">
                                                                <input
                                                                    class="p-0 m-1 form-focus  border-2 border-theme-1 dark:border-theme-1 h-8 w-8 text-center form-control rounded"
                                                                    type="number" id="sixth" maxlength="1" onkeypress="preventNonNumericalInput(event)">
                                                                <input type="hidden" name="code" value="">
                                                            </div>
                                                            @error('code')
                                                                <span class="block text-center text-theme-6 mt-2">{{ $message }}</span>
                                                            @enderror
                                                            <div class="flex justify-center text-center mt-5"
                                                                x-show="!show">
                                                                <a onclick="mobileresendotp()"
                                                                    class="flex items-center text-blue-700 hover:text-blue-900 cursor-pointer"><span
                                                                        class="font-bold resendOtp">{{ __('admin_register.resend_otp') }}</span><i
                                                                        class="bx bx-caret-right ml-1"></i></a>

                                                            </div>

                                                            <span x-show="!show"
                                                                class="block text-center text-theme-1 mt-2"
                                                                id="resendotp"></span>

                                                            <div class="flex justify-center text-center mt-3">
                                                                <div class="flex justify-center text-center mt-0">
                                                                    <div class="form-inline float-right mb-3">
                                                                        <button id="MobileVerification" type="submit"
                                                                            class="btn btn-elevated-primary w-24 mr-1">
                                                                            {{ __('admin_register.continue') }}</button>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('javascript')
<script>
        document.addEventListener("DOMContentLoaded", function(event) {
            var otp = '';

            function OTPInput() {
                const inputs = document.querySelectorAll('#otp > *[id]');

                for (let i = 0; i < inputs.length; i++) {

                    inputs[i].addEventListener('keyup', function(event) {

                        $("input[name='code']").val($("#first").val() + $("#second").val() + $("#third")
                            .val() + $("#fourth").val() + $("#fifth").val() + $("#sixth").val());
                        if (event.key === "Backspace") {
                            inputs[i].value = '';
                            if (i !== 0) inputs[i - 1].focus();
                        } else {

                            if (i === inputs.length - 1 && inputs[i].value !== '') {
                                return true;
                            } else if (event.keyCode > 47 && event.keyCode < 58) {
                                inputs[i].value = event.key;
                                if (i !== inputs.length - 1) inputs[i + 1].focus();
                                event.preventDefault();
                            } else if (event.keyCode > 95 && event.keyCode < 106) {
                                inputs[i].value = event.key;
                                if (i !== inputs.length - 1) inputs[i + 1].focus();

                                event.preventDefault();
                            }
                        }

                    });
                }
            }
            OTPInput();
        });


        function mobileresendotp() {
            $(".resendOtp").html("{{ __('admin_register.sending') }}....");
            $(".resendOtp").addClass("OtpSend");
            $(".resendOtp").removeClass("resendOtp");

            $.ajax({
                url: "{{ route('agent_create.register.resendMobileOtp',$user->id) }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response) {
                    console.log(response);
                    if (response.status == 'success') {
                        $("#resendotp").html(response.message);
                        $(".OtpSend").html("{{ __('admin_register.resend_otp') }}");
                        $(".OtpSend").addClass("resendOtp");
                        $(".OtpSend").removeClass("OtpSend");

                    }


                },
                error: function(xhr, status, errorThrown) {
                    xhr.status;
                    Toastify({
                        node: $("#message")
                            .clone()
                            .removeClass("hidden")[0],
                        duration: -1,
                        newWindow: true,
                        close: true,
                        gravity: "top",
                        position: "right",
                        backgroundColor: "white",
                        stopOnFocus: true,
                    }).showToast();
                    document.getElementById("submit").disabled = true;
                }
            });
        }

        function preventNonNumericalInput(e) {
            e = e || window.event;
            var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
            var charStr = String.fromCharCode(charCode);

            if (!charStr.match(/^[0-9]+$/))
                e.preventDefault();
        }
    </script>
@endsection