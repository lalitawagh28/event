@extends('layouts.admin')
@section('content')
    <!--top registration section-->
    <!--Membership registration UI-->
    <div class="intro-y grid grid-cols-12 gap-0 mt-5">
        <div class="intro-y col-span-12 md:col-span-12 text-center py-3">
            <h2 class="text-lg font-medium text-3xl py-4">{{ __('admin.waiting_approval') }} </h2>
            <p> {{ __('admin.your_account') }}
                        <br /> {{ __('admin.later') }}
                        </p>
        </div>
    </div>
@endsection
@section('javascript')
<script>
    var timer = setTimeout(function() {
            window.location="{{ config('app.url') }}"
        }, 3000);
</script>
@endsection