@extends('eventmie::layouts.app')
<style>
    <style>
    .ti-form-label {
        margin-bottom: .5rem;
        display: block;

        line-height: 1.25rem;
        font-weight: 300;
        margin-bottom: 5px;
        color: #000;
        font-size: 18px !important;
    }
    .ti-form-input {
        display: block;
        width: 100%;
        align-items: center;
        padding: .75rem 1rem;
        line-height: 1.25rem;
        margin-right: 10px;
        border-radius: 8px;
        border: 1px solid #999;
    }

    .profile_save {
        padding: 8px 236px 12px 236px;
        align-items: center;
        flex-shrink: 0;
        border-radius: 8px;
        background: #FF5C02;
        color: white;
    }
    .profile_info_user {
        margin-top: 10px;
    }
    .profile_form_title {
        color: #000;
        font-size: 23px;
        font-weight: 600;
        margin-bottom: 20px;
    }
    .profile_forms .card {
    border-radius: 14px;
}

.card h3 {
    border-bottom: 1px solid #333;
    padding-bottom: 5px;
    display: block;
}
.deposit_money_button2{
        background-color: #FF5C02;
        color: white;
        padding: 8 54px;
        border-radius: 5px;
    }

    .deposit_slienttag{
        font-size:10px;

    }


</style>


@section('content')
<div class="container">
<div class="row">
    <div class="col-md-12 m-auto">

        <div class="profile_forms clearfix">
            <div class="card my-5 p-4">
            <div class="card-header">
                <h3 class="profile_form_title border-b pb-2">Deposits</h3>
            </div>
            <div class="card-body">
            <form>

                <div class="col-md-12 m-auto">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Deposit To <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <select  class="ti-form-input" placeholder="Select Deposit To">
                                            <option>INR</option>
                                            <option>GBP</option>
                                            <option>Bit Coin</option>
                                            <option>Ripple Coin</option>
                                          </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-5 col-form-label ti-form-label">Payment Method <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-7">
                                        <select  class="ti-form-input" placeholder="Select Deposit To">
                                            <option>Stripe</option>
                                            <option>Manual Stripe</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Amount <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <input type="text" id="input-label" class="ti-form-input"
                                            placeholder="Naveen kumar">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-5 col-form-label ti-form-label">Reference <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-7">
                                        <input type="text" id="input-label" class="ti-form-input"
                                            placeholder="Naveen kumar">
                                            <span class="deposit_slienttag pt-3">Ex.Fees: 0 + Additional Fees, Ex Rate: 1 GBP = 1.00 GBP</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Deposit Form <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <select  class="ti-form-input" placeholder="Select Deposit To">
                                            <option>INR</option>
                                            <option>GBP</option>
                                          </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right mt-3">
                            <div class="deposit_next_button mt-4 mr-4">
                                <a class="deposit_money_button2" href="{{ route('deposit_form1') }}">Next</a>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
            </div>
            </div>
        </div>
    </div>

</div>
</div>

@endsection
@section('javascript')
@stop
