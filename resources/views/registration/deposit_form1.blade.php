@extends('eventmie::layouts.app')
<style>
    .deposit_money{
        display: flex;
    justify-content: space-between;
    }

    .deposit_money_button{
        display: flex;
        gap: 20px;
        justify-content: end;
        margin-top: 24px;
    }
    .deposit_money_button1{
        border-radius: 5px;
        border: 2px solid #FF5C02;
        color: #FF5C02;
        padding: 6 44px;

    }
    .deposit_money_button2{
        background-color: #FF5C02;
        color: white;
        padding: 6 44px;
        border-radius: 5px;
    }
    .profile_forms .card {
    border-radius: 14px;
}

.card h3 {
    border-bottom: 1px solid #333;
    padding-bottom: 5px;
    display: block;
}
.deposit_bold{
    font-size: 15px;
    font-weight: 700;
    color: #333;
}
.total_deposit{
    border-top: 1px solid rgb(240, 234, 226);
    padding-top: 10px;
}
.deposit_label,.deposit_input{
    color:#333;
    font-size: 15px;
    font-weight: 500;
}
/**/
.overlay {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 100%;
    background-color: rgb(0, 0, 0);
    background-color: rgb(255 255 255 / 1);
    overflow-x: hidden;
    transition: 0.5s;
    -webkit-box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
    -moz-box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
    box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
}

.closebtn{
    font-size: 25px;
    color: black;
    text-align: right;
    margin-left: 243px;
}
.text-header{
    border-bottom: 1px solid rgb(93, 87, 87);
    display: flex;
    align-items: center;
    padding: 5px;
}
.deposit_slienttags{
    font-size:10px;
}
.profile_info_userss{
    display:flex;
}
.otp_label{
    width:80px;
}
.otp_input{
    width:307px;
}
.resend_otp{
    border-radius: 5px;
    border: 2px solid #FF5C02;
    color: #FF5C02;
    padding: 3 20px;
}
.submit_otp{
    background-color: #FF5C02;
    color: white;
    padding: 3 20px;
    border-radius: 5px;
}



</style>


@section('content')

<div class="col-span-12">
<div class="row">
    <div class="col-md-4 m-auto">

        <div class="profile_forms clearfix">
            <div class="card my-5 p-4">
            <div class="card-header">
                <h3 class="profile_form_title border-b pb-2">Deposits</h3>
            </div>
            <div class="card-body">
               <form>

                <div class="col-md-12 m-auto ">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="deposit_bold">Deposit money via stripe</p>
                        </div>
                        <div class="col-md-12 mt-3 ">
                            <div class="deposit_money">
                                <p class="deposit_label">Deposit Amount</p>
                                <p class="deposit_input">$123</p>
                            </div>
                        </div>
                        <div class="col-md-12 mt-3">
                            <div class="deposit_money">
                                <p class="deposit_label">Fee</p>
                                <p class="deposit_input">$123</p>
                            </div>
                        </div>
                        <div class="col-md-12 mt-3 total_deposit">
                            <div class="deposit_money">
                                <p class="deposit_label">Total</p>
                                <p class="deposit_input">$1234</p>
                            </div>
                        </div>
                        <div class="col-md-12 text-right mt-3 ">
                            <div class="deposit_money_button ">
                                <a class="deposit_money_button1">Previous</a>
                                <a class="deposit_money_button2" style="float:right;" onclick="openNav()">Next</a>
                            </div>
                        </div>
                        <!-- off canvas-->
                        <div id="myNav" class="overlay" style="">

                            <div class="overlay-content">
                              <div class="text-block">
                                  <div class="text-header">
                                    <h5>OTP Verification</h5>
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                  </div>
                                  <div class="content_main_overlay mt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="profile_info_userss">
                                                <label for="input-label" class="otp_label col-form-label ti-form-label">Otp<span class="text-danger">*</span></label>
                                                  <div>
                                                    <input type="text" id="input-label" class="otp_input ti-form-input"
                                                        placeholder=""><br>
                                                        <span class="deposit_slienttags pt-3">Please Check OTP Sent to your Mobile . It will Expire in 10 Minutes</span>
                                                  </div>


                                            </div>
                                        </div>
                                    </div>
                                  </div>
                              </div>
                              <div class="col-md-3  mt-2 ">
                                <div class="deposit_money_button ">
                                    <a class="resend_otp">Resend OTP</a>
                                    <a class="submit_otp" href="{{ route('deposit_card_details') }}" >Submit</a>
                                </div>
                            </div>
                            </div>
                        </div>

                    </div>
                </div>

                </div>
               </form>
            </div>
            </div>
        </div>
    </div>

</div>
</div>
       {{-- <div class="deposit_payment">
        <form>
            <h3 class="deposit_title border-b py-2">Deposit</h3>
            <div>
                <p>Deposit money via stripe</p>
                <div class="deposit_money">
                    <p class="deposit_label">Deposit Amount</p>
                    <p class="deposit_input">$123</p>
                </div>
                <div class="deposit_money">
                    <p class="deposit_label">Fee</p>
                    <p class="deposit_input">$1</p>
                </div>
                <div class="deposit_money">
                    <p class="deposit_label">Total</p>
                    <p class="deposit_input">$1</p>
                </div>
                <div class="deposit_money_button">
                    <a class="deposit_money_button1">Previous</a>
                    <a class="deposit_money_button2">Next</a>
                </div>
            </div>
        </form>
       </div> --}}

@endsection
@section('javascript')
<script>
function openNav() {
  document.getElementById("myNav").style.left = "70%";
}

function closeNav() {
  document.getElementById("myNav").style.left = "100%";
}
</script>
@stop
