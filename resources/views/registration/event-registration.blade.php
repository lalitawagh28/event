<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">


    <style>
        .login,
        .image {
            min-height: 100vh;
        }

        .bg-image {
            background-image: url('{{ asset('images/event/event.png') }}');
            background-size: cover;
            background-position: center center
        }

        .btn {
            background: #FF5C02;
        }

        .captcha {
            background: #D9D9D9;
            padding: 5px 10px;

        }

        .ti-form-input {
            margin-right: 5px;
        }

        .reg_captcha {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .highlight {
            color: #FF5C02;
        }
    </style>
</head>

<body>

    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>
            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-10 mx-auto">
                                <h5 class="">Register Your account</h5>
                                <p>Fill the details below to register account</p>
                                <form>
                                    <div class="form-group mb-3">
                                        <div class="">
                                            <div class="form-group mb-3">
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">First Name*</label>
                                                    <input id="inputEmail" type="text" placeholder="Your First Name"
                                                        required="" autofocus="" class="form-control">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Last Name*</label>
                                                    <input id="inputEmail" type="text" placeholder="Your Last Name"
                                                        required="" autofocus="" class="form-control">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Email*</label>
                                                    <input id="inputEmail" type="email" placeholder="Your Email"
                                                        required="" autofocus="" class="form-control">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Mobile
                                                        Number*</label>
                                                    <input id="inputEmail" type="email"
                                                        placeholder="Your Mobile Number" required="" autofocus=""
                                                        class="form-control">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Pin*</label>
                                                    <input id="inputEmail" type="email"
                                                        placeholder="Enter 6 digit pin" required="" autofocus=""
                                                        class="form-control">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Confirm Pin*</label>
                                                    <input id="inputEmail" type="email"
                                                        placeholder="Confirm your 6 digit pin" required=""
                                                        autofocus="" class="form-control">
                                                </div>
                                                <div class="form-group mb-3">

                                                    <label for="input-label" class="ti-form-label">Captcha*</label>
                                                    <div class="reg_captcha">
                                                        <div class="captcha_code">
                                                            <p class="captcha">U x 7 9 Z Z</p>
                                                        </div>
                                                        <div class="refresh_icon">
                                                            <img src="{{ asset('images/event/refresh.png') }}"
                                                                alt="userbanner-img" width="28px;"
                                                                class="mx-auto mb-4 ">
                                                        </div>

                                                        <div class="captcha_input">
                                                            <input id="inputEmail" type="email"
                                                                placeholder="enter captcha" required=""
                                                                autofocus="" class="form-control mb-4">
                                                        </div>
                                                    </div>

                                                    <label class="width-adjust" for="checkBox">
                                                        <input type="checkbox" id="checkBox" name="checkBox">
                                                        By Signing, you're agree to our <span class="highlight">Terms &
                                                            Conditions</span> and <span class="highlight">Privacy
                                                            Policy</span>
                                                    </label>

                                                </div>

                                            </div>

                                        </div>
                                        <button type="submit"
                                            class="btn btn btn-yellow btn-block  mb-2 rounded-pill shadow-sm">Register</button>
                                        <div class=" text-center  justify-content-between  ">
                                            <p> Already you have an account? Login</p>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </form>

                    </div>

                </div>
            </div>

            <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
            </script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
            </script>

</body>

</html>
