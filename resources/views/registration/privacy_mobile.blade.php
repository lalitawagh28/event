<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <title>Dhigna Events -Privacy-Mobile</title>
    <style>
        dl, ol, ul {
            margin-top: 0;
            margin-bottom: 1rem;
            margin: 0;
            padding: 10px 15px;
        }
        p,li{
            margin-bottom: 13px;
            line-height: 1.7;
            color:black;

        }
        .title{
            text-align: center;
            margin-top: 30px;
            margin-bottom: 30px;
        }
        .arrow_bullet{
            font-size: 24px;
        }
        .arrow_p{
            margin-bottom: 5px !important;
            line-height: 1.1;
        }


    </style>
  </head>
  <body>
    <div class="container">
                <h3 class="title"><u>Privacy Policy</u></h3>
            <p>Please find our Privacy Policy below, which explains how we obtain, use and keep your personal data safe. </p>
            <p>This document outlines how we collect, use, and protect your personal data when you interact with our services and attend events. We take your privacy seriously and are committed to safeguarding your information while providing you with an enjoyable and secure experience. </p>
            <p>Please read this Privacy Policy carefully to understand how we handle your personal data.  </p>
        <h5>Our Responsibilities </h5>
        <ol>
           <p> Keeping your personal information safe is a priority for Dhigna, and we continually invest in new technologies to protect your data and keep it safe.</p>
            <li>Dhigna will always respect your privacy, and we are constantly working to ensure we meet both the spirit and the word of the strict regulations around data privacy. </li>
            <li> Dhigna will never sell your data to third parties; we want you to be confident that the personal information you share with us is protected against misuse. </li>
            <li>We will use the personal information you give us to ensure that our communication with you is useful, relevant, and timely. </li>
            <p>We know how important your privacy is – we will always set the highest standard to protect your personal information.</p>
        </ol>
        <h5>Our privacy principles </h5>
        <p>Globally, most privacy legislation contains similar obligations and we have embedded key principles in our personal information processing activities to align with them: </p>
        <p><b>Lawfulness, fairness, and transparency </b>- means that Dhigna will tell people how their personal information will be used and not use it in a way that is inherently ‘unfair’. We present an overview of our approach to data protection and privacy on our website and these pages are constantly reviewed. </p>
        <p><b>Purpose limitations</b> – Dhigna will only collect personal information for specified purposes and not use it for unrelated purposes. </p>
        <p><b>Data Minimisation </b>– Dhigna will collect only relevant personal information and not collect excessive or irrelevant personal information. </p>
        <p><b>Accuracy </b>– Dhigna has procedures in place to maintain the accuracy of the personal information it collects or handles and keeps on updating it from time to time. </p>
        <p><b>Storage limitations </b>– Dhigna will hold your personal information only for the necessary period. </p>
        <p><b>Integrity and confidentiality</b> – Dhigna will ensure the use of appropriate security to protect your personal information from unauthorised access, use and loss.  </p>
        <h5>The categories of Personal Data we process</h5>
         <p><b>Biometric Data :</b> This includes fingerprint, iris scan, closed-circuit television recording, and other audio/visual data analyzed by facial recognition or identification systems used at Venues (if applicable).  </p>
         <p><b>Commercial Data :</b> Information related to the services provided to you and your transactions for events and services at our Venues, such as purchase details and payment information.  </p>
         <p><b>Contact Data :</b> Information used for communication, including email addresses, phone numbers, physical addresses, social media handles, and data provided when contacting us.  </p>
         <p><b>Device/Network Data :</b> Data related to your interactions with websites, applications, or advertisements, such as browsing history, IP addresses & cookies.  </p>
         <p><b>Financial Data :</b> Information like bank account details and payment information for purchased products or services. </p>
         <p><b>Health Data :</b> Information about your health, such as accessibility requests, accident response, or health screening checks at Venues.</p>
         <p><b>Identity Data :</b> Personal details like name, gender, date of birth, account login information, social media profiles, and unique IDs linked to wristbands used for access.</p>
         <p><b>Inference Data :</b> Data used to create a profile about you, reflecting preferences, behavior, interests, market segments, and more, but not used to infer sensitive personal information. </p>
         <p><b>Location Data :</b> Information about your location, including precise location data and general location data linked to social media tags or IP addresses.</p>
         <p><b>User Content :</b> Unstructured data you provide, such as comments, answers in surveys, or any other personal data submitted through our services.</p>
         <h5>Sources of Personal Data include</h5>
         <p><b>Data you provide :</b> Personal data you provide during purchases, transactions, or interactions with our services.</p>
         <p><b>Data we collect automatically :</b> Personal data generated by devices used to access our services or Wi-Fi at our venues.</p>
         <p><b>Data from Service Providers :</b> Personal data received from ticket agents and service providers who facilitate transactions on our behalf. </p>
         <p><b>Data from promoters and event partners :</b> Personal data received from event promoters and partners related to products and services you purchase or express interests in.</p>
         <p><b>Data from aggregators and advertisers :</b> Personal data received from ad networks, data brokers, and social media companies for demographics and preferences. </p>
         <p><b>Data from social media companies :</b> Personal data received from social media companies when interacting with our services or venues. </p>
         <p><b>Data we create or infer :</b> Personal data created or inferred based on our observations or analysis of other personal data processed under the policy.</p>
         <p>If you provide personal data about another individual, you must have the authority to provide their personal data to us and you must share this Privacy Policy and any related data protection statement with them beforehand together with details of what you’ve agreed on their behalf. </p>
         <h5>How do we collect your Personal Data </h5>

            <p class="arrow_p"><span class="arrow_bullet">&#8227;</span> When you make a purchase or other transaction through our Services. </p>
            <p class="arrow_p"><span class="arrow_bullet">&#8227;</span> When you visit our venues</p>
            <p class="arrow_p"><span class="arrow_bullet">&#8227;</span> Biometric Data or other Identify Data for access to certain Venues or areas at certain Venues</p>
            <p class="arrow_p"><span class="arrow_bullet">&#8227;</span> When you contact us or submit information to us </p>
            <p class="arrow_p"><span class="arrow_bullet">&#8227;</span> Feedback and Surveys</p>
          <h5>How do we use your personal data?</h5>
          <p>We will only collect and use your data where there is a legal reason for doing so. The most common reasons are: </p>
          <p>To meet legal / regulatory requirements; or </p>
          <p>Where we have a legitimate interest in processing your personal data, for example ensure the effective functioning of our business, to carry out administrative functions or to protect our business</p>
          <p>Your Personal Data may be stored and processed by us in the following ways and for the following purposes:</p>
           <ul>
             <li>To allow Clients and prospective Clients to use and access Dhigna Products and Services; </li>
             <li>To set up / on-board prospective Clients to use Dhigna Products and Services;</li>
             <li>To keep our records up to date; </li>
             <li>To monitor IT systems in order to protect against cyber threats or malicious activity including abuse and misuse; </li>
             <li>To protect our premises from unauthorised access or use, or any unlawful activity </li>
             <li>To administer or maintain IT systems in order to uphold standards of service; </li>
             <li>For ongoing review and improvement of the information provided on Dhigna Websites to make them user friendly and prevent potential disruptions or cyber-attacks; </li>
             <li>To understand feedback on Dhigna Products and Services and to help provide more information on the use of those products and services quickly and easily;</li>
             <li>For the management and administration of our business;</li>
             <li>In order to comply with and in order to assess compliance with applicable laws, rules and regulations, and internal policies and procedures. </li>
           </ul>
           <h5>How We Retain Your Personal Data</h5>
           <p>We retain Personal Data for so long as it, in our discretion, remains relevant to its purpose, and in any event, for so long as is required by law. This period of retention may be different if required by law, or if we need to retain data to respond to claims, to protect our rights or the rights of a third party. We regularly review retention periods and, in some cases, apply data privacy measures to maintain data security and privacy over extended periods when appropriate.</p>
           <p>Dhigna will not retain your personal information for longer than necessary.</p>
           <h5>Third-Party Links and Services </h5>
           <p>Our Sites may contain links to third-party websites and services. When you use a link to go from our Sites to another website or you request a service from a third party, this Privacy Policy no longer applies. </p>
           <h5>Children  </h5>
           <p>We do not and will not knowingly collect information from any unsupervised child under the age of 13. If you are under the age of 13, you may not use the Services unless your parent/s or guardian has provided us with their consent for your use of the Services. </p>
           <h5> Your legal rights </h5>
           <p> You have certain rights in accordance with applicable data protection law, these rights are: </p>
           <ul>
              <li>Request access to your personal data (commonly known as a “data subject access request”). This enables you to receive a copy of the personal data we hold about you and to check that we are lawfully processing it.</li>
              <li>Request correction of the personal data that we hold about you. This enables you to have any incomplete or inaccurate data we hold about you corrected, though we may need to verify the accuracy of the new data you provide to us.</li>
              <li>Request erasure of your personal data. This enables you to ask us to delete or remove personal data where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your personal data where you have successfully exercised your right to object to processing.</li>
              <li>Object to processing of your personal data where we are relying on a legitimate interest (or those of a third party) and there is something about your particular situation which makes you want to object to processing on this ground as you feel it impacts your fundamental rights and freedoms. You also have the right to object where we are processing your personal data for direct marketing purposes. In some cases, we may demonstrate that we have compelling legitimate grounds to process your information which overrides your rights and freedoms.</li>
              <li> Request restriction of processing of your personal data. This enables you to ask us to suspend the processing of your personal data in the following scenarios:</li>
              <li>If you want us to establish the data’s accuracy.</li>
              <li>Where our use of the data is unlawful, but you do not want us to erase it. </li>
              <li>Where you need us to hold the data even if we no longer require it as you need it to establish, exercise, or defend legal claims. </li>
              <li>You have objected to our use of your data, but we need to verify whether we have overriding legitimate grounds to use it. </li>
            </ul>
            <h5>How to Contact us?  </h5>
            <p>If you have any questions about this Privacy Policy or want to exercise your rights set out in this Privacy Policy, please contact us by:</br>
                Sending an e-mail to: info@dhigna.com</br>
                Or mail us at: 30 Churchill Place, Canary Wharf, London E14 5RE, United Kingdom </p>
            <h5>Further Information </h5>
            <p>If you are not satisfied with our response or believe we are processing your personal data not in accordance with the law you have the right to complain to the Information Commissioner’s Office at https://ico.org.uk/. It has enforcement powers and can investigate compliance with data protection laws</p>
            <h5>Changes to this Policy </h5>
            <p>Any changes we make to our Privacy Policy in the future will be posted on this website and where appropriate. Please check back frequently to see any updates or changes to our Privacy Policy. </P>
    </div>
  </body>
</html>
