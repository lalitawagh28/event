@extends('eventmie::layouts.app')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
    integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> <!-- Load jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script> <!-- Owl Carousel JS -->
<style>
    body .form-control {
        bbackground-color: #fff;
        background-image: none;
        border: 2px solid #e4e4e4;
        border-radius: 6px;
        box-shadow: none;
        color: #868686;
        display: block;
        font-size: 14px;
        height: 42px;
        line-height: 1.42857143;
        padding: 1rem 2rem;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        width: 100%;
    }

    label.searchmain {
        position: absolute;
        right: 17px;
        top: 50%;
        transform: translateY(-50%);
    }

    .blue-bg {
        background: rgb(27, 132, 230);
        background: linear-gradient(90deg, rgba(27, 132, 230, 1) 0%, rgba(22, 56, 95, 1) 72%, rgba(20, 19, 28, 1) 100%);
        border-radius: 5px;
        padding: 5px 8px;
    }

    .orange-bg {
        background: rgb(255, 93, 3);
        background: linear-gradient(90deg, rgba(255, 93, 3, 1) 0%, rgba(251, 128, 20, 1) 50%, rgba(250, 143, 28, 1) 100%);
        border-radius: 5px;
        padding: 5px 8px;
    }

    .lightred-bg {
        background: rgb(191, 82, 68);
        background: linear-gradient(90deg, rgba(191, 82, 68, 1) 0%, rgba(190, 89, 98, 1) 46%, rgba(190, 91, 112, 1) 100%);
        border-radius: 5px;
        padding: 5px 8px;
    }

    .event-thumimg {
        border-radius: 50px;
        width: 45px;
        height: 45px;
        position: relative;
        background-color: #fff;
        padding: 8px;
    }

    .discounts {
        text-align: center;
        line-height: 1.3;
        font-size: 14px;
        border-left: 1px dashed #d1d1d1;
        padding-left: 20px;
    }

    .coupons-tab span a {
        font-size: 15px;
        color: #414141;
    }

    .coupons-tab span.active a {
        color: #ff5b00;
        border-bottom: 1px solid #ff5b00;
    }

    .redeem-section {
        background-color: #e2e2e2;
        margin-top: -2px;
    }

    .redeem-section-data p {
        font-size: 14px;
        color: #000;
        line-height: 1.7;
    }

    .redeem-section-data .btn {
        display: block;
        background-color: #ff5c02;
    }

    .radeem-wrap {
        margin-top: 70px;
    }
</style>


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="text-center">My Coupons</h3>
                <div class="form-group relative">
                    <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Search Coupons">
                    <label for="search-input" class="searchmain"><i class="fa fa-search" aria-hidden="true"></i></label>
                </div>
                <div class="coupons-tab mb-3 clearfix">
                    <span class="text-left float-left active"><a href="#">Expiring Today</a></span>
                    <span class="text-left float-right"><a href="#">Expiring Soon</a></span>
                </div>
                <div class="coupons-tab mb-3 clearfix">
                    <div class="blue-bg mb-3">
                        <div class="media text-muted pt-0 border-0">
                            <img class="event-thumimg" src="/images/event-icon.png" alt="Dhigna Events">
                            <div class="media-body py-3 px-3 small lh-125 text-white">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Once Upon a Time | Anirudh Ravichandran</strong>
                                    <span class="discounts">30% <br> Discount</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="orange-bg mb-3">
                        <div class="media text-muted pt-0 border-0">
                            <img class="event-thumimg" src="/images/event-icon.png" alt="Dhigna Events">
                            <div class="media-body py-3 px-3 small lh-125 text-white">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Oo ANTAVA | Devi Sri Prasad</strong>
                                    <span class="discounts">20% <br> Discount</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lightred-bg mb-3">
                        <div class="media text-muted pt-0 border-0">
                            <img class="event-thumimg" src="/images/event-icon.png" alt="Dhigna Events">
                            <div class="media-body py-3 px-3 small lh-125 text-white">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Adiye | Prakash</strong>
                                    <span class="discounts">10% <br> Discount</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="radeem-wrap">
                    <div class="blue-bg mb-0">
                        <div class="media text-muted pt-0 border-0">
                            <img class="event-thumimg" src="/images/event-icon.png" alt="Dhigna Events">
                            <div class="media-body py-3 px-3 small lh-125 text-white">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Once Upon a Time | Anirudh Ravichandran</strong>
                                    <span class="discounts">30% <br> Discount</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="redeem-section p-3">
                        <img src="/images/redeem-banner.png" class="img-fluid">
                        <div class="redeem-section-data pt-4">
                            <p>Expiry: Dec 31st 2023 | 11:59:00 PM</p>
                            <p>30% OFF on minimum 3 VIP Tickets upto 49 GBP</p>
                            <p>Code: RBS-Student-S1</p>
                            <a href="#" class="btn btn-warning block text-center text-white my-3 py-3">Redeem</a>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('javascript')
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script> <!-- Owl Carousel JS -->

@stop
