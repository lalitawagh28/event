<html lang="en" class="light">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}" />
    {{-- <link rel="stylesheet" href="/dist/css/support.css" /> --}}
    <!-- END: CSS Assets-->
</head>

<body>
    <!--top registration section-->
    <div class="grid grid-cols-12 gap-4 mt-4">
        <div
            class="col-span-12 md:col-span-12 lg:col-span-12 xxl:col-span-12 h-full flex flex-col sm:flex-row dark:border-dark-5">
            <div class="flex items-center w-full">
                <a href="" class="-intro-x flex items-center pt-0 mr-auto">
                    <img alt="" class="w-40" src=" /dist/img/logo.png">
                </a>

            </div>
        </div>
    </div>
    <!--top registration section-->
    <!--Membership registration UI-->
    <div class="intro-y grid grid-cols-12 gap-6 mt-5">
        <div class="col-span-12 lg:col-span-12 md:col-span-12">
            <div class="w-full pt-2 pl-3 flex gap-4 items-center">
                <h2 class="flex gap-4"> <label for="regular-form-5" class="form-label flex mb-0 text-lg pt-2"><i
                            data-lucide="calendar" class="mr-2 dark:text-slate-500"></i> Event</label> <a
                        href="{{ route('event-form') }}" class="btn btn-success btn-sm text-white"><i data-lucide="plus"
                            class="mr-2 dark:text-slate-500"></i> Add Event</a></h2>

            </div>
        </div>
    </div>
    <div class="intro-y box col-span-12 lg:col-span-12 mt-10">
        <div class="intro-y overflow-auto overflow-x-auto overflow-y-auto lg:overflow-visible sm:mt-0">
            <table class="table w-full">
                <thead class="color-lgray text-dark">
                    <tr>
                        <th class="whitespace-nowrap">Event ID</th>
                        <th class="whitespace-nowrap">Event Name</th>
                        <th class="whitespace-nowrap">Event URL</th>
                        <th class="whitespace-nowrap">Event Timings</th>
                        <th class="whitespace-nowrap">Event Location</th>
                        <th class="whitespace-nowrap">Total Capacity</th>
                        <th class="whitespace-nowrap">Total Tickets</th>
                        <th class="whitespace-nowrap">Event Date</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong class="font-medium text-dark whitespace-nowrap"><a href="#"
                                    data-tw-toggle="modal" data-tw-target="#eventID">1001</a></strong></td>
                        <td class="whitespace-nowrap">Digital Marketing Seminar</td>
                        <td class="whitespace-nowrap"><a href="www.dhigna.com">www.dhigna.com</a></td>
                        <td class="whitespace-nowrap">3.00pm UK</td>
                        <td class="whitespace-nowrap">Wembley Arena London, UK</td>
                        <td class="whitespace-nowrap text-center">1000</td>
                        <td class="whitespace-nowrap text-center">100</td>
                        <td class="whitespace-nowrap">8th June, 2023</td>
                    </tr>
                    <tr>
                        <td><strong class="font-medium text-dark whitespace-nowrap"><a href="#"
                                    data-tw-toggle="modal" data-tw-target="#eventID">1001</a></strong></td>
                        <td class="whitespace-nowrap">Digital Marketing Seminar</td>
                        <td class="whitespace-nowrap"><a href="www.dhigna.com">www.dhigna.com</a></td>
                        <td class="whitespace-nowrap">3.00pm UK</td>
                        <td class="whitespace-nowrap">Wembley Arena London, UK</td>
                        <td class="whitespace-nowrap text-center">1000</td>
                        <td class="whitespace-nowrap text-center">100</td>
                        <td class="whitespace-nowrap">8th June, 2023</td>
                    </tr>
                    <tr>
                        <td><strong class="font-medium text-dark whitespace-nowrap"><a href="#"
                                    data-tw-toggle="modal" data-tw-target="#eventID">1001</a></strong></td>
                        <td class="whitespace-nowrap">Digital Marketing Seminar</td>
                        <td class="whitespace-nowrap"><a href="www.dhigna.com">www.dhigna.com</a></td>
                        <td class="whitespace-nowrap">3.00pm UK</td>
                        <td class="whitespace-nowrap">Wembley Arena London, UK</td>
                        <td class="whitespace-nowrap text-center">1000</td>
                        <td class="whitespace-nowrap text-center">100</td>
                        <td class="whitespace-nowrap">8th June, 2023</td>
                    </tr>
                    <tr>
                        <td><strong class="font-medium text-dark whitespace-nowrap"><a href="#"
                                    data-tw-toggle="modal" data-tw-target="#eventID">1001</a></strong></td>
                        <td class="whitespace-nowrap">Digital Marketing Seminar</td>
                        <td class="whitespace-nowrap"><a href="www.dhigna.com">www.dhigna.com</a></td>
                        <td class="whitespace-nowrap">3.00pm UK</td>
                        <td class="whitespace-nowrap">Wembley Arena London, UK</td>
                        <td class="whitespace-nowrap text-center">1000</td>
                        <td class="whitespace-nowrap text-center">100</td>
                        <td class="whitespace-nowrap">8th June, 2023</td>
                    </tr>
                    <tr>
                        <td><strong class="font-medium text-dark whitespace-nowrap"><a href="#"
                                    data-tw-toggle="modal" data-tw-target="#eventID">1001</a></strong></td>
                        <td class="whitespace-nowrap">Digital Marketing Seminar</td>
                        <td class="whitespace-nowrap"><a href="www.dhigna.com">www.dhigna.com</a></td>
                        <td class="whitespace-nowrap">3.00pm UK</td>
                        <td class="whitespace-nowrap">Wembley Arena London, UK</td>
                        <td class="whitespace-nowrap text-center">1000</td>
                        <td class="whitespace-nowrap text-center">100</td>
                        <td class="whitespace-nowrap">8th June, 2023</td>
                    </tr>
                    <tr>
                        <td><strong class="font-medium text-dark whitespace-nowrap"><a href="#"
                                    data-tw-toggle="modal" data-tw-target="#eventID">1001</a></strong></td>
                        <td class="whitespace-nowrap">Digital Marketing Seminar</td>
                        <td class="whitespace-nowrap"><a href="www.dhigna.com">www.dhigna.com</a></td>
                        <td class="whitespace-nowrap">3.00pm UK</td>
                        <td class="whitespace-nowrap">Wembley Arena London, UK</td>
                        <td class="whitespace-nowrap text-center">1000</td>
                        <td class="whitespace-nowrap text-center">100</td>
                        <td class="whitespace-nowrap">8th June, 2023</td>
                    </tr>
                    <tr>
                        <td><strong class="font-medium text-dark whitespace-nowrap"><a href="#"
                                    data-tw-toggle="modal" data-tw-target="#eventID">1001</a></strong></td>
                        <td class="whitespace-nowrap">Digital Marketing Seminar</td>
                        <td class="whitespace-nowrap"><a href="www.dhigna.com">www.dhigna.com</a></td>
                        <td class="whitespace-nowrap">3.00pm UK</td>
                        <td class="whitespace-nowrap">Wembley Arena London, UK</td>
                        <td class="whitespace-nowrap text-center">1000</td>
                        <td class="whitespace-nowrap text-center">100</td>
                        <td class="whitespace-nowrap">8th June, 2023</td>
                    </tr>
                    <tr>
                        <td><strong class="font-medium text-dark whitespace-nowrap"><a href="#"
                                    data-tw-toggle="modal" data-tw-target="#eventID">1001</a></strong></td>
                        <td class="whitespace-nowrap">Digital Marketing Seminar</td>
                        <td class="whitespace-nowrap"><a href="www.dhigna.com">www.dhigna.com</a></td>
                        <td class="whitespace-nowrap">3.00pm UK</td>
                        <td class="whitespace-nowrap">Wembley Arena London, UK</td>
                        <td class="whitespace-nowrap text-center">1000</td>
                        <td class="whitespace-nowrap text-center">100</td>
                        <td class="whitespace-nowrap">8th June, 2023</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>




    <!-- BEGIN: Large Slide Over Content -->
    <div id="eventID" class="modal modal-slide-over" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header p-3">
                    <h2 class="font-medium text-base mr-auto">
                        Event ID
                    </h2>
                    <span class="font-medium text-base text-dark"><a href="javascript:void(0);">1001</a></span>
                </div>
                <div class="modal-body p-4">
                    <div class="border-b mb-2">
                        <div class="py-2 mb-0 flex items-center">
                            <div class="w-full mb-2">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">Event Name</div>
                                </div>
                                <div class="font-medium">Ticket Categories</div>
                            </div>
                            <div class="w-full mb-2 text-right">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">Digital Marketing Seminar</div>
                                </div>
                                <div class="font-medium">Gold</div>
                            </div>
                        </div>
                    </div>
                    <div class="border-b mb-2">
                        <div class="py-2 mb-0 flex items-center">
                            <div class="w-full mb-2">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">Event ID</div>
                                </div>
                                <div class="font-medium">Commision</div>
                            </div>
                            <div class="w-full mb-2 text-right">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">1001</div>
                                </div>
                                <div class="font-medium">5%</div>
                            </div>
                        </div>
                    </div>
                    <div class="border-b mb-2">
                        <div class="py-2 mb-0 flex items-center">
                            <div class="w-full mb-2">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">Ticket Price </div>
                                </div>
                                <div class="font-medium">Event Location</div>
                            </div>
                            <div class="w-full mb-2 text-right">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">95GBP</div>
                                </div>
                                <div class="font-medium">Webbley Arena London, UK</div>
                            </div>
                        </div>
                    </div>
                    <div class="border-b mb-2">
                        <div class="py-2 mb-0 flex items-center">
                            <div class="w-full mb-2">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">Event date/timings</div>
                                </div>
                                <div class="font-medium">Total Tickets</div>
                            </div>
                            <div class="w-full mb-2 text-right">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">3.00pm UK</div>
                                </div>
                                <div class="font-medium">100</div>
                            </div>
                        </div>
                    </div>
                    <div class="border-b mb-2">
                        <div class="py-2 mb-0 flex items-center">
                            <div class="w-full mb-2">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">Total Capacity</div>
                                </div>
                                <div class="font-medium">Ticket Lot</div>
                            </div>
                            <div class="w-full mb-2 text-right">
                                <div class="mr-auto pb-2">
                                    <div class="font-medium">100</div>
                                </div>
                                <div class="font-medium">100</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END: Large Slide Over Content -->


    <!-- BEGIN: JS Assets-->
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=[" your-google-map-api"]&libraries=places"></script>
    <script src="{{ asset('dist/js/app.js') }}"></script>
    <!-- END: JS Assets-->




</body>

</html>
