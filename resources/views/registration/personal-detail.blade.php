@extends('layouts.admin')
@section('content')
    <div class="grid grid-cols-8 mt-8">
        <div
            class="p-5 rounded-l24 complete-step blog col-start-2 col-span-4 sm:col-span-12 md:col-span-12 md:col-start-2 lg:col-start-2 xl:col-start-2 lg:col-span-4 xl:col-span-4">
            <h1 class="text-2xl ml-3 font-bold"> {{ __('admin_register.organizer_register') }} </h1>
        </div>
    </div>
    <div class="grid grid-cols-8 gap-4  mt-8">
        <div class="h-full xl:block md:hidden lg:hidden"></div>
        <div
            class="p-5 rounded-l24 complete-step blog col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-2 xl:col-span-2 color-lgray">
            <div class="chat px-3 py-2 m-0 text-gray-700">
                <div class="px-0 sm:px-0 step-bdr">
                    <div class="intro-x flex items-center complete-step block">
                        <button class="w-10 h-10 rounded-full btn btn-primary">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M5 13l4 4L19 7"></path>
                            </svg>
                        </button>
                        <div class="font-medium ml-3 font-bold">{{ __('admin_register.personal_detail') }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="rounded-r24 blog col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-4 xl:col-span-4 box">
            <div class="col-span-5 h-full">
                <div class="intro-y col-span-12 lg:col-span-8 xxl:col-span-9">
                    <div class="tab-content">
                        <div id="chats" class="tab-pane active" role="tabpanel" aria-labelledby="chats-tab">
                            <div class="pr-1">
                                <div class="intro-y col-span-12 lg:col-span-12">
                                    <div class="intro-y mt-0">
                                        <div
                                            class="flex sm:flex-row items-center p-3  border-b border-gray-200 dark:border-dark-5">
                                            <h2 class="font-medium text-base">
                                                {{ __('admin_register.create_account') }}
                                            </h2>
                                            <!-- BEGIN: Custom Tooltip Content -->
                                            <!-- END: Custom Tooltip Content -->
                                        </div>

                                        <form method="post" action="{{ route('admin.register.store') }}" class="p-5">
                                            @csrf
                                            <div class="preview">
                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="first_name"
                                                        class="form-label sm:w-40 font-bold">{{ __('admin_register.title') }}
                                                        <span class="text-theme-6">*</span>
                                                    </label>
                                                    <div class="sm:w-96">
                                                        <select data-search="true" name="title_id"
                                                            class=" w-full form-focus form-control @error('title_id') border-theme-6 @enderror p-2">
                                                            <option disabled=""></option>
                                                            @foreach ($titles as $key => $value)
                                                                <option value="{{ $key }}"
                                                                    @if ($loop->first) selected="selected" @endif
                                                                    @if ($key == old('title_id')) selected @endif>
                                                                    {{ $value }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('title_id')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>

                                                </div>
                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="first_name"
                                                        class="form-label sm:w-40 font-bold">{{ __('admin_register.first_name') }}
                                                        <span class="text-theme-6">*</span>
                                                    </label>
                                                    <div class="sm:w-96">
                                                        <input placeholder="{{ __('admin_register.enter_first_name') }}"
                                                            id="first_name" name="first_name"
                                                            value="{{ old('first_name', @$user?->first_name) }}"
                                                            type="text"
                                                            class="form-focus form-control @error('first_name') border-theme-6 @enderror">
                                                        @error('first_name')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>

                                                </div>
                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="middle_name"
                                                        class="form-label sm:w-40 font-bold">{{ __('admin_register.middle_name') }}</label>
                                                    <div class="sm:w-96">
                                                        <input placeholder="{{ __('admin_register.enter_middle_name') }}"
                                                            id="middle_name" name="middle_name"
                                                            value="{{ old('middle_name', @$user?->middle_name) }}"
                                                            type="text"
                                                            class="form-focus form-control @error('middle_name') border-theme-6 @enderror">
                                                        @error('middle_name')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>

                                                </div>
                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="last_name"
                                                        class="form-label sm:w-40 font-bold">{{ __('admin_register.last_name') }}
                                                        <span class="text-theme-6">*</span>
                                                    </label>
                                                    <div class="sm:w-96">
                                                        <input placeholder="{{ __('admin_register.enter_last_name') }}"
                                                            id="last_name" name="last_name"
                                                            value="{{ old('last_name', @$user?->last_name) }}"
                                                            type="text"
                                                            class="form-focus form-control @error('last_name') border-theme-6 @enderror">
                                                        @error('last_name')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>

                                                </div>

                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="email"
                                                        class="form-label sm:w-40 font-bold">{{ __('admin_register.email') }}
                                                        <span class="text-theme-6">*</span>
                                                    </label>
                                                    <div class="sm:w-96">
                                                        <input placeholder="{{ __('admin_register.enter_email') }}"
                                                            id="email" name="email"
                                                            value="{{ old('email', @$user?->email) }}" type="email"
                                                            class="form-focus form-control @error('email') border-theme-6 @enderror">
                                                        @error('email')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="phone"
                                                        class="form-label sm:w-40 font-bold">{{ __('admin_register.country_code') }}
                                                        <span class="text-theme-6">*</span></label>
                                                    <div class="sm:w-96">
                                                        <div class="input-group mobileNumber">
                                                            <div id="input-group-email"
                                                                class="input-group-text flex form-inline gap-1"style="padding: 0 5px 0 6px;">
                                                                <span id="countryWithPhoneFlagImg"
                                                                    style="display: flex; justify-content: center; align-items: center; align-self: center;">
                                                                    @foreach (@$countryWithFlags ?? [] as $country)
                                                                        @if ($country->id == old('country_id', $defaultCountry->id))
                                                                            <img src="{{ $country->flag }}">
                                                                        @endif
                                                                    @endforeach
                                                                </span>

                                                                <select class="sm:w-full" id="countryWithPhone"
                                                                    name="country_id" onchange="getFlagImg(this)" t
                                                                    data-search="true" class="form-control">
                                                                    @foreach (@$countryWithFlags ?? [] as $country)
                                                                        <option data-source="{{ $country->flag }}"
                                                                            value="{{ $country->id }}"
                                                                            @if ($country->id == old('country_id', $defaultCountry->id)) selected @endif>
                                                                            {{ $country->name }} ({{ $country->phone }})
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        @error('country_id')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="email"
                                                        class="form-label sm:w-40 font-bold">{{ __('admin_register.mobile') }}
                                                        <span class="text-theme-6">*</span>
                                                    </label>
                                                    <div class="sm:w-96">
                                                        <input placeholder="{{ __('admin_register.enter_mobile') }}"
                                                            id="mobile" name="phone"
                                                            value="{{ old('phone', @$user?->phone) }}" type="tel"
                                                            class="form-focus form-control @error('phone') border-theme-6 @enderror">
                                                        @error('phone')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="password"
                                                        class="form-label sm:w-40 font-bold">{{ __('admin_register.address') }}
                                                        <span class="text-theme-6">*</span>
                                                    </label>
                                                    <div class="sm:w-96">
                                                        <input placeholder="{{ __('admin_register.enter_address') }}"
                                                            id="address" name="address"
                                                            value="{{ old('address', @$user?->address) }}" type="text"
                                                            class="form-focus form-control @error('address') border-theme-6 @enderror">
                                                        @error('address')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>

                                                </div>

                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="password" class="form-label sm:w-40 font-bold">{{ __('admin_register.pin') }}
                                                        <span class="text-theme-6">*</span>
                                                    </label>
                                                    <div class="sm:w-96">
                                                        <input placeholder="{{ __("eventmie-pro::em.pin_message1")}}" id="password" name="password" value=""
                                                            type="password" class="form-focus form-control @error('password') border-theme-6 @enderror">
                                                        @error('password')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-inline mb-2 w-full flex items-start">
                                                    <label for="password" class="form-label sm:w-40 font-bold">{{ __('admin_register.pin_confirmation') }}
                                                        <span class="text-theme-6">*</span>
                                                    </label>
                                                    <div class="sm:w-96">
                                                        <input placeholder="{{ __('admin_register.enter_pin_confirmation') }}" id="password" name="password_confirmation" value=""
                                                            type="password" class="form-focus form-control @error('password') border-theme-6 @enderror">
                                                        @error('password_confirmation')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-inline mb-2">
                                                    <label for="captcha" class="form-label sm:w-40 font-bold">Captcha <span
                                                            class="text-theme-6">*</span></label>
                                                    <div class="sm:w-96">

                                                        <div class="captcha form-inline">
                                                            <span>{!! captcha_img('flat') !!}</span>
                                                            <button id="refresh_captcha_btn" name="refresh_captcha_btn"
                                                                type="button" onclick="refreshCaptcha()"
                                                                class="btn btn-primary ml-1"
                                                                data-url="{{ route('refresh_captcha') }}"><i
                                                                    data-lucide="refresh-cw"
                                                                    class="block mx-auto"></i></button>
                                                        </div>
                                                        <input id="captcha" type="text" class="form-control"
                                                            placeholder="Enter Captcha" name="captcha">
                                                        @error('captcha')
                                                            <span class="block text-theme-6 mt-2">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-inline mb-2 w-full">
                                                    <label class="form-label sm:w-40"></label>
                                                    <div class="sm:w-96">
                                                        <div class="form-inline float-right mt-3 mb-3">
                                                            <button type="submit" class="btn btn-elevated-primary w-24">
                                                                {{ __('admin_register.continue') }}</button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        function refreshCaptcha() {
            $.ajax({
                type: 'GET',
                url: '/refresh_captcha',
                success: function(data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        }

        function setInputFilter(textbox, inputFilter, errMsg) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "focusout"].forEach(function(event) {
                textbox.addEventListener(event, function(e) {
                    const key = e.key; // const {key} = event; ES6+
                    if (["keyup", "mouseup", "input", "focusout"].indexOf(e.type) >= 0) {
                        const pattern = /^0(0+)$/;
                        //const pattern = /^(0[1-9][0-9]{1,9})|^([1-9][0-9]{1,9})$/;
                        if (pattern.test(this.value)) {
                            if (this.oldValue) {
                                this.value = this.oldValue;
                            }
                            return;
                        }
                    }
                    if (inputFilter(this.value)) {
                        // Accepted value.
                        if (["keydown", "mousedown", "focusout"].indexOf(e.type) >= 0) {
                            this.classList.remove("input-error");
                            this.setCustomValidity("");

                        }
                    }
                    if (key === "Backspace" || key === "Delete") {
                        if (this.value.length == 1 && this.oldValue == this.value) {
                            this.oldValue = '';
                            this.value = '';
                        }
                    }
                    if (this.value[0] == '0' && this.value.length > 11) {
                        this.setCustomValidity(errMsg);
                        this.reportValidity();
                        this.value = this.oldValue;
                    }
                    if (this.value[0] != '0' && this.value.length > 10) {
                        this.setCustomValidity(errMsg);
                        this.reportValidity();
                        this.value = this.oldValue;
                    }
                    this.oldValue = this.value;
                });
            });
        }
        setInputFilter(document.getElementById("mobile"), function(value) {
            return /^\d{1,11}$/.test(value); // Allow digits and '.' only, using a RegExp.
        }, "Only 10 to 11 digits  are allowed");

        function getFlagImg(the) {
            var img = $('option:selected', the).attr('data-source');
            $('#countryWithPhoneFlagImg').html('<img src="' + img + '">');
        }

        
        if (key === "Backspace" || key === "Delete") {
            if (this.value.length == 1 && this.oldValue == this.value) {
                this.oldValue = '';
                this.value = '';
            }
        }
        if (this.value[0] == '0' && this.value.length > 11) {
            this.setCustomValidity(errMsg);
            this.reportValidity();
            this.value = this.oldValue;
        }
        if (this.value[0] != '0' && this.value.length > 10) {
            this.setCustomValidity(errMsg);
            this.reportValidity();
            this.value = this.oldValue;
        }
        this.oldValue = this.value;
       
        setInputFilter(document.getElementById("mobile"), function(value) {
            return /^\d{1,11}$/.test(value); // Allow digits and '.' only, using a RegExp.
        }, "Only 10 to 11 digits  are allowed");

        function getFlagImg(the) {
            var img = $('option:selected', the).attr('data-source');
            $('#countryWithPhoneFlagImg').html('<img src="' + img + '">');
        }
    </script>
@endsection
