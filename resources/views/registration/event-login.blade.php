<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">


    <style>

        .login, .image{
            min-height: 100vh;
        }
        .bg-image{background-image: url('{{ asset('images/event/event.png') }}');
        background-size: cover;
        background-position: center center
    }
    .btn{
        background: #FF5C02;
    }


    </style>
</head>
<body>

    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>
             <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">
                     <div class="container">
                         <div class="row">
                             <div class="col-lg-10 col-xl-10 mx-auto">
                                <h5 class="">Welcome</h5>
                                <p>Login to continue</p>
                                 <br>
                                 <form>
                                    <div class="form-group mb-3">
                                        <p>Email</p>
                                         <input id="inputEmail" type="email" placeholder="Your Email" required="" autofocus="" class="form-control"> </div>
                                         <div class="form-group mb-3">
                                            <p>Password</p>
                                            <input id="inputPassword" type="password" placeholder=" Your Password" required="" class="form-control"><br> </div>
                                            <div class="custom-control custom-checkbox mb-3">
                                                <input id="customCheck1" type="checkbox"  class="custom-control-input">
                                                <label for="customCheck1" class="custom-control-label">Remember me</label>
                                                <a href="#" class="ml-auto float-right">Forgot Pin?</a>
                                            </div>
                                            <button type="submit" class="btn btn btn-yellow btn-block text-uppercase mb-2 rounded-pill shadow-sm">Login</button>
                                            <div class="  account text-center d-flex justify-content-between mt-4 ">
                                                <p> Didn't have an account? Signup</p>
                                            </div>
                                         </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
           </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>
</html>
