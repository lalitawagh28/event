@extends('eventmie::layouts.app')
<style>


    .deposit_money_button{
        display: flex;
        gap: 20px;
        justify-content: end;
        margin-top: 24px;
    }
    .deposit_money_button1{
        border-radius: 5px;
        border: 2px solid #FF5C02;
        color: #FF5C02;
        padding: 6 44px;

    }
    .deposit_money_button2{
        background-color: #FF5C02;
        color: white;
        padding: 6 44px;
        border-radius: 5px;
    }
    .profile_forms .card {
    border-radius: 14px;
}

.card h3 {
    border-bottom: 1px solid #333;
    padding-bottom: 5px;
    display: block;
}
.ti-form-label {
        margin-bottom: .5rem;
        display: block;

        line-height: 1.25rem;
        font-weight: 300;
        margin-bottom: 5px;
        color: #000;
        font-size: 18px !important;
    }
    .ti-form-input {
        display: block;
        width: 100%;
        align-items: center;
        padding: .75rem 1rem;
        line-height: 1.25rem;
        margin-right: 10px;
        border-radius: 8px;
        border: 1px solid #999;
    }
    .profile_info_user {
        margin-top: 10px;
    }
    .verify_text{
        margin-left:13px;
        margin-bottom: 20px;
        color:rgb(67, 153, 67);
        font-size: 13px;
        font-weight: 600;
    }
    .verify_resend{
     padding-top: 10px;
      color: #000;
      font-size: 14px;
      font-weight: 600;
    }
</style>


@section('content')


<div class="row">
    <div class="col-md-5 m-auto">

        <div class="profile_forms clearfix">
            <div class="card my-5 p-4">
            <div class="card-header">
                <h3 class="profile_form_title border-b pb-2">Deposits</h3>
            </div>
            <div class="card-body">
               <form>
                <div class="col-md-12 m-auto ">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="verify_text">OTP is sent your registered mobile number. Please enter</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="profile_info_user">
                            <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Mobile Number</label>
                            <div class="input-container col-sm-8">
                                <input type="text" id="input-label" class="ti-form-input"
                                    placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="profile_info_user">
                            <label for="input-label" class="col-sm-4 col-form-label ti-form-label">OTP</label>
                            <div class="input-container col-sm-8">
                                <input type="text" id="input-label" class="ti-form-input"
                                    placeholder="">
                                <a class="verify_resend" >Resend</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-right mt-3 ">
                    <div class="deposit_money_button ">
                        <a class="deposit_money_button1">Previous</a>
                        <a class="deposit_money_button2">Next</a>
                    </div>
                </div>
               </form>
            </div>
            </div>
        </div>
    </div>

</div>

@endsection
@section('javascript')
@stop
