@extends('eventmie::layouts.app')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> <!-- Load jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script> <!-- Owl Carousel JS -->
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.4.2/css/fontawesome.min.css"
    integrity="sha384-BY+fdrpOd3gfeRvTSMT+VUZmA728cfF9Z2G42xpaRkUGu2i3DyzpTURDo5A6CaLK" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
    integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
<style>
    .nav-tabs {
        border-bottom: none !important;
    }

    .tab-card-header {
        border-radius: 5px;
        margin: 20px 40px;
        background: #cdd2e5;
        padding: 3px 0px;

    }

    .nav-item a {
        color: black;
    }

    .button_nav {
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #d3d4d6;
        padding-bottom: 10px;
    }

    .export_button,
    .payout_button {
        background-color: #FF5C02;
        color: white;
        padding: 6 14px;
        border-radius: 5px;
        margin-left: 5px;
    }

    .deposit_content {
        padding: 10px 20px;
        background-color: #f1f2f6;
        margin: 0px 10px;
    }

    .carousel-control-next,
    .carousel-control-prev {
        position: absolute;
        top: 0;
        bottom: 0;
        z-index: 1;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 2%;
        padding: 0;
        color: #020202;
        text-align: center;
        background: 0 0;
        border: 0;
        opacity: 1;
        transition: opacity .15s ease;
    }

    .heart_icon {
        border-radius: 0 !important;
        box-shadow: none !important;
        height: auto !important;
        transition: none !important;
        width:25px;
    }

    .inactive-text {
        color: #020202;
    }

    .active-text {
        color: rgb(23, 213, 23);
    }

    .carousel-control-next-icon,
    .carousel-control-prev-icon {
        color: #020202;
        background-color: #020202;
        padding: 10px;
    }

    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        text-align: center;
        pointer-events: none;
        color: #aaa;
        left: 0;
        top: 38%;
        transform: translate(-12%);
    }

    .filter-serch-section {
        display: flex;
        gap: 7px;
        justify-content: center;
        align-items: center;
    }


    /*modal popup*/
    .overlay {
        height: 100%;
        width: 100%;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 100%;
        background-color: rgb(0, 0, 0);
        background-color: rgb(255 255 255 / 1);
        overflow-x: hidden;
        transition: 0.5s;
        -webkit-box-shadow: -5px -1px 31px -8px rgba(120, 120, 120, 1);
        -moz-box-shadow: -5px -1px 31px -8px rgba(120, 120, 120, 1);
        box-shadow: -5px -1px 31px -8px rgba(120, 120, 120, 1);
    }

    .closebtn {
        font-size: 25px;
        color: black;
        text-align: right;
    }

    .text-header {
        border-bottom: 1px solid rgb(93, 87, 87);
        display: flex;
        align-items: center;
        padding: 5px;
    }

    .deposit_slienttags {
        font-size: 10px;
    }

    .profile_info_userss {
        display: flex;
    }

    .otp_label {
        width: 80px;
    }

    .otp_input {
        width: 307px;
    }

    .resend_otp {
        border-radius: 5px;
        border: 2px solid #FF5C02;
        color: #FF5C02;
        padding: 3 20px;
    }

    .submit_otp {
        background-color: #FF5C02;
        color: white;
        padding: 3 20px;
        border-radius: 5px;
    }

    a.deposit_money_button2 {
        font-weight: 600;
    }

    a.deposit_money_button2:hover {
        color: #1b96f1;
    }

    .priceText {
        color: #0ce622;
        font-size: 26px;
    }

    .flex {
        display: flex;
    }

    .items-center {
        align-items: center;
    }

    .uppercase {
        text-transform: uppercase;
    }

    .font-medium {
        font-weight: 500;
    }

    .md\:ml-4 {
        margin-left: 1rem;
    }

    .text-gray-700 {
        --text-opacity: 1;
        color: #1e293b;
    }

    .truncate.sm\:whitespace-normal {
        color: #1e293b;
        font-weight: 600;
    }

    .truncate.sm\:whitespace-normal svg {
        height: 17px;
        margin-right: 5px;
    }

    .w-2\/5 {
        width: 21%;
    }

    .float-right {
        float: right;
    }

    .filter-bg {
        background-color: #d9d9d9 !important;
        color: #000000;
    }

    .filter-bg a {
        color: #000;
    }

    .closebtn1 i {
        font-size: 22px;
        color: #000000;
        margin-top: 1px;
    }

    .closebtn1 {
        margin-left: 283px;
        display: inline-block;
    }

    .vivek {
        margin-right: 100px;
        width: 40%;
        display: flex;
        position: absolute;
        right: 18%;
        align-items: center;
        min-height: 40px;
    }

    .col-auto.my-1.flex {
        display: flex;
        gap: 10px;
        align-items: center;
        align-self: center;
    }

    .col-auto.my-1.flex label.form-check-label {
        padding: 0 0 0 17px;
        vertical-align: middle;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 13px;
        line-height: 15px;
        font-weight: 500;
    }

    .col-auto.my-1.flex input[type=checkbox],
    input[type=radio] {
        accent-color: #ff5c02;
        margin: 0 3px 0 0 !important;
        width: 16px;
        height: 16px;
    }

    .dropdown-menu.dropdown-submenu.bg-white.show {
        width: 100%;
        border: none;
        box-shadow: none;
        border-radius: 0;
    }

    .date-form {
        border: 1px solid #cfc5c5;
        border-radius: 5px;
        padding: 5px;
    }

    a.text-left.dropdown-toggle.filter-bg.btn.border.text-dark.rounded-md:after {
        float: right;
        top: 50%;
        position: absolute;
        right: 6px;
        top: 50%;
    }

    a.text-left.dropdown-toggle.filter-bg.btn.border.text-dark.rounded-md {
        position: relative;
    }
    .curreny_wallet_icon{
        width:20px !important;
        height:20px !important;
        border-radius: 0px !important;
        box-shadow: none !important;
    }
    .wallet_cursol{
        align-items: center;
        justify-content: space-between;
        gap:52px;
    }
</style>



@section('content')
    <div class="">
        <div class="row">
            <div class="col-12">
                <div class=" mt-3 tab-card">
                    <div class="tab-card-header ">
                        <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="deposit" data-toggle="tab" href="#one" role="tab"
                                    aria-controls="one" aria-selected="true">Deposit</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="payouts" data-toggle="tab" href="#two" role="tab"
                                    aria-controls="two" aria-selected="false">Payouts</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="transfer" data-toggle="tab" href="#three" role="tab"
                                    aria-controls="three" aria-selected="false">Transfer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="withdraw" data-toggle="tab" href="#four" role="tab"
                                    aria-controls="four" aria-selected="false">Withdraw</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="beneficiaries" data-toggle="tab" href="#five" role="tab"
                                    aria-controls="five" aria-selected="false">Beneficiaries</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="transactions" data-toggle="tab" href="#six" role="tab"
                                    aria-controls="six " aria-selected="false">Transactions</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade  active p-3" id="one" role="tabpanel" aria-labelledby="deposit">
                            <div class="deposit_content">
                                <div class="button_nav">
                                    <div>
                                        <h5>Payouts</h5>
                                    </div>
                                    <div class="filter-serch-section">
                                        <div class="form-group has-search mb-0" style="position: relative">
                                            <span class="fa fa-search form-control-feedback ml-2 mb-1"></span>
                                            <input type="text" class="form-control pl-5 form-rounded"
                                                placeholder="Search" style="border-radius: 30px;width:314px;height:47px">
                                        </div>

                                        <a style="font-size:20px; color:#020202" class="px-2"><span
                                                class="glyphicon glyphicon-align-justify"></a>
                                        <a style="font-size:20px; color:#020202" class="px-2"><span
                                                class="glyphicon glyphicon-th-large"></span></a>
                                        <div class="dropdown">
                                            <a class="dropdown-toggle btn bg-white border text-dark rounded-md "
                                                href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                                                Filters
                                            </a>
                                            <div class="dropdown-menu p-2">

                                                <a class="text-left dropdown-toggle filter-bg btn border text-dark rounded-md "
                                                    href="#" role="button" data-toggle="dropdown"
                                                    aria-expanded="false" style="width: 100%;">
                                                    All
                                                </a>
                                                <div class="dropdown-menu dropdown-submenu bg-white">
                                                    <a class="dropdown-item" href="#">Draft</a>
                                                    <a class="dropdown-item" href="#">Pending</a>
                                                    <a class="dropdown-item" href="#">Accpeted</a>
                                                    <a class="dropdown-item" href="#">Pending Confirmation</a>
                                                </div>

                                                <input placeholder="yyyy/mm/dd" class="date-form p-2 mt-2"
                                                    id="datepicker" width="276" />
                                            </div>



                                        </div>
                                        {{-- <button type="button" class="btn btn-default btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-filter"></i>
                                            Filters</button> --}}


                                        <div class="dropdown">
                                            <a class="dropdown-toggle btn bg-white border text-dark rounded-md "
                                                href="#" role="button" data-toggle="dropdown"
                                                aria-expanded="false">
                                                Column
                                            </a>

                                            <div class="dropdown-menu">
                                                <div class="dropdown-item col-auto my-1 flex p-0">
                                                    <input type="checkbox" id="customControlAutosizing"
                                                        name="customControlAutosizing" style="background-color: #FF5C02;">
                                                    <label class="custom-control-label"
                                                        for="customControlAutosizing">Remember </label>
                                                </div>

                                                <div class="dropdown-item col-auto my-1 flex p-0">
                                                    <input type="checkbox" id="customControlAutosizing1"
                                                        name="customControlAutosizing1"
                                                        style="background-color: #FF5C02;">
                                                    <label class="custom-control-label"
                                                        for="customControlAutosizing1">Remember </label>
                                                </div>
                                                <div class="dropdown-item col-auto my-1 flex p-0">
                                                    <input type="checkbox" id="customControlAutosizing2"
                                                        name="customControlAutosizing2"
                                                        style="background-color: #FF5C02;">
                                                    <label class="custom-control-label"
                                                        for="customControlAutosizing2">Remember </label>
                                                </div>
                                                <div class="dropdown-item col-auto my-1 flex p-0">
                                                    <input type="checkbox" id="customControlAutosizing3"
                                                        name="customControlAutosizing3"
                                                        style="background-color: #FF5C02;">
                                                    <label class="custom-control-label"
                                                        for="customControlAutosizing3">Remember </label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-toggle btn bg-white border text-dark rounded-md "
                                                href="#" role="button" data-toggle="dropdown"
                                                aria-expanded="false">
                                                50
                                            </a>

                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#">0</a>
                                                <a class="dropdown-item" href="#">10</a>
                                                <a class="dropdown-item" href="#">25</a>
                                                <a class="dropdown-item" href="#">50</a>
                                                <a class="dropdown-item" href="#">100</a>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-default btn-sm">
                                            <span class="glyphicon glyphicon-print"></span> Print
                                        </button>

                                        <div class="dropdown">
                                            <a class="dropdown-toggle export_button btn-sm text-white border rounded"
                                                href="#" role="button" data-toggle="dropdown"
                                                aria-expanded="false">
                                                Exports
                                            </a>

                                            <div class="dropdown-menu filter-bg">
                                                <a class="dropdown-item" href="#">Export XL SX</a>
                                            </div>
                                        </div>
                                        <a class="payout_button btn-sm" href="{{ route('deposit_form') }}">Payouts</a>
                                    </div>
                                </div>
                                <div class="table-responsive" style="padding-top:5px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col"> <input type="checkbox" id="yourCheckboxId"
                                                        name="yourCheckboxName" style="background-color: #FF5C02;"></th>
                                                <th scope="col">Transaction Id</th>
                                                <th scope="col">Date & Time</th>
                                                <th scope="col">Sender Name</th>
                                                <th scope="col">Receiver Name</th>
                                                <th scope="col">Payment Method</th>
                                                <th scope="col">Debit</th>
                                                <th scope="col">Credit</th>
                                                <th scope="col">Balance</th>
                                                <th scope="col">User Status</th>
                                                <th scope="col">admin Status</th>
                                                <th scope="col">Type</th>
                                                <th scope="col">Reference</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row" style="padding-top:8px;"> <input type="checkbox"
                                                        id="yourCheckboxId" name="yourCheckboxName"
                                                        style="background-color: #FF5C02; color: #FF5C02;"></th>
                                                <td><a class="deposit_money_button2" style="float:right;"
                                                        onclick="openNav()"><u>555555444411</u></a></td>
                                                <td>14-12-2023 06:28</td>
                                                <td>pakhi Kapadi</td>
                                                <td>pakhi Kapadi</td>
                                                <td>Manual Transfer</td>
                                                <td class="text-danger">P1:00</td>
                                                <td class="text-primary">V99:27</td>
                                                <td>V99:27</td>
                                                <td>Accepted</td>
                                                <td>Accepted</td>
                                                <td>Deposit</td>
                                                <td>Test</td>
                                                <td><img src="{{ asset('images/setting_icon.png') }}" width="25px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="payouts">
                            <div class="deposit_content">
                                <div class="button_nav">
                                    <div>
                                        <h5>Payouts</h5>
                                    </div>
                                    <div>
                                        <a class="export_button">Exports</a>
                                        <a class="payout_button" href="{{ route('payouts-ac-create') }}">Payouts</a>
                                    </div>
                                </div>
                                <div class="table-responsive" style="padding-top:5px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col"> <input type="checkbox" id="yourCheckboxId"
                                                        name="yourCheckboxName" style="background-color: #FF5C02;"></th>
                                                <th scope="col">Transaction Id</th>
                                                <th scope="col">Date & Time</th>
                                                <th scope="col">Sender Name</th>
                                                <th scope="col">Receiver Name</th>
                                                <th scope="col">Wallet</th>
                                                <th scope="col">Debit</th>
                                                <th scope="col">Credit</th>
                                                <th scope="col">Balance</th>
                                                <th scope="col">User Status</th>
                                                <th scope="col">Reference</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row" style="padding-top:8px;"> <input type="checkbox"
                                                        id="yourCheckboxId" name="yourCheckboxName"
                                                        style="background-color: #FF5C02; color: #FF5C02;"></th>
                                                <td><a><u>555555444411</u></a></td>
                                                <td>14-12-2023 06:28</td>
                                                <td>pakhi Kapadi</td>
                                                <td>pakhi Kapadi</td>
                                                <td>INR</td>
                                                <td class="text-danger">P1:00</td>
                                                <td class="text-primary"></td>
                                                <td>V99:27</td>
                                                <td>Accepted</td>
                                                <td>Test</td>
                                                <td><img src="{{ asset('images/setting_icon.png') }}" width="25px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="transfer">
                            <div class="deposit_content">
                                <div class="button_nav">
                                    <div>
                                        <h5>Payouts</h5>
                                    </div>
                                    <div>
                                        <a class="export_button">Exports</a>
                                        <a class="payout_button" href="{{ route('transfer-ac-create') }}">Payouts</a>
                                    </div>
                                </div>
                                <div class="table-responsive" style="padding-top:5px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col"> <input type="checkbox" id="yourCheckboxId"
                                                        name="yourCheckboxName" style="background-color: #FF5C02;"></th>
                                                <th scope="col">Transaction Id</th>
                                                <th scope="col">Date & Time</th>
                                                <th scope="col">Sender Name</th>
                                                <th scope="col">Receiver Name</th>
                                                <th scope="col">Payment Method</th>
                                                <th scope="col">Debit</th>
                                                <th scope="col">Credit</th>
                                                <th scope="col">Balance</th>
                                                <th scope="col">User Status</th>
                                                <th scope="col">admin Status</th>
                                                <th scope="col">Type</th>
                                                <th scope="col">Reference</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row" style="padding-top:8px;"> <input type="checkbox"
                                                        id="yourCheckboxId" name="yourCheckboxName"
                                                        style="background-color: #FF5C02; color: #FF5C02;"></th>
                                                <td><a><u>555555444411</u></a></td>
                                                <td>14-12-2023 06:28</td>
                                                <td>pakhi Kapadi</td>
                                                <td>pakhi Kapadi</td>
                                                <td>Manual Transfer</td>
                                                <td class="text-danger">P1:00</td>
                                                <td class="text-primary">V99:27</td>
                                                <td>V99:27</td>
                                                <td>Accepted</td>
                                                <td>Accepted</td>
                                                <td>Deposit</td>
                                                <td>Test</td>
                                                <td><img src="{{ asset('images/setting_icon.png') }}" width="25px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="four" role="tabpanel" aria-labelledby="withdraw">
                            <div class="deposit_content">
                                <div class="button_nav">
                                    <div>
                                        <h5>Payouts</h5>
                                    </div>

                                    <div class="filter-serch-section">


                                        <div class="form-group has-search mb-0" style="position: relative">
                                            <span class="fa fa-search form-control-feedback ml-2 mb-1"></span>
                                            <input type="text" class="form-control pl-5 form-rounded"
                                                placeholder="Search" style="border-radius: 30px;width:314px;height:47px">
                                        </div>

                                        <a style="font-size:20px; color:#020202" class="px-2"><span
                                                class="glyphicon glyphicon-align-justify"></a>
                                        <a style="font-size:20px; color:#020202" class="px-2"><span
                                                class="glyphicon glyphicon-th-large"></span></a>
                                        <button type="button" class="btn btn-default btn-sm"> <i
                                                class="fa fa-filter"></i> Filters</button>
                                        <button type="button" class="btn btn-default btn-sm">
                                            <span class="glyphicon glyphicon-print"></span> Print
                                        </button>
                                        <label class="text-sm">column</label>
                                        <select>
                                            <option>10</option>
                                        </select>
                                        <a class="export_button">Exports</a>
                                        <a class="payout_button" href="{{ route('withdraw-ac-create') }}">Payouts</a>
                                    </div>
                                </div>
                                <div class="table-responsive" style="padding-top:5px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col"> <input type="checkbox" id="yourCheckboxId"
                                                        name="yourCheckboxName" style="background-color: #FF5C02;"></th>
                                                <th scope="col">Transaction Id</th>
                                                <th scope="col">Date & Time</th>
                                                <th scope="col">Sender Name</th>
                                                <th scope="col">Receiver Name</th>
                                                <th scope="col">Wallet</th>
                                                <th scope="col">Debit</th>
                                                <th scope="col">Credit</th>
                                                <th scope="col">Balance</th>
                                                <th scope="col">User Status</th>
                                                <th scope="col">Reference</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row" style="padding-top:8px;"> <input type="checkbox"
                                                        id="yourCheckboxId" name="yourCheckboxName"
                                                        style="background-color: #FF5C02; color: #FF5C02;"></th>
                                                <td><a><u>555555444411</u></a></td>
                                                <td>14-12-2023 06:28</td>
                                                <td>pakhi Kapadi</td>
                                                <td>pakhi Kapadi</td>
                                                <td>INR</td>
                                                <td class="text-danger">P1:00</td>
                                                <td class="text-primary">V99:27</td>
                                                <td>V99:27</td>
                                                <td>Pending-confirmation</td>
                                                <td>Test</td>
                                                <td><img src="{{ asset('images/setting_icon.png') }}" width="25px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="five" role="tabpanel" aria-labelledby="beneficiaries">
                            <div class="deposit_content">
                                <div class="button_nav">
                                    <div>
                                        <h5>Payouts</h5>
                                    </div>
                                </div>
                                <div class="table-responsive" style="padding-top:5px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Email Address</th>
                                                <th scope="col">Mobile Number</th>
                                                <th scope="col">Bank Sort Code</th>
                                                <th scope="col">Bank Account No</th>
                                                <th scope="col">Type</th>
                                                <th scope="col">Category</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Email@gmail.com</td>
                                                <td>9542114178</td>
                                                <td></td>
                                                <td>55444545</td>
                                                <td>INR</td>
                                                <td>Personal</td>
                                                <td>Bank Transfer</td>
                                                <td><img src="{{ asset('images/setting_icon.png') }}" width="25px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="six" role="tabpanel" aria-labelledby="transactions">
                            <div class="deposit_content">
                                <div id="carouselExampleInterval" class="carousel slide m-5" data-ride="carousel">
                                    <div class="carousel-inner jumbotron">
                                        <div class="carousel-item active" data-interval="10000">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="bg-white p-3 clearfix">
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <div class="flex w-full my-3">
                                                                        <div class="flex wallet_cursol">
                                                                        <h4 class="float-left">Ripple</h4>
                                                                        <a><img src="{{ asset('images/wallet_icon1.png') }}" class="curreny_wallet_icon"></a>
                                                                        </div>
                                                                        <span class="float-right ml-auto"><img
                                                                                src="{{ asset('images/heart.png') }}"
                                                                                class="heart_icon"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <strong>125487965874521</strong>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="flex mb-3"
                                                                        style="display: flex; justify-content: space-between;">
                                                                        <a class="active-text" href="#">Active</a>
                                                                        <a href="#"
                                                                            class="inactive-text">Inactive</a>
                                                                        <span class="">12.00</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="bg-white p-3 clearfix">
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <div class="flex w-full my-3">
                                                                        <div class="flex wallet_cursol">
                                                                        <h4 class="float-left">Ripple</h4>
                                                                        <a><img src="{{ asset('images/wallet_icon2.png') }}" class="curreny_wallet_icon"></a>
                                                                        </div>
                                                                        <span class="float-right ml-auto"><img
                                                                                src="{{ asset('images/heart.png') }}"
                                                                                class="heart_icon"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <strong>125487965874521</strong>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="flex mb-3"
                                                                        style="display: flex; justify-content: space-between;">
                                                                        <a class="active-text" href="#">Active</a>
                                                                        <a href="#"
                                                                            class="inactive-text">Inactive</a>
                                                                        <span class="">12.00</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="bg-white p-3 clearfix">
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <div class="flex w-full my-3">
                                                                        <div class="flex wallet_cursol">
                                                                        <h4 class="float-left">Ripple</h4>
                                                                        <a><img src="{{ asset('images/wallet_icon3.png') }}" class="curreny_wallet_icon"></a>
                                                                        </div>
                                                                        <span class="float-right ml-auto"><img
                                                                                src="{{ asset('images/heart.png') }}"
                                                                                class="heart_icon"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <strong>125487965874521</strong>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="flex mb-3"
                                                                        style="display: flex; justify-content: space-between;">
                                                                        <a class="active-text" href="#">Active</a>
                                                                        <a href="#"
                                                                            class="inactive-text">Inactive</a>
                                                                        <span class="">12.00</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carousel-item" data-interval="2000">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="bg-white p-3 clearfix">
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <div class="flex w-full my-3">
                                                                        <div class="flex wallet_cursol">
                                                                        <h4 class="float-left">Ripple</h4>
                                                                        <a><img src="{{ asset('images/wallet_icon4.png') }}" class="curreny_wallet_icon"></a>
                                                                        </div>
                                                                        <span class="float-right ml-auto"><img
                                                                                src="{{ asset('images/heart.png') }}"
                                                                                class="heart_icon"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <strong>125487965874521</strong>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="flex mb-3"
                                                                        style="display: flex; justify-content: space-between;">
                                                                        <a class="active-text" href="#">Active</a>
                                                                        <a href="#"
                                                                            class="inactive-text">Inactive</a>
                                                                        <span class="">12.00</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="bg-white p-3 clearfix">
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <div class="flex w-full my-3">
                                                                        <div class="flex wallet_cursol">
                                                                        <h4 class="float-left">Ripple</h4>
                                                                        <a><img src="{{ asset('images/wallet_icon5.png') }}" class="curreny_wallet_icon"></a>
                                                                        </div>
                                                                        <span class="float-right ml-auto"><img
                                                                                src="{{ asset('images/heart.png') }}"
                                                                                class="heart_icon"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <strong>125487965874521</strong>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="flex mb-3"
                                                                        style="display: flex; justify-content: space-between;">
                                                                        <a class="active-text" href="#">Active</a>
                                                                        <a href="#"
                                                                            class="inactive-text">Inactive</a>
                                                                        <span class="">12.00</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="bg-white p-3 clearfix">
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <div class="flex w-full my-3">
                                                                        <div class="flex wallet_cursol">
                                                                        <h4 class="float-left">Ripple</h4>
                                                                        <a><img src="{{ asset('images/wallet_icon6.png') }}" class="curreny_wallet_icon"></a>
                                                                        </div>
                                                                        <span class="float-right ml-auto"><img
                                                                                src="{{ asset('images/heart.png') }}"
                                                                                class="heart_icon"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 mb-3 clearfix">
                                                                    <strong>125487965874521</strong>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="flex mb-3"
                                                                        style="display: flex; justify-content: space-between;">
                                                                        <a class="active-text" href="#">Active</a>
                                                                        <a href="#"
                                                                            class="inactive-text">Inactive</a>
                                                                        <span class="">12.00</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="carousel-control-prev" type="button"
                                        data-target="#carouselExampleInterval" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </button>
                                    <button class="carousel-control-next" type="button"
                                        data-target="#carouselExampleInterval" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </button>
                                </div>
                                <div class="button_nav">
                                    <div>
                                        <h5>All</h5>
                                    </div>
                                    <div>
                                        <a class="export_button">Exports</a>
                                    </div>
                                </div>
                                <div class="table-responsive" style="padding-top:5px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col"> <input type="checkbox" id="yourCheckboxId"
                                                        name="yourCheckboxName" style="background-color: #FF5C02;"></th>
                                                <th scope="col">Transaction Id</th>
                                                <th scope="col">Date & Time</th>
                                                <th scope="col">Sender Name</th>
                                                <th scope="col">Receiver Name</th>
                                                <th scope="col">Wallet</th>
                                                <th scope="col">Debit</th>
                                                <th scope="col">Credit</th>
                                                <th scope="col">Balance</th>
                                                <th scope="col">User Status</th>
                                                <th scope="col">Admin Status</th>
                                                <th scope="col">Type</th>
                                                <th scope="col">Reference</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row" style="padding-top:8px;"> <input type="checkbox"
                                                        id="yourCheckboxId" name="yourCheckboxName"
                                                        style="background-color: #FF5C02; color: #FF5C02;"></th>
                                                <td><a><u>555555444411</u></a></td>
                                                <td>14-12-2023 06:28</td>
                                                <td>pakhi Kapadi</td>
                                                <td>pakhi Kapadi</td>
                                                <td>INR</td>
                                                <td class="text-danger">P1:00</td>
                                                <td class="text-primary"></td>
                                                <td>V99:27</td>
                                                <td>Accepted</td>
                                                <td>Accepted</td>
                                                <td>Depoist</td>
                                                <td>Test</td>
                                                <td><img src="{{ asset('images/setting_icon.png') }}" width="25px">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- off canvas-->
    <div id="myNav1" class="overlay" style="">

        <div class="overlay-content">
            {{-- <div class="text-block">
                                  <div class="text-header">
                                    <h5>OTP Verification</h5>
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                  </div>
                                  <div class="content_main_overlay mt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="profile_info_userss">
                                                <label for="input-label" class="otp_label col-form-label ti-form-label">Otp<span class="text-danger">*</span></label>
                                                  <div>
                                                    <input type="text" id="input-label" class="otp_input ti-form-input"
                                                        placeholder=""><br>
                                                        <span class="deposit_slienttags pt-3">Please Check OTP Sent to your Mobile . It will Expire in 10 Minutes</span>
                                                  </div>


                                            </div>
                                        </div>
                                    </div>
                                  </div>
                              </div> --}}
            {{-- <div class="col-md-3  mt-2 ">
                                <div class="deposit_money_button ">
                                    <a class="resend_otp">Resend OTP</a>
                                    <a class="submit_otp" href="{{ route('deposit_card_details') }}" >Submit</a>
                                </div>
                            </div> --}}

            <div class="modal-content">

                <div class="text-header">
                    <h5>Transaction Details</h5>
                    <a class="closebtn1 mr-3"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                    <a href="javascript:void(0)" class="closebtn mr-3" onclick="closeNav()"><i class="fa fa-close"
                            aria-hidden="true"></i></a>
                </div>
                <div class="modal-body">
                    <div>
                        <div>
                            <div class="flex flex-col lg:flex-row px-1 sm:px-2 py-0 mb-2">
                                <div class="dark:text-theme-10">
                                    <p class="text-xl font-medium  ">
                                        <strong class="priceText">V 99.77</strong>
                                        <span class="text-sm font-medium text-gray-700 md:ml-4">Deposit / Accepted</span>
                                    </p>
                                </div>
                            </div>

                            <div class="flex flex-col lg:flex-row px-1 sm:px-2 py-0 mb-2">
                                <div class="dark:text-theme-10">
                                    <h3 class="text-theme-1 dark:text-theme-10 font-semibold text-2xl mb-2">
                                        Pakhi Kapadia</h3>
                                    <p class="text-sm font-medium text-gray-700">14122022068839</p>
                                </div>
                            </div>


                            <div class="mt-5">
                                <p class="text-sm tracking-wide font-medium">Basic Details</p>

                                <div class="flex flex-col lg:flex-row mt-3">
                                    <div class="truncate sm:whitespace-normal flex items-center">
                                        <svg height="12" xmlns="http://www.w3.org/2000/svg" width="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe">
                                            <circle cx="12" cy="12" r="10"></circle>
                                            <line x1="2" y1="12" x2="22" y2="12">
                                            </line>
                                            <path
                                                d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z">
                                            </path>
                                        </svg>
                                        <span>
                                            Stripe
                                        </span>
                                    </div>

                                    <div class="truncate sm:whitespace-normal flex items-center md:ml-4">
                                        <svg height="12" xmlns="http://www.w3.org/2000/svg" width="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock">
                                            <circle cx="12" cy="12" r="10"></circle>
                                            <polyline points="12 6 12 12 16 14"></polyline>
                                        </svg>
                                        <span>
                                            07-12-2022 12:40(GMT)
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="flex">
                                <div class="mt-5 w-2/5">
                                    <p class="text-sm tracking-wide font-medium">Secder Currency</p>

                                    <div class="flex flex-col lg:flex-row mt-3">
                                        <div class="truncate sm:whitespace-normal sm:w-4/5 w-auto flex items-center">
                                            <svg height="12" xmlns="http://www.w3.org/2000/svg" width="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-send">
                                                <line x1="22" y1="2" x2="11" y2="13">
                                                </line>
                                                <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                            </svg>
                                            <span>
                                                VIP COIN
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-5 float-right w-2/5 text-sm break-all">
                                    <p class="text-sm tracking-wide font-medium">Receiver Currency</p>

                                    <div class="flex flex-col lg:flex-row mt-3">
                                        <div class="truncate sm:whitespace-normal sm:w-4/5 w-auto flex items-center">
                                            <svg height="12" xmlns="http://www.w3.org/2000/svg" width="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-pocket">
                                                <path
                                                    d="M4 3h16a2 2 0 0 1 2 2v6a10 10 0 0 1-10 10A10 10 0 0 1 2 11V5a2 2 0 0 1 2-2z">
                                                </path>
                                                <polyline points="8 10 12 14 16 10"></polyline>
                                            </svg>
                                            <span>
                                                VIP COIN
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="flex">
                                <div class="mt-5 w-2/5">
                                    <p class="text-sm tracking-wide font-medium">Sender Account</p>

                                    <div class="flex flex-col lg:flex-row mt-3">
                                        <div class="truncate sm:whitespace-normal sm:w-4/5 w-auto flex items-center">
                                            <svg height="12" xmlns="http://www.w3.org/2000/svg" width="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-send">
                                                <line x1="22" y1="2" x2="11" y2="13">
                                                </line>
                                                <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                            </svg>
                                            <span>
                                                Pakhi Kapadia
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-5 float-right w-2/5 text-sm break-all">
                                    <p class="text-sm tracking-wide font-medium">Beneficiary Account</p>

                                    <div class="flex flex-col lg:flex-row mt-3">
                                        <div class="truncate sm:whitespace-normal sm:w-4/5 w-auto flex items-center">
                                            <svg height="12" xmlns="http://www.w3.org/2000/svg" width="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-pocket">
                                                <path
                                                    d="M4 3h16a2 2 0 0 1 2 2v6a10 10 0 0 1-10 10A10 10 0 0 1 2 11V5a2 2 0 0 1 2-2z">
                                                </path>
                                                <polyline points="8 10 12 14 16 10"></polyline>
                                            </svg>
                                            <span>
                                                Pakhi Kapadia
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="mt-5">
                                <p class="text-sm tracking-wide font-medium">Reference</p>

                                <div class="flex break-all flex-col lg:flex-row mt-3">
                                    <div class="truncate sm:whitespace-normal flex items-center">
                                        <span>
                                            test
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="saved-transaction">
                            </div>

                            <div class="edit-transaction-content hidden">
                                <form id="transaction-form"
                                    action="https://dev.kanexy.com/dashboard/banking/transactions/631" method="POST"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token"
                                        value="rMRCLm59XwWRjljBlxRoDLaWADlKU6MJ43fBFu3L"> <input type="hidden"
                                        name="_method" value="PUT">
                                    <div class="mt-5">
                                        <p class="text-sm tracking-wide font-medium uppercase">Attachment</p>

                                        <div class="flex flex-col lg:flex-row mt-3">
                                            <div class="truncate sm:whitespace-normal flex items-center">
                                                <input type="file" id="attachment" name="attachment"
                                                    class="ml-2 w-full">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mt-5">
                                        <p class="text-sm tracking-wide font-medium uppercase">Note</p>

                                        <div class="flex flex-col lg:flex-row mt-3">
                                            <div class="truncate sm:whitespace-normal flex items-center">
                                                <textarea id="note" name="note" class="form-control w-full" value=""></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>


                        </div>
                        <script>
                            $(".edit-transaction").removeClass('hidden');
                            $(".edit-transaction").addClass('flex');
                            $(".save-transaction").addClass('hidden');
                            $(".save-transaction").click(function() {
                                $("#transaction-form").submit();
                            });
                        </script>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>

    <script>
        function openNav() {
            document.getElementById("myNav1").style.left = "60%";
        }

        function closeNav() {
            document.getElementById("myNav1").style.left = "100%";
        }
    </script>
@stop
