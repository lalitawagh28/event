@extends('eventmie::layouts.app')
<style>
    <style>
    .ti-form-label {
        margin-bottom: .5rem;
        display: block;

        line-height: 1.25rem;
        font-weight: 300;
        margin-bottom: 5px;
        color: #000;
        font-size: 18px !important;
    }
    .ti-form-input {
        display: block;
        width: 100%;
        align-items: center;
        padding: .75rem 1rem;
        line-height: 1.25rem;
        margin-right: 10px;
        border-radius: 8px;
        border: 1px solid #999;
    }

    .profile_save {
        padding: 8px 236px 12px 236px;
        align-items: center;
        flex-shrink: 0;
        border-radius: 8px;
        background: #FF5C02;
        color: white;
    }
    .profile_info_user {
        margin-top: 10px;
    }
    .profile_form_title {
        color: #000;
        font-size: 23px;
        font-weight: 600;
        margin-bottom: 20px;
    }
    .profile_forms .card {
    border-radius: 14px;
}

.card h3 {
    border-bottom: 1px solid #333;
    padding-bottom: 5px;
    display: block;
}
.deposit_money_button2{
        background-color: #FF5C02;
        color: white;
        padding: 8 54px;
        border-radius: 5px;
    }

    .deposit_slienttag{
        font-size:10px;
        line-height: 15px;

    }
    .payout{
        display: flex;
        align-items: center;
    }


    .overlay {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 100%;
    background-color: rgb(0, 0, 0);
    background-color: rgb(255 255 255 / 1);
    overflow-x: hidden;
    transition: 0.5s;
    -webkit-box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
    -moz-box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
    box-shadow: -5px -1px 31px -8px rgba(120,120,120,1);
}

.closebtn{
    font-size: 25px;
    color: black;
    text-align: right;
    margin-left: 283px;
}
.text-header{
    border-bottom: 1px solid rgb(93, 87, 87);
    display: flex;
    align-items: center;
    padding: 5px;
}
.deposit_slienttags{
    font-size:10px;
}
.profile_info_userss{
    display:flex;
}
.otp_label{
    width:100px;
}
.otp_input{
    width:307px;
}
.resend_otp{
    border-radius: 5px;
    border: 2px solid #FF5C02;
    color: #FF5C02;
    padding: 3 20px;
}
.submit_otp{
    background-color: #FF5C02;
    color: white;
    padding: 3 20px;
    border-radius: 5px;
}
.deposit_money_button{
    text-align: right;
}


</style>


@section('content')
<div class="container">
<div class="row">
    <div class="col-md-12 m-auto">

        <div class="profile_forms clearfix">
            <div class="card my-5 p-4">
            <div class="card-header">
                <h3 class="profile_form_title border-b pb-2">Payouts</h3>
            </div>
            <div class="card-body">
            <form>

                <div class="col-md-12 m-auto">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Payout Form <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <select  class="ti-form-input" placeholder="Select Deposit To">
                                            <option>INR</option>
                                            <option>GBP</option>
                                            <option>VIP</option>
                                            <option>Bit Coin</option>
                                            <option>Ripple Coin</option>
                                          </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Balance</label>
                                    <div class="input-container col-sm-8">
                                        <input type="text" id="input-label" class="ti-form-input"
                                            placeholder="">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user payout">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Beneficiary <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8 ">
                                        <select  class="ti-form-input" placeholder="Select Deposit To">
                                            <option>Lalita</option>
                                            <option>SR.Developer</option>
                                        </select>

                                    </div>

                                    <img src="{{ asset('images/add_icon.png') }}" width="20px" style="float:right;" onclick="openNav()">
                                      <!-- off canvas-->
                                    <div id="myNav" class="overlay" style="">

                                        <div class="overlay-content">
                                        <div class="text-block">
                                            <div class="text-header">
                                                <h5>Beneficiary</h5>
                                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                            </div>
                                            <div class="content_main_overlay mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="profile_info_userss">
                                                            <label for="input-label" class="otp_label col-form-label ti-form-label">Name <span class="text-danger">*</span></label>
                                                            <div>
                                                                <input type="text" id="input-label" class="otp_input ti-form-input"
                                                                    placeholder="First Name">
                                                            </div>
                                                            <div>
                                                                <input type="text" id="input-label" class="otp_input ti-form-input"
                                                                    placeholder="Middle Name">
                                                            </div>
                                                            <div>
                                                                <input type="text" id="input-label" class="otp_input ti-form-input"
                                                                    placeholder="Last Name">
                                                            </div>



                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content_main_overlay mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="profile_info_userss">
                                                            <label for="input-label" class="otp_label col-form-label ti-form-label">Mobile <span class="text-danger">*</span></label>
                                                            <div>
                                                                <input type="text" id="input-label" class="otp_input ti-form-input"
                                                                    placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content_main_overlay mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="profile_info_userss">
                                                            <label for="input-label" class="otp_label col-form-label ti-form-label">Email </label>
                                                            <div>
                                                                <input type="text" id="input-label" class="otp_input ti-form-input"
                                                                    placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content_main_overlay mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="profile_info_userss">
                                                            <label for="input-label" class="otp_label col-form-label ti-form-label">Notes</label>
                                                            <div>
                                                                <input type="text" id="input-label" class="otp_input ti-form-input"
                                                                    placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content_main_overlay mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="profile_info_userss">
                                                            <label for="input-label" class="otp_label col-form-label ti-form-label">Nick Name</label>
                                                            <div>
                                                                <input type="text" id="input-label" class="otp_input ti-form-input"
                                                                    placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3  mt-2 ">
                                            <div class="deposit_money_button ">
                                                <a class="submit_otp" >Create</a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <!-- off canvas end-->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Mobile</label>
                                    <div class="input-container col-sm-8">
                                        <input type="text" id="input-label" class="ti-form-input"
                                            placeholder="">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Amount to Pay <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <input type="text" id="input-label" class="ti-form-input"
                                            placeholder="">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Remaining</label>
                                    <div class="input-container col-sm-8">
                                        <input type="text" id="input-label" class="ti-form-input"
                                            placeholder="0.00">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Payout To<span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <select  class="ti-form-input" placeholder="Select Deposit To">
                                            <option>INR</option>
                                            <option>GBP</option>
                                          </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Reference <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <input type="text" id="input-label" class="ti-form-input"
                                            placeholder="0.00">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Reference <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <input type="text" id="input-label" class="ti-form-input"
                                            placeholder="Naveen kumar">
                                            <span class="deposit_slienttag pt-3">Ex.Fees: 0.20, Ex Rate: 1 INR = 1.00 GBP</span><br>
                                            <span class="deposit_slienttag">Debit:10.00 INR , Credit: 9.80 GBP</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="profile_info_user">
                                    <label for="input-label" class="col-sm-4 col-form-label ti-form-label">Attachment <span class="text-danger">*</span></label>
                                    <div class="input-container col-sm-8">
                                        <input type="file" class="ti-form-input" id="exampleFormControlFile1">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right mt-3">
                            <div class="deposit_next_button mt-4 mr-4">
                                <a class="deposit_money_button2">Next</a>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
            </div>
            </div>
        </div>
    </div>

</div>
</div>

@endsection
@section('javascript')
<script>
    function openNav() {
      document.getElementById("myNav").style.left = "70%";
    }

    function closeNav() {
      document.getElementById("myNav").style.left = "100%";
    }
    </script>
@stop
