<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">


    <style>
        .main-table {
            margin: 20px;
        }

        table.table {
            border: none;
        }

        .table-header {
            background: #949191a2;
        }



        .table-body {
            background-color: #D4D4D4;
        }

        .booking-btn {
            background: #00B289;
            padding: 10PX 10PX 10px 10px;
            color: #fff;
            border: none;
            cursor: pointer;
            display: inline-block;
            margin: 5px;
            margin-bottom: 5px;
            border-radius: 10px;

        }
        .buttons-column {
            display: flex;
            flex-direction: column;
        }
        .cancel-btn{
            border-radius: 10px;
            background: #FF494A;
            cursor: pointer;
            display: inline-block;
            margin: 5px;
            color: #D4D4D4;

        }
        .check-btn{
            border-radius: 10px;
            background: #FF5C02;
            padding: 5PX   5px 5px 5PX;
            cursor: pointer;
            border: none;
            color: #D4D4D4;
            padding: 10px;
        }
    </style>
</head>

<body>
    <div class="main-table">
        <table class="table">
            <thead class="table-header">
                <tr>
                    <th scope="col"><input type="checkbox"></th>
                    <th scope="col">Order Number</th>
                    <th scope="col">Event</th>
                    <th scope="col">Total Tickets</th>
                    <th scope="col">Order Total</th>
                    <th scope="col">Discount</th>
                    <th scope="col">Booked On</th>
                    <th scope="col">Payment</th>
                    <th scope="col">Checked In</th>
                    <th scope="col">Status</th>
                    <th scope="col"> Ticket Status</th>
                    <th scope="col">Cancellation</th>
                    <th scope="col">Expired</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody class="table-body">
                <tr>
                    <td scope="col"><input type="checkbox"></td>
                    <td>#1692101654653149</td>
                    <th>Night Spirts<br />
                        Timings: 04 Nov,2023 02:02 AM-11:00 AM (IST)</th>
                    <td>Gold X 1</td>
                    <td>107.50GBP</td>
                    <td>0GBP</td>
                    <td>03 Nov 2023</td>
                    <td>Online</td>
                    <td>No</td>
                    <td>Enabled</td>
                    <td>Ticket Booked</td>
                    <td class="cancel-btn">Cancel</td>
                    <td>No</td>
                    <td>
                    <div class="buttons-column">
                        <button class="booking-btn">Ticket</button>
                        <button class="booking-btn">Invoice</button>
                        <button class="check-btn">Check In</button>
                    </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>

</body>

</html>
