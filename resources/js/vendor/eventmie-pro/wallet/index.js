

/**
 * This is a page specific seperate vue instance initializer
 */

// include vue common libraries, plugins and components
require('../vue_common');

/* Instances */
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist'

window.Vuex = Vuex;
Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
});

/**
 * Local Imports
*/

Vue.component('Multiselect', require('vue-multiselect').default);

/**
 * Local Components 
 */

import DepositList from './components/DepositList';
import PayoutsList from './components/PayoutsList';
import TransfersList from './components/TransfersList';
import WithdrawsList from './components/WithdrawsList';
import BenificiariesList from './components/BenificiariesList';
import TransactionsList from './components/TransactionsList';
import DepositForm from './components/DepositForm';
import DepositDetails from './components/DepositDetails';

/**
 * Store
 */
const store = new Vuex.Store({
    plugins: [vuexLocal.plugin],
    state: {
        is_dirty     : false,
        is_submit    : false,
        transaction   : null,
    },
    mutations: {
        add(state, { is_dirty,is_submit,transaction}) {
           

            if(typeof is_dirty !== "undefined") {
                state.is_dirty = is_dirty;
            }

            if(typeof is_submit !== "undefined") {
                state.is_submit = is_submit;
            }

            if(typeof transaction !== "undefined") {
                state.transaction = transaction;
            }


        }
    },
});

/**
 * Local Vue Routes 
 */
const routes = new VueRouter({
    linkActiveClass: "active", 
    linkExactActiveClass: 'active',
    // hashbang: false,
    //history: true,
    // mode: "history",
    routes: [
        {
            path: '/',
            name: 'deposit_list',
            component: DepositList,
            props: (route) => ({
                page: route.query.page,
                wallets: wallets,
                transactions:transactions,
            }),
            beforeEnter(to, from, next) {
                routeBeforeEnter(to, from, next);
            },
        },
        {
            path: '/payouts_list',
            name: 'payouts_list',
            component: PayoutsList,
            props: (route) => ({
                page: route.query.page,
                wallets: wallets,
                transactions:transactions,
            }),
            beforeEnter(to, from, next) {
                routeBeforeEnter(to, from, next);
            },
        },
        {
            path: '/transfers_list',
            name: 'transfers_list',
            component: TransfersList,
            props: (route) => ({
                page: route.query.page,
                wallets: wallets,
                transactions:transactions,
            }),
            beforeEnter(to, from, next) {
                routeBeforeEnter(to, from, next);
            },
        },
        {
            path: '/withdraws_list',
            name: 'withdraws_list',
            component: WithdrawsList,
            props: (route) => ({
                page: route.query.page,
                wallets: wallets,
                transactions:transactions,
            }),
            beforeEnter(to, from, next) {
                routeBeforeEnter(to, from, next);
            },
        },
        {
            path: '/benificiaries_list',
            name: 'benificiaries_list',
            component: BenificiariesList,
            props: (route) => ({
                page: route.query.page,
                wallets: wallets,
                transactions:transactions,
            }),
            beforeEnter(to, from, next) {
                routeBeforeEnter(to, from, next);
            },
        },
        {
            path: '/transactions_list',
            name: 'transactions_list',
            component: TransactionsList,
            props: (route) => ({
                page: route.query.page,
                wallets: wallets,
                transactions:transactions,
            }),
            beforeEnter(to, from, next) {
                routeBeforeEnter(to, from, next);
            },
        },
        {
            path: '/deposit_form',
            name: 'deposit_form',
            component: DepositForm,
            props: (route) => ({
                wallets: wallets,
            }),
            beforeEnter(to, from, next) {
                routeBeforeEnter(to, from, next);
            },
        },
        {
            path: '/deposit_details',
            name: 'deposit_details',
            component: DepositDetails,
            props: true
        },
    ],
});


/* Route configs */
function routeBeforeEnter(to, from, next) {
    // except refresh
    if(from.name !== null) {
        // don't switch if no is_event_id
        if(to.name == 'deposit_details' && !store.state.is_submit) {
            Vue.$confirm({
                title: trans('em.required'),
                message: trans('em.please_fill_details'),
                button: {
                    yes: trans('em.cancel'),
                },
                callback: confirm => {
                    next(false);
                }
            });

            next(false);

         } else {
            next();
         }
    } else {
        next();
    }
};

/**
 * This is where we finally create a page specific
 * vue instance with required configs
 * element=app will remain common for all vue instances
 *
 * make sure to use window.app to make new Vue instance
 * so that we can access vue instance from anywhere
 * e.g interceptors 
 */
window.app = new Vue({
    el: '#eventmie_app',
    store: store,
    router: routes,
});