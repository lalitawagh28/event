
/**
 * This is a page specific seperate vue instance initializer
 */

// include vue common libraries, plugins and components
require('../../../../../eventmie-pro/resources/js/vue_common');


import Vuex from 'vuex';
window.Vuex = Vuex;
Vue.use(Vuex);

/**
 * Local Imports
*/

/**
 * Local Components 
 */
import MyBooking from './components/MyOrderBooking';
import MyBookingShow from './components/MyOrderBookingShow';
Vue.component('time-zone', require('../common_components/TimeZone').default);
const store = new Vuex.Store({
    state: {
        order_id     : null,
    },
    mutations: {
        add(state, {order_id}) {
           
            if(typeof order_id !== "undefined") {
                state.order_id   = order_id;
            }

        }
    },
});

/**
 * Local Vue Routes 
 */
const routes = new VueRouter({
    mode: 'history',
    base: '/',
    linkExactActiveClass: 'there',
    routes: [
        {
            path: path ? '/'+path+'/mybookings/booking_order/:id' : '/mybookings/booking_order/:id',
            // Inject  props based on route.query values for pagination
            props: (route) => ({
                page: route.query.page,
                date_format: date_format,
                disable_booking_cancellation: disable_booking_cancellation,
                hide_ticket_download: hide_ticket_download,
                hide_google_calendar: hide_google_calendar,
            }),
            name: 'mybookingshow',
            component: MyBookingShow,

        },
        {
            path: path ? '/'+path+'/mybookings' : '/mybookings',
            // Inject  props based on route.query values for pagination
            props: (route) => ({
                page: route.query.page,
                date_format: date_format,
                disable_booking_cancellation: disable_booking_cancellation,
                hide_ticket_download: hide_ticket_download,
                hide_google_calendar: hide_google_calendar,
            }),
            name: 'mybookings',
            component: MyBooking,

        },
    ],
});


/**
 * This is where we finally create a page specific
 * vue instance with required configs
 * element=app will remain common for all vue instances
 *
 * make sure to use window.app to make new Vue instance
 * so that we can access vue instance from anywhere
 * e.g interceptors 
 */
window.app = new Vue({
    el: '#eventmie_app',
    store:store,
    router: routes,
});