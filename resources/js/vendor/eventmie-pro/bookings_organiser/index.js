

/**
 * This is a page specific seperate vue instance initializer
 */

// include vue common libraries, plugins and components
require('../../../../../eventmie-pro/resources/js/vue_common');

import Vuex from 'vuex';
window.Vuex = Vuex;
Vue.use(Vuex);
/**
 * Local Imports
*/


/**
 * Local Components 
 */
import OrganiserBooking from './components/OrganiserOrderBooking';
import OrganiserShowBooking from './components/OrganiserShowBooking';
Vue.component('time-zone', require('../common_components/TimeZone').default);
const store = new Vuex.Store({
    state: {
        order_id     : null,
    },
    mutations: {
        add(state, {order_id}) {
           
            if(typeof order_id !== "undefined") {
                state.order_id   = order_id;
            }

        }
    },
});

/**
 * Local Vue Routes 
 */
const routes = new VueRouter({
    mode: 'history',
    base: '/',
    linkExactActiveClass: 'there',
    routes: [
        {
            path: '/bookings/booking_order/:id',
            // Inject  props based on route.query values for pagination
            props: (route) => ({
                page: route.query.page,
                date_format: date_format,
                hide_ticket_download: hide_ticket_download
               
            }),
            name: 'organisershowbooking',
            component: OrganiserShowBooking,

        },
        {
            path: path ? '/'+path+'/bookings' : '/bookings',
            // Inject  props based on route.query values for pagination
            props: (route) => ({
                page: route.query.page,
                date_format: date_format,
                hide_ticket_download: hide_ticket_download
               
            }),
            name: 'organiserbooking',
            component: OrganiserBooking,

        },
    ],
});


/**
 * This is where we finally create a page specific
 * vue instance with required configs
 * element=app will remain common for all vue instances
 *
 * make sure to use window.app to make new Vue instance
 * so that we can access vue instance from anywhere
 * e.g interceptors 
 */
window.app = new Vue({
    el: '#eventmie_app',
    store:store,
    router: routes,
    
});