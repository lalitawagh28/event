<?php
return [
    'apiKey' => env('AISENSY_API_KEY'),
    'apiUrl' => env('AISENSY_URL'),
    'campaign' => env('AISENSY_CAMPAIGN', 'checkout_prod')
];
