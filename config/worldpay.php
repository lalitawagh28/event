<?php
return [
    'worldpay_url' => env('WORLDPAY_URL', 'https://secure-test.worldpay.com/wcc/purchase'),
    'instID' => env('WORLDPAY_INSTID', '1465346'),
    'testMode' => env('WORLDPAY_MODE', 100),
];
