<?php

return [
    'application_id' => env('YAPILY_APPLICATION_ID'),
    'secret' => env('YAPILY_SECRET_KEY'),
];
