<?php


return [
'email_otp_enable' => env('EMAIL_OTP_ENABLE', true),
'mobile_otp_enable' => env('MOBILE_OTP_ENABLE', true),
'default_country_code' => '231',
'registation_success_email' => env('REGISTRATION_SUCCESS_EMAIL', true),
];