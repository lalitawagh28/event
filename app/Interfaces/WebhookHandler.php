<?php

namespace App\Interfaces;

interface WebhookHandler
{
    public function handle(array $body, string $type);
}
