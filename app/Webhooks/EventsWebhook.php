<?php

namespace App\Webhooks;

use App\Interfaces\WebhookHandler;
use App\Stratergies\RegistrationCrud;
use Exception;
use Illuminate\Http\Request;


class EventsWebhook
{
    private array $strategyTypeMap = [
        'user_registration' => RegistrationCrud::class
    ];

    public function __invoke(Request $request)
    {
        $type = $request->input('notification_type');
        $body = $request->input('payload');
        $strategy = $this->getStrategyForType($type);

        $strategy->handle($body, $type);
    }

    private function getStrategyForType(string $type): WebhookHandler
    {
        if (!isset($this->strategyTypeMap[$type])) {
            throw new Exception("No strategy exists for handling type [" . $type . "].");
        }

        return new $this->strategyTypeMap[$type]();
    }
}
