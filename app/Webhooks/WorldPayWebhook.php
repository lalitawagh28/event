<?php

namespace App\Webhooks;

use App\Interfaces\WebhookHandler;
use App\Stratergies\AisensyWebhook;
use App\Stratergies\TransferTicketWebhook;
use App\Stratergies\WebWebhook;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WorldPayWebhook
{
    private array $strategyTypeMap = [
        'WebBooking' => WebWebhook::class,
        'MobileBooking' => WebWebhook::class,
        'AiSensyBooking' => AisensyWebhook::class,
        'TransferTicket' => TransferTicketWebhook::class
    ];

    public function __invoke(Request $request)
    {

        $data = explode('&', $request->getContent());

        $finalArray = array();
        foreach ($data as $dataval) {
            $term = explode('=', $dataval);
            $finalArray[$term[0]] = $term[1];
        }


        $type = $finalArray['M_'];
        Log::info('Payment Webhook Received', $finalArray);

        $strategy = $this->getStrategyForType($type);
        $strategy->handle($finalArray, $type);
    }

    private function getStrategyForType(string $type): WebhookHandler
    {

        if (!isset($this->strategyTypeMap[$type])) {

            throw new Exception("No strategy exists for handling type [" . $type . "].");
        }
        return new $this->strategyTypeMap[$type]();
    }
}
