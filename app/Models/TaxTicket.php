<?php

namespace App\Models;

use Classiebit\Eventmie\Models\Tax;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxTicket extends Model
{
    use HasFactory;
    protected $table = 'tax_ticket';
    protected $guarded = [];
    protected $fillable = [
        'ticket_id',
        'tax_id'
    ];
    public $timestamps = false;

    public function tax()
    {
        return $this->belongsTo(Tax::class, 'id', 'tax_id');
    }
}
