<?php

namespace App\Models;

use App\Models\Scopes\OrganizerUserScope;
use Classiebit\Eventmie\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organizer extends User
{
    use HasFactory;

    protected $table = 'users';
    
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganizerUserScope);
    }


    // public function agents() {
    //     return $this->hasMany(AgentOrganizer::class, 'id','organizer_id');
    // }

    public function agents() {
        return $this->belongsToMany('App\Models\Agent', 'agent_organizer', 'agent_id', 'organizer_id');
    }
}
