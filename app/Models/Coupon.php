<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'agent_name',
        'code',
        'code_type',
        'code_value',
        'discount',
        'quantity',
        'max_uses',
        'is_active',
        'valid_from',
        'valid_to',
    ];

    // check user used promocode or not
    public function couponcode_user($params = [])
    {
        return DB::table('couponcode_users')->where($params)
            ->first();
    }
    // check user used promocode or not
    public function agentcode_user($params = [])
    {
        return DB::table('couponcode_users')->where($params)
            ->count();
    }

    // check user used promocode or not
    public function couponcode_apply($params = [])
    {
        try {
            $couponcode_apply  = DB::table('couponcode_users')->where($params)
                ->first();

            if (empty($couponcode_apply)) {
                $params['used_at'] = Carbon::now();
                $couponcode_apply =  DB::table('couponcode_users')->insert($params);

                // couponcode quantity decrement
                if (!empty($couponcode_apply)) {
                    $this->couponcode_quantity($params['couponcode_id']);
                }
            }

            return $couponcode_apply;
        } catch (\Throwable $th) {
        }
    }

    // promocode quantity decrement
    public function couponcode_quantity($couponcode_id = null)
    {
        try {
            $couponcode = Coupon::where(['id' => $couponcode_id])->first();

            if (!is_null($couponcode->quantity) && $couponcode->quantity > 0) {
                $couponcode->quantity -= 1;
                $couponcode->save();
            }
        } catch (\Throwable $th) {
        }
    }

    public function agent_ticket() {
        return $this->hasOne(AgentTicket::class, 'coupon_id', 'id');
    }
}       
