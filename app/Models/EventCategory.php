<?php

namespace App\Models;

use Classiebit\Eventmie\Models\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EventCategory extends Model
{
    use HasFactory;

    protected $table = 'event_categories';
    protected $guarded = [];
    public $timestamps = false;

    public function event()
    {
        return $this->belongsTo(Event::class, 'id', 'event_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'id', 'category_id');
    }

}
