<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Classiebit\Eventmie\Models\Booking as BaseModel;
use App\Models\Attendee;
use Classiebit\Eventmie\Models\Transaction;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Booking extends BaseModel
{
    use HasFactory;

    // get booking for organiser
    public function get_organiser_bookings($params = [])
    {
        //CUSTOM
        $bookings     = new Booking;
        $table        = $bookings->getTable();

        //GET ALL COLUMNS
        $columns      = \Schema::getColumnListing($table);

        $length       = $params['length'];


        //CUSTOM

        $query = Booking::query();

        // $query->select('bookings.*', 'CM.customer_paid')

        //CUSTOM
        $query->with(['attendees']);

        $query->select('bookings.*', 'CM.customer_paid', 'events.online_location', 'events.youtube_embed', 'events.vimeo_embed')
            //CUSTOM
            ->from('bookings')
            ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
            ->selectRaw("(SELECT E.online_location FROM events E WHERE E.id = bookings.event_id AND bookings.is_paid = 1  AND bookings.status = 1) online_location")

            //CUSTOM
            ->leftJoin('events', function ($join) {
                $join->on('bookings.event_id', '=', 'events.id')
                    ->where(['bookings.is_paid' => 1, 'bookings.status' => 1]);
            })
            //CUSTOM
            ->leftJoin('commissions as CM', 'CM.booking_id', '=', 'bookings.id');

        // in case of searching by between two dates
        if (!empty($params['start_date']) && !empty($params['end_date'])) {
            $query->whereDate('bookings.created_at', '>=', $params['start_date']);
            $query->whereDate('bookings.created_at', '<=', $params['end_date']);
        }

        // in case of searching by start_date
        if (!empty($params['start_date']) && empty($params['end_date']))
            $query->whereDate('bookings.created_at', $params['start_date']);


        //CUSTOM
        // in case of searching by between two dates
        if (!empty($params['event_start_date']) && !empty($params['event_end_date'])) {
            $query->whereDate('bookings.event_start_date', '>=', $params['event_start_date']);
            $query->whereDate('bookings.event_start_date', '<=', $params['event_end_date']);
        }

        // in case of searching by start_date
        if (!empty($params['event_start_date']) && empty($params['event_end_date']))
            $query->whereDate('bookings.event_start_date', $params['event_start_date']);

        //CUSTOM

        // in case of searching by event_id
        if ($params['event_id'] > 0)
            $query->where(['bookings.event_id' => $params['event_id']]);


        // CUSTOM
        if (!empty($params['search'])) {
            // ALL COLUMNS SEARCH

            $query->where(function ($query) use ($columns, $params) {
                foreach ($columns as $key => $column) {
                    $query->orWhere('bookings.' . $column, 'like', '%' . $params['search'] . '%');
                }
            });
        }
        //CUSTOM

        return  $query->where(['bookings.organiser_id' => $params['organiser_id']])
            ->orderBy('id', 'desc')
            // ->paginate(10);
            // CUSTOM
            ->paginate($length);
        // CUSTOM
    }

    // get booking for customer
    public function get_my_bookings($params = [])
    {
        // return Booking::select('bookings.*')
        // CUSTOM
        $bookings     = new Booking;
        $table        = $bookings->getTable();

        //GET ALL COLUMNS
        $columns      = \Schema::getColumnListing($table);

        $length       = $params['length'];

        $query = Booking::query();

        $query->select('bookings.*', 'events.online_location', 'events.youtube_embed', 'events.vimeo_embed')
            // CUSTOM

            ->from('bookings')
            ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
            ->selectRaw("(SELECT E.excerpt FROM events E WHERE E.id = bookings.event_id) event_excerpt")
            ->selectRaw("(SELECT E.venue FROM events E WHERE E.id = bookings.event_id) event_venue")
            // ->selectRaw("(SELECT E.online_location FROM events E WHERE E.id = bookings.event_id AND bookings.is_paid = 1  AND bookings.status = 1) online_location")

            //CUSTOM
            ->leftJoin('events', function ($join) {
                $join->on('bookings.event_id', '=', 'events.id')
                    ->where(['bookings.is_paid' => 1, 'bookings.status' => 1]);
            });
        //CUSTOM



        //CUSTOM
        // in case of searching by between two dates
        if (!empty($params['start_date']) && !empty($params['end_date'])) {
            $query->whereDate('bookings.created_at', '>=', $params['start_date']);
            $query->whereDate('bookings.created_at', '<=', $params['end_date']);
        }

        // in case of searching by start_date
        if (!empty($params['start_date']) && empty($params['end_date']))
            $query->whereDate('bookings.created_at', $params['start_date']);

        // in case of searching by between two dates
        if (!empty($params['event_start_date']) && !empty($params['event_end_date'])) {
            $query->whereDate('bookings.event_start_date', '>=', $params['event_start_date']);
            $query->whereDate('bookings.event_start_date', '<=', $params['event_end_date']);
        }

        // in case of searching by start_date
        if (!empty($params['event_start_date']) && empty($params['event_end_date']))
            $query->whereDate('bookings.event_start_date', $params['event_start_date']);

        // in case of searching by event_id
        if ($params['event_id'] > 0)
            $query->where(['bookings.event_id' => $params['event_id']]);

        if (!empty($params['search'])) {
            // ALL COLUMNS SEARCH

            $query->where(function ($query) use ($columns, $params) {
                foreach ($columns as $key => $column) {
                    $query->orWhere('bookings.' . $column, 'like', '%' . $params['search'] . '%');
                }
            });
        }
        //CUSTOM

        return  $query->where(['customer_id' => $params['customer_id']])
            ->orderBy('id', 'desc')
            // ->paginate(10);
            // CUSTOM
            ->paginate($length);
        // CUSTOM
    }
    public function get_order_id($id){
        return OrderBooking::where('booking_id',$id)->first()?->order_id;
    } 
    public function get_all_bookings($order_numbers){
        $booking = Booking::with(['attendees' => function ($query) {
            $query->where(['status' => 1]);
        }, 'attendees.seat'])->whereIn('order_number',$order_numbers)->get();
        //CUSTOM
        return $booking;
    }
    // only admin and organiser can get particular event's booking
    public function get_event_bookings($params = [], $select = ['*'])
    {
        // $booking = Booking::select($select)->where($params)->get();

        //CUSTOM
        $booking = Booking::with(['attendees' => function ($query) {
            $query->where(['status' => 1]);
        }, 'attendees.seat'])->select('bookings.*')->selectRaw("if(tsub.title is not null, CONCAT(ticket_title,' (',tsub.title,')'),ticket_title)  as ticket_category")->where($params)->leftJoin('ticket_sub_categories as tsub', 'tsub.id', '=', 'bookings.sub_category_id')->get();
        //CUSTOM
        return $booking;
        //return to_array($booking);
    }

    // organiser view booking of customer
    public function organiser_view_booking($params = [])
    {
        // return Booking::select('bookings.*')->from('bookings')
        //CUSTOM
        return Booking::with(['attendees' => function ($query) {
            $query->where(['status' => 1]);
        }, 'attendees.seat'])
            //CUSTOM
            ->where($params)
            ->first();
    }

    // sum booked ticket quantity each booking date + each ticket id
    public function get_seat_availability_by_ticket($event_id = null)
    {
        return Booking::select('event_start_date', 'ticket_id', 'event_start_time')
            ->selectRaw("SUM(quantity) as total_booked")
            ->where("event_id", $event_id)
            ->where("status", 1)
            ->groupBy("event_start_date", "ticket_id")
            ->orderBy('ticket_id')
            ->get();
    }

    /**
     * Get the attendees for the bookings.
     */
    public function attendees()
    {
        return $this->hasMany(Attendee::class);
    }
    public function attendee()
    {
        return $this->hasOne(Attendee::class,'booking_id','id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_bookings', 'booking_id', 'order_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_bookings', 'booking_id', 'order_id');
    }

    public function orderBooking()
    {
        return $this->hasMany(OrderBooking::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(TicketSubCategory::class,'sub_category_id','id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class,'id','transaction_id');
    }

    public function customer(){
        return $this->hasOne(User::class,'id','customer_id');
    }

    public function organizer(){
        return $this->hasOne(User::class,'id','organiser_id');
    }
    public function agent_ticket(){
        return $this->hasOne(AgentTicket::class,'id','agent_id');
    }
}
