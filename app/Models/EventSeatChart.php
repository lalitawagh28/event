<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Seat;

class EventSeatChart extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'events_seat_chart';
    public $timestamps = false;

    /**
     * Get the seats for the seatchart.
     */
    public function event()
    {
        return $this->haaOne(Event::class,'id','event_id');
    }

    public function ticket()
    {
        return $this->hasOne(Ticket::class,'id','ticket_id');
    }

    public function subcatagory()
    {
        return $this->hasOne(TicketSubCategory::class,'id','sub_cat_id');
    }
}
