<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\FuncCall;
use DB;
use Illuminate\Support\Carbon;

class Promocode extends Model
{
    protected $guarded = [];

    // get promocodes
    public function get_promocodes()
    {
        return Promocode::where(['status' => 1])->get()->toArray();
    }
    public function get_organizer_promocodes($id)
    {
        return Promocode::where(['status' => 1])->where('organiser_id',$id)->get()->toArray();
    }
    

    // save promocodes with ticket' id in relational table
    public function save_ticket_promocode($params = [], $ticket_id = null)
    {
        DB::table('ticket_promocode')
            ->where(['ticket_id' => $ticket_id])
            ->delete();

        return DB::table('ticket_promocode')->insert($params);
    }

     // save promocodes with ticket' id in relational table
     public function save_event_promocode($params = [], $event_id = null)
     {
        EventPromocodes::where('event_id',$event_id)->delete();
        return EventPromocodes::insert($params);
     }

    // get particular promocodes's ids for particular ticket
    public function get_ticket_promocodes_ids($params = [])
    {
        return  DB::table('ticket_promocode')
                            ->select('promocode_id')
                            ->where(['ticket_id' => $params['ticket_id']])
                            ->get()
                            ->toArray();

    }

    // get particular promocodes's ids for particular ticket
    public function get_event_promocodes_ids($params = [])
    {   
        return EventPromocodes::where('event_id',$params['event_id'])->select('promocode_id')->get()->toArray();
    }

    // get particular ticket's promocode
    public function get_event_promocodes($params = [])
    {
        return Promocode::whereIn('id',$params['promocodes_ids'])
            ->where(['status' => 1])
            ->get()
            ->toArray();
    }
    // get particular ticket's promocode
    public function get_ticket_promocodes($params = [])
    {
        return Promocode::whereIn('id',$params['promocodes_ids'])
            ->where(['status' => 1])
            ->get()
            ->toArray();
    }

    // check user used promocode or not
    public function promocode_user($params = [])
    {
        return DB::table('promocode_user')->where($params)
                                          ->first();
    }
    
    // check user used promocode or not
    public function promocode_event_user($params = [])
    {
        return EventPromocodeUser::where($params)
                                          ->first();
    }
    

    // check user used promocode or not
    public function promocode_apply($params = [])
    {
        // check if already applied
        // promocode_user -> user_id && promocode_id && ticket_id

        // insert into promocode_user
        // promocode_user -> user_id && promocode_id && ticket_id
        $promocode_apply  = DB::table('promocode_user')->where($params)
                                          ->first();

        if(empty($promocode_apply))
        {
            $params['used_at'] = Carbon::now();
            $promocode_apply =  DB::table('promocode_user')->insert($params);

            // promocode quantity decrement
            if(!empty($promocode_apply))
            {
                $this->promocode_quantity($params['promocode_id']);
            }
        }

        return $promocode_apply;
    }

    // promocode quantity decrement
    public function promocode_quantity($promocode_id = null)
    {
        $promocode = Promocode::where(['id' => $promocode_id])->first();

        if (!is_null($promocode->quantity) && $promocode->quantity > 0 ) {
            $promocode->quantity -= 1;
            $promocode->save();
        }
    }

    public function setWebhookParameters($data)
    {
        if(is_null(@$data['deletedId'])){
            $info = [
                'payload' =>[
                    'code' => $data['code'],
                    'reward' => $data['reward'],
                    'quantity' => $data['quantity'],
                    'p_type' => $data['p_type'],
                    'status' => $data['status'],
                ]
            ];

        if(!is_null(@$data['updatedId']))
        {
            $info['updatedId'] = $data['updatedId'];
            $info['notification-type'] = 'promocode-update';
        }
        else{
            $info['notification-type'] = 'promocode-add';
        }
    }
    else
    {
        $info['deletedId'] = $data['deletedId'];
        $info['notification-type'] = 'promocode-delete';
    }
        return $info;
    }

    public function organizer($promocode_id = null)
    { 
        return $this->hasOne(User::class,'id','organiser_id');
    }
}
