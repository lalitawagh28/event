<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketSubCategory extends Model
{
    use HasFactory;

    protected $guarded = [];  

    public $timestamps = false;

    protected $appends = [
        'bookingCount'
    ];

    
    public function ticket()
    {
        return $this->hasOne(Ticket::class, 'ticket_id', 'id');
    }

    public function avaliableTickets($agent_ticket_ids=[])
    {

        $booking_counts = $this->bookings?->whereNotIn('agent_id', $agent_ticket_ids)?->count();
        
        $booking_counts = $booking_counts ?? 0; 
        return $this->quantity - $this->agent_tickets->whereNotIn('id',$agent_ticket_ids)->where('sub_category_id',$this->id)->sum('quantity') - $booking_counts;
    }

    public function agent_tickets(){
        return $this->hasMany(AgentTicket::class,'sub_category_id','id');
    }

    public function get_event_sub_categories($ids){
        if (!empty($ids)) {
            $result = TicketSubCategory::whereIn('id', $ids)
                ->orderBy('price')
                ->get();
                return $result;
        } 
        
    }
    public function bookings()
    {
        return $this->hasMany(Booking::class, 'sub_category_id', 'id');
    }

    public function getBookingCountAttribute()
    {
        return ($this->quantity - $this->bookings()->count());
    }

}
