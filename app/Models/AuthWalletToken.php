<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Booking;
use App\Models\Seat;

class AuthWalletToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'token',
    ];
}