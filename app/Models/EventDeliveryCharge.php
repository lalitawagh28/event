<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventDeliveryCharge extends Model
{
    use HasFactory;

    protected $table = 'event_delivery_charges';
    protected $guarded = []; 
    public $timestamps = false;

    public function get_delivery_charges($id) {
        $event_delivery_charge = EventDeliveryCharge::find($id);
        return $event_delivery_charge->rate;
    }
}
