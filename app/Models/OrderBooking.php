<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderBooking extends Model
{
    use HasFactory;
    protected $table = 'order_bookings';
    protected $guarded = [];
    protected $fillable = [
        'order_id',
        'booking_id'
    ];
    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo(Order::class, 'id', 'order_id');
    }

    public function Booking()
    {
        return $this->belongsTo(Booking::class, 'id', 'booking_id');
    }

    public function Bookings()
    {
        return $this->hasOne(Booking::class, 'id', 'booking_id');
    }

    public function BookingOrder()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }
}
