<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Classiebit\Eventmie\Models\Ticket as BaseModel;
use App\Models\Seatchart;
use App\Models\Attendee;
use App\Models\Promocode;
use Carbon\Carbon;

class Ticket extends BaseModel
{
    use HasFactory;

    public function get_event_tickets($params = [])
    {
        if (!empty($params['ticket_ids'])) {
            $result = Ticket::with([
                'taxes',
                'sub_categories',
                'seatchart',
                'attendees',
                'attendees.booking',
                'promocodes',
                'gates',
                'seatchart.seats'  => function ($query) {
                    // $query->where(['status' => 1]);
                },
            ])->whereIn('id', $params['ticket_ids'])
                ->where('event_id', $params['event_id'])
                ->orderBy('price', 'desc')
                ->get();
        } else {
            $result = Ticket::with([
                'taxes',
                'sub_categories',
                'seatchart',
                'attendees',
                'attendees.booking',
                'promocodes',
                'gates',
                'seatchart.seats'  => function ($query) {
                    // $query->where(['status' => 1]);
                },
            ])->where(['event_id' => $params['event_id']])
                ->orderBy('price', 'desc')
                ->get();
        }

        return $result;
    }

    /**
     * Get the seatchart record associated with the ticket.
     */
    public function seatchart()
    {
        return $this->hasOne(Seatchart::class);
    }


    /**
     * Get the attendees record associated with the ticket.
     */
    public function attendees()
    {
        return $this->hasMany(Attendee::class);
    }

    /**
     * Get the promocodes record associated with the ticket.
     */
    public function promocodes()
    {
        return $this->belongsToMany(Promocode::class, 'ticket_promocode');
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class, 'ticket_id', 'id');
    }

    public function sub_categories()
    {
        return $this->hasMany(TicketSubCategory::class, 'ticket_id', 'id');
    }

    public function gates()
    {
        return $this->hasMany(TicketGate::class, 'ticket_id', 'id');
    }

    public function get_price(){
        if (!empty($this->sale_start_date)) {
            if ($this->sale_price && $this->sale_price < $this->price && $this->sale_start_date <= Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString() && $this->sale_end_date > Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString()) {
                return  $this->sale_price;
            }
        }
        return $this->price;
    }
    public function taxes()
    {
        return $this->belongsToMany('Classiebit\Eventmie\Models\Tax', 'tax_ticket', 'ticket_id', 'tax_id');
    }

    public function agent_tickets(){
        return $this->hasMany(AgentTicket::class,'ticket_id','id');
    }
}
