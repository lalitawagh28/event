<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';
    protected $guarded = []; 
    protected $fillable = [
        'event_id',
        'customer_id',
        'quantity',
        'price',
        'tax',
        'net_price',
        'promocode_id',
        'promocode_reward',
        'delivery_address',
        'address_type',
        'collection_point'
    ]; 
    public $timestamps = false;


    public function bookings(){
        return $this->belongsToMany(Booking::class,'order_bookings','order_id','booking_id');
    }

    public function event(){
        return $this->hasOne(Event::class,'id','event_id');
    }

    public function customer(){
        return $this->hasOne(User::class,'id','customer_id');
    }

    public function get_organiser_orders($params) {
        $order     = new Order;
        $table        = $order->getTable();

        //GET ALL COLUMNS
        $columns      = \Schema::getColumnListing($table);

        $length       = $params['length'];


        //CUSTOM

        $query = Order::query();

        $query->with(['event','customer']);
        $query->select('orders.id as order_num','orders.quantity as order_quantity' ,'orders.price as order_price', 'orders.tax as order_tax' , 'orders.net_price as order_net_price' ,'orders.promocode_reward as order_promocode_reward','orders.delivery_address as order_delivery_address','bookings.event_title','bookings.currency','bookings.payment_type','bookings.event_category','bookings.event_start_time','bookings.event_end_time', 'bookings.event_start_date','bookings.event_end_date','bookings.created_at','bookings.is_paid','bookings.checked_in','bookings.status','bookings.booking_cancel','customer.email as customer_email','CM.customer_paid',  'events.youtube_embed', 'events.vimeo_embed','events.time_zone','events.slug as event_slug')
            //CUSTOM
            ->from('orders')
            ->selectRaw("(SELECT count(OB.booking_id) FROM order_bookings as OB WHERE OB.order_id = orders.id) as ticket_total")
            // ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
            ->selectRaw("(SELECT E.online_location FROM events E WHERE E.id = bookings.event_id AND bookings.is_paid = 1  AND bookings.status = 1) online_location")
            ->leftJoin('order_bookings', function ($join) {
                $join->on('order_bookings.order_id', '=', 'orders.id');
            })
            ->leftJoin('users as customer', function ($join) {
                $join->on('orders.customer_id', '=', 'customer.id');
            })
            ->leftJoin('bookings', function ($join) {
                $join->on('bookings.id', '=', 'order_bookings.booking_id');
            })
            //CUSTOM
            ->leftJoin('events', function ($join) {
                $join->on('bookings.event_id', '=', 'events.id');
                    // ->where(['bookings.is_paid' => 1, 'bookings.status' => 1]);
            })
            //CUSTOM
            ->leftJoin('commissions as CM', 'CM.booking_id', '=', 'bookings.id');

        // in case of searching by between two dates
        if (!empty($params['start_date']) && !empty($params['end_date'])) {
            $query->whereDate('bookings.created_at', '>=', $params['start_date']);
            $query->whereDate('bookings.created_at', '<=', $params['end_date']);
        }

        // in case of searching by start_date
        if (!empty($params['start_date']) && empty($params['end_date']))
            $query->whereDate('bookings.created_at', $params['start_date']);


        //CUSTOM
        // in case of searching by between two dates
        if (!empty($params['event_start_date']) && !empty($params['event_end_date'])) {
            $query->whereDate('bookings.event_start_date', '>=', $params['event_start_date']);
            $query->whereDate('bookings.event_start_date', '<=', $params['event_end_date']);
            // $query->where(DB::raw("UNIX_TIMESTAMP(CONVERT_TZ(CONCAT(bookings.event_start_date,' ',bookings.event_start_time),events.time_zone,'UTC')) >= UNIX_TIMESTAMP(CONVERT_TZ('".$params['event_start_date']."','".$params['time_zone']."','UTC'))"));
            // $query->where(DB::raw("UNIX_TIMESTAMP(CONVERT_TZ(CONCAT(bookings.event_start_date,' ',bookings.event_start_time),events.time_zone,'UTC')) <= UNIX_TIMESTAMP(CONVERT_TZ('".$params['event_end_date']."','".$params['time_zone']."','UTC'))"));
        }

        // in case of searching by start_date
        if (!empty($params['event_start_date']) && empty($params['event_end_date']))
            $query->whereDate('bookings.event_start_date', $params['event_start_date']);

        //CUSTOM

        // in case of searching by event_id
        if ($params['event_id'] > 0)
            $query->where(['bookings.event_id' => $params['event_id']]);


        // CUSTOM
        if (!empty($params['search'])) {
            // ALL COLUMNS SEARCH

            $query->where(function ($query) use ($columns, $params) {
                foreach ($columns as $key => $column) {
                    $query->orWhere('bookings.' . $column, 'like', '%' . $params['search'] . '%');
                }
            });
        }
        //CUSTOM

        return  $query->where(['bookings.organiser_id' => $params['organiser_id']])
            ->orderBy('orders.id', 'desc')
            ->groupBy('orders.id')
            // ->paginate(10);
            // CUSTOM
            ->paginate($length);
        // CUSTOM
    }

    
    public function get_my_order_bookings($params = [])
    {   
        // return Booking::select('bookings.*')
        // CUSTOM
        $orders     = new Order;
        $table        = $orders->getTable();

        //GET ALL COLUMNS
        $columns      = \Schema::getColumnListing($table);

        $length       = $params['length'];

        $query = Order::query();
        $query->with(['event','customer']);
        $query->select('orders.id as order_num','orders.quantity as order_quantity' ,'orders.price as order_price', 'orders.tax as order_tax' , 'orders.net_price as order_net_price' ,'orders.promocode_reward as order_promocode_reward','orders.delivery_address as order_delivery_address','bookings.event_title','bookings.currency','bookings.payment_type','bookings.event_category','bookings.event_start_time','bookings.event_end_time', 'bookings.event_start_date','bookings.event_end_date','bookings.created_at','bookings.is_paid','bookings.checked_in','bookings.status','bookings.booking_cancel','customer.email as customer_email','CM.customer_paid',  'events.youtube_embed', 'events.vimeo_embed','bookings.is_transferred','transactions.payment_gateway','events.slug as event_slug','events.excerpt as event_excerpt','events.venue as event_venue','events.time_zone')
            // CUSTOM

            ->from('orders')
            ->selectRaw("(SELECT count(OB.booking_id) FROM order_bookings as OB WHERE OB.order_id = orders.id) as ticket_total")
            // ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
            // ->selectRaw("(SELECT E.excerpt FROM events E WHERE E.id = bookings.event_id) event_excerpt")
            // ->selectRaw("(SELECT E.venue FROM events E WHERE E.id = bookings.event_id) event_venue")
            ->selectRaw("(SELECT E.online_location FROM events E WHERE E.id = bookings.event_id AND bookings.is_paid = 1  AND bookings.status = 1) online_location")
            ->leftJoin('order_bookings', function ($join) {
                $join->on('order_bookings.order_id', '=', 'orders.id');
            })
            ->leftJoin('users as customer', function ($join) {
                $join->on('orders.customer_id', '=', 'customer.id');
            })
            ->leftJoin('bookings', function ($join) {
                $join->on('bookings.id', '=', 'order_bookings.booking_id');
            })
            ->leftJoin('transactions', function ($join) {
                $join->on('transactions.id', '=', 'bookings.transaction_id');
            })
            //CUSTOM
            ->leftJoin('events', function ($join) {
                $join->on('bookings.event_id', '=', 'events.id');
                    // ->where(['bookings.is_paid' => 1, 'bookings.status' => 1]);
            })
            ->leftJoin('commissions as CM', 'CM.booking_id', '=', 'bookings.id');
        //CUSTOM



        //CUSTOM
        // in case of searching by between two dates
        if (!empty($params['start_date']) && !empty($params['end_date'])) {
            $query->whereDate('bookings.created_at', '>=', $params['start_date']);
            $query->whereDate('bookings.created_at', '<=', $params['end_date']);
        }

        // in case of searching by start_date
        if (!empty($params['start_date']) && empty($params['end_date']))
            $query->whereDate('bookings.created_at', $params['start_date']);

        // in case of searching by between two dates
        if (!empty($params['event_start_date']) && !empty($params['event_end_date'])) {
            $query->whereDate('bookings.event_start_date', '>=', $params['event_start_date']);
            $query->whereDate('bookings.event_start_date', '<=', $params['event_end_date']);
            // $query->where(DB::raw("UNIX_TIMESTAMP(CONVERT_TZ(CONCAT(bookings.event_start_date,' ',bookings.event_start_time),events.time_zone,'UTC')) >= UNIX_TIMESTAMP(CONVERT_TZ('".$params['event_start_date']."','".$params['time_zone']."','UTC'))"));
            // $query->where(DB::raw("UNIX_TIMESTAMP(CONVERT_TZ(CONCAT(bookings.event_start_date,' ',bookings.event_start_time),events.time_zone,'UTC')) <= UNIX_TIMESTAMP(CONVERT_TZ('".$params['event_end_date']."','".$params['time_zone']."','UTC'))"));
        }
        // in case of searching by start_date
        if (!empty($params['event_start_date']) && empty($params['event_end_date']))
            $query->whereDate('bookings.event_start_date', $params['event_start_date']);

        // in case of searching by event_id
        if ($params['event_id'] > 0)
            $query->where(['bookings.event_id' => $params['event_id']]);

        if (!empty($params['search'])) {
            // ALL COLUMNS SEARCH
            $query->orWhere('bookings.event_title' , 'like', '%' . $params['search'] . '%');
            // $query->where(function ($query) use ($columns, $params) {
            //     foreach ($columns as $key => $column) {
            //         $query->orWhere('bookings.' . $column, 'like', '%' . $params['search'] . '%');
            //     }
            // });
        }
        //CUSTOM

        return  $query->where(['orders.customer_id' => $params['customer_id']])
        ->orderBy('orders.id', 'desc')
        ->groupBy('orders.id')
            // ->paginate(10);
            // CUSTOM
            ->paginate($length);
    }

}
