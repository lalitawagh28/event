<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryCharge extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function create_delivery_charge($params){
        return DeliveryCharge::updateOrCreate(

            ['title' => $params['title'],'organiser_id' => $params['organiser_id']],
            $params
        );
    }
    public function create_event_delivery_charge($params){
        return EventDeliveryCharge::create($params);
    }
    public function update_event_delivery_charge($params){
        return EventDeliveryCharge::updateOrCreate(
            ['id' => $params['id']],
            $params
        );
    }
    public function get_delivery_charges($id){
        return DeliveryCharge::where('organiser_id',$id)->get();
    }

     // save promocodes with ticket' id in relational table
     public function delete_event_delivery_charges($event_ids)
     {
        EventDeliveryCharge::whereIn('id',$event_ids)->delete();
     }


    // get particular promocodes's ids for particular ticket
    public function get_event_delivery_charges_ids($params = [])
    {   
        return EventDeliveryCharge::where('event_id',$params['event_id'])->pluck('id')->toArray();
    }

    // get particular ticket's promocode
    public function get_event_delivery_charges($params = [])
    {
        return EventDeliveryCharge::where('event_id',$params['event_id'])
            ->get()
            ->toArray();
    }
}
