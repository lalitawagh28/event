<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventGate extends Model
{
    use HasFactory;

    protected $table = 'event_gates';
    protected $guarded = []; 
    public $timestamps = false;


    public function get_gates($event_id)
    {
        return EventGate::where(['event_id' => $event_id])->pluck('name')->toArray();
    }
}
