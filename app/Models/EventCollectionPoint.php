<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventCollectionPoint extends Model
{
    use HasFactory;

    protected $table = 'event_collection_points';
    protected $guarded = []; 
    public $timestamps = false;


    public function get_collection_points($event_id)
    {
        return EventCollectionPoint::where(['event_id' => $event_id])->pluck('name')->toArray();
    }
}
