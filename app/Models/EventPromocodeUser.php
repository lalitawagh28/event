<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventPromocodeUser extends Model
{
    use HasFactory;

    protected $table = 'event_promocode_users';
    protected $guarded = []; 
    protected $fillable = [
        'event_id',
        'promocode_id',
        'user_id',
        'used_at'
    ]; 
    public $timestamps = false;
}
