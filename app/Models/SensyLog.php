<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SensyLog extends Model
{
    use HasFactory;

    protected $fillable = ['cartId', 'meta','status','payment_success'];

    protected $casts = [
        'meta' => 'array',
    ];
}
