<?php

namespace App\Models;

use App\Models\Scopes\AgentUserScope;
use Classiebit\Eventmie\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agent extends User
{
    use HasFactory;

    protected $table = 'users';
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new AgentUserScope);
    }


    // /**
    // *  Relationship: Orders (Has many)
    // */
    // public function organizers()
    // {
    //     return $this->hasMany(AgentOrganizer::class, 'id','agent_id');
    // }   

    public function organizer() {
        return $this->belongsToMany('App\Models\Organizer', 'agent_organizer', 'organizer_id', 'agent_id');
    }

    public function agent_tickets() {
        return $this->hasMany('App\Models\AgentTicket',  'agent_id', 'id');
    }
}
