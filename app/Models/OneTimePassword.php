<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OneTimePassword extends Model
{
    use HasFactory;

    protected $casts = [
        'expires_at' => 'datetime',
        'verified_at' => 'datetime',
    ];

    protected $fillable = [
        'holder_id',
        'holder_type',
        'code',
        'type',
        'expires_at',
        'verified_at',
    ];

    public static function getExpiringDuration()
    {
        return 10; // In minutes.
    }

    public function holder()
    {
        return $this->morphTo();
    }

    public function scopeIsActive($query)
    {
        return $query->where('expires_at', '>', now())->whereNull('verified_at');
    }
}
