<?php

namespace App\Models;

use Carbon\Carbon;
use Classiebit\Eventmie\Models\Ticket;
use Classiebit\Eventmie\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AgentTicket extends Model
{

    protected $guarded = [];  

    public $timestamps = false;

    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id', 'id');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id', 'id');
    }
    public function bookings()
    {
        return $this->hasMany(Booking::class, 'agent_id', 'id');
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'coupon_id', 'id');
    }

    public function sold_tickets(){
        return $this->bookings->count();
    }

    public function getTicektPrice() {
        if($this->ticket->sale_price > 0 ){
            return $this->ticket->sale_price;
        } else {
            return $this->ticket->price;
        }
    }

    // check user used promocode or not
    public function agentcode_user($params = [])
    {
        return DB::table('agent_tickets_user')->where($params)
            ->count();
    }


     // check user used promocode or not
     public function voucher_code_apply($params = [])
     {
         try {
             $vouchercode_apply  = DB::table('agent_tickets_user')->where($params)
                 ->first();
             if (empty($vouchercode_apply)) {
                 $params['used_at'] = Carbon::now();
                 $vouchercode_apply =  DB::table('agent_tickets_user')->insert($params);
                 // couponcode quantity decrement
                 if (!empty($vouchercode_apply)) {
                     $this->vouchercode_quantity($params['agent_ticket_id']);
                 }
             }
 
             return $vouchercode_apply;
         } catch (\Throwable $th) {

         }
     }

      // promocode quantity decrement
    public function vouchercode_quantity($vouchercode_id = null)
    {
        try {
            $agent_ticket = AgentTicket::where(['id' => $vouchercode_id])->first();

            if (!is_null($agent_ticket->quantity) && $agent_ticket->quantity > 0) {
                $agent_ticket->quantity -= 1;
                $agent_ticket->save();
            }
        } catch (\Throwable $th) {
        }
    }

    public function sub_category()
    {
        return $this->hasOne(TicketSubCategory::class, 'id', 'sub_category_id');
    }

}
