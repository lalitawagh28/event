<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisterCountry extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = [
        'risk_score',
    ];

    public $timestamps = false;

    public function getFlagAttribute()
    {
        return asset('flags/' . $this->code . '.png');
    }
}
