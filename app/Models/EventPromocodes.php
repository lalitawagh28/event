<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventPromocodes extends Model
{
    use HasFactory;

    protected $table = 'event_promocodes';
    protected $guarded = []; 
    protected $fillable = [
        'event_id',
        'promocode_id'
    ]; 
    public $timestamps = false;
}
