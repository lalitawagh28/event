<?php

namespace App\Notifications;

use App\Models\OneTimePassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmailOneTimePasswordNotification extends Notification
{
    use Queueable;

    private OneTimePassword $oneTimePassword;

    /**
     * Create a new notification instance.
     *
     * @param OneTimePassword $oneTimePassword
     */
    public function __construct(OneTimePassword $oneTimePassword)
    {
        $this->oneTimePassword = $oneTimePassword;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $senderMail = config('mail.from.address');

        return (new MailMessage)->subject(config('app.name') . " Verification OTP")
            ->from($senderMail, config('mail.from.name'))
            ->line(config('app.name') . ": Your security code is {$this->oneTimePassword->code}. It expires in {$this->oneTimePassword->getExpiringDuration(10)} minutes. Don't share this code with anyone.")
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
