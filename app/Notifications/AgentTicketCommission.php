<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AgentTicketCommission extends Notification
{
    use Queueable;
    protected $event;
    protected $user;
    protected $agent_tickets;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($event,$user,$agent_tickets)
    {
        $this->event = $event;
        $this->user = $user;
        $this->agent_tickets = $agent_tickets;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject('Voucher Code Allocation to Agent')->view(
            'email_templates.agent_ticket_commission', ['event' => $this->event,'user' => $this->user,'agent_tickets' => $this->agent_tickets]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
