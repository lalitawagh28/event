<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TicketTransferRequest extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($booking,$senderUser,$bookingIds,$type)
    {
        $this->booking = $booking;
        $this->senderUser = $senderUser;
        $this->bookingIds = $bookingIds;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
       if($this->type == 'paid')
       {
            return (new MailMessage)
            ->subject('Ticket Transfer Paid Request '.$this->booking?->event_title)
            ->line('Ticket transfer Request is get from '.$this->senderUser?->name.'. Please accept request with click on below link')
            ->action('Accept Ticket',route('ticketTransferAcceptPayment',['array' => implode(',',$this->bookingIds)]))
            ->line('Thank you for using service!');
       }else{

            return (new MailMessage)
            ->subject('Ticket Transfer Request '.$this->booking?->event_title)
            ->line('Ticket transfer Request is get from '.$this->senderUser?->name.'. Please accept request with click on below link')
            ->action('Accept Ticket',route('ticketTransferAccept',['array' => implode(',',$this->bookingIds)]))
            ->line('Thank you for using service!');

       }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
