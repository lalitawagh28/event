<?php

namespace App\Notifications;

use App\Models\OneTimePassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class SmsOneTimePasswordNotification extends Notification
{
    use Queueable;

    private OneTimePassword $oneTimePassword;

    /**
     * Create a new notification instance.
     *
     * @param OneTimePassword $oneTimePassword
     */
    public function __construct(OneTimePassword $oneTimePassword)
    {

        $this->oneTimePassword = $oneTimePassword;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TwilioChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioSmsMessage())
            ->content(config('app.name') . ": Your security code is {$this->oneTimePassword->code}. It expires in {$this->oneTimePassword->getExpiringDuration(10)} minutes. Don't share this code with anyone.");
    }
}
