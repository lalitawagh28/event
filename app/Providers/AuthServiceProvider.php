<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Auth;
use Laravel\Passport\Passport;
use Carbon\Carbon;
use App\Policies\EventPolicy;
use Classiebit\Eventmie\Models\Event;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Event::class => EventPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {

        VerifyEmail::toMailUsing(function ($notifiable, $url) {
            return (new MailMessage)
                ->subject(__('eventmie-pro::em.register_success'))
                ->line('Click the button below to verify your email address.')
                ->action('Verify Email Address', $url);
        });

        $this->registerPolicies();

        if (!$this->app->routesAreCached()) {
            Passport::ignoreRoutes();
        }
        Passport::tokensExpireIn(Carbon::now()->addDays(1));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        Passport::personalAccessTokensExpireIn(Carbon::now()->addMonth(6));

        //
    }
}
