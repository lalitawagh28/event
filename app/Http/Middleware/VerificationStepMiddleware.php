<?php

namespace App\Http\Middleware;

use Classiebit\Eventmie\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerificationStepMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = User::find(Auth::id());

        if (Auth::check() && ($user->role_id == $user->getRoleId('organiser') || $user->role_id == $user->getRoleId('agent'))) {
            if($user) {
                if($user->phone_verified_at == null && config('otp.mobile_otp_enable')) {
                    return redirect()->route("admin.register.mobileVerificationUpdate");
                } elseif ($user->email_verified_at == null && config('otp.email_otp_enable')) {
                    return redirect()->route("admin.register.emailVerification");
                } 
                
            }
        }

        return $next($request);
    }
}
