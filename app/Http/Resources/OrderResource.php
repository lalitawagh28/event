<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\EventResource;
use App\Http\Resources\TransactionResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'eventId' => $this->event_id,
            "event" => ($this->whenLoaded('event')) ? EventResource::make($this->event) : null,
            'customer_id' => $this->customer_id,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'tax' => $this->tax,
            'net_price' => $this->net_price,
            'promocode_id' => $this->promocode_id,
            'promocode_reward' => $this->promocode_reward,
            'delivery_address' => $this->delivery_address,
            'booking_date' => ($this->whenLoaded('event'))?$this->whenLoaded('event')->start_date:null,
        ];
    }
}
