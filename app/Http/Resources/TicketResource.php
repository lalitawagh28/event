<?php

namespace App\Http\Resources;

use App\Models\TaxTicket;
use App\Models\TicketSubCategory;
use Classiebit\Eventmie\Models\Tax;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $price = $this->price ?? 0;
        $sale_price = $this->sale_price ?? 0;
        $displayedPrice = 0;

        if ($sale_price == 0) {
            $displayedPrice = $price;
        } elseif ($sale_price < $price) {
            $displayedPrice = $sale_price;
        } else {
            $displayedPrice = $price;
        }

        $minTicketQuantity = $this->quantity - 10;
        $ticketQuantity = rand($minTicketQuantity, $this->quantity - 1);

        $subCategories = [];
        if ($this->sub_category == 1){
            $subCategories = TicketSubCategory::where('ticket_id', $this->id)->get();
        }

        $price_type_name = "";
        if ($this->price_type == 1){
            $price_type_name = "Price By Category";
        } elseif ($this->price_type == 2) {
            $price_type_name = "Price By SubCategory";
        } else {}

        return [
            "id" => $this->id,
            "ticket_title" => $this->title,
            "price" => $this->price,
            "quantity" => $this->quantity,
            "description" => $this->description,
            "event_id" => $this->event_id,
            "customer_limit" => $this->customer_limit,
            "ticket_soldout" => $this->t_soldout,
            "sale_start_date" => $this->sale_start_date,
            "sale_end_date" => $this->sale_end_date,
            "sale_price" => $this->sale_price,
            "is_donation" => $this->is_donation,
            "ticket_price" => $displayedPrice,
            "ticket_quantity" => $ticketQuantity,
            "ticket_timer" => $this->show_counter,
            "fast_selling" => (int) $this->fast_selling,
            "processing_fee" => $this->taxes,
            "sub_categories" => TicketSubCategoryResource::Collection($subCategories),
            "price_type" => $this->price_type,
            "price_type_name"=> $price_type_name,
        ];
    }
}
