<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "eventId" => $this->event_id,
            "eventTitle" => $this->event_title,
            "eventCategory" => $this->event_category,
            "eventThumb" => ($this->whenLoaded('event')) ? url('storage/' . $this->whenLoaded('event')->thumb)  : null,
            "eventPoster" => ($this->whenLoaded('event')) ? url('storage/' . $this->whenLoaded('event')->poster) : null,
            "eventStartDate" => $this->event_start_date,
            "eventEndDate" => $this->event_end_date,
            "eventStartTime" => $this->event_start_time,
            "eventEndTime" => $this->event_end_time,
            "eventRepetitive" => $this->event_repetitive,
            "ticketId" => $this->ticket_id,
            "ticketTitle" => $this->ticket_title,
            "ticketPrice" => $this->ticket_price,
            "quantity" => $this->quantity,
            "price" => $this->price,
            "tax" => $this->tax,
            "netPrice" => $this->net_price,
            "status" => $this->status,
            "bookingCancel" => $this->booking_cancel,
            "item_sku" => $this->item_sku,
            "orderNumber" => $this->order_number,
            "customerId" => $this->customer_id,
            "organiserId" => $this->organizer_id,
            "transactionId" => $this->transaction_id,
            "customerName" => $this->customer_name,
            "customerEmail" => $this->customer_email,
            "currency" => $this->currency,
            "checkedIn" => $this->checked_in,
            "paymentType" => $this->payment_type,
            "isPaid" => $this->is_paid,
            "isBulk" => $this->is_bulk,
            "promocodeId" => $this->promocode_id,
            "promocode" => $this->promocode,
            "posId" => $this->pos_id,
            "bulkCode" => $this->bulk_code,
            "scannerId" => $this->scanner_id,
            "commonOrder" => $this->common_order,
            "promocodeReward" => $this->promocode_reward,
            "booking_date" => $this->event_start_date,
            "attendees" => ($this->whenLoaded('attendees')) ? AttendeeResource::collection($this->attendees) : null,
            "event" => ($this->whenLoaded('event')) ? EventResource::make($this->event) : null,
            "transaction" => ($this->whenLoaded('transaction')) ? TransactionResource::make($this->transaction) : null,
            "meta" => $this->meta,
            "agentId" => $this->agentId,
            "wallet" => $this->wallet,
            "isTransferred" => $this->isTransferred,
            "subCategory" => ($this->whenLoaded('subCategory')) ? TicketSubCategoryResource::make($this->subCategory) : null,
            "subTitle" => ($this->whenLoaded('subCategory')) ? $this->ticket_title . '(' . $this->subCategory->title . ')' : $this->ticket_title,
        ];
    }
}
