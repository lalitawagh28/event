<?php

namespace App\Http\Resources;

use App\Models\EventCategory;
use Classiebit\Eventmie\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class EventCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $category = Category::findOrFail($this->category_id);
        
        return [
            "category_id" => $this->category_id,
            "category_name" => ($category) ? $category->name : null,
            "event_id" => $this->event_id,
        ];
    }
}
