<?php

namespace App\Http\Resources;

use App\Models\Ticket;
use App\Models\Wishlist;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $status = null;
        $sale_status = null;

        $wishlist = Wishlist::where('user_id', auth()->user()->id)->pluck('event_id')->toArray();

        $dateToCheck = Carbon::parse($this->start_date) ?? null;
        $endDate = Carbon::parse($this->end_date) ?? null;
        $saleDateToCheck = null;
        $saleEndDate = null;

        if (!is_null($dateToCheck) && !is_null($endDate)) {

            if ($dateToCheck->isFuture()) {
                $status = 'Upcoming Events';
            } elseif ($dateToCheck->greaterThanOrEqualTo(Carbon::now()->startOfDay()) && !$endDate->isPast()) {
                $status = 'Event Started';
            } elseif ($endDate->isPast()) {
                $status = 'Event Ended';
            } else {
                $status = null;
            }
        }
        //sales ticket

        $sales = Ticket::where('event_id', $this->id)->first();

        if ($sales !== null) {
            $saleDateToCheck = Carbon::parse($sales->sale_start_date) ?? null;
            $saleEndDate = carbon::parse($sales->sale_end_date) ?? null;

            if (!is_null($saleDateToCheck) && !is_null($saleEndDate)) {

                if ($saleDateToCheck->isFuture()) {
                    $sale_status = 'Upcoming Sales';
                } elseif ($saleDateToCheck->greaterThanOrEqualTo(Carbon::now()->startOfDay()) && !$saleEndDate->isPast()) {
                    $sale_status = 'Sales  Started';
                } elseif ($saleEndDate->isPast()) {
                    $sale_status = 'Sales  Ended';
                } else {
                    $sale_status = null;
                }
            }
        }

        // Given dates and times
        $startDate = $this->start_date;
        $endDate = $this->end_date;
        $startTime = $this->start_time;
        $endTime = $this->end_time;

        // Combine dates and times
        $startDateTime = Carbon::parse("$startDate $startTime");
        $endDateTime = Carbon::parse("$endDate $endTime");
        // Convert to GMT timezone
        // dd($startDateTime, $endDateTime, $startDateTime->setTimezone('GMT'));
        $startDateTime->setTimezone('GMT');
        $endDateTime->setTimezone('GMT');

        // Get the updated dates and times
        $updatedStartDate = $startDateTime->format('Y-m-d');
        $updatedEndDate = $endDateTime->format('Y-m-d');
        $updatedStartTime = $startDateTime->format('H:i:s');
        $updatedEndTime = $endDateTime->format('H:i:s');

        // //Prod Images
        // $trending = null;
        // $card = null;
        // $detail = null;
        // $poster = null;

        // if ($this->id == 36) {
        //     // telugu
        //     $trending = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_telugu_trending.png';
        //     $card = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_telugu_event_card.png';
        //     $detail = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_telugu_event_detail.png';
        //     $poster = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_telugu_event_poster.png';
        // } elseif ($this->id == 37) {
        //     // tamil
        //     $trending = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_tamil_trending.png';
        //     $card = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_tamil_event_card.png';
        //     $detail = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_tamil_event_detail.png';
        //     $poster = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_tamil_event_poster.png';
        // } elseif ($this->id == 38) {
        //     // hindi
        //     $trending = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/pathans_hindi_trending.png';
        //     $card = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/pathans_hindi_event_card.png';
        //     $detail = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/pathans_hindi_event_detail.png';
        //     $poster = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/pathans_hindi_event_poster.png';
        // } elseif ($this->id == 106) {
        //     // tamil uat
        //     $trending = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_tamil_trending.png';
        //     $card = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_tamil_event_card.png';
        //     $detail = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_tamil_event_detail.png';
        //     $poster = 'https://kanexyprodblobstorage.blob.core.windows.net/dhigna/events/December2023/dsp_tamil_event_poster.png';
        // } else {
        //     //
        //     $trending = null;
        //     $card = null;
        //     $detail = null;
        //     $poster = null;
        // }



        if ($request->routeIs('checkout')) {
            return [

                // 'category_name' => $category->name,
                'venue' => $this->venue,
                'start_date' => $this->start_date,
                'end_date' => $this->end_date,
                'start_time' => $this->start_time,
                'end_time' => $this->end_time,
            ];
        } else {
            return [

                "id" => $this->id,
                "title" => $this->title,
                "description" => strip_tags($this->description),
                "faq" => strip_tags($this->faq),
                "thumbnail" => ($this->thumbnail) ? url('storage/' . $this->thumbnail) : null,
                "poster" => ($this->poster) ? url('storage/' . $this->poster) : null,
                "seatingchart_image" => ($this->seatingchart_image) ? asset('storage/' . $this->seatingchart_image) : null,
                "video_link" => $this->video_link,
                "country_id" => $this->country_id,
                "state" => $this->state,
                "city" => $this->city,
                "venue" => $this->venue,
                "address" => $this->address,
                "zipcode" => $this->zipcode,
                "start_date" => $this->start_date,
                "end_date" => $this->end_date,
                "start_time" => $this->start_time,
                "end_time" => $this->end_time,
                "repetitive" => $this->repetitive,
                "featured" => $this->featured,
                "status" => $this->status,
                "category_id" => $this->category_id,
                "price_type" => $this->price_type,
                "latitude" => $this->latitude,
                "longitude" => $this->longitude,
                "item_sku" => $this->item_sku,
                "merge_schedule" => $this->merge_schedule,
                "excerpt" => $this->excerpt,
                "currency" => $this->currency,
                "e_soldout" => $this->e_soldout,
                "show_reviews" => $this->show_reviews,
                "is_private" => $this->is_private,
                "delivery_charge" => $this->delivery_charge,
                "offline_payment_info" => strip_tags($this->offline_payment_info),
                "card_payment_info" => strip_tags($this->card_payment_info),
                "bank_payment_info" => strip_tags($this->bank_payment_info),
                "wishlist" => (in_array($this->id, $wishlist)) ? true : false,
                "Event_status" => $status,
                "event_timer" => $this->show_counter,
                "event_categories" => EventCategoryResource::collection($this->whenLoaded('event_category')),
                "sale_start_date" => $saleDateToCheck,
                "sale_end_date" => $saleEndDate,
                "sale_status" => $sale_status,
                "terms_info" => strip_tags($this->terms_info),
                "guest_star" => $this->guest_star,
                "organised_by" => $this->organised_by,
                "ticket_logo" => ($this->ticket_logo) ? asset('storage/' . $this->ticket_logo) : null,
                "ticket_bg" => ($this->ticket_bg) ? asset('storage/' . $this->ticket_bg) : null,
                "seat_image" => ($this->seat_image) ? asset('storage/' . $this->seat_image) : null,
                "payment_processed" => $this->payment_processed,
                "invoice_logo" => ($this->invoice_logo) ? asset('storage/' . $this->invoice_logo) : null,
                "images" => $this->images,
                "meta_title" => $this->meta_title,
                "meta_keywords" => $this->meta_keywords,
                "meta_description" => $this->meta_description,
                "user_id" => $this->user_id,
                "add_to_facebook" => $this->add_to_facebook,
                "created_at" => $this->created_at,
                "updated_at" => $this->updated_at,
                "slug" => $this->slug,
                "publish" => $this->publish,
                "is_publishable" => $this->is_publishable,
                "online_location" => $this->online_location,
                "youtube_embed" => $this->youtube_embed,
                "vimeo_embed" => $this->vimeo_embed,
                "event_password" => $this->event_password,
                "e_admin_commission" => $this->e_admin_commission,
                "short_url" => $this->short_url,
                "show_attendee_count" => $this->show_attendee_count,
                "is_admin" => $this->is_admin,
                "show_counter" => $this->show_counter,
                "privacy_info" => strip_tags($this->privacy_info),
                "faq_info" => strip_tags($this->faq_info),
                "listview" => $this->listview,
                "checkout_banner" => $this->checkout_banner,
                "calendar_image" => $this->calendar_image,
                "share_url" => url('events/' . $this->slug),
                "gmt_start_date" => $updatedStartDate ?? null,
                "gmt_end_date" => $updatedEndDate ?? null,
                "gmt_start_time" => $updatedStartTime ?? null,
                "gmt_end_time" => $updatedEndTime ?? null,
                "collection_point_enable" => $this->collection_point_enable,
                "same_as_attendee_enable" => $this->same_as_attendee_enable,
                "delivery_address_enable" => ($this->delivery_address_enable == 'true') ? 1 : 0,
                "voucher_code_enable" => $this->voucher_code_enable,
                "thumbnail_azure" => ($this->thumbnail) ? Storage::disk('azure')->temporaryUrl($this->thumbnail, now()->addMinutes(5)) : null,
                "poster_azure" => ($this->poster) ? Storage::disk('azure')->temporaryUrl($this->poster, now()->addMinutes(5)) : null,
                "seatingchart_image_azure" => ($this->seatingchart_image) ? Storage::disk('azure')->temporaryUrl($this->seatingchart_image, now()->addMinutes(5)) : null,
                "ticket_bg_azure" => ($this->ticket_bg) ? Storage::disk('azure')->temporaryUrl($this->ticket_bg, now()->addMinutes(5)) : null,
                "seat_image_azure" => ($this->seat_image) ? Storage::disk('azure')->temporaryUrl($this->seat_image, now()->addMinutes(5)) : null,
                "invoice_logo_azure" => ($this->invoice_logo) ? Storage::disk('azure')->temporaryUrl($this->invoice_logo, now()->addMinutes(5)) : null,
                "checkout_banner_azure" => ($this->checkout_banner) ? Storage::disk('azure')->temporaryUrl($this->checkout_banner, now()->addMinutes(5)) : null,
                "ticket_logo_azure" => ($this->ticket_logo) ? Storage::disk('azure')->temporaryUrl($this->ticket_logo, now()->addMinutes(5)) : null,
                "images_azure" => ($this->images) ? Storage::disk('azure')->temporaryUrl($this->images, now()->addMinutes(5)) : null,
                "calendar_image_azure" => ($this->calendar_image) ? Storage::disk('azure')->temporaryUrl($this->calendar_image, now()->addMinutes(5)) : null,
                "mat_trending" => ($this->mat_trending) ? Storage::disk('azure')->temporaryUrl($this->mat_trending, now()->addMinutes(5)) : null,
                "mat_card" => ($this->mat_card) ? Storage::disk('azure')->temporaryUrl($this->mat_card, now()->addMinutes(5)) : null,
                "mat_detail" => ($this->mat_detail) ? Storage::disk('azure')->temporaryUrl($this->mat_detail, now()->addMinutes(5)) : null,
                "mat_poster" => ($this->mat_poster) ? Storage::disk('azure')->temporaryUrl($this->mat_poster, now()->addMinutes(5)) : null,
                "additional_title" => $this->additional_title,
                "more_information" => strip_tags($this->more_information),
                "notes" => strip_tags($this->notes),
            ];
        }
    }
}
