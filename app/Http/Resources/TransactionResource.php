<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'order_number' => $this->order_number,
            'amount_paid' => $this->amount_paid,
            'item_sku' => $this->item_sku,
            'txn_id' => $this->txn_id,
            'payer_reference' => $this->payer_reference,
            'payment_status' => $this->payment_status,
            'currency' => $this->currency,
            'payment_gateway' => $this->payment_gateway,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
