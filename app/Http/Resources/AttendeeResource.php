<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttendeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "seatId" => $this->seat_id,
            "ticketId" => $this->ticket_id,
            "eventId" => $this->event_id,
            "bookingId" => $this->booking_id,
            "name" => $this->name,
            "phone" => $this->phone,
            "address" => $this->address,
            "seatName" => $this->seat_name,
            "status" => $this->status,
            "checkedIn" => $this->checked_in,
        ];
    }
}
