<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderBookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "order_id" => $this->order_id,
            "order" => ($this->whenLoaded('BookingOrder')) ? OrderResource::make($this->whenLoaded('BookingOrder')) : null,
            "booking_id" => $this->booking_id,
            "booking" => ($this->whenLoaded('Bookings')) ? BookingResource::make($this->whenLoaded('Bookings')) : null,
        ];
    }
}
