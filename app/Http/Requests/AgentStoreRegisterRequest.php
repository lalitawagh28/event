<?php

namespace App\Http\Requests;

use App\Rules\AlphaSpaces;
use App\Rules\Email;
use App\Rules\OnlyRoleEmail;
use App\Rules\PhoneUnique;
use App\Rules\MobileNumber;
use App\Rules\PhoneDigits;
use App\Rules\PhoneUk;
use Illuminate\Foundation\Http\FormRequest;

class AgentStoreRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title_id' => ['required', 'exists:titles,id'],
            'first_name' => ['required', 'string', new AlphaSpaces, 'max:40'],
            'middle_name' => ['nullable', 'string', new AlphaSpaces, 'max:40'],
            'last_name' => ['required', 'string', new AlphaSpaces, 'max:40'],
            'email' => ['required', new OnlyRoleEmail(8), 'email', new Email],
            'country_id' => ['required','exists:register_countries,id'],
            'phone' => ['required', new PhoneUk,new PhoneDigits,new PhoneUnique(3)],
            'address' => ['required'],
            'password' => ['required', 'string', 'digits:6','confirmed'],
            'captcha' => 'required|captcha'
        ];
    }

    public function messages()
    {
        return [
            'title.required'      => 'Please select your title',
            'first_name.required' => 'Please enter your first name',
            'last_name.required' => 'Please enter your last name',
            'email.required' => 'Please enter your email',
            'password.required' => 'Please enter your password',
            'country_id.required' => 'Please select country code',
            'password.required' => 'Please enter your PIN',
            'password.digits' => 'PIN should be six digits',
            'password.confirmed' => 'PIN doest not match',
            'address.required' => 'Please enter your address',
            'captcha.captcha'=>'Invalid captcha code.'
        ];
    }
}
