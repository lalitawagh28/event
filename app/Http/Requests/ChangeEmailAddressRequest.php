<?php

namespace App\Http\Requests;

use App\Rules\Email;
use App\Rules\OnlyRoleEmail;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangeEmailAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => ['required',  "email", new Email, Rule::unique('users')->ignore($this->user()->id, 'id')],
        ];
    }

    public function messages()
    {
        return [
            'email.regex' => "Please enter your email",
        ];
    }
}
