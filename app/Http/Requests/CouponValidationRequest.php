<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CouponValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'agent_name' => 'required|string|regex:/^[a-zA-Z\s]+$/',
            'code_type' => ['required', Rule::in(['free', 'paid'])],
            'code_value' => ['required_if:code_type,==,"paid"','nullable', Rule::in(['fixed', 'percentage'])],
            'discount' => 'required_if:code_type,==,"paid"|nullable|numeric|min:0|max:100',
            'quantity' => 'required|integer|min:0|max:100',
            'max_uses' => 'nullable|integer|min:0|max:10',
            'is_active' => 'required|boolean',
            'valid_from' => 'required|date|after_or_equal:today',
            'valid_to' => 'required|date|after_or_equal:valid_from|after_or_equal:today',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Coupon :Attribute is required.',
            'code_type.required' => 'Coupon type is required.',
            'discount.required_if' => 'Coupon Discount is required If Coupon Type is paid.',
            'code_value.required_if' => 'Coupon Value is required If Coupon Type is paid.',
            'agent_name.required' => 'Agent Name is required.',
            'is_active.required' => 'Coupon status is required.',
            'agent_name.regex' => 'Agent Name should only contain alphabetic characters and spaces.'
        ];
    }
}
