<?php

namespace App\Http\Requests;

use App\Rules\MobileNumber;
use App\Rules\PhoneUnique;
use Illuminate\Foundation\Http\FormRequest;

class ChangePhoneNumberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'phone' => ['required', new MobileNumber,new PhoneUnique()]
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => "Please enter your phone",
        ];
    }
}
