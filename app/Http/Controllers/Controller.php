<?php

namespace App\Http\Controllers;

use App\Http\Helper;
use App\Models\Attendee;
use App\Models\AuthWalletToken;
use App\Models\Booking;
use App\Models\Order;
use App\Models\OrderBooking;
use App\Models\TransferRequest;
use App\Models\User;
use App\Notifications\TicketTransferRequest;
use App\Service\WalletPayService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Facades\Classiebit\Eventmie\Eventmie;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Auth;
use DB;
use App\Models\Event;
use App\Notifications\TicketTransferAcceptNotification;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function updatePasswordView()
    {
        return Eventmie::view('eventmie::password');
    }

    public function updatePassword(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'oldpassword' => ['required'],
            'password' => ['required', 'numeric', 'digits:6', 'confirmed'],
            'password_confirmation' => ['required', 'digits:6', 'numeric'],
            'phone' => ['required', 'regex:/^7\d{9}$|^07\d{9}$/','numeric'],
        ]);

        $validateUser = Helper::validateUser(['email' => $request->input('email'),'phone' => $request->input('phone')]);

        if(is_null($validateUser))
        {

            $user = User::whereEmail($data['email'])->first();

            if (!Hash::check($data['oldpassword'], $user->password)) {
                throw ValidationException::withMessages(['oldpassword' => 'The old password is incorrect.']);
            }

            $user->password = Hash::make($request->input('password'));
            $user->phone = $request->input('phone');
            $user->save();

            $name = explode(' ',$user->name);
            $payload=[
                'notification_type'=>'user_registration',
                'payload'=> [
                    'first_name'=> $name[0] ?? $name[0] ,
                    'last_name'=> @$name[1] ? @$name[1] : 'lastname',
                    'email'=> $user->email,
                    'phone' => $request->input('phone'),
                    'password'=> $request->input('password')
                ]
            ];

            $url = (config('app.env') == 'production') ? 'https://dhigna.com/callback' : 'https://uat.dhigna.com/callback';

            $clientRepository =  app('Laravel\Passport\ClientRepository');
            $clientRepository->create($user->id,$user->name,$url);

            Helper::notifyThroughWebhook($payload);

            $msg = 'Password updated successfully !!';
            session()->flash('status', $msg);

            return redirect(route('eventmie.events_index'));
        }else{
            $msg = 'Wallet account already created !!';
            session()->flash('status', $msg);

            return redirect(route('eventmie.events_index'));
        }
    }

    public function ticketTransfer(Request $request)
    {
        $walletUsers = AuthWalletToken::orderBy('id','desc')->pluck('user_id');
        $users = User::whereIn('id',$walletUsers)->orderBy('id','desc')->get();
        $bookingIds = $request->input('booking_id');

        return view('ticket-transfer.transfer-user', compact('users', 'bookingIds','walletUsers'));
    }

    public function ticketTransferRequest(Request $request)
    {
        // $booking = Booking::find($request->booking_id);


        $bookingIds = explode(',',$request->input('booking_id'));


        foreach($bookingIds as $bookingId)
        {
            $transferRequest = TransferRequest::create([
                'sender_id' => auth()->user()->id,
                'receiver_id' => $request->input('user_id'),
                'booking_id' => $bookingId,
                'type' => $request->input('type'),
                'status' => 0
            ]);
        }


        $receiverUser = User::find($request->input('user_id'));

        //$orderBooking = OrderBooking::whereOrderId($request->input('order_id'))->first();

        $booking = Booking::find($bookingIds[0]);



        $receiverUser->notify(new TicketTransferRequest($booking,auth()->user(),$bookingIds,$request->input('type')));

        $msg = 'Ticekt transfered successfully !!';
        session()->flash('status', $msg);

        return redirect(route('eventmie.mybookings_index'));

    }

    public function ticketTransferAcceptPayment($id,WalletPayService $service)
    {
        session(['transfer_ticket_booking_id' => $id]);

        $bookingIds = explode(',',$id);
        $extra = NULL;

        $transferRequests = TransferRequest::whereIn('booking_id',$bookingIds)->where('status' , 0)->get();

        $transferRequestalreadyPaid = TransferRequest::whereIn('booking_id',$bookingIds)->where('status' , 1)->get();
        if($transferRequests->isEmpty() &&  !is_null($transferRequestalreadyPaid))
        {
            $msg = 'Ticekt transfered request approved already !!';
            session()->flash('status', $msg);

            return redirect(route('eventmie.mybookings_index'));
        }else{
            $qty = 0;
            $price = 0;
            if(!$transferRequests->isEmpty())
            {
                $user = User::find($transferRequests[0]->receiver_id);
                Auth::login($user);



                foreach($transferRequests as  $transferRequest)
                {
                    $booking = Booking::where('id',$transferRequest->booking_id)->first();

                    $qty += $booking->quantity;
                    $price += $booking->price;

                }
            }


            if(!is_null(Auth::user()))
            {
                $walletsInfo = $this->getWallet($service,Auth::user());
                $wallets = collect($walletsInfo['data']['wallets']);

            }
            else{
                $wallets = [];
            }

            if(!empty(setting('apps.stripe_public_key')) && !empty(setting('apps.stripe_secret_key')))
            {
                $extra['is_stripe']     = true;

                if(Auth::check())
                {
                    $user = Auth::user();

                    if(!empty(Auth::user()->is_manager))
                    {
                        $user = User::find(Auth::user()->organizer_id);
                    }

                    $extra['stripe_secret_key'] =  $user->createSetupIntent()->client_secret;
                }

            }

            return view('ticket-transfer.ticket-transfer-paymemt',compact('qty', 'price', 'booking','wallets','extra'));
        }

    }

    public function getWallet($service,$user)
    {
        $wallets = $service->getWalletList($user);
        return $wallets;
    }

    public function ticketTransferStripePayment(Request $request,WalletPayService $service)
    {
        $this->set_payment_method($request, []);
       $this->stripe($request,$service);
    }

    public function ticketTransferWalletPayment(Request $request,WalletPayService $service)
    {


        $info = $request->all();
        $id = session()->get('transfer_ticket_booking_id');

        $bookingIds = explode(',',$id);


        $transferRequest = TransferRequest::whereIn('booking_id',$bookingIds)->where('status' , 0)->first();

        $senderDetails = AuthWalletToken::whereUserId($transferRequest['sender_id'])->first();

        $senderInfo = $service->profile($senderDetails['token']);

        $data = [
            'currency' => "GBP",
            'wallet'   => $info['wallet'],
            'amount'   => $info['price'],
            'service'  => $info['event_title'],
            'sender_id' => $senderInfo['data']['id']
        ];

        $response = $service->ticketWalletPayment($data,auth()->user());


        if($response['code'] == 200)
        {
            $this->ticketTransferAccept($id);
            if (\Request::wantsJson()) {
                return response(['status' => true, 'url'=>route('eventmie.events_index')]);
                }

        } else {
            $msg = $response['message'];

            return response(['status' => false, 'url'=>route('eventmie.events_index'), 'message'=>$msg]);

        }

        return redirect(route('eventmie.events_index'));

    }

    protected function stripe($request,$service)
    {
        $info = $request->all();

        $id = session()->get('transfer_ticket_booking_id');

        $bookingIds = explode(',',$id);
        $customer = Booking::where('id',$bookingIds[0])->first();
        $user = User::find($customer->customer_id);
        Auth::login($user);
        //$customer_email = session('payment_method')['customer_email'];
        $event_title    = 'xyz';
        $flag           = [];

        try
        {
            //current user

            $user = \Auth::user();

            if(!empty(Auth::user()->is_manager))
            {
                $user = User::find(Auth::user()->organizer_id);

            }

            // create customer
            if( empty($user->stripe_id) ){
                $user->createAsStripeCustomer();
            }

            // extra params and it is optional
            $extra_params = [
                "currency"    => "GBP",
                "description" => $event_title,
            ];

            // payment method
            $paymentMethod  = session('payment_method')['setupIntent'];

            // add payment method
            //$user->addPaymentMethod($paymentMethod);

            // payment
            // amount
            $amount     = $info['price'] * 100;
            $amount     = (int) $amount;

            $stripe     = $user->charge($amount, $paymentMethod, $extra_params);

            if($stripe->status == 'succeeded')
            {

                // set data
                if($stripe->charges['data'][0]->paid)
                {
                    $flag['status']             = true;
                    $flag['balance_transaction']     = $stripe->charges['data'][0]->balance_transaction;
                    $flag['payment_method']     = $stripe->charges['data'][0]->payment_method;
                    $flag['sender_payment_id']     = $stripe->charges['data'][0]->id;
                    $flag['sender_card_fingerprint']    = $stripe->charges['data'][0]->payment_method_details['card']['fingerprint'];                  // charge_id
                    $flag['message']            = $stripe->charges['data'][0]->outcome['seller_message']; // outcome message
                    $flag['stripe_fee']    = $stripe->charges['data'][0]->application_fee;
                    $flag['stripe_receipt_url']    = $stripe->charges['data'][0]->receipt_url;
                }
                else
                {
                    $flag['status']             = false;
                    $flag['error']              = $stripe->charges['data'][0]->failure_message;
                }
            }
            else
            {
                $flag = [
                    'status'    => false,
                    'error'     => $stripe->status,
                ];
            }

        }

        // Laravel Cashier Incomplete Exception Handling for 3D Secure / SCA -> 4000000000003220 error card number
        catch (IncompletePayment $ex) {

            $redirect_url = route(
                'cashier.payment',
                [$ex->payment->id, 'redirect' => route('after3DAuthentication',['id' => $ex->payment->id ])]
            );


            return response()->json(['url' => $redirect_url, 'status' => true]);
        }

        // All Exception Handling like error card number
        catch (\Exception $ex)
        {
            // fail case
            $flag = [
                'status'    => false,
                'error'     => $ex->getMessage(),
            ];
        }

        if($flag['status'] == true)
        {

            $transferRequest = TransferRequest::whereIn('booking_id',$bookingIds)->where('status' , 0)->first();

            $senderDetails = AuthWalletToken::whereUserId($transferRequest['sender_id'])->first();

            $senderInfo = $service->profile($senderDetails['token']);

            $flag['currency'] = 'GBP';
            $flag['amount'] = $info['price'];
            $flag['service'] = $info['event_title'];
            $flag['sender_id'] = $senderInfo['data']['id'];


            $response = $service->ticketStripePayment($flag,auth()->user());

            if($response['code'] == 200)
            {

                $this->ticketTransferAccept($id);
                if (\Request::wantsJson()) {
                    return response(['status' => true, 'url'=>route('eventmie.events_index')]);
                    }

            } else {
                $msg = $response['message'];
                if (\Request::wantsJson()) {
                return response(['status' => false, 'url'=>route('eventmie.events_index'), 'message'=>$msg]);
                }

            }

            return redirect(route('eventmie.events_index'));
        }

    }

    protected function set_payment_method(Request $request, $booking = [])
    {
        $id = session()->get('transfer_ticket_booking_id');

        $bookingIds = explode(',',$id);
        $customer = Booking::where('id',$bookingIds[0])->first();
        $user = User::find($customer->customer_id);

        $payment_method = [
            'payment_method' => $request->payment_method,
            'setupIntent'    => $request->setupIntent,
            'customer_email' => $customer->customer_email,
            'customer_name'  => $customer->customer_name,
            'event_title'    => 'ccc',
            'currency'       => 'GBP',

            'cardNumber'     => $request->cardNumber,
            'cardMonth'      => $request->cardMonth,
            'cardYear'       => $request->cardYear,
            'cvc'            => $request->cardCvv,
            'cardName'       => $request->cardName,
        ];

        $cardName = explode(' ', trim($request->cardName));

        // except last
        $payment_method['firstName']        = '';
        foreach ($cardName as $key => $val) {
            if(!end($cardName) === $val) {
                $payment_method['firstName']   .= $val.' ';
            }
        }
        // remove last space
        $payment_method['firstName']        = trim($payment_method['firstName']);

        // the last word
        $payment_method['lastName']     = end($cardName);

        session(['payment_method' => $payment_method]);
    }

    public function ticketTransferAccept($id)
    {

        $bookingIds = explode(',',$id);

        $transferRequests = TransferRequest::whereIn('booking_id',$bookingIds)->where('status' , 0)->get();

        if(!$transferRequests->isEmpty())
        {

            $qty = 0;
            $price = 0;
            $promocode = 0;
            $netprice = 0;
            $tax = 0;
            $receiverId = '';
            $sender= NULL;
            $receiver= NULL;

            foreach($transferRequests as  $transferRequest)
            {
                $receiverUser = User::find($transferRequest->receiver_id);

                $booking = Booking::where('id',$transferRequest->booking_id)->first();

                $qty += $booking->quantity;
                $price += $booking->price;
                $promocode += $booking->promocode_reward;
                $netprice += $booking->net_price;
                $tax += $booking->tax;

                $booking->update([
                    'customer_id' => $transferRequest->receiver_id,
                    'is_transferred' => 1,
                    'customer_name' => $receiverUser?->name,
                    'customer_email' => $receiverUser?->email,
                ]);

                // $attendee = Attendee::where('booking_id',$transferRequest->booking_id)->update([
                //     'name' => $receiverUser?->name,
                //     'phone' => $receiverUser?->phone,
                //     'address' => $receiverUser?->email,
                // ]);


                $transferRequest->status = 1;
                $transferRequest->update(['status' => 1 ]);

                $receiverId = $transferRequest->receiver_id;
                $sender= User::find($transferRequest->sender_id);
                $receiver= User::find($transferRequest->receiver_id);
            }

            $orderBooking = OrderBooking::whereIn('booking_id',$bookingIds)->first();

            $oldOrder = Order::find($orderBooking->order_id);

            $newOrder = Order::create([
                'event_id' => $oldOrder->event_id,
                'customer_id' => $receiverId,
                'quantity' => $qty,
                'price' => $price,
                'promocode_reward' => $promocode,
                'net_price' => $netprice,
                'tax' => $tax
            ]);

            if(($oldOrder->quantity - $qty) > 0)
            {
                $oldOrder->update([
                    'quantity' =>  $oldOrder->quantity - $qty,
                    'price' => $oldOrder->price - $price,
                    'promocode_reward' => $oldOrder->promocode_reward - $promocode,
                    'net_price' => $oldOrder->net_price - $netprice,
                    'tax' => $oldOrder->tax - $tax
                ]);
            }else{
                \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
                $oldOrder->delete();
                \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            }

            foreach($bookingIds as $bookingId)
            {
                OrderBooking::create(['booking_id' => $bookingId,'order_id' => $newOrder->id]);
            }



            $event = Event::find($oldOrder->event_id);
            $sender->notify(new TicketTransferAcceptNotification($sender,$receiver,$event));

            $msg = 'Ticekt transfered request approved successfully !!';

            if($transferRequests[0]->type == 'paid')
            {

                return true;
            }

            session()->flash('status', $msg);

        }else
        {

            $msg = 'Ticekt transfered request approved already!!';

            session()->flash('status', $msg);
        }

        return redirect(route('eventmie.events_index'));

    }

    public function refresh_captcha()
    {
        return response()->json(['captcha'=> captcha_img('flat')]);
    }

    public function refresh_captcha_guest()
    {
        return response()->json(['captcha'=> captcha_src('flat')]);
    }


    public function searchEvent(Request $request)
    {

        return response()->json(['event'=> $request->event,'status' => true]);

    }

    public function getEventInfo($id,$page)
    {
       $event = Event::find($id);
       if($page == 'terms')
       {
            $content =  $event->terms_info;
       }else{
            $content =  $event->privacy_info;
       }
        return view('eventinfodetail',compact('event','content'));
    }

}
