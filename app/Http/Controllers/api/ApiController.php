<?php

namespace App\Http\Controllers\api;

use App\Models\User;
use App\Models\Event;
use App\Models\Order;
use App\Models\Booking;
use App\Service\ApiService;
use Illuminate\Support\Str;
use App\Models\OrderBooking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use App\Models\AuthWalletToken;
use Illuminate\Http\Client\Pool;
use App\Models\EventDeliveryCharge;
use App\Http\Controllers\Controller;
use App\Models\EventCollectionPoint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\EventResource;
use App\Http\Resources\OrderResource;
use Classiebit\Eventmie\Models\Venue;
use App\Http\Resources\BookingResource;
use App\Http\Resources\VenueCollection;
use Classiebit\Eventmie\Models\Category;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\OrderBookingResource;
use App\Http\Resources\EventCollectionPointResource;
use App\Http\Controllers\InvoicesController;
use Classiebit\Eventmie\Scopes\BulkScope;
use Illuminate\Support\Facades\View;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Auth\Events\Registered;
use Classiebit\Eventmie\Notifications\MailNotification;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Notification;

class ApiController extends Controller
{
    private $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    public function filterEvents(Request $request)
    {
        if (($request->input('venue') !== null) || ($request->input('category_id') !== null)) {
            $filteredEvents = $this->apiService->filterEvents($request);
            return response()->json(['data' => $filteredEvents], Response::HTTP_OK);
        }

        $events = Event::with('category', 'event_category', 'event_category.category')->where('status', '1')->where('is_private', '!=', true)->whereDate('end_date', '>=', Carbon::today()->toDateString())->orderBy('id', 'desc')->get();

        return response()->json(['data' => EventResource::collection($events)]);
    }

    public function getCategoryList()
    {
        $category = Category::where(['status' => 1])->get();

        return response()->json(['data' => CategoryCollection::make($category)], Response::HTTP_OK);
    }

    public function getVenueList()
    {
        $venue = Venue::where(['status' => 1])->get();

        return response()->json(['data' => VenueCollection::make($venue)], Response::HTTP_OK);
    }

    public function getEvent(Request $request)
    {
        $event = Event::with('category', 'event_category', 'event_category.category')->findOrFail($request->id);

        return response()->json(['data' => new EventResource($event)], Response::HTTP_OK);
    }

    public function showBooking(Request $request)
    {
        $bookings = Booking::with(['attendees', 'event'])->where('common_order', $request->id)->get();

        return response()->json(['data' => BookingResource::collection($bookings)], Response::HTTP_OK);
    }

    public function getPastBookings()
    {
        $user = Auth::user();

        $bookings = Booking::with(['attendees', 'event'])->whereHas('event', function ($query) {
            $query->where('event_end_date', '<', Carbon::today()->toDateString());
        })->where('customer_id', $user->id)->get();

        return response()->json(['data' => BookingResource::collection($bookings)], Response::HTTP_OK);
    }

    public function getUpcomingBookings()
    {
        $user = Auth::user();

        $bookings = Booking::with(['attendees', 'event'])->whereHas('event', function ($query) {
            $query->where('event_start_date', '>', Carbon::today()->toDateString());
        })->where('customer_id', $user->id)->get();

        return response()->json(['data' => BookingResource::collection($bookings)], Response::HTTP_OK);
    }

    public function myBookings()
    {
        $user = Auth::user();
        $bookings = Booking::with(['attendees', 'event'])->where('customer_id', $user->id)->get();

        return response()->json(['data' => BookingResource::collection($bookings)], Response::HTTP_OK);
    }

    public function validateUser(Request $request)
    {
        /** @var \App\Models\User $user */

        $data = $request->validate([
            'email' => 'required',
            'dgn_tkn' => 'nullable'
        ]);

        $user = User::where('email', $data['email'])->first();

        if (is_null($user)) {
            return response()->json(['message' => 'User Not Found !', 'status' => false], Response::HTTP_NOT_FOUND);
        } elseif (is_null($user->role_id)) {
            $user->role_id = 2;
            $user->update();
        }

        $auth = Auth::loginUsingId($user->id);
        $token = $auth->createToken(Str::random(20))->accessToken;

        AuthWalletToken::updateOrCreate(['user_id' => auth()->user()->id], [
            'user_id' => auth()->user()->id,
            'token' => $data['dgn_tkn'] ?? ''
        ]);
        if (is_null($token)) {
            return response()->json(['message' => 'User Not Found !', 'status' => false], Response::HTTP_UNAUTHORIZED);
        }
        return response()->json(['data' => $token, 'status' => true], Response::HTTP_OK);
    }

    public function registerUser(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'name'     => 'required',
            'dgn_tkn' => 'nullable'
        ]);

        $type = null;

        if (preg_match('/ac.uk$/', $data['email'])) {
            $type = 'student';
        }

        $user = User::create(
            ['password' => Hash::make($request->password), 'email' => $request->email, 'name' => $request->name, 'role_id' => 2, 'type' => $type]
        );

        AuthWalletToken::updateOrCreate(['user_id' => $user->id], [
            'user_id' => $user->id,
            'token' => $data['dgn_tkn'] ?? ''
        ]);

        $token =  $user->createToken(Str::random(20))->accessToken;
        $user->token = $token;
        $this->SendRegisteredMail($user);

        if (is_null($token)) {
            return response()->json(['message' => 'Something went wrong !', 'status' => false], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(['data' => $token, 'status' => true]);
    }

    public function myOrderBookings(Request $request)
    {
        $user = Auth::user();

        $orders = Order::with('event', 'event.category')
            ->where('customer_id', $user->id)
            ->orderBy('id', 'desc')
            ->get();

        return response()->json([
            'orders' => OrderResource::collection($orders),
        ], Response::HTTP_OK);
    }

    // get bookings by customer id
    public function myBookingsByOrder($id)
    {
        $params     = [
            'customer_id'  => Auth::id(),
            'order_id'   => $id,
        ];

        $bookings = OrderBooking::with([
            'Bookings',
            'BookingOrder',
            'Bookings.event',
            'Bookings.transaction',
            'Bookings.attendees',
            'Bookings.event.category'
        ])->where('order_id', $id)->get();

        return response()->json([
            'bookings'  => OrderBookingResource::collection($bookings),
        ], Response::HTTP_OK);
    }

    //get delivery charges of event
    public function getDeliveryCharges($id)
    {
        $event = Event::findOrFail($id);

        $eventDeliveryCharge = EventDeliveryCharge::where('event_id', $event->id)->get();

        return response()->json([
            'data'  => $eventDeliveryCharge
        ], Response::HTTP_OK);
    }

    public function search($postcode)
    {
        $format = [
            'type' => 'boolean',
            'required' => false,
            'default' => true,
        ];

        $sort = [
            'type' => 'boolean',
            'required' => false,
            'default' => true,
        ];

        $data = Http::get("https://api.getAddress.io/suggest/" . $postcode, array("api-key" => config('services.getaddress_api_key'), "top" => 1000, $format, $sort));
        $data = $data->json();

        $addresses = [];

        if (isset($data['suggestions'])) {
            $suggestions = collect($data['suggestions']);

            $addresses_found = Http::pool(fn (Pool $pool) =>
            $suggestions->map(
                fn ($search_id) => $pool->get("https://api.getAddress.io/get/" . $search_id['id'] . "?api-key=" . config('services.getaddress_api_key'))
            ));

            foreach ($addresses_found as $key => $address_found) {
                $address = $address_found->object();

                $addresses[$key]['postcode'] = $address->postcode;
                $addresses[$key]['house_no'] = $address->building_number;
                $addresses[$key]['street'] = $address->thoroughfare;
                $addresses[$key]['address_info'] = $address->line_3;
                $addresses[$key]['city'] = $address->town_or_city;
                $addresses[$key]['county'] = $address->county;
            }
        }
        return response(['data' => $addresses]);
    }

    public function getCollectionPoints($id)
    {
        $event = Event::findOrFail($id);

        $collectionPoints = EventCollectionPoint::where('event_id', $event->id)->get();

        return response()->json(['data'  => EventCollectionPointResource::collection($collectionPoints)], Response::HTTP_OK);
    }

    public function dashboardApis(Request $request)
    {
            try
             {
            $type = $request->input('type') ?? null;
            $events = $this->apiService->getTrendingEvents($type);
            $category = Category::where(['status' => 1])->get();

            $currentDate = Carbon::now();
        $featureEvents = Event::where('status', '1')
        ->where('end_date', '>', $currentDate)
        ->get();
            $data = [
                "trending" => EventResource::collection($events),
                 "categories" => CategoryResource::collection($category)
            ];
            $allData = [
                'events' => $featureEvents,
                'data' => $data,
            ];
     return response()->json($allData, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Something Went Wrong!'], 500);
        }
    }

    public function downloadTicket(Request $request)
    {
        if (!empty(setting('booking.hide_ticket_download')) && (Auth::user()->hasRole('organiser') || Auth::user()->hasRole('customer'))) {
            return response()->json(['error' => 'Not found.'], 404);
        }

        $id = $request->id;
        $bookingData = Booking::where('id', $id)->first();
        $order_number = $bookingData['order_number'];

        // get the booking
        if (!Auth::user()->hasRole('admin')) {
            $booking = Booking::where('id', $id)->where('order_number', $order_number)->first();
        } else {
            $booking = Booking::with(['attendees' => function ($query) {
                $query->where(['status' => 1]);
            }, 'attendees.seat'])->withoutGlobalScope(BulkScope::class)->where(['id' => $id])->get();
        }

        if (empty($booking)) {
            return response()->json(['error' => 'Not found.'], 404);
        }

        // customer can see only their bookings
        if (Auth::user()->hasRole('customer') && $booking['customer_id'] != Auth::id()) {
            return response()->json(['error' => 'Not found.'], 404);
        }

        // organiser can see only their events bookings
        if (Auth::user()->hasRole('organiser') && $booking['organiser_id'] != Auth::id()) {
            return response()->json(['error' => 'Not found.'], 404);
        }

        // generate QrCode
        $qrcode_data = [
            'event_name'          => $booking['event_title'],
            'event_start_date'    => $booking['event_start_date'],
            'event_start_time'    => $booking['event_start_time'],
            'venue'               => $booking->event->venues?->first()?->country->country_name . ',' . $booking->event->venues?->first()?->zipcode
        ];
        $this->createQrcode($booking, $qrcode_data);

        // get event data for ticket pdf
        $event = Event::where('id', $booking['event_id'])->first();
        $currency = setting('regional.currency_default');

        if (!empty($event->currency)) {
            $currency = $event->currency;
        }

        // generate PDF
        $img_path = str_replace('https://', 'http://', url(''));
        $pdf_html = View::make('vendor.eventmie-pro.tickets.pdf1', ['booking' => $booking, 'event' => $event, 'currency' => $currency, 'img_path' => $img_path])->render();
        $pdf_name = $booking['id'] . '-' . $booking['order_number'];
        $this->generatePdf($pdf_html, $pdf_name, $booking);

        $path           = '/storage/ticketpdfs/' . $booking['customer_id'];
        $pdf_file       = public_path() . $path . '/' . $booking['id'] . '-' . $booking['order_number'] . '.pdf';
        $fileStoragePdf = config('app.url') . $path . '/' . $booking['id'] . '-' . $booking['order_number'] . '.pdf';
        if (!\File::exists($pdf_file)) {
            abort('404');
        }
        return response()->json(['data' => $fileStoragePdf], 200);
    }

    protected function createQrcode($data = [], $qrcode_data = [])
    {
        $path           = '/storage/qrcodes/' . $data['customer_id'];
        // first check if directory exists or not
        if (!\File::exists(public_path() . $path))
            \File::makeDirectory(public_path() . $path, 0755, true);

        $qrcode_file    = public_path() . $path . '/' . $data['id'] . '-' . $data['order_number'] . '.png';

        // only create if not already created
        // if (\File::exists($qrcode_file))
        //     return TRUE;

        // generate QrCode
        \QrCode::format('png')->size(512)->generate(json_encode($qrcode_data), $qrcode_file);

        return TRUE;
    }

    /**
     *  generate pdf
     */
    protected function generatePdf($html = null, $pdf_name = null, $data = [])
    {
        $path  = '/storage/ticketpdfs/' . $data['customer_id'];

        // first check if directory exists or not
        if (!\File::exists(public_path() . $path))
            \File::makeDirectory(public_path() . $path, 0755, true);

        $pdf_file    = public_path() . $path . '/' . $data['id'] . '-' . $data['order_number'] . '.pdf';

        // only create if not already created
        // if (\File::exists($pdf_file))
        //     return TRUE;

        // start PDF generation

        // remove white spaces and comments
        $html =  preg_replace('/>\s+</', '><', $html);
        if (empty($html))
            return false;

        $options = [
            'defaultFont' => 'sans-serif',
            'isRemoteEnabled' => TRUE,
            'isJavascriptEnabled' => FALSE,
            'debugKeepTemp' => TRUE,
            'isHtml5ParserEnabled' => TRUE,
            'enable_html5_parser' => TRUE,
        ];
        \PDF::setOptions($options)
            ->loadHTML($html)
            ->setWarnings(false)
            ->setPaper('a3', 'landscape')
            ->save($pdf_file);

        return TRUE;
    }

    public function downloadInvoice(Request $request)
    {
        $orders = OrderBooking::where('order_id', $request->order_id)->first();
        $bookingData = Booking::where('id', $orders['booking_id'])->first();

        $common_order = $bookingData['common_order'];

        // if have no common_order then update
        if (empty($common_order)) {
            $bookingData->common_order = time() . rand(1, 988);
        }
        $bookingData->save();

        $common_order = $bookingData->refresh()->common_order;

        //CUSTOM
        if (!Auth::user()->hasRole('admin')) {
            $bookings = Booking::with(['attendees' => function ($query) {
                $query->where(['status' => 1]);
            }, 'attendees.seat'])->where(['common_order' => $common_order])->get();
        } else {
            $bookings = Booking::with(['attendees' => function ($query) {
                $query->where(['status' => 1]);
            }, 'attendees.seat'])->withoutGlobalScope(BulkScope::class)->where(['common_order' => $common_order])->get();
        }

        //CUSTOM
        if ($bookings->isEmpty()) {
            abort('404');
        }

        // customer can see only their bookings
        if (Auth::user()->hasRole('customer')) {
            if ($bookingData['customer_id'] != Auth::id()) {
                abort('404');
            }
        }
        // organiser can see only their events bookings
        if (Auth::user()->hasRole('organiser')) {
            if ($bookingData['organiser_id'] != Auth::id()) {
                abort('404');
            }
        }
        $common_order = $bookingData['common_order'];

        $file = public_path('/storage/invoices/' . $bookingData['customer_id'] . '/' . $bookingData['common_order'] . '-invoice.pdf');
        $fileStorage = config('app.url') . '/storage/invoices/' . $bookingData['customer_id'] . '/' . $bookingData['common_order'] . '-invoice.pdf';

        if (!\File::exists($file)) {
            $img_path      = str_replace('https://', 'http://', url(''));
            $organizer     = User::where(['id' => $bookingData['organiser_id']])->first();
            $customer      = User::where(['id' => $bookingData['customer_id']])->first();

            // test
            // $img_path      = '';
            // return view('invoice.invoice', compact('booking', 'organizer', 'customer', 'img_path'));
            $pdf_html      = View::make('invoice.invoice1', ['bookings' => $bookings, 'organizer' => $organizer, 'customer' => $customer, 'img_path' => $img_path])->render();
            $pdf_name      = 'invoices/' . $bookingData['customer_id'];

            $invoice       = new InvoicesController();
            $file          = $invoice->generatePdf($pdf_html, $pdf_name, $bookingData);
            return response()->download($file);
        }
        return response()->json(['data' => $fileStorage]);
    }
    public function contactUs()
    {
        $data = [
            "contactUs" => [
                "address" => "2 ,The Beacons, 1st Floor  Hatfield ,Hertfordshire, AL10 8RS, United Kingdom",
                "Phone" => "+44 204577 1234",
                "Email" => " info@dhigna.com",
                "aiSensy" => "7944663450"

            ],
        ];
        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function SendRegisteredMail(User $user)
    {
        if (!empty($user->id)) {
            // ====================== Notification ======================
            $mail['mail_subject']   = __('eventmie-pro::em.register_success');
            $mail['mail_message']   = __('eventmie-pro::em.get_tickets');
            $mail['action_title']   = __('eventmie-pro::em.login');
            $mail['action_url']     = eventmie_url();
            $mail['n_type']         = "user";

            // notification for
            $notification_ids       = [
                1, // admin
                $user->id, // new registered user
            ];

            $users = User::whereIn('id', $notification_ids)->get();
            $setting =  Voyager::model('Setting')->where('key', 'multi-vendor.verify_email')->first();

            try {
                Notification::locale(App::getLocale())->send($users, new MailNotification($mail));
            } catch (\Throwable $th) {
            }
        }
    }

    public function  cmdbForgot(Request $request)
    {
        $data = $request->validate([
            'password'=>'required|min:6|numeric',
            'dgn_tkn'=>'nullable',
            'email' => 'required|email',
        ]);

        $user = User::where('email', $data['email'])->first();

        if (is_null($user)) {
            return response()->json(['message' => 'User Not Found !', 'status' => false], Response::HTTP_NOT_FOUND);
        } else {
            $user->password = Hash::make($data['password']);
            $user->save();
            return response()->json(['message' => 'forgot pin updated successfully'], 200);
        }
    }
}
