<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WishlistResource;
use App\Models\Event;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function getWishlist()
    {
        $user = Auth::user();
        $status = 1;
        $eventIds = Event::where('status', $status)->pluck('id')->toArray();

        $wishlist = Wishlist::with('event', 'event.category')->where('user_id', $user->id)->whereIn('event_id', $eventIds)->get();

        if ($wishlist->count() == 0) {
            return response()->json(['message' => 'No wishlist']);
        }

        return response()->json(['data' => WishlistResource::collection($wishlist)], Response::HTTP_OK);
    }

    public function add_remove_wishlist(Request $request)
    {
        $user = Auth::user();
        $wishlist = new Wishlist();

        $data = [
            'user_id' => $user->id,
            'event_id' => $request['id'],
        ];

        $existWishlist = $wishlist->where('event_id', $data['event_id'])->where('user_id',$data['user_id'])->first();

        if (!is_null($existWishlist) && ($request->wishlist_status == true)) {

            if ($existWishlist['user_id'] == $data['user_id'] && $existWishlist['event_id'] == $data['event_id']) {

                $existWishlist->delete();

                return response()->json(['data' => "Successfully removed from wishlist"], Response::HTTP_OK);
            }
        }

        $store = $wishlist->create($data);

        return response()->json(['message' => "Successfully added in wishlist"], Response::HTTP_OK);
    }
}
