<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\InvoicesController;
use App\Http\Controllers\SmsNotificationController;
use App\Http\Resources\EventResource;
use App\Http\Resources\EventsShowResource;
use App\Http\Resources\TicketResource;
use App\Mail\BookingMail;
use App\Models\AgentTicket;
use App\Models\Attendee;
use App\Models\Booking;
use App\Models\DeliveryCharge;
use App\Models\Event;
use App\Models\Order;
use App\Models\Promocode;
use App\Models\Seat;
use App\Models\Ticket;
use App\Models\User;
use App\Notifications\BookingNotification;
use App\Service\API\BookingService;
use App\Service\WalletPayService;
use Classiebit\Eventmie\Models\Commission;
use Classiebit\Eventmie\Models\Notification;
use Classiebit\Eventmie\Models\Tax;
use Classiebit\Eventmie\Models\Transaction;
use Classiebit\Eventmie\Services\PaypalExpress;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use App\Models\EventDeliveryCharge;
use App\Models\SensyLog;
use App\Models\TicketSubCategory;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;



class BookingController extends Controller
{
    protected $currency = null;

    private $event;
    private $ticket;
    private $booking;
    private $transaction;
    private $user;
    private $commission;
    private $tax;
    private $customer_id;
    private $organiser_id;
    private $service;
    private $promocode;
    private $valid_promocodes;
    private $delivery_charge;
    private $event_delivery_charge;
    private $valid_vouchercodes;
    private $agent_tickets;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->event = new Event;
        $this->ticket = new Ticket;
        $this->booking = new Booking();
        $this->transaction = new Transaction();
        $this->user = new User();
        $this->commission = new Commission();
        $this->tax = new Tax();
        $this->promocode = new Promocode();
        $this->customer_id = null;
        $this->organiser_id = null;
        $this->service = new WalletPayService;
        $this->delivery_charge = new DeliveryCharge();
        $this->event_delivery_charge = new EventDeliveryCharge();
        $this->agent_tickets = new AgentTicket();
    }

    /**
     * showBooking
     *
     * @param  mixed $request
     * @return void
     */
    public function showBooking(Request $request)
    {
        $event = Event::with(['category'])->where('id', $request->id)->first();
        return new EventResource($event);
    }

    /**
     * ticketDetail
     *
     * @param  mixed $request
     * @return void
     */
    public function ticketDetail(Request $request)
    {
        $ticketInfo = Ticket::where('event_id', $request->id)->get();
        return TicketResource::collection($ticketInfo);
    }

    // check for available seats
    protected function availability_validation($params = [])
    {
        $event_id = $params['event_id'];
        $selected_tickets = $params['selected_tickets'];
        $ticket_ids = $params['ticket_ids'];
        $booking_date = $params['booking_date'];

        //CUSTOM
        if (empty($params['is_bulk'])) {
            //CUSTOM
            // 1. Check booking.max_ticket_qty
            foreach ($selected_tickets as $key => $value) {
                // user can't book tickets more than limitation
                if ($value['quantity'] > setting('booking.max_ticket_qty')) {
                    $msg = __('eventmie-pro::em.max_ticket_qty');

                    return ['status' => false, 'error' => $msg . setting('booking.max_ticket_qty')];
                }
            }
            //CUSTOM
        }
        //CUSTOM

        // 2. Check availability over booked tickets

        // actual tickets
        $tickets = $this->ticket->get_booked_tickets($ticket_ids);

        // get the bookings for live availability check
        $bookings = $this->booking->get_seat_availability_by_ticket($event_id);

        // actual tickets (quantity) - already booked tickets on booking_date (total_booked)
        foreach ($tickets as $key => $ticket) {
            //CUSTOM
            if ($ticket->t_soldout > 0) {

                return ['status' => false, 'error' => $ticket->title . ':- ' . __('eventmie-pro::em.t_soldout')];
            }
            //CUSTOM
            foreach ($selected_tickets as $k => $selected_ticket) {
                if ($ticket->id == $selected_ticket['ticket_id']) {
                    //CUSTOM
                    if (empty($params['is_bulk'])) {
                        //CUSTOM

                        // Customer limit check
                        $error = $this->customer_limit($ticket, $selected_ticket, $booking_date);
                        if (!empty($error))
                            return ['status' => false, 'error' => $error];

                        // First. check selected quantity against actual ticket capacity
                        if ($selected_ticket['quantity'] > $ticket->quantity)
                            return ['status' => false, 'error' => $ticket->title . ' ' . __('eventmie-pro::em.vacant') . ' - ' . $ticket->quantity];
                        //CUSTOM
                    }
                    //CUSTOM

                    // Second. seat availability for selected booking-date in bookings table
                    foreach ($bookings as $k2 => $booking) {

                        // check for specific dates + specific ticket
                        if ($booking->event_start_date == $booking_date && $booking->ticket_id == $ticket->id) {
                            $available = $ticket->quantity - $booking->total_booked;

                            //CUSTOM
                            if (empty($params['is_bulk'])) {
                                //CUSTOM

                                // false condition
                                // if selected ticket quantity is greator than available
                                if ($selected_ticket['quantity'] > $available)
                                    return ['status' => false, 'error' => $ticket->title . ' ' . __('eventmie-pro::em.vacant') . ' - ' . $available];

                                //CUSTOM
                            }
                            //CUSTOM
                        }
                    }
                }
            }
        }
        return ['status' => true];
    }


    /* =================== PAYPAL ==================== */

    /**
     * 4 Finish checkout process
     * Last: Add data to purchases table and finish checkout
     */
    protected function finish_checkout($flag = [])
    {
        // prepare data to insert into table
        $data = session('pre_payment');

        $booking = session('booking');

        // IMPORTANT!!! clear session data setted during checkout process


        /* CUSTOM */
        $payment_method = (int) session('payment_method')['payment_method'];

        // CUSTOM

        // if success
        if ($flag['status']) {
            $data['txn_id'] = $flag['transaction_id'];
            $data['amount_paid'] = $data['price'];
            unset($data['price']);
            $data['payment_status'] = $flag['message'];
            $data['payer_reference'] = $flag['payer_reference'];
            $data['status'] = 1;
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            // $data['currency_code']      = setting('regional.currency_default');
            $data['currency_code'] = !empty($booking[key($booking)]['currency']) ? $booking[key($booking)]['currency'] : setting('regional.currency_default');
            $data['payment_gateway'] = 'paypal';
            /* CUSTOM */
            $data['payment_gateway'] = $payment_method == 2 ? 'Stripe' : 'PayPal';

            if ($payment_method == 3)
                $data['payment_gateway'] = 'AuthorizeNet';

            if ($payment_method == 4)
                $data['payment_gateway'] = 'BitPay';

            if ($payment_method == 5)
                $data['payment_gateway'] = 'Stripe Direct';

            if ($payment_method == 6)
                $data['payment_gateway'] = 'Paystack';

            if ($payment_method == 7)
                $data['payment_gateway'] = 'Razorpay';

            if ($payment_method == 8)
                $data['payment_gateway'] = 'Paytm';

            /* CUSTOM */ // insert data of paypal transaction_id into transaction table

            // insert data of paypal transaction_id into transaction table
            $flag = $this->transaction->add_transaction($data);

            $data['transaction_id'] = $flag; // transaction Id

            $flag = $this->finish_booking($booking, $data);

            // in case of database failure
            if (empty($flag)) {
                $msg = __('eventmie-pro::em.booking') . ' ' . __('eventmie-pro::em.failed');

                $err_response[] = $msg;
            }

            // redirect no matter what so that it never turns back
            $msg = __('eventmie-pro::em.booking_success');
        }

        // if fail
        // redirect no matter what so that it never turns back
        $msg = __('eventmie-pro::em.payment') . ' ' . __('eventmie-pro::em.failed');
        // session()->flash('error', $msg);

        /* CUSTOM */
        // if Stripe
        if (Request::wantsJson()) {
            return response(['status' => false, 'url' => $url, 'message' => $msg], 200);
        }

        $err_response[] = $msg;

        return redirect($url)->withErrors($err_response);
        /* CUSTOM */

        // return error_redirect($msg);
    }

    // validate user post data
    protected function general_validation(Request $request)
    {
        $attedees = [];

        if (empty($request->is_bulk)) {
            $attedees = $this->attendeesValidations($request);
        }

        $request->validate([
            'event_id' => 'required|numeric|gte:1',
            'ticket_id' => ['required', 'array'],
            'ticket_id.*' => ['required', 'numeric'],
            'quantity' => ['required', 'array'],
            'quantity.*' => ['required', 'numeric', 'integer', 'gte:0'],

            // repetitve booking date validation
            'booking_date' => 'date_format:Y-m-d|required',
            'start_time' => 'date_format:H:i:s|required',
            'end_time' => 'date_format:H:i:s|required',
            'voucher_code' => ['nullable'],
            'address_type'  => ['required', 'in:1,2,3'],
            'delivery_charge_id' => ['nullable', 'numeric', 'exists:event_delivery_charges,id'],

        ]);

        if (!empty($request->merge_schedule)) {
            $request->validate([
                'booking_end_date' => 'date_format:Y-m-d|required',
            ]);
        }
        $voucher_code = null;
        if (!empty($request->voucher_code)) {
            $voucher_code = $request->voucher_code;
        }

        // get event by event_id
        $event = $this->event->get_event(null, $request->event_id);
        //CUSTOM
        if ($event->e_soldout > 0)
            return ['status' => false, 'error' => __('eventmie-pro::em.e_soldout')];

        // if event not found then access denied
        if (empty($event))
            return ['status' => false, 'error' => __('eventmie-pro::em.event') . ' ' . __('eventmie-pro::em.not_found')];

        // get only ticket_ids which quantity is >0
        $ticket_ids = [];
        $selected_tickets = [];

        // CUSTOM

        $customer = Auth::user();

        if (!empty($request->is_bulk)) {
            $customer = Auth::user();
        }

        $selected_attendees = [];


        // CUSTOM
        $finalFamilyTotal = [];
        $voucher_reward = 0;
        foreach ($request->quantity as $key => $val) {
            if ($val) {
                $params = [];
                $params['ticket_id'] = $request->ticket_id[$key];
                $params['sub_category_id'] = null;
                $params['voucher_code'] = $voucher_code;
                $params['quantity'] = $val < 1 ? 1 : $val;
                $ticket_ids[] = $request->ticket_id[$key];
                $selected_tickets[$key]['ticket_id'] = $request->ticket_id[$key];
                $selected_tickets[$key]['ticket_title'] = $request->ticket_title[$key];
                $selected_tickets[$key]['quantity'] = $val < 1 ? 1 : $val; // min qty = 1

                // CUSTOM
                if (empty($request->is_bulk)) {
                    $selected_attendees[$key]['name'] = $attedees['name'][$key];
                    $selected_attendees[$key]['phone'] = $attedees['phone'][$key];
                    $selected_attendees[$key]['address'] = $attedees['address'][$key];
                    $selected_attendees[$key]['ticket_id'] = $request->ticket_id[$key];
                }

                $selected_tickets[$key]['is_donation'] = floatval($request->is_donation[$key]);
                if ($voucher_code) {
                    // dd($params);
                    list($amt, $voucher_data) = $this->get_voucher_rewards($params);

                    $voucher_reward += $amt;
                    $selected_tickets[$key]['voucher_data'] = $voucher_data;
                }
                // CUSTOM
            }
        }

        if (empty($ticket_ids))
            return ['status' => false, 'error' => __('eventmie-pro::em.select_a_ticket')];

        $params = [
            'event_id' => $request->event_id,
            'ticket_ids' => $ticket_ids,
        ];

        // check ticket in tickets table that exist or not
        $tickets = $this->ticket->get_event_tickets($params);

        // if ticket not found then access denied
        if ($tickets->isEmpty())

            return ['status' => false, 'error' => __('eventmie-pro::em.tickets') . ' ' . __('eventmie-pro::em.not_found')];

        //CUSTOM
        $seats = [];

        foreach ($tickets as $key => $ticket) {

            if (!empty($request->familyPackAttendee[$key])) {
                array_push($finalFamilyTotal, ['ticket_id' => $ticket->id, 'FamilMembersTotal' => $request->familyPackAttendee[$key]]);
            }
            $seat_ticket = 'seat_id_' . $ticket->id;

            if (!empty($ticket->seatchart)) {
                if ($ticket->seatchart->seats->isNotEmpty() && !empty($request->$seat_ticket)) {
                    foreach ($request->$seat_ticket as $key1 => $seat_id) {
                        //check seat in database
                        $seats[$seat_ticket][$key1] = Seat::with(['attendees', 'attendees.booking'])->where(['id' => $seat_id, 'status' => 1])->first()->toArray();

                        //if seat not found then show error
                        if (empty($seats[$seat_ticket][$key1]))

                            return ['status' => false, 'error' => __('eventmie-pro::em.seat') . ' ' . __('eventmie-pro::em.not_found')];

                        //attendees on particular seat
                        $attendees = collect($seats[$seat_ticket][$key1]['attendees'])->where(['status' => 1]);

                        if ($attendees->isNotEmpty()) {
                            // date wise validation and check that the seat reserved on specific date or not
                            $seat_available = $attendees->every(function ($attendee, $ak) use ($request) {

                                return $attendee['booking']['event_start_date'] != $request->booking_date;
                            });

                            if (!$seat_available)
                                return ['status' => false, 'error' => __('eventmie-pro::em.seat') . ' ' . __('eventmie-pro::em.not_found')];
                        }
                    }
                }
            }
        }

        $deliveryChargeValidation = EventDeliveryCharge::findOrFail($request->delivery_charge_id);
        if (!empty($request->delivery_charge_id) && $deliveryChargeValidation->event_id != $request->event_id)
            return ['status' => false, 'error' => __('eventmie-pro::em.invalid_delivery_charge_id')];


        return [
            'status' => true,
            'event_id' => $request->event_id,
            'selected_tickets' => $selected_tickets,
            'tickets' => $tickets,
            'ticket_ids' => $ticket_ids,
            'event' => $event,
            'booking_date' => $request->booking_date,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'customer' => $customer,
            'is_bulk' => $request->is_bulk,
            'selected_attendees' => $selected_attendees,
            'seats' => $seats,
            'delivery_charge_id' => $request->delivery_charge_id ?? '',
            'transaction_id' => $request->transaction_id ?? '',
            'voucher_reward' => $voucher_reward,

        ];
    }

    protected function get_voucher_rewards($params)
    {

        $type[] = 'general';
        $data = [];
        $voucher_reward = 0;
        if (Auth::user()->type == 'student') {
            $type[] = 'student';
        }

        try {

            $ticket = Ticket::findOrFail($params['ticket_id']);
            $price = $ticket->get_price();
            $check_vouchercode = AgentTicket::where(['ticket_id' => $params['ticket_id']])->whereIn('type', $type)->orderBy('type', 'desc')->where(['coupon_code' => $params['voucher_code']])->firstOrFail();
            $book_count = $check_vouchercode->bookings?->count();

            if (($check_vouchercode->quantity - $book_count) < $params['quantity']) {
                $sold_out = $params['quantity'] - $check_vouchercode->quantity + $book_count;
                $qty = $params['quantity'] - $sold_out;
            } else {
                $qty = $params['quantity'];
            }

            if ($qty > 0) {
                $validDate = strtotime($check_vouchercode->valid_to);
                $todayDate = strtotime(date("Y-m-d"));

                if (($validDate < $todayDate) && ($validDate != $todayDate)) {
                } else {

                    $user_promocode = [];
                    // manual check in promocode_user if promocode not already applied by the user

                    $params = [
                        'user_id' => Auth::id(),
                        'agent_ticket_id' => $check_vouchercode->id,
                        'ticket_id' => $params['ticket_id'],
                        'sub_category_id' => $params['sub_category_id'] ?? null,
                    ];

                    $agent_tickets = new AgentTicket();
                    $user_promocode = $agent_tickets->agentcode_user($params);
                    $already_used = $check_vouchercode->max_uses - $user_promocode;

                    if ($already_used > $qty) {
                        //$voucher_reward = $qty * $check_vouchercode->discount;
                        if ($check_vouchercode->code_value == 'percent') {
                            $voucher_reward = $qty * $price * ($check_vouchercode->discount / 100);
                        } else {
                            $voucher_reward = $qty * $check_vouchercode->discount;
                        }
                    } else {
                        $qty = $already_used;
                        //$voucher_reward = $already_used * $check_vouchercode->discount;
                        if ($check_vouchercode->code_value == 'percent') {
                            $voucher_reward = $already_used * $price * ($check_vouchercode->discount / 100);
                        } else {
                            $voucher_reward = $already_used * $check_vouchercode->discount;
                        }
                    }
                    if ($voucher_reward) {
                        $data['qty'] = $qty;
                        $data['agent_id'] = $check_vouchercode->id;
                    }
                }
            }
        } catch (\Throwable $e) {
            // dd($e->getMessage());
        }
        return [$voucher_reward, $data];
    }
    // calculate admin commission
    protected function calculate_commission($booking = [], $booking_organiser_price = [], $booking_admin_tax = [])
    {
        $commission = [];
        $admin_commission = setting('multi-vendor.admin_commission');
        //CUSTOM
        $admin_commission = $this->e_admin_commission($booking[key($booking)]['event_id'], $admin_commission);
        //CUSTOM

        $margin = 0;

        if (empty($admin_commission))
            $admin_commission = 0;

        foreach ($booking as $key => $value) {
            // skip for free tickets
            // calculate commission on organiser_price
            // excluding admin_tax
            $organiser_price = $booking_organiser_price[$key]['organiser_price'];
            $admin_tax = $booking_admin_tax[$key]['admin_tax'];

            if ($organiser_price > 0) {
                $commission[$key]['organiser_id'] = $value['organiser_id'];
                $commission[$key]['customer_paid'] = $organiser_price;

                if ($admin_commission > 0)
                    $margin = (float) (($admin_commission * $organiser_price) / 100);

                $commission[$key]['organiser_earning'] = (float) $organiser_price - $margin;

                // customer_paid - organizer_earning = admin_commission
                $commission[$key]['admin_commission'] = $commission[$key]['customer_paid'] - $commission[$key]['organiser_earning'];

                $commission[$key]['admin_tax'] = $admin_tax;
            }
        }

        return $commission;
    }

    // 5. finish booking
    protected function finish_booking($booking = [], $data = [], $seats, $selected_attendees, $transaction_data = [], $flag = [], $commission = [], $order = [], Request $request)
    {
        //CUSTOM
        $bulk_code = empty($data['bulk_code']) ? null : $data['bulk_code'];
        $payment_gateway = !empty($data['payment_gateway']) ? $data['payment_gateway'] : 'Free';
        //CUSTOM

        $admin_commission = setting('multi-vendor.admin_commission');
        // dd($data);
        $params = [];
        foreach ($booking as $key => $value) {
            $params[$key] = $value;
            // $params[$key]['order_number']    = $data['order_number'];
            //CUSTOM
            $params[$key]['order_number'] = time() . rand(1, 988);
            $params[$key]['bulk_code'] = $bulk_code;
            //CUSTOM
            $params[$key]['transaction_id'] = $data['transaction_id'];

            // is online or offline
            $params[$key]['payment_type'] = 'offline';
            if ($data['transaction_id'])
                $params[$key]['payment_type'] = 'online';
        }

        // get booking_id
        // update commission session array
        // insert into commission
        $commission_data = [];
        // $commission                 = session('commission');

        // delete commission data from session
        $booking_data = [];
        foreach ($booking as $key => $value) {
            $data = $this->booking->make_booking($params[$key]);
            $booking_data[] = $data;
            if ($value['net_price'] > 0) {
                $commission_data[$key] = $commission[$key];
                $commission_data[$key]['booking_id'] = $data->id;
                $commission_data[$key]['month_year'] = Carbon::parse($data->created_at)->format('m Y');
                $commission_data[$key]['created_at'] = Carbon::now();
                $commission_data[$key]['updated_at'] = Carbon::now();
                $commission_data[$key]['event_id'] = $data->event_id;
                $commission_data[$key]['status'] = $data->is_paid > 0 ? 1 : 0;
            }
        }

        if ($flag['status']) {
            $transaction_data['txn_id'] = $flag['transaction_id'];
            $transaction_data['user_id'] = auth()->user()->id;
            $transaction_data['amount_paid'] = $order['price'];
            unset($transaction_data['price']);
            $transaction_data['currency_code'] = !empty($booking[key($booking)]['currency']) ? $booking[key($booking)]['currency'] : setting('regional.currency_default');
            $transaction_data['payment_status'] = $flag['message'];
            $transaction_data['order_number'] = $order['order_number'];
            $transaction_data['payer_reference'] = $flag['payer_reference'];
            $transaction_data['status'] = 1;
            $transaction_data['item_sku'] = $order['item_sku'];
            $transaction_data['payment_gateway'] = 'Worldpay';
            $transaction_data['currency'] = $order['currency'];
            $transaction_data['created_at'] = Carbon::now();
            $transaction_data['updated_at'] = Carbon::now();
        }

        // insert data in commission table
        $this->commission->add_commission($commission_data);

        //CUSTOM
        $this->check_promocode($this->valid_promocodes);

        $this->save_attendee($booking_data, $seats, $selected_attendees);
        // dd($booking_data);
        $this->transaction->add_transaction($transaction_data);
        $order = $this->create_order($booking_data, $request->event_promocode, $request->delivery_address, $request->address_type, $request->collection_point);

        $this->send_email($booking_data);
        return true;
    }

    /**
     *  calculate net price for paypal
     */

    protected function calculate_price($params = [])
    {
        // check ticket in tickets table that exist or not
        $ticket = $this->ticket->get_ticket($params);

        //CUSTOM
        //set sale price
        if (!empty($ticket)) {
            if (!empty($ticket->sale_start_date)) {
                if ($ticket->sale_price > 0 && $ticket->sale_start_date <= Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString() && $ticket->sale_end_date > Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString()) {
                    $ticket->price = $ticket->sale_price;
                }
            }
        }
        //CUSTOM

        // apply admin tax
        $ticket = $this->admin_tax($ticket);

        $net_price = [];
        $amount = 0;
        $tax = 0;
        $excluding_tax = 0;
        $including_tax = 0;

        $amount = $ticket['price'] * $params['quantity'];

        //CUSTOM
        if (!empty($ticket['is_donation'])) {
            $amount = $params['is_donation'] * 1;
        }
        //CUSTOM

        $net_price['tax'] = $tax;
        $net_price['net_price'] = $tax + $amount;

        // organiser_price = net_price excluding admin_tax
        $net_price['organiser_price'] = $tax + $amount;
        $excluding_tax_organiser = 0;
        $including_tax_organiser = 0;
        $admin_tax = 0;

        // calculate multiple taxes on ticket
        if ($ticket['taxes']->isNotEmpty() && $amount > 0) {
            foreach ($ticket['taxes'] as $tax_k => $tax_v) {
                //if have no taxes then return net_price
                if (empty($tax_v->rate_type))
                    return $net_price;

                // in case of percentage
                if ($tax_v->rate_type == 'percent') {
                    $tax = (float) ($amount * $tax_v->rate) / 100;

                    // in case of including
                    if ($tax_v->net_price == 'including') {
                        $including_tax = $tax + $including_tax;

                        // exclude admin tax
                        if (!$tax_v->admin_tax)
                            $including_tax_organiser = $tax + $including_tax_organiser;

                        //admin tax
                        if ($tax_v->admin_tax)
                            $admin_tax = $admin_tax + $tax;
                    }

                    // in case of excluding
                    if ($tax_v->net_price == 'excluding') {
                        $excluding_tax = $tax + $excluding_tax;

                        // exclude admin tax
                        if (!$tax_v->admin_tax)
                            $excluding_tax_organiser = $tax + $excluding_tax_organiser;

                        //admin tax
                        if ($tax_v->admin_tax)
                            $admin_tax = $admin_tax + $tax;
                    }
                }

                //  in case of fixed
                if ($tax_v->rate_type == 'fixed') {
                    $tax = (float) ($params['quantity'] * $tax_v->rate);

                    // // in case of including
                    if ($tax_v->net_price == 'including') {
                        $including_tax = $tax + $including_tax;

                        // exclude admin tax
                        if (!$tax_v->admin_tax)
                            $including_tax_organiser = $tax + $including_tax_organiser;

                        //admin tax
                        if ($tax_v->admin_tax)
                            $admin_tax = $admin_tax + $tax;
                    }

                    // // in case of excluding
                    if ($tax_v->net_price == 'excluding') {
                        $excluding_tax = $tax + $excluding_tax;

                        // exclude admin tax
                        if (!$tax_v->admin_tax)
                            $excluding_tax_organiser = $tax + $excluding_tax_organiser;

                        //admin tax
                        if ($tax_v->admin_tax)
                            $admin_tax = $admin_tax + $tax;
                    }
                }
            }
        }

        $net_price['tax'] = (float) ($excluding_tax + $including_tax);
        $net_price['net_price'] = (float) ($amount + $excluding_tax);

        // organiser_price excluding admin_tax
        $net_price['organiser_price'] = (float) ($amount + $excluding_tax_organiser);

        //admin tax
        $net_price['admin_tax'] = (float) ($admin_tax);

        return $net_price;
    }

    /*====================== Payment Method Store In Session =======================*/

    protected function set_payment_method(Request $request, $booking = [], $stripeData = [])
    {
        $payment_method = [
            'payment_method' => $request->payment_method ?? null,
            'setupIntent' => $stripeData['paymentIntent'] ?? null,
            'customer_email' => $booking[key($booking)]['customer_email'] ?? null,
            'customer_name' => $booking[key($booking)]['customer_name'] ?? null,
            'event_title' => $booking[key($booking)]['event_title'] ?? null,
            'currency' => $booking[key($booking)]['currency'] ?? null,
            'cardNumber' => $request->cardNumber ?? null,
            'cardMonth' => $request->cardMonth ?? null,
            'cardYear' => $request->cardYear ?? null,
            'cvc' => $request->cardCvv ?? null,
            'cardName' => $request->cardName ?? null,
        ];

        $cardName = explode(' ', trim($request->cardName));

        // except last
        $payment_method['firstName'] = '';
        foreach ($cardName as $key => $val) {
            if (!end($cardName) === $val) {
                $payment_method['firstName'] .= $val . ' ';
            }
        }
        // remove last space
        $payment_method['firstName'] = trim($payment_method['firstName']);

        // the last word
        $payment_method['lastName'] = end($cardName);

        return $payment_method;
    }

    /*===========================multiple payment method ===============*/

    protected function multiple_payment_method($order = [], $booking = [], $payment_data = [])
    {
        $url = route('eventmie.events_index');
        $msg = __('eventmie-pro::em.booking') . ' ' . __('eventmie-pro::em.failed');

        $payment_method = (int) $payment_data['payment_method'];

        $currency = !empty($booking[key($booking)]['currency']) ? $booking[key($booking)]['currency'] : setting('regional.currency_default');

        if ($payment_method == 1) {
            if (empty(setting('apps.paypal_secret')) || empty(setting('apps.paypal_client_id')))
                return response()->json(['status' => false, 'url' => $url, 'message' => $msg]);

            return $this->paypal($order, $currency);
        }

        if ($payment_method == 2) {
            if (empty(setting('apps.stripe_public_key')) || empty(setting('apps.stripe_secret_key')))
                return response()->json(['status' => false, 'url' => $url, 'message' => $msg]);

            return $this->stripe($order, $currency, $payment_data);
        }

        if ($payment_method == 3) {
            if (empty(setting('apps.authorize_transaction_key')) || empty(setting('apps.authorize_login_id')))
                return response()->json(['status' => false, 'url' => $url, 'message' => $msg]);

            return $this->authorizeNetPayment($order, $currency);
        }

        if ($payment_method == 4) {
            if (empty(setting('apps.bitpay_key_name')) || empty(setting('apps.bitpay_encrypt_code')))
                return response()->json(['status' => false, 'url' => $url, 'message' => $msg]);

            return $this->bitpay->bitpayPaymentRequest($order, $currency);
        }

        if ($payment_method == 5) {
            if (empty(setting('apps.stripe_public_key')) || empty(setting('apps.stripe_secret_key')) || empty(setting('apps.stripe_direct'))) {

                return response()->json(['status' => false, 'url' => $url, 'message' => $msg]);
            }

            //check stripe account verified or not
            $stripeCheckout = new StripeDirectController();

            $error = $stripeCheckout->checkStripeAccount($booking[key($booking)]['organiser_id']);

            if (!empty($error)) {
                return response()->json(['status' => false, 'url' => $url, 'message' => $error]);
            }

            return response()->json(['status' => true, 'url' => route('stripe_checkout')]);
        }

        if ($payment_method == 6) {
            return $this->paystack($order, $currency);
        }

        if ($payment_method == 7) {
            if (empty(setting('apps.razorpay_keyid')) || empty(setting('apps.razorpay_keysecret')))
                return response()->json(['status' => false, 'url' => $url, 'message' => $msg]);

            return $this->razorpay($order, $currency);
        }

        if ($payment_method == 8) {
            if (empty(setting('apps.paytm_merchant_id')) || empty(setting('apps.paytm_merchant_key')))
                return response()->json(['status' => false, 'url' => $url, 'message' => $msg]);

            return $this->paytm($order, $currency);
        }
    }

    /*==========================Stripe Validation ====================*/

    /* ================ Stripe Integration ================ */

    protected function stripe($order = [], $currency = 'USD', $payment_data = [])
    {
        $customer_email = $payment_data['customer_email'];
        $event_title = $payment_data['event_title'];
        $flag = [];

        try {
            //current user

            $user = \Auth::user();

            if (!empty(Auth::user()->is_manager)) {
                $user = User::find(Auth::user()->organizer_id);
            }

            // create customer
            if (empty($user->stripe_id)) {
                $user->createAsStripeCustomer();
            }

            // extra params and it is optional
            $extra_params = [
                "currency" => $currency,
                "description" => $event_title,
            ];

            // payment method
            $paymentMethod = $payment_data['setupIntent'];

            // add payment method
            $user->addPaymentMethod($paymentMethod);

            // payment
            // amount
            $amount = $order['price'] * 100;
            $amount = (int) $amount;
            $stripe = $user->charge($amount, $paymentMethod, $extra_params);

            if ($stripe->status == 'succeeded') {
                // set data
                if ($stripe->charges['data'][0]->paid) {
                    $flag['status'] = true;
                    $flag['transaction_id'] = $stripe->charges['data'][0]->balance_transaction; // transation_id
                    $flag['payer_reference'] = $stripe->charges['data'][0]->id;                  // charge_id
                    $flag['message'] = $stripe->charges['data'][0]->outcome['seller_message']; // outcome message
                } else {
                    $flag['status'] = false;
                    $flag['error'] = $stripe->charges['data'][0]->failure_message;
                }
            } else {
                $flag = [
                    'status' => false,
                    'error' => $stripe->status,
                ];
            }
        }

        // Laravel Cashier Incomplete Exception Handling for 3D Secure / SCA -> 4000000000003220 error card number
        catch (IncompletePayment $ex) {

            $redirect_url = route(
                'cashier.payment',
                [$ex->payment->id, 'redirect' => route('after3DAuthentication', ['id' => $ex->payment->id])]
            );

            return response()->json(['url' => $redirect_url, 'status' => true]);
        }

        // All Exception Handling like error card number
        catch (\Exception $ex) {
            // fail case
            $flag = [
                'status' => false,
                'error' => $ex->getMessage(),
            ];
        }

        return $this->finish_checkout($flag);
    }

    protected function stripe_validation(Request $request)
    {
        // stripe
        if ((int) $request->payment_method == 2) {
            $request->validate([
                'setupIntent' => 'required',
            ]);
        }
    }

    // after redirect after3DAuthentication

    public function after3DAuthentication($paymentIntent = null)
    {
        session(['authentication_3d' => 1]);

        $user = \Auth::user();
        $flag = [];

        try {
            $stripe = \Stripe\PaymentIntent::retrieve($paymentIntent, [
                'api_key' => setting('apps.stripe_secret_key'),
            ]);

            // successs
            if ($stripe->status == 'succeeded') {

                // set data
                if ($stripe->charges['data'][0]->paid) {
                    $flag['status'] = true;
                    $flag['transaction_id'] = $stripe->charges['data'][0]->balance_transaction; // transation_id
                    $flag['payer_reference'] = $stripe->charges['data'][0]->id;                  // charge_id
                    $flag['message'] = $stripe->charges['data'][0]->outcome['seller_message']; // outcome message
                } else {
                    $flag['status'] = false;
                    $flag['error'] = $stripe->charges['data'][0]->failure_message;
                }
            } else {
                $flag = [
                    'status' => false,
                    'error' => $stripe->status,
                ];
            }
        }

        // All Exception Handling like error card number
        catch (\Exception $ex) {

            // fail case
            $flag = [
                'status' => false,
                'error' => $ex->getMessage(),
            ];
        }
        return $this->finish_checkout($flag);
    }

    /**
     *  event admin commission
     */
    protected function e_admin_commission($event_id = null, $admin_commission = null)
    {
        $event = Event::select('e_admin_commission')->where(['id' => $event_id])->first();

        if (!is_null($event->e_admin_commission) && is_numeric($event->e_admin_commission))
            $admin_commission = $event->e_admin_commission;

        return $admin_commission;
    }

    /*===================== Apply Promocode ==============================*/

    protected function apply_promocode(Request $request, $booking = [])
    {
        $net_total_price = 0;

        // count total price
        foreach ($booking as $key => $val) {
            $net_total_price += $val['net_price'];
        }

        $tickets = $request->ticket_id;
        $tickets_quantity = $request->quantity;
        $promocodes = $request->promocode;

        if ($net_total_price > 0) {
            foreach ($tickets as $key => $value) {
                if ($value && $tickets_quantity[$key] > 0 && $promocodes[$key]) {
                    // check promocode
                    try {

                        $check_promocode = Promocode::where(['code' => $promocodes[$key]])->where('quantity', '>', 0)->firstOrFail();

                        if (empty($check_promocode))
                            continue;

                        $params = [
                            'ticket_id' => $value,
                        ];
                        $this->promocode = new Promocode;

                        // get ticket's promocode's ids
                        $ticket_promocodes_ids = $this->promocode->get_ticket_promocodes_ids($params);

                        if (empty($ticket_promocodes_ids))
                            continue;

                        $promocodes_ids = [];

                        foreach ($ticket_promocodes_ids as $key1 => $value1) {
                            $promocodes_ids[] = $value1->promocode_id;
                        }

                        $params = [
                            'promocodes_ids' => $promocodes_ids,
                        ];

                        // get tikcet's promocodes
                        $ticket_promocodes = $this->promocode->get_ticket_promocodes($params);

                        if (empty($ticket_promocodes))
                            continue;

                        $promocode_match = false;

                        // match user promocode with particular tickets's promocodes
                        foreach ($ticket_promocodes as $key2 => $value2) {
                            if ($value2['code'] == $promocodes[$key]) {
                                $promocode_match = true;
                                break;
                            }
                        }

                        if ($promocode_match) {
                            // apply promocode
                            $user_promocode = [];

                            // manual check in promocode_user if promocode not already applied by the user

                            $params = [
                                'user_id' => $booking[key($booking)]['customer_id'],
                                'promocode_id' => $check_promocode->id,
                                'ticket_id' => $value
                            ];

                            $user_promocode = $this->promocode->promocode_user($params);

                            if (empty($user_promocode)) {
                                if (!empty($booking)) {
                                    $booking_collection = collect($booking)->groupBy('ticket_id');

                                    foreach ($booking as $key3 => $value3) {

                                        if ((int) $value == (int) $value3['ticket_id'] && (int) $value3['net_price'] > 0) {
                                            if ($check_promocode->p_type == 'fixed') {
                                                $promocode_reward = (float) ($check_promocode->reward / $booking_collection[$value3['ticket_id']]->count());

                                                $booking[$key3]['net_price'] = (float) $value3['net_price'] - $promocode_reward;

                                                $booking[$key3]['promocode_reward'] = $promocode_reward;
                                            } else {
                                                $promocode_reward = (float) (($booking[$key3]['net_price'] * $check_promocode->reward) / 100);

                                                $booking[$key3]['net_price'] = (float) ($booking[$key3]['net_price'] - $promocode_reward);

                                                $booking[$key3]['promocode_reward'] = $promocode_reward;
                                            }

                                            $booking[$key3]['promocode_id'] = $check_promocode->id;
                                            $booking[$key3]['promocode'] = $check_promocode->code;

                                            // store valid promocode
                                            $this->valid_promocodes[$key3]['promocode_id'] = (int) $check_promocode->id;
                                            $this->valid_promocodes[$key3]['user_id'] = (int) $booking[$key3]['customer_id'];
                                            $this->valid_promocodes[$key3]['ticket_id'] = (int) $value3['ticket_id'];
                                        }
                                    }
                                }
                            }
                        }
                    } catch (\Throwable $e) {
                    }
                }
            }
        }

        if (!empty($this->valid_promocodes)) {
            session(['valid_promocodes' => $this->valid_promocodes]);
        }

        return $booking;
    }

    /*=================== Check Promocode then Apply =======================*/

    protected function check_promocode($promocodes)
    {
        if (!is_null($promocodes)) {
            foreach ($promocodes as $key => $value) {
                $this->promocode = new Promocode;
                $this->promocode->promocode_apply($value);
            }
        }
    }

    /**
     *  set event currecny
     */

    protected function setEventCurrency(Request $request)
    {
        $this->currency = setting('regional.currency_default');

        // get event by event_id
        $event = $this->event->get_event(null, $request->event_id);

        // if event not found then access denied
        if (empty($event))
            return response()->json(['status' => false, 'error' => __('eventmie-pro::em.event') . ' ' . __('eventmie-pro::em.not_found')], 400);

        if (!empty($event->currency))
            $this->currency = $event->currency;
    }

    /**
     *  authorize net payment
     */
    protected function authorizeNetPayment($order = [], $currency = 'USD')
    {
        $flag = [];
        try {
            $authrizeNet = Omnipay::create('AuthorizeNetApi_Api');
            $authrizeNet->setAuthName(setting('apps.authorize_login_id'));
            $authrizeNet->setTransactionKey(setting('apps.authorize_transaction_key'));
            $authrizeNet->setTestMode((int) setting('apps.authorize_test_mode')); //comment this line when move to 'live'

            $creditCard = new \Omnipay\Common\CreditCard([
                'number' => session('payment_method')['cardNumber'],
                'expiryMonth' => session('payment_method')['cardMonth'],
                'expiryYear' => session('payment_method')['cardYear'],
                'cvc' => session('payment_method')['cvc'],
                'firstName' => session('payment_method')['firstName'],
                'lastName' => session('payment_method')['lastName'],
                'email' => session('payment_method')['customer_email']
            ]);

            // Generate a unique merchant site transaction ID.
            $transactionId = rand(100000000, 999999999);

            $response = $authrizeNet->authorize([
                'amount' => $order['price'],
                'currency' => $currency,
                'transactionId' => $transactionId,
                'card' => $creditCard,
            ])->send();

            if ($response->isSuccessful()) {

                // Captured from the authorization response.
                $transactionReference = $response->getTransactionReference();

                $response = $authrizeNet->capture([
                    'amount' => $order['price'],
                    'currency' => $currency,
                    'transactionReference' => $transactionReference,
                ])->send();

                $transaction_id = $response->getTransactionReference();

                $flag['status'] = true;
                $flag['transaction_id'] = $transaction_id; // transation_id
                $flag['payer_reference'] = session('payment_method')['customer_email'];
                $flag['message'] = 'Captured';
            } else {

                // not successful
                $flag = [
                    'status' => false,
                    'error' => $response->getMessage(),
                ];
            }
        } catch (\Exception $e) {

            $flag = [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }

        return $this->finish_checkout($flag);
    }

    /**
     *  authorize net validation
     */
    protected function authorizeNetValidation(Request $request)
    {

        if ((int) $request->payment_method == 3) {
            $request->validate([
                'cardNumber' => 'required',
                'cardMonth' => 'required',
                'cardYear' => 'required',
                'cardCvv' => 'required',
                'cardName' => 'required|max:256',
            ]);
        }
    }

    // only customers can book tickets so check login user customer or not but admin and organisers can book tickets for customer
    protected function is_admin_organiser(Request $request)
    {
        if (Auth::check()) {
            // get event by event_id
            $event = $this->event->get_event(null, $request->event_id);

            // if event not found then access denied
            if (empty($event))
                return response()->json(['status' => false, 'error' => __('eventmie-pro::em.event') . ' ' . __('eventmie-pro::em.not_found')], 400);

            // organiser can't book other organiser event's tikcets but  admin can book any organiser events'tikcets for customer
            // if(Auth::user()->hasRole('organiser') )
            //CUSTOM
            if (Auth::user()->hasRole('organiser') && !Auth::user()->hasRole('manager')) {
                //CUSTOM
                if (Auth::id() != $event->user_id)
                    return false;
            }

            if (Auth::user()->hasRole('manager')) {
                if (Auth::user()->organizer_id != $event->user_id)
                    return false;
            }

            /* CUSTOM */
            // Assign organizer permissions to POS
            if (Auth::user()->hasRole('pos')) {
                if (Auth::user()->organizer_id != $event->user_id)
                    return false;

                $pos_events = $this->event->get_pos_event_ids()->all();
                if (!in_array($event->id, $pos_events)) {
                    return false;
                }
            }
            /* CUSTOM */

            //organiser_id
            $this->organiser_id = $event->user_id;

            // if login user is customer then
            // customer id = Auth::id();
            $this->customer_id = Auth::id();

            // for complimentary booking
            if (Auth::user()->hasRole('admin') && !empty($request->is_bulk)) {
                $this->customer_id = 1;
            }

            // if admin and organiser is creating booking
            // then user Auth::id() as $customer_id
            // and customer id will be the id selected from Vue dropdown
            if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('organiser') || Auth::user()->hasRole('pos')) {
                if (empty($request->is_bulk)) {
                    // 1. validate data
                    $request->validate([
                        'customer_id' => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
                    ], [
                        'customer_id.*' => __('eventmie-pro::em.customer') . ' ' . __('eventmie-pro::em.required'),
                    ]);
                    $this->customer_id = $request->customer_id;
                }
            }

            if (Auth::user()->hasRole('scanner')) {
                return false;
            }

            return true;
        }
    }

    /**
     *  attendees validations
     */

    protected function attendeesValidations(Request $request)
    {
        // dd($request->all());
        $attedees = [
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
        ];

        $rules = [
            'name' => ['required', 'array'],
            'name.*' => ['max:255'],
            'phone' => ['required', 'array'],
            'phone.*' => ['max:255'],
            'address' => ['required', 'array'],
            'address.*' => ['max:255'],
        ];

        Validator::make($attedees, $rules)->validate();

        return $attedees;
    }

    /**
     * create attendee
     */

    protected function save_attendee($booking_data = [], $seats, $selected_attendees)
    {

        // dd($seats, $selected_attendees);

        if (empty($selected_attendees)) {
            return true;
        }

        $selected_attendees1 = [];

        // refresh index
        foreach ($selected_attendees as $key => $value) {
            $selected_attendees1[] = $value;
        }

        $count = 0;

        foreach ($selected_attendees as $key => $value) {
            // dd($value['name']);
            $attedees = [];

            foreach ($value['name'] as $name_k => $name_v) {
                $attedees[$name_k]['name'] = $name_v;
                $attedees[$name_k]['phone'] = $value['phone'][$name_k];
                $attedees[$name_k]['address'] = $value['address'][$name_k];
                $attedees[$name_k]['ticket_id'] = $booking_data[$count]->ticket_id;
                $attedees[$name_k]['event_id'] = $booking_data[$count]->event_id;
                $attedees[$name_k]['booking_id'] = $booking_data[$count]->id;
                $attedees[$name_k]['created_at'] = Carbon::now()->toDateTimeString();
                $attedees[$name_k]['updated_at'] = Carbon::now()->toDateTimeString();

                $attedees[$name_k]['seat_name'] = empty($seats['seat_id_' . $booking_data[$count]->ticket_id][$name_k]['name']) ? null : $seats['seat_id_' . $booking_data[$count]->ticket_id][$name_k]['name'];

                $attedees[$name_k]['seat_id'] = empty($seats['seat_id_' . $booking_data[$count]->ticket_id][$name_k]['id']) ? null : $seats['seat_id_' . $booking_data[$count]->ticket_id][$name_k]['id'];

                $count++;
            }

            // save attendees

            \App\Models\Attendee::insert($attedees);
        }
    }

    public function send_email1($booking_data)
    {
        // dd($booking_data);
        if (empty($booking_data))
            return response()
                ->json(['status' => 0]);


        // get online event info
        $event = $this->event->get_event(NULL, $booking_data[key($booking_data)]['event_id']);
        $mail['is_online'] = FALSE;
        if (!empty($event->online_location))
            $mail['is_online'] = TRUE;
        // dd(true);

        $transaction = null;
        if (!is_null($booking_data[0]['transaction_id']) && $booking_data[0]['is_paid'] == 0) {
            $transaction = Transaction::find($booking_data[0]['transaction_id']);
        }

        // ====================== Notification ======================
        //send notification after bookings
        $mail['mail_data'] = $booking_data;
        $mail['transaction'] = $transaction;
        $mail['event'] = $event;
        $mail['action_title'] = __('eventmie-pro::em.download_tickets');
        $mail['action_url'] = route('eventmie.mybookings_index');
        $mail['mail_subject'] = __('eventmie-pro::em.booking_success');
        $mail['n_type'] = "bookings";

        $mail['event_start_date'] = userTimezone($mail['mail_data'][0]['event_start_date'] . ' ' . $mail['mail_data'][0]['event_start_time'], 'Y-m-d H:i:s', format_carbon_date(true)) . ' - ' . userTimezone($mail['mail_data'][0]['event_end_date'] . ' ' . $mail['mail_data'][0]['event_end_time'], 'Y-m-d H:i:s', format_carbon_date(true)) . showTimezone();

        $mail['event_end_date'] = userTimezone($mail['mail_data'][0]['event_start_date'] . ' ' . $mail['mail_data'][0]['event_start_time'], 'Y-m-d H:i:s', format_carbon_date(false)) . ' -' . userTimezone($mail['mail_data'][0]['event_end_date'] . ' ' . $mail['mail_data'][0]['event_end_time'], 'Y-m-d H:i:s', format_carbon_date(false)) . showTimezone();

        $mail['tickets'] = View::make('vendor.eventmie-pro.mail.custom.tickets', ['mail_data' => (object) $mail])->render();


        $invoice = new InvoicesController($booking_data);
        // $mail['invoices'] = $invoice->makeInvoice();

        $notification_ids = [1, $booking_data[key($booking_data)]['organiser_id'], $booking_data[key($booking_data)]['customer_id']];

        $users = User::whereIn('id', $notification_ids)->get();
        try {

            //CUSTOM
            Mail::to($users)->send(new BookingMail($mail));

            Notification::locale(App::getLocale())->send($users, new BookingNotification($mail));

            // send SMS to customers only
            $notification_ids = [$booking_data[key($booking_data)]['customer_id']];
            $users = User::whereIn('id', $notification_ids)->whereNotNull('phone')->get();
            $sms = new SmsNotificationController;
            $sms->smsNotification($users->pluck('phone'), $booking_data, $mail['invoices']);
        } catch (\Throwable $th) {

            $flag = [
                'data' => [
                    'message' => $th->getMessage(),
                    'file' => $th->getFile(),
                    'line' => $th->getLine(),
                    'code' => $th->getCode(),
                ]
            ];
            // dd($th->getMessage());
            // dd($flag);
        }
        // ====================== Notification ======================

        // delete booking_email_data data from session

        return response()
            ->json(['status' => 1]);
    }

    public function send_email($booking_data)
    {

        /* CUSTOM */

        // mailchimp notificaton
        // $this->mailchimp_notification($booking_data);
        /* CUSTOM */

        if (empty($booking_data))
            return response()
                ->json(['status' => 0]);

        // get online event info
        $event = $this->event->get_event(NULL, $booking_data[key($booking_data)]['event_id']);
        $mail['is_online'] = FALSE;
        if (!empty($event->online_location))
            $mail['is_online'] = TRUE;

        $transaction = null;
        if (!is_null($booking_data[0]['transaction_id']) && $booking_data[0]['is_paid'] == 0) {
            $transaction = Transaction::find($booking_data[0]['transaction_id']);
        }


        // ====================== Notification ======================
        //send notification after bookings
        $mail['mail_data'] = $booking_data;
        $mail['transaction'] = $transaction;
        $mail['event'] = $event;
        $mail['client_logo'] = Storage::disk('azure')->temporaryUrl($event->invoice_logo, now()->addMinutes(5)) ?? null;
        $mail['action_title'] = __('eventmie-pro::em.download_tickets');
        $mail['action_url'] = route('eventmie.mybookings_index');
        $mail['mail_subject'] = __('eventmie-pro::em.booking_success');
        $mail['n_type'] = "bookings";


        $mail['event_start_date'] = Carbon::parse($mail['mail_data'][0]['event_start_date'] . ' ' . $mail['mail_data'][0]['event_start_time'])->format('Y-m-d') . ' - ' . Carbon::parse($mail['mail_data'][0]['event_end_date'] . ' ' . $mail['mail_data'][0]['event_end_time'])->format('Y-m-d') . ' GMT';

        $mail['event_end_date'] = Carbon::parse($mail['mail_data'][0]['event_start_date'] . ' ' . $mail['mail_data'][0]['event_start_time'])->format('H:i A') . ' -' . Carbon::parse($mail['mail_data'][0]['event_end_date'] . ' ' . $mail['mail_data'][0]['event_end_time'])->format('H:i A') . ' GMT';

        $mail['tickets'] = View::make('vendor.eventmie-pro.mail.custom.tickets', ['mail_data' => (object) $mail])->render();
        //CUSTOM


        $invoice = new InvoicesController($booking_data);
        $mail['invoices'] = $invoice->makeInvoice();


        //CUSTOM
        $notification_ids = [1, $booking_data[key($booking_data)]['organiser_id'], $booking_data[key($booking_data)]['customer_id']];

        $users = User::whereIn('id', $notification_ids)->get();

        try {
            //CUSTOM
            Mail::to($users)->send(new BookingMail($mail));

            //CUSTOM
            Notification::locale(App::getLocale())->send($users, new BookingNotification($mail));

            // send SMS to customers only
            $notification_ids = [$booking_data[key($booking_data)]['customer_id']];
            $users = User::whereIn('id', $notification_ids)->whereNotNull('phone')->get();
            $sms = new SmsNotificationController;
            $sms->smsNotification($users->pluck('phone'), $booking_data, $mail['invoices']);
        } catch (\Throwable $th) {
            // dd($th->getMessage());
            $flag = [
                'data' => [
                    'message' => $th->getMessage(),
                    'file' => $th->getFile(),
                    'line' => $th->getLine(),
                    'code' => $th->getCode(),
                ]
            ];
            // dd($th->getMessage());
            // dd($flag);
        }
        // ====================== Notification ======================

        // delete booking_email_data data from session

        return response()
            ->json(['status' => 1]);
    }

    public function customer_limit($ticket = null, $selected_ticket = null, $booking_date = null)
    {
        $booked_tickets = Booking::where(['customer_id' => $this->customer_id, 'ticket_id' => $ticket->id, 'event_start_date' => $booking_date])->sum('quantity');

        $ticket = Ticket::where(['id' => $ticket->id])->first();

        if (!empty($ticket->customer_limit)) {
            // check existing booked_ticket agains customer_limit
            $msg = __('eventmie-pro::em.already_booked');
            if ($booked_tickets >= $ticket->customer_limit) {
                return ['status' => false, 'error' => $ticket->title . ':-' . $msg];
            }

            // check selected quantity against remaining customer_limit
            // $ticket->customer_limit - $booked_tickets = remaining customer limit
            if ($selected_ticket['quantity'] > ($ticket->customer_limit - $booked_tickets)) {
                return ['status' => false, 'error' => $ticket->title . ':-' . $msg];
            }
        }
    }

    protected function time_validation($params = [])
    {
        $booking_date = $params['booking_date'];
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];

        // booking date is event start date and it is less then today date then user can't book tickets
        $end_date = Carbon::parse($booking_date . '' . $end_time);

        // for repetitive event
        if ($params['event']['repetitive'] && empty(request()->booking_end_date))
            $end_date = Carbon::parse($booking_date . '' . $end_time);

        // for single event
        if (!$params['event']['repetitive'])
            $end_date = Carbon::parse($params['event']['end_date'] . '' . $end_time);

        // for merge event
        if (!empty(request()->booking_end_date))
            $end_date = Carbon::parse(request()->booking_end_date . '' . $end_time);

        $today_date = Carbon::parse(Carbon::now(setting('regional.timezone_default')));

        // 1.Booking date should not be less than today's date
        if ($end_date < $today_date)
            return ['status' => false, 'error' => __('eventmie-pro::em.event') . ' ' . __('eventmie-pro::em.ended')];

        // 2. Check prebooking time from settings (in hour)
        $default_prebook_time = (float) setting('booking.pre_booking_time');

        $min = number_format((float) ($end_date->diffInMinutes($today_date)), 2, '.', '');

        $hours = (float) sprintf("%d.%02d", floor($min / 60), $min % 60);

        if ($hours < $default_prebook_time)
            return ['status' => false, 'error' => __('eventmie-pro::em.bookings_over')];

        return ['status' => true];
    }

    protected function admin_tax($tickets = [])
    {
        // get admin taxes
        $admin_tax = $this->tax->get_admin_taxes();

        // if admin taxes are not existed then return
        if ($admin_tax->isEmpty())
            return $tickets;

        // it work when tickets show for purchasing
        // for multiple tickets
        if ($tickets instanceof \Illuminate\Database\Eloquent\Collection) {
            // push admin taxes in every tickets
            foreach ($tickets as $key => $value) {
                foreach ($admin_tax as $ad_k => $ad_v) {
                    $value->taxes->push($ad_v);
                }
            }
        } else {
            // it work when booking data prepare
            // for single ticket
            foreach ($admin_tax as $ad_k => $ad_v) {
                $tickets['taxes'] = $tickets['taxes']->push($ad_v);
            }
        }

        return $tickets;
    }

    // ---------------------------------------------------------------------//
    public function book_tickets(Request $request)
    {
        if (($request->event_promocode && $request->promocode[0])) {

            return response()->json(['status' => false, 'error' => __('eventmie-pro::em.apply_voucher_promocode')], Response::HTTP_BAD_REQUEST);
        }

        $inputData = $request->all();
        $address_type = $request['address_type'] ?? 1;
        $event_promocode = $request['event_promocode'] ?? null;
        //CUSTOM
        $this->setEventCurrency($request);
        //CUSTOM

        // check login user role
        $status = $this->is_admin_organiser($request);

        // organiser can't book other organiser event's tikcets but  admin can book any organiser events'tikcets for customer
        if (!$status) {

            return response()->json([
                'status' => false,
                'message' => __('eventmie-pro::em.organiser_note_5'),
            ], 200);
        }

        // 1. General validation and get selected ticket and event id
        $data = $this->general_validation($request);
        if (!$data['status']) {
            return response()->json(['error' => $data['error']], 400);
        }

        // 2. Check availability
        $check_availability = $this->availability_validation($data);
        if (!$check_availability['status'])
            return response()->json(['error' => $check_availability['error']], 400);

        // 3. TIMING & DATE CHECK
        $pre_time_booking = $this->time_validation($data);
        if (!$pre_time_booking['status'])
            return response()->json(['error' => $pre_time_booking['error']], 400);

        $selected_tickets = $data['selected_tickets'];
        $tickets = $data['tickets'];
        $selected_attendees = $data['selected_attendees'];
        $seats = $data['seats'];

        // dd($seats, $selected_attendees);

        $booking_date = $request->booking_date;

        $params = [
            'customer_id' => $this->customer_id,
        ];
        // get customer information by customer id
        $customer = $this->user->get_customer($params);

        //CUSTOM
        if (!empty($request->is_bulk)) {
            $customer = $data['customer'];
        }
        //CUSTOM

        if (empty($customer))
            return response()->json(['data' => $pre_time_booking['error']], 400);

        //CUSTOM

        if (!empty(setting('apps.twilio_sid')) && !empty(setting('apps.twilio_auth_token')) && !empty(setting('apps.twilio_number')) && empty($customer->phone)) {
            $request->validate([
                'phone_t' => 'nullable',
            ]);

            User::where(['id' => $this->customer_id])->update(['phone' => $request->phone_t]);
        }

        //CUSTOM

        $booking = [];
        $price = 0;
        $total_price = 0;

        // organiser_price excluding admin_tax
        $booking_organiser_price = [];
        $admin_tax = [];
        $common_order = time() . rand(1, 988);
        $coupon_data = [];

        $delivery_charges = 0;

        if (!empty($request->delivery_charge_id)) {
            $delivery_charges = $this->delivery_charges($data['delivery_charge_id']) ?? '';
        }
        $delivery_address = $request->delivery_address ?? '';

        $collection_point = '';
        if ($request->address_type == 2) {
            $collection_point = $request->collection_point ?? '';
        }

        if ($request->address_type == 1 || $request->address_type == 3) {
            $delivery_address = $request->delivery_address ?? '';
        }

        $count = 0;

        foreach ($selected_tickets as $key => $value) {
            $count += $value['quantity'];
        }

        $voucher_reward = $data['voucher_reward'] / $count;

        $delivery_charge = $delivery_charges / $count;

        foreach ($selected_tickets as $key => $value) {
            $key = count($booking) == 0 ? 0 : count($booking);
            $voucher_qty = 0;
            $agent_id = null;
            if ($request->voucher_code && @$value['voucher_data'] && $value['voucher_data']['qty']) {
                $voucher_qty = $value['voucher_data']['qty'];
                $agent_id = $value['voucher_data']['agent_id'];
            }
            for ($i = 1; $i <= $value['quantity']; $i++) {
                $booking[$key]['customer_id'] = $this->customer_id;
                $booking[$key]['customer_name'] = $customer['name'];
                $booking[$key]['customer_email'] = $customer['email'];
                $booking[$key]['organiser_id'] = $this->organiser_id;
                $booking[$key]['event_id'] = $request->event_id;
                $booking[$key]['ticket_id'] = $value['ticket_id'];
                $booking[$key]['delivery_charge'] = $delivery_charge;
                $booking[$key]['quantity'] = 1;
                $booking[$key]['status'] = 1;
                $booking[$key]['created_at'] = Carbon::now();
                $booking[$key]['updated_at'] = Carbon::now();
                $booking[$key]['event_title'] = $data['event']['title'];
                $booking[$key]['event_category'] = $data['event']['category_name'];
                $booking[$key]['ticket_title'] = $value['ticket_title'];
                $booking[$key]['item_sku'] = $data['event']['item_sku'];
                // $booking[$key]['currency']          = setting('regional.currency_default');
                //CUSTOM
                $booking[$key]['currency'] = $this->currency;
                $booking[$key]['wallet'] = $request->input('wallet') ?? NULL;
                $booking[$key]['is_bulk'] = !empty($request->is_bulk) ? $request->is_bulk : 0;
                //CUSTOM
                $booking[$key]['event_repetitive'] = $data['event']->repetitive > 0 ? 1 : 0;

                // non-repetitive
                $booking[$key]['event_start_date'] = $data['event']->start_date;
                $booking[$key]['event_end_date'] = $data['event']->end_date;
                $booking[$key]['event_start_time'] = $data['event']->start_time;
                $booking[$key]['event_end_time'] = $data['event']->end_time;
                $booking[$key]['common_order'] = $common_order;
                $booking[$key]['meta'] = $data['finalFamilyTotal'] ?? '';
                if ($voucher_qty) {
                    $voucher_qty--;
                    $booking[$key]['promocode'] = $request->voucher_code;
                    // if($check_promocode[$key3]['agent_ticket']) {
                    $booking[$key]['agent_id'] = (int) $agent_id;
                    $this->valid_vouchercodes[$key]['agent_ticket_id'] = (int) $agent_id;
                    $this->valid_vouchercodes[$key]['user_id'] = (int) $this->customer_id;
                    $this->valid_vouchercodes[$key]['ticket_id'] = (int) $value['ticket_id'];
                    $this->valid_vouchercodes[$key]['sub_category_id'] = @$value['sub_category_id'] ?? null;
                }
                $booking[$key]['promocode_reward'] = $voucher_reward;
                // repetitive event
                if ($data['event']->repetitive) {
                    $booking[$key]['event_start_date'] = $booking_date;
                    $booking[$key]['event_end_date'] = $request->merge_schedule ? $request->booking_end_date : $booking_date;
                    $booking[$key]['event_start_time'] = $request->start_time;
                    $booking[$key]['event_end_time'] = $request->end_time;
                }


                //CUSTOM
                //set sale price
                if ($tickets->isNotEmpty()) {
                    $tickets = $tickets->map(function ($ticket, $key) {

                        if (!empty($ticket->sale_start_date)) {
                            if ($ticket->sale_price > 0 && $ticket->sale_start_date <= Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString() && $ticket->sale_end_date > Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString()) {
                                $ticket->price = $ticket->sale_price;
                            }
                        }
                        return $ticket;
                    });
                }

                //CUSTOM

                foreach ($tickets as $k => $v) {
                    if ($v['id'] == $value['ticket_id']) {
                        $price = $v['price'];

                        //CUSTOM
                        if (!empty($v['is_donation']))
                            $price = round($value['is_donation'] / $value['quantity']);
                        //CUSTOM

                        break;
                    }
                }
                $booking[$key]['price'] = $price * 1;

                // CUSTOM
                if (!empty($value['is_donation']))
                    $booking[$key]['price'] = $price * 1;
                // CUSTOM

                $booking[$key]['ticket_price'] = $price;

                // call calculate price
                $params = [
                    'ticket_id' => $value['ticket_id'],
                    'quantity' => 1,
                    //CUSTOM
                    'is_donation' => $price,
                    //CUSTOM
                ];

                // calculating net price
                $net_price = $this->calculate_price($params);
                if ($request->voucher_code && $voucher_reward > 0) {
                    $net_price['net_price'] -= $voucher_reward;
                }

                $booking[$key]['tax'] = number_format((float) ($net_price['tax']), 2, '.', '');
                $booking[$key]['net_price'] = number_format((float) ($net_price['net_price']), 2, '.', '') + $delivery_charge;

                // organiser price excluding admin_tax
                $booking_organiser_price[$key]['organiser_price'] = number_format((float) ($net_price['organiser_price']), 2, '.', '');

                //  admin_tax
                $admin_tax[$key]['admin_tax'] = number_format((float) ($net_price['admin_tax']), 2, '.', '');

                // if payment method is offline then is_paid will be 0
                if ($request->payment_method == 'offline') {
                    // except free ticket
                    if (((int) $booking[$key]['net_price']))
                        $booking[$key]['is_paid'] = 0;

                    //CUSTOM
                    if (!empty($request->is_bulk)) {
                        $booking[$key]['is_paid'] = 1;
                    }
                    //CUSTOM
                } else {
                    $booking[$key]['is_paid'] = 1;
                }

                //CUSTOM
                if (Auth::user()->hasRole('pos'))
                    $booking[$key]['pos_id'] = Auth::id();
                //CUSTOM

                $key++;
            }
        }

        if (!empty($this->valid_vouchercodes)) {
            session(['valid_vouchercodes' => $this->valid_vouchercodes]);
        }
        /* CUSTOM */
        // $booking = $this->apply_promocode($request, $booking);

        if (!empty($request->event_promocode)) {
            $booking = $this->apply_event_promocode($request, $booking);
        }        /* CUSTOM */

        // calculate commission
        $commission = $this->calculate_commission(
            $booking,
            $booking_organiser_price,
            $admin_tax
        );

        // if net price total == 0 then no paypal process only insert data into booking
        foreach ($booking as $k => $v) {
            $total_price += (float) $v['net_price'];
            $total_price = number_format((float) ($total_price), 2, '.', '');
        }

        $is_direct_checkout = $this->checkDirectCheckout($request, $total_price);

        // IF FREE EVENT THEN ONLY INSERT DATA INTO BOOKING TABLE
        // AND DON'T INSERT DATA INTO TRANSACTION TABLE
        // AND DON'T CALLING PAYPAL API
        // if ($is_direct_checkout) {
        //     $data = [
        //         'order_number' => time() . rand(1, 988),
        //         'transaction_id' => 0
        //     ];

        //     //CUSTOM
        //     $data['bulk_code'] = null;
        //     if (!empty($request->is_bulk)) {
        //         $data['bulk_code']         = time() . rand(1, 988);
        //     }
        //     //CUSTOM

        //     // $flag =  $this->finish_booking($booking, $data);
        //     $flag =  $this->finish_booking($booking, $data, $seats, $selected_attendees, $commission);
        //     $order = $this->create_order($flag, $request->event_promocode, $delivery_address, $request->address_type, $collection_point);


        //     // in case of database failure
        //     if (empty($flag)) {
        //         return response()->json(["Database Failure"], 400);
        //     }

        //     // redirect no matter what so that it never turns back return response
        //     $msg = __('eventmie-pro::em.booking_success');

        //     return response()->json(['status'    => true, 'message'   => $msg, 'order' => $order], Response::HTTP_OK);
        // }

        if ($request->input('payment_method') == 2) {

            $gross_total = $total_price + $delivery_charges;

            if (empty(setting('apps.stripe_secret_key'))) {
                return response()->json(['message' => 'Payment method is not activated'], 400);
            }
            // Set your secret key
            Stripe::setApiKey(setting('apps.stripe_secret_key'));

            // Get the amount and currency from the request
            // dd($amount = (int)$total_price);
            $amount = (empty($request->amount)) ? $gross_total * 100 : $request->amount * 100;
            $currency = $this->currency;

            $stripe = new \Stripe\StripeClient(setting('apps.stripe_secret_key'));
            // Use an existing Customer ID if this is a returning customer.
            $customer = $stripe->customers->create();
            $ephemeralKey = $stripe->ephemeralKeys->create([
                'customer' => $customer->id,
            ], [
                'stripe_version' => '2022-08-01',
            ]);
            $paymentIntent = $stripe->paymentIntents->create([
                'amount' => $amount,
                'currency' => $currency,
                'customer' => $customer->id,
                'automatic_payment_methods' => [
                    'enabled' => 'true',
                ],
            ]);

            $stripeData = [
                'ephemeralKey' => $ephemeralKey->secret,
                'customer' => $customer->id,
                "paymentIntent" => $paymentIntent->client_secret,
                "amount" => $paymentIntent->amount,
                "stripe_publishable_key" => setting('apps.stripe_public_key'),
                "stripe_intent_data" => $paymentIntent
            ];

            $responseData = [
                'stripeData' => $stripeData,
                'booking' => $booking,
                'customerData' => $data,
                'seats' => $seats,
                'selected_attendees' => $selected_attendees,
                'commission' => $commission,
                'delivery_address' => $delivery_address
            ];

            return response()->json(['message' => 'Please complete the payment', 'data' => $responseData], 200);
        }

        if ($request->input('payment_method') == 9) {

            $currency = !empty($booking[key($booking)]['currency']) ? $booking[key($booking)]['currency'] : setting('regional.currency_default');

            // add all info into session
            $order = [
                'item_sku' => $booking[key($booking)]['item_sku'],
                'order_number' => time() . rand(1, 988),
                'product_title' => $booking[key($booking)]['event_title'],

                'price_title' => '',
                'price_tagline' => '',
                'wallet' => @$booking[key($booking)]['wallet'],
                'currency' => @$booking[key($booking)]['currency']
            ];

            $total_price = 0;
            foreach ($booking as $key => $val) {
                $order['price_title'] .= ' | ' . $val['ticket_title'][0] . ' | ';
                $order['price_tagline'] .= ' | ' . $val['quantity'] . ' | ';
                $total_price += $val['net_price'];
            }

            // calculate total price
            $order['price'] = $total_price;

            $flag = $this->finish_booking_wallet(
                $booking,
                $data,
                $seats,
                $selected_attendees,
                $commission,
                [],
                $order,
                $currency,
                $this->service,
                $request->address_type,
                $collection_point,
                $delivery_address,
                $request->event_promocode
            );

            if (!empty($this->valid_vouchercodes)) {

                foreach ($this->valid_vouchercodes as $key => $value) {
                    $agent_tickets = new AgentTicket();
                    $agent_tickets->voucher_code_apply($value);
                }
            }

            if (($flag['status'] == false)) {
                return response()->json(
                    [
                        'message' => $flag['data']['message'],
                        'file' => $flag['data']['file'] ?? '',
                        'line' => $flag['data']['line'] ?? '',
                        'code' => $flag['data']['code'] ?? '',
                    ],
                    400
                );
            }

            // $order = $this->create_order($flag['booking'], $request->event_promocode, $delivery_address, $request->address_type, $collection_point);


            $msg = __('eventmie-pro::em.booking_success');

            return response()->json([
                'status' => true,
                'message' => $msg,
                'data' => $flag['booking'],
                'order' => $flag['order']
            ], 200);
        }

        if ($request->input('payment_method') == 11) {

            $currency = !empty($booking[key($booking)]['currency']) ? $booking[key($booking)]['currency'] : setting('regional.currency_default');

            // add all info into session
            $order = [
                'item_sku' => $booking[key($booking)]['item_sku'],
                'order_number' => time() . rand(1, 988),
                'product_title' => $booking[key($booking)]['event_title'],

                'price_title' => '',
                'price_tagline' => '',
                'wallet' => @$booking[key($booking)]['wallet'],
                'currency' => @$booking[key($booking)]['currency']
            ];

            $total_price = 0;

            foreach ($booking as $key => $val) {
                $order['price_title'] .= ' | ' . $val['ticket_title'] . ' | ';
                $order['price_tagline'] .= ' | ' . $val['quantity'] . ' | ';
                $total_price += $val['net_price'];
            }

            // calculate total price
            $order['price'] = $total_price;

            $payment_method_data = $this->set_payment_method($request, $booking);

            $gross_total = $total_price;

            $sessionData = [
                "selected_attendees" => $selected_attendees,
                "commission" => $commission,
                "booking" => $booking,
                "event_promocode" => $event_promocode,
                "delivery_address" => $delivery_address,
                "address_type" => $address_type,
                "collection_point" => $collection_point,
                "inputData" => $inputData,
                "payment_method" => $payment_method_data,
                "pre_payment" => $order,
                "user_id" => auth()->user()->id
            ];

            $sensyLog = new SensyLog();
            $sensyLog->cartId = $order['order_number'];
            $sensyLog->meta = json_encode($sessionData);
            $sensyLog->save();



            if (!empty(setting('apps.worldpay_url'))) {
                $url = setting('apps.worldpay_url');
            } else {
                $url = config('worldpay.worldpay_url');
            }

            $params = [
                'instId' => setting('apps.worldpay_instid'),
                'cartId' => $order['order_number'],
                'amount' => $gross_total,
                'testMode' => setting('apps.worldpay_mode'),
                'currency' => 'GBP',
                'M_' => 'MobileBooking',
                'resultfile' => 'resultY.html'

            ];

            // Construct the URL with query parameters
            $worldpayUrl = $url . '?' . http_build_query($params);

            return response()->json([
                'status' => true,
                'return_url' => $worldpayUrl,
                'sessionData' => $sessionData
            ], 200);
        }

        // $flag = $this->finish_booking($booking, $data, $seats, $selected_attendees,[],[], $commission,[],$request);
        // $order = $this->create_order($flag, $request->event_promocode, $delivery_address, $request->address_type, $collection_point);
        // /* CUSTOM */
        // $payment_method = $this->set_payment_method($request, $booking);
        // /* CUSTOM */

        // $this->init_checkout($booking, $payment_method);
        // in case of database failure
        if (empty($flag)) {
            return response()->json(["Database Failure"], 400);
        }

        // redirect no matter what so that it never turns backreturn response
        $msg = __('eventmie-pro::em.booking_success');

        return response()->json([
            'status' => true,
            'message' => $msg,
        ], 200);
    }

    public function preBookingValidate(Request $request)
    {
        // if (($request->event_promocode && $request->promocode[0])) {

        //     return response()->json(['status' => false, 'error' => __('eventmie-pro::em.apply_voucher_promocode')], Response::HTTP_BAD_REQUEST);
        // }

        $inputData = $request->all();

        //CUSTOM
        $this->setEventCurrency($request);
        //CUSTOM

        // check login user role
        $status = $this->is_admin_organiser($request);

        // organiser can't book other organiser event's tikcets but  admin can book any organiser events'tikcets for customer
        if (!$status) {

            return response()->json([
                'status' => false,
                'url' => route('eventmie.events_index'),
                'message' => __('eventmie-pro::em.organiser_note_5'),
            ], 200);
        }

        // 1. General validation and get selected ticket and event id
        $data = $this->general_validation($request);
        if (!$data['status']) {
            return response()->json(['data' => $data['error']], 400);
        }

        // 2. Check availability
        $check_availability = $this->availability_validation($data);
        if (!$check_availability['status'])
            return response()->json(['data' => $check_availability['error']], 400);

        // 3. TIMING & DATE CHECK
        $pre_time_booking = $this->time_validation($data);
        if (!$pre_time_booking['status'])
            return response()->json(['data' => $pre_time_booking['error']], 400);

        //CUSTOM
        //4. Validate Event Promocode
        if (!empty($request->event_promocode)) {

            $promoCodeValidation = $this->apply_event_promocodes($request);
            if (!$promoCodeValidation['status'])
                return response()->json(['data' => $promoCodeValidation['message']], 400);
        }


        $selected_tickets = $data['selected_tickets'];
        $tickets = $data['tickets'];
        $selected_attendees = $data['selected_attendees'];
        $seats = $data['seats'];

        // dd($seats, $selected_attendees);

        $booking_date = $request->booking_date;
        $user = Auth::user();
        $params = [
            'customer_id' => $user->id,
        ];
        // get customer information by customer id
        $customer = $this->user->get_customer($params);

        //CUSTOM
        if (!empty($request->is_bulk)) {
            $customer = $data['customer'];
        }
        //CUSTOM

        if (empty($customer))
            return response()->json(['data' => $pre_time_booking['error']], 400);

        //CUSTOM

        if (!empty(setting('apps.twilio_sid')) && !empty(setting('apps.twilio_auth_token')) && !empty(setting('apps.twilio_number')) && empty($customer->phone)) {
            $request->validate([
                'phone_t' => 'nullable',
            ]);

            User::where(['id' => $this->customer_id])->update(['phone' => $request->phone_t]);
        }
        //CUSTOM

        $booking = [];
        $price = 0;
        $total_price = 0;

        // organiser_price excluding admin_tax
        $booking_organiser_price = [];
        $admin_tax = [];

        $common_order = time() . rand(1, 988);

        $collection_point = '';
        $delivery_charge = 0;
        $delivery_charges = 0;
        $count = 0;
        foreach ($selected_tickets as $key => $value) {
            $count += $value['quantity'];
        }

        if (!empty($request->delivery_charge_id)) {
            $delivery_charges = $this->delivery_charges($data['delivery_charge_id']);
            $delivery_charge = $delivery_charges / $count;
        }

        if ($request->address_type == 2) {
            $collection_point = $request->collection_point ?? '';
        }

        $delivery_address = $request->delivery_address ?? '';

        if ($request->address_type == 1 || $request->address_type == 3) {
            $delivery_address = $request->delivery_address ?? '';
        }

        $voucher_reward = $data['voucher_reward'] / $count;
        // $processing_fee = [];
        $totalProcessing = 0;
        $totalTicketPrice = 0;

        foreach ($selected_tickets as $key => $value) {
            $key = count($booking) == 0 ? 0 : count($booking);
            $voucher_qty = 0;
            $agent_id = null;
            if ($request->voucher_code && @$value['voucher_data'] && $value['voucher_data']['qty']) {
                $voucher_qty = $value['voucher_data']['qty'];
                $agent_id = $value['voucher_data']['agent_id'];
            }
            for ($i = 1; $i <= $value['quantity']; $i++) {
                $booking[$key]['customer_id'] = $this->customer_id;
                $booking[$key]['customer_name'] = $customer['name'];
                $booking[$key]['customer_email'] = $customer['email'];
                $booking[$key]['organiser_id'] = $this->organiser_id;
                $booking[$key]['event_id'] = $request->event_id;
                $booking[$key]['ticket_id'] = $value['ticket_id'];
                $booking[$key]['delivery_charge'] = $delivery_charge;
                $booking[$key]['quantity'] = 1;
                $booking[$key]['status'] = 1;
                $booking[$key]['created_at'] = Carbon::now();
                $booking[$key]['updated_at'] = Carbon::now();
                $booking[$key]['event_title'] = $data['event']['title'];
                $booking[$key]['event_category'] = $data['event']['category_name'];
                $booking[$key]['ticket_title'] = $value['ticket_title'];
                $booking[$key]['item_sku'] = $data['event']['item_sku'];
                // $booking[$key]['currency']          = setting('regional.currency_default');
                //CUSTOM
                $booking[$key]['currency'] = $this->currency;
                $booking[$key]['wallet'] = $request->input('wallet') ?? NULL;
                $booking[$key]['is_bulk'] = !empty($request->is_bulk) ? $request->is_bulk : 0;
                //CUSTOM
                $booking[$key]['event_repetitive'] = $data['event']->repetitive > 0 ? 1 : 0;

                // non-repetitive
                $booking[$key]['event_start_date'] = $data['event']->start_date;
                $booking[$key]['event_end_date'] = $data['event']->end_date;
                $booking[$key]['event_start_time'] = $data['event']->start_time;
                $booking[$key]['event_end_time'] = $data['event']->end_time;
                $booking[$key]['common_order'] = $common_order;
                $booking[$key]['meta'] = $data['finalFamilyTotal'] ?? '';
                if ($voucher_qty) {
                    $voucher_qty--;
                    $booking[$key]['promocode'] = $request->voucher_code;
                    // if($check_promocode[$key3]['agent_ticket']) {
                    $booking[$key]['agent_id'] = (int) $agent_id;
                    $this->valid_vouchercodes[$key]['agent_ticket_id'] = (int) $agent_id;
                    $this->valid_vouchercodes[$key]['user_id'] = (int) $this->customer_id;
                    $this->valid_vouchercodes[$key]['ticket_id'] = (int) $value['ticket_id'];
                    $this->valid_vouchercodes[$key]['sub_category_id'] = @$value['sub_category_id'] ?? null;
                }
                $booking[$key]['promocode_reward'] = $voucher_reward;
                // repetitive event
                if ($data['event']->repetitive) {
                    $booking[$key]['event_start_date'] = $booking_date;
                    $booking[$key]['event_end_date'] = $request->merge_schedule ? $request->booking_end_date : $booking_date;
                    $booking[$key]['event_start_time'] = $request->start_time;
                    $booking[$key]['event_end_time'] = $request->end_time;
                }


                //CUSTOM
                //set sale price
                if ($tickets->isNotEmpty()) {
                    $tickets = $tickets->map(function ($ticket, $key) {

                        if (!empty($ticket->sale_start_date)) {
                            if ($ticket->sale_price > 0 && $ticket->sale_start_date <= Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString() && $ticket->sale_end_date > Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString()) {
                                $ticket->price = $ticket->sale_price;
                            }
                        }
                        return $ticket;
                    });
                }

                //CUSTOM

                foreach ($tickets as $k => $v) {
                    if ($v['id'] == $value['ticket_id']) {
                        $price = $v['price'];

                        //CUSTOM
                        if (!empty($v['is_donation']))
                            $price = round($value['is_donation'] / $value['quantity']);
                        //CUSTOM

                        break;
                    }
                }
                $booking[$key]['price'] = $price * 1;

                // CUSTOM
                if (!empty($value['is_donation']))
                    $booking[$key]['price'] = $price * 1;
                // CUSTOM

                $booking[$key]['ticket_price'] = $price;

                // call calculate price
                $params = [
                    'ticket_id' => $value['ticket_id'],
                    'quantity' => 1,
                    //CUSTOM
                    'is_donation' => $price,
                    //CUSTOM
                ];

                // calculating net price
                $net_price = $this->calculate_price($params);
                if ($request->voucher_code && $voucher_reward > 0) {
                    $net_price['net_price'] -= $voucher_reward;
                }

                $booking[$key]['tax'] = number_format((float) ($net_price['tax']), 2, '.', '');
                $booking[$key]['net_price'] = number_format((float) ($net_price['net_price']), 2, '.', '') + $delivery_charge;

                // organiser price excluding admin_tax
                $booking_organiser_price[$key]['organiser_price'] = number_format((float) ($net_price['organiser_price']), 2, '.', '');

                $totalProcessing += $net_price['tax'];
                $totalTicketPrice += $net_price['net_price'];
                //  admin_tax
                $admin_tax[$key]['admin_tax'] = number_format((float) ($net_price['admin_tax']), 2, '.', '');

                // if payment method is offline then is_paid will be 0
                if ($request->payment_method == 'offline') {
                    // except free ticket
                    if (((int) $booking[$key]['net_price']))
                        $booking[$key]['is_paid'] = 0;

                    //CUSTOM
                    if (!empty($request->is_bulk)) {
                        $booking[$key]['is_paid'] = 1;
                    }
                    //CUSTOM
                } else {
                    $booking[$key]['is_paid'] = 1;
                }

                //CUSTOM
                if (Auth::user()->hasRole('pos'))
                    $booking[$key]['pos_id'] = Auth::id();
                //CUSTOM

                $key++;
            }
        }


        $promocode_reward = 0;

        // $booking = $this->apply_promocode($request, $booking);

        /* CUSTOM */
        if (!empty($request->event_promocode)) {
            $booking = $this->apply_event_promocode($request, $booking);
            $getPromocode = Promocode::where(['code' => $request->event_promocode])->where('quantity', '>', 0)->firstOrFail();
            $promocode_reward = $getPromocode['reward'] ?? 0;
        }
        /* CUSTOM */


        // calculate commission
        $commission = $this->calculate_commission($booking, $booking_organiser_price, $admin_tax);

        // if net price total == 0 then no paypal process only insert data into booking
        foreach ($booking as $k => $v) {
            $total_price += (float) $v['net_price'];
            $total_price = number_format((float) ($total_price), 2, '.', '');
        }

        $is_direct_checkout = $this->checkDirectCheckout($request, $total_price);

        $ticketTotal = count($booking);
        $totalProcessingFees = $net_price['tax'] * $ticketTotal;
        $inputQuantity = $request['quantity'];
        $inputTitle = $request['ticket_title'];
        $seatingDetails = [];

        foreach ($inputQuantity as $key => $value) {
            if ($value > 0) {
                $seatingDetails[] = $inputTitle[$key];
            }
        }
        $paymentDetails = [
            'net_amount' => $net_price,
            'total_ticket' => $ticketTotal,
            'currency' => $this->currency,
            'net_ticket_price' => $totalTicketPrice - $totalProcessing + $data['voucher_reward'] ?? 0,
            'total_ticket_price' => $total_price + $promocode_reward + $data['voucher_reward'] - $totalProcessingFees - $delivery_charges,
            'tp_promo' => $total_price - $promocode_reward - $totalProcessingFees - $delivery_charges,
            'tp_voucher' => $total_price - $promocode_reward - $data['voucher_reward'] - $totalProcessing - $delivery_charges,
            'processing_fee' => $totalProcessing,
            'total_processing_fee' => $totalProcessingFees,
            'delivery_fee' => $delivery_charges,
            'promocode_reward' => $promocode_reward ?? 0,
            'voucher_reward' => $data['voucher_reward'] ?? 0,
            'delivery_address' => $delivery_address,
            'direct_checkout' => $is_direct_checkout,
            'voucher_code' => $request->voucher_code ?? null,
            'gross_total' => number_format((float) ($totalTicketPrice + $delivery_charges), 2, '.', ''),
            'seating_details' => $seatingDetails
        ];

        //5. Validate Event Promocode
        if (!empty($request->voucher_code)) {

            $voucherCodeValidation = $this->validateVoucherCode($request);
            // dd($voucherCodeValidation);
            if (!$voucherCodeValidation['status']) {
                return response()->json(['validation' => $voucherCodeValidation['errors'], 'data' => $inputData, 'payment_data' => $paymentDetails], 400);
            }
            if ($voucherCodeValidation['status'] && !empty($voucherCodeValidation['errors'])) {
                return response()->json(['validation' => $voucherCodeValidation['errors'], 'data' => $inputData, 'payment_data' => $paymentDetails], 400);
            }
        }

        return response()->json(['data' => $inputData, 'payment_data' => $paymentDetails], 200);
    }

    public function stripeFinishBooking(Request $request)
    {
        // dd($request->all());
        $inputData = $request->validate([
            'payment_intent_id' => 'required',
            'booking' => 'required',
            'customerData' => 'required',
            'seats' => 'required',
            'selected_attendees' => 'required',
            'commission' => 'required',
            'delivery_address' => 'required'
        ]);

        $paymentIntentId = $inputData['payment_intent_id'];
        $booking = $inputData['booking'];
        $data = $inputData['customerData'];
        $seats = $inputData['seats'];
        $selected_attendees = $inputData['selected_attendees'];
        $commission = $inputData['commission'];
        $delivery_address = $inputData['delivery_address'];

        $order = [
            'item_sku' => $booking[key($booking)]['item_sku'],
            'order_number' => time() . rand(1, 988),
            'product_title' => $booking[key($booking)]['event_title'],

            'price_title' => '',
            'price_tagline' => '',
            'currency' => @$booking[key($booking)]['currency']
        ];

        // Set your Stripe API key
        Stripe::setApiKey(setting('apps.stripe_secret_key'));
        try {
            // Retrieve the payment intent from Stripe
            $paymentIntent = PaymentIntent::retrieve($paymentIntentId);

            // Perform actions after successful payment confirmation
            $booked_data = $this->finish_booking_stripe(
                $booking,
                $data,
                $seats,
                $selected_attendees,
                $paymentIntent['charges'],
                $commission,
                $order
            );

            $order = $this->create_order($booked_data, $request->event_promocode, $delivery_address);

            return response()->json(['message' => 'Payment confirmed successfully', 'data' => $booked_data, 'order' => $order]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    protected function finish_booking_stripe($booking = [], $data = [], $seats, $selected_attendees, $paymentIntent, $commission, $order = [])
    {
        $stripePaymentDetails = $paymentIntent['data'][0];
        //CUSTOM
        $bulk_code = empty($data['bulk_code']) ? null : $data['bulk_code'];
        $payment_gateway = 'Stripe';
        //CUSTOM

        $admin_commission = setting('multi-vendor.admin_commission');

        $params = [];
        foreach ($booking as $key => $value) {
            $params[$key] = $value;
            // $params[$key]['order_number']    = $data['order_number'];
            //CUSTOM
            $params[$key]['order_number'] = $order['order_number'];
            $params[$key]['bulk_code'] = $bulk_code;
            //CUSTOM
            $params[$key]['transaction_id'] = $stripePaymentDetails['id'];

            // is online or offline
            $params[$key]['payment_type'] = 'offline';
            if ($stripePaymentDetails['transaction_id'])
                $params[$key]['payment_type'] = 'online';
        }

        // get booking_id
        // update commission session array
        // insert into commission
        $commission_data = [];

        // delete commission data from session
        $booking_data = [];
        foreach ($booking as $key => $value) {
            $data = $this->booking->make_booking($params[$key]);
            $booking_data[] = $data;
            if ($value['net_price'] > 0) {
                $commission_data[$key] = $commission[$key];
                $commission_data[$key]['booking_id'] = $data->id;
                $commission_data[$key]['month_year'] = Carbon::parse($data->created_at)->format('m Y');
                $commission_data[$key]['created_at'] = Carbon::now();
                $commission_data[$key]['updated_at'] = Carbon::now();
                $commission_data[$key]['event_id'] = $data->event_id;
                $commission_data[$key]['status'] = $data->is_paid > 0 ? 1 : 0;
            }
        }

        // insert data in commission table
        $this->commission->add_commission($commission_data);

        //CUSTOM
        $this->check_promocode($this->valid_promocodes);

        $this->save_attendee($booking_data, $seats, $selected_attendees);

        $this->send_email($booking_data);



        $transaction_data['txn_id'] = $stripePaymentDetails['id'] ?? 0;
        $transaction_data['user_id'] = auth()->user()->id;
        $transaction_data['amount_paid'] = $data['price'] ?? 0;
        unset($transaction_data['price']);
        $transaction_data['currency_code'] = !empty($booking[key($booking)]['currency']) ? $booking[key($booking)]['currency'] : setting('regional.currency_default');
        $transaction_data['payment_status'] = $data['message'] ?? '';
        $transaction_data['item_sku'] = $order['item_sku'];
        $transaction_data['order_number'] = $order['order_number'];
        $transaction_data['currency'] = $order['currency'];
        $transaction_data['payer_reference'] = $stripePaymentDetails['id'] ?? 0;
        $transaction_data['payment_gateway'] = 'Stripe';
        $transaction_data['meta'] = $paymentIntent;
        $transaction_data['status'] = 1;
        $transaction_data['created_at'] = Carbon::now();
        $transaction_data['updated_at'] = Carbon::now();

        $this->transaction->add_transaction($transaction_data);

        return $booking_data;
    }

    protected function checkDirectCheckout(Request $request, $total_price = 0)
    {
        // check if Free event
        if ($total_price <= 0)
            return true;

        // if it's Admin
        if (Auth::user()->hasRole('admin'))
            return true;

        // get payment method
        // paypal will always be default payment method
        // payment_method can either 1 or offline
        $payment_method = 1;
        if ($request->has('payment_method')) {
            if ($request->payment_method == 'offline')
                $payment_method = 'offline';
            else
                $payment_method = (int) $request->payment_method;
        }

        // if not-offline
        if ($payment_method != 'offline')
            return false;

        /* In case of offline method selected */

        // if Organizer
        // check if offline_payment_organizer enabled
        if (Auth::user()->hasRole('organiser') || Auth::user()->hasRole('pos'))
            // if(setting('booking.offline_payment_organizer'))
            //CUSTOM
            // if(setting('booking.offline_payment_organizer'))
            if (setting('booking.offline_payment_organizer') || !empty($request->is_bulk))
                //CUSTOM
                return true;

        // if Customer
        // check if offline_payment_customer enabled
        if (Auth::user()->hasRole('customer'))
            if (setting('booking.offline_payment_customer'))
                return true;

        return false;
    }

    /*===================== Apply Event Promocode ==============================*/
    protected function apply_event_promocode(Request $request, $booking = [])
    {
        if (!$request->event_promocode) {
            return $booking;
        }
        $net_total_price = 0;

        // count total price
        foreach ($booking as $key => $val) {
            $net_total_price += $val['net_price'];
        }

        $event = $request->event_id;
        $tickets = $request->ticket_id;
        $tickets_quantity = $request->quantity;
        $promocode = $request->event_promocode;
        $check_promocode = Promocode::where(['code' => $promocode])->where('quantity', '>', 0)->firstOrFail();
        if ($net_total_price > 0) {
            if ($promocode) {
                // check promocode
                try {


                    if (!empty($check_promocode)) {
                        $params = [
                            'event_id' => $event,
                        ];
                        $this->promocode = new Promocode;

                        // get ticket's promocode's ids
                        $event_promocodes_ids = $this->promocode->get_event_promocodes_ids($params);

                        if (!empty($event_promocodes_ids)) {
                            $promocodes_ids = [];

                            foreach ($event_promocodes_ids as $key1 => $value1) {
                                $promocodes_ids[] = $value1['promocode_id'];
                            }

                            $params = [
                                'promocodes_ids' => $promocodes_ids,
                            ];

                            // get tikcet's promocodes
                            $event_promocodes = $this->promocode->get_event_promocodes($params);
                            if (!empty($event_promocodes)) {

                                $promocode_match = false;

                                // match user promocode with particular tickets's promocodes
                                foreach ($event_promocodes as $key2 => $value2) {
                                    if ($value2['code'] == $promocode) {
                                        $promocode_match = true;
                                        break;
                                    }
                                }

                                if ($promocode_match) {
                                    // apply promocode
                                    $user_promocode = [];

                                    // manual check in promocode_user if promocode not already applied by the user

                                    $params = [
                                        'user_id' => $booking[key($booking)]['customer_id'],
                                        'promocode_id' => $check_promocode->id,
                                        'event_id' => $event
                                    ];

                                    $user_promocode = $this->promocode->promocode_event_user($params);

                                    if (empty($user_promocode)) {
                                        if (!empty($booking)) {
                                            $total_booking = collect($booking)->count();
                                            $booking_collection = collect($booking)->groupBy('ticket_id');

                                            foreach ($booking as $key3 => $value3) {

                                                if ((int) $value3['net_price'] > 0) {
                                                    if ($check_promocode->p_type == 'fixed') {
                                                        $promocode_reward = (float) ($check_promocode->reward / $total_booking);

                                                        $booking[$key3]['net_price'] = (float) $value3['net_price'] - $promocode_reward;

                                                        $booking[$key3]['promocode_reward'] = $promocode_reward;
                                                    } else {
                                                        $promocode_reward = (float) (($booking[$key3]['net_price'] * (float) ($check_promocode->reward / $total_booking)) / 100);

                                                        $booking[$key3]['net_price'] = (float) ($booking[$key3]['net_price'] - $promocode_reward);

                                                        $booking[$key3]['promocode_reward'] = $promocode_reward;
                                                    }
                                                    $booking[$key3]['promocode_id'] = $check_promocode->id;
                                                    $booking[$key3]['promocode'] = $check_promocode->code;

                                                    // store valid promocode
                                                    $this->valid_promocodes[$key3]['promocode_id'] = (int) $check_promocode->id;
                                                    $this->valid_promocodes[$key3]['user_id'] = (int) $booking[$key3]['customer_id'];
                                                    $this->valid_promocodes[$key3]['ticket_id'] = (int) $value3['ticket_id'];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (\Throwable $e) {
                }
            }
        }

        $valid_promocodes = null;

        if (!empty($this->valid_promocodes)) {
            $valid_promocodes = $this->valid_promocodes;
        }

        return $booking;
    }
    protected function apply_voucher_code(Request $request, $booking = [], $coupon_data = [])
    {
        $net_total_price = 0;

        // count total price
        foreach ($booking as $key => $val) {
            $net_total_price += $val['net_price'];
        }

        $tickets = $request->ticket_id;
        $tickets_quantity = $request->quantity;
        $sub_category_ids = $request->sub_category_id;
        $promocodes = $request->promocode;
        $type[] = 'general';
        if (Auth::user()->type == 'student') {
            $type[] = 'student';
        }
        foreach ($booking as $key3 => $value3) {
            if (@$coupon_data[$key3]) {
                if (@$booking[$key3]['sub_category_id']) {
                    $check_promocode = AgentTicket::where('sub_category_id', $booking[$key3]['sub_category_id'])->whereIn('type', $type)->where('coupon_code', $coupon_data[$key3])->orderBy('type', 'desc')->where('quantity', '>', 0)?->first();
                } else {
                    $check_promocode = AgentTicket::where('coupon_code', $coupon_data[$key3])->whereIn('type', $type)->whereNull('sub_category_id')->orderBy('type', 'desc')->where('quantity', '>', 0)?->first();
                }
                if ($check_promocode) {
                    $check_promocode = $check_promocode->toArray();
                    if ($value3['ticket_id'] && (int) $value3['net_price'] > 0) {
                        if ($check_promocode['code_value'] == 'fixed') {
                            $promocode_reward = (float) ($check_promocode['discount']);

                            $booking[$key3]['net_price'] = (float) $value3['net_price'] - $promocode_reward;

                            $booking[$key3]['promocode_reward'] = $promocode_reward;
                        } else {
                            $promocode_reward = (float) (($booking[$key3]['net_price'] * $check_promocode['discount']) / 100);

                            $booking[$key3]['net_price'] = (float) ($booking[$key3]['net_price'] - $promocode_reward);

                            $booking[$key3]['promocode_reward'] = $promocode_reward;
                        }

                        $booking[$key3]['promocode'] = $check_promocode['code'];
                        // if($check_promocode[$key3]['agent_ticket']) {
                        $booking[$key3]['agent_id'] = (int) $check_promocode['id'];
                        //}
                        $this->valid_vouchercodes[$key3]['agent_ticket_id'] = (int) $check_promocode['id'];
                        $this->valid_vouchercodes[$key3]['user_id'] = (int) $booking[$key3]['customer_id'];
                        $this->valid_vouchercodes[$key3]['ticket_id'] = (int) $value3['ticket_id'];
                    }
                }
            }
        }

        if (!empty($this->valid_vouchercodes)) {
            (['valid_vouchercodes' => $this->valid_vouchercodes]);
        }

        return $booking;
    }

    protected function create_order($booking = [], $event_promocode = null, $delivery_address = '', $address_type = 1, $collection_point = '')
    {
        // dd($booking);
        $order_data = [];
        $order_data['event_id'] = $booking[0]['event_id'];
        $order_data['customer_id'] = $booking[0]['customer_id'];
        $order_data['quantity'] = count($booking);
        $net_total_price = 0;
        $total_price = 0;
        $total_tax = 0;
        $promocode_reward = 0;
        $ids = [];

        // count total price
        foreach ($booking as $key => $val) {
            // dd($val);
            $net_total_price += $val['net_price'];
            $total_price += $val['price'];
            $total_tax += $val['tax'];
            $promocode_reward += $val['promocode_reward'] ?? 0;
            $order_data['address_type'] = $address_type;
            $order_data['collection_point'] = $collection_point;
            $ids[] = $val['id'];
        }

        $order_data['net_price'] = $net_total_price;
        $order_data['price'] = $total_price;
        $order_data['tax'] = $total_tax;
        $order_data['promocode_reward'] = $promocode_reward;
        $order_data['delivery_address'] = $delivery_address;
        if ($event_promocode) {
            $order_data['promocode_id'] = $booking[0]['promocode_id'];
        }

        $order = Order::create($order_data);
        if ($ids) {
            $order->bookings()->sync($ids);
        }
        return $order;
    }

    protected function finish_booking_wallet($booking = [], $data = [], $seats, $selected_attendees, $commission = [], $transaction_data = [], $order = [], $currency = [], $service, $address_type = '', $collection_point = '', $delivery_address = '', $promocode = '')
    {
        try {
            $payment_data = [

                'currency' => $order['currency'] ?? null,
                'wallet' => $order['wallet'] ?? null,
                'amount' => $order['price'] ?? null,
                'service' => $order['price_title'] ?? null,
            ];

            $response = $service->walletPayment($payment_data, auth()->user());
            if (($response['code'] === 200) && $response['code'] === 200) {
                $flag['transaction_id'] = $flag['payer_reference'] = $response['data']['urn'];
                $flag['message'] = 'Captured';
                $flag['status'] = true;


                $bulk_code = empty($data['bulk_code']) ? null : $data['bulk_code'];
                $payment_gateway = 'Wallet';
                //CUSTOM

                $admin_commission = setting('multi-vendor.admin_commission');
                $params = [];
                foreach ($booking as $key => $value) {
                    $params[$key] = $value;
                    // $params[$key]['order_number']    = $data['order_number'];
                    //CUSTOM
                    $params[$key]['order_number'] = $order['order_number'];
                    $params[$key]['bulk_code'] = $bulk_code;
                    //CUSTOM
                    $params[$key]['transaction_id'] = $flag['transaction_id'];

                    // is online or offline
                    $params[$key]['payment_type'] = 'offline';
                    if ($flag['transaction_id'])
                        $params[$key]['payment_type'] = 'online';
                }

                // get booking_id
                // update commission session array
                // insert into commission
                $commission_data = [];
                $booking_data = [];

                // var_dump($params);
                foreach ($booking as $key => $value) {
                    $data = $this->booking->make_booking($params[$key]);
                    $booking_data[] = $data;
                    if ($value['net_price'] > 0) {
                        $commission_data[$key] = $commission[$key];
                        $commission_data[$key]['booking_id'] = $data->id;
                        $commission_data[$key]['month_year'] = Carbon::parse($data->created_at)->format('m Y');
                        $commission_data[$key]['created_at'] = Carbon::now();
                        $commission_data[$key]['updated_at'] = Carbon::now();
                        $commission_data[$key]['event_id'] = $data->event_id;
                        $commission_data[$key]['status'] = $data->is_paid > 0 ? 1 : 0;
                    }
                }

                $booked_orders = $this->create_order($booking_data, $promocode, $delivery_address, $address_type, $collection_point);
                // insert data in commission table
                $this->commission->add_commission($commission_data);

                //CUSTOM
                $this->check_promocode($this->valid_promocodes);

                $this->save_attendee($booking_data, $seats, $selected_attendees);
                $this->send_email($booking_data);

                if ($flag['status']) {
                    $transaction_data['txn_id'] = $flag['transaction_id'];
                    $transaction_data['user_id'] = auth()->user()->id;
                    $transaction_data['amount_paid'] = $order['price'];
                    unset($transaction_data['price']);
                    $transaction_data['currency_code'] = !empty($booking[key($booking)]['currency']) ? $booking[key($booking)]['currency'] : setting('regional.currency_default');
                    $transaction_data['payment_status'] = $flag['message'];
                    $transaction_data['item_sku'] = $order['item_sku'];
                    $transaction_data['order_number'] = $order['order_number'];
                    $transaction_data['currency'] = $order['currency'];
                    $transaction_data['payment_gateway'] = $payment_gateway;
                    $transaction_data['status'] = 1;
                    $transaction_data['created_at'] = Carbon::now();
                    $transaction_data['updated_at'] = Carbon::now();
                }

                $this->transaction->add_transaction($transaction_data);

                return ['booking' => $booking_data, 'order' => $booked_orders, 'status' => true];
            } else {
                return ['status' => false, 'data' => $response];
            }
        } catch (Exception $e) {
            // Handle the error
            return $flag = [
                'status' => false,
                'data' => [
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'code' => $e->getCode(),
                ]
            ];
        }

        //CUSTOM
    }

    protected function admin_delivery_charges($event_id)
    {
        // get admin delivery_charge
        return $this->event->get_admin_delivery_charges($event_id);
    }

    /**
     *  admin tax apply on all tickets
     */

    protected function delivery_charges($id)
    {
        // get admin delivery_charge
        return $this->event_delivery_charge->get_delivery_charges($id);
    }

    public function apply_event_promocodes(Request $request)
    {
        $request->validate([
            'event_id' => 'required|numeric|gt:0',
            'event_promocode' => 'required|max:32|String',
        ]);

        $check_promocode = [];

        // check promocode
        try {

            $check_promocode = Promocode::where(['code' => $request->event_promocode])->where('quantity', '>', 0)->firstOrFail();
        } catch (\Throwable $e) {

            return ['message' => __('eventmie-pro::em.invalid_promocode'), 'status' => false];
        }

        if (empty($check_promocode)) {
            return ['message' => __('eventmie-pro::em.invalid_promocode'), 'status' => false];
        }

        $user_promocode = [];
        // manual check in promocode_user if promocode not already applied by the user

        $params = [
            'user_id' => auth()->user()->id,
            'promocode_id' => $check_promocode->id,
            'event_id' => $request->event_id
        ];

        $user_promocode = $this->promocode->promocode_event_user($params);

        if (!empty($user_promocode))
            return ['message' => __('eventmie-pro::em.already_used_promocode'), 'status' => false];

        $params = [
            'event_id' => $request->event_id
        ];

        // check ticket exist or not
        $check_event = $this->event->get_event_only($params);

        if (empty($check_event)) {
            return ['message' => __('eventmie-pro::em.event_not_found'), 'status' => false];
        }

        $event_promocodes_ids = $this->promocode->get_event_promocodes_ids($params);

        if (empty($event_promocodes_ids))
            return ['message' => __('eventmie-pro::em.invalid_promocode'), 'status' => false];

        $promocodes_ids = [];

        foreach ($event_promocodes_ids as $key => $value) {
            $promocodes_ids[] = $value['promocode_id'];
        }

        $params = [
            'promocodes_ids' => $promocodes_ids
        ];

        $event_promocodes = $this->promocode->get_event_promocodes($params);

        if (empty($event_promocodes))
            return ['message' => __('eventmie-pro::em.invalid_promocode'), 'status' => false];

        $promocode_match = false;

        // match user promocode with particular tickets's promocodes
        foreach ($event_promocodes as $key => $value) {
            if ($value['code'] == $request->event_promocode) {
                $promocode_match = true;
                break;
            }
        }
        if (!$promocode_match)
            return ['message' => __('eventmie-pro::em.invalid_promocode'), 'status' => false];

        return ['status' => true, 'promocode' => $check_promocode];
    }

    public function ai_sensy_book_tickets(Request $request, $flag)
    {

        //CUSTOM
        $this->setEventCurrency($request);
        //CUSTOM

        // check login user role
        $status = $this->is_admin_organiser($request);

        // organiser can't book other organiser event's tikcets but  admin can book any organiser events'tikcets for customer
        if (!$status) {

            return response()->json([
                'status' => false,
                'message' => __('eventmie-pro::em.organiser_note_5'),
            ], 200);
        }

        // 1. General validation and get selected ticket and event id
        $data = $this->general_validation($request);
        if (!$data['status']) {
            return response()->json(['error' => $data['error']], 400);
        }

        // 2. Check availability
        $check_availability = $this->availability_validation($data);
        if (!$check_availability['status'])
            return response()->json(['error' => $check_availability['error']], 400);

        // 3. TIMING & DATE CHECK
        $pre_time_booking = $this->time_validation($data);
        if (!$pre_time_booking['status'])
            return response()->json(['error' => $pre_time_booking['error']], 400);

        $selected_tickets = $data['selected_tickets'];
        $tickets = $data['tickets'];
        $selected_attendees = $data['selected_attendees'];
        $seats = $data['seats'];

        // dd($seats, $selected_attendees);

        $booking_date = $request->booking_date;

        $params = [
            'customer_id' => $this->customer_id,
        ];
        // get customer information by customer id
        $customer = $this->user->get_customer($params);

        //CUSTOM
        if (!empty($request->is_bulk)) {
            $customer = $data['customer'];
        }
        //CUSTOM

        if (empty($customer))
            return response()->json(['data' => $pre_time_booking['error']], 400);

        //CUSTOM

        if (!empty(setting('apps.twilio_sid')) && !empty(setting('apps.twilio_auth_token')) && !empty(setting('apps.twilio_number')) && empty($customer->phone)) {
            $request->validate([
                'phone_t' => 'nullable',
            ]);

            User::where(['id' => $this->customer_id])->update(['phone' => $request->phone_t]);
        }
        //CUSTOM

        $booking = [];
        $price = 0;
        $total_price = 0;

        // organiser_price excluding admin_tax
        $booking_organiser_price = [];
        $admin_tax = [];
        $common_order = time() . rand(1, 988);

        $delivery_charges = 0;

        if (!empty($request->delivery_charge_id)) {
            $delivery_charges = $this->delivery_charges($data['delivery_charge_id']) ?? '';
        }

        $collection_point = '';
        if ($request->address_type == 2) {
            $collection_point = $request->collection_point ?? '';
        }

        if ($request->address_type == 1 || $request->address_type == 3) {
            $delivery_address = $request->delivery_address ?? '';
        }

        $count = 0;

        foreach ($selected_tickets as $key => $value) {
            $count += $value['quantity'];
        }

        $voucher_reward = $data['voucher_reward'] / $count;

        $delivery_charge = $delivery_charges / $count;

        foreach ($selected_tickets as $key => $value) {
            $key = count($booking) == 0 ? 0 : count($booking);
            $voucher_qty = 0;
            $agent_id = null;
            if ($request->voucher_code && @$value['voucher_data'] && $value['voucher_data']['qty']) {
                $voucher_qty = $value['voucher_data']['qty'];
                $agent_id = $value['voucher_data']['agent_id'];
            }
            for ($i = 1; $i <= $value['quantity']; $i++) {
                $booking[$key]['customer_id'] = $this->customer_id;
                $booking[$key]['customer_name'] = $customer['name'];
                $booking[$key]['customer_email'] = $customer['email'];
                $booking[$key]['organiser_id'] = $this->organiser_id;
                $booking[$key]['event_id'] = $request->event_id;
                $booking[$key]['ticket_id'] = $value['ticket_id'];
                $booking[$key]['delivery_charge'] = $delivery_charge;
                $booking[$key]['quantity'] = 1;
                $booking[$key]['status'] = 1;
                $booking[$key]['created_at'] = Carbon::now();
                $booking[$key]['updated_at'] = Carbon::now();
                $booking[$key]['event_title'] = $data['event']['title'];
                $booking[$key]['event_category'] = $data['event']['category_name'];
                $booking[$key]['ticket_title'] = $value['ticket_title'][0];
                $booking[$key]['item_sku'] = $data['event']['item_sku'];
                // $booking[$key]['currency']          = setting('regional.currency_default');
                //CUSTOM
                $booking[$key]['currency'] = $this->currency;
                $booking[$key]['wallet'] = $request->input('wallet') ?? NULL;
                $booking[$key]['is_bulk'] = !empty($request->is_bulk) ? $request->is_bulk : 0;
                //CUSTOM
                $booking[$key]['event_repetitive'] = $data['event']->repetitive > 0 ? 1 : 0;

                // non-repetitive
                $booking[$key]['event_start_date'] = $data['event']->start_date;
                $booking[$key]['event_end_date'] = $data['event']->end_date;
                $booking[$key]['event_start_time'] = $data['event']->start_time;
                $booking[$key]['event_end_time'] = $data['event']->end_time;
                $booking[$key]['common_order'] = $common_order;
                $booking[$key]['meta'] = $data['finalFamilyTotal'] ?? '';
                if ($voucher_qty) {
                    $voucher_qty--;
                    $booking[$key]['promocode'] = $request->voucher_code;
                    // if($check_promocode[$key3]['agent_ticket']) {
                    $booking[$key]['agent_id'] = (int) $agent_id;
                    $this->valid_vouchercodes[$key]['agent_ticket_id'] = (int) $agent_id;
                    $this->valid_vouchercodes[$key]['user_id'] = (int) $this->customer_id;
                    $this->valid_vouchercodes[$key]['ticket_id'] = (int) $value['ticket_id'];
                    $this->valid_vouchercodes[$key]['sub_category_id'] = @$value['sub_category_id'] ?? null;
                }
                $booking[$key]['promocode_reward'] = $voucher_reward;
                // repetitive event
                if ($data['event']->repetitive) {
                    $booking[$key]['event_start_date'] = $booking_date;
                    $booking[$key]['event_end_date'] = $request->merge_schedule ? $request->booking_end_date : $booking_date;
                    $booking[$key]['event_start_time'] = $request->start_time;
                    $booking[$key]['event_end_time'] = $request->end_time;
                }


                //CUSTOM
                //set sale price
                if ($tickets->isNotEmpty()) {
                    $tickets = $tickets->map(function ($ticket, $key) {

                        if (!empty($ticket->sale_start_date)) {
                            if ($ticket->sale_price > 0 && $ticket->sale_start_date <= Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString() && $ticket->sale_end_date > Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString()) {
                                $ticket->price = $ticket->sale_price;
                            }
                        }
                        return $ticket;
                    });
                }

                //CUSTOM

                foreach ($tickets as $k => $v) {
                    if ($v['id'] == $value['ticket_id']) {
                        $price = $v['price'];

                        //CUSTOM
                        if (!empty($v['is_donation']))
                            $price = round($value['is_donation'] / $value['quantity']);
                        //CUSTOM

                        break;
                    }
                }
                $booking[$key]['price'] = $price * 1;

                // CUSTOM
                if (!empty($value['is_donation']))
                    $booking[$key]['price'] = $price * 1;
                // CUSTOM

                $booking[$key]['ticket_price'] = $price;

                // call calculate price
                $params = [
                    'ticket_id' => $value['ticket_id'],
                    'quantity' => 1,
                    //CUSTOM
                    'is_donation' => $price,
                    //CUSTOM
                ];

                // calculating net price
                $net_price = $this->calculate_price($params);
                if ($request->voucher_code && $voucher_reward > 0) {
                    $net_price['net_price'] -= $voucher_reward;
                }

                $booking[$key]['tax'] = number_format((float) ($net_price['tax']), 2, '.', '');
                $booking[$key]['net_price'] = number_format((float) ($net_price['net_price']), 2, '.', '') + $delivery_charge;

                // organiser price excluding admin_tax
                $booking_organiser_price[$key]['organiser_price'] = number_format((float) ($net_price['organiser_price']), 2, '.', '');

                //  admin_tax
                $admin_tax[$key]['admin_tax'] = number_format((float) ($net_price['admin_tax']), 2, '.', '');

                // if payment method is offline then is_paid will be 0
                if ($request->payment_method == 'offline') {
                    // except free ticket
                    if (((int) $booking[$key]['net_price']))
                        $booking[$key]['is_paid'] = 0;

                    //CUSTOM
                    if (!empty($request->is_bulk)) {
                        $booking[$key]['is_paid'] = 1;
                    }
                    //CUSTOM
                } else {
                    $booking[$key]['is_paid'] = 1;
                }

                //CUSTOM
                if (Auth::user()->hasRole('pos'))
                    $booking[$key]['pos_id'] = Auth::id();
                //CUSTOM

                $key++;
            }
        }
        if (!empty($this->valid_vouchercodes)) {
            session(['valid_vouchercodes' => $this->valid_vouchercodes]);
        }
        /* CUSTOM */
        // $booking = $this->apply_promocode($request, $booking);
        if (!empty($request->event_promocode)) {
            $booking = $this->apply_event_promocode($request, $booking);
        }        /* CUSTOM */

        // calculate commission
        $commission = $this->calculate_commission(
            $booking,
            $booking_organiser_price,
            $admin_tax
        );

        // if net price total == 0 then no paypal process only insert data into booking
        foreach ($booking as $k => $v) {
            $total_price += (float) $v['net_price'];
            $total_price = number_format((float) ($total_price), 2, '.', '');
        }

        $order = [
            'item_sku' => $booking[key($booking)]['item_sku'],
            'order_number' => time() . rand(1, 988),
            'product_title' => $booking[key($booking)]['event_title'],

            'price_title' => '',
            'price_tagline' => '',
            'wallet' => @$booking[key($booking)]['wallet'],
            'currency' => @$booking[key($booking)]['currency']
        ];

        $order['price'] = $total_price;


        $flag = $this->finish_booking($booking, $data, $seats, $selected_attendees, [], $flag, $commission, $order, $request);

        if (!empty($this->valid_vouchercodes)) {

            foreach ($this->valid_vouchercodes as $key => $value) {
                $agent_tickets = new AgentTicket();
                $agent_tickets->voucher_code_apply($value);
            }
        }

        if (empty($flag)) {
            return response()->json(["Database Failure"], 400);
        }

        // redirect no matter what so that it never turns backreturn response
        $msg = __('eventmie-pro::em.booking_success');
        // dd($msg);

        return response()->json([
            'status' => true,
            'message' => $msg,
            'order' => $order
        ], 200);
    }

    protected function check_voucher_code()
    {

        if (!empty(session('valid_promocodes'))) {
            foreach (session('valid_promocodes') as $key => $value) {
                $this->promocode = new Promocode;
                $this->promocode->promocode_apply($value);
            }

            session()->forget(['valid_promocodes']);
        } elseif (!empty(session('valid_vouchercodes'))) {
            foreach (session('valid_vouchercodes') as $key => $value) {

                $this->agent_tickets = new AgentTicket();
                $this->agent_tickets->voucher_code_apply($value);
            }

            session()->forget(['valid_vouchercodes']);
        }
    }

    public function validateVoucherCode(Request $request)
    {
        $request->validate([
            'voucher_code' => 'max:32|String',
            'ticket_id' => 'required',
        ], [
            'voucher_code' => __('eventmie-pro::em.voucher_code') . ' ' . __('eventmie-pro::em.required'),
        ]);

        $type[] = 'general';
        if (Auth::user()?->type == 'student') {
            $type[] = 'student';
        }
        $errors = [];
        $error = '';
        $validation = [];
        $validationCode = [];
        $message = '';
        $voucher_rewards = 0;
        foreach ($request->tickets_id as $key => $id) {
            try {
                $ticket = Ticket::findOrFail($id);
                $price = $ticket->get_price();
                $qty = $request->quantity[$key];
                $sub_cat_title = '';
                $i = null;

                $check_vouchercode  = AgentTicket::where('ticket_id', $id)->whereIn('type', $type)->where('coupon_code', $request->voucher_code)->orderBy('type', 'desc')->first();
                if (is_null($check_vouchercode)) {
                    $message = "Voucher code is invalid.";
                    // $validationCode[] = "Voucher code is not applied for " . $ticket->title;
                    // return ['status' => $status, 'errors' => $errors];
                }
                $book_count = $check_vouchercode->bookings?->count();
                if (($check_vouchercode->quantity - $book_count) < $qty) {
                    $sold_out = $qty - $check_vouchercode->quantity + $book_count;
                    $qty  = $qty - $sold_out;
                    if ($qty < 1) {
                        $errors[] = "Ticket : " . $ticket->title . " " . $sub_cat_title . " Qty = " . $sold_out . " Sold Out";
                    }
                    if ($qty > 0) {
                        $errors[] = "Ticket : " . $ticket->title . " " . $sub_cat_title . " Qty = " . $qty . " only applied";
                    }
                }
                if ($qty > 0) {
                    $validDate = strtotime($check_vouchercode->valid_to);
                    $todayDate = strtotime(date("Y-m-d"));
                    if (($validDate < $todayDate) && ($validDate != $todayDate)) {
                        $errors[] = "Ticket : " . $ticket->title . " " . $sub_cat_title . " Voucher code Expired ";
                    } else {
                        $user_promocode = [];
                        // manual check in promocode_user if promocode not already applied by the user

                        $params = [
                            'user_id'   => auth()->user()->id,
                            'agent_ticket_id' => $check_vouchercode->id,
                            'ticket_id' => $id,
                            'sub_category_id' => $i > 0 ?? null,
                        ];
                        $user_promocode = $this->agent_tickets->agentcode_user($params);
                        // dd(true);
                        $already_used =  $check_vouchercode->max_uses - $user_promocode;
                        if ($already_used <  1) {
                            $errors[] = "Ticket : " . $ticket->title . " " . $sub_cat_title . "Qty= " . $already_used . " Voucher Code already Used ";
                        }
                        if ($already_used > $qty) {
                            if ($check_vouchercode->code_value == 'percent') {
                                $voucher_rewards +=  $qty * $price * ($check_vouchercode->discount / 100);
                            } else {
                                $voucher_rewards += $qty * $check_vouchercode->discount;
                            }
                        } else {
                            $qty = $already_used;
                            if ($check_vouchercode->code_value == 'percent') {
                                $voucher_rewards +=  $already_used * $price * ($check_vouchercode->discount / 100);
                            } else {
                                $voucher_rewards += $already_used * $check_vouchercode->discount;
                            }
                        }
                        $error = '';
                    }
                }
            } catch (\Throwable $e) {

                $check_vouchercode_student  = AgentTicket::where('ticket_id', $id)->where('type', 'student')->where('coupon_code', $request->voucher_code)->orderBy('type', 'desc')->first();

                if (!is_null($check_vouchercode_student)) {
                    $error = "To apply Student Voucher Code, please Register/Login.";
                } else {
                    // dd(true);
                    $error = "Voucher code is invalid.";
                }
            }
        }

        // dd($voucher_rewards,(count($validationCode) === 0));
        // if ($voucher_rewards == 0 && !empty($message)) {
        //     $errors[] = $message;
        //     return ['status' => false, 'errors' => $errors];
        // } elseif ($voucher_rewards == 0 && (count($errors) === 0)) {
        //     $errors[] = $error;
        //     return ['status' => false, 'errors' => $errors];
        // } elseif ($voucher_rewards != 0 && !empty($message)) {
        //     $errors[] = $message;
        //     return ['status' => false, 'errors' => $errors];
        // } elseif ($voucher_rewards != 0 && (count($errors) === 0)) {
        //     $errors[] = $error;
        //     return ['status' => false, 'errors' => $errors];
        // }
        if ($voucher_rewards == 0 && !empty($message)) {
            $errors[] = $message;
            return ['status' => false, 'errors' => $errors];
        } elseif ($voucher_rewards == 0 && (count($errors) === 0)) {
            $errors[] = $error;
            return ['status' => false, 'errors' => $errors];
        } elseif ($voucher_rewards != 0 && !empty($message)) {
            // $errors[] = $message;
            return ['status' => true, 'errors' => $errors];
        } elseif ($voucher_rewards != 0 && (count($errors) === 0)) {
            $errors[] = $error;
            return ['status' => false, 'errors' => $errors];
        }

        // elseif ($voucher_rewards != 0 && !(count($validationCode) === 0)) {
        //     $errors[] = $error;
        //     return ['status' => false, 'errors' => $validationCode];
        // }elseif ($voucher_rewards == 0 && (count($validationCode) === 0)) {
        //     $errors[] = $error;
        //     return ['status' => false, 'errors' => $validationCode];
        // }

        return ['status' => true, 'errors' => $errors, 'voucher_rewards' => $voucher_rewards];
    }
}
