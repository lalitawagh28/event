<?php

namespace App\Http\Controllers\api;

use App\Models\User;
use App\Models\Order;
use App\Models\Booking;
use App\Models\Attendee;
use App\Service\ApiService;
use App\Models\OrderBooking;
use Illuminate\Http\Request;
use App\Models\AuthWalletToken;
use App\Models\TransferRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\TransferResource;
use App\Http\Resources\TransferCollection;
use App\Models\Event;
use App\Models\SensyLog;
use App\Notifications\TicketTransferAcceptNotification;
use App\Notifications\TicketTransferRequest;
use App\Service\WalletPayService;
use Laravel\Cashier\Exceptions\IncompletePayment;
use Stripe\Stripe;

class TransferController extends Controller
{
    private $apiService;

    public function __construct(ApiService $service)
    {
        $this->apiService = $service;
    }

    public function getWalletUser()
    {
        $walletUsers = AuthWalletToken::pluck('user_id');
        $users = User::whereIn('id', $walletUsers)->get();

        return response()->json(['data' => UserResource::collection($users)], 200);
    }

    public function InitiateTransferRequest(Request $request)
    {
        $bookingIds = explode(',', $request->input('booking_id'));

        if ($request->input('type') == 'free') {
            foreach ($bookingIds as $bookingId) {
                $transferRequest = TransferRequest::create([
                    'sender_id' => auth()->user()->id,
                    'receiver_id' => $request->input('user_id'),
                    'booking_id' => $bookingId,
                    'type' => $request->input('type'),
                    'status' => 0
                ]);
            }
        } else {
            foreach ($bookingIds as $bookingId) {
                $transferRequest = TransferRequest::create([
                    'sender_id' => auth()->user()->id,
                    'receiver_id' => $request->input('user_id'),
                    'booking_id' => $bookingId,
                    'type' => $request->input('type'),
                    'status' => 0
                ]);
            }
        }

        $receiverUser = User::find($request->input('user_id'));

        //$orderBooking = OrderBooking::whereOrderId($request->input('order_id'))->first();

        $booking = Booking::find($bookingIds[0]);

        $receiverUser->notify(new TicketTransferRequest($booking, auth()->user(), $bookingIds, $request->input('type')));

        $msg = 'Ticekt transfered successfully !!';

        return response()->json(['message' => $msg, 'data' => $transferRequest], 200);
    }

    public function getTransferRequest()
    {
        $requests = TransferRequest::where('receiver_id', auth()->user()->id)->get();
        return response()->json(['data' => $requests], 200);
    }

    public function AcceptRequest($booking_id)
    {
        $bookingIds = explode(',', $booking_id);
        $transferRequests = TransferRequest::whereIn('booking_id', $bookingIds)->where('status', 0)->get();

        if (!$transferRequests->isEmpty()) {
            $qty = 0;
            $price = 0;
            $promocode = 0;
            $netprice = 0;
            $tax = 0;
            $receiverId = '';
            $sender = NULL;

            foreach ($transferRequests as $transferRequest) {
                $receiverUser = User::find($transferRequest->receiver_id);
                $booking = Booking::where('id', $transferRequest->booking_id)->first();

                $qty += $booking->quantity;
                $price += $booking->price;
                $promocode += $booking->promocode_reward;
                $netprice += $booking->net_price;
                $tax += $booking->tax;

                $booking->update([
                    'customer_id' => $transferRequest->receiver_id,
                    'is_transferred' => 1,
                    'customer_name' => $receiverUser?->name,
                    'customer_email' => $receiverUser?->email,
                ]);

                $transferRequest->status = 1;
                $transferRequest->update(['status' => 1]);

                $receiverId = $transferRequest->receiver_id;
                $sender = User::find($transferRequest->sender_id);
            }

            $orderBooking = OrderBooking::where('booking_id', $booking->id)->first();
            $oldOrder = Order::find($orderBooking->order_id);

            $newOrder = Order::create([
                'event_id' => $oldOrder->event_id,
                'customer_id' => $receiverId,
                'quantity' => $qty,
                'price' => $price,
                'promocode_reward' => $promocode,
                'net_price' => $netprice,
                'tax' => $tax
            ]);

            if (($oldOrder->quantity - $qty) > 0) {
                $oldOrder->update([
                    'quantity' => $oldOrder->quantity - $qty,
                    'price' => $oldOrder->price - $price,
                    'promocode_reward' => $oldOrder->promocode_reward - $promocode,
                    'net_price' => $oldOrder->net_price - $netprice,
                    'tax' => $oldOrder->tax - $tax
                ]);
            } else {
                $oldOrder->delete();
            }

            foreach ($bookingIds as $bookingId) {
                OrderBooking::create(['booking_id' => $bookingId, 'order_id' => $newOrder->id]);
            }

            $event = Event::find($oldOrder->event_id);
            $sender->notify(new TicketTransferAcceptNotification($sender, $receiverUser, $event));

            $msg = 'Ticekt transfered request approved successfully !!';

            if ($transferRequests[0]->type == 'paid') {
                return $this->payByWorldPay($request, $booking);
            }
            return response()->json(['status' => true, 'message' => $msg]);
        } else {
            $msg = 'Ticekt transfered request approved already!!';

            return response()->json(['status' => true, 'message' => $msg]);
        }
    }

    public function DeclineRequest()
    {
        //
    }

    public function ticketTransferAcceptPayment($id, WalletPayService $service)
    {
        $bookingIds = explode(',', $id);
        $transferRequests = TransferRequest::whereIn('booking_id', $bookingIds)->where('status', 0)->get();

        $transferRequestalreadyPaid = TransferRequest::whereIn('booking_id', $bookingIds)->where('status', 1)->get();
        if ($transferRequests->isEmpty() && !is_null($transferRequestalreadyPaid)) {
            $msg = 'Ticket transfered request approved already !!';

            return response()->json(['message' => $msg]);
        } else {
            $qty = 0;
            $price = 0;
            if (!$transferRequests->isEmpty()) {
                $user = User::find($transferRequests[0]->receiver_id);
                Auth::login($user);

                foreach ($transferRequests as $transferRequest) {
                    $booking = Booking::where('id', $transferRequest->booking_id)->first();

                    $qty += $booking->quantity;
                    $price += $booking->price;
                }
            }
            if (!is_null(Auth::user())) {
                $walletsInfo = $this->getWallet($service, Auth::user());
                $wallets = collect($walletsInfo['data']['wallets']);
            } else {
                $wallets = [];
            }
        }
        return response()->json([$qty, $price, $booking, $wallets]);
    }

    public function getWallet($service, $user)
    {
        $wallets = $service->getWalletList($user);
        return $wallets;
    }

    public function ticketTransferStripePayment(Request $request, WalletPayService $service)
    {
        $this->set_payment_method($request, []);
        $this->stripe($request, $service);
    }

    public function ticketTransferWalletPayment(Request $request, WalletPayService $service)
    {
        $info = $request->all();
        $id = $request->id;

        $bookingIds = explode(',', $id);
        $transferRequest = TransferRequest::whereIn('booking_id', $bookingIds)->where('status', 0)->first();

        $senderDetails = AuthWalletToken::whereUserId($transferRequest['sender_id'])->first();
        $senderInfo = $service->profile($senderDetails['token']);

        $data = [
            'currency' => "GBP",
            'wallet' => $info['wallet'],
            'amount' => $info['price'],
            'service' => $info['event_title'],
            'sender_id' => $senderInfo['data']['id']
        ];

        $response = $service->ticketWalletPayment($data, auth()->user());

        if ($response['code'] == 200) {
            $this->ticketTransferAccept($request->id);
        } else {
            $msg = $response['message'];
            return response(['status' => false, 'message' => $msg]);
        }
    }

    protected function stripe($request, $service, $payment_data = [])
    {
        $info = $request->all();

        $bookingIds = explode(',', $request->id);
        $customer = Booking::where('id', $bookingIds[0])->first();
        $user = User::find($customer->customer_id);
        Auth::login($user);

        $event_title = 'xyz';
        $flag = [];

        try {
            //current user
            $user = Auth::user();

            if (!empty(Auth::user()->is_manager)) {
                $user = User::find(Auth::user()->organizer_id);
            }

            // create customer
            if (empty($user->stripe_id)) {
                $user->createAsStripeCustomer();
            }

            // extra params and it is optional
            $extra_params = [
                "currency" => "GBP",
                "description" => $event_title,
            ];

            // payment method
            $paymentMethod = $payment_data['setupIntent'];

            // add payment method
            //$user->addPaymentMethod($paymentMethod);

            // payment
            // amount
            $amount = $info['price'] * 100;
            $amount = (int) $amount;

            $stripe = $user->charge($amount, $paymentMethod, $extra_params);

            if ($stripe->status == 'succeeded') {

                // set data
                if ($stripe->charges['data'][0]->paid) {
                    $flag['status'] = true;
                    $flag['balance_transaction'] = $stripe->charges['data'][0]->balance_transaction;
                    $flag['payment_method'] = $stripe->charges['data'][0]->payment_method;
                    $flag['sender_payment_id'] = $stripe->charges['data'][0]->id;
                    $flag['sender_card_fingerprint'] = $stripe->charges['data'][0]->payment_method_details['card']['fingerprint'];                  // charge_id
                    $flag['message'] = $stripe->charges['data'][0]->outcome['seller_message']; // outcome message
                    $flag['stripe_fee'] = $stripe->charges['data'][0]->application_fee;
                    $flag['stripe_receipt_url'] = $stripe->charges['data'][0]->receipt_url;
                } else {
                    $flag['status'] = false;
                    $flag['error'] = $stripe->charges['data'][0]->failure_message;
                }
            } else {
                $flag = [
                    'status' => false,
                    'error' => $stripe->status,
                ];
            }
        }

        // Laravel Cashier Incomplete Exception Handling for 3D Secure / SCA -> 4000000000003220 error card number
        catch (IncompletePayment $ex) {

            $redirect_url = route(
                'cashier.payment',
                [$ex->payment->id, 'redirect' => route('after3DAuthentication', ['id' => $ex->payment->id])]
            );

            return response()->json(['url' => $redirect_url, 'status' => true]);
        }

        // All Exception Handling like error card number
        catch (\Exception $ex) {
            // fail case
            $flag = [
                'status' => false,
                'error' => $ex->getMessage(),
            ];
        }

        if ($flag['status'] == true) {

            $transferRequest = TransferRequest::whereIn('booking_id', $bookingIds)->where('status', 0)->first();
            $senderDetails = AuthWalletToken::whereUserId($transferRequest['sender_id'])->first();
            $senderInfo = $service->profile($senderDetails['token']);

            $flag['currency'] = 'GBP';
            $flag['amount'] = $info['price'];
            $flag['service'] = $info['event_title'];
            $flag['sender_id'] = $senderInfo['data']['id'];

            $response = $service->ticketStripePayment($flag, auth()->user());

            if ($response['code'] == 200) {
                $this->ticketTransferAccept($request->id);
            } else {
                $msg = $response['message'];

                return response(['status' => false, 'message' => $msg]);
            }
        }
    }

    protected function set_payment_method(Request $request, $booking = [])
    {
        $data = $request->validated();

        $bookingIds = explode(',', $request->id);
        $customer = Booking::where('id', $bookingIds[0])->first();
        $user = User::find($customer->customer_id);

        $total_price = $request->amount;

        if ($request->input('payment_method') == 2) {

            if (empty(setting('apps.stripe_secret_key'))) {
                return response()->json(['message' => 'Payment method is not activated'], 400);
            }
            // Set your secret key
            Stripe::setApiKey(setting('apps.stripe_secret_key'));

            // Get the amount and currency from the request
            // dd($amount = (int)$total_price);
            $amount = (int)$total_price * 100;
            $currency = 'GBP';

            $stripe = new \Stripe\StripeClient(setting('apps.stripe_secret_key'));
            // Use an existing Customer ID if this is a returning customer.
            $customer = $stripe->customers->create();
            $ephemeralKey = $stripe->ephemeralKeys->create([
                'customer' => $customer->id,
            ], [
                'stripe_version' => '2022-08-01',
            ]);
            $paymentIntent = $stripe->paymentIntents->create([
                'amount' => $amount,
                'currency' => $currency,
                'customer' => $customer->id,
                'automatic_payment_methods' => [
                    'enabled' => 'true',
                ],
            ]);

            $stripeData = [
                'ephemeralKey' => $ephemeralKey->secret,
                'customer' => $customer->id,
                "paymentIntent" => $paymentIntent->client_secret,
                "amount" => $paymentIntent->amount,
                "stripe_publishable_key" => setting('apps.stripe_public_key'),
                "stripe_intent_data" => $paymentIntent
            ];

            $responseData = [
                'stripeData' => $stripeData,
                'booking' => $booking,
                'customerData' => $data,
            ];

            return response()->json(['message' => 'Please complete the payment', 'data' => $responseData], 200);
        }

        if ($request->input('payment_method') == 11) {

            $payment_method = [
                'payment_method' => $request->payment_method ?? null,
                'setupIntent' => $data->setupIntent ?? null,
                'customer_email' => $booking[key($booking)]['customer_email'] ?? null,
                'customer_name' => $booking[key($booking)]['customer_name'] ?? null,
                'event_title' => $booking[key($booking)]['event_title'] ?? null,
                'currency' => $booking[key($booking)]['currency'] ?? null,
                'cardNumber' => $request->cardNumber ?? null,
                'cardMonth' => $request->cardMonth ?? null,
                'cardYear' => $request->cardYear ?? null,
                'cvc' => $request->cardCvv ?? null,
                'cardName' => $request->cardName ?? null,
            ];

            $cardName = explode(' ', trim($request->cardName));

            // except last
            $payment_method['firstName'] = '';
            foreach ($cardName as $key => $val) {
                if (!end($cardName) === $val) {
                    $payment_method['firstName'] .= $val . ' ';
                }
            }
            // remove last space
            $payment_method['firstName'] = trim($payment_method['firstName']);

            // the last word
            $payment_method['lastName'] = end($cardName);

            return $payment_method;
        }
    }

    public function getTransferTicket()
    {
        $user = Auth::user();
        $userId = $user->id;
        $request = TransferRequest::where(function ($query) use ($userId) {
            $query->where('sender_id', $userId)->orWhere('receiver_id', $userId);
        })->get();
        return response()->json(['data' => TransferResource::collection($request)], 200);
    }

    protected function payByWorldPay(Request $request, $booking = [])
    {
        if ($request->input('payment_method') == 11) {

            $currency = !empty($booking[key($booking)]['currency']) ? $booking[key($booking)]['currency'] : setting('regional.currency_default');

            // add all info into session
            $order = [
                'item_sku' => $booking[key($booking)]['item_sku'],
                'order_number' => time() . rand(1, 988),
                'title' => $booking[key($booking)]['event_title'],
                'wallet' => @$booking[key($booking)]['wallet'],
                'currency' => @$booking[key($booking)]['currency']
            ];

            $total_price = 0;

            foreach ($booking as $key => $val) {
                $total_price += $val['net_price'];
            }

            // calculate total price
            $order['price'] = $total_price;

            $payment_method_data = $this->set_payment_method($request, $booking);

            $sessionData = [
                "booking" => $booking,
                "payment_method" => $payment_method_data,
                "pre_payment" => $order,
                "user_id" => auth()->user()->id,
                "type" => "paid_transfer",
                "transfer_status" => "pending",
                "booking_id" => $booking['id']
            ];

            $sensyLog = new SensyLog();
            $sensyLog->cartId = $order['order_number'];
            $sensyLog->meta = json_encode($sessionData);
            $sensyLog->save();

            if (!empty(setting('apps.worldpay_url'))) {
                $url = setting('apps.worldpay_url');
            } else {
                $url = config('worldpay.worldpay_url');
            }

            $params = [
                'instId' => setting('apps.worldpay_instid'),
                'cartId' => $order['order_number'],
                'amount' => $total_price,
                'testMode' => setting('apps.worldpay_mode'),
                'currency' => 'GBP',
                'M_' => 'TransferTicket',
                'resultfile' => 'resultY.html'
            ];

            // Construct the URL with query parameters
            $worldpayUrl = $url . '?' . http_build_query($params);

            return response()->json([
                'status' => true,
                'return_url' => $worldpayUrl,
                'sessionData' => $sessionData
            ], 200);
        }
    }
}
