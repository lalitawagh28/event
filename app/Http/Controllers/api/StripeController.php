<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Stripe\Stripe;

class StripeController extends Controller
{

    public function createPaymentIntent(Request $request)
    {
        $validated = $request->validate([
            'amount' => 'required',
            'currency' => 'required',
        ]);

        if (empty(setting('apps.stripe_secret_key'))) {
            return response()->json(['message' => 'Payment method is not activated'], Response::HTTP_BAD_REQUEST);
        }
        // Set your secret key
        Stripe::setApiKey(setting('apps.stripe_secret_key'));

        // Get the amount and currency from the request
        $amount = $request->input('amount');
        $currency = $request->input('currency');

        // Create a PaymentIntent with the amount and currency
        $paymentIntent = \Stripe\PaymentIntent::create([
            'amount' => $amount,
            'currency' => $currency,
        ]);


        $stripeData = [
            "client_secret" => $paymentIntent->client_secret,
            "paymentIntent" => $paymentIntent->id,
            "amount" => $paymentIntent->amount,
            "stripe_publishable_key" => setting('apps.stripe_public_key'),
            "stripe_payment_intent" => $paymentIntent
        ];


        // Return the client secret to the frontend
        return response()->json(['stripe' => $stripeData]);
    }
}
