<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\AgentTicket;
use App\Models\EventDeliveryCharge;
use App\Models\SensyLog;
use App\Models\Ticket;
use App\Models\User;
use Classiebit\Eventmie\Models\Tax;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class SensyController extends Controller
{
    private $ticket;
    private $tax;
    private $agent_tickets;
    public function __construct()
    {
        $this->ticket       = new Ticket();
        $this->tax          = new Tax();
        $this->agent_tickets = new AgentTicket();
    }

    public function search(Request $request)
    {
        $postcode = $request->query('postcode');

        $format = [
            'type' => 'boolean',
            'required' => false,
            'default' => true,
        ];

        $sort = [
            'type' => 'boolean',
            'required' => false,
            'default' => true,
        ];

        $data = Http::get("https://api.getAddress.io/suggest/" . $postcode, array("api-key" => config('services.getaddress_api_key'), "top" => 2000, "all" => true, $format, $sort));
        $data = $data->json();

        $addresses = [];

        if (isset($data['suggestions'])) {
            $suggestions = collect($data['suggestions']);

            $addresses_found = Http::pool(fn (Pool $pool) =>
            $suggestions->map(
                fn ($search_id) => $pool->get("https://api.getAddress.io/get/" . $search_id['id'] . "?api-key=" . config('services.getaddress_api_key'))
            ));

            foreach ($addresses_found as $key => $address_found) {
                $address = $address_found->object();
                $addresses[$key]['postcode'] = $address->postcode;
                $addresses[$key]['house_no'] = $address->building_number;
                $addresses[$key]['street'] = substr($address->thoroughfare, 0, 20);
                $addresses[$key]['address_info'] = $address->line_3;
                $addresses[$key]['city'] = $address->town_or_city;
                $addresses[$key]['county'] = $address->county;
            }

            // Use array_column to extract the 'street' values
            $streets = array_column($addresses, 'street');

            // Use array_filter to filter unique values
            $uniqueStreets = array_unique(array_filter($streets));

            // If you want to keep the original structure with all addresses:
            // $filteredAddresses = array_filter($addresses, function ($address) use ($uniqueStreets) {
            //     return in_array($address['street'], $uniqueStreets);
            // });

            // If you want to keep only the addresses with unique streets:
            $filteredAddresses = array_values(array_intersect_key($addresses, array_unique($streets)));

            return response(['data' => $filteredAddresses]);
        }

        return response(['data' => $addresses]);
    }

    public function initiateCheckout(Request $request)
    {
        try {
            // Extracting input data
            $inputData = $request->all();
            $data = json_decode($inputData['cartData']);
            $data2 = json_decode($data);

            $fullAddress = $inputData['house_no'] . ',' . $inputData['street']  . ',' . $inputData['city']  . ',' . $inputData['county']  . ',' . $inputData['postcode'];
            $userMobileNumber = $inputData['mobile'] ?? '';
            $username = $inputData['name'] ?? '';
            $userMail = $inputData['mail'] ?? '';
            $userAddress = $fullAddress ?? '';
            $voucherCode = $inputData['voucher_code'] ?? '';
            $voucherReward = $inputData['voucher_reward'] ?? '';
            $random = rand(1000, 3000);
            $cartId = $data2->catalog_id . $userMobileNumber . $random;
            $totalQuantity = 0;
            $ticketIds = [];
            $ticketQty = [];
            $netPrice = 0;
            $taxes = 0;

            // Fetching ticket information
            $ticket = Ticket::where('ticket_retailer_id', $data2->product_items[0]->product_retailer_id)->first();

            // Checking if the user already exists
            $user = User::where('email',  $userMail)->first();

            // Creating a new user if not exists
            $user = User::firstOrCreate(
                ['email' => $userMail],
                ['password' => Hash::make(123456), 'name' => $username, 'role_id' => 2, 'type' => 'general']
            );

            //Get delivery Charge
            $eventDeliveryCharge = EventDeliveryCharge::where('event_id', $ticket->event_id)->first();
            $deliveryCharge = (!is_null($eventDeliveryCharge->rate)) ? $eventDeliveryCharge->rate : 0;
            // Calculating total quantity and processing fee
            foreach ($data2->product_items as $key => $item) {
                $totalQuantity += $item->quantity;
                $ticketIds[] = $item->product_retailer_id;
                $ticketQty[] = $item->quantity;
            }
            foreach ($ticketIds as $key => $value) {
                $params   = [
                    'ticket_id'         => $value,
                    'quantity'          => $ticketQty[$key],
                    'is_donation'       => '',
                ];
                $price = $this->calculate_price($params);
                $netPrice += $price['net_price'];
                $taxes += $price['tax'];
            }

            $grossAmount = $netPrice - $voucherReward + $deliveryCharge;


            // Constructing transaction data
            $transactionData = [
                'quantity' => $totalQuantity,
                'fee' => $taxes,
                'amount' => $grossAmount,
                'cartId' => $cartId,
                'catalog_id' => $data2->catalog_id,
                'currency' => 'GBP',
                'name' => $username,
                'mobile' => $userMobileNumber,
                'address' => $userAddress,
                'user' => $user->id,
                'event_id' => $ticket->event_id,
                'product_items' => $data2->product_items,
                'voucher_code'  => $voucherCode,
                'delivery_charge_id' => $eventDeliveryCharge->id,
            ];

            // dd($grossAmount, $transactionData);

            // Logging transaction data
            $sensyLog = new SensyLog();
            $sensyLog->cartId = $cartId;
            $sensyLog->meta = json_encode($transactionData);
            $sensyLog->save();

            // Handling Worldpay URL
            if (!empty(setting('apps.worldpay_url'))) {
                $url = setting('apps.worldpay_url');
            } else {
                $url = config('worldpay.worldpay_url');
            }

            $params = [
                'instId' => setting('apps.worldpay_instid'),
                'cartId' => $cartId,
                'amount' => $grossAmount,
                'testMode' => setting('apps.worldpay_mode'),
                'currency' => 'GBP',
                'M_'       => 'AiSensyBooking',
                'resultfile' => 'resultY.html'
            ];

            $worldpayUrl =  $url . '?' . http_build_query($params);

            // Handling AISensy URL
            $aiSensyUrl = '?' . http_build_query($params);

            // Sending data to AISensy
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
            ];

            $templateMobile = '+' . $userMobileNumber;

            $data = [
                "apiKey" => config('aisensy.apiKey'),
                "campaignName" =>  config('aisensy.campaign'),
                "destination" => "$templateMobile",
                "userName" => "$username",
                "templateParams" => [
                    "$aiSensyUrl",
                ],
            ];
            // dd($data,$userMobileNumber);

            $response = $client->post('https://backend.aisensy.com/campaign/t1/api/v2', [
                'headers' => $headers,
                'json' => $data,
            ]);

            // Get the response body as a string
            $body = $response->getBody()->getContents();

            // Returning the response
            return response(["url" => $worldpayUrl, "data" => $transactionData]);
        } catch (\Exception $e) {
            // Handle exceptions here, log them, and return an appropriate response
            return response(['error' => $e->getMessage()], 500);
        }
    }

    public function getSensyLog(Request $request)
    {
        $sensyLog = SensyLog::orderBy('id', 'desc')->get();
        return response()->json(['data' => $sensyLog]);
    }

    public function getStatus(Request $request)
    {
        $request->validate(['cartId' => 'required']);

        $cartId = $request->query('cartId');
        $sensLogExists = SensyLog::where('cartId', $cartId)->first();
        $status = '';
        if (!is_null($sensLogExists)) {
            if ($sensLogExists->payment_success == 1) {
                $status = 'Success';
                return response()->json(['message' => 'Booking Successfully Completed', 'data' => $cartId, 'status' => $status], 200);
            } else {
                $status = 'Failed';
            }
            return response()->json(['data' => $cartId, 'status' => $status, 'message' => 'Checking']);
        }
    }
    public function applyVoucher(Request $request)
    {
        $inputData = $request->all();
        $data = json_decode($inputData['cartData']);
        $data2 = json_decode($data);
        $ticketIds = [];
        $quantity = [];
        foreach ($data2->product_items as $item) {
            $ticketIds[] = $item->product_retailer_id;
            $quantity[] = $item->quantity;
        }

        $type[] = 'general';
        if ($inputData['type'] == 'student') {
            $type[] = 'student';
        }
        $errors = [];
        $error = '';
        $voucher_code = $inputData['voucher_code'];
        $amount = $inputData['amount'] ?? 0;
        $voucher_rewards = 0;

        foreach ($ticketIds as  $key => $retail_id) {
            try {
                $qty = $quantity[$key];
                $ticket = Ticket::where('ticket_retailer_id', $retail_id)->firstOrFail();
                $id = $ticket->id;
                $price = $ticket->price;
                $check_vouchercode  = AgentTicket::where('ticket_id', $id)->whereIn('type', $type)->where('coupon_code', $voucher_code)->orderBy('type', 'desc')->first();
                if ($check_vouchercode->quantity < $qty) {
                    $sold_out = $qty - $check_vouchercode->quantity;
                    $errors[] = "Ticket : " . $ticket->title . " " . " Qty = " . $sold_out . " Sold Out";
                    $qty  = $qty - $sold_out;
                }
                if ($qty > 0) {
                    $validDate = strtotime($check_vouchercode->valid_to);
                    $todayDate = strtotime(date("Y-m-d"));
                    if (($validDate < $todayDate) && ($validDate != $todayDate)) {
                        $errors[] = "Ticket : " . $ticket->title . " " . " Voucher code Expired ";
                    } else {
                        // manual check in promocode_user if promocode not already applied by the user
                        $already_used =  $check_vouchercode->max_uses;
                        if ($already_used > $qty) {
                            if ($check_vouchercode->code_value == 'percent') {
                                $voucher_rewards +=  $qty * $price * ($check_vouchercode->discount / 100);
                            } else {
                                $voucher_rewards += $qty * $check_vouchercode->discount;
                            }
                        } else {
                            $qty = $already_used;
                            if ($check_vouchercode->code_value == 'percent') {
                                $voucher_rewards +=  $already_used * $price * ($check_vouchercode->discount / 100);
                            } else {
                                $voucher_rewards += $already_used * $check_vouchercode->discount;
                            }
                        }
                        $error = '';
                    }
                }
            } catch (\Throwable $e) {
                $flag = [
                    'status' => false,
                    'data' => [
                        'message' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'code' => $e->getCode(),
                    ]
                ];

                Log::info($e->getMessage());

                $error = "Invalid Voucher Code";
            }
        }

        // dd($errors, $error,$voucher_rewards);
        if ($voucher_rewards == 0 && $error || count($errors)) {
            return response()->json(['status' => Response::HTTP_BAD_REQUEST, 'message' => 'Invalid Voucher Code'], Response::HTTP_BAD_REQUEST);
        }

        $total = $amount - $voucher_rewards;
        return response()->json(['status' => Response::HTTP_OK, 'voucher_reward' => $voucher_rewards, 'total' => $total, 'message' => 'Voucher applied'], Response::HTTP_OK);
    }
    public function validateVoucherCode(Request $request)
    {
        $inputData = $request->all();
        $data = json_decode($inputData['cartData']);
        $data2 = json_decode($data);
        $ticketIds = [];
        $quantity = [];
        foreach ($data2->product_items as $item) {
            $ticketIds[] = $item->product_retailer_id;
            $quantity[] = $item->quantity;
        }
        $type[] = 'general';
        if ($inputData['type'] == 'student') {
            $type[] = 'student';
        }
        $errors = [];
        $error = '';
        $voucher_code = $inputData['voucher_code'];
        $amount = $inputData['amount'] ?? 0;
        $voucher_rewards = 0;
        $sub_cat_title = '';


        foreach ($ticketIds as  $key => $retail_id) {
            try {
                $qty = $quantity[$key];
                $ticket = Ticket::where('ticket_retailer_id', $retail_id)->firstOrFail();
                $id = $ticket->id;
                $price = $ticket->price;
                $i = null;

                $check_vouchercode  = AgentTicket::where('ticket_id', $id)->whereIn('type', $type)->where('coupon_code', $voucher_code)->orderBy('type', 'desc')->firstOrFail();

                $book_count = $check_vouchercode->bookings?->count();
                if (($check_vouchercode->quantity - $book_count) < $qty) {
                    $sold_out = $qty - $check_vouchercode->quantity + $book_count;
                    $qty  = $qty - $sold_out;
                    if ($qty < 1) {
                        $errors[] = "Ticket : " . $ticket->title . " " . $sub_cat_title . " Qty = " . $sold_out . " Sold Out";
                    }
                    if ($qty > 0) {
                        $errors[] = "Ticket : " . $ticket->title . " " . $sub_cat_title . " Qty = " . $qty . " only applied";
                    }
                }
                if ($qty > 0) {
                    $validDate = strtotime($check_vouchercode->valid_to);
                    $todayDate = strtotime(date("Y-m-d"));
                    if (($validDate < $todayDate) && ($validDate != $todayDate)) {
                        $errors[] = "Ticket : " . $ticket->title . " " . $sub_cat_title . " Voucher code Expired ";
                    } else {
                        $user_promocode = [];
                        // manual check in promocode_user if promocode not already applied by the user
                        $params = [
                            'user_id'   => auth()->user()->id,
                            'agent_ticket_id' => $check_vouchercode->id,
                            'ticket_id' => $id,
                            'sub_category_id' => $i > 0 ?? null,
                        ];
                        $user_promocode = $this->agent_tickets->agentcode_user($params);
                        // dd(true);
                        $already_used =  $check_vouchercode->max_uses - $user_promocode;
                        if ($already_used <  1) {
                            $errors[] = "Ticket : " . $ticket->title . " " . $sub_cat_title . "Qty= " . $already_used . " Voucher Code already Used ";
                        }
                        if ($already_used > $qty) {
                            if ($check_vouchercode->code_value == 'percent') {
                                $voucher_rewards +=  $qty * $price * ($check_vouchercode->discount / 100);
                            } else {
                                $voucher_rewards += $qty * $check_vouchercode->discount;
                            }
                        } else {
                            $qty = $already_used;
                            if ($check_vouchercode->code_value == 'percent') {
                                $voucher_rewards +=  $already_used * $price * ($check_vouchercode->discount / 100);
                            } else {
                                $voucher_rewards += $already_used * $check_vouchercode->discount;
                            }
                        }
                        $error = '';
                    }
                }
            } catch (\Throwable $e) {
                $error = "Invalid Voucher Code";
            }
        }

        // dd($error,$errors);
        if ($voucher_rewards == 0 && $error || count($errors)) {
            return response()->json(['status' => Response::HTTP_BAD_REQUEST, 'message' => 'Invalid Voucher Code'], Response::HTTP_BAD_REQUEST);
        }

        $total = $amount - $voucher_rewards;
        return response()->json(['status' => Response::HTTP_OK, 'voucher_reward' => $voucher_rewards, 'total' => $total, 'message' => 'Voucher applied'], Response::HTTP_OK);
    }



    public function calculate_price($params)
    {
        $ticket = Ticket::where('ticket_retailer_id', $params['ticket_id'])->firstOrFail();
        //CUSTOM
        //set sale price
        if (!empty($ticket)) {
            if (!empty($ticket->sale_start_date)) {
                if ($ticket->sale_price > 0 && $ticket->sale_start_date <= Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString() && $ticket->sale_end_date > Carbon::now()->timezone(setting('regional.timezone_default'))->toDateTimeString()) {
                    $ticket->price = $ticket->sale_price;
                }
            }
        }
        //CUSTOM

        // apply admin tax
        $ticket   = $this->admin_tax($ticket);

        $net_price      = [];
        $amount         = 0;
        $tax            = 0;
        $excluding_tax  = 0;
        $including_tax  = 0;

        $amount  = $ticket['price'] * $params['quantity'];

        //CUSTOM
        if (!empty($ticket['is_donation'])) {
            $amount  = $params['is_donation'] * 1;
        }
        //CUSTOM

        $net_price['tax']               = $tax;
        $net_price['net_price']         = $tax + $amount;

        // organiser_price = net_price excluding admin_tax
        $net_price['organiser_price']   = $tax + $amount;
        $excluding_tax_organiser        = 0;
        $including_tax_organiser        = 0;
        $admin_tax                      = 0;

        // calculate multiple taxes on ticket
        if ($ticket['taxes']->isNotEmpty() && $amount > 0) {
            foreach ($ticket['taxes'] as $tax_k => $tax_v) {
                //if have no taxes then return net_price
                if (empty($tax_v->rate_type))
                    return $net_price;

                // in case of percentage
                if ($tax_v->rate_type == 'percent') {
                    $tax     = (float) ($amount * $tax_v->rate) / 100;

                    // in case of including
                    if ($tax_v->net_price == 'including') {
                        $including_tax       = $tax + $including_tax;

                        // exclude admin tax
                        if (!$tax_v->admin_tax)
                            $including_tax_organiser  = $tax + $including_tax_organiser;

                        //admin tax
                        if ($tax_v->admin_tax)
                            $admin_tax = $admin_tax + $tax;
                    }

                    // in case of excluding
                    if ($tax_v->net_price == 'excluding') {
                        $excluding_tax       = $tax + $excluding_tax;

                        // exclude admin tax
                        if (!$tax_v->admin_tax)
                            $excluding_tax_organiser  = $tax + $excluding_tax_organiser;

                        //admin tax
                        if ($tax_v->admin_tax)
                            $admin_tax = $admin_tax + $tax;
                    }
                }

                //  in case of fixed
                if ($tax_v->rate_type == 'fixed') {
                    $tax                     = (float) ($params['quantity'] * $tax_v->rate);

                    // // in case of including
                    if ($tax_v->net_price == 'including') {
                        $including_tax = $tax + $including_tax;

                        // exclude admin tax
                        if (!$tax_v->admin_tax)
                            $including_tax_organiser  = $tax + $including_tax_organiser;

                        //admin tax
                        if ($tax_v->admin_tax)
                            $admin_tax = $admin_tax + $tax;
                    }

                    // // in case of excluding
                    if ($tax_v->net_price == 'excluding') {
                        $excluding_tax   = $tax + $excluding_tax;

                        // exclude admin tax
                        if (!$tax_v->admin_tax)
                            $excluding_tax_organiser  = $tax + $excluding_tax_organiser;

                        //admin tax
                        if ($tax_v->admin_tax)
                            $admin_tax = $admin_tax + $tax;
                    }
                }
            }
        }

        $net_price['tax']               = (float) ($excluding_tax + $including_tax);
        $net_price['net_price']         = (float) ($amount + $excluding_tax);

        // organiser_price excluding admin_tax
        $net_price['organiser_price']   = (float) ($amount + $excluding_tax_organiser);

        //admin tax
        $net_price['admin_tax']         = (float) ($admin_tax);

        return $net_price;
    }
    protected function admin_tax($tickets = [])
    {
        // get admin taxes
        $admin_tax  = $this->tax->get_admin_taxes();

        // if admin taxes are not existed then return
        if ($admin_tax->isEmpty())
            return $tickets;

        // it work when tickets show for purchasing
        // for multiple tickets
        if ($tickets instanceof \Illuminate\Database\Eloquent\Collection) {
            // push admin taxes in every tickets
            foreach ($tickets as $key => $value) {
                foreach ($admin_tax as $ad_k => $ad_v) {
                    $value->taxes->push($ad_v);
                }
            }
        } else {
            // it work when booking data prepare
            // for single ticket
            foreach ($admin_tax as $ad_k => $ad_v) {
                $tickets['taxes'] = $tickets['taxes']->push($ad_v);
            }
        }

        return $tickets;
    }
}
