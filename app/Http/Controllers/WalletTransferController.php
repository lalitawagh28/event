<?php

namespace App\Http\Controllers;

use App\Models\AuthWalletToken;
use App\Service\WalletPayService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WalletTransferController extends Controller
{
    protected $walletPayService;

    public function __construct(WalletPayService $walletPayService)
    {
        $this->walletPayService = $walletPayService;
        // dd( $this->walletPayService );
    }
    public function transferData()
    {
        // $data =
          $user = Auth::user();
          $userName =$user->name;
          $pNumber = $user->phone;
            //  dd( $user);

            $walletList = $this->walletPayService->getWalletList($user);
            //    dd($walletList);
            // dd($walletList['data']['workspaceId']);

        return view('walletPayments.transfer', compact('walletList','userName','pNumber'));

    }
    public function transferGetExchangeRate(Request $request)
    {
        $user = Auth::user();

        $data = [
             'from_id' => $request->input('currency'),
        // 'from_id' => 1,//walletId
             'to_id' => $request->input('transfer_to'),
        // 'to_id' =>1149, //ledger id
            'amount' => $request->input('amount_to_pay'),
            'transaction_type' => "deposit",
        ];
        $walletList = $this->walletPayService->getWalletList($user);
        $currencyName = $data['to_id'];
        $walletId = 0;
       foreach ($walletList['data']['wallets'] as $wallet) {

           if ($wallet['ledger']['id'] == $currencyName) {
               $walletId = $wallet['id'];
               break;
           }
       }
       $data['to_id'] = $walletId;

        $response =  $this->walletPayService->getExchangeRate($data, $user);
        // dd($response);
        return $response;
    }

    public function storeData(Request $request)
    {

        // $validator = Validator::make($request->all(), [
        //     'currency' => 'required',
        //     'balance' => 'required|numeric',
        //     'mobile' => 'required',
        //     'remaining_balance' => 'required|numeric|min:0',
        //     'Reference' => 'required',
        //     'amount_to_pay' => 'required|numeric',
        // ]);
        // if ($validator->fails()) {
        //     return redirect()->back()->withErrors($validator)->withInput();
        // }

        $user = Auth::user();
        $walletList = $this->walletPayService->getWalletList($user);
        //   dd($walletList );

        $data = [
            // 'wallet' => 4273,
            'sendercurrency' => $request->input('currency'),
            // 'sendercurrency' => $request['ledger']['id'],
            'balance' => $request->input('balance'),
            'beneficiary' => $request->input('mobile'),
            // 'beneficiary' => 7999999999,
            'country_code' => '44',
            'remaining_amount' =>$request->input('remaining_balance'),
            // 'receiver_currency'=>'2',
             'receiver_currency' => $request->input('transfer_to'),
            'reference'=>$request->input('Reference'),
            'amount'=>$request->input('amount_to_pay'),
            'note'=>$request->input('Note'),
            'workspace_id'   => $walletList['data']['workspaceId'],
            // 'wallet'=>
        ];
    //  dd($data);
       $currencyName = $data['sendercurrency'];
         $walletId = 0;
        foreach ($walletList['data']['wallets'] as $wallet) {

            if ($wallet['ledger']['id'] == $currencyName) {
                $walletId = $wallet['id'];
                break;
            }
        }
        $data['wallet'] = $walletId;
        $recivercurrency = $data['receiver_currency'];
        $reciverwallet = 0;
        foreach ($walletList['data']['wallets'] as $wallet) {

            if ($wallet['ledger']['id'] == $recivercurrency) {
                $reciverwallet = $wallet['id'];
                break;
            }
        }
        // dd( $walletId);
        $data['receiver_currency'] =  $reciverwallet;

        //   dd( $data);
        $response =  $this->walletPayService->sendTransferPaymentData( $data,$user);
        // $response['status'] = 'success';
        // dd($response);
        return  $response;
            //   return  $response;  // view('walletPayments.transferOtpverify');
    }

   public function verifyotp(Request $request)
{
    $user = Auth::user();

    // Retrieve OTP and transaction ID from the request
    $code = $request->input('otpValue');
    $transaction_Id = $request->input('transactionId');
    // dd($data);
    // dd($transactionId);

    $response =  $this->walletPayService->verifyotp($code, $transaction_Id ,$user);

    return $response;

}
    public function resendOtp(Request $request)
    {
        $user = Auth::user();
        $transaction_Id = $request->input('transactionId');
        $response =  $this->walletPayService->resendOtp($transaction_Id ,$user);

        return  $response;

    }

}
