<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use App\Models\Ticket;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\Http\Requests\CouponValidationRequest;
use App\Models\AgentTicket;
use App\Models\Event;
use App\Models\User;

class DesignController extends Controller
{
    public function index()
    {
        $agent_tickets = AgentTicket::whereIn('id',[1343,1342,1341,1340])->get();
        $user = User::find(1087);
        $event = Event::find(339);
        return view('email_templates.agent_ticket_commission', ['event' => $event,'user' => $user,'agent_tickets' => $agent_tickets]);
    }
    public function mobileVerification()
    {
        return view('registration.mobile-verification');
    }
    public function emailVerification()
    {
        return view('registration.email-verification');
    }
    public function signup()
    {
        return view('registration.signup');
    }
    public function organiserEvent()
    {
        return view('registration.organiser-event');
    }
    public function eventForm()
    {
        return view('registration.event-form');
    }
    public function eventList()
    {
        return view('registration.event-list');
    }

    public function ticketsBooking()
    {
        return view('registration.tickets-booking');
    }

    public function profile()
    {
        return view('registration.profile');
    }

    public function eventDetail()
    {
        return view('registration.event-details');
    }

    public function depositForm()
    {
        return view('registration.deposit-form');
    }
    public function depositForm1()
    {
        return view('registration.deposit_form1');
    }
    public function depositVerification()
    {
        return view('registration.deposit_verification');
    }
    public function depositCardDetails()
    {
        return view('registration.deposit_card_details');
    }
    public function paymentSuccess()
    {
        return view('registration.payment_success');
    }

    public function login()
    {
        return view('registration.event-login');
    }

    public function registration()
    {
        return view('registration.event-registration');
    }
    public function faqMobile()
    {
        return view('registration.faq_mobile');
    }
    public function termsMobile()
    {
        return view('registration.terms_mobile');
    }
    public function privacyMobile()
    {
        return view('registration.privacy_mobile');
    }
    public function payments()
    {
        return view('registration.payments');
    }
    public function payoutsAcCreate()
    {
        return view('registration.payouts-ac-create');
    }
    public function transferAcCreate()
    {
        return view('registration.transfer-ac-create');
    }
    public function withdrawAcCreate()
    {
        return view('registration.withdraw-ac-create');
    }


}
