<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Eventmie\BookingsController;
use App\Models\Event;
use App\Http\Controllers\api\BookingController as ApiBookingController;
use App\Models\SensyLog;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class WorldPayController extends Controller
{
    public function showResponse(Request $request)
    {
        $booking                = session('booking');
        $event_promocode        = session('event_promocode');
        $delivery_address        = session('delivery_address');
        $payment_method        = session('payment_method');
        $address_type             = session('address_type');
        $collection_point       = session('collection_point');
        session('booking', $booking);
        session('event_promocode', $event_promocode);
        session('delivery_address', $delivery_address);
        session('payment_methods', $payment_method);
        session('address_type', $address_type);
        session('collection_point', $collection_point);

        $sessionData = [
            "selected_attendees" => session('selected_attendees'),
            "commission" => session('commission'),
            "booking" => session('booking'),
            "event_promocode" => session('event_promocode'),
            "delivery_address" => session('delivery_address'),
            "address_type" => session('address_type'),
            "collection_point" => session('collection_point'),
            "inputData" => session('inputData'),
            "payment_method" => session('payment_method'),
            "pre_payment" => session('pre_payment'),
            "user_id" => auth()->user()->id
        ];

        $cartId = $request->query('cartId');
        $amount = $request->query('amount');


        $sensyLog = new SensyLog();
        $sensyLog->cartId = $cartId;
        $sensyLog->meta = json_encode($sessionData);
        $sensyLog->save();


        if (!empty(setting('apps.worldpay_url'))) {
            $url = setting('apps.worldpay_url');
        } else {
            $url = config('worldpay.worldpay_url');
        }

        $params = [
            'instId' => setting('apps.worldpay_instid'),
            'cartId' => $cartId,
            'amount' => $amount,
            'testMode' => setting('apps.worldpay_mode'),
            'currency' => 'GBP',
            'M_'       => 'WebBooking',
            'resultfile' => 'resultY.html'
        ];
        // Construct the URL with query parameters
        $worldpayUrl = $url . '?' . http_build_query($params);


        return Redirect::away($worldpayUrl);
    }

    public function finish_booking_worldpay(Request $request)
    { //dd(explode('&',$request->getContent()));

        // $data = explode('&', $request->getContent());

        // $finalArray = array();
        $finalArray = $request->all();

        // foreach ($data as $dataval) {

        //     $term = explode('=', $dataval);

        //     $finalArray[$term[0]] = $term[1];
        // }

        //dd($finalArray);

        //dd(explode('=',$data[36])[1]);

        // $sessionId = $finalArray['M_'];

        //explode('=',$data[4])[1]

        //explode('=',$data[4])[1]
        // Session::setId($sessionId);

        Session::start();

        $sensyLogExists = SensyLog::where('cartId', $finalArray['cartId'])->first();
        $metaArray = json_decode($sensyLogExists->meta, true);

        $selected_attendees = $metaArray['selected_attendees'];
        $commission = $metaArray['commission'];
        $booking = $metaArray['booking'];
        $event_promocode = $metaArray['event_promocode'];
        $delivery_address = $metaArray['delivery_address'];
        $address_type = $metaArray['address_type'];
        $collection_point = $metaArray['collection_point'];
        $inputData = $metaArray['inputData'];
        $payment_method = $metaArray['payment_method'];
        $pre_payment = $metaArray['pre_payment'];

        session()->put('selected_attendees', $selected_attendees);
        session()->put('commission', $commission);
        session()->put('booking', $booking);
        session()->put('event_promocode', $event_promocode);
        session()->put('delivery_address', $delivery_address);
        session()->put('address_type', $address_type);
        session()->put('collection_point', $collection_point);
        session()->put('inputData', $inputData);
        session()->put('payment_method', $payment_method);
        session()->put('pre_payment', $pre_payment);

        Auth::loginUsingId($metaArray['user_id']);

        // dd($metaArray,$sensyLogExists);

        if ($finalArray['transStatus']  == 'Y') {

            $flag['transaction_id']    = $finalArray['transId'];

            $flag['payer_reference']    = $finalArray['transId'];

            $flag['status']             = true;

            $flag['message']            = 'Captured';
        } else {

            // not successful

            $flag = [

                'status'    => false,

                'error'     => $request['message'],

            ];
        }


        $booking = new BookingsController();

        return $booking->finish_checkout($flag);
    }

    public function finish_booking_ai_sensy(Request $request)
    {
        $inputData = $request->all();


        $sensLogExists = SensyLog::where('cartId', $inputData['cartId'])->first();

        if ($inputData['transStatus']  == 'Y') {

            $flag['transaction_id']    = $inputData['transId'];

            $flag['payer_reference']    = $inputData['transId'];

            $flag['status']             = true;

            $flag['message']            = 'Captured';
        } else {

            // not successful

            $flag = [

                'status'    => false,

                'error'     => $request['message'],

            ];
        }


        $this->aiSensyBooking($sensLogExists, $flag);

        return response()->json(['status' => true], 200);
    }

    public function aiSensyBooking($sensLogExists, $flag)
    {


        $transaction_id = $flag['transaction_id'];

        $log = json_decode($sensLogExists->meta);

        // $transaction_id=$request['transId'];

        $metaArray = json_decode($sensLogExists->meta, true);
        $meta = array_merge($metaArray, $flag);

        $event = Event::with('tickets')->find($metaArray['event_id']);
        $tickets = $event->tickets;
        $ticketIds = $tickets->pluck('id')->toArray();
        $ticketTitle = [];
        $names = [];
        $addresses = [];
        $phones = [];
        $is_donations = [];
        $quantities = [];

        $username = $metaArray['name'];
        $userMobile = $metaArray['mobile'];
        $userAddress = $metaArray['address'];
        $product_items = $metaArray['product_items'];

        foreach ($tickets as $ticket) {
            $title = [];
            $name = [];
            $address = [];
            $mobile = [];
            $donation = [];
            $title[] = $ticket->title;
            $ticketTitle[] = $title;
            $name[] = $username;
            $names[] = $name;
            $address[] = $userAddress;
            $addresses[] = $address;
            $mobile[] = $userMobile;
            $phones[] = $mobile;
            $is_donations[] = $donation;
            $qt = 0;
            foreach ($product_items as $item) {
                if ($ticket->ticket_retailer_id == $item['product_retailer_id']) {
                    $qt = $item['quantity'];
                }
            }
            $quantities[] = $qt;
        }

        $jsonData = [
            "event_id" => $event->id,
            "voucher_code" => $metaArray['voucher_code'],
            "booking_date" => $event->start_date,
            "start_time" => $event->start_time,
            "end_time" => $event->end_time,
            "customer_id" => $metaArray['user'],
            "address_type" => 1,
            "delivery_address" => $userAddress,
            "collection_point" => '',
            "quantity" => $quantities,
            "ticket_id" => $ticketIds,
            "ticket_title" => $ticketTitle,
            "address" => $addresses,
            "name" => $names,
            "phone" => $phones,
            "is_donation" => $is_donations,
            'transaction_id' => $transaction_id,
            'delivery_charge_id' => $metaArray['delivery_charge_id']

        ];


        Auth::loginUsingId($metaArray['user']);

        $apiBooking = new ApiBookingController();
        $request = new  Request($jsonData);

        $bookingResponse = $apiBooking->ai_sensy_book_tickets($request, $flag);

        $sensLogExists->payment_success = 1;
        $sensLogExists->status = 1;
        $sensLogExists->meta = json_encode($meta);
        $sensLogExists->save();

        $client = new Client();

        $headers = [
            'Content-Type' => 'application/json',
        ];
        $templateMobile = '+' . $log->mobile;
        $data = [
            "apiKey" => config('aisensy.apiKey'),
            "campaignName" => "Success Booking",
            "destination" => "$templateMobile",
            "userName" => "$log->name",
            "templateParams" => [
                "$transaction_id",
            ],
        ];

        $response = $client->post('https://backend.aisensy.com/campaign/t1/api/v2', [
            'headers' => $headers,
            'json' => $data,
        ]);

        // Get the response body as a string
        $body = $response->getBody()->getContents();

        return response()->json(['data' => 'Booking Successfully completed']);
    }
}
