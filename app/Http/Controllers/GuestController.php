<?php

namespace App\Http\Controllers;

use App\Http\Helper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use Classiebit\Eventmie\Notifications\MailNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Facades\Voyager;

class GuestController extends Controller
{
    /**
     *  organizer create user
     */
    use RegistersUsers;


    public function registerGuest(Request $request)
    {
        // 1. validate data
        $request->validate([
            'name'             => ['required', 'string', 'max:255'],
            'email'            => ['required', 'string', 'email', 'max:255','unique:users,email'],
            'password'         => ['required', 'max:512']
        ]);

        $flag = \Auth::attempt ([
            'email' => $request->email,
            'password' => $request->password 
        ]);

        $user = [];

        if($flag)
        {
            $user = \Auth::user();
        }
        else
        {
            $email_exist = User::where('email', $request->email)->first();
            
            if(!empty($email_exist))
            {
                $request->validate([
                    'password1'            => 'required',
                ],[
                    'password1.required' => __('eventmie-pro::em.email_password_error'),
                ]);
            }

        }

        // check if user already exists
        
        if(!empty($user)) {
            $stripe_secret_key     = null;
            if(!empty(setting('apps.stripe_public_key')) && !empty(setting('apps.stripe_secret_key')))
                $stripe_secret_key = $user->createSetupIntent()->client_secret;

            // auto login
            \Auth::login($user);
            
            return response()->json([
                'status' => true, 
                'user' => $user->only(['id', 'name', 'phone', 'email','address']),
                'verify_email' => 1, 
                'is_verify_email' => setting('multi-vendor.verify_email'), 
                'stripe_secret_key' => $stripe_secret_key
            ]);        
        }

        if(!empty(setting('apps.twilio_sid')) && !empty(setting('apps.twilio_auth_token')) && !empty(setting('apps.twilio_number')))
        {
            $request->validate([
                'phone'           => 'required|string|max:255',
            ]);        
        }

        // create user
        $user = User::create([
                    'name'          => $request->name,
                    'email'         => $request->email,
                    'password'      => \Hash::make($request->password),
                    'role_id'       =>  2,
                    'phone'         => $request->phone,
                ]);
        $user->roles()->sync([2]);

        $user->role_id = 2;

        $user->save();
        
        // auto login
        \Auth::login($user);


        // ====================== Notification ====================== 
        $mail['mail_subject']   = __('eventmie-pro::em.register_success');
        $mail['mail_message']   = __('eventmie-pro::em.get_tickets');
        $mail['action_title']   = __('eventmie-pro::em.login');
        $mail['action_url']     = route('eventmie.login');
        $mail['n_type']         = "user";

        /* Guest password reset notification */
        $msg                    = [];
        $msg[]                  = __('eventmie-pro::em.guest_password_reset');
        $mail['extra_lines']    = $msg;

        // notification for
        $notification_ids       = [
            1, // admin
            $user->id, // new registered user
        ];
        
        $users = User::whereIn('id', $notification_ids)->get();
        
        \App\Jobs\RegistrationEmailJob::dispatch($mail, $users, 'register')->delay(now()->addSeconds(10));
        /* Send email verification link */
        $user->sendEmailVerificationNotification();
        /* Send email verification link */

        $user->markEmailAsVerified();
        // ====================== Notification ======================     

        if(setting('multi-vendor.verify_email') && empty($user->email_verified_at))
        {
            $user->sendEmailVerificationNotification();
            
            return response()->json(['status' => true, 'user' => $user->only(['id', 'name', 'phone', 'email']),  'verify_email' => 0, 'is_verify_email' => setting('multi-vendor.verify_email')
            ]);

        }
       
        $stripe_secret_key     = null;
        if(!empty(setting('apps.stripe_public_key')) && !empty(setting('apps.stripe_secret_key')))
            $stripe_secret_key = $user->createSetupIntent()->client_secret;

        
        return response()->json(['status' => true, 'user' => $user->only(['id', 'name', 'phone', 'email']),
        'verify_email' => 1, 'is_verify_email' => setting('multi-vendor.verify_email'), 'stripe_secret_key' => $stripe_secret_key
        ]);        
    } 


    public function registerCheckout(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string','max:40','regex:/^[\pL\s]+$/u'],
            'last_name' => ['required', 'string', 'max:40','regex:/^[\pL\s]+$/u'],
            'email' => ['required', 'email', 'max:255', 'regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',function ($attribute, $value, $fail) {
                $count = User::where('email',$value)->count();
                if($count > 0 ){
                    $fail('Email  already registered.');
                }
            } ],
            'phone' => ['required', 'regex:/^7\d{9}$|^07\d{9}$/','numeric'],
            'password' => ['required', 'numeric', 'digits:6', 'confirmed'],
            'password_confirmation' => ['required', 'digits:6', 'numeric'],
            'captcha' => 'required|captcha'
        ],[
            'first_name.regex' => 'The first name may only contain letters and spaces.',
            'last_name.regex' => 'The last name may only contain letters and spaces.',
            'captcha.captcha'=>'Invalid captcha code.',
            'phone.regex'=> 'Phone number should be 10 to 11 Digit',

        ],['password' => 'Pin', 'password_confirmation' => 'Confirm pin']);

        $data = [
            'name' => $request->get('first_name').' '.$request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' => $request->get('password'),
            'captcha' => $request->get('captcha'),
        ];

        $validateUser = Helper::validateUser(['email' => $request->get('email'),'phone' => $request->get('phone')]);

        if(is_null($validateUser))
        {
            event(new Registered($user = $this->create($data)));

            //\Auth::login($user);
            $this->guard()->login($user);

            // Send payload of user registration to hookdesk

            $payload=[
                'notification_type'=>'user_registration',
                'payload'=> [
                    'first_name'=> $request->get('first_name'),
                    'last_name'=> $request->get('last_name'),
                    'email'=> $request->get('email'),
                    'phone' => $request->get('phone'),
                    'password'=> $request->get('password'),
                    'captcha' => $request->get('captcha'),
                ]
            ];

            Helper::notifyThroughWebhook($payload);

            $clienturl = (config('app.env') == 'production') ? 'https://dhigna.com/callback' : 'https://uat.dhigna.com/callback';

            $clientRepository =  app('Laravel\Passport\ClientRepository');
            $clientRepository->create($user->id,$request->get('first_name').' '.$request->get('last_name'),$clienturl);

            $msg = 'Please check your Email to Verify the Email Address.';
            session()->flash('status', $msg);

            if ($response = $this->registered($request, $user)) {

                return $response;
            }

            // if(\Auth::user()->hasRole('customer'))
            // {
            //     $response = $this->service->setupAccessToken([
            //         'email' => $request->get ( 'email' ),
            //         'password' => $request->get ( 'password' )
            //     ]);

            //     AuthWalletToken::updateOrCreate(['user_id' => $user->id],[
            //         'user_id' => $user->id,
            //         'token' => @$response['token']
            //     ]);
            // }
            $stripe_secret_key     = null;
            if(!empty(setting('apps.stripe_public_key')) && !empty(setting('apps.stripe_secret_key')))
                $stripe_secret_key = $user->createSetupIntent()->client_secret;

            session(['password' => $request->get('password')]);
            $user = \Illuminate\Support\Facades\Auth::user();
            $clientDetails = \Illuminate\Support\Facades\DB::table('oauth_clients')->whereUserId($user?->getKey())->first();

            $url = (config('app.env') == 'production') ? 'https://dhigna.com/user-portal-detail/'.$clientDetails?->id.'/'.$clientDetails?->secret : 'https://uat.dhigna.com/user-portal-detail/'.$clientDetails?->id.'/'.$clientDetails?->secret;

            return response()->json(['status' => true, 'user' => $user->only(['id', 'name', 'phone', 'email','address']),
            'verify_email' => 1, 'is_verify_email' => setting('multi-vendor.verify_email'), 'stripe_secret_key' => $stripe_secret_key
            ]);  
        }else
        {
            if(!is_null(@$validateUser['errors']['phone']))
            {
                $validatMsg = $validateUser['errors']['phone'][0];
            }else if(!is_null(@$validateUser['errors']['email'])){
                $validatMsg = $validateUser['errors']['email'][0];
            }
           
            return response()->json(['status' => false, 'user' => NULL,
            'verify_email' => NULL, 'is_verify_email' =>NULL , 'stripe_secret_key' => NULL,'errorMsg' => $validatMsg
            ]);
        }

    }
    public function loginCheckout(Request $request)
    {
        // 1. validate data
        $request->validate([
            'email'            => ['required', 'string', 'email', 'max:255'],
            'password'         => ['required', 'max:512']
        ]);

        $flag = \Auth::attempt ([
            'email' => $request->email,
            'password' => $request->password 
        ]);

        $user = [];
        if($flag)
        {
            $user = \Auth::user();
            $stripe_secret_key     = null;
            if(!empty(setting('apps.stripe_public_key')) && !empty(setting('apps.stripe_secret_key')))
                $stripe_secret_key = $user->createSetupIntent()->client_secret;
            return response()->json(['status' => true, 'user' => $user->only(['id', 'name', 'phone', 'email','address']),
            'verify_email' => 1, 'is_verify_email' => setting('multi-vendor.verify_email'), 'stripe_secret_key' => $stripe_secret_key
            ]);  
        } else {
            
            throw ValidationException::withMessages(['msg' => __('admin.login_failed')]);
        }
    }
    protected function create(array $data)
    {
        $user   = User::create([
                    'name'      => $data['name'],
                    'email'     => $data['email'],
                    'phone'     => $data['phone'],
                    'password'  => Hash::make($data['password']),
                    'role_id'  => 2,
                    'captcha'  => $data['captcha']
                ]);

        // Send welcome email
        if(!empty($user->id))
        {
            // ====================== Notification ======================
            $mail['mail_subject']   = __('eventmie-pro::em.register_success');
            $mail['mail_message']   = __('eventmie-pro::em.get_tickets');
            $mail['action_title']   = __('eventmie-pro::em.login');
            $mail['action_url']     = eventmie_url();
            $mail['n_type']         = "user";

            // notification for
            $notification_ids       = [
                1, // admin
                $user->id, // new registered user
            ];

            $users = User::whereIn('id', $notification_ids)->get();
            $setting =  Voyager::model('Setting')->where('key' ,'multi-vendor.verify_email')->first();
            if($setting->value == 1)
            {
                if(checkMailCreds())
                {
                    try {
                        \Notification::locale(\App::getLocale())->send($users, new MailNotification($mail));
                    } catch (\Throwable $th) {}
                }
            }
            // ====================== Notification ======================
        }

        $this->redirectTo = \Session::get('url.intended');

        return $user;
    }
    
}
