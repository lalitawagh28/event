<?php

namespace App\Http\Controllers;

use App\Models\AuthWalletToken;
use App\Models\User;
use App\Service\WalletPayService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class WalletPayController extends Controller
{

    private WalletPayService $service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->service = new WalletPayService;

    }

    

    public function deposit_form(Request $request){
        $wallets = $this->getWallets();
        $transactions =  $this->getTransactions($request);
        return view('wallet.deposits.form',compact('wallets','transactions'));
    } 
    public function getTransactions($request){
        $user = session()->get('user');
        $page = 1;
        if($request->has('page')){
            $page = $request->page;
        }
        $data = [ 'transaction_method' => 'wallet', 'page' => $page,'per_page' => '10','transaction_type' => 'deposit'];

        return $this->service->getTransactions( $data,$user);
    }
    public function getWallets(){
        $user = session()->get('user');
        $walletToken = $this->getApiToken($user);

        if (!is_null($user) && !is_null($walletToken)) {
            $walletsInfo = $this->service->getTransactionList($user);
            $wallets = collect($walletsInfo);
        } else {
            $wallets = [];
        }
        return $wallets;
    }

    public function getApiToken($user){
        $walletToken = AuthWalletToken::whereUserId($user?->id)->first();
        if (is_null($walletToken) && !is_null($user)) {
            $response = $this->service->setupAccessToken([
                'email' =>  $user->email,
                'password' => session()->get('password')
            ]);

            if ($user->hasRole('customer')) {
                AuthWalletToken::updateOrCreate(['user_id' => $user->id], [
                    'user_id' => $user->id,
                    'token' => @$response['token']
                ]);
                $walletToken = @$response['token'];
            }
        }
        return $walletToken;
    }

    public function get_exchange_rate(Request $request){
        $this->validate($request,[
            'to_currency'       => 'required',
            't_currency'       => 'required',
            'from_currency'     => 'required|different:t_currency',
            'amount'            =>     'required|gt:0',
        ],[ 'to_currency.required' => 'To Currency is required.',
            'from_currency.required' => 'From Currency is Required.',
            'from_currency.different'=> 'From Currency and To Currency should not be same.',
            'amount.required' => 'Amount is required.',
            'amount.gt' => 'Amount should be greater than zero.',]);


            $data = [
                'from_id' => $request->from_currency,
                'to_id' => $request->to_currency,
                'amount' => $request->amount,
                'transaction_type' => 'deposit'
            ];
            $user = session()->get('user');
            $response =  $this->service->getExchangeRate( $data,$user);
            return $response['data'];
    }

    public function deposit_store(Request $request){
        $this->validate($request,[
            'to_currency'       => 'required',
            't_currency'       => 'required',
            'from_currency'     => 'required|different:t_currency',
            'amount'            =>     'required|gt:0',
            'payment_method' => 'required',
            'reference' => 'required',
        ],[ 'to_currency.required' => 'To Currency is required.',
            'from_currency.required' => 'From Currency is Required.',
            'from_currency.different'=> 'From Currency and To Currency should not be same.',
            'amount.required' => 'Amount is required.',
            'payment_method.required' => 'Payment Method is required.',
            'reference.required' => 'Referene is required.',
            'amount.gt' => 'Amount should be greater than zero.',]);
            $data = [
                'wallet' => $request->to_currency,
                'currency' => $request->from_currency,
                'amount' => $request->amount,
                'reference' => $request->reference,
                'payment_method' => $request->payment_method,
                'workspace_id'   => $request->workspace_id,
            ];
            $user = session()->get('user');
            $response =  $this->service->saveWalletDeposit( $data,$user);
            $response['status'] = 'success';
            return $response;
            //$user = session()->get('user');
    }

    public function deposit_otp_verify(Request $request){
        $this->validate($request,[
            'code'       => 'required',
            'transaction_id'       => 'required',
        ],[ 'code.required' => 'Code is required.',
            'transaction_id.required' => 'Transaction ID is Required.',]);
            $data = [
                'code' =>  $request->code,
                'transaction_id' => $request->transaction_id,
            ];

        $user = session()->get('user');
        $response =  $this->service->walletDepositVerify( $data,$user);
        $response['status'] = 'success';
        return $response;
    }

    public function deposit_resend_otp(Request $request){
        $this->validate($request,[
            'transaction_id'       => 'required',
        ],[ 
            'transaction_id.required' => 'Transaction ID is Required.',]);
            $data = [
                'transaction_id' => $request->transaction_id,
            ];
        $user = session()->get('user');
        $response =  $this->service->walletDepositResendOtp( $data,$user);
        $response['status'] = 'success';
        return $response;
    } 

    public function my_deposits(Request $request){
        $page = 1;
        if($request->has('page')){
            $page = $request->page;
        }
        $user = session()->get('user');
        $data = [ 'transaction_method' => 'wallet', 'page' => $page,'per_page' => $request->per_page,'transaction_type' => 'deposit'];

        return $this->service->getTransactions( $data,$user);
    }
}