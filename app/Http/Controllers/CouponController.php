<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use App\Models\Ticket;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\Http\Requests\CouponValidationRequest;
use App\Models\AgentTicket;
use App\Models\TicketSubCategory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class CouponController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->couponcode = new Coupon;
        $this->agent_ticket = new AgentTicket();
        $this->ticket    = new Ticket();
    }

    public function index()
    {

        $coupons = Coupon::latest()->get();

        return Voyager::view('voyager::coupon.browse', compact('coupons'));
    }

    public function create()
    {
        return Voyager::view('voyager::coupon.create');
    }

    public function store(CouponValidationRequest $request)
    {
        $data = $request->except(['_method', '_token']);

       // try {
            $sentences = Str::of($data['agent_name'])
                ->explode(' ')
                ->map(function ($sentence) {
                    $sentence = trim($sentence);
                    return Str::substr(Str::upper($sentence), 0, 1);
                })
                ->toArray();

            $agentName = collect($sentences)->join('');

            if ($data['code_type'] == 'free') {

                $type = '00';

                $data['discount'] = 0;

                $data['code_value'] = 'fixed';
            } else {
                $type = '01';
            }

            $data['code'] = $agentName . $type . (int) $data['discount'];
            if(Coupon::where('code', $data['code'])?->first()){
                throw ValidationException::withMessages(['name' => __('admin.coupon_name_already_taken')]);
            }
            $coupon = Coupon::create($data);

            $status = 'success';

            $message = 'Coupon created successfully';
        // } catch (\Throwable $th) {

        //     $status = 'error';

        //     $message = 'Something went wrong';
        // }

        return redirect()->route('coupon.index')
            ->with($status, $message);
    }

    public function edit($id)
    {
        $coupon = Coupon::find($id);

        return Voyager::view('voyager::coupon.edit', compact('coupon'));
    }

    public function update(CouponValidationRequest $request, $id)
    {
        $data = $request->except(['_method', '_token']);

        try {
            $sentences = Str::of($data['agent_name'])
                ->explode(' ')
                ->map(function ($sentence) {
                    $sentence = trim($sentence);
                    return Str::substr(Str::upper($sentence), 0, 1);
                })
                ->toArray();

            $agentName = collect($sentences)->join('');

            if ($data['code_type'] == 'free') {

                $type = '00';

                $data['discount'] = 0;

                $data['code_value'] = 'fixed';
            } else {
                $type = '01';
            }

            $data['code'] = $agentName . $type . (int) $data['discount'];

            $coupon = Coupon::find($id)->update($data);

            $status = 'success';

            $message = 'Coupon updated successfully';
        } catch (\Throwable $th) {

            $status = 'error';

            $message = 'Something went wrong';
        }

        return redirect()->route('coupon.index')
            ->with($status, $message);
    }

    public function destroy(Request $request, $id)
    {
        $arrayId = $request->ids;

        if (!empty($arrayId)) {
            $ids = explode(',', $arrayId);
        } else {
            $ids = [$id];
        }

        try {
            $coupon = Coupon::whereIn('id', $ids)->delete();

            $status = 'success';

            $message = 'Coupon deleted successfully';
        } catch (\Throwable $th) {

            $status = 'error';

            $message = 'Something went wrong ' . $th->getMessage();
        }

        return redirect()->route('coupon.index')
            ->with($status, $message);
    }
    public function applyVoucher(Request $request){
        $request->validate([
            'voucher_code'         => 'required|max:32|String',
            'tickets'         => 'required',
        ], [
            'voucher_code' => __('eventmie-pro::em.voucher_code') . ' ' . __('eventmie-pro::em.required'),
        ]);
        $type[]='general';
        if(Auth::user()?->type == 'student'){
            $type[]='student';
        }
        $errors = [];
        $error ='';
        $voucher_rewards = 0;
        
            foreach($request->tickets as  $id => $subCat){
                try {
                    $ticket = Ticket::findOrFail($id);
                    $price = $ticket->get_price();
                    foreach($subCat as $i => $qty){
                        $sub_cat_title = '';
                        if( $i){
                            $sub_cat = TicketSubCategory::find($i);
                            $sub_cat_title = "( ".$sub_cat->title." )";
                            $price = $sub_cat->price;
                        }
                            $sales_error = false;
                            if($i){
                                $check_vouchercode  = AgentTicket::where('sub_category_id',$i)->whereIn('type',$type)->where('ticket_id',$id)->where('coupon_code' , $request->voucher_code)->orderBy('type','desc')->first();
                            } else {
                                $check_vouchercode  = AgentTicket::where('ticket_id', $id)->whereIn('type',$type)->whereNull('sub_category_id')->where('coupon_code',$request->voucher_code)->orderBy('type','desc')->first();
                                
                            }
                            $book_count = $check_vouchercode->bookings?->count();
                            if (($check_vouchercode->quantity - $book_count) < $qty) {
                                $sold_out = $qty - $check_vouchercode->quantity + $book_count;
                                $qty  = $qty - $sold_out;
                                if( $qty < 1 ){
                                    $errors[] = "Ticket : " .$ticket->title. " " . $sub_cat_title. " Qty = ".$sold_out . " Sold Out";
                                }
                                if($qty> 0){
                                    $errors[] = "Ticket : " .$ticket->title. " " . $sub_cat_title. " Qty = ".$qty . " only applied";
                                }
                                
                            }
                            if ($ticket->sale_start_date && Carbon::parse($ticket->sale_start_date)->gt(Carbon::today())) {
                                $errors[] = "Ticket : " .$ticket->title. " " . $sub_cat_title. " Voucher valid from " .Carbon::parse($ticket->sale_start_date)->format('Y-m-d');
                                $sales_error = true;
                            } else if (!empty($check_vouchercode) && Carbon::parse($check_vouchercode->valid_from)->gt(Carbon::today())) {
                                $errors[] = "Ticket : " .$ticket->title. " " . $sub_cat_title. " Voucher valid from " .Carbon::parse($check_vouchercode->valid_from)->format('Y-m-d');
                                $sales_error = true;
                            } 
                            if($qty > 0 && !$sales_error)  {
                                $validDate = strtotime($check_vouchercode->valid_to);
                                $todayDate = strtotime(date("Y-m-d"));
                                if (($validDate < $todayDate) && ($validDate != $todayDate)) {
                                    $errors[] = "Ticket : " .$ticket->title. " " . $sub_cat_title. " Voucher code Expired. ";
                                } else {
                                    $user_promocode = [];
                                    // manual check in promocode_user if promocode not already applied by the user

                                    $params = [
                                        'user_id'   => $request->customer_id,
                                        'agent_ticket_id' => $check_vouchercode->id,
                                        'ticket_id' => $id,
                                        'sub_category_id' => $i > 0 ?? null,
                                    ];
                                    $user_promocode = $this->agent_ticket->agentcode_user($params);
                                    $already_used =  $check_vouchercode->max_uses - $user_promocode;
                                    if($already_used <  1) {
                                        $errors[] = "Ticket : " .$ticket->title. " " . $sub_cat_title. "Qty= ".$already_used." Voucher Code already Used. ";
                                    }
                                    if($already_used > $qty){
                                        if(Str::substr(Str::upper($check_vouchercode->code_value),1,1) == 'P'){
                                            $voucher_rewards +=  $qty * $price * ($check_vouchercode->discount/100);
                                        } else {
                                            $voucher_rewards += $qty * $check_vouchercode->discount;
                                        }

                                    } else {
                                        $qty = $already_used;
                                        if(Str::substr(Str::upper($check_vouchercode->code_value),1,1) == 'P'){
                                            $voucher_rewards +=  $already_used * $price * ($check_vouchercode->discount/100);

                                        } else {
                                            $voucher_rewards += $already_used * $check_vouchercode->discount;
                                        }

                                    }
                                    $error = '';
                                }
                            }



                    }
                } catch (\Throwable $e) {

                    $check_vouchercode_student  = AgentTicket::where('ticket_id', $id)->where('type','student')->where('coupon_code',$request->voucher_code)->orderBy('type','desc')->first();
        
                    if(!is_null($check_vouchercode_student))
                    {
                        $error = "To apply Student Voucher Code, please Register/Login.";
                    }else{
                        $error = "Voucher code is invalid.";
                    }
        
                }

            }
       

        if($voucher_rewards == 0 && $error) {
            $errors[] = $error;
        }
        return response()->json(['status' => 1, 'errors' => $errors,'voucher_rewards' => $voucher_rewards]);
    }
    public function applyCoupon(Request $request)
    {
        $request->validate([
            'couponcode'         => 'required|max:32|String',
            'ticket_id'         => 'required',
            'sub_category_id'   => 'nullable'
        ], [
            'couponcode' => __('eventmie-pro::em.voucher_code') . ' ' . __('eventmie-pro::em.required'),
        ]);

        $check_promocode = [];
        $type[]='general';
        if(Auth::user()->type == 'student'){
            $type[]='student';
        }
        // check promocode
        try {
            if(@$request->sub_category_id){
                $check_couponcode  = AgentTicket::where(['sub_category_id' => $request->sub_category_id])->whereIn('type',$type)->where(['ticket_id' => $request->ticket_id])->where(['coupon_code' => $request->couponcode])->orderBy('type','desc')->firstOrFail();
            } else {
                $check_couponcode  = AgentTicket::where(['ticket_id' => $request->ticket_id])->whereIn('type',$type)->where(['coupon_code' => $request->couponcode])->orderBy('type','desc')->firstOrFail();
            }

        } catch (\Throwable $e) {
            return response()->json(['message' => __('admin.invalid_voucher_code'), 'status' => 0]);
        }
        if ($check_couponcode->quantity < $request->quantity) {
            return response()->json(['message' => __('admin.coupon_sold_out'), 'status' => 0]);
        }

        // if ($check_couponcode->is_active !== 1) {

        //     return response()->json(['message' => __('eventmie-pro::em.in_active'), 'status' => 0]);
        // }
        if(!$check_couponcode) {
            return response()->json(['message' => __('admin.voucher_code_not_applicable'), 'status' => 0]);
        }
        $validDate = strtotime($check_couponcode->valid_to);
        $todayDate = strtotime(date("Y-m-d"));

        if (($validDate < $todayDate) && ($validDate != $todayDate)) {
            return response()->json(['message' => __('eventmie-pro::em.expired_voucher_code'), 'status' => 0]);
        }

        $user_promocode = [];
        // manual check in promocode_user if promocode not already applied by the user

        $params = [
            'user_id'   => $request->customer_id,
            'agent_ticket_id' => $check_couponcode->id,
            'ticket_id' => $request->ticket_id
        ];

        $user_promocode = $this->agent_ticket->agentcode_user($params);
        if(($user_promocode + $request->quantity) > $check_couponcode->max_uses) {
            return response()->json(['message' => __('eventmie-pro::em.already_used_voucher_code'), 'status' => 0]);
        }

        return response()->json(['status' => 1, 'promocode' => $check_couponcode]);
    }
}
