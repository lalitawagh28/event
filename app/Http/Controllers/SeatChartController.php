<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Helper;
use App\Models\Booking;
use App\Models\Event;
use App\Models\EventSeatChart;
use App\Models\Ticket;
use App\Models\Seatchart;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SeatChartController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // language change
        $this->middleware('common');

        // authenticate except login_first
        $this->middleware('auth');
        $this->event  = new Event;
    }

    public function upload_seatchart(Request  $request)
    {
        // 1. validate data
        $request->validate([
            'file'        => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'event_id'    => 'required|numeric|gt:0',
            'ticket_id'   =>  'required|numeric|gt:0',
        ]);

        $path            = 'seatschart/' . Carbon::now()->format('FY') . '/';

        $file            = $request->file('file');
        $extension       = $file->getClientOriginalExtension(); // getting image extension
        $image           = time() . rand(1, 988) . '.' . $extension;

        $file->storeAs('public/' . $path, $image);

        $chart_image           = $path . $image;

        $params = [
            'ticket_id'   => $request->ticket_id,
            'event_id'    => $request->event_id,
            'chart_image' => $chart_image
        ];

        // if ticket_id and event_id exist then will update image unless create
        Seatchart::updateOrCreate(
            ['ticket_id' => $params['ticket_id'], 'event_id' => $params['event_id']],

            $params

        );

        $ticket     = Ticket::with([
            'seatchart',
            'seatchart.seats'  => function ($query) {
                // $query->where(['status' => 1]);
            }
        ])->where(['id' => $request->ticket_id])->first();

        // $payload = [
        //     'upload_seatchart' => $params,
        //     'action' => 'event_seatchart_upload'
        // ];

        // $webhookResponse = $this->hitWebhook($payload, new Event);
        // if (!is_null($webhookResponse)) {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        return response()->json(['ticket' => $ticket, 'status' => true]);
    }

    public function upload_event_seatchart(Request  $request)
    {
        // 1. validate data
        $request->validate([
            'file'        => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'event_id'    => 'required|numeric|gt:0',
        ]);

        $path            = 'seatschart/' . Carbon::now()->format('Y') . '/';

        $file            = $request->file('file');
        $extension       = $file->getClientOriginalExtension(); // getting image extension
        $image           = time() . rand(1, 988) . '.' . $extension;

        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $file->storeAs($path, $image,'azure');

        $chart_image           = $path . $image;

        $params = [
            'seat_image' => $chart_image
        ];
        $this->event->update_data($request->event_id,$params);
        EventSeatChart::where('event_id', $request->event_id)->delete();
        $event= Event::where('id',$request->event_id)->with('seatchart')->first();
        if($event->seat_image){
            $event->seat_image = Storage::disk('azure')->temporaryUrl($event->seat_image, now()->addMinutes(5));
        }
        

        // $payload = [
        //     'upload_seatchart' => $params,
        //     'action' => 'event_seatchart_upload'
        // ];

        // $webhookResponse = $this->hitWebhook($payload, new Event);
        // if (!is_null($webhookResponse)) {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        return response()->json(['event' => $event, 'status' => true]);
    }
    /**
     *  disable or enable seatchart
     */

    public function disable_enable_seatchart(Request $request)
    {
        // 1. validate data
        $request->validate([
            'ticket_id'   =>  'required|numeric|gt:0',
        ]);

        $ticket = Ticket::with(['seatchart'])->where(['id' => $request->ticket_id])->first();

        if (empty($ticket))
            return response()->json(['status' => false, 'error' => __('eventmie-pro::em.ticket_not_found')]);

        if (empty($ticket->seatchart))
            return response()->json(['status' => false, 'error' => __('eventmie-pro::em.seatchart_not_found')]);

        $params = [
            'status'   => !$ticket->seatchart->status,

        ];


        // if ticket_id and event_id exist then will update image unless create
        Seatchart::updateOrCreate(
            ['ticket_id' => $ticket->id, 'event_id' => $ticket->event_id],

            $params

        );
        $params['ticket_id']=$ticket->id;
        $params['event_id']=$ticket->event_id;
        // $payload = [
        //     'disable_enable_seatchart' => $params,
        //     'action' => 'event_seatchart_status'
        // ];

        // $webhookResponse = $this->hitWebhook($payload, new Event);
        // if (!is_null($webhookResponse)) {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        return response()->json(['status' => true]);
    }

    public function hitWebhook($data, $model)
    {
        $response = $model->setWebhookParameters($data);
        return $response;
    }



    /**
     *  save seat
     */
    public function save_event_seats(Request $request)
    {
        // 1. validate data
        $request->validate([
            'ids.*'   => 'nullable|gt:0',
            'event_id'       => 'required|numeric|gt:0',
            'ticket_ids.*'      => 'required|gt:0',
            'sub_cat_ids.*'      => 'nullable|gt:0',
            'x.*'    => 'required|string',
            'y.*'  => 'required|string',
            'seat_names.*'     => 'required|string',


        ]);

        $params      = [];

        //prepare data
        if($request->ticket_ids){
            foreach ($request->ticket_ids as $key => $value) {
                $params['ticket_id']    = $request->ticket_ids[$key];
                $params['sub_cat_id']    = $request->sub_cat_ids[$key];
                $params['event_id']     = $request->event_id;
                $params['x']  =  $request->x[$key];
                $params['y']  =  $request->y[$key];
                $params['seat_name']         = $request->seat_names[$key];

                //save seats
                EventSeatChart::updateOrCreate(
                    [
                        'id' => $request->ids[$key]
                    ],
                    $params
                );
            }
        }
        $event     = Event::with('seatchart')->where('id',$request->event_id)->first();

        return response()->json(['status' => true, 'event' => $event]);
    }


    /**
     *  delete seat
     */
    public function delete_event_seat(Request $request)
    {
        // 1. validate data
        $request->validate([
            'id'        => 'required|numeric|gt:0',
            'event_id'        => 'required|numeric|gt:0',
            'ticket_id'      => 'required|numeric|gt:0',
        ]);

        $seat = EventSeatChart::where(['id' => $request->id])->first();
        $bookings = Booking::where('event_id',$request->event_id)->with(['attendees'])->first();
        if (empty($seat))
            return response()->json(['status' => false, 'error' => __('eventmie-pro::em.seat_not_found')]);

        // seat will be not deleted if have bookings
        if ($bookings->attendees->isNotEmpty())
            return response()->json(['status' => false, 'error' => __('eventmie-pro::em.seat_cannot_delete')]);

        EventSeatChart::where(['id' => $request->id])->delete();

        $event     = Event::with('seatchart')->where('id',$request->event_id)->first();


        return response()->json(['status' => true, 'event' => $event]);
    }

}
