<?php

namespace App\Http\Controllers\Eventmie\Agents;

use App\Models\Agent;
use App\Models\Booking;
use App\Models\Event;
use Illuminate\Http\Request;
use Facades\Classiebit\Eventmie\Eventmie;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Models\DataType;

class EventsController extends Controller
{
    use BreadRelationshipParser;
    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        
        // CUSTOM
        $this->event      = new Event;
        $this->booking    = new Booking;
        // CUSTOM
    
    }
    public function index(Request $request)
    {
        
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        //$this->authorize('browse', app($dataType->model_name));
        //$this->authorize('view', app($dataType->model_name));
        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                
                if(empty($dataRow->where('field', $value)->first()))
                    continue;
                
                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');
                
                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }
            $agent = Agent::where('id',Auth::id())->first();
            $ids = [];
            $agent->agent_tickets()->with('ticket.event')->get()->map(function($item) use(&$ids){
                if($item->ticket?->event?->id){
                    $ids[] = $item->ticket->event->id;
                }
               
            });
            $ids = array_unique($ids);
            $query->whereIn('id',$ids);
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }
            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');
            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }
        
        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }
        #Voyager::model('Event')->whereIn('id',)
        
        // $view = 'eventmie::vendor.voyager.events.browse';

        // CUSTOM
        $view = 'agents.events';
        // CUSTOM
        return Eventmie::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function getSlug(Request $request)
    {
        if (isset($this->slug)) {
            $slug = $this->slug;
        } else {
            $slug = explode('.', $request->route()->getName())[2];
        }

        return $slug;
    }

     /**
     * export_attendees_agent
     */

     public function export_attendees_agent($slug)
     {
        
         // check event is valid or not
         $event    = $this->event->get_event($slug);
         if(empty($event))
         {
             return error('access denied!', Response::HTTP_BAD_REQUEST );
         }
         
         $params   = [
             'event_id' => $event->id,
         ];
 
         // get particular event's bookings
         $bookings = $this->booking->get_event_bookings_agent($params);
         if(empty($bookings))
             return error_redirect('Booking Not Found!');
 
         // customize column values
         $bookings_csv = [];
         foreach($bookings as $key => $item)
         {
             $bookings[$key]['event_repetitive'] = $item['event_repetitive'] ? __('eventmie-pro::em.yes') : __('eventmie-pro::em.no');
             $bookings[$key]['is_paid']          = $item['is_paid'] ? __('eventmie-pro::em.yes') : __('eventmie-pro::em.no');
             
             
             if($item['booking_cancel'] == 1)
                 $bookings[$key]['booking_cancel']       = __('eventmie-pro::em.pending');
             elseif($item['booking_cancel'] == 2)
                 $bookings[$key]['booking_cancel']       = __('eventmie-pro::em.approved');
             elseif($item['booking_cancel'] == 3)
                 $bookings[$key]['booking_cancel']       = __('eventmie-pro::em.refunded');
             else
                 $bookings[$key]['booking_cancel']   = __('eventmie-pro::em.no_cancellation');
 
            if(!$bookings[$key]['promocode']) {
                $bookings[$key]['voucher_code']       = 'NA';
                $bookings[$key]['discount']       =  0;
            } else {
                $bookings[$key]['voucher_code']       = $bookings[$key]['promocode'];
                $bookings[$key]['discount']       = $bookings[$key]['promocode_reward'];
            }
                
             if($item['status'])
                 $bookings[$key]['status']           = __('eventmie-pro::em.enabled');
             else
                 $bookings[$key]['status']           = __('eventmie-pro::em.disabled');
 
             
             $bookings[$key]['checked_in']           = $item['checked_in'].' / '.$item['quantity'];
         }    
 
         // convert array to collection for csv
         $bookings = collect($bookings);
 
         // create object of laracsv
         $csvExporter = new \Laracsv\Export();
     
         // create csv 
         $csvExporter->build($bookings, [
             
             //events fields which will be include
             'id',
             
             'event_title',
             'event_start_date',
             'event_end_date',
             'event_start_time',
             'event_end_time',
             'event_repetitive',
 
             'customer_name', 
             'customer_email', 
 
             'order_number',
             'ticket_category',
             'ticket_price',
             'price',
             'quantity', 
             'voucher_code',
             'discount',
             'tax',
             'net_price',
             'currency',
             'transaction_id',
             'is_paid',
             'payment_type',
             
             'booking_cancel',
             'status',
             'checked_in',
 
             'created_at', 
             'updated_at'
         ]);
         
         // download csv
         $csvExporter->download($event->slug.'-attendies.csv');
     } 
}
