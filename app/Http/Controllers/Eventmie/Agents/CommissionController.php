<?php

namespace App\Http\Controllers\Eventmie\Agents;

use App\Models\Agent;
use App\Models\AgentTicket;
use App\Models\Coupon;
use App\Models\TicketSubCategory;
use App\Models\User;
use App\Notifications\AgentTicketCommission;
use Carbon\Carbon;
use Classiebit\Eventmie\Models\Event;
use Classiebit\Eventmie\Models\Ticket;
use Illuminate\Http\Request;
use Facades\Classiebit\Eventmie\Eventmie;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Models\DataType;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class CommissionController extends Controller
{
    use BreadRelationshipParser;
    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************
    public function index(Request $request)
    {

        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'commission')->first();

        // Check permission
        //$this->authorize('browse', app($dataType->model_name));
        //$this->authorize('view', app($dataType->model_name));
        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {

                if(empty($dataRow->where('field', $value)->first()))
                    continue;

                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');

                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::query();
            }
            //$organizer_ids = Agent::find(Auth::id())->organizer->pluck('organizer_id')->toArray();
            $query->where('agent_id',Auth::id());
            $query->join('users', 'users.id', '=', 'agent_tickets.agent_id');
            $query->join('tickets', 'tickets.id', '=', 'agent_tickets.ticket_id');
            $query->join('events', 'events.id', '=', 'tickets.event_id');
            $query->select(['agent_tickets.*','users.email as email','users.name as name']);
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }
            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');
            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }
        #Voyager::model('Event')->whereIn('id',)

        // $view = 'eventmie::vendor.voyager.events.browse';

        // CUSTOM
        $view = 'agents.commission';
        // CUSTOM
        return Eventmie::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function getSlug(Request $request)
    {
        if (isset($this->slug)) {
            $slug = $this->slug;
        } else {
            $slug = explode('.', $request->route()->getName())[2];
        }

        return $slug;
    }

    public function editSubmitTickets($id) {
        $view = 'agents.submit_tickets';
        $dataType = Voyager::model('DataType')->whereName('agent_tickets')->first();
        $data = $this->prepopulateBreadInfo('agent_tickets');
        $agent_ticket = AgentTicket::find($id);
        if(Str::substr(Str::upper($agent_ticket->code_value),0,1) == 'P') {
            $agent_ticket->commission =  $agent_ticket->commission.'%';
        }
        if(Str::substr(Str::upper($agent_ticket->code_value),1,1) == 'P') {
            $agent_ticket->discount =  $agent_ticket->discount.'%';
        }

        $data['agent_ticket'] = $agent_ticket ;
        $data['agent'] = Agent::find(Auth::id());
        $event_id = Ticket::find($data['agent_ticket']->ticket_id)->event_id;
        $data['events']= Event::whereIn('id',[$event_id])->get();
        return Eventmie::view($view, $data);
    }

    private function prepopulateBreadInfo($table)
    {
        $displayName = Str::singular(implode(' ', explode('_', Str::title($table))));
        $modelNamespace = config('voyager.models.namespace', app()->getNamespace());
        if (empty($modelNamespace)) {
            $modelNamespace = app()->getNamespace();
        }

        return [
            'isModelTranslatable'  => true,
            'table'                => $table,
            'slug'                 => Str::slug($table),
            'display_name'         => $displayName,
            'display_name_plural'  => Str::plural($displayName),
            'model_name'           => $modelNamespace.Str::studly(Str::singular($table)),
            'generate_permissions' => true,
            'server_side'          => false,
        ];
    }

    public function ticketUpdate(Request $request,$id) {
        $data = $request->validate([
            'agent_id' => 'required',
            'commission' => 'required',
            'user_commission' => 'required',
            'max_uses'        => 'required',
            'coupon_code'     => 'nullable|min:5|max:25',
            'valid_from'     => 'nullable',
            'valid_to'     => 'nullable',
        ],['agent_id' => 'Agent is required']);
        $agentTicket = AgentTicket::find($id);
        if(!$agentTicket) {
            throw ValidationException::withMessages(['quantity' => __('admin.invalid_ticket')]);
        }
        if($agentTicket->sub_category_id) {
            $ticket_model = TicketSubCategory::find($agentTicket->sub_category_id);
        } else {
            $ticket_model = Ticket::find($agentTicket->ticket_id);
        }
        $code = Str::substr(Str::upper($agentTicket->code_value),0,1);
        $commission = $agentTicket->commission;
        if( strpos(  $data ['user_commission'] , '%' ) !== false) {
            $code = $code.'P';
            $data['user_commission'] = str_replace('%','',$data ['user_commission']);
            $discount = $ticket_model->price * ($data ['user_commission']/100);
        } else {
            $code =  $code.'F';
            $data['user_commission'] = $data ['user_commission'];
            $discount = $data ['user_commission'];
        }
        $data['code_value'] = $code;
        if($discount + $commission  > (int)$request->price)
        {
            throw ValidationException::withMessages(['user_commission' => __('User Discount should no be greater than Ticket Price')]);

        }
        
        $coupon_id = $agentTicket->coupon_id;
        $agent_id =  $data ['agent_id'];
        $update = ["user_commission" => $data["user_commission"]];
        $event_id = Ticket::find($agentTicket->ticket_id)->event_id;
        $event =  Event::find($event_id);

        $ticketIds = $event->tickets()->pluck('id')->toArray();

        if(AgentTicket::where('code', $data['coupon_code'])->where('agent_id', '!=', $data['agent_id'])->whereNotIn('ticket_id', $ticketIds)->count()){
            throw ValidationException::withMessages(['quantity' => __('admin.coupon_code_is_used')]);
        }
        $data['code'] = $data['coupon_code'];
        unset($data['coupon_code']);
        if($coupon_id){
            if($agentTicket->code  != $data['code']){
                $agentTicket->coupon_code = $data['code'] . '00'. (int) $data["user_commission"];
            } else {
                $agentTicket->coupon_code = $data['code'];
            }

            $agentTicket->discount = $data["user_commission"];
            if($agentTicket->ticket->sale_price > 0){
                $valid_from = \Carbon\Carbon::parse($agentTicket->ticket->sale_start_date);
                $valid_to  =  \Carbon\Carbon::parse($agentTicket->ticket->sale_end_date)->format('Y-m-d');
            } else {
                $valid_to = \Carbon\Carbon::parse($agentTicket->ticket->event->end_date)->format('Y-m-d');
                $valid_from = \Carbon\Carbon::parse($agentTicket->ticket->event->start_date)->format('Y-m-d');
            }
            if($request->valid_from && $request->valid_to){
                if(Carbon::parse($request->valid_to)->gt($valid_to)) {
                    throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                }
                if(Carbon::parse($request->valid_from)->gt($valid_from)) {
                    throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_error')]);
                }
                $agentTicket->valid_to  = Carbon::parse($request->valid_to)->format('Y-m-d');
                $agentTicket->valid_from  = Carbon::parse($request->valid_from)->format('Y-m-d');
            } else {
                $agentTicket->valid_to  = $valid_to->format('Y-m-d');
                $agentTicket->valid_from  =  $valid_from->format('Y-m-d');
            }

            $agentTicket->max_uses = $data["max_uses"];
           // $coupon_data = Coupon::find($coupon_id)->update($coupon_data);
           
        }  else {
            $agentTicket->coupon_code = $data['code'];
            $agentTicket->discount = $data["user_commission"];
            $agentTicket->coupon_code = $data['code'];
            $agentTicket->discount = $data["user_commission"];
            if($agentTicket->ticket->sale_price > 0){
                $valid_from = \Carbon\Carbon::parse($agentTicket->ticket->sale_start_date)->format('Y-m-d');
                $valid_to =  \Carbon\Carbon::parse($agentTicket->ticket->sale_end_date)->format('Y-m-d');
            } else {
                $valid_to = \Carbon\Carbon::parse($agentTicket->ticket->event->end_date)->format('Y-m-d');
                $valid_from  = \Carbon\Carbon::now();
            }
            if($request->valid_from && $request->valid_to){
                if(Carbon::parse($request->valid_to)->gt($valid_to)) {
                    throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                }
                if(Carbon::parse($request->valid_from)->gt($valid_from)) {
                    throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_error')]);
                }
                $agentTicket->valid_to = Carbon::parse($request->valid_to)->format('Y-m-d');
                $agentTicket->valid_from  = Carbon::parse($request->valid_from)->format('Y-m-d');
            } else {
                $agentTicket->valid_to  = $valid_to->format('Y-m-d');
                $agentTicket->valid_from  =  $valid_from->format('Y-m-d');
            }

            $agentTicket->max_uses = $data["max_uses"];
            //$agentTicket->code_value = 'fixed';
           
        }
        $agentTicket->code_value= $data['code_value'];
        $agentTicket->save();
        $user = User::find($data['agent_id']);
        try{
            $user->notify(new AgentTicketCommission($event,$user,[$agentTicket]));
        } catch(\Exception $e) {
            \Log::info($e->getMessage());
        }
        //$agentTicket->update($update);
        return redirect()->route('voyager.agents.commission');
    }
}
