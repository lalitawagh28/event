<?php

namespace App\Http\Controllers\Eventmie\Agents;

use App\Exports\Export;
use App\Models\Agent;
use App\Models\AgentTicket;
use App\Models\Booking;
use App\Models\Event;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Facades\Classiebit\Eventmie\Eventmie;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Models\DataType;

class ReportsController extends Controller
{
    use BreadRelationshipParser;
    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************
    public function index(Request $request)
    {
        
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'agent_reports')->first();
        
        // Check permission
        //$this->authorize('browse', app($dataType->model_name));
        //$this->authorize('view', app($dataType->model_name));
        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                
                if(empty($dataRow->where('field', $value)->first()))
                    continue;
                
                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');
                
                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }
            $query->where('agent_id',Auth::id());
            
            $query->join('tickets', 'tickets.id', '=', 'agent_tickets.ticket_id');
            $query->join('events', 'events.id', '=', 'tickets.event_id');
            $query->select(['agent_tickets.*','events.title as event_title']);
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }
            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');
            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }
        
        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }
        #Voyager::model('Event')->whereIn('id',)
        
        // $view = 'eventmie::vendor.voyager.events.browse';

        // CUSTOM
        $view = 'agents.reports';
        // CUSTOM
        return Eventmie::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function getSlug(Request $request)
    {
        if (isset($this->slug)) {
            $slug = $this->slug;
        } else {
            $slug = explode('.', $request->route()->getName())[2];
        }

        return $slug;
    }

    public function salesReportByEvent()
    {
        $agent = Agent::where('id',Auth::id())->first();
            $ids = [];
            $agent->agent_tickets()->with('ticket.event')->get()->map(function($item) use(&$ids){
                if($item->ticket?->event?->id){
                    $ids[] = $item->ticket->event->id;
                }
               
            });
            $ids = array_unique($ids);
        $events = Event::whereIn('id',$ids)->get();
        
        return view('agents.reports.sales-report-by-event',compact('events'));
    }

    public function getSalesReportByEventData(Request $request)
    {
        $data = $request->validate([
            'events.*' => 'required|string',
            'from_date' => 'nullable|date',
            'to_date' => 'nullable|date'
        ]); 
        
        $agent = Agent::where('id', Auth::id())->first();
        $ids = [];
        $agent->agent_tickets()->with('ticket.event')->get()->map(function ($item) use (&$ids,$data) {
            if(in_array('all',$data['events'])){
                if ($item->ticket?->event?->id) {
                    $ids[] = $item->ticket->event->id;
                }
            } else {
                 if(in_array($item->ticket?->event?->id,$data['events'])){
                    $ids[] = $item->ticket->event->id;
                 }   
            }
        });
        $ids = array_unique($ids);
        $events = Event::whereIn('id', $ids)->with('tickets')->get();
        $ticket_ids = AgentTicket::where('agent_id',Auth::id())->pluck('ticket_id')->toArray();
        $selectedEvent =  Ticket::whereIn('tickets.id',$ticket_ids)->join('events', 'tickets.event_id', '=', 'events.id')
            ->select(['tickets.*','events.title as event_title'])->whereIn('events.id', $ids)->get();

        $agentIds = [];
        $agentIds = $agent->agent_tickets()->pluck('id')->toArray();
        $eventIds  = $data['events'];
        $eventsId = $ids;
        if(!is_null($request->input('from_date')) && !is_null($request->input('to_date')))
        {
            $bookings  = Booking::whereIn('agent_id', $agentIds)->join('events', 'bookings.event_id', '=', 'events.id')
            ->join('users', 'bookings.customer_id', '=', 'users.id')
            ->select(['bookings.*','events.title as event_title','users.name as customer_name'])->whereIn('event_id',$ids)->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($request->input('from_date'))), date('Y-m-d', strtotime('+1 day', strtotime($request->input('to_date'))))])->get();
        }else
        {
            $bookings  = Booking::whereIn('agent_id', $agentIds)->join('events', 'bookings.event_id', '=', 'events.id')
            ->join('users', 'bookings.customer_id', '=', 'users.id')
            ->select(['bookings.*','events.title as event_title','users.name as customer_name'])->whereIn('event_id',$ids)
            ->get();
        }
       
       // $bookings  = Booking::wherewhereEventId($request->input('event'))->whereBetween('created_at', [date('Y-m-d', strtotime($request->input('from_date'))), date('Y-m-d', strtotime('+1 day', strtotime($request->input('to_date'))))])->with('ticket')->with('agent_ticket')->get();


        return view('agents.reports.sales-report-by-event', compact('events', 'bookings', 'selectedEvent','eventIds','eventsId'));
    }


    public function salesReportByVoucher()
    {
        $agent = Agent::where('id',Auth::id())->first();
            $ids = [];
            $agent->agent_tickets()->with('ticket.event')->get()->map(function($item) use(&$ids){
                if($item->ticket?->event?->id){
                    $ids[] = $item->ticket->event->id;
                }
               
            });
            $ids = array_unique($ids);
        $events = Event::whereIn('id',$ids)->with('tickets')->get();
        
        return view('agents.reports.sales-report-by-voucher',compact('events'));
    }

    public function getVoucherCodeByEvent(Request $request)
    {
        
        $agent = Agent::where('id',Auth::id())->first();
        $voucherIds = [];
        $agent->agent_tickets()->with('ticket.event')->get()->map(function($item) use(&$voucherIds,$request){
           
            if($item->ticket?->event?->id == $request->eventId){
                $voucherIds[] = $item->id;
            }
           
        });

        $vouchers = AgentTicket::whereIn('id',$voucherIds)->groupBy('coupon_code')->get();
    
        return response()->json(['vouchers' => $vouchers]);
        
    }

    public function getSalesReportByVoucherData(Request $request)
    {
        $data = $request->validate([
            'event' => 'required|string',
            'voucher.*' => 'required|string',
            'from_date' => 'nullable|date',
            'to_date' => 'nullable|date'
        ]); 

        $agent = Agent::where('id', Auth::id())->first();
        $ids = [];
        $agent->agent_tickets()->with('ticket.event')->get()->map(function ($item) use (&$ids) {
            if ($item->ticket?->event?->id) {
                $ids[] = $item->ticket->event->id;
            }
        });
        $ids = array_unique($ids);

        $events = Event::whereIn('id', $ids)->with('tickets')->get();

        $ticket_ids = AgentTicket::where('agent_id',Auth::id())->pluck('ticket_id')->toArray();
        $selectedEvent =  Ticket::whereIn('tickets.id',$ticket_ids)->join('events', 'tickets.event_id', '=', 'events.id')
            ->select(['tickets.*','events.title as event_title'])->where('events.id', $request->input('event'))->get();

        if(in_array('Select All',$request->input('voucher')))
        {
            $voucherData = AgentTicket::whereIn('ticket_id',$selectedEvent->pluck('id')->toArray())->pluck('coupon_code')->toArray();
            $voucherData = array_unique( $voucherData);
            $couponCodes = AgentTicket::whereIn('coupon_code',$voucherData)->pluck('id')->toArray();
        }else{
            $voucherData = $request->input('voucher');
            $couponCodes =  AgentTicket::whereIn('coupon_code',$request->input('voucher'))->pluck('id')->toArray();
        }
      
        if(!is_null($request->input('from_date')) && !is_null($request->input('to_date')))
        {
            $bookings  = Booking::select('bookings.*','agent_tickets.commission','agent_tickets.discount','agent_tickets.id')->whereEventId($request->input('event'))->whereIn('bookings.agent_id',$couponCodes)->whereBetween('created_at', [date('Y-m-d', strtotime($request->input('from_date'))), date('Y-m-d', strtotime('+1 day', strtotime($request->input('to_date'))))])->with('ticket')
            ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')
            ->get();
        }else{
            $bookings  = Booking::select('bookings.*','agent_tickets.commission','agent_tickets.discount','agent_tickets.id')->whereEventId($request->input('event'))->whereIn('bookings.agent_id',$couponCodes)
            ->with('ticket')
            ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')
            ->get();
        }

        $agent = Agent::where('id',Auth::id())->first();
        $voucherIds = [];
        $agent->agent_tickets()->with('ticket.event')->get()->map(function($item) use(&$voucherIds,$request){
            if($item->ticket?->event?->id == $request->input('event')){
                $voucherIds[] = $item->id;
            }
        });

        $vouchers = AgentTicket::whereIn('id',$voucherIds)->groupBy('coupon_code')->get();
        $vouchersId = $couponCodes;
        $voucherIds  = $data['voucher'];

        return view('agents.reports.sales-report-by-voucher', compact('events', 'bookings', 'selectedEvent','vouchers','voucherIds','vouchersId'));
    }


    public function salesReportCommission()
    {
        $agent = Agent::where('id',Auth::id())->first();
            $ids = [];
            $agent->agent_tickets()->with('ticket.event')->get()->map(function($item) use(&$ids){
                if($item->ticket?->event?->id){
                    $ids[] = $item->ticket->event->id;
                }
               
            });
            $ids = array_unique($ids);
        $events = Event::whereIn('id',$ids)->with('tickets')->get();
        
        return view('agents.reports.sales-report-commission',compact('events'));
    }

    public function getSalesReportByCommissionData(Request $request)
    {
        $data = $request->validate([
            'events.*' => 'required|string',
            'from_date' => 'nullable|date',
            'to_date' => 'nullable|date'
        ]); 

        $agent = Agent::where('id', Auth::id())->first();
        $ids = [];
        $agent->agent_tickets()->with('ticket.event')->get()->map(function ($item) use (&$ids,$data) {
            if(in_array('all',$data['events'])){
                if ($item->ticket?->event?->id) {
                    $ids[] = $item->ticket->event->id;
                }
            } else {
                 if(in_array($item->ticket?->event?->id,$data['events'])){
                    $ids[] = $item->ticket->event->id;
                 }   
            }
        });
        $ids = array_unique($ids);
        $events = Event::whereIn('id', $ids)->with('tickets')->get();
        $ticket_ids = AgentTicket::where('agent_id',Auth::id())->pluck('ticket_id')->toArray();
        $selectedEvent =  Ticket::whereIn('tickets.id',$ticket_ids)->join('events', 'tickets.event_id', '=', 'events.id')
            ->select(['tickets.*','events.title as event_title'])->whereIn('events.id', $ids)->with('agent_tickets')->get();

        $agentIds = [];
        $agentIds = $agent->agent_tickets()->pluck('id')->toArray();
        $eventIds  = $data['events'];
        $eventsId = $ids;
        if(!is_null($request->input('from_date')) && !is_null($request->input('to_date')))
        {
            $bookings  = Booking::whereIn('agent_id', $agentIds)->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($request->input('from_date'))), date('Y-m-d', strtotime('+1 day', strtotime($request->input('to_date'))))])
            ->join('events', 'bookings.event_id', '=', 'events.id')
            ->join('users', 'bookings.customer_id', '=', 'users.id')
            ->select(['bookings.*','events.title as event_title','users.name as customer_name'])->get();
        }else{
            $bookings  = Booking::whereIn('agent_id', $agentIds)
            ->join('events', 'bookings.event_id', '=', 'events.id')
            ->join('users', 'bookings.customer_id', '=', 'users.id')
            ->select(['bookings.*','events.title as event_title','users.name as customer_name'])->get();
        }
     
        return view('agents.reports.sales-report-commission', compact('events', 'bookings', 'selectedEvent','eventIds','eventsId'));

    }

    
    public function exportEventSale(Request $request)
    {
       
        $data_ids = explode(',',$request->input('event_ids'));
        $agent = Agent::where('id', Auth::id())->first();
        $ids = [];
        $agent->agent_tickets()->with('ticket.event')->get()->map(function ($item) use (&$ids,$data_ids) {
            if(in_array($item->ticket?->event?->id,$data_ids)){
                $ids[] = $item->ticket->event->id;
             }  
        });
        $eventIds = array_unique($ids);
       
        if (!is_null($request->from_date) && !is_null($request->to_date)){
            
                $from_date = date('Y-m-d', strtotime($request->from_date));
                $to_date = date('Y-m-d', strtotime('+1 day', strtotime($request->to_date)));
                    
                    $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
                                ->join('users', 'bookings.customer_id', '=', 'users.id')
                                ->leftJoin('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                                ->select('bookings.*','events.title as event_title','users.name as customer_name',
                                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),                
                                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                                )->whereIn('event_id',$eventIds)
                                ->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($from_date)), date('Y-m-d', strtotime('+1 day', strtotime($to_date)))])
                                ->groupBy('ticket_id','sub_category_id')
                                ->orderBy('event_id','desc')
                                ->orderBy('ticket_id','desc')
                                ->orderBy('sub_category_id','asc')
                                ->get();
                
                
           
            } else {
           
                $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
                ->join('users', 'bookings.customer_id', '=', 'users.id')
                ->leftJoin('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                ->select('bookings.*','events.title as event_title','users.name as customer_name',
                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),
                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                )->whereIn('event_id',$eventIds)
                ->groupBy('ticket_id','sub_category_id')
                ->orderBy('event_id','desc')
                ->orderBy('ticket_id','desc')
                ->orderBy('sub_category_id','asc')
                ->get();
              
            
            }
                
            $list = collect();
            foreach ($bookings as  $booking){
                
                   
                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                    
                
                if($booking->ticket){
                    $columnDetail = [
                        $booking->event->title,
                        $booking->ticket?->title,
                        $booking->subcategory?->title ?? '',
                        $price,
                        $booking->tot_quantity
                       
                    ];
                    $list->push($columnDetail);
                }
                    

                
                
            }
                                               

        

        $columnsHeading = [
            'Event Name',
            'Ticket Category',
            'Ticket SubCategory',
            'Sales',
            'No Of Ticket',
        ];

        return Excel::download(new Export($list,$columnsHeading), 'EventSaleReport.xlsx');
    }
    public function exportCommisionSale(Request $request)
    {
       
        $data_ids = explode(',',$request->input('event_ids'));
        $agent = Agent::where('id', Auth::id())->first();
        $ids = [];
        $agent->agent_tickets()->with('ticket.event')->get()->map(function ($item) use (&$ids,$data_ids) {
            if(in_array($item->ticket?->event?->id,$data_ids)){
                $ids[] = $item->ticket->event->id;
             }  
        });
        $eventIds = array_unique($ids);
       
        if (!is_null($request->from_date) && !is_null($request->to_date)){
            
                $from_date = date('Y-m-d', strtotime($request->from_date));
                $to_date = date('Y-m-d', strtotime('+1 day', strtotime($request->to_date)));
                    
                    $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
                                ->join('users', 'bookings.customer_id', '=', 'users.id')
                                ->leftJoin('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                                ->select('bookings.*','events.title as event_title','users.name as customer_name',
                                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),                
                                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                                )->whereIn('event_id',$eventIds)
                                ->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($from_date)), date('Y-m-d', strtotime('+1 day', strtotime($to_date)))])
                                ->groupBy('ticket_id','sub_category_id','bookings.promocode')
                                ->orderBy('event_id','desc')
                                ->orderBy('ticket_id','desc')
                                ->orderBy('sub_category_id','asc')
                                ->get();
                
                
           
            } else {
           
                $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
                ->join('users', 'bookings.customer_id', '=', 'users.id')
                ->leftJoin('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                ->select('bookings.*','events.title as event_title','users.name as customer_name',
                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),
                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                )->whereIn('event_id',$eventIds)
                ->groupBy('ticket_id','sub_category_id','bookings.promocode')
                ->orderBy('event_id','desc')
                ->orderBy('ticket_id','desc')
                ->orderBy('sub_category_id','asc')
                ->get();
              
            
            }
                
            $list = collect();
            foreach ($bookings as  $booking){
                
                   
                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                    
                
                if($booking->ticket){
                    $columnDetail = [
                        $booking->event->title,
                        $booking->ticket?->title,
                        $booking->subcategory?->title ?? '',
                        $booking->promocode,
                        $price,
                        $booking->tot_quantity,
                        $booking->tot_commission,
                       
                    ];
                    $list->push($columnDetail);
                }
                    

                
                
            }
                                               

        

        $columnsHeading = [
            'Event Name',
            'Ticket Category',
            'Ticket SubCategory',
            'Voucher Code',
            'Sales',
            'No Of Ticket',
            'Agent Commission Earned'
        ];

        return Excel::download(new Export($list,$columnsHeading), 'CommissionSaleReport.xlsx');
    }
    public function exportVoucherSale(Request $request)
    {
       
        $event_id = $request->input('event_id');

        $voucherIds = explode(',',$request->input('voucher_ids'));
        if (!is_null($request->from_date) && !is_null($request->to_date)){
            
                $from_date = date('Y-m-d', strtotime($request->from_date));
                $to_date = date('Y-m-d', strtotime('+1 day', strtotime($request->to_date)));
                    
                    $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
                                ->join('users', 'bookings.customer_id', '=', 'users.id')
                                ->leftJoin('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                                ->select('bookings.*','events.title as event_title','users.name as customer_name',
                                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),                
                                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                                )->whereEventId($event_id)
                                ->whereIn('bookings.agent_id',$voucherIds)
                                ->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($from_date)), date('Y-m-d', strtotime('+1 day', strtotime($to_date)))])
                                ->groupBy('ticket_id','sub_category_id','bookings.promocode')
                                ->orderBy('event_id','desc')
                                ->orderBy('ticket_id','desc')
                                ->orderBy('sub_category_id','asc')
                                ->get();
                
                
           
            } else {
                $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
                ->join('users', 'bookings.customer_id', '=', 'users.id')
                ->leftJoin('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                ->select('bookings.*','events.title as event_title','users.name as customer_name',
                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),
                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                )->whereEventId($event_id)
                ->whereIn('bookings.agent_id',$voucherIds)
                ->groupBy('ticket_id','sub_category_id','bookings.promocode')
                ->orderBy('event_id','desc')
                ->orderBy('ticket_id','desc')
                ->orderBy('sub_category_id','asc')
                ->get();
              
            
            }   
            $list = collect();
            foreach ($bookings as  $booking){
                
                   
                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                    
                
                if($booking->ticket){
                    $columnDetail = [
                        $booking->event->title,
                        $booking->ticket?->title,
                        $booking->subcategory?->title ?? '',
                        $booking->promocode,
                        $price,
                        $booking->tot_quantity,
                       
                    ];
                    $list->push($columnDetail);
                }
                    

                
                
            }
                                               

        

        $columnsHeading = [
            'Event Name',
            'Ticket Category',
            'Ticket SubCategory',
            'Voucher Code',
            'Sales',
            'No Of Ticket',
        ];

        return Excel::download(new Export($list,$columnsHeading), 'VoucherSalesReport.xlsx');
    }
    
}
