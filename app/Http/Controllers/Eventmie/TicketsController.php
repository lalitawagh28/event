<?php

namespace App\Http\Controllers\Eventmie;

use App\Http\Helper;
use App\Models\AgentTicket;
use App\Models\Event;
use App\Models\Ticket;
use App\Models\Promocode;
use App\Models\TicketSubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;

/* CUSTOM */
use Illuminate\Http\Response;
use Classiebit\Eventmie\Http\Controllers\TicketsController as BaseTicketsController;
use Illuminate\Validation\ValidationException;

/* CUSTOM */

class TicketsController extends BaseTicketsController
{
    public function __construct()
    {
        parent::__construct();
        $this->promocode = new Promocode;
        $this->ticket    = new Ticket;
    }

    // add/edit tickets
    public function store(Request $request)
    {


        // if logged in user is admin
        $this->is_admin($request);

        // float validation and don't except nagitive value
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
        // 1. validate data
        $request->validate([

            'title'         => [
                'required', 'max:64',
            ],
            'price'         => ['nullable', 'regex:' . $regex],
            "sale_price"   => 'nullable|lte:'.(int)$request->price,
            'quantity'      => 'required|integer|min:0',
            'description'   => 'max:512',
            'event_id'      => 'required|numeric',
            'customer_limit' => [
                'nullable', 'integer', 'gt:0',
            ],
            'show_sub_category' => 'nullable|integer',
            'sale_end_date'    => 'required_with:sale_start_date',
            'price_type'     => 'required_if:show_sub_category,==,1|nullable|in:1,2',
            'sub_category.*'   => 'required_if:show_sub_category,==,1|nullable|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'sub_category_quantity.*' => 'required_if:show_sub_category,==,1|nullable|numeric|gt:0',
            'sub_category_price.*' => 'required_if:price_type,==,2|nullable|numeric|gt:0',
        ]);

        // $ticketId=  $request->input('ticket_id');
        $ticketTitle =  $request->input('title');
        $ticketIds = Event::find($request->event_id)->tickets->where('id', '!=', $request->ticket_id)->pluck('id')->toArray();

        if ($ticketIds) {
            $data = Ticket::whereIn('id', $ticketIds)->where('title', $ticketTitle)->first();
            if ($data !== null && $data->title = $ticketTitle) {
                throw ValidationException::withMessages(['title' => __('Ticket title already present')]);
            }
        }
        $subCategory = $request->input('sub_category');
        if ($subCategory) {
            if (count($subCategory) > count(array_unique($subCategory))) {
                throw ValidationException::withMessages(['sub_category.*' => __('Sub Category title should be Unique')]);
            };
        }
        $quantity=$request->input('quantity');

            if ((int)$quantity  == 0) {
                throw ValidationException::withMessages(['quantity.*' => __('Total Ticket Quantity must be greater than 0')]);

        }

        $customerLimite=$request->input('customer_limit');
        $quantity=$request->input('quantity');
        if((int)$customerLimite > (int)$quantity){
            throw ValidationException::withMessages(['customer_limit' => __('Booking Limit Per Customer should not exceed Total Ticket Quantity')]);
        }
        // check event id with login user that event id valid or not
        $check_event            = $this->event->get_user_event($request->event_id, $this->organiser_id);
        if ($request?->show_sub_category == 1) {
            $sub_category_total = 0;
            foreach ($request->sub_category_quantity as $key => $item) {
                $sub_category_total += $item;
            }
            if ($request->quantity !=  $sub_category_total) {
                throw ValidationException::withMessages(['code' => __('admin.quantity_does_not_match')]);
            }
        }
        if (empty($check_event)) {
            return error('access denied!', Response::HTTP_BAD_REQUEST);
        }

        $params    = [
            'event_id' =>  $request->event_id,
        ];
        $ticket_id  = $request->ticket_id;
        
        if ($request->sale_start_date) {
            $ticket = Ticket::find($ticket_id);
            if (Carbon::parse($request->sale_start_date)->ne(Carbon::parse($ticket->sale_start_date)) && Carbon::parse($request->sale_start_date)->lt(Carbon::today())) {
                throw ValidationException::withMessages(['code' => __('admin.invalid_sales_start_date_now')]);
            }
            if (Carbon::parse($request->sale_start_date)->ne(Carbon::parse($ticket->sale_end_date)) && Carbon::parse($request->sale_end_date)->lt(Carbon::today())) {
                throw ValidationException::withMessages(['code' => __('admin.invalid_sales_end_date_now')]);
            }
            if (Carbon::parse($request->sale_start_date)->gte(Carbon::parse($check_event->start_date))) {
                throw ValidationException::withMessages(['code' => __('admin.invalid_sales_start_date')]);
            }
            if (Carbon::parse($request->sale_end_date)->gte(Carbon::parse($check_event->start_date))) {
                throw ValidationException::withMessages(['code' => __('admin.invalid_sales_end_date')]);
            }
        }

        $params = [
            "title"        => $request->title,
            "price"        => $request->price,
            "quantity"     => $request->quantity,
            "description"  => $request->description,
            "event_id"     => $request->event_id,
            //CUSTOM
            "t_soldout"     => !empty($request->t_soldout) ? 1 : 0,
            "fast_selling"     => !empty($request->fast_selling) ? 1 : 0,
            "is_donation"     => !empty($request->is_donation) ? 1 : 0,
            "show_counter"     => !empty($request->show_counter) ? 1 : 0,
            //CUSTOM
        ];
        if ($request->show_sub_category &&  $request->price_type == 2) {
            $params['price'] = $request->sub_category_price[0];
        }

        //CUSTOM

        $sale_ticket   =  Ticket::where(['id' => $request->ticket_id, 'event_id' => $request->event_id])?->first();
        if($sale_ticket?->sub_category == 1 && $request?->show_sub_category == 0){
            if(AgentTicket::where('ticket_id',$request->ticket_id)->count()){
                throw ValidationException::withMessages(['show_sub_category' => __('admin.already_voucher_code_created')]);
            }
        }
        if($sale_ticket?->sub_category == 0 && $request?->show_sub_category == 1){
            if(AgentTicket::where('ticket_id',$request->ticket_id)->count()){
                throw ValidationException::withMessages(['show_sub_category' => __('admin.already_voucher_code_created')]);
            }
        }

        if (empty($sale_ticket)) {
            $sale_ticket                       = (object) [];
            $sale_ticket->sale_start_date      = null;
            $sale_ticket->sale_end_date        = null;
        }

        if ( $request->sub_category == 0  && !empty($request->sale_start_date) && !empty($request->sale_end_date) && (!empty($request->sale_price) &&  $request->sale_price > 0)) {
            if ($sale_ticket->sale_start_date != $request->sale_start_date || empty($sale_ticket->sale_start_date)) {
                $request->validate([
                    'sale_start_date'        => 'required|date|date_format:Y-m-d H:i:s',

                ]);
            }

            if ($sale_ticket->sale_end_date != $request->sale_end_date || empty($sale_ticket->sale_end_date)) {
                $request->validate([
                    'sale_end_date'          => 'required|date|after:sale_start_date|date_format:Y-m-d H:i:s',

                ]);
            }

            $request->validate([
                'sale_price'                  => ['required','gte:0','lte:'.(int)$request->price],
            ]);
            
            $params['sale_start_date']   = $request->sale_start_date;
            $params['sale_end_date']     = $request->sale_end_date;
            $params['sale_price']        = $request->sale_price ?? 0;
        } else {
            $params['sale_start_date']   = null;
            $params['sale_end_date']     = null;
            $params['sale_price']        =  0;
        }

        $params['sub_category']      = $request->show_sub_category ?? 0;
        $params['price_type']      = $request->price_type ?? 0;
        //CUSTOM


        $params['customer_limit'] = empty($request->customer_limit) ? null : $request->customer_limit;

        

        $ticket     =  $this->ticket->add_tickets($params, $ticket_id);

        if (empty($ticket)) {
            return response()->json(['status' => false]);
        }

        // add data in tax_ticket pivot table
        $taxes_ids    = json_decode($request->taxes_ids, true);
        $ticket->taxes()->sync($taxes_ids);

        /* CUSTOM */
        // Update ticket_id in case of new ticket
        if (!$ticket_id)
            $ticket_id = $ticket->id;

        $gate_ids    = json_decode($request->gates_ids, true);
        if (!empty($gate_ids)) {
            $this->ticket->update_gates($ticket_id, $gate_ids);
        } else {
            $this->ticket->update_gates($ticket_id);
        }
        // if have tickets then check free tickets or not
        $tickets   = $this->ticket->get_event_tickets($params);

        if ($tickets->isNotEmpty()) {
            // check free tickets
            $free_tickets           = $this->ticket->check_free_tickets($request->event_id);

            if (!empty($free_tickets) || (int)$request->price <= 0) {
                $params = [
                    'price_type' => 0
                ];

                // update price type column of event table by 1
                $this->event->update_price_type($request->event_id, $params);
            } else {
                $params = [
                    'price_type' => 1
                ];
                // update price type column of event table by 0
                $this->event->update_price_type($request->event_id, $params);
            }
        }


        if ($request?->show_sub_category == 1) {
            $this->create_sub_category($request, $ticket_id);
        } else {
            TicketSubCategory::where('ticket_id', $ticket_id)->delete();
        }
        // save promocodes
        $this->save_promocode($request, $ticket_id);
        /* CUSTOM */

        // get update event
        $event            = $this->event->get_user_event($request->event_id, $this->organiser_id);
        // set step complete
        $this->complete_step($event->is_publishable, 'tickets', $request->event_id);


        // $payload=[
        //     'store_ticket'=>$request->all(),
        //     'action'=>$request->ticket_id?'event_ticket_updated':'event_ticket_added',
        // ];

        // $webhookResponse = $this->hitWebhook($payload,new $this->event);
        // if(!is_null($webhookResponse))
        // {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        return response()->json(['status' => true]);
    }

    public function create_sub_category(Request $request, $ticket_id)
    {

        $ticket_sub_categorys = TicketSubCategory::where('ticket_id', $ticket_id)->pluck('id')->toArray();
        foreach ($request->sub_category as $key => $item) {
            $param = [];
            $param['title'] = $request->sub_category[$key];
            $param['quantity'] = $request->sub_category_quantity[$key];
            if ($request->price_type == 2) {
                $param['price'] = $request->sub_category_price[$key];
            } else {
                $param['price'] = $request->price;
            }
            $param['ticket_id'] = $ticket_id;
            if (@$request->sub_categories_id[$key]) {
                $update = TicketSubCategory::find($request->sub_categories_id[$key]);
                $update->update($param);
            } else {
                TicketSubCategory::create($param);
            }
        }
        if ($request->sub_categories_id) {
            $delete_categories = array_diff($ticket_sub_categorys, $request->sub_categories_id);
            if ($delete_categories) {
                TicketSubCategory::destroy($delete_categories);
            }
        } else {
            TicketSubCategory::destroy($ticket_sub_categorys);
        }
    }
    // get tickets by events
    public function tickets(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'          => 'required',
        ]);

        // if logged in user is admin
        $this->is_admin($request);

        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if (empty($check_event)) {
            return error(__('eventmie-pro::em.event') . ' ' . __('eventmie-pro::em.not_found'), Response::HTTP_BAD_REQUEST);
        }


        //CUSTOM
        $currency = setting('regional.currency_default');

        if (!empty($check_event->currency)) {
            $currency = $check_event->currency;
        }
        //CUSTOM

        $params    = [
            'event_id' =>  $request->event_id,
        ];
        $tickets   = $this->ticket->get_event_tickets($params);

        if ($tickets->isEmpty()) {
            return response()->json(['status' => false, 'currency' => setting('regional.currency_default')]);
        }

        return response()->json(['tickets' => $tickets, 'status' => true, 'currency' => $currency]);
    }

    /**
     *  custom function start
     */

    // save promocode
    public function save_promocode(Request $request, $ticket_id = null)
    {
        // save promocodes
        $promocodes = [];
        $params     = [];

        if (!empty($request->promocodes_ids)) {
            $promocodes = explode(",", $request->promocodes_ids);

            foreach ($promocodes as $key => $value) {
                $params[$key]['ticket_id']    =  $ticket_id;
                $params[$key]['promocode_id'] =  $value;
            }
        }
        $this->promocode->save_ticket_promocode($params, $ticket_id);
    }

    public function hitWebhook($data, $model)
    {
        $response = $model->setWebhookParameters($data);
        return $response;
    }
}
