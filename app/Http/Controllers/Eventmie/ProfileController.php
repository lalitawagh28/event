<?php

namespace App\Http\Controllers\Eventmie;

use Classiebit\Eventmie\Http\Controllers\ProfileController as BaseProfileController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\PhoneDigits;
use App\Rules\PhoneUk;
use Illuminate\Support\Facades\Hash;
use Classiebit\Eventmie\Models\User;
use Facades\Classiebit\Eventmie\Eventmie;

use Classiebit\Eventmie\Notifications\MailNotification;
use Auth;
use Illuminate\Validation\Rule;

class ProfileController extends BaseProfileController
{
    public function index($view = 'vendor.eventmie-pro.profile.profile', $extra = [])
    {

        return parent::index($view, $extra);
    }

    public function resetPin($view = 'vendor.eventmie-pro.profile.resetpin', $extra = [])
    {
        return parent::index($view, $extra);
    }
    public function profileSupport($view = 'vendor.eventmie-pro.profile.support', $extra = [])
    {
        return parent::index($view, $extra);
    }
    public function profileBank($view = 'vendor.eventmie-pro.profile.bank', $extra = [])
    {
        return parent::index($view, $extra);
    }
    public function profileSeller($view = 'vendor.eventmie-pro.profile.seller', $extra = [])
    {
        return parent::index($view, $extra);
    }

    public function profileOrganizer($view = 'vendor.eventmie-pro.profile.organizer', $extra = [])
    {
        return parent::index($view, $extra);
    }

    public function profileMailchimp($view = 'vendor.eventmie-pro.profile.mailchimp', $extra = [])
    {
        return parent::index($view, $extra);
    }

    public function profileStripe($view = 'vendor.eventmie-pro.profile.stripe', $extra = [])
    {
        return parent::index($view, $extra);
    }






    // update user
    public function updateAuthUser (Request $request)
    {


        // demo mode restrictions
        if(config('voyager.demo_mode'))
        {
            return error_redirect('Demo mode');
        }


        if(Auth::user()->hasRole('organiser')) {
            $this->validate($request,[
                'name'        => 'required|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'organization_data' => 'nullable',
                'organisation' => 'required_if:organization_data,1|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'email'       => 'required|max:50|email|unique:users,email,'.Auth::id(),
                // 'email' =>    Rule::unique('users')->ignore(Auth::id()),
                'bank_name'    => 'nullable|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'bank_code'    => 'nullable|max:20|regex:/^[a-zA-Z0-9]+$/',
                'bank_branch_name' => 'nullable|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'bank_branch_code' => 'nullable|max:40|regex:/^[a-zA-Z0-9]+$/',
                'bank_account_number' => 'nullable|max:20|regex:/^[0-9]+$/',
                'bank_account_name' => 'nullable|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'bank_account_phone' => ['nullable', new PhoneUk,new PhoneDigits],
                'phone' => ['required', new PhoneUk,new PhoneDigits],
                'seller_name' => 'nullable|max:40|regex:/^[a-zA-Z_\-\s]*$/',
            ],[ 'organisation.required_if' => 'The Organization name is required.',
                'organisation.regex' => 'The Organization name should be Alphabets and spaces.',
                'seller_name.regex'=> 'The Seller name should be Alphabets and spaces.',
                'bank_name.regex' => 'The Bank name should be Alphabets and spaces.',
                'name.regex' => 'The Name name should be Alphabets and spaces.',
                'bank_code.regex' => 'The Bank code should be Alphabets,Numbers and spaces.',
                'bank_branch_name.regex' => 'The Bank Branch name should be Alphabets and spaces.',
                'bank_branch_code.regex' => 'The Bank Branch code should be Alphabets,Numbers and spaces.',
                'bank_account_number.regex' => 'The Bank Account number should be Numbers only.',
                'bank_account_phone.regex'  => 'The Bank Account phone should be 10 or 11 Digits.',
                'phone.regex'  => 'The Mobile should be 10 or 11 Digits.',]);
        } else {
            $this->validate($request,[
                'name'        => 'required|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'email'       => 'required|max:50|email|unique:users,email,'.Auth::id(),
                // 'email' =>    Rule::unique('users')->ignore(Auth::id()),
                'bank_name'    => 'nullable|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'bank_code'    => 'nullable|max:20|regex:/^[a-zA-Z0-9]+$/',
                'bank_branch_name' => 'nullable|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'bank_branch_code' => 'nullable|max:40|regex:/^[a-zA-Z0-9]+$/',
                'bank_account_number' => 'nullable|max:20|regex:/^[0-9]+$/',
                'bank_account_name' => 'nullable|max:40|regex:/^[a-zA-Z_\-\s]*$/',
                'bank_account_phone' => ['nullable', new PhoneUk,new PhoneDigits],
                'phone' => ['required', new PhoneUk,new PhoneDigits],
            ],[
            'name.regex' => 'The Name name should be Alphabets and spaces.',
            'bank_code.regex' => 'The Bank code should be Alphabets,Numbers and spaces.',
            'bank_branch_name.regex' => 'The Bank Branch name should be Alphabets and spaces.',
            'bank_branch_code.regex' => 'The Bank Branch code should be Alphabets,Numbers and spaces.',
            'bank_account_number.regex' => 'The Bank Account number should be Numbers only.',
            'bank_account_phone.regex'  => 'The Bank Account phone should be 10 or 11 Digits.',
            'phone.regex'  => 'The Mobile should be 10 or 11 Digits.',
            'phone.max'=>'The Mobile number should accept 10 or 11 Digits',]);
        }
        
        if(!empty($request->current))
        {
            $data = $this->updateAuthUserPassword($request);

            if($data['status'] == false)
            {
                return error_redirect($data['errors']);
            }else if($data['status'] == true){
                $msg = 'Pin '.__('eventmie-pro::em.saved').' '.__('eventmie-pro::em.successfully');
                return success_redirect($msg, route('eventmie.resetPin'));
            }
        }
        
        $user = User::find(Auth::id());

        if($user->phone != $request->phone){
            $user->markMobileAsNotVerified();
        }
        if($user->email != $request->email){
            $user->markEmailAsNotVerified();
        }
        $user->name                  = $request->name;
        $user->email                 = $request->email;
        $user->address               = $request->address;
        $user->phone                 = $request->phone;

        //CUSTOM
        $user->taxpayer_number                 = $request->taxpayer_number;
        if(Auth::user()->hasRole('organiser') && $request->has('stripe_data')){
            $this->organizerCountry($request, $user);
        }
        if(Auth::user()->hasRole('organiser')&& $request->has('organization_data')){
            $user->org_description       = $request->org_description;
            $user->org_facebook          = $request->org_facebook;
            $user->org_instagram         = $request->org_instagram;
            $user->org_youtube           = $request->org_youtube;
            $user->org_twitter           = $request->org_twitter;
            $user->organisation          = $request->organisation;
        }
        if(Auth::user()->hasRole('organiser') && $request->has('seller_data')) {


            $this->sellerInfo($request, $user);
            $path   = 'users/';
            if($request->hasfile('seller_signature'))
            {

                $request->validate([
                    'seller_signature' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',

                ]);

                $file = $request->file('seller_signature');

                $extension       = $file->getClientOriginalExtension(); // getting image extension
                $avatar          = time().rand(1,988).'.'.$extension;
                $file->storeAs('public/'.$path, $avatar);

                $user->seller_signature    = $path.$avatar;

            }

            if(empty($user->seller_signature) || $user->seller_signature == 'users/default.png')
            {
                $request->validate([
                    'seller_signature' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',

                ]);
            }
        }
        $this->mailchimp($request, $user);

        if($request->has('avatar')){
            $this->uploadImage($request, $user);
        }
        if($request->has('organization_logo')){
            $this->uploadImage($request, $user);
        }
        

        //CUSOTM
        $this->updatebankDetails($request, $user);



        $user->save();

        // redirect no matter what so that it never turns back
        $msg = __('eventmie-pro::em.saved').' '.__('eventmie-pro::em.successfully');
        $route = route('eventmie.profile');
        if($request->has('route_name')){
            $route =  $request->route_name;
        }
        return success_redirect($msg,  $route);

    }
    public function updateSupport(Request $request){
          // demo mode restrictions
          if(config('voyager.demo_mode'))
          {
              return error_redirect('Demo mode');
          }


          if(Auth::user()->hasRole('organiser')) {
              $this->validate($request,[
                  'support_email'       => 'required|max:50|email',
                  'support_phone'       => 'required|',
                  'support_address'       => 'required|max:500',
                ],[
                    'support_email.required' => 'Email is Required',
                    'support_email.email' => 'Not a valid Email address',
                    'support_phone.required' =>'Phone Number is required.',
                    'support_phone.regex'  => 'The phone should be 10 or 11 Digits.']);
          }
          $user = User::find(Auth::id());
          if(Auth::user()->hasRole('organiser')) {
            $user->support_email       = $request->support_email;
            $user->support_phone          = $request->support_phone;
            $user->support_address = $request->support_address;
          }

          $user->save();

          // redirect no matter what so that it never turns back
          $msg = __('eventmie-pro::em.saved').' '.__('eventmie-pro::em.successfully');
          return success_redirect($msg, route('eventmie.profile-support'));
    }
    // update user role
    public function updateAuthUserRole(Request $request)
    {
        $this->validate($request, [
            'organisation'  => 'required',
        ]);

        $manually_approve_organizer = (int)setting('multi-vendor.manually_approve_organizer');


        $user = User::find(Auth::id());

        // manually aporove organizer setting on then don't change role
        if(empty($manually_approve_organizer))
        {
            //CUSTOM
            $user->roles()->sync([3]);
            //CUSTOM

            $user->role_id      = 3;

        }

        $user->organisation = $request->organisation;

        $user->save();

        // ====================== Notification ======================
        // Manual Organizer approval email
        $msg[]                  = __('eventmie-pro::em.name').' - '.$user->name;
        $msg[]                  = __('eventmie-pro::em.email').' - '.$user->email;
        $extra_lines            = $msg;

        $mail['mail_subject']   = __('eventmie-pro::em.requested_to_become_organiser');
        $mail['mail_message']   = __('eventmie-pro::em.become_organiser_notification');
        $mail['action_title']   = __('eventmie-pro::em.view').' '.__('eventmie-pro::em.profile');
        $mail['action_url']     = route('eventmie.profile');
        $mail['n_type']         = "Approve-Organizer";
        if(empty($manually_approve_organizer))
        {
            // Became Organizer successfully email
            $msg[]                  = __('eventmie-pro::em.name').' - '.$user->name;
            $msg[]                  = __('eventmie-pro::em.email').' - '.$user->email;
            $extra_lines            = $msg;

            $mail['mail_subject']   = __('eventmie-pro::em.became_organiser_successful');
            $mail['mail_message']   = __('eventmie-pro::em.became_organiser_successful_msg');
            $mail['action_title']   = __('eventmie-pro::em.view').' '.__('eventmie-pro::em.profile');
            $mail['action_url']     = route('eventmie.profile');
            $mail['n_type']         = "Approved-Organizer";
        }

        /* CUSTOM */
        $mail['user'] = $user;
        /* CUSTOM */

        // notification for
        $notification_ids       = [
            1, // admin
            $user->id, // logged in user by
        ];

        $users = User::whereIn('id', $notification_ids)->get();
        try {
            // \Notification::locale(\App::getLocale())->send($users, new MailNotification($mail, $extra_lines));
            //CUSTOM
            \App\Jobs\RegistrationEmailJob::dispatch($mail, $users, 'become_organizer')->delay(now()->addSeconds(10));
            // test
            // return view('email_templates.becomeOrganizer', compact('mail'));
            //CUSTOM
        } catch (\Throwable $th) {}
        // ====================== Notification ======================


        return redirect()->route('eventmie.profile');
    }

    /**
     *  custom functions start
     */

    // mailchimp fields
    protected function mailchimp(Request $request, $user = null)
    {
        $user->mailchimp_apikey      = $request->mailchimp_apikey;
        $user->mailchimp_list_id     = $request->mailchimp_list_id;

    }

    /**
     *  upload imgate
     */
    protected function uploadImage(Request $request, User $user)
    {
        $path   = 'users/';

        // for image
        if($request->hasfile('avatar'))
        {
            $request->validate([
                'avatar' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',

            ]);

            $file = $request->file('avatar');

            $extension       = $file->getClientOriginalExtension(); // getting image extension
            $avatar          = time().rand(1,988).'.'.$extension;
            $file->storeAs('public/'.$path, $avatar);

            $user->avatar    = $path.$avatar;

        }

        // if(empty($user->avatar) || $user->avatar == 'users/default.png')
        // {
        //     $request->validate([
        //         'avatar' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',

        //     ]);
        // }
        if($request->hasfile('organization_logo'))
        {
            $request->validate([
                'organization_logo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',

            ]);

            $file = $request->file('organization_logo');

            $extension       = $file->getClientOriginalExtension(); // getting image extension
            $avatar          = time().rand(1,988).'.'.$extension;
            $file->storeAs('public/'.$path, $avatar);

            $user->organization_logo    = $path.$avatar;

        }

        if(Auth::user()->hasRole('organiser'))
        {
            if($request->hasfile('seller_signature'))
            {
                $request->validate([
                    'seller_signature' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',

                ]);

                $file = $request->file('seller_signature');

                $extension       = $file->getClientOriginalExtension(); // getting image extension
                $avatar          = time().rand(1,988).'.'.$extension;
                $file->storeAs('public/'.$path, $avatar);

                $user->seller_signature    = $path.$avatar;

            }

            // if(empty($user->seller_signature) || $user->seller_signature == 'users/default.png')
            // {
            //     $request->validate([
            //         'seller_signature' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',

            //     ]);
            // }

        }

    }

    /**
     *  seller info
     * */
    protected function sellerInfo(Request $request, User $user)
    {
        $request->validate([
            'seller_name'        => 'required|string|max:256',
            'seller_info'        => 'required|string|max:256',
            'seller_tax_info'    => 'required|string|max:256',
            'seller_note'        => 'required|string|max:256',
        ]);

        $user->seller_name       = $request->seller_name;
        $user->seller_info       = $request->seller_info;
        $user->seller_tax_info   = $request->seller_tax_info;
        $user->seller_note       = $request->seller_note;
    }

    /**
     *  country
     * */
    protected function organizerCountry(Request $request, User $user)
    {
        $request->validate([
            'country'        => 'required',
        ]);

        $user->country       = $request->country;
    }
}
