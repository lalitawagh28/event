<?php

namespace App\Http\Controllers\Eventmie;

use Auth;
use Carbon\Carbon;
use App\Http\Helper;
use App\Models\User;
use App\Models\Event;
use App\Models\Booking;
use App\Models\DeliveryCharge;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Classiebit\Eventmie\Models\Venue;
use Facades\Classiebit\Eventmie\Eventmie;
use Classiebit\Eventmie\Http\Controllers\MyEventsController as BaseMyEventsController;
use App\Models\Promocode;
use App\Notifications\EmailOrganizerEvent;
use Exception;
use Illuminate\Support\Facades\Storage;
use Image;
class MyEventsController extends BaseMyEventsController
{
    public $event;
    public $booking;
    public $promocode;
    public $delivery_charges;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // CUSTOM
        $this->event      = new Event;
        $this->booking    = new Booking;
        $this->promocode = new Promocode;
        $this->delivery_charges = new DeliveryCharge();
        // CUSTOM

    }

    // only organiser can see own events and admin or customer can't see organiser events

    public function index($view = 'vendor.eventmie-pro.myevents.index', $extra = [])
    {


        // get prifex from eventmie config
        $path = false;
        if(!empty(config('eventmie.route.prefix')))
            $path = config('eventmie.route.prefix');
        // admin can't see organiser bookings
        // if(Auth::user()->hasRole('admin'))
        // {
        //     return redirect()->route('voyager.events.index');
        // }

        //CUSTOM
        $is_admin   = Auth::user()->hasRole('admin') ? 1 : 0;
        //CUSTOM

        // organizer
        $organizer_id = Auth::id();

        return view($view, compact('path', 'organizer_id', 'extra', 'is_admin'));

        //CUSTOM

    }

    /**
     * Create-edit event
     *
     * @return array
     */
    public function form($slug = null, $view = 'vendor.eventmie-pro.events.form', $extra = [])
    {

        

        $event  = [];
        $seatchart = null;
        // get event by event_slug
        if($slug)
        {
            $event  = $this->event->get_event($slug);
            $event  = $event->makeVisible('online_location');

            /* CUSTOM */
            $event  = $event->makeVisible('youtube_embed');
            $event  = $event->makeVisible('vimeo_embed');
            if($event->seat_image){
                $seatchart = Storage::disk('azure')->temporaryUrl($event->seat_image, now()->addMinutes(5));
            }
            $event = $this->get_azure_events_images($event);
            /* CUSTOM */


            // user can't edit other user event but only admin can edit event's other users
            if(!Auth::user()->hasRole('admin') && Auth::id() != $event->user_id)
                return redirect()->route('eventmie.events_index');
        }

        $organisers = [];
        // fetch organisers dropdown
        // only if login user is admin
        if(Auth::user()->hasRole('admin'))
        {
            // fetch organisers
            $organisers    = $this->event->get_organizers(null);
            foreach($organisers as $key => $val)
                $organisers[$key]->name = $val->name.'  ( '.$val->email.' )';
            if($slug)
            {
                // in case of edit event, organiser_id won't change
                $this->organiser_id = $event->user_id;
            }

        }
        
        $organiser_id             = $this->organiser_id ? $this->organiser_id : 0;
        $selected_organiser       = User::find($this->organiser_id);

        return Eventmie::view($view, compact('event','seatchart', 'organisers', 'organiser_id', 'extra', 'selected_organiser'));
    }

    // create event
    public function store(Request $request)
    {

        // if logged in user is admin
        $this->is_admin($request);

        // 1. validate data
        $request->validate([
            'title'             => 'required|max:40',
            "category_ids"      => 'required',
            "category_ids.*"    => 'required',
            'excerpt'           => 'required|max:512',
            'slug'              => 'required|max:512',
            'description'       => 'required',
            'faq'               => 'nullable',
            'promocodes_ids' => 'nullable',
            'delivery_charge.*' => 'nullable|max:256',
            'delivery_charge_price.*' => 'nullable|numeric|gte:0',
            'delivery_charge_id.*' => 'nullable|numeric',
            'offline_payment_info' => 'nullable|max:2048',
            'bank_payment_info'  => 'nullable|max:256',
            'card_payment_info'  => 'nullable|max:256',
            'terms_info'  => 'nullable',
            'privacy_info'  => 'nullable',
            'faq_info'  => 'nullable',
            // CUSTOM
            'short_url'         => 'nullable|max:256',
            'currency'          => 'nullable',
            'show_reviews'      => 'nullable',
            'guest_star'     => 'nullable',
            'payment_processed' => 'nullable',
            'organised_by'     => 'nullable',
            'enable_additional_information'     => 'nullable',
            'additional_title'     => 'required_if:enable_additional_information,1',
            'more_information'     => 'required_if:enable_additional_information,1',     
            'notes'     => 'nullable',       
            'entrance_gate.*'     => 'nullable|regex:/^[a-zA-Z0-9 \,]*$/',
            'collection_point.*'     => 'nullable|regex:/^[a-zA-Z0-9 \,]*$/',
            // CUSTOM
        ], [
            'category_ids.*' => __('eventmie-pro::em.category').' '.__('eventmie-pro::em.required'),
            'delivery_charge_price.*.gte' => __('eventmie-pro::em.delivery_error')
        ]);

        if($request->delivery_charge){
            forEach( $request->delivery_charge as $key=> $delivery_charge){
                if(!empty($delivery_charge)){
                    $delivery_charge_data = explode(" ",$delivery_charge);
                    $params = [
                        'title' => $delivery_charge_data[0],
                        'rate' => $request->delivery_charge_price[$key],
                        'organiser_id' => Auth::id()
                    ];
                    $this->delivery_charges->create_delivery_charge($params);
                }
            }
        }
        $result             = (object) [];
        $result->title      = null;
        $result->excerpt    = null;
        $result->slug       = null;
        //CUSTOM
        $result->short_url  = null;
        //CUSTOM

        // in case of edit
        if(!empty($request->event_id))
        {
            $request->validate([
                'event_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            ]);

            // check this event id have login user relationship
            $result      = (object) $this->event->get_user_event($request->event_id, $this->organiser_id);

            if(empty($result))
                return error('access denied', Response::HTTP_BAD_REQUEST );

        }

        // title is not equal to before title then apply unique column rule
        if($result->title != $request->title)
        {
            $request->validate([
                'title'             => 'unique:events,title',
            ]);
        }

        // slug is not equal to before slug then apply unique column rule
        if($result->slug != $request->slug)
        {
            $request->validate([
                'slug'             => 'unique:events,slug',
            ]);
        }
          //CUSTOM
        // short_url is not equal to before short_url then apply unique column rule
        if($result->short_url != $request->short_url && !empty($request->short_url))
        {
            $request->validate([
                'short_url'             => 'unique:events,short_url',
            ]);
        }
        //CUSTOM

        $params = [
            "title"         => $request->title,
            "excerpt"       => $request->excerpt,
            "slug"          => $this->slugify($request->slug),
            "description"   => $request->description,
            "faq"           => $request->faq,
            "category_id"   => $request->category_id,
            "featured"      => 0,
            "offline_payment_info" => $request->offline_payment_info,
            'bank_payment_info'  => $request->bank_payment_info,
            'card_payment_info'  => $request->card_payment_info,
            'terms_info'  =>  $request->terms_info,
            'privacy_info'  =>  $request->privacy_info,
            'faq_info'  =>  $request->faq_info,
            'delivery_charge' =>  0,
            //CUSTOM
            "short_url"     => $request->short_url,
            "currency"      => $request->currency,
            "e_soldout"     => !empty($request->e_soldout) ? 1 : 0,
            "show_reviews"  => !empty($request->show_reviews) ? 1 : 0,
            "show_attendee_count" => !empty($request->attendee_count) ? 1 : 0,
            "guest_star"         => $request->guest_star,
            "organised_by"         => $request->organised_by,
            "payment_processed"     => $request->payment_processed,
            "delivery_address_enable" => $request->delivery_address_enable,
            "collection_point_enable" => $request->collection_point_enable,
            "same_as_attendee_enable" => $request->same_as_attendee_enable,
            'enable_additional_information'     =>  $request->enable_additional_information,
            'additional_title'     =>  $request->additional_title,
            'more_information'     =>  $request->more_information,
            'notes'     =>  $request->notes,
            //CUSTOM
        ];
        $path = 'events/'.Carbon::now()->format('FY').'/';
        //Storage::disk('azure')->makeDirectory($path);
        $ticket_logo = null;
        if($request->hasfile('ticket_logo'))
        {
            // if have  image and database have images no images this event then apply this rule
            $request->validate([
                'ticket_logo'        => 'required',
                'ticket_logo'      => 'mimes:jpeg,png,jpg,gif,svg',
            ]);

            $file = $request->file('ticket_logo');


            $extension       = $file->getClientOriginalExtension(); // getting image extension
            $filename     = time().rand(1,988).'.'.$extension;
            $file->storeAs($path, $filename,'azure');
            $ticket_logo    = $filename;
        }

        if($ticket_logo){
            $params['ticket_logo'] = $path.$ticket_logo;
        }
        $invoice_logo = null;
        if($request->hasfile('invoice_logo'))
        {
            // if have  image and database have images no images this event then apply this rule
            $request->validate([
                'invoice_logo'        => 'required',
                'invoice_logo'      => 'mimes:jpeg,png,jpg,gif,svg',
            ]);

            $file = $request->file('invoice_logo');


            $extension       = $file->getClientOriginalExtension(); // getting image extension
            $filename     = time().rand(1,988).'.'.$extension;
            $file->storeAs($path, $filename,'azure');
            $invoice_logo    = $filename;
        }

        if($invoice_logo){
            $params['invoice_logo'] = $path.$invoice_logo;
        }
        //CUSTOM
        if(Auth::user()->hasRole('admin'))
               $params = $this->e_admin_commission($request, $params);
        //CUSTOM


        //featured
        if(!empty($request->featured))
        {
            $request->validate([
                'featured'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            ]);

            $params["featured"]       = $request->featured;
        }

        // Admin controls status via checkbox
        if(Auth::user()->hasRole('admin'))
        {
            $status             = (int) $request->status;
            $params["status"]   = $status ? 1 : 0;
            $params["is_admin"]  = true;


        }
        else
        {
            // organizer event status will be controlled by admin
            // - when organizer login
            // - when creating event
            if(empty($request->event_id))
            {
                // - manual approval on
                if(setting('multi-vendor.verify_publish'))
                {
                    $params["status"] = 0;
                }
                else
                {
                    // - manual approval off
                    $params["status"] = 1;
                }
            }
        }

        // only at the time of event create
        if(!$request->event_id)
        {
            $params["user_id"]       = $this->organiser_id;
            $params["item_sku"]      = (string) time().rand(1,98);
        }
        $old_status = 1;
        if($request->event_id){
            $event = Event::find($request->event_id);
            $old_status = $event->status;
        }
        $event    = $this->event->save_event($params, $request->event_id);

        if(!empty($request->entrance_gate)) {
            $this->event->update_gates($event->id,$request->entrance_gate);
        } else {
            $this->event->update_gates($event->id);
        }
        if(!empty($request->collection_point)) {
            $this->event->update_collection_points($event->id,$request->collection_point);
        } else {
            $this->event->update_collection_points($event->id);
        }

        if(!empty($request->category_ids))
        {
            $category_ids = explode (",", $request->category_ids);
            $event->categories()->sync($category_ids);
        }
        // $payload=[
        //     'store_event_details'=>$params,
        //     'action'=>$request->event_id?'event_details_updated':'event_details_added'
        // ];

        // $webhookResponse = $this->hitWebhook($payload,new $this->event);
        // if(!is_null($webhookResponse))
        // {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        if(empty($event))
            return error(__('eventmie-pro::em.event_not_created'), Response::HTTP_BAD_REQUEST );

        // ====================== Notification ======================
        //send notification after bookings
        // $msg[]                  = __('eventmie-pro::em.event').' - '.$event->title;
        // $extra_lines            = $msg;

        // $mail['mail_subject']   = __('eventmie-pro::em.event_created');
        // $mail['mail_message']   = __('eventmie-pro::em.event_info');
        // $mail['action_title']   = __('eventmie-pro::em.manage_events');
        // $mail['action_url']     = route('eventmie.myevents_index');
        // $mail['n_type']         = "events";

        // /* CUSTOM */
        // $mail['event']          = $event;
        /* CUSTOM */

        $notification_ids       = [1, $this->organiser_id];

        if($request->delivery_charge){
            $params = [
                'event_id' =>  $event->id
            ];
            $exist_delivery_charges = $this->delivery_charges->get_event_delivery_charges_ids($params);
            if($request->delivery_charge_id){
                $delete_delivery_charges =array_diff($exist_delivery_charges,$request->delivery_charge_id);
                if($delete_delivery_charges){
                    $this->delivery_charges->delete_event_delivery_charges($delete_delivery_charges);
                }
            }
            forEach( $request->delivery_charge as $key=> $delivery_charge){

                $delivery_charge_data = explode(" ",$delivery_charge);
                if(@$request->delivery_charge_id[$key]) {
                    $params = [
                        'title' => $delivery_charge_data[0],
                        'rate' => $request->delivery_charge_price[$key],
                        'event_id' => $event->id,
                        'id' => $request->delivery_charge_id[$key]
                    ];
                    $this->delivery_charges->update_event_delivery_charge($params);
                } else {
                    if(!empty($delivery_charge)){
                        $params = [
                            'title' => $delivery_charge_data[0],
                            'rate' => $request->delivery_charge_price[$key],
                            'event_id' => $event->id,
                        ];
                        $this->delivery_charges->create_event_delivery_charge($params);
                    }
                }
            }



        }
        // save promocodes
        $this->save_promocode($request, $event->id);
        //$this->save_delivery_charges($request, $event->id);

        $users = User::whereIn('id', $notification_ids)->get();
        try {
            // \Notification::locale(\App::getLocale())->send($users, new MailNotification($mail, $extra_lines));

            //CUSTOM
            //\App\Jobs\RegistrationEmailJob::dispatch($mail, $users, 'event')->delay(now()->addSeconds(10));
            // test
            // return view('email_templates.event', compact('mail'));
            //CUSTOM
        } catch (\Throwable $th) {}
        // ====================== Notification ======================


        // in case of create
        if(empty($request->event_id))
        {
            // set step complete
            $this->complete_step($event->is_publishable, 'detail', $event->id);
            return response()->json(['status' => true, 'id' => $event->id, 'organiser_id' => $event->user_id , 'slug' => $event->slug ]);
        }
        // update event in case of edit
        $event      = $this->event->get_user_event($request->event_id, $this->organiser_id);
        if(Auth::user()->hasRole('admin')){
            $status             = (int) $request->status;
            if($old_status == 0 && $status  == 1 && $event->publish == 1)
            {
                $user = User::find($this->organiser_id);
                try{
                    $user->notify(new EmailOrganizerEvent($event));
                } catch(Exception $e) {
                    \Log::info($e->getMessage());
                }
            }
        }
        return response()->json(['status' => true, 'slug' => $event->slug]);
    }
     /**
     *  custom function start
     */

    // save promocode
    public function save_promocode(Request $request, $event_id = null)
    {
        // save promocodes
        $promocodes = [];
        $params     = [];

        if(!empty($request->promocodes_ids))
        {
            $promocodes = explode (",", $request->promocodes_ids);

            foreach($promocodes as $key => $value)
            {
                $params[$key]['event_id']    =  $event_id;
                $params[$key]['promocode_id'] =  $value;
            }
            $this->promocode->save_event_promocode($params, $event_id);
        }

    }

    // save promocode
    public function save_delivery_charges(Request $request, $event_id = null)
    {
        // save promocodes
        $promocodes = [];
        $params     = [];

        if(!empty($request->delivery_charges_ids))
        {
            $promocodes = explode (",", $request->delivery_charges_ids);

            foreach($promocodes as $key => $value)
            {
                $params[$key]['event_id']    =  $event_id;
                $params[$key]['delivery_charge_id'] =  $value;
            }
            $this->delivery_charges->save_event_delivery_charges($params, $event_id);
        }

    }


    // crate location of event
    public function store_location(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        // 1. validate data
        $request->validate([
            'event_id'          => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            'country_id'        => 'numeric|min:0',
            // 'venue'             => 'required|max:256',
            'address'           => 'max:512',
            'city'              => 'max:256',
            'state'             => 'max:256',
            'zipcode'           => 'max:64',
            'latitude'          => ['nullable','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude'         => ['nullable','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'online_location'   => 'nullable|string',

        ]);


        if(empty($request->online_event))
        {
            $request->validate([
                'venues_ids'        => 'required'
            ]);
        }

        $venue = Venue::whereId($request->venues_ids)->first();

        $params = [
            "country_id"    => !empty($venue) ? $venue->country_id : 0,
            "venue"         => !empty($venue) ? $venue->title : null,
            "address"       => !empty($venue) ? $venue->address : null,
            "city"          => !empty($venue) ? $venue->city : null,
            "zipcode"       => !empty($venue) ? $venue->zipcode : null,
            "state"         => !empty($venue) ? $venue->state : null,
            "latitude"      => !empty($venue) ? $venue->glat : null,
            "longitude"     => !empty($venue) ? $venue->glong : null,
            "online_location" => $request->online_location,

            //CUSTOM
            'youtube_embed' => $request->youtube_embed,
            'vimeo_embed'   => $request->vimeo_embed,
            //CUSTOM
        ];

        // only at the time of event create
        if(!$request->event_id)
        {
            $params["user_id"]       = $this->organiser_id;
        }

        // check this event id have login user or not
        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);
        if(empty($check_event))
        {
            return error('access denied', Response::HTTP_BAD_REQUEST );
        }

        $location   = $this->event->save_event($params, $request->event_id);
        if(empty($location))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }

        // get update event
        $event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        $venues_ids = [];
        if(!empty($request->venues_ids))
            $venues_ids = explode(",",$request->venues_ids);

        $event->venues()->sync($venues_ids);

        // set step complete
        $this->complete_step($event->is_publishable, 'location', $request->event_id);
        $params['event_id']=$request->event_id;
        // $payload=[
        //     'store_location'=>$params,
        //     'action'=>'event_location_updated'
        // ];

        // $webhookResponse = $this->hitWebhook($payload,new $this->event);
        // if(!is_null($webhookResponse))
        // {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        return response()->json(['status' => true, 'event' => $event]);
    }

    // crate media of event
    public function store_media(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);
                
        $result              = [];
        // check this event id have login user or not
        $result    = $this->event->get_user_event($request->event_id, $this->organiser_id);


        $images    = [];
        $poster    = null;
        $thumbnail = null;
        $rules = [];
        $rules['event_id'] =  'required|numeric|min:1|regex:^[1-9][0-9]*$^';
        if(!$result->poster) {
            $rules['poster']  =  'required';
        } else {
            $rules['poster'] =  'nullable';
        }
        if(!$result->listview) {
            $rules['listview']  =  'required';
        } else {
            $rules['listview'] =  'nullable';
        }
        if(!$result->calendar_image) {
            $rules['calendar_image']  =  'required';
        } else {
            $rules['calendar_image'] =  'nullable';
        }
        if(!$result->thumbnail) {
            $rules['thumbnail']  =  'required';
        } else {
            $rules['thumbnail'] =  'nullable';
        }
        if(!$result->checkout_banner) {
            $rules['checkout_banner']  =  'required';
        } else {
            $rules['checkout_banner'] =  'nullable';
        }
        if(!$result->mat_trending) {
            $rules['mat_trending']  =  'required';
        } else {
            $rules['mat_trending'] =  'nullable';
        }
        if(!$result->mat_card) {
            $rules['mat_card']  =  'required';
        } else {
            $rules['mat_card'] =  'nullable';
        }
        if(!$result->mat_detail) {
            $rules['mat_detail']  =  'required';
        } else {
            $rules['mat_detail'] =  'nullable';
        }
        if(!$result->mat_poster) {
            $rules['mat_poster']  =  'required';
        } else {
            $rules['mat_poster'] =  'nullable';
        }
        $request->validate( $rules);
        // 1. validate data

        // vedio link optional so if have vedio ling then validation apply
        if(!empty($request->video_link) && (!empty($request->video_link[0])))
        {
            $request->validate([
                //CUSTOM
                'video_link'      => 'required|array',
                'video_link.*'    => 'required',
                //CUSTOM
            ]);
        }


        if(empty($result))
        {
            return error('access denied', Response::HTTP_BAD_REQUEST );
        }

        // for multiple image
        $path = 'events/'.Carbon::now()->format('FY').'/';
        // for thumbnail
        if(!empty($_REQUEST['thumbnail']))
        {
            $params  = [
                'image'  => $_REQUEST['thumbnail'],
                'path'   => 'events',
                'width'  => 512,
                'height' => 512,
            ];
            $thumbnail   = $this->upload_to_azure($params);
           
        }


        if(!empty($_REQUEST['listview']))
        {
            $params  = [
                'image'  => $_REQUEST['listview'],
                'path'   => 'events',
                'width'  => 329,
                'height' => 172,
            ];
            $listview   = $this->upload_to_azure($params);
        }


        if(!empty($_REQUEST['calendar_image']))
        {
            $params  = [
                'image'  => $_REQUEST['calendar_image'],
                'path'   => 'events',
                'width'  => 248,
                'height' => 153,
            ];
            $calendar_image   = $this->upload_to_azure($params);
        }

        if(!empty($_REQUEST['checkout_banner']))
        {
            $params  = [
                'image'  => $_REQUEST['checkout_banner'],
                'path'   => 'events',
                'width'  => 1920,
                'height' => 300,
            ];
            $checkout_banner   = $this->upload_to_azure($params);
        }

        if(!empty($_REQUEST['mat_trending']))
        {
            $params  = [
                'image'  => $_REQUEST['mat_trending'],
                'path'   => 'events',
                'width'  => 135,
                'height' => 240,
            ];
            $mat_trending = $this->upload_to_azure($params);
        }

        

        if(!empty($_REQUEST['mat_card']))
        {
            $params  = [
                'image'  => $_REQUEST['mat_card'],
                'path'   => 'events',
                'width'  => 354,
                'height' => 95,
            ];
            $mat_card = $this->upload_to_azure($params);
        }

        if(!empty($_REQUEST['mat_detail']))
        {
            $params  = [
                'image'  => $_REQUEST['mat_detail'],
                'path'   => 'events',
                'width'  => 375,
                'height' => 300,
            ];
            $mat_detail = $this->upload_to_azure($params);
        }

        if(!empty($_REQUEST['mat_poster']))
        {
            $params  = [
                'image'  => $_REQUEST['mat_poster'],
                'path'   => 'events',
                'width'  => 127,
                'height' => 165,
            ];
            $mat_poster = $this->upload_to_azure($params);
        }


        if(!empty($_REQUEST['poster']))
        {
            $params  = [
                'image'  => $_REQUEST['poster'],
                'path'   => 'events',
                'width'  => 751,
                'height' => 389,
            ];

            $poster   = $this->upload_to_azure($params);
        }

        // for image
        if($request->hasfile('images'))
        {
            // if have  image and database have images no images this event then apply this rule
            $request->validate([
                'images'        => 'required',
                'images.*'      => 'mimes:jpeg,png,jpg,gif,svg',
            ]);

            $files = $request->file('images');

            foreach($files as  $key => $file)
            {
                $extension       = $file->getClientOriginalExtension(); // getting image extension
                $image[$key]     = time().rand(1,988).'.'.$extension;
                $file->storeAs($path, $image[$key],'azure');

                $images[$key]    = $path.$image[$key];
            }
        }
        $ticket_bg = null;

        if(!empty($_REQUEST['ticket_bg']))
        {
            // if have  image and database have images no images this event then apply this rule
            $request->validate([
                'ticket_bg'        => 'required',
                'ticket_bg.*'      => 'mimes:jpeg,png,jpg,gif,svg',
            ]);

            $params  = [
                'image'  => $_REQUEST['ticket_bg'],
                'path'   => 'events',
                'width'  => 2069,
                'height' => 971,
            ];

            $ticket_bg   = $this->upload_to_azure($params);
        }
         //CUSTOM
        // for seatingchart_image
        if($request->hasfile('seatingchart_image'))
        {
            // if have  image and database have images no images this event then apply this rule
            $request->validate([
                'seatingchart_image'      => 'required|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $file = $request->file('seatingchart_image');


            $extension              = $file->getClientOriginalExtension(); // getting image extension
            $seatingchart_image     = time().rand(1,988).'.'.$extension;
            $file->storeAs($path, $seatingchart_image,'azure');
            // $file->storeAs('public/'.$path, $seatingchart_image);

            $seatingchart_image     = $path.$seatingchart_image;

        }
        //CUSTOM

        //CUSTOM
        $video_link         = null;
        if(!empty($request->video_link)) {
            if(is_array($request->video_link)) {
                if(!empty($request->video_link[0])) {
                    $video_link         = json_encode($request->video_link);
                }
            }
        }
        $params = [
            "thumbnail"     => !empty($thumbnail) ? $path.$thumbnail : $result->thumbnail ,
            "listview"     => !empty($listview) ? $path.$listview :  $result->listview ,
            "calendar_image"     => !empty($calendar_image) ? $path.$calendar_image : $result->calendar_image  ,
            "checkout_banner" =>  !empty($checkout_banner) ? $path.$checkout_banner : $result->checkout_banner  ,
            "mat_trending"   =>  !empty($mat_trending) ? $path.$mat_trending : $result->mat_trending  , 
            "mat_card"   =>  !empty($mat_card) ? $path.$mat_card : $result->mat_card  ,
            "mat_detail"   =>  !empty($mat_detail) ? $path.$mat_detail : $result->mat_detail  ,
            "mat_poster"   =>  !empty($mat_poster) ? $path.$mat_poster : $result->mat_poster  , 
            "poster"        => !empty($poster) ? $path.$poster : $result->poster ,
            "video_link"    => $video_link,
            "user_id"       => $this->organiser_id,
        ];

        if($ticket_bg){
            $params['ticket_bg'] = $path.$ticket_bg;
        }

        // if images uploaded
        if(!empty($images))
        {
            if(!empty($result->images))
            {
                $exiting_images = json_decode($result->images, true);

                $images = array_merge($images, $exiting_images);
            }

            $params["images"] = json_encode(array_values($images));

        }

        //CUSTOM
        // if seatingchart_image
        if(!empty($seatingchart_image))
            $params["seatingchart_image"] = $seatingchart_image;

        //CUSTOM

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }

        // get media  related event_id who have created now
        $images   = $this->event->get_user_event($request->event_id, $this->organiser_id);

        // set step complete
        $this->complete_step($images->is_publishable, 'media', $request->event_id);

        $params['event_id']=$request->event_id;
        // $payload=[
        //     'store_media'=>$params,
        //     'action'=>'event_media_updated'
        // ];

        // $webhookResponse = $this->hitWebhook($payload,new $this->event);
        // if(!is_null($webhookResponse))
        // {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        return response()->json(['images' => $images, 'status' => true]);
    }


    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }

    /**
     *  my  event for particular organiser
     */

    public function get_myevents(Request $request)
    {
        // if(Auth::user()->hasRole('admin'))
        // {
        //     return redirect()->route('voyager.events.index');
        // }

        //CUSTOM
        $this->is_admin($request);
        //CUSTOM

        $params   = [
            // 'organiser_id' => Auth::id(),
            //CUSTOM
            'organiser_id' => $this->organiser_id,
            'search'            => $request->search,
            'length'            => $request->length,

            //CUSTOM
        ];

        $myevents    = $this->event->get_my_events($params);

        $myevents->makeVisible(['event_password']);

        if(empty($myevents))
            return error(__('eventmie-pro::em.event').' '.__('eventmie-pro::em.not_found'), Response::HTTP_BAD_REQUEST );

        //CUSTOM
        // add sub organizers
        $myevents = $this->get_sub_organizers($myevents);
        //CUSTOM


        return response([
            'myevents'=> $myevents->jsonSerialize(),
        ], Response::HTTP_OK);

    }

    /**
     *  get organiser with pagination
     */
    public function get_organizers(Request $request)
    {
        //CUSTOM
        if(!Auth::user()->hasRole('admin'))
        {
            $request->validate([
                'search'        => 'required|string|max:256',
            ]);
        }
        //CUSTOM

        $search     = $request->search;
        $organizers = $this->event->get_organizers($search);

        if(empty($organizers))
        {
            return response()->json(['status' => false, 'organizers' => []]);
        }

        foreach($organizers as $key => $val)
            $organizers[$key]->name = $val->name.'  ( '.$val->email.' )';

        return response()->json(['status' => true, 'organizers' => $organizers ]);
    }

    // check login user role
    protected function is_admin(Request $request)
    {
        // if login user is Organiser then
        // organiser id = Auth::id();
        $this->organiser_id = Auth::id();
        
        // if admin is creating event
        // then user Auth::id() as $organiser_id
        // and organiser id will be the id selected from Vue dropdown
        if(Auth::user()->hasRole('admin'))
        {
            $request->validate([
                'organiser_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            ]);
            $this->organiser_id = $request->organiser_id;
        }

    }

    /**
     *  event admin commission
     */
    protected function e_admin_commission(Request $request, $params = [])
    {
        // 1. validate data
        $request->validate([
            'e_admin_commission' => 'nullable|numeric',

        ]);

        $params['e_admin_commission'] = $request->e_admin_commission;

        return $params;
    }

    /**
     *  set event currecny
     */

    protected function setEventCurrency(Request $request)
    {
        $this->currency = setting('regional.currency_default');

        // get event by event_id
        $event          = $this->event->get_event(null, $request->event_id);

        // if event not found then access denied
        if(empty($event))
            return ['status' => false, 'error' =>  __('eventmie-pro::em.event').' '.__('eventmie-pro::em.not_found')];

        if(!empty($event->currency))
            $this->currency = $event->currency;

    }


    /**
     *  save sub organizers
     */

    public function save_sub_organizers(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $event_id  = (int) $request->event_id;

        // convet json data into array
        $pos       =  json_decode($request->pos_ids, true);
        $scanner   =  json_decode($request->scanner_ids, true);

        // for pos sub organizers
        $this->prepare_sub_organizers($pos, $event_id, 4);

        // for scanner sub organizers
        $this->prepare_sub_organizers($scanner, $event_id, 5);

        // redirect
        $url = route('eventmie.myevents_index');
        $msg = __('eventmie-pro::em.add').' '.__('eventmie-pro::em.sub_organizer').' '.__('eventmie-pro::em.successful');
        session()->flash('status', $msg);

        return success_redirect($msg, $url);
    }

    /**
     *  get selected sub organizers for particular event
     */

    protected function get_sub_organizers($myevents = null)
    {
        // fetch all ids of all events
        $event_ids = $myevents->pluck('id');

        // get sub organizers
        $sub_organizers = \DB::table('user_roles')->select('user_roles.*', 'users.name', 'users.email')
                            ->leftJoin('users', 'users.id', '=', 'user_roles.user_id')
                            ->whereIn('event_id', $event_ids)
                            ->where(function ($query) {
                                $query->where('user_roles.role_id', '=', 4)
                                    ->orWhere('user_roles.role_id', '=', 5)
                                    ->orWhere('user_roles.role_id', '=', 6);
                            })
                            ->get();

        // group by event_id
        $sub_organizers   = $sub_organizers->groupBy('event_id');

        //set sub organizer for relevant event_id
        foreach($myevents as $key => $event)
        {
            $myevents[$key]['sub_organizers'] = [];

            //match event_id then attach sub_organizers with group by role_id
            if($sub_organizers->has($event->id))
                $myevents[$key]['sub_organizers'] = $sub_organizers[$event->id]->groupBy('role_id');

        };

        return $myevents;

    }

    /**
     *    get users whose created by login organizer and they have role_id  4 or 5
     */

    public function get_organizer_users(Request $request)
    {
        $this->is_admin($request);

        $data = User::where(['organizer_id' => $this->organiser_id])
                ->where(function ($query) {
                    $query->where('role_id', '=', 4)
                        ->orWhere('role_id', '=', 5)
                        ->orWhere('role_id', '=', 6);
                })->get();

        $sub_organizers = $data->groupBy('role_id');

                 // group by role_id
        return response()->json(['status' => true,  'sub_organizers' => $sub_organizers ]);
    }

    /**
     *  prepare data for sub organizers then insert
     */

    protected function prepare_sub_organizers($sub_organizers = [], $event_id = null, $role_id = null)
    {
        // delete before
        \DB::table('user_roles')->where(['event_id' => $event_id, 'role_id' => $role_id])->delete();

        // if empty sub_organizers then return
        if(empty($sub_organizers))
            return true;

        $params = [];

        //prepare data
        foreach($sub_organizers as $key => $value)
        {
            $params[$key]['user_id']  = $value;
            $params[$key]['role_id']  = $role_id;
            $params[$key]['event_id'] = $event_id;
        }

        // If the record exists, it will be updated and If the record not exists, it will be inserted
        foreach($params as $key => $value)
        {
             \DB::table('user_roles')
                ->updateOrInsert([
                    // check that the record exist or not base on user_id and event_id
                    'event_id' => $value['event_id'],
                    'user_id'  => $value['user_id']
                ],
                    $value
                );
        }

    }

    /**
     *  organizer create user
     */

    public function organizer_create_user(Request $request)
    {
        //CUSTOM
        $this->is_admin($request);
        //CUSTOM

        // 1. validate data
        $request->validate([
            // 'organizer_id'     => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            'role'             => 'required|numeric|min:4|max:6|regex:^[1-9][0-9]*$^',
            'name'             => ['required', 'string', 'max:255'],
            'email'            => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'         => ['required', 'string', 'min:8'],
        ]);

        // create user
        $user = User::create([
                    'name'          => $request->name,
                    'email'         => $request->email,
                    'password'      => \Hash::make($request->password),
                    'role_id'       =>  $request->role,
                    // 'organizer_id'  =>  $request->organizer_id,
                    //CUSTOM
                    'organizer_id'  =>  $this->organiser_id,
                    //CUSTOM
                ]);


         /* CUSTOM */
        // ====================== Notification ======================
        $mail['mail_subject']   = __('eventmie-pro::em.register_success');
        $mail['mail_message']   = __('eventmie-pro::em.get_tickets');
        $mail['action_title']   = __('eventmie-pro::em.login');
        $mail['action_url']     = route('eventmie.login');
        $mail['n_type']         = "user";

        // notification for
        $notification_ids       = [
            1, // admin
            $user->id, // new registered user
        ];

        $users = User::whereIn('id', $notification_ids)->get();

        \App\Jobs\RegistrationEmailJob::dispatch($mail, $users, 'register')->delay(now()->addSeconds(10));
        /* Send email verification link */
        $user->sendEmailVerificationNotification();
        /* Send email verification link */
        // ====================== Notification ======================

        return response()->json(['status' => true]);
    }
    public function delete_poster(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'poster' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }
    public function delete_thumbnail(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'thumbnail' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }

    /**
     *  delete seatchart image
     */

    public function delete_seatchart(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'seatingchart_image' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }
    public function delete_invoice_logo(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'invoice_logo' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }
    public function delete_ticket_logo(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'ticket_logo' => null,
        ];
        $event = Event::find($request->event_id);
        $status   = $this->event->save_event($params, $request->event_id);
        Storage::disk('azure')->delete($event->ticket_logo);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }
    public function delete_ticket_bg(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'ticket_bg' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }

    public function delete_mobile_trending(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'mat_trending' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }
    public function delete_mobile_card(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'mat_card' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }

    public function delete_mobile_detail(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'mat_detail' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }

    public function delete_mobile_poster(Request $request)
    {
        // 1. validate data
        $request->validate([
            'event_id'                => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',

        ]);

        $params = [
            'mat_poster' => null,
        ];

        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['status' => true]);
    }





    /**
     * export_attendees
     */

    public function export_attendees($slug)
    {
        
        // check event is valid or not
        $event    = $this->event->get_event($slug);
        if(empty($event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }

        $params   = [
            'event_id' => $event->id,
        ];

        // get particular event's bookings
        $bookings = $this->booking->get_event_bookings($params);
        if(empty($bookings))
            return error_redirect('Booking Not Found!');

        // customize column values
        $bookings_csv = [];
        foreach($bookings as $key => $item)
        {
            $bookings[$key]['event_repetitive'] = $item['event_repetitive'] ? __('eventmie-pro::em.yes') : __('eventmie-pro::em.no');
            $bookings[$key]['is_paid']          = $item['is_paid'] ? __('eventmie-pro::em.yes') : __('eventmie-pro::em.no');


            if($item['booking_cancel'] == 1)
                $bookings[$key]['booking_cancel']       = __('eventmie-pro::em.pending');
            elseif($item['booking_cancel'] == 2)
                $bookings[$key]['booking_cancel']       = __('eventmie-pro::em.approved');
            elseif($item['booking_cancel'] == 3)
                $bookings[$key]['booking_cancel']       = __('eventmie-pro::em.refunded');
            else
                $bookings[$key]['booking_cancel']   = __('eventmie-pro::em.no_cancellation');


            if($item['status'])
                $bookings[$key]['status']           = __('eventmie-pro::em.enabled');
            else
                $bookings[$key]['status']           = __('eventmie-pro::em.disabled');


            $bookings[$key]['checked_in']           = $item['checked_in'].' / '.$item['quantity'];

            $bookings[$key]['attendee_name']        = null;
            $bookings[$key]['attendee_email']       = null;
            $bookings[$key]['attendee_phone']       = null;
            $bookings[$key]['processing_fee']       = $bookings[$key]['tax'];
            if(!$bookings[$key]['promocode']) {
                $bookings[$key]['voucher_code']       = 'NA';
                $bookings[$key]['discount']       =  0;
            } else {
                $bookings[$key]['voucher_code']       = $bookings[$key]['promocode'];
                $bookings[$key]['discount']       = $bookings[$key]['promocode_reward'];
            }
            if($bookings[$key]['enabled_additional_information']) {
                if($event->enable_additional_information == 1 && $event->additional_title){
                    $bookings[$key][$event->additional_title] = 'Yes';
                }
            } else {
                if($event->enable_additional_information == 1 && $event->additional_title){
                    $bookings[$key][$event->additional_title] = 'No';
                }
                
            }
            //attendees
            if(!empty($bookings[$key]['attendees']))
            {
                $bookings[$key]['attendee_name']  =  @$bookings[$key]['attendees'][0]['name'];
                $bookings[$key]['attendee_address'] = @$bookings[$key]['attendees'][0]['address'];
                $bookings[$key]['attendee_phone'] = @$bookings[$key]['attendees'][0]['phone'];

            }
        }

        // convert array to collection for csv
        $bookings = collect($bookings);

        // create object of laracsv
        $csvExporter = new \Laracsv\Export();
        if($event->enable_additional_information == 1 && $event->additional_title && $event->enable_additional_information == 1){
            $column_names = [

                //events fields which will be include
                'id',
                'event_title',
                'event_start_date',
                'event_end_date',
                'event_start_time',
                'event_end_time',
                'event_repetitive',
    
                'customer_name',
                'customer_email',
    
                'attendee_name',
                'attendee_address',
                'attendee_phone',
    
                'order_number',
                'ticket_category',
                'price',
                'quantity',
                'voucher_code',
                'discount',
                'processing_fee',
                'net_price',
                'currency',
                'transaction_id',
                'is_paid',
                'payment_type',
    
                'booking_cancel',
                'status',
                $event->additional_title,
                'checked_in',
    
                'created_at',
                'updated_at'
            ];
        } else {
            $column_names = [

                //events fields which will be include
                'id',
                'event_title',
                'event_start_date',
                'event_end_date',
                'event_start_time',
                'event_end_time',
                'event_repetitive',
    
                'customer_name',
                'customer_email',
    
                'attendee_name',
                'attendee_address',
                'attendee_phone',
    
                'order_number',
                'ticket_category',
                'price',
                'quantity',
                'voucher_code',
                'discount',
                'processing_fee',
                'net_price',
                'currency',
                'transaction_id',
                'is_paid',
                'payment_type',
    
                'booking_cancel',
                'status',
                'checked_in',
    
                'created_at',
                'updated_at'
            ];
        }
       
        // create csv
        $csvExporter->build($bookings, $column_names);

        // download csv
        $csvExporter->download($event->slug.'-attendies.csv');
    }

    public function hitWebhook($data,$model)
    {
        $response = $model->setWebhookParameters($data);
        return $response;
    }

    public function myevents_delivery_charges(){
        $delivery_charges  = $this->delivery_charges->get_delivery_charges(Auth::id());
        if(!count($delivery_charges))
        {
            return response()->json(['status' => false]);
        }
        return response()->json(['status' => true, 'delivery_charges' => $delivery_charges ]);
    }
}
