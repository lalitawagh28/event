<?php

namespace App\Http\Controllers\Eventmie\Voyager;

use App\Models\AgentTicket;
use App\Models\Booking;
use Classiebit\Eventmie\Models\Event;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class SalesReportController extends VoyagerBaseController
{
    use BreadRelationshipParser;
    public $event;
    public function __construct()
    {
        $this->middleware('auth');
        $this->event    = new Event;
    }


    public function getVoucherCodeByEvent(Request $request)
    {
        
        $event = Event::where('id',$request->eventId)->with('tickets')->first();
      
        $ticketIds = [];
        foreach($event->tickets as $ticket)
        {
            array_push($ticketIds,$ticket->id);
        }
        

       $vouchers = AgentTicket::whereIn('ticket_id',$ticketIds)->groupBy('coupon_code')->get();

       $vouchersArray = [];
       foreach($vouchers as $voucher)
       {
            $booking = Booking::wherePromocode($voucher->coupon_code)->first();
            if(!is_null($booking))
            {
                array_push( $vouchersArray,$voucher);
            }
       }
   
        return response()->json(['vouchers' => $vouchersArray]);
        
    }

    public function getAgentByEvent(Request $request)
    {
        $event = Event::where('id',$request->eventId)->with('tickets')->first();

        $ticketIds = [];
        foreach($event->tickets as $ticket)
        {
            array_push($ticketIds,$ticket->id);
        }

       $agents = AgentTicket::whereIn('ticket_id',$ticketIds)->with('agent')->get()->unique('agent_id');
       return response()->json(['agents' => $agents]);
    }
}
