<?php

namespace App\Http\Controllers\Eventmie\Voyager;

use App\Http\Controllers\Controller;
use App\Http\Helper;
use App\Http\Requests\AgentStoreRegisterRequest;
use App\Http\Requests\ChangeEmailAddressRequest;
use App\Http\Requests\ChangePhoneNumberRequest;
use App\Http\Requests\StoreRegisterRequest;
use App\Models\RegisterCountry;
use App\Models\Title;
use App\Notifications\CustomRegisterationNotification;
use App\Notifications\EmailOneTimePasswordNotification;
use App\Notifications\SmsOneTimePasswordNotification;
use Classiebit\Eventmie\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\Setting;

class RegistrationController extends Controller
{
    public function index(){
        $titles = Title::orderBy('id', 'asc')->pluck("name", "id");
        $countries = RegisterCountry::where('enable',1)->orderBy("name")->pluck("name", "id");
        $countryWithFlags = RegisterCountry::where('enable',1)->orderBy("name")->get();
        $defaultCountry = RegisterCountry::find(config('otp.default_country_code'));
        return view("registration.personal-detail",compact("titles","countries","countryWithFlags","defaultCountry"));
    }
    public function agent_register(){
        $titles = Title::orderBy('id', 'asc')->pluck("name", "id");
        $countries = RegisterCountry::where('enable',1)->orderBy("name")->pluck("name", "id");
        $countryWithFlags = RegisterCountry::where('enable',1)->orderBy("name")->get();
        $defaultCountry = RegisterCountry::find(config('otp.default_country_code'));
        return view("registration.agent-personal-detail",compact("titles","countries","countryWithFlags","defaultCountry"));
    }

    public function store(StoreRegisterRequest $request)
    {
    
        $data = $request->validated();
       
        $data['password'] = Hash::make($data['password']);

        $data['phone'] = Helper::normalizePhone($data['phone']);
        $data['name'] = $data['first_name'] . ' ' . $data['middle_name'] . ' ' . $data['last_name'];
        $data['role_id'] = Role::where('name','organiser')->first()->id;
        
        DB::beginTransaction();
        try {
          
            /** @var \App\Models\User $user */
            $user = Auth::user();
            if (!$user) {
                $validateUser = Helper::validateUser(['email' => $request->get('email'),'phone' => $request->get('phone')]);
                if(is_null($validateUser))
                {
                    $user = User::updateOrCreate($data);
                } else {
                    return redirect()->back()->withErrors($validateUser['errors']);
                }
            }
           
            //event(new Registered($user));

            Auth::attempt(request()->only('email', 'password'));

            $user->logRegistrationStep("personal_details");
            if(config('otp.mobile_otp_enable')) {
                $user->notify(new SmsOneTimePasswordNotification($user->generateOtp("sms")));
            }
            if(config('otp.email_otp_enable')) {
                $user->notify(new EmailOneTimePasswordNotification($user->generateOtp("email")));
            }
            
            $payload=[
                'notification_type'=>'user_registration',
                'payload'=> [
                    'first_name'=> $data['first_name'],
                    'last_name'=> $data['last_name'],
                    'email'=> $data['email'],
                    'phone' =>$data['phone'],
                    'password'=>  $data['password']
                ]
            ];

            Helper::notifyThroughWebhook($payload);

            $clienturl = (config('app.env') == 'production') ? 'https://dhigna.com/callback' : 'https://uat.dhigna.com/callback';

            $clientRepository =  app('Laravel\Passport\ClientRepository');
            $clientRepository->create($user->id,$request->get('first_name').' '.$request->get('last_name'),$clienturl);

            DB::commit();
            if(config('otp.mobile_otp_enable')) {
                return redirect()->route('admin.register.mobileVerification');
            }
            if(config('otp.email_otp_enable')) {
                return redirect()->route('admin.register.emailVerification');
            }

            if(config('otp.registation_success_email')) {
                $user->notify(new CustomRegisterationNotification($user));
            }


            return redirect()->route("voyager.organizer.dashboard");
        } catch (\Exception $exception) {
           // dd($exception);
            DB::rollback();
            \Log::error($exception);
            if ($exception instanceof ValidationException){
                throw $exception;
            }
            return redirect()->route("admin.register.personal_detail");
        }
        
        
    }

    public function agent_store(AgentStoreRegisterRequest $request)
    {
     
        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);

        $data['phone'] = Helper::normalizePhone($data['phone']);
        $data['name'] = $data['first_name'] . ' ' . $data['middle_name'] . ' ' . $data['last_name'];
        $data['role_id'] = Role::where('name','agent')->first()->id;
        
        DB::beginTransaction();
        try {
            /** @var \App\Models\User $user */
            $user = Auth::user();
            if (!$user) {
                $validateUser = Helper::validateUser(['email' => $request->get('email'),'phone' => $request->get('phone')]);
                if(is_null($validateUser))
                {
                    $user = User::updateOrCreate($data);
                } else {
                    return redirect()->back()->withErrors($validateUser['errors']);
                }
            }

            Auth::attempt(request()->only('email', 'password'));

            $user->logRegistrationStep("personal_details");
            if(config('otp.mobile_otp_enable')) {
                $user->notify(new SmsOneTimePasswordNotification($user->generateOtp("sms")));
            }
            
            if(config('otp.email_otp_enable')) {
                $user->notify(new EmailOneTimePasswordNotification($user->generateOtp("email")));    
            }
            $payload=[
                'notification_type'=>'user_registration',
                'payload'=> [
                    'first_name'=> $data['first_name'],
                    'last_name'=> $data['last_name'],
                    'email'=> $data['email'],
                    'phone' =>$data['phone'],
                    'password'=>  $data['password']
                ]
            ];

            Helper::notifyThroughWebhook($payload);

            $clienturl = (config('app.env') == 'production') ? 'https://dhigna.com/callback' : 'https://uat.dhigna.com/callback';

            $clientRepository =  app('Laravel\Passport\ClientRepository');
            $clientRepository->create($user->id,$request->get('first_name').' '.$request->get('last_name'),$clienturl);
            
            DB::commit();
            if(config('otp.mobile_otp_enable')) {
                return redirect()->route('admin.register.mobileVerification');
            }
            if(config('otp.email_otp_enable')) {
                return redirect()->route('admin.register.emailVerification');
            }
            
            if(config('otp.registation_success_email')) {
                $user->notify(new CustomRegisterationNotification($user));
            }
            return redirect()->route("voyager.agents.dashboard");
        } catch (\Exception $exception) {
            DB::rollback();
            \Log::error($exception);
            if ($exception instanceof ValidationException){
                throw $exception;
            }
            return redirect()->route("admin.register.personal_detail");
        }
        
        
    }
    public function signup()
    {
        return view("registration.signup");
    }
    public function mobileVerificationCreate()
    {
        return view("registration.mobile-verification");
    }
    public function mobileVerificationUpdate()
    {
        return view("registration.mobile-verification-update");
    }
    public function mobileVerificationStore(Request $request)
    {
        $request->validate([
            'code' => ['required'],
        ], [
            'code.required' => __('admin_register.enter_otp'),
        ]);
        /** @var \App\Models\User $user */
        $user = User::find(Auth::id());
        $oneTimePassword = $user->oneTimePasswords()->whereType("sms")->first();

        if (is_null($oneTimePassword)) {
            throw ValidationException::withMessages(['code' => __('admin_register.send_otp')]);
        }

        $manualOtp = Setting::where('key','apps.default_otp')->first()?->value;
      
        if (isset($manualOtp) && ($manualOtp == $request->input('code'))) {
            $user->update(['phone_verified_at' => now()]);
            $oneTimePassword->update(['verified_at' => now()]);

            $user->logRegistrationStep("mobile_otp_verification");

            if(config('otp.email_otp_enable')) {
                return redirect()->route('admin.register.emailVerification');
            }
            
            if(config('otp.registation_success_email')) {
                $user->notify(new CustomRegisterationNotification($user));
            }
    
            if($user->role_id == $user->getRoleId('organiser')) {
                return redirect()->route("voyager.organizer.dashboard");
            } elseif ($user->role_id == $user->getRoleId('agent')) {
                return redirect()->route("voyager.agents.dashboard");
            }
        }

        if ($oneTimePassword->code != $request->input('code')) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_didnot_match')]);
        } else if (now()->greaterThan($oneTimePassword->expires_at)) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_expired')]);
        }

        $user->update(['phone_verified_at' => now()]);
        $oneTimePassword->update(['verified_at' => now()]);

        $user->logRegistrationStep("mobile_otp_verification");

        if(config('otp.email_otp_enable')) {
            return redirect()->route('admin.register.emailVerification');
        }
        
        if(config('otp.registation_success_email')) {
            $user->notify(new CustomRegisterationNotification($user));
        }

        if($user->role_id == $user->getRoleId('organiser')) {
            return redirect()->route("voyager.organizer.dashboard");
        } elseif ($user->role_id == $user->getRoleId('agent')) {
            return redirect()->route("voyager.agents.dashboard");
        }
    }

    public function mobileVerificationUpdateStore(Request $request)
    {
        $request->validate([
            'code' => ['required'],
        ], [
            'code.required' => __('admin_register.enter_otp'),
        ]);
        /** @var \App\Models\User $user */
        $user = User::find(Auth::id());
        $oneTimePassword = $user->oneTimePasswords()->whereType("sms")->first();

        if (is_null($oneTimePassword)) {
            throw ValidationException::withMessages(['code' => __('admin_register.send_otp')]);
        }

        $manualOtp = Setting::where('key','apps.default_otp')->first()?->value;
       
        if (isset($manualOtp) && ($manualOtp == $request->input('code'))) {
            $user->update(['phone_verified_at' => now()]);
            $oneTimePassword->update(['verified_at' => now()]);

            $user->logRegistrationStep("mobile_otp_verification");

            if(config('otp.email_otp_enable')) {
                return redirect()->route('admin.register.emailVerification');
            }
            
            if(config('otp.registation_success_email')) {
                $user->notify(new CustomRegisterationNotification($user));
            }
    
            if($user->role_id == $user->getRoleId('organiser')) {
                return redirect()->route("voyager.organizer.dashboard");
            } elseif ($user->role_id == $user->getRoleId('agent')) {
                return redirect()->route("voyager.agents.dashboard");
            }
        }

        if ($oneTimePassword->code != $request->input('code')) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_didnot_match')]);
        } else if (now()->greaterThan($oneTimePassword->expires_at)) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_expired')]);
        }

        $user->update(['phone_verified_at' => now()]);
        $oneTimePassword->update(['verified_at' => now()]);

        $user->logRegistrationStep("mobile_otp_verification");
        if($user->email_verified_at == null && config('otp.email_otp_enable')) {
            return redirect()->route("admin.register.emailVerification");
        }
        $user->logRegistrationStep('email_otp_verification');
        
        if(config('otp.registation_success_email')) {
            $user->notify(new CustomRegisterationNotification($user));
        }

        if($user->role_id == $user->getRoleId('organiser')) {
            return redirect()->route("voyager.organizer.dashboard");
        } elseif ($user->role_id == $user->getRoleId('agent')) {
            return redirect()->route("voyager.agents.dashboard");
        }
    }

    public function emailVerificationCreate()
    {
        return view("registration.email-verification");
    }

    public function emailVerificationStore(Request $request)
    {
        $request->validate([
            'code' => ['required', 'size:6'],
        ], [
            'code.required' => __('admin_register.enter_otp'),
        ]);
        

        /** @var \App\Models\User $user */
        $user = User::find(Auth::id());
       
        $oneTimePassword = $user->oneTimePasswords()->whereType("email")->first();

        if (is_null($oneTimePassword)) {
            throw ValidationException::withMessages(['code' => __('admin_register.send_otp')]);
        }

        $manualOtp = Setting::where('key','apps.default_otp')->first()?->value;
       
        if (isset($manualOtp) && ($manualOtp == $request->input('code'))) {
            $user->update(['email_verified_at' => now()]);
            $oneTimePassword->update(['verified_at' => now()]);

            $user->logRegistrationStep("email_otp_verification");

            if(config('otp.registation_success_email')) {
                $user->notify(new CustomRegisterationNotification($user));
            }
    
            if($user->role_id == $user->getRoleId('organiser')) {
                return redirect()->route("voyager.organizer.dashboard");
            } elseif ($user->role_id == $user->getRoleId('agent')) {
                return redirect()->route("voyager.agents.dashboard");
            }
        }


        if ($oneTimePassword->code != $request->input('code')) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_didnot_match')]);
        } else if (now()->greaterThan($oneTimePassword->expires_at)) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_expired')]);
        }

        $user->update(['email_verified_at' => now()]);
        $oneTimePassword->update(['verified_at' => now()]);

        $user->logRegistrationStep('email_otp_verification');
        
        if(config('otp.registation_success_email')) {
            $user->notify(new CustomRegisterationNotification($user));
        }

        if($user->role_id == $user->getRoleId('organiser')) {
            return redirect()->route("voyager.organizer.dashboard");
        } elseif ($user->role_id == $user->getRoleId('agent')) {
            return redirect()->route("voyager.agents.dashboard");
        }

        
    }

    public function changePhoneNumber(ChangePhoneNumberRequest $request)
    {
        $request->validated();
        $phone = Helper::normalizePhone($request->input("phone"));

        /** @var \App\Models\User $user */
        $user = User::find(Auth::id());
        $user->update(['phone' => $phone]);

        $user->notify(new SmsOneTimePasswordNotification($user->generateOtp("sms")));

        return redirect()->back();
    }

    public function changeEmailAddress(ChangeEmailAddressRequest $request)
    {
        $request->validated();

        /** @var \App\Models\User $user */
        $user = User::find(Auth::id());
        $email = $user->email;
        $user->update(['email' => $request->input("email")]);

        $user->notify(new EmailOneTimePasswordNotification($user->generateOtp("email")));
        return redirect()->back();
    }

    public function resendMobileOnetimePassword()
    {
        /** @var \App\Models\User $user */
        $user = User::find(Auth::id());

        if ($user->hasActiveOneTimePassword("sms")) {
            $oneTimePassword = $user->oneTimePasswords()->whereType("sms")->first();
        } else {
            $oneTimePassword = $user->generateOtp("sms");
        }

        $user->notify(new SmsOneTimePasswordNotification($oneTimePassword));

        return response()->json(['message' => __('admin_register.resend_success'), 'status' => 'success']);
    }

    public function resendEmailOnetimePassword()
    {
        /** @var \App\Models\User $user */
        $user = User::find(Auth::id());

        if ($user->hasActiveOneTimePassword("email")) {
            $oneTimePassword = $user->oneTimePasswords()->whereType("email")->first();
        } else {
            $oneTimePassword = $user->generateOtp("email");
        }
        $user->notify(new EmailOneTimePasswordNotification($oneTimePassword));

        return response()->json(['message' => __('admin_register.resend_success'), 'status' => 'success']);
    }
}