<?php

namespace App\Http\Controllers\Eventmie\Voyager;

use App\Http\Controllers\Controller;
use Classiebit\Eventmie\Models\Booking;
use Classiebit\Eventmie\Models\Event;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function getEventDetails($id) {
        $event = Event::find($id);
        return view('organizer.single_event', compact('event'));
    }

    public function getTransactionDetails($id) {
        $booking = Booking::find($id);
        return view('organizer.single_transaction', compact('booking'));
    }

    public function approve() {
        Auth::logout();
        return view('registration.approval');
    }
}