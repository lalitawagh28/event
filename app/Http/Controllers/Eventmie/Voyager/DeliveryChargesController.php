<?php

namespace App\Http\Controllers\Eventmie\Voyager;

use App\Models\DeliveryCharge;
use Classiebit\Eventmie\Models\Event;
use Illuminate\Http\Request;
use Facades\Classiebit\Eventmie\Eventmie;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class DeliveryChargesController extends VoyagerBaseController
{
    use BreadRelationshipParser;
    public function __construct()
    {
        $this->middleware('auth');
        $this->delivery_charge = new DeliveryCharge();
        $this->event    = new Event;
    }

    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************
    public function index(Request $request)
    {
        
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        //$slug = $this->getSlug($request);
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'delivery-charges')->first();
        
        // Check permission
        //$this->authorize('browse', app($dataType->model_name));
        //m$this->authorize('view', app($dataType->model_name));
        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                
                if(empty($dataRow->where('field', $value)->first()))
                    continue;
                
                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');
                
                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }
            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');
            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }
        
        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = true;
        
        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }
        #Voyager::model('Event')->whereIn('id',)
        
        // $view = 'eventmie::vendor.voyager.events.browse';

        // CUSTOM
        $view = 'admin.delivery-charges';
        // CUSTOM
        return Eventmie::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }
    public function create_delivery_charges(Request $request, $table)
    {
        //$this->authorize('edit');

        $dataType = Voyager::model('DataType')->whereName($table)->first();
        $dataTypeContent = (strlen($dataType->model_name) != 0) ? new $dataType->model_name() : false;

        foreach ($dataType->addRows as $key => $row) {
        $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'add', $isModelTranslatable);

        return Voyager::view('admin.delivery-charges-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    
    public function edit(Request $request, $id)
    {
        

        $dataType = Voyager::model('DataType')->where('slug', '=', 'delivery-charges')->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $query = $model->query();
            //$query->where('organiser_id',Auth::id());
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $query = $query->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $query->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$query, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        //$this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'edit', $isModelTranslatable);


        return Voyager::view('admin.delivery-charges-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function get_event_delivery_charges($event_id = null)
    {
        if (empty($event_id))
            return response()->json(['message' => __('eventmie-pro::em.event_not_found'), 'status' => 0]);

        $params = [
            'event_id' => $event_id
        ];

        // check ticket exist or not
        $check_event = $this->event->get_event_only($params);

        if (empty($check_event)) {
            return response()->json(['message' => __('eventmie-pro::em.event_not_found'), 'status' => 0]);
        }

        $event_delivery_charges = $this->delivery_charge->get_event_delivery_charges($params);

        if (empty($event_delivery_charges))
            return response()->json(['event_delivery_charges' => [], 'status' => 0]);

        

        foreach ($event_delivery_charges as $key => $value) {
            $event_delivery_charges[$key]['currency'] = setting('regional.currency_default');
        }
        return response()->json(['event_delivery_charges' => $event_delivery_charges, 'status' => 1]);
    }

    public function show(Request $request, $id)
    {

        $dataType = Voyager::model('DataType')->where('slug', '=', 'delivery-charges')->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $query = $model->query();
            //$query->where('organiser_id',Auth::id());
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $query = $query->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $query->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$query, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        //$this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        return Voyager::view('admin.delivery-charges-show', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted'));
    }

    public function store(Request $request)
    {
        
        $data = $request->validate([
            'title'        => 'required|min:6|max:25|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'rate'         => 'required|numeric|gt:0',
            'net_price'    => 'nullable|in:excluding,including',
            'admin_delivery_charge'    => 'required|in:0,1',
            'status'      => 'required|in:0,1',
        ]);
        //$data['organiser_id'] = Auth::id();

        $key_check = DeliveryCharge::where('rate', $request->input('rate'))->get()->count();

        if ($key_check > 0) {
            return back()->with([
                'message'    => __('admin.rate_already_exists', ['rate' => $request->input('rate')]),
                'alert-type' => 'error',
            ]);
        }
        DeliveryCharge::create($data);
        return redirect()->route("voyager.delivery-charges.index")->with([
            'message'    => __('admin.delivery_charge_successfully_created'),
            'alert-type' => 'success',
        ]);
    }

    public function update(Request $request,$id)
    {
        $data = $request->validate([
            'title'        => 'required|min:6|max:25|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'rate'         => 'required|numeric|gt:0',
            'net_price'    => 'nullable|in:excluding,including',
            'admin_delivery_charge'    => 'required|in:0,1',
            'status'      => 'required|in:0,1',
        ]);
        //$data['organiser_id'] = Auth::id();

        $key_check = DeliveryCharge::where('rate', $request->input('rate'))->where('id','!=',$id)->get()->count();

        if ($key_check > 0) {
            return back()->with([
                'message'    => __('admin.rate_already_exists', ['rate' => $request->input('rate')]),
                'alert-type' => 'error',
            ]);
        }

        $delivery_charge = DeliveryCharge::find($id);
        $delivery_charge->update($data);
        return redirect()->route("voyager.delivery-charges.index")->with([
            'message'    => __('admin.delivery_charge_successfully_updated'),
            'alert-type' => 'success',
        ]);

    }

    public function delete($id)
    {
        // Check permission
        $this->authorize('delete', Voyager::model('DeliveryCharge'));
        $delivery_charge = DeliveryCharge::find($id);

        $delivery_charge ->destroy($id);


        return back()->with([
            'message'    => __('admin.delivery_charge_successfully_deleted'),
            'alert-type' => 'success',
        ]);
    }

    public function destroy(Request $request, $id)
    {
        

        $dataType = Voyager::model('DataType')->where('slug', '=', 'delivery-charges')->first();

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        $affected = 0;
        
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            //$this->authorize('delete', $data);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }

            $res = $data->delete();

            if ($res) {
                $affected++;

                event(new BreadDataDeleted($dataType, $data));
            }
        }

        $displayName = $affected > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

        $data = $affected
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        return redirect()->route("voyager.delivery-charges.index")->with($data);
    }


}
