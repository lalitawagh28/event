<?php

namespace App\Http\Controllers\Eventmie\ReportManager;

use App\Http\Controllers\Controller;
use App\Http\Helper;
use App\Service\ReportManager\Dashboard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware(['only_reports_manager']);
        // dd(Helper::menu('report_manager', '_json'),route('voyager.report-manager.agent_reports'),url('/report-manager/dashboard'));
        $this->dashboard_service = new Dashboard;
    }

    public function index(Request $request)
    {
        $user_id = Auth::id();
        return $this->dashboard_service->index($request,$user_id);

    }
}
