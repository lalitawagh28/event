<?php

namespace App\Http\Controllers\Eventmie\Organizer;

use Facades\Classiebit\Eventmie\Eventmie;
use Classiebit\Eventmie\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;

class VoyagerAuthController extends Controller
{
    use AuthenticatesUsers;

    public function login()
    {

        if ($this->guard()->user()) {
            dd(true);
            if(Auth::user()->role_id == 3 ) {
                $route= User::find(Auth::id())->getRegistrationStep(Auth::user()->role_id);
                return redirect()->route($route);
            } elseif(Auth::user()->role_id == 8 ) {
                $route= User::find(Auth::id())->getRegistrationStep(Auth::user()->role_id);
                return redirect()->route($route);
            } else {
                return redirect()->route('voyager.dashboard');
            }
        }

        //return Voyager::view('voyager::login');
        return view('eventmie::auth.admin_login');
    }

    public function postLogin(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /*
     * Preempts $redirectTo member variable (from RedirectsUsers trait)
     */
    public function redirectTo()
    {
        if($this->guard()->user()->hasRole("organiser")) {
            return route('voyager.organizer.dashboard');
        } elseif($this->guard()->user()->hasRole("organiser")) {
            return route('voyager.agents.dashboard');
        } else {
            return config('voyager.user.redirect', route('voyager.dashboard'));
        }

    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard(app('VoyagerGuard'));
    }
}
