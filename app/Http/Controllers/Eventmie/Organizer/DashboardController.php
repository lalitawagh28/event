<?php

namespace App\Http\Controllers\Eventmie\Organizer;

use App\Http\Controllers\Controller;
use App\Service\Organizer\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware(['only_organizer']);

        $this->dashboard_service = new Dashboard;
    }

    public function index(Request $request)
    {
        $user_id = Auth::id();
        return $this->dashboard_service->index($request,$user_id);  
        
    }
}
