<?php

namespace App\Http\Controllers\Eventmie\Organizer;

use App\Exports\Export;
use App\Models\AgentTicket;
use App\Models\Booking;
use App\Models\Organizer;
use Facades\Classiebit\Eventmie\Eventmie;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Classiebit\Eventmie\Models\Commission;
use Classiebit\Eventmie\Models\Event;
use Classiebit\Eventmie\Models\Ticket;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    use BreadRelationshipParser;

    public function __construct()
    {
        // disable modification functions that are not managed from admin panel
        $route_name     = "voyager.organizer.reports";
        $enable_routes = [
            "$route_name.index", 
            "$route_name.show", 
            "$route_name.edit", 
            "$route_name.update", 
            "$route_name.destroy",
        ];
        // if(! in_array(\Route::current()?->getName(), $enable_routes))
        // {
        //     return redirect()->route('voyager.bookings.index')->send();
        // }
        // ---------------------------------------------------------------------

        $this->commission   = new Commission;  
    }

    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************
    public function index(Request $request)
    {
        
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'organizer_reports')->first();

        // Check permission
        //$this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }
            $event_ids = Event::where('user_id',Auth::id())->pluck('id')->toArray();
            $ticket_ids = Ticket::whereIn('event_id',$event_ids)->pluck('id')->toArray();
            
            $query->leftJoin('tickets','tickets.id','bookings.ticket_id');
            $query->leftJoin('events','events.id','bookings.event_id');
            $query->where('events.user_id', Auth::id());
            $query->leftJoin('users','users.id','bookings.customer_id');
            $query->select("bookings.*");
            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest("bookings.".$model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy("bookings.".$model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        
        $view = 'organizer.reports';

        return Eventmie::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn',
        ));
    }

    public function agent(Request $request)
    {
        
        
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'organizer_agent_reports')->first();
        
        // Check permission
        //$this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }
            $event_ids = Event::where('user_id',Auth::id())->pluck('id')->toArray();
            $ticket_ids = Ticket::whereIn('event_id',$event_ids)->pluck('id')->toArray();
            $query->whereIn('bookings.ticket_id', $ticket_ids);
            $query->where('bookings.agent_id','!=', NULL);
            $query->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id');
            $query->join('users', 'users.id', '=', 'agent_tickets.agent_id');
            $query->join('tickets','tickets.id','agent_tickets.ticket_id');
            $query->join('events','events.id','tickets.event_id');
            $query->select(['bookings.*','users.email as email','users.name as name']);
            // If a column has a relationship associated with it, we do not want to show that field
            //$this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest("bookings.".$model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy("bookings.".$model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        
        $view = 'organizer.agent_reports';

        return Eventmie::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn',
        ));
    }

     
    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = $model->findOrFail($id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);



        // extra data update ========================================================================
        
        $params = [
            'booking_id'       => $id,
            'organiser_id'     => $data->organiser_id,
            'status'           => $request->status == "on" ? 1 : 0,
        ];
        
        // edit commision table status when change booking table status change by organiser 
        $edit_commission  = $this->commission->edit_commission($params);    
        
        if(empty($edit_commission))
            return error('Commission not found!', Response::HTTP_BAD_REQUEST );
        // extra data update ========================================================================

        event(new BreadDataUpdated($dataType, $data));

        if (auth()->user()->can('browse', app($dataType->model_name))) {
            $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);
    }

    public function getSlug(Request $request)
    {
        if (isset($this->slug)) {
            $slug = $this->slug;
        } else {
            $slug = explode('.', $request->route()->getName())[2];
        }

        return $slug;
    }

    public function salesReportByEvent()
    {
        if(Auth::user()->hasRole('admin'))
        {
            $events = Event::get();
            return view('admin.reports.sales-report-by-event',compact('events'));
        }else{
            $events = Event::where('user_id',Auth::id())->get();
            return view('organizer.reports.sales-report-by-event',compact('events'));
        }
    }

    public function getSalesReportByEventData(Request $request)
    {
        $data = $request->validate([
            'events.*' => 'required|string',
            'from_date' => 'nullable|date',
            'to_date' => 'nullable|date'
        ]); 
        
        if(Auth::user()->hasRole('admin'))
        {
            $events = Event::get();
        }else{
            $events = Event::where('user_id',Auth::id())->get();
        }
        if(in_array('all',$data['events'])){
            if(Auth::user()->hasRole('admin')) {
                $eventIds = Event::get()->pluck('id')->toArray();
            } else {
                $eventIds = Event::where('user_id',Auth::id())->pluck('id')->toArray();
            }
            
        } else {
            $eventIds  = $data['events'];
        }
        $selectedEvent =  Ticket::join('events', 'tickets.event_id', '=', 'events.id')
            ->select(['tickets.*','events.title as event_title'])->whereIn('events.id', $eventIds)->get();   

        $eventsId = $eventIds;
        if(!is_null($request->input('from_date')) && !is_null($request->input('to_date')))
        {
            $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
            ->join('users', 'bookings.customer_id', '=', 'users.id')
            ->select(['bookings.*','events.title as event_title','users.name as customer_name'])->whereIn('event_id',$eventIds)->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($request->input('from_date'))), date('Y-m-d', strtotime('+1 day', strtotime($request->input('to_date'))))])->get();
        }else{
            $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
            ->join('users', 'bookings.customer_id', '=', 'users.id')
            ->select(['bookings.*','events.title as event_title','users.name as customer_name'])->whereIn('event_id',$eventIds)
            ->get();
        }
        $eventIds = $data['events'];
       // $bookings  = Booking::wherewhereEventId($request->input('event'))->whereBetween('created_at', [date('Y-m-d', strtotime($request->input('from_date'))), date('Y-m-d', strtotime('+1 day', strtotime($request->input('to_date'))))])->with('ticket')->with('agent_ticket')->get();
       
        if(Auth::user()->hasRole('admin'))
        {
            return view('admin.reports.sales-report-by-event', compact('events', 'bookings', 'selectedEvent','eventIds','eventsId'));
        }else{
            return view('organizer.reports.sales-report-by-event', compact('events', 'bookings', 'selectedEvent','eventIds','eventsId'));
        }
    }
    

    public function salesReportByVoucher()
    {
        //  dd('xxxxx');
        if(Auth::user()->hasRole('admin'))
        {
            $events = Event::with('tickets')->get();
            return view('admin.reports.sales-report-by-voucher',compact('events'));
        }else{
            $events = Event::where('user_id',Auth::id())->with('tickets')->get();
            return view('organizer.reports.sales-report-by-voucher',compact('events'));
        }
    }

    public function getVoucherCodeByEvent(Request $request)
    {
        
        $event = Event::where('id',$request->eventId)->with('tickets')->first();
      
        $ticketIds = [];
        foreach($event->tickets as $ticket)
        {
            array_push($ticketIds,$ticket->id);
        }
        


       $vouchers = AgentTicket::whereIn('ticket_id',$ticketIds)->groupBy('coupon_code')->get();
       
   
        return response()->json(['vouchers' => $vouchers]);
        
    }

    public function getSalesReportByVoucherData(Request $request)
    {
        $data = $request->validate([
            'event' => 'required|string',
            'voucher.*' => 'required',
            'from_date' => 'nullable|date',
            'to_date' => 'nullable|date'
        ]); 

        if(Auth::user()->hasRole('admin'))
        {
            $events = Event::with('tickets')->get();
            $ticket_ids = AgentTicket::pluck('ticket_id')->toArray();
        }else{
            $events = Event::where('user_id',Auth::id())->with('tickets')->get();
            $ticket_ids = AgentTicket::where('agent_id',Auth::id())->pluck('ticket_id')->toArray();
        }
       
        $selectedEvent =  Ticket::join('events', 'tickets.event_id', '=', 'events.id')
            ->select(['tickets.*','events.title as event_title'])->where('events.id', $request->input('event'))->get();

        

            if(in_array('Select All',$request->input('voucher')))
            {
                $voucherData = AgentTicket::whereIn('ticket_id',$selectedEvent->pluck('id')->toArray())->pluck('coupon_code')->toArray();
                $voucherData = array_unique( $voucherData);
                $couponCode = AgentTicket::whereIn('coupon_code',$voucherData)->pluck('id')->toArray();
            }else{
                $voucherData = $request->input('voucher');
                $couponCode =  AgentTicket::whereIn('coupon_code',$request->input('voucher'))->pluck('id')->toArray();
            }
         
        //$couponCode = AgentTicket::whereIn('coupon_code',$voucherData)->pluck('id');

        $from_date = $request->input('from_date'); 
        $to_date = $request->input('to_date');
        $event_id = $request->input('event');
      
       
        if(!is_null($request->input('from_date')) && !is_null($request->input('to_date')))
        {
            $bookings  = Booking::whereEventId($request->input('event'))->whereIn('bookings.agent_id',$couponCode)->whereBetween('created_at', [date('Y-m-d', strtotime($request->input('from_date'))), date('Y-m-d', strtotime('+1 day', strtotime($request->input('to_date'))))])->with('ticket')
            ->get();
        }else{
            $bookings  = Booking::whereEventId($request->input('event'))->whereIn('bookings.agent_id',$couponCode)
            ->with('ticket')
            ->get();
           
        }

        $event = Event::where('id',$request->input('event'))->with('tickets')->first();
     
        $ticketIds = [];
        foreach($event->tickets as $ticket)
        {
            array_push($ticketIds,$ticket->id);
        }
        //$couponCode = AgentTicket::whereIn('coupon_code',$voucherData)->get();
       
       $vouchersArray = AgentTicket::whereIn('ticket_id',$ticketIds)->groupBy('coupon_code')->get();
     
       $vouchers = [];
       foreach($vouchersArray as $voucher)
       {
            $booking = Booking::whereEventId($request->input('event'))->wherePromocode($voucher->coupon_code)->first();
            
           
            if(!is_null($booking))
            {
                array_push( $vouchers,$voucher);
            }
       }

       if(Auth::user()->hasRole('admin'))
       {
            return view('admin.reports.sales-report-by-voucher', compact('events', 'bookings', 'selectedEvent','couponCode','vouchers','from_date','to_date','event_id','voucherData'));
       }else{
            return view('organizer.reports.sales-report-by-voucher', compact('events', 'bookings', 'selectedEvent','couponCode','vouchers','from_date','to_date','event_id','voucherData'));
       }
    }

    public function salesReportByAgent()
    {
        
        if(Auth::user()->hasRole('admin'))
        {
            $events = Event::get();
        
            return view('admin.reports.sales-report-by-agent',compact('events'));
        }else{
            $events = Event::where('user_id',Auth::id())->get();
        
            return view('organizer.reports.sales-report-by-agent',compact('events'));
        }
    }

    public function getAgentByEvent(Request $request)
    {
        $event = Event::where('id',$request->eventId)->with('tickets')->first();

        $ticketIds = [];
        foreach($event->tickets as $ticket)
        {
            array_push($ticketIds,$ticket->id);
        }

       $agents = AgentTicket::whereIn('ticket_id',$ticketIds)->with('agent')->get()->unique('agent_id');
       return response()->json(['agents' => $agents]);
    }

    public function getSalesReportByAgentData(Request $request)
    {
        $data = $request->validate([
            'event' => 'required|string',
            'agents.*' => 'required|string',
            'from_date' => 'nullable|date',
            'to_date' => 'nullable|date'
        ]); 

        if(Auth::user()->hasRole('admin'))
        {
            $events = Event::get();
        }else{
            $events = Event::where('user_id',Auth::id())->get();
        }
        
        $selectedEvent =  Ticket::join('events', 'tickets.event_id', '=', 'events.id')->with('agent_tickets')
            ->select(['tickets.*','events.title as event_title'])->where('events.id', $request->input('event'))->get();
        $agids = [];
        if(in_array('all',$data['agents'])){
            $agentIds = [];
            foreach($selectedEvent as $ticket )  {
                $id = $ticket->agent_tickets->pluck('id')->toArray();
                if($id){
                    $agids = array_merge( $ticket->agent_tickets->pluck('agent_id')->toArray(),$agids);
                    $agentIds = array_merge($ticket->agent_tickets->pluck('id')->toArray(),$agentIds);
                }
                
            }
        } else {
        $agentIds = [];
            $agids = $data['agents'];
           foreach($selectedEvent as $ticket )  {
                $id = $ticket->agent_tickets->whereIn('agent_id',$data['agents'])->pluck('id')->toArray();
                if($id){
                    $agentIds = array_merge($ticket->agent_tickets->whereIn('agent_id',$data['agents'])->pluck('id')->toArray(),$agentIds);
                }
                
            }
        }
        $agids = array_unique($agids);
        $agentsId = $agentIds;
        if(!is_null($request->input('from_date')) && !is_null($request->input('to_date')))
        {
            $bookings  = Booking::whereEventId($request->input('event'))->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($request->input('from_date'))), date('Y-m-d', strtotime('+1 day', strtotime($request->input('to_date'))))])
            ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')
            ->whereIn('agent_tickets.id',$agentIds)
            ->select('bookings.*','agent_tickets.id','agent_tickets.agent_id')
            ->get();
        }else{
            $bookings  = Booking::whereEventId($request->input('event'))
            ->join('agent_tickets','agent_tickets.id','=','bookings.agent_id')
            ->whereIn('agent_tickets.id',$agentIds)
            ->select('bookings.*','agent_tickets.id','agent_tickets.agent_id')
            ->get();
        }
       
        $agentIds = $data['agents'];

        $agentevent = Event::where('id',$request->input('event'))->with('tickets')->first();

        $ticketIds = [];
        foreach($agentevent->tickets as $ticket)
        {
            array_push($ticketIds,$ticket->id);
        }

        $agents = AgentTicket::whereIn('ticket_id',$ticketIds)->with('agent')->get()->unique('agent_id');

        if(Auth::user()->hasRole('admin'))
        {
            return view('admin.reports.sales-report-by-agent', compact('events', 'bookings', 'selectedEvent','agents','agentIds','agentsId','agids'));
        }else{
            return view('organizer.reports.sales-report-by-agent', compact('events', 'bookings', 'selectedEvent','agents','agentIds','agentsId','agids'));
        }
    }

    public function salesReportCommission()
    {
        if(Auth::user()->hasRole('admin'))
        {
            $events = Event::get();
            return view('admin.reports.sales-report-commission',compact('events'));
        }else{
            $events = Event::where('user_id',Auth::id())->get();
            return view('organizer.reports.sales-report-commission',compact('events'));
        }
    }

    public function getSalesReportCommissionData(Request $request)
    {
        $data = $request->validate([
            'event' => 'required|string',
            'from_date' => 'nullable|date',
            'to_date' => 'nullable|date'
        ]); 

        if(Auth::user()->hasRole('admin'))
        {
            $events = Event::get();
        }else{
            $events = Event::where('user_id',Auth::id())->get();
        }

        
        
        $selectedEvent =  Ticket::join('events', 'tickets.event_id', '=', 'events.id')
            ->select(['tickets.*','events.title as event_title'])->where('events.id', $request->input('event'))->with('agent_tickets')->get();
        
        $from_date = $request->input('from_date'); 
        $to_date = $request->input('to_date');
        $event_id = $request->input('event');
        $agent_id = $request->input('agent');

        $agentevent = Event::where('id',$event_id)->with('tickets')->first();

        $ticketIds = [];
        $agids = [];
        foreach($agentevent->tickets as $ticket)
        {
            array_push($ticketIds,$ticket->id);
        }
        if(in_array('all',$request->input('agents'))){
            $agentIds = [];
            foreach($selectedEvent as $ticket )  {
                $ids =$ticket->agent_tickets->pluck('id')->toArray();
                if($ids){
                    $agids = array_merge( $ticket->agent_tickets->pluck('agent_id')->toArray(),$agids);
                    $agentIds = array_merge($ids,$agentIds);
                }
                
            }
        } else {
            $agentIds = [];
            $agids = $request->input('agents');
            foreach($selectedEvent as $ticket )  {
                $ids =AgentTicket::whereIn('agent_id',$request->input('agents'))->where('ticket_id',$ticket->id)->pluck('id')->toArray();
                if($ids){
                    $agentIds = array_merge($ids,$agentIds);
                }
                
            }

        }
        $agids = array_unique($agids);
        $agentsId  = $agentIds;
        $agents = AgentTicket::whereIn('ticket_id',$ticketIds)->with('agent')->get()->unique('agent_id');
        $agentid=  $request->input('agents');
        if(Auth::user()->hasRole('admin'))
        {
            return view('admin.reports.sales-report-commission', compact('events',  'selectedEvent','from_date','to_date','event_id','agent_id','agents','agentid','agentsId','agids'));
        }else
        {
            return view('organizer.reports.sales-report-commission', compact('events',  'selectedEvent','from_date','to_date','event_id','agent_id','agents','agentid','agentsId','agids'));
        }
    }

    public function exportEventSale(Request $request)
    {
       

        if(Auth::user()->hasRole('admin'))
        {
            $eventIds = Event::where('id',explode(',',$request->input('event_ids')))->pluck('id')->toArray();
        } else {
            $eventIds = Event::where('user_id',Auth::id())->where('id',explode(',',$request->input('event_ids')))->pluck('id')->toArray();
        }
       
        if (!is_null($request->from_date) && !is_null($request->to_date)){
            
                $from_date = date('Y-m-d', strtotime($request->from_date));
                $to_date = date('Y-m-d', strtotime('+1 day', strtotime($request->to_date)));
                    
                    $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
                                ->join('users', 'bookings.customer_id', '=', 'users.id')
                                ->leftJoin('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                                ->select('bookings.*','events.title as event_title','users.name as customer_name',
                                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),                
                                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                                )->whereIn('event_id',$eventIds)
                                ->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($from_date)), date('Y-m-d', strtotime('+1 day', strtotime($to_date)))])
                                ->groupBy('ticket_id','sub_category_id')
                                ->orderBy('event_id','desc')
                                ->orderBy('ticket_id','desc')
                                ->orderBy('sub_category_id','asc')
                                ->get();
                
                
           
            } else {
           
                $bookings  = Booking::join('events', 'bookings.event_id', '=', 'events.id')
                ->join('users', 'bookings.customer_id', '=', 'users.id')
                ->leftJoin('agent_tickets','agent_tickets.id','=','bookings.agent_id')
                ->select('bookings.*','events.title as event_title','users.name as customer_name',
                    DB::raw(" COALESCE(SUM(bookings.price),0)  as tot_net_price"),
                    DB::raw("COALESCE(SUM(bookings.quantity) ,0) as tot_quantity"),
                    DB::raw(" COALESCE(SUM(agent_tickets.commission),0)  as tot_commission"),
                    DB::raw(" COALESCE(SUM(agent_tickets.discount),0)  as tot_discount")
                )->whereIn('event_id',$eventIds)
                ->groupBy('ticket_id','sub_category_id')
                ->orderBy('event_id','desc')
                ->orderBy('ticket_id','desc')
                ->orderBy('sub_category_id','asc')
                ->get();
              
            
            }
                
            $list = collect();
            foreach ($bookings as  $booking){
                
                   
                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                    
                
                if($booking->ticket){
                    $columnDetail = [
                        $booking->event->title,
                        $booking->ticket?->title,
                        $booking->subcategory?->title ?? '',
                        $price,
                        $booking->tot_quantity
                       
                    ];
                    $list->push($columnDetail);
                }
                    

                
                
            }
                                               

        

        $columnsHeading = [
            'Event Name',
            'Ticket Category',
            'Ticket SubCategory',
            'Sales',
            'No Of Ticket',
        ];

        return Excel::download(new Export($list,$columnsHeading), 'EventSaleReport.xlsx');
    }

    public function exportAgentSale(Request $request)
    {
       
        $aids = explode(",",$request->agent_ids);
        if (!is_null($request->from_date) && !is_null($request->to_date)){
            
                $from_date = date('Y-m-d', strtotime($request->from_date));
                $to_date = date('Y-m-d', strtotime('+1 day', strtotime($request->to_date)));
                $bookings = Booking::whereEventId(request()->event_id)
                ->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($from_date), date('Y-m-d', strtotime('+1 day', strtotime($to_date))))])
                ->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id')
                ->whereIn('agent_tickets.agent_id', @$aids ?? [])
                ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', DB::raw('COALESCE(SUM(bookings.price),0) as tot_net_price'), DB::raw('COALESCE(SUM(bookings.quantity),0) as tot_quantity'), DB::raw('COALESCE(SUM(agent_tickets.commission),0) as tot_commission'), DB::raw('COALESCE(SUM(agent_tickets.discount),0) as tot_discount'))
                ->groupBy('ticket_id','sub_category_id','agent_tickets.agent_id')
                ->orderBy('event_id','desc')
                ->orderBy('ticket_id','desc')
                ->orderBy('sub_category_id','asc')
                ->get();
                    
                
                
           
            } else {
           
                $bookings = Booking::whereEventId(request()->event_id)
                ->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id')
                ->whereIn('agent_tickets.agent_id', @$aids ?? [])
                ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', DB::raw('COALESCE(SUM(bookings.price),0) as tot_net_price'), DB::raw('COALESCE(SUM(bookings.quantity),0) as tot_quantity'), DB::raw('COALESCE(SUM(agent_tickets.commission),0) as tot_commission'), DB::raw('COALESCE(SUM(agent_tickets.discount),0) as tot_discount'))
                ->groupBy('ticket_id','sub_category_id','agent_tickets.agent_id')
                ->orderBy('event_id','desc')
                ->orderBy('ticket_id','desc')
                ->orderBy('sub_category_id','asc')
                ->get();
              
            
            }
                
            $list = collect();
            foreach ($bookings as  $booking){
                
                   
                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                    
                
                if($booking->tot_quantity){
                    $columnDetail = [
                        $booking->event->title,
                        $booking->ticket?->title,
                        $booking->subcategory?->title ?? '',
                        $booking->agent_ticket?->agent?->name,
                        $price,
                        $booking->tot_quantity
                       
                    ];
                    $list->push($columnDetail);
                }
                    

                
                
            }
                                               


        $columnsHeading = [
            'Event Name',
            'Ticket Category',
            'Ticket SubCategory',
            'Agent Name',
            'Sales',
            'No Of Ticket',
        ];

        return Excel::download(new Export($list,$columnsHeading), 'AgentSaleReport.xlsx');
    }

    public function exportCommisionSale(Request $request)
    {
       
        $aids = explode(",",$request->agent_ids);
        if (!is_null($request->from_date) && !is_null($request->to_date)){
            
                $from_date = date('Y-m-d', strtotime($request->from_date));
                $to_date = date('Y-m-d', strtotime('+1 day', strtotime($request->to_date)));
                $bookings = Booking::whereEventId(request()->event_id)
                ->whereBetween('bookings.created_at', [date('Y-m-d', strtotime($from_date), date('Y-m-d', strtotime('+1 day', strtotime($to_date))))])
                ->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id')
                ->whereIn('agent_tickets.agent_id', @$aids ?? [])
                ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', DB::raw('COALESCE(SUM(bookings.price),0) as tot_net_price'), DB::raw('COALESCE(SUM(bookings.quantity),0) as tot_quantity'), DB::raw('COALESCE(SUM(agent_tickets.commission),0) as tot_commission'), DB::raw('COALESCE(SUM(agent_tickets.discount),0) as tot_discount'))
                ->groupBy('ticket_id','sub_category_id','agent_tickets.agent_id','bookings.promocode')
                ->orderBy('event_id','desc')
                ->orderBy('ticket_id','desc')
                ->orderBy('sub_category_id','asc')
                ->get();
                    
                
                
           
            } else {
           
                $bookings = Booking::whereEventId(request()->event_id)
                ->join('agent_tickets', 'agent_tickets.id', '=', 'bookings.agent_id')
                ->whereIn('agent_tickets.agent_id', @$aids ?? [])
                ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', DB::raw('COALESCE(SUM(bookings.price),0) as tot_net_price'), DB::raw('COALESCE(SUM(bookings.quantity),0) as tot_quantity'), DB::raw('COALESCE(SUM(agent_tickets.commission),0) as tot_commission'), DB::raw('COALESCE(SUM(agent_tickets.discount),0) as tot_discount'))
                ->groupBy('ticket_id','sub_category_id','agent_tickets.agent_id','bookings.promocode')
                ->orderBy('event_id','desc')
                ->orderBy('ticket_id','desc')
                ->orderBy('sub_category_id','asc')
                ->get();
              
            
            }
                
            $list = collect();
            foreach ($bookings as  $booking){
                
                   
                $price = $booking->tot_net_price - $booking?->tot_commission - $booking?->tot_discount;
                    
                
                if($booking->tot_quantity){
                    $columnDetail = [
                        $booking->event->title,
                        $booking->ticket?->title,
                        $booking->subcategory?->title ?? '',
                        $booking->agent_ticket?->agent?->name,
                        $booking->promocode,
                        $price,
                        $booking->tot_quantity
                       
                    ];
                    $list->push($columnDetail);
                }
                    

                
                
            }
                                               


        $columnsHeading = [
            'Event Name',
            'Ticket Category',
            'Ticket SubCategory',
            'Agent Name',
            'Voucher Code',
            'Sales',
            'No Of Ticket',
        ];

        return Excel::download(new Export($list,$columnsHeading), 'EventCommissionReport.xlsx');
    }
    public function exportVoucherSale(Request $request)
    {
       

        if(request()->input('voucherData') == 'Select All'){
          
                $voucherData = explode(',',request()->input('selectAllExcel'));
          
        }else{
          
                $voucherData = explode(',',request()->input('voucherData'));
           
        }
      
        if (!is_null($request->from_date) && !is_null($request->to_date)){
            
                $from_date = date('Y-m-d', strtotime($request->from_date));
                $to_date = date('Y-m-d', strtotime('+1 day', strtotime($request->to_date)));

                

                    $booking = \App\Models\Booking::whereEventId($request->event_id)
                    ->whereBetween('bookings.created_at', [$from_date, $to_date])
                    ->whereIn('bookings.promocode', $voucherData)
                    ->get();
                
                $finalArray = array();
                foreach($booking as $bookingValue){
                    $bookingDetails = \App\Models\Booking::whereEventId($request->event_id)->leftJoin('agent_tickets', 'bookings.agent_id', '=', 'agent_tickets.id')->where('bookings.ticket_id',$bookingValue->ticket_id)
                    ->whereIn('bookings.promocode', $voucherData)
                    ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', 'agent_tickets.commission', 'agent_tickets.discount', DB::raw('SUM(bookings.price) as tot_net_price'), DB::raw('SUM(bookings.quantity) as tot_quantity'), DB::raw('SUM(agent_tickets.commission) as tot_commission'), DB::raw('SUM(agent_tickets.discount) as tot_discount'))
                    ->groupBy('bookings.promocode')->get();
                
                    foreach($bookingDetails as $bookingDetailsVlaue)
                    {
                        if(!in_array($bookingDetailsVlaue,$finalArray))
                        {
                            array_push($finalArray,$bookingDetailsVlaue);
                        }
                        
                        
                    }
                    
                }
                
           
            }else{
           
                
                $booking = \App\Models\Booking::whereEventId($request->event_id)
                    ->whereIn('bookings.promocode', $voucherData)
                    ->get();
                
                $finalArray = array();
                foreach($booking as $bookingValue){
                    $bookingDetails = \App\Models\Booking::whereEventId($request->event_id)->leftJoin('agent_tickets', 'bookings.agent_id', '=', 'agent_tickets.id')->where('bookings.ticket_id',$bookingValue->ticket_id)
                    ->whereIn('bookings.promocode', $voucherData)
                    ->select('bookings.*', 'agent_tickets.id', 'agent_tickets.agent_id', 'agent_tickets.commission', 'agent_tickets.discount', DB::raw('SUM(bookings.price) as tot_net_price'), DB::raw('SUM(bookings.quantity) as tot_quantity'), DB::raw('SUM(agent_tickets.commission) as tot_commission'), DB::raw('SUM(agent_tickets.discount) as tot_discount'))
                    ->groupBy('bookings.promocode')->get();
                
                    foreach($bookingDetails as $bookingDetailsVlaue)
                    {
                        if(!in_array($bookingDetailsVlaue,$finalArray))
                        {
                            array_push($finalArray,$bookingDetailsVlaue);
                        }
                        
                        
                    }
                    
                }
            
            
            }
                
            $list = collect();
            foreach ($finalArray as  $bookingVal){
                
                    $ticket = \Classiebit\Eventmie\Models\Ticket::whereId($bookingVal->ticket_id)->first();
                    $agentTicket = \App\Models\AgentTicket::whereCouponCode($bookingVal->promocode)->first();
                    $price = $bookingVal->tot_net_price - $bookingVal?->tot_commission - $bookingVal?->tot_discount;
                    $ticketSubcategory = \App\Models\TicketSubCategory::whereId($bookingVal->sub_category_id)->first();
                    
                
                if(!is_null($agentTicket)){
                    $subttle = '';
                    if(!is_null($ticketSubcategory?->title)){ 
                     $subttle=    $ticketSubcategory?->title;
                    } else 
                    { 
                        $subttle=    '-';
                    }
                    $columnDetail = [
                        $ticket?->title,
                        $subttle,
                        $bookingVal->promocode,
                        $price,
                        $bookingVal->tot_quantity
                       
                    ];
                    $list->push($columnDetail);

                
                }
            }
                                               


        

        $columnsHeading = [
            'Ticket Category',
             'Ticket SubCategory',
             'Voucher Code',
             'Sales',
            'No Of Ticket',
           
        ];

        return Excel::download(new Export($list,$columnsHeading), 'VoucherCodeSaleReport.xlsx');
    }
}
