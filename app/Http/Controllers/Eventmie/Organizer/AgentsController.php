<?php

namespace App\Http\Controllers\Eventmie\Organizer;

use App\Http\Coupon;
use App\Http\Helper;
use App\Http\Requests\AgentStoreRegisterRequest;
use App\Http\Requests\ChangeEmailAddressRequest;
use App\Http\Requests\ChangePhoneNumberRequest;
use App\Models\Agent;
use App\Models\AgentSubCategory;
use App\Models\AgentTicket;
use App\Models\Event;
use App\Models\RegisterCountry;
use App\Models\TicketSubCategory;
use App\Models\Title;
use App\Notifications\AgentTicketAllocation;
use App\Notifications\AgentTicketCommission;
use App\Notifications\CustomRegisterationNotification;
use App\Notifications\CustomSendEmailVerificationNotification;
use App\Notifications\EmailOneTimePasswordNotification;
use App\Notifications\SmsOneTimePasswordNotification;
use Carbon\Carbon;
use Classiebit\Eventmie\Models\Ticket;
use Classiebit\Eventmie\Models\User;
use Facades\Classiebit\Eventmie\Eventmie;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Models\Role;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Setting;


class AgentsController extends Controller
{
    use BreadRelationshipParser;

    public function __construct()
    {
        // disable modification functions that are not managed from admin panel
        $route_name     = "voyager.organizer.agents";
        $enable_routes = [
            "$route_name.index",
            "$route_name.show",
            "$route_name.edit",
            "$route_name.update",
            "$route_name.destroy",
        ];
        // if(! in_array(\Route::current()?->getName(), $enable_routes))
        // {
        //     return redirect()->route('voyager.bookings.index')->send();
        // }
        // ---------------------------------------------------------------------

    }

    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************
    public function index(Request $request)
    {
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'agent_tickets')->first();

        // Check permission
        //$this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }
            $event_ids = Event::where('user_id', Auth::id())->pluck('id')->toArray();
            $ticket_ids = Ticket::whereIn('event_id', $event_ids)->pluck('id')->toArray();
            $query->whereIn('ticket_id', $ticket_ids);
            $query->join('users', 'users.id', '=', 'agent_tickets.agent_id');
            $query->join('tickets', 'tickets.id', '=', 'agent_tickets.ticket_id');
            $query->join('events', 'events.id', '=', 'tickets.event_id');
            $query->select(['agent_tickets.*', 'users.email as email', 'users.name as name']);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }
            //$query->whereHas('event', function($query) { $query->where('organiser_id',Auth::id());});
            // If a column has a relationship associated with it, we do not want to show that field
            //$this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%' . $search->value . '%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'organizer.agents_allocated';

        return Eventmie::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function available(Request $request)
    {
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'agents')->first();

        // Check permission
        //$this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }
            //$query->whereHas('event', function($query) { $query->where('organiser_id',Auth::id());});
            // If a column has a relationship associated with it, we do not want to show that field
            //$this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%' . $search->value . '%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;
        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }


        $view = 'organizer.agents_available';

        return Eventmie::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function agent_register()
    {
        $titles = Title::orderBy('id', 'asc')->pluck("name", "id");
        $countries = RegisterCountry::where('enable', 1)->orderBy("name")->pluck("name", "id");
        $countryWithFlags = RegisterCountry::where('enable', 1)->orderBy("name")->get();
        $defaultCountry = RegisterCountry::find(config('otp.default_country_code'));
        return view("registration.agent-create-personal-detail", compact("titles", "countries", "countryWithFlags", "defaultCountry"));
    }
    public function agent_store(AgentStoreRegisterRequest $request)
    {
        $data = $request->validated();
        $data['password'] = Hash::make($data['password']);

        $data['phone'] = Helper::normalizePhone($data['phone']);
        $data['name'] = $data['first_name'] . ' ' . $data['middle_name'] . ' ' . $data['last_name'];
        $data['role_id'] = Role::where('name', 'agent')->first()->id;;

        DB::beginTransaction();
        try {
            /** @var \App\Models\User $user */
            $validateUser = Helper::validateUser(['email' => $request->get('email'), 'phone' => $request->get('phone')]);
            if (is_null($validateUser)) {
                $user = User::updateOrCreate($data);
            } else {
                return redirect()->back()->withErrors($validateUser['errors']);
            }

            $user->logRegistrationStep("personal_details");
            if (config('otp.mobile_otp_enable')) {
                $user->notify(new SmsOneTimePasswordNotification($user->generateOtp("sms")));
            }
            if (config('otp.email_otp_enable')) {
                $user->notify(new EmailOneTimePasswordNotification($user->generateOtp("email")));
            }
            $payload = [
                'notification_type' => 'user_registration',
                'payload' => [
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'password' =>  $data['password']
                ]
            ];

            Helper::notifyThroughWebhook($payload);

            $clienturl = (config('app.env') == 'production') ? 'https://dhigna.com/callback' : 'https://uat.dhigna.com/callback';

            $clientRepository =  app('Laravel\Passport\ClientRepository');
            $clientRepository->create($user->id, $request->get('first_name') . ' ' . $request->get('last_name'), $clienturl);

            DB::commit();
            if (config('otp.mobile_otp_enable')) {
                return redirect()->route('agent_create.register.mobileVerification', $user->id);
            }
            if (config('otp.email_otp_enable')) {
                return redirect()->route('agent_create.register.emailVerification', $user->id);
            }
            if (config('otp.registation_success_email')) {
                $user->notify(new CustomRegisterationNotification($user));
            }
            return redirect()->route("voyager.organizer.agents_available");
        } catch (\Exception $exception) {
            DB::rollback();
            \Log::error($exception);
            return redirect()->route("registration.agent-create-personal-detail");
        }
    }

    public function mobileVerificationCreate($id)
    {
        $user = User::find($id);
        return view("registration.agent-create-mobile-verification", compact('user'));
    }

    public function mobileVerificationStore(Request $request, $id)
    {
        $request->validate([
            'code' => ['required'],
        ], [
            'code.required' => __('admin_register.enter_otp'),
        ]);
        /** @var \App\Models\User $user */
        $user = User::find($id);
        $oneTimePassword = $user->oneTimePasswords()->whereType("sms")->first();

        if (is_null($oneTimePassword)) {
            throw ValidationException::withMessages(['code' => __('admin_register.send_otp')]);
        }

        $manualOtp = Setting::whereKey('apps.default_otp')->first()?->value;

        if (isset($manualOtp) && ($manualOtp == $request->input('code'))) {
            $user->update(['phone_verified_at' => now()]);
            $oneTimePassword->update(['verified_at' => now()]);

            $user->logRegistrationStep("mobile_otp_verification");

            if (config('otp.email_otp_enable')) {
                return redirect()->route('agent_create.register.emailVerification', $user->id);
            }
            if (config('otp.registation_success_email')) {
                $user->notify(new CustomRegisterationNotification($user));
            }
            return redirect()->route("voyager.organizer.agents_available");
        }

        if ($oneTimePassword->code != $request->input('code')) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_didnot_match')]);
        } else if (now()->greaterThan($oneTimePassword->expires_at)) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_expired')]);
        }

        $user->update(['phone_verified_at' => now()]);
        $oneTimePassword->update(['verified_at' => now()]);

        $user->logRegistrationStep("mobile_otp_verification");

        if (config('otp.email_otp_enable')) {
            return redirect()->route('agent_create.register.emailVerification', $user->id);
        }
        if (config('otp.registation_success_email')) {
            $user->notify(new CustomRegisterationNotification($user));
        }
        return redirect()->route("voyager.organizer.agents_available");
    }

    public function emailVerificationCreate($id)
    {
        $user = User::find($id);
        return view("registration.agent-create-email-verification", compact('user'));
    }

    public function emailVerificationStore(Request $request, $id)
    {
        $request->validate([
            'code' => ['required', 'size:6'],
        ], [
            'code.required' => __('admin_register.enter_otp'),
        ]);


        /** @var \App\Models\User $user */
        $user = User::find($id);

        $oneTimePassword = $user->oneTimePasswords()->whereType("email")->first();

        if (is_null($oneTimePassword)) {
            throw ValidationException::withMessages(['code' => __('admin_register.send_otp')]);
        }

        $manualOtp = Setting::whereKey('apps.default_otp')->first()?->value;

        if (isset($manualOtp) && ($manualOtp == $request->input('code'))) {
            $user->update(['email_verified_at' => now()]);
            $oneTimePassword->update(['verified_at' => now()]);

            $user->logRegistrationStep("email_otp_verification");

            if (config('otp.registation_success_email')) {
                $user->notify(new CustomRegisterationNotification($user));
            }
            return redirect()->route("voyager.organizer.agents_available");
        }

        if ($oneTimePassword->code != $request->input('code')) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_didnot_match')]);
        } else if (now()->greaterThan($oneTimePassword->expires_at)) {
            throw ValidationException::withMessages(['code' => __('admin_register.otp_expired')]);
        }

        $user->update(['email_verified_at' => now()]);
        $oneTimePassword->update(['verified_at' => now()]);

        $user->logRegistrationStep('email_otp_verification');

        if (config('otp.registation_success_email')) {
            $user->notify(new CustomRegisterationNotification($user));
        }
        return redirect()->route("voyager.organizer.agents_available");
    }

    public function changePhoneNumber(ChangePhoneNumberRequest $request, $id)
    {
        $request->validated();
        $phone = Helper::normalizePhone($request->input("phone"));

        /** @var \App\Models\User $user */
        $user = User::find($id);
        $user->update(['phone' => $phone]);

        $user->notify(new SmsOneTimePasswordNotification($user->generateOtp("sms")));

        return redirect()->back();
    }

    public function changeEmailAddress(ChangeEmailAddressRequest $request, $id)
    {

        $request->validated();

        /** @var \App\Models\User $user */
        $user = User::find($id);
        $email = $user->email;
        $user->update(['email' => $request->input("email")]);

        $user->notify(new EmailOneTimePasswordNotification($user->generateOtp("email")));
        return redirect()->back();
    }

    public function resendMobileOnetimePassword($id)
    {
        /** @var \App\Models\User $user */
        $user = User::find($id);

        if ($user->hasActiveOneTimePassword("sms")) {
            $oneTimePassword = $user->oneTimePasswords()->whereType("sms")->first();
        } else {
            $oneTimePassword = $user->generateOtp("sms");
        }

        $user->notify(new SmsOneTimePasswordNotification($oneTimePassword));

        return response()->json(['message' => __('admin_register.resend_success'), 'status' => 'success']);
    }

    public function resendEmailOnetimePassword($id)
    {
        /** @var \App\Models\User $user */
        $user = User::find($id);

        if ($user->hasActiveOneTimePassword("email")) {
            $oneTimePassword = $user->oneTimePasswords()->whereType("email")->first();
        } else {
            $oneTimePassword = $user->generateOtp("email");
        }

        $user->notify(new EmailOneTimePasswordNotification($oneTimePassword));

        return response()->json(['message' => __('admin_register.resend_success'), 'status' => 'success']);
    }

    public function submitTickets($id)
    {
        $view = 'organizer.submit_tickets';
        $data = $this->prepopulateBreadInfo('agent_tickets');

        $data['agent'] = Agent::find($id);
        $data['events'] = Event::where('user_id', Auth::id())->whereDate('start_date','>',now())->get();
        return Eventmie::view($view, $data);
    }
    public function bulkAllocateTickets()
    {

        $view = 'organizer.allocate_tickets_bulk';
        $data = $this->prepopulateBreadInfo('agent_tickets');

        $data['agents'] = Agent::all();
        $data['events'] = Event::where('user_id', Auth::id())->whereDate('start_date','>',now())->get();
        return Eventmie::view($view, $data);
    }

    public function bulkTicketUpdate(Request $request){
        $data = $request->validate([
            'event_id.*' => 'required',
            'agents.*' => 'required',
            'ticket_id'=>'required',
            'ticket_id.*' => 'required',
            'code_value.*' => 'required',
            'type' => 'required',
            'type.*' => 'required',
            'agent_commission.*' => 'required',
            'quantity.*'   => 'required',
            'voucher_code.*' => 'required|min:5|max:25',
            'max_uses' =>  'required',
        ]);
        //print_r($request->all());exit();
        //$ticket_index=0;
        DB::beginTransaction();
        $agent_ids = [];
        $agent_tickets = [];
        foreach($request->event_id as $key =>$event_id){
          try{
                $agent_ids[] = $request->agents[$key];
                // $ticket_ids = array_slice($request->ticket_id,$ticket_index,$request->ticket_count[$key]);
                //$icket_index = $ticket_index + $request->ticket_count[$key];
                $ticket_ids = $request->ticket_id[$key];
                $isAll = false;
                foreach($ticket_ids as $ticket_id){
                    @list ($id,$sub_id) = explode(',', preg_replace("/\([^)]*\)+/", '',$ticket_id));
                    if($id == 'all'){
                        $isAll = true;
                    }
                }
                if($isAll){
                    if($id == 'all'){
                        
                        $event = Event::find($event_id);
                        $tickets = $event->tickets;
                        foreach($tickets as $ticket){
                            
                            if($ticket->sub_category){
                                $sub_categories = $ticket->sub_categories;
                                foreach($sub_categories as $sub_category){
                                    $agent_ticket = AgentTicket::where('agent_id',$request->agents[$key])->where('ticket_id',$ticket->id)->where('sub_category_id',$sub_category->id);
                                    $exists =  $agent_ticket?->first();
                                    if( $exists){
                                        $ag_ticket = AgentTicket::where('agent_id',$request->agents[$key])->where('ticket_id',$ticket->id)->where('sub_category_id',$sub_category->id)->whereIn('type',['student','general']);
                                        $available = $sub_category->avaliableTickets($ag_ticket->pluck('id')->toArray());
                                    } else {
                                        $available = $sub_category->avaliableTickets();
                                    }
                                    $insert = [];
                                    $insert['price'] = $sub_category->price;
                                    $insert['sub_category_id'] = $sub_category->id;
                                    $available = $sub_category->avaliableTickets();
                                    if ($available < $request->quantity[$key]) {
                                        throw ValidationException::withMessages(['quantity' => __('admin.ticket_unavailable')]);
                                    }
                                    $valid_to = \Carbon\Carbon::parse($ticket->event->end_date);
                                    $valid_from  = \Carbon\Carbon::now();
                                    if($request->valid_from[$key] && $request->valid_to[$key]){
                                        if(Carbon::parse($request->valid_to[$key])->gt($valid_to)) {
                                            throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                                        }
                                        if(Carbon::parse($request->valid_from[$key])->gt($valid_from)) {
                                            throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_error')]);
                                        }
                                        $insert['valid_to']  = Carbon::parse($request->valid_to[$key])->format('Y-m-d');
                                        $insert['valid_from']  = Carbon::parse($request->valid_from[$key])->format('Y-m-d');
                                    } else {
                                        $insert['valid_to']  = $valid_to->format('Y-m-d');
                                        $insert['valid_from']  = $valid_from->format('Y-m-d');
                                    }
                                    
                                    
                                    $insert['ticket_id'] = $ticket->id;
                                    $insert['quantity'] = $request->quantity[$key];
                                    $code = '';
                                    if( strpos(  $request->agent_commission[$key], '%' ) !== false) {
                                        $code = 'P';
                                        $insert['commission'] = str_replace('%','',$request->agent_commission[$key]);
                                        $commission = $sub_category->price * ($insert['commission']/100);
                                    } else {
                                        $code = 'F';
                                        $insert['commission'] = $request->agent_commission[$key];
                                        $commission = $insert['commission'];
                                    }
                                    if( strpos(  $request->user_discount[$key], '%' ) !== false) {
                                        $code = $code.'P';
                                        $insert['discount'] = str_replace('%','',$request->user_discount[$key]);
                                        $discount = $sub_category->price * ($insert['discount']/100);
                                    } else {
                                        $code =  $code.'F';
                                        $insert['discount'] = $request->user_discount[$key];
                                        $discount = $insert['discount'];
                                    }
                                    $insert['code_value'] = $code;
                                    if( $commission  +  $discount > $ticket->price){
                                        throw ValidationException::withMessages(['quantity' => __('admin.comissions_greter_price')]);
                                    }
                                   
                                    $insert['agent_id'] = $request->agents[$key];
                                    if(@$agent_tickets[$insert['agent_id']]){
                                        $agent_tickets[$insert['agent_id']] = [];
                                    }
                                    $insert['max_uses'] = $request->max_uses[$key];
                                    $insert['coupon_code'] = $request->voucher_code[$key];
                                    $types = $request->type[$key];
                                    foreach($types as $type){
                                        $insert['type'] = $type;
                                        $coupon_exist = false;
                                        while (!$coupon_exist) {
                                            $coupon =  Coupon::generate(8);
                                            if (!AgentTicket::where('code', $coupon)->count()) {
                                                $coupon_exist = true;
                                                $insert['code'] = $coupon;
                                            }
                                        }
                                        $exists = $agent_ticket?->where('type',$type)->where('coupon_code', $request->voucher_code[$key])->first();
                                        if($exists){
                                            $exists->update($insert);
                                            $agent_tickets[$insert['agent_id']][] = $exists;
                                        } else {
                                            $agent_tickets[$insert['agent_id']][] = AgentTicket::create($insert);
                                        }

                                    }
                                }
                            } else {
                               
                                $agent_ticket = AgentTicket::where('agent_id',$request->agents[$key])->where('ticket_id',$ticket->id);
                                $exists = $agent_ticket?->first();
                                $insert = [];
                                $insert['price'] = $ticket->price;
                                if( $exists){
                                    $ag_ticket = AgentTicket::where('agent_id',$request->agents[$key])->where('ticket_id',$ticket->id)->whereIn('type',['student','general']);
                                    $available = $ticket->avaliableTickets($ag_ticket->pluck('id')->toArray());
                                } else {
                                    $available = $ticket->avaliableTickets();
                                }
                                if ($available < $request->quantity[$key]) {
                                    throw ValidationException::withMessages(['quantity' => __('admin.ticket_unavailable')]);
                                }
                                if($ticket->sale_price > 0){
                                    $insert['valid_from'] = \Carbon\Carbon::parse($ticket->sale_start_date)->format('Y-m-d');
                                    $insert['valid_to']  =  \Carbon\Carbon::parse($ticket->sale_end_date)->format('Y-m-d');
                                    if($request->valid_from[$key] && $request->valid_to[$key]){
                                        $valid_to = \Carbon\Carbon::parse($insert['valid_to'] );
                                        $valid_from  = \Carbon\Carbon::parse($insert['valid_from']);
                                        if(Carbon::parse($request->valid_to[$key])->gt($valid_to)) {
                                            throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                                        }
                                        if(Carbon::parse($request->valid_from[$key])->lt($valid_from)) {
                                            throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_sale_error')]);
                                        }
                                        $insert['valid_to']  = Carbon::parse($request->valid_to[$key])->format('Y-m-d');
                                        $insert['valid_from']  = Carbon::parse($request->valid_from[$key])->format('Y-m-d');
                                    } 
                                } else {
                                    $insert['valid_to']  = \Carbon\Carbon::parse($ticket->event->end_date)->format('Y-m-d');
                                    $insert['valid_from']  = \Carbon\Carbon::now();
                                    if($request->valid_from[$key] && $request->valid_to[$key]){
                                        $valid_to = \Carbon\Carbon::parse($insert['valid_to'] );
                                        $valid_from  = \Carbon\Carbon::parse($insert['valid_from']);
                                        if(Carbon::parse($request->valid_to[$key])->gt($valid_to)) {
                                            throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                                        }
                                        if(Carbon::parse($request->valid_from[$key])->gt($valid_from)) {
                                            throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_error')]);
                                        }
                                        $insert['valid_to']  = Carbon::parse($request->valid_to[$key])->format('Y-m-d');
                                        $insert['valid_from']  = Carbon::parse($request->valid_from[$key])->format('Y-m-d');
                                    } 
                                }
                               
                                $insert['ticket_id'] = $ticket->id;
                                $insert['quantity'] = $request->quantity[$key];
                                $code = '';
                                if( strpos(  $request->agent_commission[$key], '%' ) !== false) {
                                    $code = 'P';
                                    $insert['commission'] = str_replace('%','',$request->agent_commission[$key]);
                                    $commission = $ticket->price * ($insert['commission']/100);
                                } else {
                                    $code = 'F';
                                    $insert['commission'] = $request->agent_commission[$key];
                                    $commission = $insert['commission'];
                                }
                                if( strpos(  $request->user_discount[$key], '%' ) !== false) {
                                    $code = $code.'P';
                                    $insert['discount'] = str_replace('%','',$request->user_discount[$key]);
                                    $discount = $ticket->price * ($insert['discount']/100);
                                } else {
                                    $code =  $code.'F';
                                    $insert['discount'] = $request->user_discount[$key];
                                    $discount = $insert['discount'];
                                }
                                $insert['code_value'] = $code;
                                if( $commission  +  $discount > $ticket->price){
                                    throw ValidationException::withMessages(['quantity' => __('admin.comissions_greter_price')]);
                                }
                                $insert['agent_id'] = $request->agents[$key];
                                if(@$agent_tickets[$insert['agent_id']]){
                                    $agent_tickets[$insert['agent_id']] = [];
                                }
                                $insert['max_uses'] = $request->max_uses[$key];
                                $insert['coupon_code'] = $request->voucher_code[$key];
                                $types = $request->type[$key];
                                foreach($types as $type){
                                    $insert['type'] = $type;
                                    $coupon_exist = false;
                                    while (!$coupon_exist) {
                                        $coupon =  Coupon::generate(8);
                                        if (!AgentTicket::where('code', $coupon)->count()) {
                                            $coupon_exist = true;
                                            $insert['code'] = $coupon;
                                        }
                                    }
                                    $exists = $agent_ticket?->where('type',$type)->where('coupon_code', $request->voucher_code[$key])->first();
                                    if($exists){
                                        $exists->update($insert);
                                        $agent_tickets[$insert['agent_id']][] = $exists; 
                                    } else {
                                        $agent_tickets[$insert['agent_id']][] = AgentTicket::create($insert);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    foreach($ticket_ids as $ticket_id){
                        $insert = [];
                        @list ($id,$sub_id) = explode(',', preg_replace("/\([^)]*\)+/", '',$ticket_id));
                        
                        $ticket = Ticket::find($id);
                        if($sub_id){
                            $sub_category = TicketSubCategory::find($sub_id);
                            $insert['price'] = $sub_category->price;
                            $insert['sub_category_id'] = $sub_id;
                            $agent_ticket =  AgentTicket::where('agent_id',$request->agents[$key])->where('ticket_id',$id)->where('sub_category_id',$sub_id);
                            $exists = $agent_ticket?->first();
                            $available = $sub_category->avaliableTickets();
                            if( $exists){
                                $ag_ticket = AgentTicket::where('agent_id',$request->agents[$key])->where('ticket_id',$id)->where('sub_category_id',$sub_id)->whereIn('type',['student','general']);
                                $available = $ticket->avaliableTickets($ag_ticket->pluck('id')->toArray());
                            } else {
                                $available = $ticket->avaliableTickets();
                            }
                            if ($available < $request->quantity[$key]) {
                                throw ValidationException::withMessages(['quantity' => __('admin.ticket_unavailable')]);
                            }
                        } else {
                            $insert['price'] = $ticket->price;
                            $agent_ticket = AgentTicket::where('agent_id',$request->agents[$key])->where('ticket_id',$id);
                            $exists = $agent_ticket?->first();
                            if( $exists){
                                $ag_ticket = AgentTicket::where('agent_id',$request->agents[$key])->where('ticket_id',$id)->whereIn('type',['student','general']);
                                $available = $ticket->avaliableTickets($ag_ticket->pluck('id')->toArray());
                            } else {
                                $available = $ticket->avaliableTickets();
                            }
                            if ($available < $request->quantity[$key]) {
                                throw ValidationException::withMessages(['quantity' => __('admin.ticket_unavailable')]);
                            }
                            
                        }
                        
                        if ($available < $request->quantity[$key]) {
                            throw ValidationException::withMessages(['quantity' => __('admin.ticket_unavailable')]);
                        }
                        if($ticket->sale_price > 0){
                            $insert['valid_from'] = \Carbon\Carbon::parse($ticket->sale_start_date)->format('Y-m-d');
                            $insert['valid_to']  =  \Carbon\Carbon::parse($ticket->sale_end_date)->format('Y-m-d');
                            if($request->valid_from[$key] && $request->valid_to[$key]){
                                $valid_to = \Carbon\Carbon::parse($insert['valid_to'] );
                                $valid_from  = \Carbon\Carbon::parse($insert['valid_from']);
                                if(Carbon::parse($request->valid_to[$key])->gt($valid_to)) {
                                    throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                                }
                                
                                if(Carbon::parse($request->valid_from[$key])->lt($valid_from)) {
                                    throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_sale_error')]);
                                }
                                $insert['valid_to']  = Carbon::parse($request->valid_to[$key])->format('Y-m-d');
                                $insert['valid_from']  = Carbon::parse($request->valid_from[$key])->format('Y-m-d');
                            } 
                        } else {
                            $insert['valid_to']  = \Carbon\Carbon::parse($ticket->event->end_date)->format('Y-m-d');
                            $insert['valid_from']  = \Carbon\Carbon::now();
                            if($request->valid_from[$key] && $request->valid_to[$key]){
                                $valid_to = \Carbon\Carbon::parse($insert['valid_to'] );
                                $valid_from  = \Carbon\Carbon::parse($insert['valid_from']);
                                if(Carbon::parse($request->valid_to[$key])->gt($valid_to)) {
                                    throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                                }
                                
                                if(Carbon::parse($request->valid_from[$key])->lt($valid_from)) {
                                    throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_error')]);
                                }
                                $insert['valid_to']  = Carbon::parse($request->valid_to[$key])->format('Y-m-d');
                                $insert['valid_from']  = Carbon::parse($request->valid_from[$key])->format('Y-m-d');
                            } 
                        }
                        
                        $insert['ticket_id'] = $id;
                        $insert['quantity'] = $request->quantity[$key];
                        $insert['agent_id'] = $request->agents[$key];
                        if(@$agent_tickets[$insert['agent_id']]){
                            $agent_tickets[$insert['agent_id']] = [];
                        }
                        $insert['max_uses'] = $request->max_uses[$key];
                        $insert['coupon_code'] = $request->voucher_code[$key];
                        $code = '';
                        if( strpos(  $request->agent_commission[$key], '%' ) !== false) {
                            $code = 'P';
                            $insert['commission'] = str_replace('%','',$request->agent_commission[$key]);
                            $commission = $insert['price'] * ($insert['commission']/100);
                        } else {
                            $code = 'F';
                            $insert['commission'] = $request->agent_commission[$key];
                            $commission = $insert['commission'];
                        }
                        if( strpos(  $request->user_discount[$key], '%' ) !== false) {
                            $code = $code.'P';
                            $insert['discount'] = str_replace('%','',$request->user_discount[$key]);
                            $discount = $insert['price'] * ($insert['discount']/100);
                        } else {
                            $code =  $code.'F';
                            $insert['discount'] = $request->user_discount[$key];
                            $discount = $insert['discount'];
                        }
                        $insert['code_value'] = $code;
                        if( $commission  +  $discount > $ticket->price){
                            throw ValidationException::withMessages(['quantity' => __('admin.comissions_greter_price')]);
                        }
                        $types = $request->type[$key];
                        foreach($types as $type){
                            $insert['type'] = $type;
                            $coupon_exist = false;
                            while (!$coupon_exist) {
                                $coupon =  Coupon::generate(8);
                                if (!AgentTicket::where('code', $coupon)->count()) {
                                    $coupon_exist = true;
                                    $insert['code'] = $coupon;
                                }
                            }
                            $exists = $agent_ticket?->where('type',$type)->where('coupon_code', $request->voucher_code[$key])->first();
                            if($exists){
                                $exists->update($insert);
                                $agent_tickets[$insert['agent_id']][] = $exists;
                            } else {
                                $agent_tickets[$insert['agent_id']][] = AgentTicket::create($insert);
                            }
                        }
                    }

                }
                
           } catch (\Exception $exception) {
                DB::rollback();
                \Log::error($exception);
                if ($exception instanceof ValidationException) {
                    throw $exception;
                }
            }
        }
        DB::commit();
        $agent_ids = array_unique($agent_ids);
        forEach($agent_ids as $id){
            $user = User::find($id);
            try{
                $user->notify(new AgentTicketCommission($event,$user,$agent_tickets[$id]));
            } catch(\Exception $e) {
                \Log::info($e->getMessage());
            }
        }  
        return redirect()->route('voyager.organizer.agents_allocated')->with('success', 'Successfully allocated Tickets');
    }
    private function prepopulateBreadInfo($table)
    {
        $displayName = Str::singular(implode(' ', explode('_', Str::title($table))));
        $modelNamespace = config('voyager.models.namespace', app()->getNamespace());
        if (empty($modelNamespace)) {
            $modelNamespace = app()->getNamespace();
        }

        return [
            'isModelTranslatable'  => true,
            'table'                => $table,
            'slug'                 => Str::slug($table),
            'display_name'         => $displayName,
            'display_name_plural'  => Str::plural($displayName),
            'model_name'           => $modelNamespace . Str::studly(Str::singular($table)),
            'generate_permissions' => true,
            'server_side'          => false,
        ];
    }

    public function getTickets($id, $agent_ticket_id = 0)
    {
        $event = Event::find($id);
        $tickets = $event->tickets;
        $data = [];
        foreach ($tickets as $ticket) {

            if ($ticket->sub_category) {
                $sub_category = '';
                $categories = $ticket->sub_categories;
                foreach ($categories ?? [] as $category) {
                    $available = $category->avaliableTickets();
                    $data[] = ["id" => $ticket->id, "title" => $ticket->title, 'price' => $category->price, 'old_price' => 0, 'available' => $available, 'sub_category' => $category->title, 'sub_category_id' => $category->id];
                }
            } else {
                if ($ticket) {
                    $old_price = 0;
                    if ($ticket->sale_price > 0) {
                        $price = $ticket->sale_price;
                        $old_price = $ticket->price;
                    } else {
                        $price = $ticket->price;
                    }
                    $sub_category = '';
                    $available = $ticket->avaliableTickets();
                    $data[] = ["id" => $ticket->id, "title" => $ticket->title, 'price' => $price, 'old_price' => $old_price, 'available' => $available, 'sub_category' => null, 'sub_category_id' => null];
                }
            }
        }
        return response()->json(['tickets' => $data, 'status' => 'success']);
    }

    public function ticketStore(Request $request)
    {
        $data = $request->validate([
            'agent_id' => 'required|',
            'event_id' => 'required',
            'ticket_id.*' => 'required',
            'sub_category_id.*' => 'nullable|integer',
            'commission.*' => 'required',
            'quantity.*'   => 'required|gte:0',
            'price'     => 'nullable',
            'coupon_code' => 'nullable',
            'common_code' =>  'nullable',
            'coupon_type.*' =>  'nullable|in:general,student',
        ],['event_id' => __('admin.event_required'),'agent_id' => __('admin.agent_required'),'ticket_id.*.required'=> __('admin.ticket_required'),'quantity.*.required' => __('admin.agent_quantity_required')]);
        $hasRecords = false;
        DB::beginTransaction();
        $agent_tickets = [];
        try{

            foreach($data['ticket_id'] as $key=>$ticket_id){
                $insert = [];
                if ($data['quantity'][$key]) {
                    $event_id = Ticket::find($ticket_id)->event_id;
                    $ticketIds = Event::find($event_id)->tickets()->pluck('id')->toArray();
                    if (@$data['sub_category_id'][$key]) {
                        $ticket_model = TicketSubCategory::find($data['sub_category_id'][$key]);
                        $available = $ticket_model->avaliableTickets();
                    } else {
                        $ticket_model = Ticket::find($ticket_id);
                        $available = $ticket_model->avaliableTickets();
                    }
                    //$available = Ticket::find($ticket_id)->avaliableTickets();
                    $event = Event::find($event_id);
                    $ticketIds = $event->tickets()->pluck('id')->toArray();
                    $available = Ticket::find($ticket_id)->avaliableTickets();
                    if ($available < $data['quantity'][$key]) {
                        throw ValidationException::withMessages(['quantity' => __('admin.ticket_unavailable')]);
                    }   
                    if(@$data['sub_category_id'][$key]) {
                        $exists = AgentTicket::where('agent_id',$data['agent_id'])->where('type',$data['coupon_type'])->where('ticket_id',$data['ticket_id'][$key])->where('sub_category_id',$data['sub_category_id'][$key])?->first();
                    } else {
                        $exists = AgentTicket::where('agent_id',$data['agent_id'])->where('type',$data['coupon_type'])->where('ticket_id',$data['ticket_id'][$key])?->first();
                    }
                    $code = '';
                    if( strpos(  $data ['commission'][$key] , '%' ) !== false) {
                        $code = 'PP';
                        $data['commission'][$key] = str_replace('%','',$data ['commission'][$key]);
                        $commission = $ticket_model->price * ($data['commission'][$key]/100);
                    } else {
                        $code = 'FF';
                        $data['commission'][$key] = $data['commission'][$key];
                        $commission = $data['commission'][$key];
                    }
                     $checkCodeWithCommission = AgentTicket::where([
                        'agent_id' => $data['agent_id'],
                        'ticket_id' => $data['ticket_id'][$key],
                        'code' => $data['coupon_code'][$key],
                        'commission' => $data['commission'][$key],
                        'type' => $data['coupon_type']
                    ])?->first();

                    if (!is_null($checkCodeWithCommission)) {
                        throw ValidationException::withMessages(['quantity' => __('admin.ticket_allocated_code')]);
                    }

                    if (@$data['coupon_code'][$key]) {
                        if (AgentTicket::where('code', $data['coupon_code'][$key])->where('agent_id', '!=', $data['agent_id'])->whereNotIn('ticket_id', $ticketIds)->count()) {
                            throw ValidationException::withMessages(['quantity' => __('admin.coupon_code_is_used')]);
                        }
                        $insert['code'] = $data['coupon_code'][$key];
                    } else {
                        $coupon_exist = false;
                        while (!$coupon_exist) {
                            $coupon =  Coupon::generate(8);
                            if (!AgentTicket::where('code', $coupon)->count()) {
                                $coupon_exist = true;
                                $insert['code'] = $coupon;
                            }
                        }
                    }
                    $insert['ticket_id'] = $ticket_id;
                    $insert['quantity'] = $data['quantity'][$key];
                    $insert['price'] = $data['price'][$key];
                    $insert['commission'] = $data['commission'][$key];
                    $insert['agent_id'] = $data['agent_id'];
                    $insert['code_value'] =  $code;
                    $insert['type'] = $data['coupon_type'][$key];

                    if(@$data['sub_category_id'][$key]){
                        $insert['sub_category_id'] = $data['sub_category_id'][$key];
                    }
                    $agent_ticket = AgentTicket::create($insert);
                    $agent_tickets[] = $agent_ticket;
                    $hasRecords = true;
                }
            }
            DB::commit();
            if(!$hasRecords)  throw ValidationException::withMessages(['msg' => __('admin.no_valid_input')]);
            $user = User::find($data['agent_id']);
            try{
                $user->notify(new AgentTicketAllocation($event,$user,$agent_tickets));
            } catch(\Exception $e) {
                \Log::info($e->getMessage());
            }
        } catch (\Exception $exception) {
            DB::rollback();
            \Log::error($exception);
            if ($exception instanceof ValidationException) {
                throw $exception;
            }
            return redirect()->back()->withErrors("Something Went Wrong");
        }

        return redirect()->route('voyager.organizer.agents_allocated');
    }

    public function editSubmitTickets($id)
    {
        $view = 'organizer.edit_submit_tickets';
        $dataType = Voyager::model('DataType')->whereName('agent_tickets')->first();
        $data = $this->prepopulateBreadInfo('agent_tickets');
        $agent_ticket = AgentTicket::find($id);
        if(Str::substr(Str::upper($agent_ticket->code_value),0,1) == 'P') {
            $agent_ticket->commission =  $agent_ticket->commission.'%';
        }
        if(Str::substr(Str::upper($agent_ticket->code_value),1,1) == 'P') {
            $agent_ticket->discount =  $agent_ticket->discount.'%';
        }

        $data['agent_ticket'] = $agent_ticket ;
        $data['agent'] = Agent::find($data['agent_ticket']->agent_id);
        $data['events'] = Event::where('user_id', Auth::id())->get();
        return Eventmie::view($view, $data);
    }

    public function ticketUpdate(Request $request, $id)
    {
        $data = $request->validate([
            'agent_id' => 'required|',
            'ticket_id' => 'required',
            'sub_category_id' => 'nullable',
            'commission' => 'required',
            'quantity'   => 'required|gte:0',
            'price'     => 'nullable',
            'coupon_type' =>  'nullable|in:general,student',
            'user_commission' => 'required',
            'max_uses'        => 'required',
            'coupon_code'     => 'nullable|min:5|max:25',
            'valid_from'     => 'nullable',
            'valid_to'     => 'nullable',
        ],['agent_id' => __('admin.agent_required'),'ticket_id'=> __('admin.ticket_required'),'quantity.required' => __('admin.agent_quantity_required')]);
        $data['type'] = $data['coupon_type'];
        unset($data['coupon_type']);
        if (@$data['sub_category_id']) {
            $ticket_model = TicketSubCategory::find($data['sub_category_id']);
            $available =  $ticket_model->avaliableTickets($id);
        } else {
            $ticket_model = Ticket::find($data['ticket_id']);
            $available = $ticket_model->avaliableTickets($id);
        }
        if ($available < $data['quantity']) {
            throw ValidationException::withMessages(['quantity' => __('admin.ticket_unavailable')]);
        }
        $code = '';
        if( strpos(  $data ['commission'] , '%' ) !== false) {
            $code = 'P';
            $data['commission'] = str_replace('%','',$data ['commission']);
            $commission = $ticket_model->price * ($data['commission']/100);
        } else {
            $code = 'F';
            $data['commission'] = $data['commission'];
            $commission = $data['commission'];
        }
        if( strpos(  $data ['user_commission'] , '%' ) !== false) {
            $code = $code.'P';
            $data['user_commission'] = str_replace('%','',$data ['user_commission']);
            $discount = $ticket_model->price * ($data ['user_commission']/100);
        } else {
            $code =  $code.'F';
            $data['user_commission'] = $data ['user_commission'];
            $discount = $data ['user_commission'];
        }

        $data['code_value'] = $code;
        if($discount + $commission > (int)$request->price)
        {
            throw ValidationException::withMessages(['user_commission' => __('User Discount should no be greater than Ticket Price')]);

        }
        
        $agentTicket = AgentTicket::find($id);
        $coupon_id = $agentTicket->coupon_id;
        if(!$agentTicket) {
            throw ValidationException::withMessages(['quantity' => __('admin.invalid_ticket')]);
        }
        $event_id = Ticket::find($agentTicket->ticket_id)->event_id;
        $event =  Event::find($event_id);

        $ticketIds = $event->tickets()->pluck('id')->toArray();

        if(AgentTicket::where('code', $data['coupon_code'])->where('agent_id', '!=', $data['agent_id'])->whereNotIn('ticket_id', $ticketIds)->count()){
            throw ValidationException::withMessages(['quantity' => __('admin.coupon_code_is_used')]);
        }
        $data['code'] = $data['coupon_code'];
        unset($data['coupon_code']);
        if($coupon_id){
            if($agentTicket->code  != $data['code']){
                $agentTicket->coupon_code = $data['code'] . '00'. (int) $data["user_commission"];
            } else {
                $agentTicket->coupon_code = $data['code'];
            }

            $agentTicket->discount = $data["user_commission"];
            if($agentTicket->ticket->sale_price > 0){
                $valid_from = \Carbon\Carbon::parse($agentTicket->ticket->sale_start_date);
                $valid_to  =  \Carbon\Carbon::parse($agentTicket->ticket->sale_end_date)->format('Y-m-d');
            } else {
                $valid_to = \Carbon\Carbon::parse($agentTicket->ticket->event->end_date)->format('Y-m-d');
                $valid_from = \Carbon\Carbon::now();
            }
            if($request->valid_from && $request->valid_to){
                if(Carbon::parse($request->valid_to)->gt($valid_to)) {
                    throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                }
                if(Carbon::parse($request->valid_from)->gt($valid_from)) {
                    throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_error')]);
                }
                $agentTicket->valid_to  = Carbon::parse($request->valid_to)->format('Y-m-d');
                $agentTicket->valid_from  = Carbon::parse($request->valid_from)->format('Y-m-d');
            } else {
                $agentTicket->valid_to  = $valid_to->format('Y-m-d');
                $agentTicket->valid_from  =  $valid_from->format('Y-m-d');
            }
            $agentTicket->max_uses = $data["max_uses"];
           // $coupon_data = Coupon::find($coupon_id)->update($coupon_data);
        }  else {
            $agentTicket->coupon_code = $data['code'];
            $agentTicket->discount = $data["user_commission"];
            if($agentTicket->ticket->sale_price > 0){
                $valid_from = \Carbon\Carbon::parse($agentTicket->ticket->sale_start_date);
                $valid_to =  \Carbon\Carbon::parse($agentTicket->ticket->sale_end_date);
            } else {
                $valid_to = \Carbon\Carbon::parse($agentTicket->ticket->event->end_date);
                $valid_from  = \Carbon\Carbon::parse($agentTicket->ticket->event->start_date);
            }
            if($request->valid_from && $request->valid_to){
                if(Carbon::parse($request->valid_to)->gt($valid_to)) {
                    throw ValidationException::withMessages(['valid_to' => __('admin.voucher_expire_error')]);
                }
                if(Carbon::parse($request->valid_from)->gt($valid_from)) {
                    throw ValidationException::withMessages(['valid_from' => __('admin.voucher_start_error')]);
                }
                $agentTicket->valid_to = Carbon::parse($request->valid_to)->format('Y-m-d');
                $agentTicket->valid_from  = Carbon::parse($request->valid_from)->format('Y-m-d');
            } else {
                $agentTicket->valid_to  = $valid_to->format('Y-m-d');
                $agentTicket->valid_from  =  $valid_from->format('Y-m-d');
            }
            $agentTicket->max_uses = $data["max_uses"];
            //$agentTicket->code_value = 'fixed';
            

        }
        $agentTicket->quantity = $data["quantity"];
        $agentTicket->commission  = $data["commission"];
        $agentTicket->type= $data['type'];
        $agentTicket->code_value= $data['code_value'];
        $agentTicket->save();
        return redirect()->route('voyager.organizer.agents_allocated');
    }

    public function donwloadTemplate($id,$agent_id){
        $event = Event::find($id);
        $tickets = $event->tickets;
        $data = [];
        $agent_ticket_id = 0;
        foreach ($tickets as $ticket) {

            if ($ticket->sub_category) {
                $sub_category = '';
                $categories = $ticket->sub_categories;
                foreach ($categories ?? [] as $category) {
                    $available = $category->avaliableTickets();
                    $type = '';
                    $code = '';
                    $coupon_code = '';
                    $max_uses  = '';
                    $commission = '';
                    $quantity = '';
                    $agent_ticket_id = '';
                                            $sub_category_id = $category->id;
                    $discount = '';
                    $agent_tickets = AgentTicket::where('agent_id',$agent_id)->where('ticket_id',$ticket->id)->where('sub_category_id',$category->id)->first();
                    if(!empty($agent_tickets)){
                        $type = $agent_tickets->type;
                        $coupon_code = $agent_tickets->coupon_code;
                        $code = $agent_tickets->code;
                        $max_uses = $agent_tickets->max_uses;
                        $commission = $agent_tickets->commission;
                        $quantity = $agent_tickets->quantity;
                        $agent_ticket_id = $agent_tickets->id;
                        $discount = $agent_tickets->discount;
                    }

                    $data[] = ["ticket_category_id" => $ticket->id, "agent_id" => $agent_id, "ticket_category" => $ticket->title, 'sale_price' => $category->price, 'original_price' => 0, 'available' => $available, 'sub_category' => $category->title, 'sub_category_id' => $sub_category_id,'type'=>$type,'number_of_tickets' => $quantity,'agent_commission' => $commission,'code' => $code,'max_uses' => $max_uses,'voucher_code' => $coupon_code,'agent_ticket_id'=>$agent_ticket_id,'discount' => $discount,'valid_from' => '','valid_to' => ''];
                }
            } else {
                if ($ticket) {
                    $old_price = 0;
                    if ($ticket->sale_price > 0) {
                        $price = $ticket->sale_price;
                        $old_price = $ticket->price;
                    } else {
                        $price = $ticket->price;
                    }
                    $sub_category = '';
                    $available = $ticket->avaliableTickets();
                    $type = '';
                    $code = '';
                    $coupon_code = '';
                    $max_uses  = '';
                    $commission = '';
                    $quantity = '';
                    $agent_ticket_id = '';
                    $discount = '';
                    $agent_tickets = AgentTicket::where('agent_id',$agent_id)->where('ticket_id',$ticket->id)->first();
                    if(!empty($agent_tickets)){
                        $type = $agent_tickets->type;
                        $coupon_code = $agent_tickets->coupon_code;
                        $code = $agent_tickets->code;
                        $max_uses = $agent_tickets->max_uses;
                        $commission = $agent_tickets->commission;
                        $quantity = $agent_tickets->quantity;
                        $agent_ticket_id = $agent_tickets->id;
                        $discount = $agent_tickets->discount;
                    }
                    $data[] = ["ticket_category_id" => $ticket->id, "agent_id" => $agent_id,"ticket_category" => $ticket->title, 'sale_price' => $price, 'original_price' => $old_price, 'available' => $available, 'sub_category' => '', 'sub_category_id' => '','type'=>$type,'number_of_tickets' => $quantity,'agent_commission' => $commission,'code' => $code,'max_uses' => $max_uses,'voucher_code' => $coupon_code,'agent_ticket_id'=>$agent_ticket_id,'discount' => $discount,'valid_from' => '','valid_to' => ''];
                }
            }
        }

         // convert array to collection for csv
         $tickets = collect($data);

         // create object of laracsv
         $csvExporter = new \Laracsv\Export();

         // create csv
         $csvExporter->build($tickets, [

             //events fields which will be include
             'ticket_category_id',
             'agent_id',
             'ticket_category',
             'sale_price',
             'original_price',
             'available',
             'sub_category',
             'sub_category_id',
             'type',
             'number_of_tickets',
             'agent_commission',
             'code',
             'discount',
             'max_uses',
             'voucher_code',
             'valid_from',
             'valid_to',
         ]);

         // download csv
         $csvExporter->download($event->slug.'-template.csv');
    }

    function csv_content_parser($content) {
        foreach (explode("\n", $content) as $line) {
          // Generator saves state and can be resumed when the next value is required.
          yield str_getcsv($line);
        }
    }

    public function uploadEventAllocation(Request $request)
    {
        $request->validate(['import_csv' => 'required']);
        $file = $request->file('import_csv');
        if($file->getClientOriginalExtension() != 'csv'){
            throw ValidationException::withMessages(['code' => __('admin.only_csv_file_excepted')]);
        }
        //$request->validate(['import_csv' => 'mimes:text/csv']);

        //$agent_id = $request->input('agent_id');
        $route = "";
        $agent_tickets = [];
        if(isset($file)){
            $name = $file->getClientOriginalName();
            // Get content from csv file.
            $content = file_get_contents($file);
            // Create arrays from csv file's lines.
            $data = array();
            $headers = array();
            $rows = array();
            //get all content
            $header = false;
            $coupon_code = null;
            $agent_ids = [];
            $event  = null;
            $event_id = null;
            $ticketIds = null;
            DB::beginTransaction();
            $errors = [];
            $records = 0;
            $import_records = 0;
            foreach ($this->csv_content_parser($content) as $data) {
                $agent_ticket_id = 0;
                $price = 0;
                if($header && @$data[0]){
                    $state = true;
                    $insert = [];
                    if(!@$data[1]){
                        $state = false;
                    } else {
                        $insert['agent_id'] = $data[1];
                        $agent_id = $data[1];
                        if(@$agent_tickets[$insert['agent_id']]){
                            $agent_tickets[$insert['agent_id']] = [];
                        }
                    }
                    $insert['ticket_id'] = $data[0];
                    $ticket_id = $data[0];
                    $ticket = Ticket::find($ticket_id);
                    $price = $ticket->price;
                    $records++;
                    if($ticket){
                        $import_records++;
                        if($event_id != $ticket->event_id){
                            $expired=false;
                            $event_id = $ticket->event_id;
                            $event = Event::find($event_id);
                            if(Carbon::parse($event->start_date)->isPast()){
                                $errors[$event->id] = $event->title .' ' . __('admin.event_is_expired');
                                $expired = true;
                            }
                        } 
                        if(!$expired) {
                            $ticketIds = $event->tickets()->pluck('id')->toArray();
                            $available = $ticket->avaliableTickets();
                        
                            if(@$data[7] && $ticket->sub_category) {
                                $insert['sub_category_id'] = $data[7];
                                $subcategory = TicketSubCategory::find($insert['sub_category_id']);
                                $available =$subcategory->avaliableTickets();
                                $price = $subcategory->price;
                            }
                            $code = '';
                            if(@$data[12]) {


                                if( strpos( @$data[10] , '%' ) !== false) {
                                    $code = 'P';
                                    $data[10]  = str_replace('%','',$data[10] );
                                    $commission = $price * ($data[10]/100);
                                } else {
                                    $code = 'F';
                                    $commission = $data[10];
                                }
                                if( strpos(  $data[12], '%' ) !== false) {
                                    $code = $code.'P';
                                    $data[12] = str_replace('%','',$data[12]);
                                    $discount = $price * ($data[12]/100);
                                } else {
                                    $code =  $code.'F';
                                    $discount = $data[12];
                                }
                            }
                            if(@$data[10]) {
                                $insert['commission']  = $data[10];
                                if($commission > $price){
                                    $state = false;
                                    $errors['commission'] = 'Agent Commission price is greater than Ticket Price';
                                }
                            } else {
                                if(is_numeric($data[10]) && $data[10] >= 0){
                                    $insert['commission']  = $data[10];
                                    
                                } else {
                                    $state = false;
                                    $errors['commission'] = 'Agent Commission is Mandatory Field';
                                }
                                
                            }
                            if(@$data[9]) {
                                $insert['quantity']  = $data[9];
                            } else {
                                $state = false;
                                $errors['quantity'] = 'Number of Tickets is Mandatory Field';
                            }
                            $insert['price']  = $data[3];
                            if(@$data[11] && @$data[1]){
                                if (AgentTicket::where('code', $data[11])->where('agent_id', '!=', $agent_id)->whereNotIn('ticket_id', $ticketIds)->count()) {
                                    $state = false;
                                    $errors['code'] = 'Code must be unique';
                                }
                                if(strlen($data[11]) < 5){
                                    $state = false;
                                    $errors['code'] = 'Code must be atleast 5 characters';
                                }
                                $insert['code'] = $data[11];
                                $coupon_code   = $data[11];
                            } else {
                                if(!$coupon_code){
                                    $coupon_exist = false;
                                    while (!$coupon_exist) {
                                        $coupon =  Coupon::generate(8);
                                        if (!AgentTicket::where('code', $coupon)->count()) {
                                            $coupon_exist = true;
                                            $coupon_code = $coupon;
                                        }
                                    }
                                } else {
                                    $insert['code'] = $coupon_code;
                                }
                            }

                            if(in_array(@$data[8],['general','student'])){
                                $insert['type'] = $data[8];
                            } else {
                                $state = false;
                                $errors['type'] = 'Type must be general or student';
                            }
                            if(@$data[12]) {
                                $insert['discount']= $data[12];
                                if($discount > $price){
                                    $state = false;
                                    $errors['discount'] = 'Discount price is greater than Ticket Price';
                                }
                            } else {
                                $state = false;
                                $errors['discount'] = 'Discount is Mandatory Field';
                            }

                            if(@$data[13]) {
                                $insert['max_uses']= $data[13];
                            } else {
                                $errors['max_uses'] = 'Max Uses is Mandatory Field';
                                $state = false;
                            }

                            if(@$data[14]) {
                                if(strlen($data[14]) < 5) {
                                    $state = false;
                                    $errors['coupon_code'] = 'Voucher Code Length should be 5 - 25';
                                } else {
                                    $insert['coupon_code']= $data[14];
                                }
                                
                            } else {
                                if(@$insert["discount"]){
                                    $insert['coupon_code'] = $coupon_code . '00'. (int) $insert["discount"];
                                } else {
                                    $state = false;
                                    $errors['discount'] = 'Discount is Mandatory Field';
                                }
                            }
                            if(@$insert['quantity']  > 0){
                                if ($available < (int) $insert['quantity'] ) {
                                    $state= false;
                                    $errors['quantity'] = 'Ticket is not available';
                                }

                                if( $state){
                                    if(@$data[7] && $ticket->sub_category) {
                                        $exists = AgentTicket::where('agent_id',$agent_id)->where('type',$insert['type'])->where('code',@$data[14])->where('ticket_id',$ticket_id)->where('sub_category_id',$data[7])?->first();
                                    } else {
                                        $exists = AgentTicket::where('agent_id',$agent_id)->where('type',$insert['type'])->where('code',@$data[14])->where('ticket_id',$ticket_id)?->first();
                                    }
                                    // $insert['code_value']= 'fixed';
                                    $insert['code_value']= $code;
    
                                    if($exists){
                                        $agent_ticket_id = $exists->id;
                                    }
                                    if($ticket->sale_price > 0){
                                        $valid_from = \Carbon\Carbon::parse($ticket->sale_start_date);
                                        $valid_to =  \Carbon\Carbon::parse($ticket->sale_end_date);
                                    } else {
                                        $valid_to = \Carbon\Carbon::parse($ticket->event->start_date);
                                        $valid_from  = \Carbon\Carbon::now();
                                    }
                                    if(@$data[15] && @$data[16]){
                                        $input = ['date_start' => $data[15]];
                                        $rule  = ['date_start'  => 'date'];
                                        if(!Validator::make($input,$rule)->passes()){
                                            $state= false;
                                            $errors['valid_from'] = 'Valid from Date Format is Invalid.(Y-m-d)';
                                        };
                                        $input = ['date_end' => $data[16]];
                                        $rule  = ['date_end'  => 'date'];
                                        if(!Validator::make($input,$rule)->passes()){
                                            $state= false;
                                            $errors['valid_to'] = 'Valid to Date Format is Invalid.(Y-m-d)';
                                        };
                                        if($state){
                                            if(Carbon::parse(@$data[15])->lt(Carbon::today())) {
                                                $errors['valid_from'] = __('admin.voucher_start_today_error');
                                                $state = false;
                                            }
                                            if(Carbon::parse(@$data[16])->lt(Carbon::today())) {
                                                $errors['valid_to'] = __('admin.voucher_end_today_error');
                                                $state = false;
                                            }
                                            if(Carbon::parse(@$data[16])->gt($valid_to)) {
                                                $errors['valid_to'] = __('admin.voucher_expire_error');
                                                $state = false;
                                            }
                                            if(Carbon::parse(@$data[15])->gt($valid_from)) {
                                                $errors['valid_to'] = __('admin.voucher_start_error');
                                                $state = false;
                                            }
                                            $insert['valid_to']  = Carbon::parse($data[16])->format('Y-m-d');
                                            $insert['valid_from']  = Carbon::parse($data[15])->format('Y-m-d');
                                        }
                                        
                                    } else {
                                        $insert['valid_to']  = $valid_to->format('Y-m-d');
                                        $insert['valid_from']  = $valid_from->format('Y-m-d');
                                    }
                                    if($state){
                                        if($agent_ticket_id) {
                                            $agent_ticket = AgentTicket::find($agent_ticket_id);
                                            if(!empty($agent_ticket)){
                                                $agent_ticket->update($insert);
                                                $agent_tickets[$insert['agent_id']][] = $agent_ticket;
                                            } else {
                                                $agent_tickets[$insert['agent_id']][]= AgentTicket::create($insert);
                                            }
                                        } else {
                                            $agent_tickets[$insert['agent_id']][] = AgentTicket::create($insert);
                                        }
                                        $agent_ids[] = $agent_id;
                                    }
                                    
                                }
                            }
                        }

                        
                    } else {
                        \Log::error("Failed Processing Ticket:".$ticket_id);
                    }
                } else {
                    $header = true;
                }
            }
            if(!empty($errors)){
                DB::rollback();
                throw ValidationException::withMessages($errors);
            } else {
                DB::commit();
                \Log::info("Total Processed Record: ".$import_records . ' of '.$records );
            }
        } else {
            throw ValidationException::withMessages(['code' => __('admin.failed_to_download')]);
        }
        $agent_ids = array_unique($agent_ids);
        forEach($agent_ids as $id){
            $user = User::find($id);
            try{
                $user->notify(new AgentTicketCommission($event,$user,$agent_tickets[$id]));
            } catch(\Exception $e) {
                \Log::info($e->getMessage());
            }
        }
        return redirect()->route('voyager.organizer.agents_allocated')->with([
            'message'    => __('admin.successfully_allocated'),
            'alert-type' => 'success',
        ]);

    }
}
