<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Search extends Component
{
    public $users;
    public $searchTerm;
    public $walletUsers;

    public function mount($users,$walletUsers)
    {
        $this->users= $users;
        $this->walletUsers= $walletUsers;
    }

    public function render()
    {
        $searchTerm = '%'.  $this->searchTerm .'%';
        $this->users = User::whereIn('id',$this->walletUsers)->orderBy('id','desc')->where('name','like', $searchTerm)->get();
        return view('livewire.search');
    }
}
