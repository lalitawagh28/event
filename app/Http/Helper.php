<?php

namespace App\Http;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Twilio\Rest\Client;

class Helper
{
    public static function notifyThroughWebhook(array $data)
    {
        $webhook_url = config('services.webhook_url');
        //Log::info(json_encode(['data' => $data, 'type' => 'tax-create']));

        if (is_null($webhook_url)) {
            return;
        }
       
        try {
            Http::post($webhook_url, $data);
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());
        }
    }

    public static function validateUser(array $data)
    {
        
        //Log::info(json_encode(['data' => $data, 'type' => 'tax-create']));
        $response = null;
        try {
           
           $response =  Http::acceptJson()->post(env('VALIDATE_API_URL').'api/v1/user-validate', $data)
            ->json();
           
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());
        }
        return $response;
    }

    public static function menu($menuName, $type = null, array $options = [])
    {
        return \App\Models\Menu::display($menuName, $type, $options);
    }

    public static function agent_menu($menuName, $type = null, array $options = [])
    {
        return \App\Models\AgentMenu::display($menuName, $type, $options);
    }

    public static function normalizePhone(string $phone): string
    {
        if (strlen($phone) > 10 && $phone[0] == 0) {
            return ltrim($phone, '0');
        }

        return $phone;
    }

    public static function send_otp($phone, $otp)
    {

        $account_sid = config('services.twilio.account_sid');
        $auth_token = config('services.twilio.auth_token');
        $twilio_number = config('services.twilio.mobile');

        $client = new Client($account_sid, $auth_token);
        $msg_details = ['from' => $twilio_number, 'body' => "Kanexy: Your security code is " . $otp . ". It expires in 10 minutes. Don't share this code with anyone."];
        
        $client->messages->create($phone, $msg_details);
    }
}
?>
