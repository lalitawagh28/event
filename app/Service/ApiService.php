<?php

namespace App\Service;

use App\Http\Resources\EventResource;
use App\Http\Resources\OrderResource;
use App\Models\Event;
use App\Models\Order;
use Illuminate\Support\Carbon;

class ApiService
{
    public function filterEvents($request)
    {
        $query = Event::with('category', 'event_category', 'event_category.category');

        if (!is_null($request['category_id'])) {
            $query->whereHas('event_category', function ($subQuery) use ($request) {
                $subQuery->where('category_id', $request['category_id']);
            });
        }

        if (!is_null($request['venue'])) {
            $query->where('venue', $request['venue']);
        }

        $result =  $query->where('status', '1')->where('is_private', '!=', true)->whereDate('end_date', '>=', Carbon::today()->toDateString())->orderBy('id', 'desc')->get();

        return EventResource::collection($result);
    }

    public function getOrders($request)
    {
        $query = Order::query();

        if (!is_null($request['customer_id'])) {
            $query->where('customer_id', $request['customer_id']);
        }

        if (!is_null($request['event_id'])) {
            $query->where('event_id', $request['event_id']);
        }

        if (!is_null($request['event.category'])) {
            $query->where('event.category', $request['event.category']);
        }

        if (!is_null($request['start_date']) && !is_null($request['end_date'])) {
            $query->where('start_date', $request['start_date']);
            $query->where('end_date', $request['end_date']);
        }

        if (!is_null($request['event_start_date']) && !is_null($request['event_end_date'])) {
            $query->where('event_start_date', $request['event_start_date']);
            $query->where('event_end_date', $request['event_end_date']);
        }

        $data = $query->whereDate('end_date', '>=', Carbon::today()->toDateString())->get();

        $data->load('event', 'event.category');

        return OrderResource::collection($data);
    }

    public function getTrendingEvents($type = null, $limit = 3)
    {
        try {
            $query = Event::with('category', 'event_category', 'event_category.category');

            if ($type === 'trending') {
                $query->where('featured', 1)->where('status', 1)->whereDate('end_date', '>=', Carbon::today()->toDateString())->where('is_private', '!=', true);
                return $query->get();
            } elseif ($type === 'upcoming') {
                $query->whereDate('end_date', '>=', Carbon::today()->toDateString())->where('status', 1)->where('is_private', '!=', true);
                return $query->get();
            } else {
                $query->whereDate('end_date', '>=', Carbon::today()->toDateString())->where('status', 1)->where('is_private', '!=', true);
            }

            $events = $query->take($limit)->get();

            return $events;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
