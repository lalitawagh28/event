<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class YapilyService
{

    private string $appId;
    private string $secret;
    private string $authToken;

    public function __construct()
    {
        $this->appId = config('yapily.application_id');
        $this->secret = config('yapily.secret');
        $this->authToken = base64_encode($this->appId . ':' . $this->secret);
    }

    public function getInstitutions()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Basic ' . $this->authToken
        ])->get('https://api.yapily.com/institutions')
            ->throw()
            ->json();

        return $response;
    }

    public function Authorization($order = [], $booking = [])
    {

        $inputData = session('inputData');

        $requestData = [
            "applicationUserId" => $inputData['userId'],
            "institutionId" => $inputData['selectedBank'],
            "callback" => "https://display-parameters.com/",
            "paymentRequest" => [
                "type" => "DOMESTIC_PAYMENT",
                "reference" => "Dhigna Limited",
                "paymentIdempotencyId" => Str::random(20), // Generate a unique ID for paymentIdempotencyId
                "amount" => [
                    "amount" => $order['price'],
                    "currency" => "GBP"
                ],
                "payee" => [
                    "name" => "Dhigna Limited",
                    "accountIdentifications" => [
                        [
                            "type" => "ACCOUNT_NUMBER",
                            "identification" => "12345678"
                        ],
                        [
                            "type" => "SORT_CODE",
                            "identification" => "123456"
                        ]
                    ]
                ]
            ]
        ];


        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . $this->authToken,
        ])->post('https://api.yapily.com/payment-auth-requests', $requestData)->json();

        return $response;
    }
}
