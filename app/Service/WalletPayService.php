<?php

namespace App\Service;

use App\Models\AuthWalletToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Client\RequestException;

class WalletPayService extends Controller
{
    private string $apiUrl;

    private string $email;

    public $token;

    private string $password;

    public function __construct()
    {
        $this->apiUrl = config('services.wallet_pay_api_url');

        $this->email = 'lalita@kanexy.com';

        $this->password  = '123456';

        $data = ['email' => $this->email, 'password' => $this->password];
        $this->user          = new User;
        // $user = session()->all();
        // dd(auth()->user() );

    }

    public function setupAccessToken($accessTokenDto)
    {

        try {
            //dd($this->apiUrl . '/login', $accessTokenDto);
            $token = Http::post($this->apiUrl . '/auth/login', $accessTokenDto)
                ->throw()
               ->json();
              return $token;

        } catch (\Exception $exception) {

            return ['data' => '', 'code' => $exception->getCode()];
        }

    }

    public function profile($token)
    {

        try {
            //dd($this->apiUrl . '/login', $accessTokenDto);
            $response = Http::withHeaders(['Authorization' => 'Bearer '. $token])->get($this->apiUrl . '/auth/profile')
                ->throw()
               ->json();
              return $response;

        } catch (\Exception $exception) {

            return $exception;
        }

    }

    public function getAccessToken($user)
    {
        // dd($user);
        $this->token = AuthWalletToken::whereUserId($user['id'])->first();

    }

    public function getWalletList($user)
    {
        $this->getAccessToken($user);
        // dd(['Authorization' => 'Bearer'. $this->token['token']]);

        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])

            ->get($this->apiUrl . '/get-wallet-list')
            ->throw()
            ->json();

        return $response;
    }
    public function getTransactionList($user)
    {
        $this->getAccessToken($user);
        // dd(['Authorization' => 'Bearer'. $this->token['token']]);

        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])

            ->get($this->apiUrl . '/transactions?transaction_method=wallet')
            ->throw()
            ->json();

        return $response;
    }

    public function getExchangeRate($data,$user)
    {
        $this->getAccessToken($user);
        // dd(['Authorization' => 'Bearer'. $this->token['token']]);

        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])

        ->post($this->apiUrl . '/exchange-rate',$data)
        ->json();

        return $response;

    }

    public function saveWalletDeposit($data,$user){
        $this->getAccessToken($user);

        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])

        ->post($this->apiUrl . '/wallet/initiate-deposit',$data)
        ->json();

        return $response;
    }
    public function walletPayment($data,$user)
    {
        $this->getAccessToken($user);
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])

        ->post($this->apiUrl . '/wallet-pay',$data)
        ->json();

        return $response;

    }


    public function ticketWalletPayment($data,$user)
    {
        $this->getAccessToken($user);
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])
        ->post($this->apiUrl . '/ticket-transfer-wallet-pay',$data)
        ->json();

        return $response;

    }

    public function ticketStripePayment($data,$user)
    {
        $this->getAccessToken($user);
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])
        ->post($this->apiUrl . '/ticket-transfer-stripe-pay',$data)
        ->json();

        return $response;

    }

    public function sendTransferPaymentData($data,$user)
    {

        $this->getAccessToken($user);

        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])

        ->post($this->apiUrl . '/wallet/initiate-transfer',$data)
        ->json();

        return $response;
    }


public function verifyotp($code, $transactionId, $user)
{
    $this->getAccessToken($user);

    try {
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])
            ->post($this->apiUrl . '/wallet/transfer-verification', [
                'code' => $code,
                'transaction_id' => $transactionId
            ])
            ->json();

        return $response;
    } catch (RequestException $exception) {
        $response = $exception->response();

        if ($response && $response->status() === 422) {
            $responseData = $response->json();
            return $responseData; // Return the JSON error response to the caller
        } else {
            return ['error' => 'An unexpected error occurred. Please try again later.'];
        }
    }
}
public function resendOtp($transactionId, $user)
{
    $this->getAccessToken($user);
    try {
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])
            ->post($this->apiUrl . '/wallet/resend/otp', [
                'transaction_id' => $transactionId
            ])
            // ->throw()
            ->json();

        return $response;
    } catch (\Exception $exception) {
        return $exception;
    }
}
    public function walletDepositVerify($data,$user){
        $this->getAccessToken($user);
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])
        ->post($this->apiUrl . '/wallet/deposit-verification',$data)
        ->json();

        return $response;
    }
    public function walletDepositResendOtp($data,$user){
        $this->getAccessToken($user);
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])
        ->post($this->apiUrl . '/wallet/resend/otp',$data)
        ->json();

        return $response;
    }

    public function getTransactions($data,$user){
        $this->getAccessToken($user);
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $this->token['token']])
        ->get($this->apiUrl . '/transactions',$data)
        ->json();

        return $response;

    }



}
