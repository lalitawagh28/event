<?php

namespace App\Service\Agents;

use App\Http\Controllers\Controller;

use Facades\Classiebit\Eventmie\Eventmie;


use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Classiebit\Eventmie\Models\Event;
use Classiebit\Eventmie\Models\User;
use Classiebit\Eventmie\Models\Booking;

use Classiebit\Eventmie\Charts\EventChart;
use Classiebit\Eventmie\Models\Notification;
use Yajra\Datatables\Datatables;

/* From the DownloadsController */
class Dashboard extends Controller
{
    public function __construct()
    {
        $this->event         = new Event; 
        $this->booking       = new Booking;
        $this->notification  = new Notification;
        $this->user          = new User;
    }
    /**
     *  index page
     */
    public function index(Request $request, $user_id = null, $view = 'agents.dashboard')
    {
        $total_customers          = $this->user->total_customers_agent($user_id);
        $total_events             = $this->event->total_events_agent($user_id);
        $total_bookings           = $this->booking->total_bookings_agent($user_id);
        $total_revenue            = $this->booking->total_revenue_agent($user_id);
        $total_notifications      = $this->notification->total_notifications($user_id);
        $events                   = $this->event->get_all_events_agent([], $user_id);
        
        $top_selling_events       = $this->event->top_selling_events_agent($user_id);
        $labels = [];
        $values = [];
        if(!empty($top_selling_events))
        {
            foreach($top_selling_events as $val)
            {
                $labels[] = strlen($val['title']) > 25 ? mb_substr($val['title'], 0, 25, 'utf-8')."..." : $val['title'];
                $values[] = $val['total_bookings'];
            }
        }
        $eventsChart = new EventChart;
        $eventsChart
        ->labels($labels)
        ->dataset(__('voyager::generic.total').' '.__('voyager::generic.Bookings'), 'bar', $values)
        ->color("rgba(27, 137, 239, 1)")
        ->backgroundcolor("rgba(26, 136, 239, 0.7)");

        // if organizer dashboard
        $isOrgDash = false;
        if($user_id)
            $isOrgDash = true;
        
        return view($view, compact(
            'eventsChart', 'total_customers', 'total_bookings', 
            'total_revenue', 'total_notifications', 'total_events', 'events', 'isOrgDash'));
    }


}
