<?php

namespace App\Rules;

use App\Http\Helper;
use Classiebit\Eventmie\Models\User;
use Illuminate\Contracts\Validation\Rule;

class PhoneUnique implements Rule
{
    private $role_id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($role_id=0)
    {
		$this->role_id = $role_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // if($this->role_id > 0){
		// 	return User::where("phone",Helper::normalizePhone($value))->where("role_id","=",$this->role_id)->doesntExist();
		// }
		return User::where("phone",Helper::normalizePhone($value))->doesntExist();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute already exists.';
    }
}
