<?php

namespace App\Stratergies;

use App\Interfaces\WebhookHandler as InterfacesWebhookHandler;
use App\Models\SensyLog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Eventmie\BookingsController;

class WebWebhook implements InterfacesWebhookHandler
{
    public function handle(array $input, string $type)
    {
        if ($type == 'WebBooking') {
            $this->finishWebBooking($input, $type);
        }
        if ($type == 'MobileBooking') {
            $this->finishWebBooking($input, $type);
        }
    }

    public function finishWebBooking($input, $type)
    {
        $finalArray = $input;

        Session::start();

        $sensyLogExists = SensyLog::where('cartId', $finalArray['cartId'])->firstOrFail();
        $metaArray = json_decode($sensyLogExists->meta, true);

        $selected_attendees = $metaArray['selected_attendees'];
        $commission = $metaArray['commission'];
        $booking = $metaArray['booking'];
        $event_promocode = $metaArray['event_promocode'];
        $delivery_address = $metaArray['delivery_address'];
        $address_type = $metaArray['address_type'];
        $collection_point = $metaArray['collection_point'];
        $inputData = $metaArray['inputData'];
        $payment_method = $metaArray['payment_method'];
        $pre_payment = $metaArray['pre_payment'];

        session()->put('selected_attendees', $selected_attendees);
        session()->put('commission', $commission);
        session()->put('booking', $booking);
        session()->put('event_promocode', $event_promocode);
        session()->put('delivery_address', $delivery_address);
        session()->put('address_type', $address_type);
        session()->put('collection_point', $collection_point);
        session()->put('inputData', $inputData);
        session()->put('payment_method', $payment_method);
        session()->put('pre_payment', $pre_payment);

        Auth::loginUsingId($metaArray['user_id']);

        if ($finalArray['transStatus']  == 'Y') {
            $flag['transaction_id']    = $finalArray['transId'];
            $flag['payer_reference']    = $finalArray['transId'];
            $flag['status']             = true;
            $flag['message']            = 'Captured';
        } else {
            $flag = [
                'status'    => false,
                'error'     => $inputData['rawAuthMessage'],
            ];
        }
        $booking = new BookingsController();
        return $booking->finish_checkout($flag);
    }
}
