<?php

namespace App\Stratergies;

use App\Interfaces\WebhookHandler as InterfacesWebhookHandler;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegistrationCrud implements InterfacesWebhookHandler
{
    public function handle(array $body, string $type)
    {
        if($type == 'user_registration'){
            $this->usersRegistration($body,$type);
        }
    }

    public function usersRegistration($body,$type)
    {
        try {
            
            $body['password'] = Hash::make($body['password']);
            $body['phone'] = $body['phone'];
            $body['role_id'] = 2;
            
            $user = User::create($body);
        
            $url = (config('app.env') == 'production') ? 'https://dhigna.com/callback' : 'https://uat.dhigna.com/callback';
            
            $clientRepository =  app('Laravel\Passport\ClientRepository');
            $clientRepository->create($user->id,$user->name,$url);

            
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
