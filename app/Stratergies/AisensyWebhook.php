<?php

namespace App\Stratergies;

use App\Interfaces\WebhookHandler as InterfacesWebhookHandler;
use App\Models\Event;
use App\Models\SensyLog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\api\BookingController as ApiBookingController;
use GuzzleHttp\Client;

class AisensyWebhook implements InterfacesWebhookHandler
{
    public function handle(array $input, string $type)
    {
        if ($type == 'AiSensyBooking') {
            $this->aiSensyBooking($input, $type);
        }
    }

    public function aiSensyBooking($input, $type)
    {
        $inputData = $input;

        $sensLogExists = SensyLog::where('cartId', $inputData['cartId'])->first();
        if ($inputData['transStatus']  == 'Y') {
            $flag['transaction_id']    = $inputData['transId'];
            $flag['payer_reference']    = $inputData['transId'];
            $flag['status']             = true;
            $flag['message']            = 'Captured';
        } else {
            // not successful
            $flag = [
                'status'    => false,
                'error'     => $inputData['rawAuthMessage'],
            ];
        }
        $this->finishAiSensyBooking($sensLogExists, $flag);
        return response()->json(['status' => true], 200);
    }

    public function finishAiSensyBooking($sensLogExists, $flag)
    {
        $transaction_id = $flag['transaction_id'];
        $log = json_decode($sensLogExists->meta);
        $metaArray = json_decode($sensLogExists->meta, true);
        $meta = array_merge($metaArray, $flag);
        $event = Event::with('tickets')->find($metaArray['event_id']);
        $tickets = $event->tickets;
        $ticketIds = $tickets->pluck('id')->toArray();
        $ticketTitle = [];
        $names = [];
        $addresses = [];
        $phones = [];
        $is_donations = [];
        $quantities = [];

        $username = $metaArray['name'];
        $userMobile = $metaArray['mobile'];
        $userAddress = $metaArray['address'];
        $product_items = $metaArray['product_items'];

        foreach ($tickets as $ticket) {
            $title = [];
            $name = [];
            $address = [];
            $mobile = [];
            $donation = [];
            $title[] = $ticket->title;
            $ticketTitle[] = $title;
            $name[] = $username;
            $names[] = $name;
            $address[] = $userAddress;
            $addresses[] = $address;
            $mobile[] = $userMobile;
            $phones[] = $mobile;
            $is_donations[] = $donation;
            $qt = 0;
            foreach ($product_items as $item) {
                if ($ticket->ticket_retailer_id == $item['product_retailer_id']) {
                    $qt = $item['quantity'];
                }
            }
            $quantities[] = $qt;
        }

        $jsonData = [
            "event_id" => $event->id,
            "voucher_code" => $metaArray['voucher_code'],
            "booking_date" => $event->start_date,
            "start_time" => $event->start_time,
            "end_time" => $event->end_time,
            "customer_id" => $metaArray['user'],
            "address_type" => 1,
            "delivery_address" => $userAddress,
            "collection_point" => '',
            "quantity" => $quantities,
            "ticket_id" => $ticketIds,
            "ticket_title" => $ticketTitle,
            "address" => $addresses,
            "name" => $names,
            "phone" => $phones,
            "is_donation" => $is_donations,
            'transaction_id' => $transaction_id,
            'delivery_charge_id' => $metaArray['delivery_charge_id']

        ];

        Auth::loginUsingId($metaArray['user']);

        $apiBooking = new ApiBookingController();
        $request = new  Request($jsonData);

        $bookingResponse = $apiBooking->ai_sensy_book_tickets($request, $flag);
        $sensLogExists->payment_success = 1;
        $sensLogExists->status = 1;
        $sensLogExists->meta = json_encode($meta);
        $sensLogExists->save();

        $client = new Client();

        $headers = [
            'Content-Type' => 'application/json',
        ];
        $templateMobile = '+' . $log->mobile;
        $data = [
            "apiKey" => config('aisensy.apiKey'),
            "campaignName" => "Success Booking",
            "destination" => "$templateMobile",
            "userName" => "$log->name",
            "templateParams" => [
                "$transaction_id",
            ],
        ];

        $response = $client->post('https://backend.aisensy.com/campaign/t1/api/v2', [
            'headers' => $headers,
            'json' => $data,
        ]);

        // Get the response body as a string
        $body = $response->getBody()->getContents();

        return response()->json(['data' => 'Booking Successfully completed']);
    }
}
