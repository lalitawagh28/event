<?php

namespace App\Stratergies;

use App\Interfaces\WebhookHandler as InterfacesWebhookHandler;
use App\Models\SensyLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\api\TransferController;

class TransferTicketWebhook implements InterfacesWebhookHandler
{
    public function handle(array $input, string $type)
    {
        if ($type == 'TransferTicket') {
            $this->ticketTransferWebhook($input, $type);
        }
    }

    public function ticketTransferWebhook($input, $type)
    {
        $data = $input;

        $sensyLogExists = SensyLog::where('cartId', $data['cartId'])->first();
        $metaArray = json_decode($sensyLogExists->meta, true);

        $booking = $metaArray['booking'];
        $payment_method = $metaArray['payment_method'];
        $pre_payment = $metaArray['pre_payment'];
        $type = $metaArray['type'];
        $transfer_status = $metaArray['transfer_status'];
        $booking_id = $metaArray['booking_id'];

        session()->put('booking', $booking);
        session()->put('payment_method', $payment_method);
        session()->put('pre_payment', $pre_payment);
        session()->put('type', $type);
        session()->put('transfer_status', $transfer_status);
        session()->put('booking_id', $booking_id);

        Auth::loginUsingId($metaArray['user_id']);

        if ($data['transStatus']  == 'Y') {
            $flag['transaction_id']    = $data['transId'];
            $flag['payer_reference']    = $data['transId'];
            $flag['status']             = true;
            $flag['message']            = 'Captured';
            $transfer_status            = 'success';
        } else {
            $flag = [
                'status'    => false,
                'error'     => $input['message'],
            ];
        }

        $booking = new TransferController();
        return $booking->AcceptRequest($booking_id);
    }
}
