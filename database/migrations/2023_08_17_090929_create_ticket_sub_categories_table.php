<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_sub_categories', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title', 64);
			$table->decimal('price', 10);
			$table->integer('quantity');
            $table->timestamps();
            $table->integer('ticket_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_sub_categories');
    }
};
