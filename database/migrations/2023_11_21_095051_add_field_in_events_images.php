<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function(Blueprint $table)
		{
            $table->string('listview', 255)->nullable();
            $table->string('checkout_banner', 255)->nullable();
            $table->string('calendar_image', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('listview');
            $table->dropColumn('checkout_banner');
            $table->dropColumn('calendar_image');
        });
    }
};
