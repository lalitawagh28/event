<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agent_tickets', function(Blueprint $table)
		{
            $table->enum('code_value', ['fixed', 'percentage']);
            $table->decimal('discount', 8, 2);
            $table->integer('max_uses')->unsigned()->default(true)->nullable();
            $table->date('valid_from')->nullable();
            $table->date('valid_to')->nullable();
            $table->string('coupon_code', 32);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent_tickets', function(Blueprint $table)
		{
            $table->dropColumn('code_value');
            $table->dropColumn('discount');
            $table->dropColumn('max_uses');
            $table->dropColumn('valid_from');
            $table->dropColumn('valid_to');
        });
    }
};
