<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_seat_chart', function (Blueprint $table) {
            $table->id();
            $table->string('seat_name');
            $table->string('x');
            $table->string('y');
            $table->integer('ticket_id')->unsigned();
            $table->integer('sub_cat_id')->unsigned()->nullable();
            $table->integer('event_id')->unsigned();
            $table->foreign('ticket_id')->references('id')->on('tickets')->onDelete('cascade');
            $table->foreign('sub_cat_id')->references('id')->on('ticket_sub_categories')->onDelete('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_seat_chart');
    }
};
