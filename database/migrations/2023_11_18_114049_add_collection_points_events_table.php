<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            if (!Schema::hasColumn('events', 'collection_point_enable')) {
                $table->boolean('collection_point_enable')->default(false)->after('delivery_address_enable');;
            }

            if (!Schema::hasColumn('events', 'same_as_attendee_enable')) {
                $table->boolean('same_as_attendee_enable')->default(false)->after('collection_point_enable');;
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function(Blueprint $table)
		{
            $table->dropColumn('collection_point_enable');
            $table->dropColumn('same_as_attendee_enable');
        });
    }
};
