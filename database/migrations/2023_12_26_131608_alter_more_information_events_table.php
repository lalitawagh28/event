<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            if (!Schema::hasColumn('events', 'enable_additional_information')) {
                $table->boolean('enable_additional_information')->nullable()->after('meta_description');
            }
            if (!Schema::hasColumn('events', 'additional_title')) {
                $table->string('additional_title')->nullable()->after('enable_additional_information');
            }
            if (!Schema::hasColumn('events', 'more_information')) {
                $table->string('more_information')->nullable()->after('additional_title');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('enable_additional_information');
            $table->dropColumn('additional_title');
            $table->dropColumn('more_information');
        });
    }
};
