<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->string('agent_name', 100);
            $table->string('code', 32)->unique();
            $table->enum('code_type', ['free', 'paid']);
            $table->enum('code_value', ['fixed', 'percentage']);
            $table->decimal('discount', 8, 2);
            $table->integer('quantity')->nullable();
            $table->integer('max_uses')->unsigned()->default(true)->nullable();
            $table->boolean('is_active')->default(true);
            $table->date('valid_from')->nullable();
            $table->date('valid_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
};
