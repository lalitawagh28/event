<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_tickets', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedBigInteger('agent_id');
            $table->integer('ticket_id')->unsigned();
			$table->integer('quantity')->unsigned();
			$table->decimal('price', 10)->unsigned();
            $table->decimal('commission', 10)->unsigned();
            $table->foreign('ticket_id')->references('id')->on('tickets')->onDelete('cascade');
            $table->foreign('agent_id')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_tickets');
    }
};
