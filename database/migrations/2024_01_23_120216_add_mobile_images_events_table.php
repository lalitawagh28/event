<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function(Blueprint $table)
		{
            $table->string('mat_trending', 255)->nullable()->after('poster');
            $table->string('mat_card', 255)->nullable()->after('mat_trending');
            $table->string('mat_detail', 255)->nullable()->after('mat_card');
            $table->string('mat_poster', 255)->nullable()->after('mat_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function(Blueprint $table)
		{
            $table->dropColumn('mat_trending');
            $table->dropColumn('mat_card');
            $table->dropColumn('mat_detail');
            $table->dropColumn('mat_poster');
        });
    }
};
