<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function(Blueprint $table)
		{
            $table->string('guest_star', 255)->nullable()->after('faq');
            $table->string('organised_by', 255)->nullable()->after('guest_star');
            $table->string('ticket_logo', 255)->nullable()->after('organised_by');
            $table->string('ticket_bg', 255)->nullable()->after('ticket_logo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function(Blueprint $table)
		{
            $table->dropColumn('guest_star');
            $table->dropColumn('organised_by');
            $table->dropColumn('ticket_logo');
            $table->dropColumn('ticket_bg');
        });
    }
};
