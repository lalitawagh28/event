<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('event_id')->unsigned();
            $table->unsignedBigInteger('customer_id');
            $table->integer('quantity')->unsigned();
            $table->decimal('price', 10)->unsigned();
			$table->decimal('tax', 10)->nullable();
			$table->decimal('net_price', 10)->nullable();
            $table->string('promocode_reward')->nullable()->default(0);
            $table->unsignedBigInteger('promocode_id')->nullable();
            $table->foreign('customer_id')->references('id')->on('users')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('promocode_id')->references('id')->on('promocodes')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('event_id')->references('id')->on('events')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
