<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_promocodes', function (Blueprint $table) {
            $table->unsignedBigInteger('promocode_id');
            $table->unsignedInteger('event_id');
            $table->unique(['promocode_id', 'event_id']);
            $table->foreign('promocode_id')->references('id')->on('promocodes')
                ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('event_id')->references('id')->on('events')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_promocodes');
    }
};
