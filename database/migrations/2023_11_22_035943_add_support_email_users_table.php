<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'support_email')) {
                $table->string('support_email')->nullable()->after('status');
            }
            if (!Schema::hasColumn('users', 'support_phone')) {
                $table->string('support_phone')->nullable()->after('support_email');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('support_email');
            $table->dropColumn('support_phone');
        });
    }
};
