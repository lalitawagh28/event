<?php

namespace Database\Seeders;

use App\Models\Event;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UpdateEventCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = Event::all();
        forEach($events as $event){
            $categories = [];
            if($event->category_id){
                $categories[] = $event->category_id;
                $event->categories()->sync($categories);
            }
        }
    }
}
