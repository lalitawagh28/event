<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Classiebit\Eventmie\Models\Banner;

class UpdateBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banner = $this->banner('id', 1);
        if ($banner->exists) {
            $banner->fill([
                'title' => 'Dhigna Events FullyLoaded',
                'subtitle' => 'Event management & selling platform',
                'image' => 'banners/August2019/3MIAC8BaLwk8ytlYYvVi.jpg',
                'status' => 1,
                'order' => 1,
                'button_url' => config('app.url').'events',
                'button_title' => 'Get Event Tickets',
            ])->save();
        }
    }

    
    protected function banner($field, $for)
    {
        return Banner::firstOrNew([$field => $for]);
    }
}
