<?php

namespace Database\Seeders;

use App\Models\Title;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = collect(["Mr","Mrs","Miss","Dr","Lord","Sir"])
            ->map(fn ($title) => ['name' => $title]);

        Title::insert($titles->toArray());
    }
}
