<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class UserRoleAgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = $this->role('id', 8);
        if (!$role->exists) {
            $role->fill(["name" => "agent", "display_name" => "Agent (agent)"])->save();
        }
    }

    protected function role($field, $for)
    {
        return Role::firstOrNew([$field => $for]);
    }
}
