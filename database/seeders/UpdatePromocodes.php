<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class UpdatePromocodes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slug           = 'promocodes';
        $DataType      = DataType::where('slug', $slug)->firstOrFail();

        $dataRow = $this->dataRow($DataType, "code");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Promo Code", "required" => 1, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" =>  [
            "validation" => [
                    "rule" => "required|max:32|unique:promocodes,code,1"
                ]
            ], "order" => 2, ])->save();
        }
    }

      /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
