<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class TicketDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singular       = 'ticket';
        $slug           = 'tickets';
        $dataType       = $this->dataType('slug', $slug);
        // if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $slug,
                'slug'                  => $slug,
                'display_name_singular' => ucfirst($singular),
                'display_name_plural'   => ucfirst($slug),

                'icon'                  => 'voyager-dollar',
                'model_name'            => 'Classiebit\\Eventmie\\Models\\Ticket',
                'policy_name'           => NULL,
                'controller'            => '\\App\\Http\\Controllers\\Eventmie\\Organizer\\TicketsController',
                'description'           => NULL,
                'generate_permissions'  => 1,
                'server_side'           => 1,
                'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
            ])->save();
        // }

        $DataType      = DataType::where('slug', $slug)->firstOrFail();

        // add rows (auto-generated)
        $dataRow = $this->dataRow($DataType, "id");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Id", "required" => 1, "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 9, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "title");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Ticket Category", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "validation" => [
                "rule" => "max:256" 
            ] 
        ], "order" => 3, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "price");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Ticket Price", "required" => 1, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 4, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "quantity");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Ticket Lot", "required" => 1, "browse" => 1, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 5, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "description");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "rich_text_box", "display_name" => "Description", "required" => 0, "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => "{}", "order" => 6, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "event_id");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Event Id", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 1, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "created_at");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "timestamp", "display_name" => "Created At", "required" => 0, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 10, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "updated_at");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "timestamp", "display_name" => "Updated At", "required" => 0, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 11, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "status");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "select_dropdown", "display_name" => "Status", "required" => 1, "browse" => 1, "read" => 1, "edit" => 1, "add" => 0, "delete" => 0, "details" => [
        "default" => "1", 
        "options" => [
                "1"=>"Enabled",
                "0"=>"Disabled", 
            ], 
        "validation" => [
                    "rule" => "required" 
                ] 
        ], "order" => 32, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "customer_limit");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Customer Limit", "required" => 0, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 14, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "t_soldout");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Tickets Sold", "required" => 0, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 15, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "sale_start_date");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Sale Start Date", "required" => 0, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 15, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "sale_end_date");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Sale End Date", "required" => 0, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 15, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "sale_price");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Sale Price", "required" => 1, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 3, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "is_donation");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "select_dropdown", "display_name" => "Donation", "required" => 1, "browse" => 1, "read" => 1, "edit" => 1, "add" => 0, "delete" => 0, "details" => [
        "default" => "1", 
        "options" => [
                "1"=>"Yes",
                "0"=>"No", 
        ]],"order" => 33 ])->save();
        }$dataRow = $this->dataRow($DataType, "ticket_hasone_event_relationship");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Event Name", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\Event", 
        "table" => "events", 
        "type" => "belongsTo", 
        "column" => "event_id", 
        "key" => "id", 
        "label" => "title", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 2, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "event_url");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Event Url", "required" => 1, "browse" => 1, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 12, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "data_time");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Date/Timings", "required" => 1, "browse" => 1, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 13, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "event_location");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Event Location", "required" => 1, "browse" => 1, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 2, ])->save();
        }
    }

         /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
