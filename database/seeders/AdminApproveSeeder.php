<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Classiebit\Eventmie\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminApproveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::where('role_id',1)->update([
            'admin' => true,
            'approved_at' => \Carbon\Carbon::now()
         ]);
    }
}
