<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;


class UpdateDataTaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singular       = 'tax';
        $slug           = 'taxes';
        $dataType       = $this->dataType('slug', $slug);
        // if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $slug,
                'slug'                  => $slug,
                'display_name_singular' => ucfirst($singular),
                'display_name_plural'   => ucfirst($slug),

                'icon'                  => 'voyager-documentation',
                'model_name'            => 'Classiebit\\Eventmie\\Models\\Tax',
                'policy_name'           => NULL,
                'controller'            => '\\Classiebit\\Eventmie\\Http\\Controllers\\TaxController',
                'description'           => NULL,
                'generate_permissions'  => 1,
                'server_side'           => 1,
                'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
            ])->save();
        // }
        
        $singular       = 'banner';
        $slug           = 'banners';
        $dataType       = $this->dataType('slug', $slug);
        // if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $slug,
                'slug'                  => $slug,
                'display_name_singular' => ucfirst($singular),
                'display_name_plural'   => ucfirst($slug),

                'icon'                  => 'voyager-photo',
                'model_name'            => 'Classiebit\\Eventmie\\Models\\Banner',
                'policy_name'           => NULL,
                'controller'            => '\\Classiebit\\Eventmie\\Http\\Controllers\\BannerController',
                'description'           => NULL,
                'generate_permissions'  => 1,
                'server_side'           => 0,
                'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
            ])->save();
    }

     

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
