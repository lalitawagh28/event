<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class DefaultOrganizerPassword extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $setting = $this->findSetting("apps.default_organizar_password");
        if (!$setting->exists) {
            $setting->fill(["display_name" => "Default Organizar Password", "value" => "", "details"=> null, "type" => "text", "order" => "52", "group" => "Apps", ])->save();
        }
    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }

}