<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class SalesReportMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agentMenu = $this->menus('name', 'agents');
        if (!$agentMenu->exists) {
            $agentMenu->save();
        }


         // Parent
        $menuItemAgent = MenuItem::firstOrNew(["menu_id" => $agentMenu->id, "title" => "Sales Report", "url" => "#", "route" => null, ]);
        if (!$menuItemAgent->exists) {
             $menuItemAgent->fill(["target" => "_self", "icon_class" => "voyager-people", "color" => "", "parent_id" => null, "order" => "5", ])->save();
        }
         // Child
        $menuItem = MenuItem::firstOrNew(["menu_id" => $agentMenu->id, "title" => "Sales Report By Event", "url" => "", "route" => "voyager.agents.sales_report_by_event", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => null , "color" => "", "parent_id" => $menuItemAgent->id, "order" => "1", ])->save();
        }
        $menuItem = MenuItem::firstOrNew(["menu_id" => $agentMenu->id, "title" => "Sales Report By Voucher", "url" => "", "route" => "voyager.agents.sales_report_by_voucher", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemAgent->id, "order" => "2", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $agentMenu->id, "title" => "Sales Report By Commission", "url" => "", "route" => "voyager.agents.sales_report_commission", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemAgent->id, "order" => "3", ])->save();
        }


        $organizarMenu = $this->menus('name', 'organizer');
        if (!$organizarMenu->exists) {
            $organizarMenu->save();
        }


         // Parent
        $menuItemOrganizer = MenuItem::firstOrNew(["menu_id" => $organizarMenu->id, "title" => "Sales Report", "url" => "#", "route" => null, ]);
        if (!$menuItemOrganizer->exists) {
             $menuItemOrganizer->fill(["target" => "_self", "icon_class" => "voyager-people", "color" => "", "parent_id" => null, "order" => "5", ])->save();
        }
         // Child
        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $organizarMenu->id, "title" => "Sales Report By Event", "url" => "", "route" => "voyager.organizer.sales_report_by_event", ]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => null , "color" => "", "parent_id" => $menuItemOrganizer->id, "order" => "1", ])->save();
        }
        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $organizarMenu->id, "title" => "Sales Report By Voucher", "url" => "", "route" => "voyager.organizer.sales_report_by_voucher", ]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemOrganizer->id, "order" => "2", ])->save();
        }

        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $organizarMenu->id, "title" => "Sales Report By Agent", "url" => "", "route" => "voyager.organizer.sales_report_by_agent", ]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemOrganizer->id, "order" => "3", ])->save();
        }

        $menuAdminItem = MenuItem::firstOrNew(["menu_id" => $organizarMenu->id, "title" => "Sales Report By Commission", "url" => "", "route" => "voyager.organizer.sales_report_commission", ]);
        if (!$menuAdminItem->exists) {
            $menuAdminItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemOrganizer->id, "order" => "4", ])->save();
        }


        $adminMenu = $this->menus('name', 'admin');
        if (!$adminMenu->exists) {
            $adminMenu->save();
        }


        $menuItemAdmin = MenuItem::firstOrNew(["menu_id" => $adminMenu->id, "title" => "Sales Report", "url" => "#", "route" => null, ]);
        if (!$menuItemAdmin->exists) {
             $menuItemAdmin->fill(["target" => "_self", "icon_class" => "voyager-people", "color" => "", "parent_id" => null, "order" => "5", ])->save();
        }
         // Child
        $menuAdminItem = MenuItem::firstOrNew(["menu_id" => $adminMenu->id, "title" => "Sales Report By Event", "url" => "", "route" => "voyager.sales_report_by_event", ]);
        if (!$menuAdminItem->exists) {
            $menuAdminItem->fill(["target" => "_self", "icon_class" => null , "color" => "", "parent_id" => $menuItemAdmin->id, "order" => "1", ])->save();
        }
        $menuAdminItem = MenuItem::firstOrNew(["menu_id" => $adminMenu->id, "title" => "Sales Report By Voucher", "url" => "", "route" => "voyager.sales_report_by_voucher", ]);
        if (!$menuAdminItem->exists) {
            $menuAdminItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemAdmin->id, "order" => "2", ])->save();
        }

        $menuAdminItem = MenuItem::firstOrNew(["menu_id" => $adminMenu->id, "title" => "Sales Report By Agent", "url" => "", "route" => "voyager.sales_report_by_agent", ]);
        if (!$menuAdminItem->exists) {
            $menuAdminItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemAdmin->id, "order" => "3", ])->save();
        }

        $menuAdminItem = MenuItem::firstOrNew(["menu_id" => $adminMenu->id, "title" => "Sales Report By Commission", "url" => "", "route" => "voyager.sales_report_commission", ]);
        if (!$menuAdminItem->exists) {
            $menuAdminItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemAdmin->id, "order" => "4", ])->save();
        }

    }

    protected function menus($field, $for)
    {
        return Menu::firstOrNew([$field => $for]);
    }
    protected function menuItem($field, $for)
    {
        return MenuItem::firstOrNew([$field => $for]);
    }
}
