<?php

namespace Database\Seeders;

use App\Models\Booking;
use App\Models\Order;
use App\Models\OrderBooking;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UpdateOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bookings = Booking::all();
        $data = [];
        $total = 0;
        $tax = 0;
        $net = 0;
        $quantity = 0;
        $promocode_reward = 0;
        $ids = [];
        $previousBooking = null;
        foreach($bookings as $booking){
            $exists = OrderBooking::where('booking_id',$booking->id)?->first();
            if(!$exists){
                if($previousBooking){
                    if($previousBooking->customer_id == $booking->customer_id && 
                       $previousBooking->event_id == $booking->event_id &&
                       $previousBooking->ticket_id == $booking->ticket_id &&
                       $previousBooking->created_at == $booking->created_at ) {
                        $data[] = $booking;
                        $total += $booking->price;
                        $tax += $booking->tax;
                        $net += $booking->net_price;
                        $promocode_reward += $booking->promocode_reward;
                        $ids[] = $booking->id;
                        $quantity++;
                        $previousBooking = $booking;
                       } else {
                        $order = New Order();
                        $order->event_id = $previousBooking->event_id;
                        $order->customer_id = $previousBooking->customer_id;
                        $order->quantity = $quantity;
                        $order->price = $total;
                        $order->tax = $tax;
                        $order->net_price = $net;
                        $order->promocode_reward = $promocode_reward;
                        $order->save();
                        $order->bookings()->sync($ids);
                        $previousBooking = $booking;
                        $data = [];
                        $ids = [];
                        $data[] = $booking;
                        $total = $booking->price;
                        $tax = $booking->tax;
                        $net = $booking->net_price;
                        $promocode_reward = $booking->promocode_reward;
                        $ids[] = $booking->id;
                        $quantity = 1;
                       }
                } else {
                    $previousBooking = $booking;
                    $data[] = $booking;
                    $total = $booking->price;
                    $tax = $booking->tax;
                    $net = $booking->net_price;
                    $ids[] = $booking->id;
                    $promocode_reward = $booking->promocode_reward;
                    $quantity = 1;

                }
            }
        }
        if($data){
            $order = New Order();
            $order->event_id = $previousBooking->event_id;
            $order->customer_id = $previousBooking->customer_id;
            $order->quantity = $quantity;
            $order->price = $total;
            $order->tax = $tax;
            $order->net_price = $net;
            $order->promocode_reward = $promocode_reward;
            $order->save();
            $order->bookings()->sync($ids);
        }
    }
}
