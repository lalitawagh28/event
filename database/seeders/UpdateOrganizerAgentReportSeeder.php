<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class UpdateOrganizerAgentReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slug           = 'organizer_agent_reports';
        $DataType      = DataType::where('slug', $slug)->firstOrFail();
        $dataRow = $this->dataRow($DataType, "promocode");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Voucher Code", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 4, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "promocode_reward");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Discount", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 4, ])->save();
        }
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
