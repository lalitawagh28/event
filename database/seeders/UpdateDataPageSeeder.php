<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class UpdateDataPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singular       = 'page';
        $slug           = 'pages';
        $dataType       = $this->dataType('slug', $slug);
        // if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $slug,
                'slug'                  => $slug,
                'display_name_singular' => ucfirst($singular),
                'display_name_plural'   => ucfirst($slug),
                'icon'                  => 'voyager-file-text',
                'model_name'            => 'Classiebit\\Eventmie\\Models\\Page',
                'policy_name'           => NULL,
                'controller'            => 'Classiebit\\Eventmie\\Http\\Controllers\\Voyager\\PageController',
                'description'           => NULL,
                'generate_permissions'  => 1,
                'server_side'           => 0,
                'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
            ])->save();
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
