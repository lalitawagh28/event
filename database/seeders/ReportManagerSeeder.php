<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class ReportManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reportManagerMenu = $this->menus('name', 'report_manager');
        if (!$reportManagerMenu->exists) {
            $reportManagerMenu->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "Dashboard", "url" => url('/report-manager/dashboard'), "route" => ""]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-boat", "color" => "", "parent_id" => null, "order" => "1",])->save();
        }

        // Parent
        $menuItemReportManager = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "Reports", "url" => "#", "route" => null,]);
        if (!$menuItemReportManager->exists) {
            $menuItemReportManager->fill(["target" => "_self", "icon_class" => "voyager-people", "color" => "", "parent_id" => null, "order" => "2",])->save();
        }
        // Child
        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "Sales Report By Event", "url" => "", "route" => "voyager.report-manager.sales_report_by_event",]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => 'voyager-list', "color" => "", "parent_id" => $menuItemReportManager->id, "order" => "1",])->save();
        }
        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "Sales Report By Voucher", "url" => "", "route" => "voyager.report-manager.sales_report_by_voucher",]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => 'voyager-list', "color" => "", "parent_id" => $menuItemReportManager->id, "order" => "2",])->save();
        }

        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "Sales Report By Agent", "url" => "", "route" => "voyager.report-manager.sales_report_by_agent",]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => 'voyager-list', "color" => "", "parent_id" => $menuItemReportManager->id, "order" => "3",])->save();
        }

        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "Sales Report By Commission", "url" => "", "route" => "voyager.report-manager.sales_report_commission",]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => 'voyager-list', "color" => "", "parent_id" => $menuItemReportManager->id, "order" => "4",])->save();
        }

        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "Agent Reports", "url" => "", "route" => "voyager.report-manager.agent_reports",]);

        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => "voyager-list", "color" => "", "parent_id" => $menuItemReportManager->id, "order" => "5",])->save();
        }

        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "User Bookings", "url" => "", "route" => "voyager.report-manager.user_bookings", ]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => "voyager-list", "color" => "", "parent_id" =>  $menuItemReportManager->id, "order" => "6", ])->save();
        }

        $menuOrganizerItem = MenuItem::firstOrNew(["menu_id" => $reportManagerMenu->id, "title" => "User Invoices", "url" => "", "route" => "voyager.report-manager.reports", ]);
        if (!$menuOrganizerItem->exists) {
            $menuOrganizerItem->fill(["target" => "_self", "icon_class" => "voyager-list", "color" => "", "parent_id" =>  $menuItemReportManager->id, "order" => "7", ])->save();
        }
    }

    protected function menus($field, $for)
    {
        return Menu::firstOrNew([$field => $for]);
    }
    protected function menuItem($field, $for)
    {
        return MenuItem::firstOrNew([$field => $for]);
    }
}
