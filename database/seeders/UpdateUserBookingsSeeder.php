<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class UpdateUserBookingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slug           = 'user_bookings';
        $DataType      = DataType::where('slug', $slug)->firstOrFail();

        // add rows (auto-generated)
        $dataRow = $this->dataRow($DataType, "event_id");

        $dataRow = $this->dataRow($DataType, "bookings_hasone_ticket_relationship");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Ticket Category", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\Ticket", 
        "table" => "tickets", 
        "type" => "belongsTo", 
        "column" => "ticket_id", 
        "key" => "id", 
        "label" => "title", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 2, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "bookings_hasone_sub_cat_relationship");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Sub Category", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "App\Models\TicketSubCategory", 
        "table" => "ticket_sub_categories", 
        "type" => "belongsTo", 
        "column" => "sub_category_id", 
        "key" => "id", 
        "label" => "title", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 3, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "net_price");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Ticket Total", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 7, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "agent_hasone_event_relationship");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Total Tickets", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\Event", 
        "table" => "users", 
        "type" => "belongsTo", 
        "column" => "event_id", 
        "key" => "id", 
        "label" => "quantity", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 8, ])->save();
        }
    }

      /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
