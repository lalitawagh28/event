<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class UpdateAgentAdminDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slug           = 'agent_tickets';

        $DataType      = DataType::where('slug', $slug)->firstOrFail();

        // add rows (auto-generated)
        $dataRow = $this->dataRow($DataType, "code_value");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Discount Type", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 6, ])->save();
        }
    }
     /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
