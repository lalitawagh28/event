<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class UserRoleReportManager extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = $this->role('id', 9);
        if (!$role->exists) {
            $role->fill(["name" => "report_manager", "display_name" => "Reports Manager"])->save();
        }
    }

    protected function role($field, $for)
    {
        return Role::firstOrNew([$field => $for]);
    }
}
