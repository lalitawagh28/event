<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;


class WorldPaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $worldpay_url = $this->findSetting("apps.worldpay_url");
        $worldpay_mode = $this->findSetting("apps.worldpay_mode");
        $worldpay_instid = $this->findSetting("apps.worldpay_instid");
        $worldpay_status = $this->findSetting("apps.worldpay_status");

        if (!$worldpay_url->exists) {
            $worldpay_url->fill(["display_name" => "WorldPay URL", "value" => "", "details" => null, "type" => "text", "order" => "52", "group" => "Apps",])->save();
        }
        if (!$worldpay_mode->exists) {
            $worldpay_mode->fill(["display_name" => "Mode", "value" => "", "details" => null, "type" => "text", "order" => "52", "group" => "Apps",])->save();
        }
        if (!$worldpay_instid->exists) {
            $worldpay_instid->fill(["display_name" => "Installment ID", "value" => "", "details" => null, "type" => "text", "order" => "52", "group" => "Apps",])->save();
        }
        if (!$worldpay_status->exists) {
            $worldpay_status->fill(["display_name" => "Status", "value" => "", "details" => null, "type" => "text", "order" => "52", "group" => "Apps",])->save();
        }
    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
