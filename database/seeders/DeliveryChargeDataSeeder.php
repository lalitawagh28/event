<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;

class DeliveryChargeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singular       = 'delivery-charge';
        $slug           = 'delivery-charges';
        $DataType       = $this->dataType('slug', $slug);
        // if (!$dataType->exists) {
        $DataType->fill([
            'name'                  => $slug,
            'slug'                  => $slug,
            'display_name_singular' => 'Delivery Charge',
            'display_name_plural'   => 'Delivery Charges',

            'icon'                  => 'voyager-tag',
            'model_name'            => 'App\\Models\\DeliveryCharge',
            'policy_name'           => NULL,
            'controller'            => '\\App\\Http\\Controllers\\Eventmie\\Voyager\\DeliveryChargesController',
            'description'           => NULL,
            'generate_permissions'  => 1,
            'server_side'           => 1,
            'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
        ])->save();

        $dataRow = $this->dataRow($DataType, "id");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Id", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 1, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "title");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Title", "required" => 1, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "validation" => [
                "rule" => "required|max:64|unique:categories,name,1" 
            ] 
        ], "order" => 2, ])->save();
        }
        
        $dataRow = $this->dataRow($DataType, "rate");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Rate", "required" => 1, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "validation" => [
                "rule" => "required|numeric" 
            ] 
        ], "order" => 4, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "net_price");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "select_dropdown", "display_name" => "Net Price", "required" => 1, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "default" => "including", 
        "options" => [
                "including" => "Including", 
                "excluding" => "Excluding" 
            ], 
        "validation" => [
                    "rule" => "required|in:including,excluding" 
                ] 
        ], "order" => 5, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "status");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "select_dropdown", "display_name" => "Status", "required" => 1, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "default" => "1", 
        "options" => [
                "1"=>"Enabled", 
                "0"=>"Disabled", 
            ], 
        "validation" => [
                    "rule" => "required" 
                ] 
        ], "order" => 6, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "admin_delivery_charge");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "select_dropdown", "display_name" => "Admin Delivery Charges", "required" => 1, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "default" => "1", 
        "options" => [
                "1"=>"Yes", 
                "0"=>"No", 
            ], 
        "validation" => [
                    "rule" => "required" 
                ] 
        ], "order" => 9, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "created_at");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "timestamp", "display_name" => "Created At", "required" => 0, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 7, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "updated_at");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "timestamp", "display_name" => "Updated At", "required" => 0, "browse" => 1, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 8, ])->save();
        }
        
    }

     /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
