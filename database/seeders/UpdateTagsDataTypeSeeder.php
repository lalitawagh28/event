<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class UpdateTagsDataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   $singular       = 'tag';
        $slug           = 'tags';
        $dataType      = DataType::where('slug', $slug)->firstOrFail();
        $dataType->fill([
            'name'                  => $slug,
            'slug'                  => $slug,
            'display_name_singular' => ucfirst($singular),
            'display_name_plural'   => ucfirst($slug),

            'icon'                  => 'voyager-puzzle',
            'model_name'            => 'Classiebit\\Eventmie\\Models\\Tag',
            'policy_name'           => NULL,
            'controller'            => '\\Classiebit\\Eventmie\\Http\\Controllers\\TagsController',
            'description'           => NULL,
            'generate_permissions'  => 1,
            'server_side'           => 1,
            'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
        ])->save();
    }
}
