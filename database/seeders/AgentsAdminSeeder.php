<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class AgentsAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = $this->menus('name', 'agents');
        if (!$menu->exists) {
            $menu->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Dashboard", "url" => "", "route" => "voyager.agents.dashboard", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-boat", "color" => "", "parent_id" => null, "order" => "1", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Events", "url" => "", "route" => "voyager.agents.events", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-calendar", "color" => "", "parent_id" => null, "order" => "2", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Ticket Category", "url" => "", "route" => "voyager.agents.ticket_category", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-categories", "color" => "", "parent_id" => null, "order" => "3", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Reports", "url" => "", "route" => "voyager.agents.reports", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-documentation", "color" => "", "parent_id" => null, "order" => "4", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Agent Commission", "url" => "", "route" => "voyager.agents.commission", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-dollar", "color" => "", "parent_id" => null, "order" => "5", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "User Bookings", "url" => "", "route" => "voyager.agents.user_bookings", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-dollar", "color" => "", "parent_id" => null, "order" => "6", ])->save();
        }

    

    }

    protected function menus($field, $for)
    {
        return Menu::firstOrNew([$field => $for]);
    }
    protected function menuItem($field, $for)
    {
        return MenuItem::firstOrNew([$field => $for]);
    }
}
