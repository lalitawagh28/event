<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class UpdateAgentAllocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singular       = 'agent_ticket';
        $slug           = 'agent_tickets';
        
        // }

        $DataType      = DataType::where('slug', $slug)->firstOrFail();

        // add rows (auto-generated)
        $dataRow = $this->dataRow($DataType, "agent_id");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Agent ID", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 1, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "agent_hasone_user_relationship");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Agent Name", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\User", 
        "table" => "users", 
        "type" => "belongsTo", 
        "column" => "agent_id", 
        "key" => "id", 
        "label" => "name", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 2, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "agent_hasone_user_phone_relationship");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Agent Phone", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\User", 
        "table" => "users", 
        "type" => "belongsTo", 
        "column" => "agent_id", 
        "key" => "id", 
        "label" => "phone", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 3, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "agent_hasone_user_email_relationship");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Agent Email", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\User", 
        "table" => "users", 
        "type" => "belongsTo", 
        "column" => "agent_id", 
        "key" => "id", 
        "label" => "email", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 4, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "event_id");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Event Name", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 5, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "agent_hasone_ticket_relationship");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Ticket Category", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\Ticket", 
        "table" => "users", 
        "type" => "belongsTo", 
        "column" => "ticket_id", 
        "key" => "id", 
        "label" => "title", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 6, ])->save();
        } 
        $dataRow = $this->dataRow($DataType, "type");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Customer Type", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => [
                "default" => "general", 
                "options" => [
                "general"=>"General", 
                "student"=>"Student", 
            ]], "order" => 7, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "commission");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Agent Commission", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 8, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "discount");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "User Discount", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 9, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "code");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Coupon Code", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 10, ])->save();
        }


        $dataRow = $this->dataRow($DataType, "valid_from");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "date", "display_name" => "Validity From", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 11, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "valid_to");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "date", "display_name" => "Validity To", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 12, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "quantity");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "No of Tickets Allocated", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 13, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "max_uses");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Maxium uses per user", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 14, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "coupon_code");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Voucher Code", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 15, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "price");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Ticket Price", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 16, ])->save();
        }
        

    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
