<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class AgentsDataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singular       = 'agent';
        $slug           = 'agents';
        $dataType       = $this->dataType('slug', $slug);
        // if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $slug,
                'slug'                  => $slug,
                'display_name_singular' => ucfirst($singular),
                'display_name_plural'   => ucfirst($slug),

                'icon'                  => 'voyager-people',
                'model_name'            => 'App\\Models\\Agent',
                'policy_name'           => NULL,
                'controller'            => '\\App\\Http\\Controllers\\Eventmie\\Organizer\\AgentsController',
                'description'           => NULL,
                'generate_permissions'  => 1,
                'server_side'           => 1,
                'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
            ])->save();
        // }
        $DataType      = DataType::where('slug', $slug)->firstOrFail();
        $dataRow = $this->dataRow($DataType, "name");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Agent Name", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 1, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "phone");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Agent Phone", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 2, ])->save();
        }
        
        $dataRow = $this->dataRow($DataType, "email");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Agent Email", "required" => 1, "browse" => 1, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 3, ])->save();
        }
        
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
