<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\RegisterCountry;
class RegisterCountriesEnableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code = ['UK'];
        RegisterCountry::whereIn('code',$code)->update(['enable'=>1]);
        RegisterCountry::whereNotIn('code',$code)->update(['enable'=>0]);
    }
}
