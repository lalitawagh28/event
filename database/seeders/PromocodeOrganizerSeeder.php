<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class PromocodeOrganizerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'organizer')->firstOrFail();

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Promocodes", "url" => "", "route" => "voyager.organizer.promocodes.index", ]);
        if ($menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-tag", "color" => "", "parent_id" => null, "order" => "4","route" => "" ])->save();
        }
    }

    protected function menuItem($field, $for)
    {
        return MenuItem::firstOrNew([$field => $for]);
    }

}

