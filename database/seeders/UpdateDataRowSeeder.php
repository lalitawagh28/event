<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Classiebit\Eventmie\Models\Banner;
use TCG\Voyager\Models\DataType;

class UpdateDataRowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banner = $this->banner('id', 1);
        if ($banner->exists) {
            $banner->fill([
                'title' => 'Eventmie Pro FullyLoaded',
                'subtitle' => 'Event management & selling platform',
                'image' => 'banners/August2019/3MIAC8BaLwk8ytlYYvVi.jpg',
                'status' => 1,
                'order' => 1,
                'button_url' => config('app.url').'events',
                'button_title' => 'Get Event Tickets',
            ])->save();
        }
        $singular       = 'post';
        $slug           = 'posts';
        $dataType       = $this->dataType('slug', $slug);
        // if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $slug,
                'slug'                  => $slug,
                'display_name_singular' => ucfirst($singular),
                'display_name_plural'   => ucfirst($slug),
                'icon'                  => 'voyager-news',
                'model_name'            => 'Classiebit\\Eventmie\\Models\\Post',
                'policy_name'           => 'TCG\\Voyager\\Policies\\PostPolicy',
                'controller'            => '\\Classiebit\\Eventmie\\Http\\Controllers\\PostController',
                'description'           => NULL,
                'generate_permissions'  => 1,
                'server_side'           => 1,
                'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
            ])->save();
            $singular       = 'page';
            $slug           = 'pages';
            $dataType       = $this->dataType('slug', $slug);
            // if (!$dataType->exists) {
                $dataType->fill([
                    'name'                  => $slug,
                    'slug'                  => $slug,
                    'display_name_singular' => ucfirst($singular),
                    'display_name_plural'   => ucfirst($slug),
                    'icon'                  => 'voyager-file-text',
                    'model_name'            => 'Classiebit\\Eventmie\\Models\\Page',
                    'policy_name'           => NULL,
                    'controller'            => '\\Classiebit\\Eventmie\\Http\\Controllers\\PageController',
                    'description'           => NULL,
                    'generate_permissions'  => 1,
                    'server_side'           => 0,
                    'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
                ])->save();
    }

    
    protected function banner($field, $for)
    {
        return Banner::firstOrNew([$field => $for]);
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
