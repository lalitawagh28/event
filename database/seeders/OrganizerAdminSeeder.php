<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class OrganizerAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = $this->menus('name', 'organizer');
        if (!$menu->exists) {
            $menu->save();
        }


        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Dashboard", "url" => "", "route" => "voyager.organizer.dashboard", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-boat", "color" => "", "parent_id" => null, "order" => "1", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Events", "url" => "", "route" => "voyager.organizer.events", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-calendar", "color" => "", "parent_id" => null, "order" => "2", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Ticket Category", "url" => "", "route" => "voyager.organizer.ticket_category", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-categories", "color" => "", "parent_id" => null, "order" => "3", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Bookings", "url" => "", "route" => "voyager.organizer.bookings", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-dollar", "color" => "", "parent_id" => null, "order" => "4", ])->save();
        }
         // Parent
         $menuItemAgent = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Agents", "url" => "#", "route" => null, ]);
         if (!$menuItemAgent->exists) {
             $menuItemAgent->fill(["target" => "_self", "icon_class" => "voyager-people", "color" => "", "parent_id" => null, "order" => "5", ])->save();
         }
         // Child
         $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Agents Allocated", "url" => "", "route" => "voyager.organizer.agents_allocated", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => null , "color" => "", "parent_id" => $menuItemAgent->id, "order" => "2", ])->save();
        }
        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Agents Avaliable", "url" => "", "route" => "voyager.organizer.agents_available", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => null, "color" => "", "parent_id" => $menuItemAgent->id, "order" => "1", ])->save();
        }
        
        $menuItemReport = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Reports", "url" => "#", "route" => "", ]);
        
        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "User Bookings", "url" => "", "route" => "voyager.organizer.user_bookings", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-dollar", "color" => "", "parent_id" =>  $menuItemReport->id, , "order" => "6", ])->save();
        }

        if (!$menuItemReport->exists) {
            $menuItemReport->fill(["target" => "_self", "icon_class" => "voyager-list", "color" => "", "parent_id" => null, "order" => "7", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Booking Reports", "url" => "", "route" => "voyager.organizer.reports", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-list", "color" => "", "parent_id" => $menuItemReport->id, "order" => "1", ])->save();
        }

        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Agent Reports", "url" => "", "route" => "voyager.organizer.agent_reports", ]);
        if (!$menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-list", "color" => "", "parent_id" => $menuItemReport->id, "order" => "2", ])->save();
        }

    }
    protected function menus($field, $for)
    {
        return Menu::firstOrNew([$field => $for]);
    }
    protected function menuItem($field, $for)
    {
        return MenuItem::firstOrNew([$field => $for]);
    }
}
