<?php

namespace Database\Seeders;

use App\Models\Coupon;
use Classiebit\Eventmie\Models\Booking;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UpdateAgentBookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bookings = Booking::all();
        foreach($bookings as $booking) {
            if($booking->promocode) {
                $agent_ticket = Coupon::where('code',$booking->promocode)->first()?->agent_ticket;
                if($agent_ticket) {
                        if(!$booking->agent_id) {
                            $booking->agent_id = $agent_ticket->agent_id;
                            $booking->save();
                        }
                }
            }
        }
    }
}
