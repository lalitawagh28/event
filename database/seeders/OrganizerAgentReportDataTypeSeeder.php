<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class OrganizerAgentReportDataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singular       = 'report';
        $slug           = 'organizer_agent_reports';
        $dataType       = $this->dataType('slug', $slug);
        // if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $slug,
                'slug'                  => $slug,
                'display_name_singular' => "Report",
                'display_name_plural'   => "Reports",

                'icon'                  => 'voyager-list',
                'model_name'            => '\\Classiebit\\Eventmie\\Models\\Booking',
                'policy_name'           => NULL,
                'controller'            => '\\App\\Http\\Controllers\\Eventmie\\Organizer\\ReportsController',
                'description'           => NULL,
                'generate_permissions'  => 1,
                'server_side'           => 1,
                'details'               => json_decode('{"order_column":"updated_at","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}'),
            ])->save();
        // }

        $DataType      = DataType::where('slug', $slug)->firstOrFail();

        // add rows (auto-generated)
        
        $dataRow = $this->dataRow($DataType, "transaction_id");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Transaction Id", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 1, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "booking_hasone_event_relationship");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Event Name", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\Event", 
        "table" => "tickets", 
        "type" => "belongsTo", 
        "column" => "event_id", 
        "key" => "id", 
        "label" => "title", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 2, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "booking_hasone_ticket_relationship");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Ticket Category", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "Classiebit\Eventmie\Models\Ticket", 
        "table" => "tickets", 
        "type" => "belongsTo", 
        "column" => "ticket_id", 
        "key" => "id", 
        "label" => "title", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 3, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "data_time");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Date/Time", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 4, ])->save();
        }
        $dataRow = $this->dataRow($DataType, "agent_id");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "text", "display_name" => "Agent Id", "required" => 1, "browse" => 1, "read" => 0, "edit" => 1, "add" => 1, "delete" => 0, "details" => "{}", "order" => 5, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "booking_hasone_agent_relationship");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Agent Name", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "App\Models\Agent", 
        "table" => "tickets", 
        "type" => "belongsTo", 
        "column" => "agent_id", 
        "key" => "id", 
        "label" => "name", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 6, ])->save();
        }
        
        $dataRow = $this->dataRow($DataType, "booking_hasone_agent_email_relationship");
        if (!$dataRow->exists) {
            $dataRow->fill(["type" => "relationship", "display_name" => "Agent Email", "required" => 0, "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
        "model" => "App\Models\Agent", 
        "table" => "tickets", 
        "type" => "belongsTo", 
        "column" => "agent_id", 
        "key" => "id", 
        "label" => "email", 
        "pivot_table" => "categories", 
        "pivot" => "0", 
        "taggable" => "0" 
        ], "order" => 7, ])->save();
        }
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
