<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class RemovePromocodeFields extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slug           = 'promocodes';
        $DataType      = DataType::where('slug', $slug)->firstOrFail();

        $dataRow = $this->dataRow($DataType, "p_type");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "select_dropdown", "display_name" => "Type", "required" => 0, "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1, "details" => [
            "default" => "fixed",
            "options" => [
                    "fixed" => "Fixed",
                    "percent" => "Percent"
                ],
            "validation" => [
                        "rule" => "required|in:fixed,percent"
                    ]
            ], "order" => 5, ])->save();
        }

        $dataRow = $this->dataRow($DataType, "expires_at");
        if ($dataRow->exists) {
            $dataRow->fill(["type" => "timestamp", "display_name" => "Expires At", "required" => 0, "browse" => 0, "read" => 1, "edit" => 0, "add" => 0, "delete" => 0, "details" => "{}", "order" => 8, ])->save();
        }

    }

      /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
