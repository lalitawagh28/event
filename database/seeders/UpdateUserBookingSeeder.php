<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class UpdateUserBookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = $this->menus('name', 'organizer');
        if (!$menu->exists) {
            $menu->save();
        }

        $menuItemReport = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "Reports", "url" => "#", "route" => "", ]);
        $menuItem = MenuItem::firstOrNew(["menu_id" => $menu->id, "title" => "User Bookings", "url" => "", "route" => "voyager.organizer.user_bookings", ]);
        if ($menuItem->exists) {
            $menuItem->fill(["target" => "_self", "icon_class" => "voyager-dollar", "color" => "", "parent_id" =>  $menuItemReport->id, "order" => "6", ])->save();
        }

    }
    protected function menus($field, $for)
    {
        return Menu::firstOrNew([$field => $for]);
    }
    protected function menuItem($field, $for)
    {
        return MenuItem::firstOrNew([$field => $for]);
    }
}
