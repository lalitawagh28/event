<?php

namespace Classiebit\Eventmie\Http\Controllers;
use Facades\Classiebit\Eventmie\Eventmie;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use File;
use App\Http\Helper;
use Auth;
use Classiebit\Eventmie\Http\Controllers\Voyager\VoyagerBaseController;
use Classiebit\Eventmie\Models\Category;
use TCG\Voyager\Facades\Voyager;


class CategoryController extends VoyagerBaseController
{

    public function __construct()
    {

        // language change
        $this->middleware('common');

        $this->category          = new Category;
    }

    // add tags
    public function store(Request $request)
    {

        $path = '/categories/'.Carbon::now()->format('FY').'/';
        $request->validate([
            'name'        => 'required|max:40|regex:/^[&a-zA-Z0-9_\-\s]*$/',
            'slug'        => 'required|max:40|regex:/^[&a-zA-Z0-9_\-]*$/',
        ]);
        // for image
        if($request->hasfile('thumb'))
        {
            // image type validation
            $request->validate([
                'thumb'        => 'mimes:jpeg,png,jpg',
            ]);

            $file      = $request->file('thumb');
            $extension = $file->getClientOriginalExtension(); // getting poster extension
            $image     = time().rand(1,988).'.'.$extension;
            // $file->storeAs('public/'.$path, $image);
            $image_resize    = Image::make($file)->resize(512,512);

            // if directory not exist then create directiory
            if (! File::exists(storage_path('/app/public/').$path)) {
                File::makeDirectory(storage_path('/app/public/').$path, 0777, true);
            }

            $image_resize->save(storage_path('/app/public/'.$path.$image));
            $image     = $path.$image;
        }

        // image is required
        if(empty($image))
        {
            // if have  image and database have images no images this event then apply this rule
            $request->validate([
                'thumb'        => 'required|mimes:jpeg,png,jpg',
            ]);
        }
        return parent::store($request);
    }

    public function update(Request $request, $id)
    {

        $path = '/categories/'.Carbon::now()->format('FY').'/';
        $request->validate([
            'name'        => 'required|max:40|regex:/^[&a-zA-Z0-9_\-\s]*$/',
            'slug'        => 'required|max:40|regex:/^[&a-zA-Z0-9_\-]*$/',
        ]);
        // for image
        if($request->hasfile('thumb'))
        {
            // image type validation
            $request->validate([
                'thumb'        => 'mimes:jpeg,png,jpg',
            ]);

            $file      = $request->file('thumb');
            $extension = $file->getClientOriginalExtension(); // getting poster extension
            $image     = time().rand(1,988).'.'.$extension;
            // $file->storeAs('public/'.$path, $image);
            $image_resize    = Image::make($file)->resize(512,512);

            // if directory not exist then create directiory
            if (! File::exists(storage_path('/app/public/').$path)) {
                File::makeDirectory(storage_path('/app/public/').$path, 0777, true);
            }

            $image_resize->save(storage_path('/app/public/'.$path.$image));
            $image     = $path.$image;
        }
        return parent::update($request, $id);
    }

    public function update_order(Request $request)
    {

        //$slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', 'categories')->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $model = app($dataType->model_name);

        $order = json_decode($request->input('order'));
        $column = $dataType->order_column;
        $index = 0;
        foreach ($order as $key => $item) {
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $i = $model->withTrashed()->findOrFail($item->id);
            } else {
                $i = $model->findOrFail($item->id);
            }
            $i->$column = ($index + 1);
            $i->save();
        }
        return response()->json(['success' => true]);

    }
}
