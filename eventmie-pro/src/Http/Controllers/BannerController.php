<?php

namespace Classiebit\Eventmie\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use File;
use Classiebit\Eventmie\Http\Controllers\Voyager\VoyagerBaseController;



class BannerController extends VoyagerBaseController
{

    public function __construct()
    {

        // language change
        $this->middleware('common');
    }

    // add tags
    public function store(Request $request)
    {

        $path = "/banners/".Carbon::now()->format('FY').'/';
        $request->validate([
            'title'        => 'required|max:40|regex:/^[a-zA-Z&0-9_\-\s]*$/',
            'subtitle'        => 'required|max:40|regex:/^[a-zA-Z&0-9_\-\s]*$/',
            'button_url'        => 'required|max:40|regex:/[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)/i',
            'button_title'        => 'required|max:40|regex:/^[a-zA-Z0-9_\-\s]*$/',
        ]);
        // for image
        if($request->hasfile('image'))
        {
            // image type validation
            $request->validate([
                'image'        => 'mimes:jpeg,png,jpg',
            ]);

            $file      = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting poster extension
            $image     = time().rand(1,988).'.'.$extension;
            // $file->storeAs('public/'.$path, $image);
            $image_resize    = Image::make($file)->resize(512,512);

            // if directory not exist then create directiory
            if (! File::exists(storage_path('/app/public/').$path)) {
                File::makeDirectory(storage_path('/app/public/').$path, 0777, true);
            }

            $image_resize->save(storage_path('/app/public/'.$path.$image));
            $image     = $path.$image;
        }

        // image is required
        if(empty($image))
        {
            // if have  image and database have images no images this event then apply this rule
            $request->validate([
                'image'        => 'required|mimes:jpeg,png,jpg',
            ]);
        }
        return parent::store($request);
    }

    public function update(Request $request, $id)
    {

        $path = '/banners/'.Carbon::now()->format('FY').'/';
        $request->validate([
            'title'        => 'required|max:40|regex:/^[a-zA-Z&0-9_\-\s]*$/',
            'subtitle'        => 'required|max:40|regex:/^[a-zA-Z&0-9_\-\s]*$/',
            'button_url'        => 'required|max:40|regex:/[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)/i',
            'button_title'        => 'required|max:40|regex:/^[a-zA-Z0-9_\-\s]*$/',
        ]);
        // for image
        if($request->hasfile('image'))
        {
            // image type validation
            $request->validate([
                'image'        => 'mimes:jpeg,png,jpg',
            ]);

            $file      = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting poster extension
            $image     = time().rand(1,988).'.'.$extension;
            // $file->storeAs('public/'.$path, $image);
            $image_resize    = Image::make($file)->resize(512,512);

            // if directory not exist then create directiory
            if (! File::exists(storage_path('/app/public/').$path)) {
                File::makeDirectory(storage_path('/app/public/').$path, 0777, true);
            }

            $image_resize->save(storage_path('/app/public/'.$path.$image));
            $image     = $path.$image;
        }

        return parent::update($request, $id);
    }

}
