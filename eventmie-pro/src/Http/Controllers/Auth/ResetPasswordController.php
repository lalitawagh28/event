<?php

namespace Classiebit\Eventmie\Http\Controllers\Auth;
use Facades\Classiebit\Eventmie\Eventmie;

use App\Http\Controllers\Controller;
use App\Http\Helper;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Classiebit\Eventmie\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         // language change
        $this->middleware('common');
        $this->middleware('guest');
        $this->redirectTo = !empty(config('eventmie.route.prefix')) ? config('eventmie.route.prefix') : '/';
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        return Eventmie::view('eventmie::auth.passwords.reset', ['token' => $token, 'email' => $request->email]);
    }

    // forgot password reset 
    public function reset(Request $request)
    {
        $data = $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required', 'numeric', 'digits:6', 'confirmed'],
            'password_confirmation' => ['required', 'digits:6', 'numeric'],
            'phone' => ['required', 'regex:/^7\d{9}$|^07\d{9}$/','numeric'],
        ],[],[
            'password' => 'Pin',
            'password_confirmation' => 'Confirm Pin',
        ]);

        $user = User::whereEmail($data['email'])->first();
        $validateUser = Helper::validateUser(['email' => $request->input('email'),'phone' => $request->input('phone')]);

        if(is_null($validateUser))
        {

            $user->password = Hash::make($request->input('password'));
            $user->phone = $request->input('phone');
            $user->save();

            $name = explode(' ',$user->name);
            $payload=[
                'notification_type'=>'user_registration',
                'payload'=> [
                    'first_name'=> $name[0] ?? $name[0] ,
                    'last_name'=> @$name[1] ? @$name[1] : 'lastname',
                    'email'=> $user->email,
                    'phone' => $request->input('phone'),
                    'password'=> $request->input('password')
                ]
            ];

            $url = (config('app.env') == 'production') ? 'https://dhigna.com/callback' : 'https://uat.dhigna.com/callback';
            $url = (config('app.env') == 'preprod') ? 'https://uat.dhigna.com/callback' : $url;

            $clientRepository =  app('Laravel\Passport\ClientRepository');
            $clientRepository->create($user->id,$user->name,$url);

            Helper::notifyThroughWebhook($payload);
           
        }else{

            $name = explode(' ',$user->name);
            $payload=[
                'notification_type'=>'update_password',
                'payload'=> [
                    'first_name'=> $name[0] ?? $name[0] ,
                    'last_name'=> @$name[1] ? @$name[1] : 'lastname',
                    'email'=> $user->email,
                    'phone' => $request->input('phone'),
                    'password'=> $request->input('password')
                ]
            ];

            Helper::notifyThroughWebhook($payload);
        }
     
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
     
                $user->save();
     
                event(new PasswordReset($user));
            }
        );

        // login
        if($status === Password::PASSWORD_RESET)
        {   
            $user = User::where(['email' => $request->email])->first();
            \Auth::loginUsingId($user->id, TRUE);
        }


        $msg = 'Pin '.__('eventmie-pro::em.reset').' '.__('eventmie-pro::em.successfully');
        return $status === Password::PASSWORD_RESET
                    ? redirect()->route('eventmie.events_index')->with('status', $msg)
                    : back()->withErrors(['email' => [__($status)]]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return PasswordBroker
     */
    public function broker()
    {
        // users is table name of users
        return Password::broker('users');
    }
}
