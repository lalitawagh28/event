<?php

namespace Classiebit\Eventmie\Http\Controllers\Auth;
use Facades\Classiebit\Eventmie\Eventmie;

use App\Http\Controllers\Controller;
use App\Http\Helper;
use App\Models\AuthWalletToken;
use App\Rules\PhoneDigits;
use App\Rules\PhoneUk;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Classiebit\Eventmie\Models\User;
use Classiebit\Eventmie\Notifications\MailNotification;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\Service\WalletPayService;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         // language change
        $this->middleware('common');
        $this->middleware('guest');
        $this->redirectTo = (config('app.env') == 'production') ? 'https://dhigna.com/user-portal' : 'https://uat.dhigna.com/user-portal';
        $this->service = new WalletPayService;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        \Session::put('url.intended',\URL::previous());

        return Eventmie::view('eventmie::auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        
        $this->validator($request->all())->validate();

        $data = [
            'name' => $request->get('first_name').' '.$request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' => $request->get('password'),
            'captcha' => $request->get('captcha'),
        ];

       
        $validateUser = Helper::validateUser(['email' => $request->get('email'),'phone' => $request->get('phone')]);

        if(is_null($validateUser))
        {
            event(new Registered($user = $this->create($data)));


            $this->guard()->login($user);

            // Send payload of user registration to hookdesk

            $payload=[
                'notification_type'=>'user_registration',
                'payload'=> [
                    'first_name'=> $request->get('first_name'),
                    'last_name'=> $request->get('last_name'),
                    'email'=> $request->get('email'),
                    'phone' => $request->get('phone'),
                    'password'=> $request->get('password'),
                    'captcha' => $request->get('captcha'),
                ]
            ];

            Helper::notifyThroughWebhook($payload);

            $clienturl = (config('app.env') == 'production') ? 'https://dhigna.com/callback' : 'https://uat.dhigna.com/callback';

            $clientRepository =  app('Laravel\Passport\ClientRepository');
            $clientRepository->create($user->id,$request->get('first_name').' '.$request->get('last_name'),$clienturl);

            $msg = 'Please check your Email to Verify the Email Address.';
            session()->flash('status', $msg);

            if ($response = $this->registered($request, $user)) {

                return $response;
            }

            // if(\Auth::user()->hasRole('customer'))
            // {
            //     $response = $this->service->setupAccessToken([
            //         'email' => $request->get ( 'email' ),
            //         'password' => $request->get ( 'password' )
            //     ]);

            //     AuthWalletToken::updateOrCreate(['user_id' => $user->id],[
            //         'user_id' => $user->id,
            //         'token' => @$response['token']
            //     ]);
            // }
            session(['password' => $request->get('password')]);
            $user = \Illuminate\Support\Facades\Auth::user();
            $clientDetails = \Illuminate\Support\Facades\DB::table('oauth_clients')->whereUserId($user?->getKey())->first();

            $url = (config('app.env') == 'production') ? 'https://dhigna.com/user-portal-detail/'.$clientDetails?->id.'/'.$clientDetails?->secret : 'https://uat.dhigna.com/user-portal-detail/'.$clientDetails?->id.'/'.$clientDetails?->secret;

            if(config('services.user_portal_redirection') == false)
            {
                return $request->wantsJson()
                        ? new JsonResponse([], 201)
                        : redirect('/');
            }
            return $request->wantsJson()
                        ? new JsonResponse([], 201)
                        : redirect($url);
        }else
        {

            return redirect()->back()->withErrors($validateUser['errors']);
        }

    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string','max:40','regex:/^[\pL\s]+$/u'],
            'last_name' => ['required', 'string', 'max:40','regex:/^[\pL\s]+$/u'],
            'email' => ['required', 'email', 'max:255', 'regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',function ($attribute, $value, $fail) {
                $count = User::where('email',$value)->count();
                if($count > 0 ){
                    $fail('Email  already registered.');
                }
            } ],
            'phone' => ['required', new PhoneUk,new PhoneDigits,'numeric'],
            'password' => ['required', 'numeric', 'digits:6', 'confirmed'],
            'password_confirmation' => ['required', 'digits:6', 'numeric'],
            'accept' => ['required'],
            'captcha' => 'required|captcha'
        ], [
            'first_name.regex' => 'The first name may only contain letters and spaces.',
            'last_name.regex' => 'The last name may only contain letters and spaces.',
            'captcha.captcha'=>'Invalid captcha code.',
            'phone.required'=> 'Movile Number is required',
            'phone.regex'=> 'Invalid Mobile Number',
            'password.required' => "Pin is required",
            'password.digits' => "Pin Must be 6 digits",
            'password.confirmed' => "Pin Confirmation does not match",
            'password_confirmation.required' => "Pin Confirmation is required",
            'password_confirmation.digits' => "Pin Confirmation must be 6 digits",
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $user   = User::create([
                    'name'      => $data['name'],
                    'email'     => $data['email'],
                    'phone'     => $data['phone'],
                    'password'  => Hash::make($data['password']),
                    'role_id'  => 2,
                    'captcha'  => $data['captcha']
                ]);

        // Send welcome email
        if(!empty($user->id))
        {
            // ====================== Notification ======================
            $mail['mail_subject']   = __('eventmie-pro::em.register_success');
            $mail['mail_message']   = __('eventmie-pro::em.get_tickets');
            $mail['action_title']   = __('eventmie-pro::em.login');
            $mail['action_url']     = eventmie_url();
            $mail['n_type']         = "user";

            // notification for
            $notification_ids       = [
                1, // admin
                $user->id, // new registered user
            ];

            $users = User::whereIn('id', $notification_ids)->get();
            $setting =  Voyager::model('Setting')->where('key' ,'multi-vendor.verify_email')->first();
            if($setting->value == 1)
            {
                if(checkMailCreds())
                {
                    try {
                        \Notification::locale(\App::getLocale())->send($users, new MailNotification($mail));
                    } catch (\Throwable $th) {}
                }
            }
            // ====================== Notification ======================
        }

        $this->redirectTo = \Session::get('url.intended');

        return $user;
    }



}
