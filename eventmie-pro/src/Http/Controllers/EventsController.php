<?php

namespace Classiebit\Eventmie\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\WalletPayController;
use App\Models\AuthWalletToken;
use App\Models\DeliveryCharge;
use App\Models\User;
use App\Service\WalletPayService;
use Carbon\Carbon;
use App\Service\YapilyService;
use Facades\Classiebit\Eventmie\Eventmie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Classiebit\Eventmie\Models\Event;
use Classiebit\Eventmie\Models\Ticket;
use Classiebit\Eventmie\Models\Category;
use Classiebit\Eventmie\Models\Country;
use Classiebit\Eventmie\Models\Schedule;
use Classiebit\Eventmie\Models\Tag;
use Classiebit\Eventmie\Models\Tax;
use Classiebit\Eventmie\Models\Booking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class EventsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // language change
        $this->middleware('common');

        $this->event    = new Event;

        $this->ticket   = new Ticket;

        $this->category = new Category;

        $this->delivery_charge = new DeliveryCharge();

        $this->country  = new Country;

        $this->schedule = new Schedule;

        $this->tag  = new Tag;

        $this->tax      = new Tax;

        $this->booking      = new Booking;

        $this->organiser_id = null;

        $this->service = new WalletPayService;

        $this->yapilyService = new YapilyService;
    }

    /* ==================  EVENT LISTING ===================== */

    /**
     * Show all events
     *
     * @return array
     */
    public function index($view = 'eventmie::events.index', $extra = [])
    {

        // get prifex from eventmie config
        $path = false;
        if (!empty(config('eventmie.route.prefix')))
            $path = config('eventmie.route.prefix');


        $events          = $this->event->events();

        $events_calendar = [];
        foreach($events as $key => $value)
        {
            if($value->start_date != $value->end_date){
                $event_date = Carbon::parse($value->start_date . " ".$value->start_time)->format('l, dS M Y h:i A') . ' to ' . Carbon::parse($value->end_date . " ".$value->end_time)->format('dS M Y h:i A');
            } else {
                $event_date = Carbon::parse($value->start_date . " ".$value->start_time)->format('l, dS M Y h:i A') . ' to ' . Carbon::parse($value->end_date . " ".$value->end_time)->format(' h:i A');
            }

            $calendarimg = '';
            if(!is_null($value->calendar_image))
            {
                $calendarimg = Storage::disk('azure')->temporaryUrl($value->calendar_image, now()->addMinutes(5));
            }


            $events_calendar[] = ['title' => $value->title,'date' => $value->start_date,'event_date'=> $event_date,'image' => '/storage/'.$value->thumbnail,'calendar_image' => $calendarimg];
        }

        foreach($events as $key => $event)
        {

            if(!is_null($event['calendar_image']) && !is_null(($event['listview'])))
            {
                $events[$key]['calendar_image'] = Storage::disk('azure')->temporaryUrl($event['calendar_image'], now()->addMinutes(5));
                $events[$key]['listview'] = Storage::disk('azure')->temporaryUrl($event['listview'], now()->addMinutes(5));

            }

        }


        return Eventmie::view($view, compact('path', 'extra' ,'events_calendar'));
    }

    /**
     * Show all events
     *
     * @return array
     */
    public function upcoming($view = 'eventmie::events.upcoming', $extra = [])
    {

        // get prifex from eventmie config
        $path = false;
        if (!empty(config('eventmie.route.prefix')))
            $path = config('eventmie.route.prefix');
        return Eventmie::view($view, compact('path', 'extra'));
    }

    // filters for get_events function
    protected function event_filters(Request $request)
    {
        $request->validate([
            'category'          => 'max:256|String|nullable',
            'search'            => 'max:256|String|nullable',
            'start_date'        => 'date_format:Y-m-d|nullable',
            'end_date'          => 'date_format:Y-m-d|nullable',
            'price'             => 'max:256|String|nullable',
            'city'              => 'max:256|String|nullable',
            'state'             => 'max:256|String|nullable',
            'country'           => 'max:256|String|nullable',

        ]);

        $category_id            = null;
        $category               = urldecode($request->category);
        $search                 = $request->search;
        $price                  = $request->price;
        $city                   = $request->city == 'All' ? '' : $request->city;
        $state                  = $request->state == 'All' ? '' : $request->state;
        $country_id             = null;
        $country                = urldecode($request->country);

        // search category id
        if (!empty($category)) {
            $categories  = $this->category->get_categories();

            foreach ($categories as $key => $value) {
                if ($value['name'] == $category)
                    $category_id = $value['id'];
            }
        }

        // search country id
        if (!empty($country)) {
            $countries = $this->country->get_countries();

            foreach ($countries as $key => $value) {
                if ($value['country_name'] == $country)
                    $country_id = $value['id'];
            }
        }

        $filters                    = [];
        $filters['category_id']     = $category_id;
        $filters['search']          = $search;
        $filters['price']           = $price;
        $filters['start_date']      = $request->start_date;
        $filters['end_date']        = $request->end_date;
        $filters['city']            = $city;
        $filters['state']           = $state;
        $filters['country_id']      = $country_id;



        return $filters;
    }

    // EVENT LISTING APIs
    // get all events
    public function events(Request $request)
    {


        $filters         = [];
        // call event fillter function
        $filters         = $this->event_filters($request);

        $events          = $this->event->events($filters);

        $event_ids       = [];
        $events_calendar = [];
        foreach($events as $key => $value)
            $event_ids[] = $value->id;

        // pass events ids
        // tickets
        $events_tickets     = $this->ticket->get_events_tickets($event_ids);
        $events_calendar= [];
        $events_data             = [];
        foreach ($events as $key => $value) {

            // online event - yes or no
            $value                  = $value->makeVisible('online_location');
            // check event is online or not
            $value->online_location    = (!empty($value->online_location)) ? 1 : 0;
            if(isset($value->venues[0])){
                $value->event_address   =   $value->venues[0]->city .','. $value->venues[0]?->state .','. $value->venues[0]?->country->country_name .','. $value->venues[0]?->zipcode ;    
            } else {
                $value->event_address   =  '' ;
            }
            $events_data[$key]             = $value;
            $tickets_data   = $this->get_tickets($value->id);
            $value->tickets        = $tickets_data['tickets'];
            $value->currency       = $tickets_data['currency'];
            $value->booked_tickets = $tickets_data['booked_tickets'];
            $value->total_capacity = $tickets_data['total_capacity'];
            if($value->start_date != $value->end_date){
                $event_date = Carbon::parse($value->start_date . " ".$value->start_time)->format('l, dS M Y h:i A') . ' to ' . Carbon::parse($value->end_date . " ".$value->end_time)->format('dS M Y h:i A');
            } else {
                $event_date = Carbon::parse($value->start_date . " ".$value->start_time)->format('l, dS M Y h:i A') . ' to ' . Carbon::parse($value->end_date . " ".$value->end_time)->format(' h:i A');
            }

            $events_calendar[] = ['title' => $value->title,'date' => $value->start_date,'event_date'=> $event_date,'image' => '/storage/'.$value->thumbnail,'calendar_image' => Storage::disk('azure')->temporaryUrl($value->calendar_image, now()->addMinutes(5))];
        //    foreach($events_tickets as $key1 => $value1)
        //     {
        //         // check relevant event_id with ticket id
        //         if($value->id == $value1['event_id'])
        //         {
        //             $events_data[$key]->tickets[]       = $value1;
        //         }
        //     }
        }

        // set pagination values
        $event_pagination = $events->jsonSerialize();

        // get all countries
        $data = $this->country->get_countries_having_events($filters['country_id']);

        $countries = $data['countries'];
        $states    = $data['states'];
        $cities    = $data['cities'];

        foreach($events_data as $key => $event)
        {

            if(!is_null($event['calendar_image']) && !is_null(($event['listview'])))
            {
                $events_data[$key]['calendar_image'] = Storage::disk('azure')->temporaryUrl($event['calendar_image'], now()->addMinutes(5));
                $events_data[$key]['listview'] = Storage::disk('azure')->temporaryUrl($event['listview'], now()->addMinutes(5));

            }

        }


        return response([
            'events' => [
                'currency' => setting('regional.currency_default'),
                'data' => $events_data,
                'total' => $event_pagination['total'],
                'per_page' => $event_pagination['per_page'],
                'current_page' => $event_pagination['current_page'],
                'last_page' => $event_pagination['last_page'],
                'from' => $event_pagination['from'],
                'to' => $event_pagination['to'],
                'countries' => $countries,
                'cities'    => $cities,
                'states'    => $states,
                'events_calendar' => $events_calendar,
            ],
        ], Response::HTTP_OK);
    }

    /**
     * Show single event
     *
     * @return array
     */
    public function show(Event $event, $view = 'eventmie::events.show', $extra = [])
    {


        $banks = [];
        //    dd(session()->get('user'));
        // it is calling from model because used subquery
        $upcomming_data  = $this->event->get_upcomming_events();


        $event_ids           = [];

        foreach($upcomming_data as $key => $value)
            $event_ids[] = $value->id;

        // pass events ids
        // tickets
        $events_tickets     = $this->ticket->get_events_tickets($event_ids);

        $upcomming_events             = [];
        foreach($upcomming_data as $key => $value)
        {
            // online event - yes or no
            $value                  = $value->makeVisible('online_location');
            // check event is online or not
            $value->online_location    = (!empty($value->online_location)) ? 1 : 0;

            $upcomming_events[$key]             = $value;

           foreach($events_tickets as $key1 => $value1)
            {
                // check relevant event_id with ticket id
                if($value->id == $value1['event_id'])
                {
                    $upcomming_events[$key]->tickets[]       = $value1;
                }
            }
        }
        $upcomming_events = collect($upcomming_events);

        foreach($upcomming_events as  $key => $upcomming_event){
            if($upcomming_event['thumbnail']){
                $upcomming_events[$key]['thumbnail'] = Storage::disk('azure')->temporaryUrl($upcomming_event['thumbnail'], now()->addMinutes(5));
            }
            
        }


        $event = $this->event->get_event($event->slug);
        $is_upcoming = $this->event->is_upcomming($event->slug);
        if (!$event->status || !$event->publish)
            abort('404');

        // online event - yes or no
        $event                  = $event->makeVisible('online_location');
        // check event is online or not
        $event['online_location']    = (!empty($event['online_location'])) ? 1 : 0;
        $category = null;
        // check if category is disabled
        // $category            = $this->category->get_event_category($event['category_id']);
        // if(empty($category))
        //     abort('404');

        $tags                = $this->tag->get_event_tags($event['id']);
        $max_ticket_qty      = (int) setting('booking.max_ticket_qty');
        $google_map_key      = setting('apps.google_map_key');

        // group by type
        $tag_groups          = [];
        if ($tags)
            $tag_groups          = collect($tags)->groupBy('type');

        // check free ticket
        $free_tickets        = $this->ticket->check_free_tickets($event['id']);

        // event country
        $country            = $this->country->get_event_country($event['country_id']);

        // check event and or not
        $ended  = false;

        // if event is repetitive then event will be expire according to end date
        if ($event['repetitive']) {
            if (\Carbon\Carbon::now()->format('Y/m/d') > \Carbon\Carbon::createFromFormat('Y-m-d', $event['end_date'])->format('Y/m/d'))
                $ended = true;
        } else {
            // none repetitive event so check start date for event is ended or not
            if (\Carbon\Carbon::now()->format('Y/m/d') > \Carbon\Carbon::createFromFormat('Y-m-d', $event['start_date'])->format('Y/m/d'))
                $ended = true;
        }

        $is_paypal = $this->is_paypal();

        // get tickets
        $tickets_data   = $this->get_tickets($event['id']);
        $tickets        = $tickets_data['tickets'];
        $currency       = $tickets_data['currency'];
        $booked_tickets = $tickets_data['booked_tickets'];
        $total_capacity = $tickets_data['total_capacity'];

        $user = session()->get('user');
        $walletToken = AuthWalletToken::whereUserId($user?->id)->first();

        if (is_null($walletToken) && !is_null($user)) {
            $response = $this->service->setupAccessToken([
                'email' =>  $user->email,
                'password' => session()->get('password')
            ]);

            if ($user->hasRole('customer')) {
                AuthWalletToken::updateOrCreate(['user_id' => $user->id], [
                    'user_id' => $user->id,
                    'token' => @$response['token']
                ]);
            }
        }

        if (!is_null(session()->get('user')) && !is_null($walletToken)) {
            $walletsInfo = $this->getWallet($this->service, session()->get('user'));
            $wallets = collect($walletsInfo['data']['wallets']);
        } else {
            $wallets = [];
        }
        if($event['poster']){
            $event['poster'] = Storage::disk('azure')->temporaryUrl($event['poster'], now()->addMinutes(5));
        }
        
        return Eventmie::view($view, compact('is_upcoming',
            'event', 'tag_groups', 'max_ticket_qty', 'free_tickets',
            'ended', 'category', 'country', 'google_map_key', 'is_paypal',
            'tickets', 'currency', 'booked_tickets', 'total_capacity', 'extra', 'upcomming_events', 'wallets'));
    }

    /**
     * Show single event
     *
     * @return array
     */
    public function checkout(Event $event, $view = 'eventmie::events.checkout', $extra = [])
    {

        $event = $this->event->get_event($event->slug);
        forEach($event->seatchart as $key=>$seat){
            $event->seatchart[$key]->ticket= $seat->ticket;
            $event->seatchart[$key]->subcatagory= $seat->subcatagory;
            if($seat->subcatagory) {
                $event->seatchart[$key]->available =  $seat->subcatagory->quantity -  $seat->subcatagory->bookings?->count();
            } else {
                $event->seatchart[$key]->available = $seat->ticket->quantity - $seat->ticket->attendees?->count();
            }
        }
        if(isset($event->venues[0])){
            $event->event_address = $event->venues[0]?->city .','. $event->venues[0]?->state .','. $event->venues[0]?->country->country_name .','. $event->venues[0]?->zipcode ;   
        } else {
            $event->event_address   =  '' ;
        }
        if(!$event->status || !$event->publish)
            abort('404');

        // online event - yes or no
        $event                  = $event->makeVisible('online_location');
        // check event is online or not
        $event['online_location']    = (!empty($event['online_location'])) ? 1 : 0;


        $max_ticket_qty      = (int) setting('booking.max_ticket_qty');

        $is_paypal = $this->is_paypal();

        // get tickets
        $tickets_data   = $this->get_tickets($event['id']);
        $tickets        = $tickets_data['tickets'];
        foreach ($tickets as $key => $ticket) {
            if ($ticket->sale_start_date != null &&  $ticket->sale_end_date != null && $ticket->sale_price != null) {
                if (Carbon::parse($ticket->sale_start_date) <= Carbon::today() &&  Carbon::parse($ticket->sale_end_date) > Carbon::today()) {
                    if ($ticket->sale_price > 0 && $ticket->sale_price < $ticket->price   && !$ticket->sub_category) {
                        $ticket->old_price = $ticket->price;
                        $ticket->price     = $ticket->sale_price;
                    }
                }
                $tickets[$key] = $ticket;
            }
        }
        $currency       = $tickets_data['currency'];
        $booked_tickets = $tickets_data['booked_tickets'];

        $user = session()->get('user');
        $walletToken = AuthWalletToken::whereUserId($user?->id)->first();
        if (!is_null(session()->get('user')) && !is_null($walletToken)) {
            $walletsInfo = $this->getWallet($this->service, session()->get('user'));
            $wallets = collect($walletsInfo['data']['wallets']);
        } else {
            $wallets = [];
        }
        // dd($event);
        $user_data = null;
        if(Auth::id()){
            $user_data = User::find(Auth::id());
            $user_data = $user_data->only(['id', 'name', 'phone', 'email','address']);
        }
        try{
            $bankLists = $this->yapilyService->getInstitutions();
        } catch(\Exception $e){
            $bankLists = ['data'=>[]];
        }
        
        $banks = collect($bankLists['data']) ?? [];
        if($event['checkout_banner']){
            $event['checkout_banner'] = Storage::disk('azure')->temporaryUrl($event['checkout_banner'], now()->addMinutes(5));
        }
        // dd($banks, collect($banks));
        return Eventmie::view($view, compact(
            'event',
            'max_ticket_qty',
            'is_paypal',
            'tickets',
            'currency',
            'booked_tickets',
            'extra',
            'wallets',
            'banks',
            'user_data'
        ));
    }
    public function getWallet($service, $user)
    {
        $wallets = $service->getWalletList($user);
        return $wallets;
    }
    /**
     *  Event tag detail by title
     *
     */

    public function tag($event_slug = null, $tag_title = null, $view = 'eventmie::tags.show', $extra = [])
    {
        $tag_title  = str_replace('-', ' ', strtolower(urldecode($tag_title)));
        $tag        = $this->tag->get_tag_by_title($tag_title);

        if (empty($tag))
            return error_redirect(__('eventmie-pro::em.tag') . ' ' . __('eventmie-pro::em.not_found'));

        return Eventmie::view($view, compact('tag', 'extra'));
    }


    // get all categories
    public function categories()
    {
        $categories  = $this->category->get_categories();

        if (empty($categories)) {
            return response()->json(['status' => false]);
        }
        return response()->json(['status' => true, 'categories' => $categories]);
    }

    // get all delivery charges
    public function delivery_charges()
    {
        $delivery_charges  = $this->delivery_charge->get_admin_delivery_charges();

        if (empty($delivery_charges)) {
            return response()->json(['status' => false]);
        }
        return response()->json(['status' => true, 'delivery_charges' => $delivery_charges]);
    }


    // check session
    public function check_session()
    {
        session(['verify' => 1]);

        return response()->json(['status' => true]);
    }


    // is_paypal

    protected function is_paypal()
    {
        // if have paypal keys then will show paypal payment option otherwise hide
        $is_paypal = 1;
        if (empty(setting('apps.paypal_secret')) || empty(setting('apps.paypal_client_id')))
            $is_paypal = 0;

        return $is_paypal;
    }

    // get tickets and it is public
    protected function get_tickets($event_id = null)
    {
        $params    = [
            'event_id' =>  (int) $event_id,
        ];
        $tickets     = $this->ticket->get_event_tickets($params);

        // apply admin tax
        $tickets     = $this->admin_tax($tickets);

        // get the bookings by ticket for live availability check
        $bookedTickets  = $this->booking->get_seat_availability_by_ticket($params['event_id']);
        // make a associative array by ticket_id-event_start_date
        // to reduce the loops on Checkout popup
        $booked_tickets = [];
        foreach ($bookedTickets as $key => $val) {
            // calculate total_vacant each ticket
            $ticket         = $tickets->where('id', $val->ticket_id)->first();

            // Skip if ticket not found or deleted
            if (!$ticket)
                continue;

            $booked_tickets["$val->ticket_id-$val->event_start_date"] = $val;

            // min 0 or else it'll throw JS error
            $total_vacant   = $ticket->quantity - $val->total_booked;
            $total_vacant   = $total_vacant < 0 ? 0 : $total_vacant;
            $booked_tickets["$val->ticket_id-$val->event_start_date"]->total_vacant = $total_vacant;

            // unset if total_vacant > global max_ticket_qty
            // in case of high values, it throw JS error
            $max_ticket_qty = (int) setting('booking.max_ticket_qty');
            if ($total_vacant > $max_ticket_qty)
                unset($booked_tickets["$val->ticket_id-$val->event_start_date"]);
        }

        // sum all ticket's capacity
        $total_capacity = 0;
        foreach ($tickets as $val)
            $total_capacity += $val->quantity;

        return [
            'tickets' => $tickets,
            'currency' => setting('regional.currency_default'),
            'booked_tickets' => $booked_tickets,
            'total_capacity' => $total_capacity,
        ];
    }

    /**
     *  admin tax apply on all tickets
     */
    protected function admin_tax($tickets = [])
    {
        // get admin taxes
        $admin_tax  = $this->tax->get_admin_taxes();

        // if admin taxes are not existed then return
        if ($admin_tax->isEmpty())
            return $tickets;

        // it work when tickets show for purchasing
        // for multiple tickets
        if ($tickets instanceof \Illuminate\Database\Eloquent\Collection) {
            // push admin taxes in every tickets
            foreach ($tickets as $key => $value) {
                foreach ($admin_tax as $ad_k => $ad_v) {
                    $value->taxes->push($ad_v);
                }
            }
        } else {
            // it work when booking data prepare
            // for single ticket
            foreach ($admin_tax as $ad_k => $ad_v) {
                $tickets['taxes'] = $tickets['taxes']->push($ad_v);
            }
        }

        return $tickets;
    }
}
