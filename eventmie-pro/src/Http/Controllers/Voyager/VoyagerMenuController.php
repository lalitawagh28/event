<?php

namespace Classiebit\Eventmie\Http\Controllers\Voyager;

use App\Http\Helper;
use Facades\Classiebit\Eventmie\Eventmie;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerMenuController as BaseVoyagerMenuController;

class VoyagerMenuController extends BaseVoyagerMenuController
{
    public function builder($id)
    {
        $menu = Voyager::model('Menu')->findOrFail($id);

        $this->authorize('edit', $menu);

        $isModelTranslatable = is_bread_translatable(Voyager::model('MenuItem'));

        $view = 'eventmie::vendor.voyager.menus.builder';
        return Eventmie::view($view, compact(
            'menu',
            'isModelTranslatable'
        ));
    }

    public function add_item(Request $request)
    {
        $menu = Voyager::model('Menu');

        $menuItem = Voyager::model('MenuItem');

        $this->authorize('add', $menu);

        $data = $this->prepareParameters(
            $request->all()
        );

        unset($data['id']);
        $data['order'] = $menuItem->highestOrderMenuItem();

        // Check if is translatable
        $_isTranslatable = is_bread_translatable(Voyager::model('MenuItem'));
        if ($_isTranslatable) {
            // Prepare data before saving the menu
            $trans = $this->prepareMenuTranslations($data);
        }

        $menuItem = Voyager::model('MenuItem')->create($data);
        // $webhookResponse = $this->hitWebhook($data, $menuItem);

        // if (!is_null($webhookResponse)) {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        // Save menu translations
        if ($_isTranslatable) {
            $menuItem->setAttributeTranslations('title', $trans, true);
        }

        return redirect()
            ->route('voyager.menus.builder', [$data['menu_id']])
            ->with([
                'message'    => __('voyager::menu_builder.successfully_created'),
                'alert-type' => 'success',
            ]);
    }

    public function update_item(Request $request)
    {
        $id = $request->input('id');
        $data = $this->prepareParameters(
            $request->except(['id'])
        );

        $menuItem = Voyager::model('MenuItem')->findOrFail($id);

        $this->authorize('edit', $menuItem->menu);

        if (is_bread_translatable($menuItem)) {
            $trans = $this->prepareMenuTranslations($data);

            // Save menu translations
            $menuItem->setAttributeTranslations('title', $trans, true);
        }

        $menuItem->update($data);

        $info = $request->all();
        $info['updatedId'] = $id;

        // $webhookResponse = $this->hitWebhook($info, $menuItem);

        // if (!is_null($webhookResponse)) {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        return redirect()
            ->route('voyager.menus.builder', [$menuItem->menu_id])
            ->with([
                'message'    => __('voyager::menu_builder.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }

    public function delete_menu($menu, $id)
    {
        $item = Voyager::model('MenuItem')->findOrFail($id);

        $menuItem = Voyager::model('MenuItem');

        $this->authorize('delete', $item);

        $item->deleteAttributeTranslation('title');

        $item->destroy($id);

        $info['deletedId'] = $id;
        $info['menu_id'] = $menu;

        // $webhookResponse = $this->hitWebhook($info, $menuItem);
        // if (!is_null($webhookResponse)) {
        //     Helper::notifyThroughWebhook($webhookResponse);
        // }

        return redirect()
            ->route('voyager.menus.builder', [$menu])
            ->with([
                'message'    => __('voyager::menu_builder.successfully_deleted'),
                'alert-type' => 'success',
            ]);
    }

    public function hitWebhook($data, $model)
    {
        $response = $model->setWebhookParameters($data);
        return $response;
    }
}
