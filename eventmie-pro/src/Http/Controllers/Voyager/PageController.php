<?php

namespace Classiebit\Eventmie\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use File;
use Classiebit\Eventmie\Http\Controllers\Voyager\VoyagerBaseController;



class PageController extends VoyagerBaseController
{

    public function __construct()
    {

        // language change
        $this->middleware('common');
    }

    // add tags
    public function store(Request $request)
    {
        
        
        $path = "/pages/".Carbon::now()->format('FY').'/';
        $request->validate([
            'title'        => 'required|max:255|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'excerpt'        => 'nullable',
            'body'           => 'required',
            'slug'           => 'required|max:300|regex:/^[a-zA-Z0-9_\-]*$/',
            'meta_description' => 'required|max:500|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'meta_keywords' => 'required|max:500|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'status'        => 'required|in:INACTIVE,ACTIVE',
        ]);
        
        // for image
        if($request->hasfile('image'))
        {
            // image type validation
            $request->validate([
                'image'        => 'mimes:jpeg,png,jpg',
            ]);

            $file      = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting poster extension
            $image     = time().rand(1,988).'.'.$extension;
            // $file->storeAs('public/'.$path, $image);
            $image_resize    = Image::make($file)->resize(512,512);

            // if directory not exist then create directiory
            if (! File::exists(storage_path('/app/public/').$path)) {
                File::makeDirectory(storage_path('/app/public/').$path, 0777, true);
            }

            $image_resize->save(storage_path('/app/public/'.$path.$image));
            $image     = $path.$image;
        }

        // image is required
        if(empty($image))
        {
            // if have  image and database have images no images this event then apply this rule
            $request->validate([
                'image'        => 'required|mimes:jpeg,png,jpg',
            ]);
        }
        return parent::store($request);
    }

    public function update(Request $request, $id)
    {
        
        $path = "/pages/".Carbon::now()->format('FY').'/';
        $request->validate([
            'title'        => 'required|max:255|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'excerpt'        => 'nullable',
            'body'           => 'required',
            'slug'           => 'required|max:300|regex:/^[a-zA-Z0-9_\-]*$/',
            'meta_description' => 'required|max:500|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'meta_keywords' => 'required|max:500|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'status'        => 'required|in:INACTIVE,ACTIVE',
        ]);
        // for image
        if($request->hasfile('image'))
        {
            // image type validation
            $request->validate([
                'image'        => 'mimes:jpeg,png,jpg',
            ]);

            $file      = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting poster extension
            $image     = time().rand(1,988).'.'.$extension;
            // $file->storeAs('public/'.$path, $image);
            $image_resize    = Image::make($file)->resize(512,512);

            // if directory not exist then create directiory
            if (! File::exists(storage_path('/app/public/').$path)) {
                File::makeDirectory(storage_path('/app/public/').$path, 0777, true);
            }

            $image_resize->save(storage_path('/app/public/'.$path.$image));
            $image     = $path.$image;
        }

        return parent::update($request, $id);
    }

}
