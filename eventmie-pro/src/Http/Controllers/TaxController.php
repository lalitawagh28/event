<?php

namespace Classiebit\Eventmie\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use File;
use Classiebit\Eventmie\Http\Controllers\Voyager\VoyagerBaseController;



class TaxController extends VoyagerBaseController
{

    public function __construct()
    {

        // language change
        $this->middleware('common');
    }

    // add tags
    public function store(Request $request)
    {
        
        
        $request->validate([
            'title'        => 'required|max:40|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'rate'        => 'required|numeric|gt:0',
        ]);
        
        
        return parent::store($request);
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'title'        => 'required|max:40|regex:/^[a-zA-Z0-9_\-\s]*$/',
            'rate'        => 'required|numeric|gt:0',
        ]);
        
        
        return parent::update($request, $id);
    }

}
