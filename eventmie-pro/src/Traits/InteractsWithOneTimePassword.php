<?php

namespace Classiebit\Eventmie\Traits;

use App\Models\OneTimePassword;
use Exception;

trait InteractsWithOneTimePassword
{
    public function oneTimePasswords()
    {
        return $this->morphMany(OneTimePassword::class, 'holder')->latest();
    }

    public function generateOtp(string $type)
    {
        return OneTimePassword::create([
            'holder_id' => $this->id,
            'holder_type' => $this->getMorphClass(),
            'code' => rand(100000, 999999),
            'type' => $type,
            'expires_at' => now()->addMinutes(OneTimePassword::getExpiringDuration()),
        ]);
    }

    public function scopeVerified($query)
    {
        return $query->whereHas('oneTimePasswords', fn ($subQuery) => $subQuery->whereNotNull('verified_at'));
    }

    public function scopeVerifiedOn($query, string $type)
    {
        return $query->verified()->whereType($type);
    }

    public function hasActiveOneTimePassword($type)
    {

        return $this->oneTimePasswords()->whereType($type)->where('expires_at', '>', now())->whereNull('verified_at')->exists();
    }

    public function redirectForVerification(string $callbackUrl, string $type)
    {
        
        if (! $this->hasActiveOneTimePassword($type)) {
            throw new Exception('No active one-time password exists.');
        }

        $params['oneTimePassword'] = $this->oneTimePasswords()->first()->id;
        $params['callback_url'] = $callbackUrl;

        return redirect()->route('dashboard.one-time-passwords.show', $params);
    }
}
