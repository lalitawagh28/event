<?php

namespace Classiebit\Eventmie\Models;

use App\Models\Event;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    // get categories
    public function get_categories()
    {
        $category = Category::select('*')->selectRaw('(select count(*)   from event_categories where event_categories.category_id = categories.id) as category_count')->where(['status' => 1])->get();

        return to_array($category);
    }

    // get event category
    public function get_event_category($id = null)
    {
        $category = Category::where(['id' => $id, 'status' => 1])->first();

        return to_array($category);
    }

    public function setWebhookParameters($data)
    {
        if (!is_null(@$data['updatedId'])) {
            $info['notification_type'] = 'category_update';
            $info['payload'] = $data;
        } elseif (!is_null(@$data)) {
            $info['notification_type'] = 'category_create';
            $info['payload'] = $data;
        } elseif (!is_null(@$data['deletedId'])) {
            $info['notification_type'] = 'category_delete';
            $info['payload'] = $data['deletedId'];
        } elseif (!is_null(@$data['order'])) {
            $info['notification_type'] = 'category_order_update';
            $info['payload'] = $data;
        }
        return $info;
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_categories', 'event_id', 'category_id');
    }

    public function eventCategory()
    {
        return $this->hasMany(EventCategory::class, 'category_id', 'id');
    }
}
