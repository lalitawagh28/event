<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends \TCG\Voyager\Models\Page
{
    protected $translatable = [];

    public function setWebhookParameters($data)
    {
        if (is_null(@$data['deletedId']) && is_null(@$data['updatedOrderId'])) {
            $info = [
                'payload' => [
                    'title' => $data['title'],
                    'excerpt' => $data['excerpt'],
                    'body' => $data['body'],
                    'image' => $data['image'],
                    'slug' => $data['slug'],
                    'status' => $data['status']
                ]
            ];

            if (!is_null(@$data['updatedId'])) {
                $info['updatedId'] = $data['updatedId'];
                $info['notification-type'] = 'page-update';
            } else {
                $info['notification-type'] = 'page-add';
            }
        } elseif (!is_null(@$data['updatedOrderId'])) {
            $info['updatedOrderId'] = $data['updatedOrderId'];
            $info['notification-type'] = 'pages-order-update';
        } else {
            $info['deletedId'] = $data['deletedId'];
            $info['notification-type'] = count($data['deletedId']) > 1 ? 'pages-bulk-delete' : 'pages-delete';
        }
        return $info;
    }
}
