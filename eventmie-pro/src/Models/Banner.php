<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $guarded = [];
    public function get_banners()
    {
        return Banner::where(['status' => 1])->orderBy('order', 'asc')->get();
    }

    public function setWebhookParameters($data)
    {
        if (is_null(@$data['deletedId']) && is_null(@$data['updatedOrderId'])) {
            $info = [
                'payload' => [
                    'title' => $data['title'],
                    'subtitle' => $data['subtitle'],
                    'image' => $data['image'],
                    'button_url' => $data['button_url'],
                    'button_title' => $data['button_title'],
                    'order' => $data['order'],
                    'status' => $data['status']
                ]
            ];
            if (!is_null(@$data['updatedId'])) {
                $info['updatedId'] = $data['updatedId'];
                $info['notification-type'] = 'banner-update';
            } else {
                $info['notification-type'] = 'banner-add';
            }
        } elseif (!is_null(@$data['updatedOrderId'])) {
            $info['updatedOrderId'] = $data['updatedOrderId'];
            $info['notification-type'] = 'banner-order-update';
        } else {
            $info['deletedId'] = $data['deletedId'];
            $info['notification-type'] = count($data['deletedId']) > 1 ? 'banners-bulk-delete' : 'banners-delete';
        }
        return $info;
    }
}
