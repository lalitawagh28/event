<?php

namespace Classiebit\Eventmie\Models;

use App\Http\Helper;
use App\Models\AgentOrganizer;
use App\Models\AgentTicket;
use App\Models\RegisterCountry;
use App\Models\UserLog;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Sanctum\HasApiTokens;
use Classiebit\Eventmie\Notifications\ForgotPasswordNotification;
use Classiebit\Eventmie\Models\Tag;
use Classiebit\Eventmie\Models\Venue;
use Classiebit\Eventmie\Traits\InteractsWithOneTimePassword;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Models\Role;

class User extends \TCG\Voyager\Models\User  implements MustVerifyEmail
{
    use HasApiTokens, Notifiable,InteractsWithOneTimePassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function preferredLocale()
    {
        return \App::getLocale();
    }

    // get customer when customer create booking or organiser create booking for customer
    public function get_customer($params = [])
    {
        return User::
            where([
                'id' => $params['customer_id'], 
            ])   
            ->first();   
    }

    // get user
    public function get_user($params = [])
    {
        return User::where($params)->first();   
    }

    // get organisers
    public function get_organisers()
    {
        return User::where(['role_id' => 3])->get();   
    }

    // total customers
    public function total_customers($user_id = null)
    {
        
        if(!empty($user_id))
        {
            return Booking::distinct('customer_id')->where(['organiser_id' => $user_id])->pluck('customer_id')->count();

        }

        return User::where(['role_id' => 2])->count();
    }

    // total customers
    public function total_customers_agent($user_id = null)
    {
        
        if(!empty($user_id))
        {
            $agent_ids =  AgentTicket::where('agent_id',$user_id)->pluck('id')->toArray();
            return Booking::distinct('customer_id')->whereIn('agent_id' , $agent_ids)->pluck('customer_id')->count();

        }

        return User::where(['role_id' => 2])->count();
    }
    
    // total organizers
    public function total_organizers($user_id = null)
    {
        if(!empty($user_id))
        {
            return User::where(['id' => $user_id])->count();

        }

        return User::where(['role_id' => 3])->count();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        // ====================== Notification ====================== 
        //forgot password notification
        try {
            $this->notify(new ForgotPasswordNotification($token));
        } catch (\Throwable $th) {}
        // ====================== Notification ====================== 
    }

    // get all tags for particular organiser 
    public function get_tags($params = [])
    {   
        $user = User::find($params['organizer_id']);
        return $user->tags()->orderBy('updated_at', 'DESC')->paginate(10);
        
    }
    
    // get all tags for particular organiser 
    public function get_venues($params = [])
    {   
        $user = User::find($params['organizer_id']);
        return $user->venues()->orderBy('updated_at', 'DESC')->paginate(10);
        
    }

    /**
     *  the tags belong to organiser means users
     */

    public function tags()
    {
        return $this->hasMany(Tag::class, 'organizer_id');
    }
    
    public function venues()
    {
        return $this->hasMany(Venue::class, 'organizer_id');
    }

    /**
     *  total user count
     */

    public function total_users()
    {
        return User::count();
    }

    /**
     * search tags
     */
    public function search_tags($params = [])
    {
        return User::with(['tags' => function ($query) use($params) {

                if(!empty($params['search']))
                {
                    $query->where(function($query) use($params) {

                        $query->orWhere('title','LIKE',"%{$params['search']}%");
                         
                    });
                }

                $query->limit(10);
               
            }])->where(['id' => $params['organizer_id'] ])->first()->tags;
       
    }

    /**
     * search venues
     */
    public function search_venues($params = [])
    {
        return User::with(['venues' => function ($query) use($params) {

                if(!empty($params['search']))
                {
                    $query->where(function($query) use($params) {

                        $query->orWhere('title','LIKE',"%{$params['search']}%");
                         
                    });
                }

                $query->inRandomOrder()->limit(10);
               
            }])->where(['id' => $params['organizer_id'] ])->first()->venues;
       
    }

    public function setWebhookUserParameters($data)
    {

        if(!is_null(@$data['create_user']))
        {   
            $info['notification_type'] = $data['action'];
            $info['payload'] = $data['create_user'];
        }elseif(!is_null(@$data['update_user'])){
            $info['notification_type'] = $data['action'];
            $info['payload'] = $data['update_user'];
        }elseif(!is_null(@$data['delete_users'])){
            $info['notification_type'] = $data['action'];
            $info['payload'] = $data['delete_users'];
        }

        return $info;
    }

    public static function getRegistrationStep($role_id)
    {
        $user = User::find(Auth::id());
        
        $user_log =  UserLog::where(['holder_id' => Auth::user()->getKey(), 'holder_type' => $user->getMorphClass()])->orderBy('id', 'desc')->first();
        if( $role_id == 3){
            if(!$user_log) {
                return "admin.register.personal_detail";
            } elseif($user_log->value == 'personal_details'  && config('otp.mobile_otp_enable')) {
                return "admin.register.mobileVerification";
            } elseif($user_log->value == 'mobile_otp_verification' && config('otp.email_otp_enable')) {
                return "admin.register.emailVerification";
            } 
            return "voyager.organizer.dashboard";

        } elseif( $role_id == 8){
            if(!$user_log) {
                return "agent.register.personal_detail";
            } elseif($user_log->value == 'personal_details'  && config('otp.mobile_otp_enable')) {
                return "admin.register.mobileVerification";
            } elseif($user_log->value == 'mobile_otp_verification' && config('otp.email_otp_enable')) {
                return "admin.register.emailVerification";
            } 
            return "voyager.agents.dashboard";
        }
    }

    public function title()
    {
        return $this->hasOne(Title::class, 'id', 'title_id');
    }

    public function logRegistrationStep($step)
    {
        return UserLog::updateOrCreate([
            'holder_type' => $this->getMorphClass(),
            'holder_id'   => $this->getKey(),
            'value'       => $step,
        ], [
            'holder_type' => $this->getMorphClass(),
            'holder_id'   => $this->getKey(),
            'key'         => 'registration_step',
            'value'       => $step,
        ]);
    }


    public function getNextRegistrationRoute($currentStep)
    {
        //$currentStep = User::getRegistrationStep()?->value;

        if($currentStep == 'personal_details') {
            $nextStep = "admin.register.mobileVerification";
        } elseif($currentStep == 'mobile_otp_verification') {
            $nextStep = "admin.register.emailVerification";
        }
        return $nextStep;
    }
    public function register_country() {
        return $this->hasOne(RegisterCountry::class,'id','country_id');
    }

    public function getPhoneWithCountryCode(): string
    {
            if($this->country_id){
                return '+' . $this->register_country->phone   . Helper::normalizePhone($this->phone);
            } else {
                $country = RegisterCountry::find(config('otp.default_country_code'));
                return '+' . $country->phone   . Helper::normalizePhone($this->phone);
            }
    }

    public function routeNotificationForTwilio()
    {
        return $this->getPhoneWithCountryCode();
    }

    public function routeNotificationForMail($notification)
    {

        return $this->email;
    }
    public function getRoleId($role) {
        return Role::where('name',$role)->first()->id;
    }

    public function markEmailAsNotVerified()
    {
        return $this->forceFill([
            'email_verified_at' => null,
        ])->save();
    }
    public function markMobileAsNotVerified()
    {
        return $this->forceFill([
            'phone_verified_at' => null,
        ])->save();
    }
}
