<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;


class Contact extends Model
{
    protected $guarded = [];

    // save contact details
    public function store_contact($data = [])
    {
        return Contact::create($data);
    }

    public function setWebhookParameters($data)
    {
        $info = [
            'payload' => [
                'deletedId' => $data['deletedId'],
            ],
            'notification-type' => 'contact-delete',
        ];

        return $info;
    }

}
