<?php

namespace Classiebit\Eventmie\Models;

use App\Models\Agent;
use App\Models\AgentTicket;
use App\Models\Order;
use App\Models\TicketSubCategory;
use Illuminate\Database\Eloquent\Model;
use DB;

use Classiebit\Eventmie\Models\User;
use Classiebit\Eventmie\Models\Transaction;
use Classiebit\Eventmie\Models\Commission;

use Classiebit\Eventmie\Scopes\BulkScope;
use Illuminate\Database\Eloquent\Builder;
use Classiebit\Eventmie\Models\Event;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Booking extends Model
{
    protected $guarded = [];

    protected $casts = [

        'meta' => 'array',
    ];
    /**
     * Table used
    */
    private $tb                 = 'bookings';
    private $tb_tickets         = 'tickets'; 

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        
        if(\Request::route()?->getName() != 'voyager.bookings.bulk_bookings')
        {
            static::addGlobalScope(new BulkScope);
        }
        
        if(\Request::route()?->getName() == 'voyager.bookings.bulk_bookings')
        {
            static::addGlobalScope('bulk_scope', function (Builder $builder) {
                $builder->where(['is_bulk' => 1]);
            });
        }
        
    }
    
    // make booking
    public function make_booking($params = [])
    {
        return Booking::create($params);
    }    
    
    public function get_my_bookings_byorder($params = [])
    {   
        return Booking::select('bookings.*','transactions.payment_gateway','E.time_zone','E.slug as event_slug','E.excerpt as event_excerpt','E.venue as event_venue')
                ->from('bookings')
                ->selectRaw("if(tsub.title is not null, CONCAT('(',tsub.title,')'),'')  as sub_category_title")
                // ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
                // ->selectRaw("(SELECT E.excerpt FROM events E WHERE E.id = bookings.event_id) event_excerpt")
                // ->selectRaw("(SELECT E.venue FROM events E WHERE E.id = bookings.event_id) event_venue")
                ->selectRaw("(SELECT E.online_location FROM events E WHERE E.id = bookings.event_id AND bookings.is_paid = 1  AND bookings.status = 1) online_location")
                ->leftJoin('ticket_sub_categories as tsub', 'tsub.id', '=', 'bookings.sub_category_id')
                ->leftJoin('transactions', function ($join) {
                    $join->on('transactions.id', '=', 'bookings.transaction_id');
                })
                ->join('order_bookings', function($join) use($params) {
                    $join->on('bookings.id', '=', 'order_bookings.booking_id');
                    $join->on('order_bookings.order_id', '=',DB::raw("'".$params['order_id']."'") );
                })
                ->leftJoin('events as E', 'E.id', '=', 'bookings.event_id')
                ->where(['customer_id' => $params['customer_id'] ])
                ->orderBy('id', 'desc')
                ->paginate(10);
    }

    // get booking for customer
    public function get_my_bookings($params = [])
    {   
        return Booking::select('bookings.*')
                ->from('bookings')
                ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
                ->selectRaw("(SELECT E.excerpt FROM events E WHERE E.id = bookings.event_id) event_excerpt")
                ->selectRaw("(SELECT E.venue FROM events E WHERE E.id = bookings.event_id) event_venue")
                ->selectRaw("(SELECT E.online_location FROM events E WHERE E.id = bookings.event_id AND bookings.is_paid = 1  AND bookings.status = 1) online_location")
                ->where(['customer_id' => $params['customer_id'] ])
                ->orderBy('id', 'desc')
                ->paginate(10);
    }

    
    

    // check booking id for cancellation
    public function check_booking($params = [])
    {
        return Booking::
            where([
                'status'        => 1, 
                'customer_id'   => $params['customer_id'], 
                'id'            => $params['booking_id'], 
                'ticket_id'     => $params['ticket_id'], 
                'event_id'      => $params['event_id'] ])
            ->first();   
    }

    // booking_cancel for customer
    public function booking_cancel($params = [])
    {
        return Booking::
                where([
                    'status'        => 1, 
                    'checked_in'    => 0, 
                    'customer_id'   => $params['customer_id'], 
                    'id'            => $params['booking_id'], 
                    'ticket_id'     => $params['ticket_id'], 
                    'event_id'      => $params['event_id'] ])
                ->update(['booking_cancel' => 1 ]);
    }
    public function get_organiser_bookings_byorder($params = []){
        $query = Booking::query();
        $query->select('bookings.*', 'CM.customer_paid','E.time_zone','E.slug as event_slug')
            ->from('bookings')
            ->selectRaw("if(tsub.title is not null, CONCAT('(',tsub.title,')'),'')  as sub_category_title")
            // ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
            // ->selectRaw("(SELECT E.time_zone FROM events E WHERE E.id = bookings.event_id) time_zone")
            ->selectRaw("(SELECT E.online_location FROM events E WHERE E.id = bookings.event_id AND bookings.is_paid = 1  AND bookings.status = 1) online_location")
            ->leftJoin('commissions as CM', 'CM.booking_id', '=', 'bookings.id')
            ->leftJoin('events as E', 'E.id', '=', 'bookings.event_id')
            ->leftJoin('ticket_sub_categories as tsub', 'tsub.id', '=', 'bookings.sub_category_id')
            ->join('order_bookings', function($join) use($params) {
                $join->on('bookings.id', '=', 'order_bookings.booking_id');
                $join->on('order_bookings.order_id', '=',DB::raw("'".$params['order_id']."'") );
            });
        return  $query->where([ 'bookings.organiser_id' => $params['organiser_id'] ])
            ->orderBy('id', 'desc')
            ->paginate(10);
    }
    /**
     * ================Organiser Booking Start=========================================
     */

     // get booking for organiser
    public function get_organiser_bookings($params = [])
    {
        $query = Booking::query();
        
        $query->select('bookings.*', 'CM.customer_paid')
            ->from('bookings')
            ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
            ->selectRaw("(SELECT E.online_location FROM events E WHERE E.id = bookings.event_id AND bookings.is_paid = 1  AND bookings.status = 1) online_location")
            ->leftJoin('commissions as CM', 'CM.booking_id', '=', 'bookings.id');
            
            // in case of searching by between two dates
            if(!empty($params['start_date']) && !empty($params['end_date']))
            {
                $query ->whereDate('bookings.created_at', '>=' , $params['start_date']);
                $query ->whereDate('bookings.created_at', '<=' , $params['end_date']);
            }
            
            // in case of searching by start_date
            if(!empty($params['start_date']) && empty($params['end_date']))
                $query ->whereDate('bookings.created_at', $params['start_date']);

            // in case of searching by event_id
            if($params['event_id'] > 0)
                $query->where(['bookings.event_id' => $params['event_id']]);

            
        return  $query->where([ 'bookings.organiser_id' => $params['organiser_id'] ])
                ->orderBy('id', 'desc')
                ->paginate(10);
    }
    
    // check booking id for cancellation for organiser
    public function organiser_check_booking($params = [])
    {
        return Booking::where($params)->first();   
    }

    // booking_edit for customer by organiser
    public function organiser_edit_booking($data = [], $params = [])
    {
        return Booking::where($params)->update($data);
    }

    // organiser view booking of customer
    public function organiser_view_booking($params = [])
    {
        return Booking::select('bookings.*')->from('bookings')
            ->where($params)
            ->first();  
    }

    // only admin can delete booking
    public function delete_booking($params = [])
    {
        // delete commission after deleting booking
        DB::transaction(function () use ($params) {
            DB::table($this->tb)->where($params)->delete();
            DB::table("commissions")->where(['booking_id'=>$params['id']])->delete();
        });

        return true;
    }

    // only admin and organiser can get particular event's booking
    public function get_event_bookings($params = [], $select = ['*'])
    {
        $booking = Booking::select($select)->selectRaw("if(tsub.title is not null, CONCAT(ticket_title,' (',tsub.title,')'),ticket_title)  as ticket_category")->where($params)->leftJoin('ticket_sub_categories as tsub', 'tsub.id', '=', 'bookings.sub_category_id')->get();

        return to_array($booking);
    }

    
    // only admin and organiser can get particular event's booking
    public function get_event_bookings_agent($params = [], $select = ['*'])
    {
        $agentIds = \App\Models\AgentTicket::where('agent_id',Auth::id())->pluck('id')->toArray();
        $booking = Booking::select($select)->selectRaw("if(tsub.title is not null, CONCAT(ticket_title,' (',tsub.title,')'),ticket_title)  as ticket_category")->where($params)->whereIn('agent_id',$agentIds)->leftJoin('ticket_sub_categories as tsub', 'tsub.id', '=', 'bookings.sub_category_id')->get();

        return to_array($booking);
    }
    /**
     *  total booking coutn
     */

    public function total_bookings($user_id = null)
    {
        if(!empty($user_id))
        {
            return Booking::where(['organiser_id' => $user_id])->count();
        }
        return Booking::count();
    }
    
    /**
     *  total booking coutn
     */

     public function total_bookings_agent($user_id = null)
     {
         if(!empty($user_id))
         {
            $agent = Agent::where('id',$user_id)->first();
            $ids = [];
            $ids = $agent->agent_tickets()->pluck('id')->toArray();
             return Booking::whereIn('agent_id' , $ids)->count();
         }
         return Booking::count();
     }

    /**
     *  total revenue count
     */

    public function total_revenue($user_id = null)
    {
        if(!empty($user_id))
        {
            return Booking::where(['organiser_id' => $user_id])->sum('net_price');
        }
        return Booking::sum('net_price');
    } 
    
    
     /**
     *  total revenue count
     */

     public function total_revenue_agent($user_id = null)
     {
        $commission = 0;
         if(!empty($user_id))
         {   
            Booking::where(['agent_id' => $user_id])->each(function($item, $key) use(&$commission,&$user_id) {
                $commission += $item->commission_earned($user_id);
            });
         }
         return $commission;
     }

    public function commission_earned($user_id) {
        if(Str::substr(Str::upper($this->agent_ticket?->code_value),1,1) == 'F'){
            return  $this->agent_ticket?->commission;
        } else {
            return  $this->price*($this->agent_ticket?->commission/100);
        }   
        
        

    }

    // update payment_type only when upgrading to v1.3.x
    public static function update_payment_type()
    {
        $bookings = Booking::get();
        if($bookings->isNotEmpty())
        {
            foreach($bookings as $key => $value)
            {   
                // offline
                if($value->transaction_id == 0 && $value->net_price > 0)
                {
                    Booking::where(['id' => $value->id])->update(['payment_type' => 'offline']);
                }
            }
        }    
    }
    
    // sum booked ticket quantity each booking date + each ticket id
    public function get_seat_availability_by_ticket($event_id = null)
    {
        return DB::table($this->tb)
                ->select('event_start_date', 'ticket_id')
                ->selectRaw("SUM(quantity) as total_booked")
                ->where("event_id", $event_id)
                ->where("status", 1)
                ->groupBy("event_start_date", "ticket_id")
                ->orderBy('ticket_id')
                ->get();
    }


    /* Get event tickets */
    public function event()
    {
        return $this->hasOne(Event::class,'id','event_id');
    }

    public function ticket()
    {
        return $this->hasOne(Ticket::class,'id','ticket_id');
    }

    public function order(){
        return $this->belongsTo(Order::class,'order_bookings','booking_id','order_id');
    }

    public function agent_ticket(){
        return $this->hasOne(AgentTicket::class,'id','agent_id');
    }

    public function customer(){
        return $this->hasOne(User::class,'id','customer_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class,'id','transaction_id');
    }

    public function subcategory()
    {
        return $this->belongsTo(TicketSubCategory::class,'sub_category_id','id');
    }

    public function organizer(){
        return $this->hasOne(User::class,'id','organiser_id');
    }
}
