<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public function setWebhookParameters($data)
    {
        $info = [
            'payload' =>[
                'setting_tab' => $data['setting_tab'],

                //site
                'site_site_name' => $data['site_site_name'],
                'site_site_slogan' => $data['site_site_slogan'],
                'site_site_footer' => $data['site_site_footer'],
                'site_logo_group' => $data['site_logo_group'],
                'site_site_favicon_group' => $data['site_site_favicon_group'],

                //seo
                'seo_meta_title' => $data['seo_meta_title'],
                'seo_meta_description' => $data['seo_meta_description'],
                'seo_meta_keywords' => $data['seo_meta_keywords'],

                //social
                'social_facebook' => $data['social_facebook'],
                'social_twitter' => $data['social_twitter'],
                'social_instagram' => $data['social_instagram'],
                'social_linkedin' => $data['social_linkedin'],

                //contact
                'contact_address' => $data['contact_address'],
                'contact_phone' => $data['contact_phone'],
                'contact_email' => $data['contact_email'],
                'contact_google_map_lat' => $data['contact_email'],
                'contact_google_map_long' => $data['contact_google_map_long'],

                //booking
                'booking_pre_booking_time' => $data['booking_pre_booking_time'],
                'booking_pre_cancellation_time' => $data['booking_pre_cancellation_time'],
                'booking_max_ticket_qty' => $data['booking_max_ticket_qty'],
                'booking_hide_expire_events' => $data['booking_hide_expire_events'],
                'booking_offline_payment_organizer' => $data['booking_offline_payment_organizer'],
                'booking_offline_payment_customer' => $data['booking_offline_payment_customer'],
                'booking_disable_booking_cancellation' => $data['booking_disable_booking_cancellation'],
                'booking_hide_ticket_download' => $data['booking_hide_ticket_download'],
                'booking_hide_google_calendar' => $data['booking_hide_google_calendar'],

                //multi-vendor
                'multi-vendor_multi_vendor' => $data['multi-vendor_multi_vendor'],
                'multi-vendor_admin_commission' => $data['multi-vendor_admin_commission'],
                'multi-vendor_verify_email' => $data['multi-vendor_verify_email'],
                'multi-vendor_verify_publish' => $data['multi-vendor_verify_publish'],
                'multi-vendor_manually_approve_organizer' => $data['multi-vendor_manually_approve_organizer'],

                //admin
                'admin_bg_image_group' => $data['admin_bg_image_group'],
                'admin_title' => $data['admin_title'],
                'admin_description' => $data['admin_description'],
                'admin_loader_group' => $data['admin_loader_group'],
                'admin_icon_image_group' => $data['admin_icon_image_group'],

                //apps google
                'apps_google_client_id' => $data['apps_google_client_id'],
                'apps_google_client_secret' => $data['apps_google_client_secret'],
                'apps_google_map_key' => $data['apps_google_map_key'],
                'apps_facebook_app_id' => $data['apps_facebook_app_id'],
                'apps_google_analytics_code' => $data['apps_google_analytics_code'],
                'apps_facebook_app_secret' => $data['apps_facebook_app_secret'],
                'apps_paypal_client_id' => $data['apps_paypal_client_id'],
                'apps_paypal_secret' => $data['apps_paypal_secret'],
                'apps_paypal_mode' => $data['apps_paypal_mode'],
                'apps_authorize_login_id' => $data['apps_authorize_login_id'],
                'apps_authorize_transaction_key' => $data['apps_authorize_transaction_key'],
                'apps_authorize_test_mode' => $data['apps_authorize_test_mode'],
                'apps_stripe_public_key' => $data['apps_stripe_public_key'],
                'apps_stripe_secret_key' => $data['apps_stripe_secret_key'],
                'apps_stripe_direct' => $data['apps_stripe_direct'],
                'apps_bitpay_key_name' => $data['apps_bitpay_key_name'],
                'apps_bitpay_encrypt_code' => $data['apps_bitpay_encrypt_code'],
                'apps_bitpay_production' => $data['apps_bitpay_production'],
                'apps_twilio_sid' => $data['apps_twilio_sid'],
                'apps_twilio_auth_token' => $data['apps_twilio_auth_token'],
                'apps_twilio_number' => $data['apps_twilio_number'],
                'apps_paystack_public_key' => $data['apps_paystack_public_key'],
                'apps_paystack_secret_key' => $data['apps_paystack_secret_key'],
                'apps_paystack_merchant_email' => $data['apps_paystack_merchant_email'],
                'apps_razorpay_keyid' => $data['apps_razorpay_keyid'],
                'apps_razorpay_keysecret' => $data['apps_razorpay_keysecret'],
                'apps_paytm_production' => $data['apps_paytm_production'],
                'apps_paytm_merchant_id' => $data['apps_paytm_merchant_id'],
                'apps_paytm_merchant_key' => $data['apps_paytm_merchant_key'],
                'apps_paytm_merchant_website' => $data['apps_paytm_merchant_website'],
                'apps_paytm_channel' => $data['apps_paytm_channel'],
                'apps_paytm_industry_type' => $data['apps_paytm_industry_type'],
                'apps_default_otp' => $data['apps_default_otp'],
                'apps_worldpay_url' => $data['apps_worldpay_url'],
                'apps_worldpay_mode' => $data['apps_worldpay_mode'],
                'apps_worldpay_instid' => $data['apps_worldpay_instid'],
                'apps_worldpay_status' => $data['apps_worldpay_status'],

                //mail
                'mail_mail_driver' => $data['mail_mail_driver'],
                'mail_mail_host' => $data['mail_mail_host'],
                'mail_mail_port' => $data['mail_mail_port'],
                'mail_mail_username' => $data['mail_mail_username'],
                'mail_mail_password' => $data['mail_mail_password'],
                'mail_mail_encryption' => $data['mail_mail_encryption'],
                'mail_mail_sender_email' => $data['mail_mail_sender_email'],
                'mail_mail_sender_name' => $data['mail_mail_sender_name'],

                //regional
                'regional_timezone_default' => $data['regional_timezone_default'],
                'regional_currency_default' => $data['regional_currency_default'],
                'regional_date_format' => $data['regional_date_format'],
                'regional_time_format' => $data['regional_time_format'],
                'regional_timezone_conversion' => $data['regional_timezone_conversion'],
            ]
        ];

        if (!is_null(@$data['updatedKeyId'])) {
            $info['updatedKeyId'] = $data['updatedKeyId'];
            $info['notification-type'] = 'setting-update';
        }

        return $info;
    }

}
