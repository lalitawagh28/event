<?php

namespace Classiebit\Eventmie\Models;

use App\Models\AgentTicket;
use App\Models\DeliveryCharge;
use App\Models\EventCollectionPoint;
use App\Models\EventDeliveryCharge;
use App\Models\EventGate;
use App\Models\EventSeatChart;
use App\Models\Promocode;
use App\Models\RegisterCountry;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
use Classiebit\Eventmie\Models\Tag;
use Classiebit\Eventmie\Models\Booking;
use Classiebit\Eventmie\Models\commission;
use Classiebit\Eventmie\Models\User;
use Classiebit\Eventmie\Models\Venue;
class Event extends Model
{

    protected $guarded = [];
    protected $hidden  = ['online_location'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     *  total events
     */

    public function total_events()
    {
        return Event::where(['status' => 1])->count();
    }

    /**
     *  total events
     */

     public function total_events_agent($user_id)
     {
        $ticket_ids = AgentTicket::where('agent_id',$user_id)->pluck('ticket_id')->toArray();
        $event_ids = Ticket::whereIn('id',$ticket_ids)->pluck('event_id')->toArray();
         return Event::whereIn('id',$event_ids)->where(['status' => 1])->count();
     }

    /* Get event tickets */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * Get the promocodes record associated with the ticket.
     */
    public function promocodes()
    {
        return $this->belongsToMany(Promocode::class, 'event_promocodes');
    }
    // get event
    public function get_event($slug = null, $event_id = null)
    {

        return Event::select('events.*')->from('events')
            //->with('promocodes','delivery_charges')
            ->with(['promocodes','delivery_charges','gates','collection_points','categories','seatchart','register_countries'])
            ->where(['slug' => $slug])
            ->orWhere(['id' => $event_id])
            ->selectRaw("(SELECT CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
            ->selectRaw("(SELECT SD.repetitive_type FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
            ->first();
    }
    public function is_upcomming($slug = null) {
        $tickets = Event::where('slug',$slug)->first()->tickets;

        if($tickets) {
            forEach($tickets as $ticket){
                if(Carbon::parse($ticket->sale_start_date)->format('Y-m-d') > Carbon::now() && Carbon::parse($ticket->sale_end_date)->format('Y-m-d') <= Carbon::now() ){
                    return Carbon::parse($ticket->sale_start_date)->format('Y-m-d');
                }
            }
        }
        return null;
    }

    // get event
    public function get_event_only($param)
    {
        return Event::select('events.*')->from('events')
            ->where(['id' => $param['event_id']])
            ->first();
    }

    // check event id that event id have login user or not
    public function get_user_event($event_id = null, $user_id = null)
    {
        return Event::select('events.*')->from('events')
                    ->with(['gates','collection_points','categories'])
                    ->where(['id' => $event_id, 'user_id' => $user_id ])
                    ->selectRaw("(SELECT SD.repetitive_type FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
                    ->selectRaw("(SELECT COUNT(BK.id) FROM bookings BK WHERE BK.event_id = events.id  ) count_bookings")
                    ->first();
    }

    // create user event
    public function save_event($params = [], $event_id = null)
    {

       // if have no event id then create new event
       return Event::updateOrCreate(
            ['id' => $event_id],
            $params
        );
    }


    // add tags to event
    public function event_tags($params = [])
    {
        $event = Event::where(['id' => $params['event_id']])->first();
        return $event->tags()->sync($params['event_tags']);
    }



    /**
     * Get events with
     * pagination and custom selection
     *
     * @return string
     */
    public function events($params  = [])
    {
        $query = Event::query();

        $query
        ->leftJoin("categories", "categories.id", '=', "events.category_id")
        ->select(["events.*", "categories.name as category_name"]);

        if(!empty($params['search']))
        {
            $query
            ->whereRaw("( title LIKE '%".$params['search']."%'
                OR venue LIKE '%".$params['search']."%' OR state LIKE '%".$params['search']."%' OR city LIKE '%".$params['search']."%')");
        }

        if(!empty($params['city']))
        {
            $query
            ->where('city','LIKE',"%{$params['city']}%");
        }

        if(!empty($params['state']))
        {
            $query
            ->where('state','LIKE',"%{$params['state']}%");
        }

        $query
        ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
        ->selectRaw("(SELECT SD.repetitive_type  FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type");

        if (!empty($params['category_id'])){
            $event_ids = EventCategory::where('category_id',$params['category_id'])->pluck('event_id')->toArray();
            $query->whereIn('events.id', $event_ids);
        }

        if(!empty($params['country_id']))
            $query->where('country_id',$params['country_id']);


        if(!empty($params['start_date']) && !empty($params['end_date']))
        {
            $query->whereRaw('CASE WHEN repetitive = 1 THEN start_date <= "'.$params['start_date'].'"
            AND end_date >= "'.$params['end_date'].'" ELSE  start_date >= "'.$params['start_date'].'" END' );
        }


        if(!empty($params['price']))
        {
            if($params['price'] == 'free')
                $query->where('price_type', "0" );

            if($params['price'] == 'paid')
                $query->where('price_type', 1);
        }

        $query
        ->where(["events.status" => 1, "events.publish" => 1]);

        // if hide expired events is on
        if(!empty(setting('booking.hide_expire_events')))
        {
            $today  = \Carbon\carbon::now(setting('regional.timezone_default'))->format('Y-m-d');
            $query->whereRaw('(IF(events.repetitive = 1, events.end_date >= "'.$today.'", events.start_date >= "'.$today.'"))');
        }

        return $query->with('categories')->orderBy('events.updated_at', 'DESC')->paginate(9);
    }

    // update price_type column of event table by 1 if have no free tickets
    public function update_price_type($event_id = null, $params = [])
    {
        return Event::where('id', $event_id)->update($params);
    }

    // get featured event for welocme page
    public function get_featured_events()
    {
        return Event::leftJoin("categories", "categories.id", '=', "events.category_id")
                ->select(["events.*", "categories.name as category_name"])
                ->where(['events.featured' => 1, 'events.publish' => 1, 'events.status' => 1, 'categories.status' => 1])
                ->whereDate('end_date', '>=', Carbon::today()->toDateString())
                ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
                ->selectRaw("(SELECT SD.repetitive_type  FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
                ->with('categories')
                ->limit(6)
                ->get();
    }

    // get top selling event
    public function get_top_selling_events()
    {
        return Event::leftJoin("categories", "categories.id", '=', "events.category_id")
                ->select(["events.*", "categories.name as category_name"])
                ->selectRaw("(SELECT SUM(BK.quantity) FROM bookings BK WHERE BK.event_id = events.id) total_booking")
                ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
                ->selectRaw("(SELECT SD.repetitive_type  FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
                ->where(['events.publish' => 1, 'events.status' => 1, 'categories.status' => 1])
                ->whereDate('end_date', '>=', Carbon::today()->toDateString())
                ->orderBy('total_booking', 'desc')
                ->limit(6)
                ->get();
    }

    // get upcomming events
    public function get_upcomming_events()
    {
        return  Event::leftJoin("categories", "categories.id", '=', "events.category_id")
                    ->select(["events.*", "categories.name as category_name"])
                    ->whereDate('start_date', '!=', Carbon::now()->format('Y-m-d'))
                    ->whereDate('start_date', '>', Carbon::now()->format('Y-m-d'))
                    ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
                    ->selectRaw("(SELECT SD.repetitive_type  FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
                    ->where(['events.publish' => 1, 'events.status' => 1, 'categories.status' => 1])
                    ->whereDate('end_date', '>', Carbon::today()->toDateString())
                    ->with('categories')
                    ->orderBy('start_date')
                    ->limit(6)
                    ->get();

    }

    // get cities events
    public function get_cities_events()
    {
        $mode           = config('database.connections.mysql.strict');

        $table          = 'events';
        $query          = DB::table($table);

        if(!$mode)
        {
            // safe mode is off
            $select = array(
                            "city",
                            "poster",
                            DB::raw("COUNT(*) AS cities"),
                        );
        }
        else
        {
            // safe mode is on
            $select = array(
                            "city",
                            DB::raw("ANY_VALUE(poster) AS poster"),
                            DB::raw("COUNT(*) AS cities"),
                        );
        }

        $query->select($select)
                ->where(['publish' => 1, 'status' => 1]);


        // if hide expired events is on
        if(!empty(setting('booking.hide_expire_events')))
        {
            $today  = \Carbon\carbon::now(setting('regional.timezone_default'))->format('Y-m-d');
            $query->whereRaw('(IF(events.repetitive = 1, events.end_date >= "'.$today.'", events.start_date >= "'.$today.'"))');

        }

        $result = $query->where(['events.publish' => 1, 'events.status' => 1])->groupBy('city')
                        ->orderBy('cities', 'DESC')
                        ->limit(6)->get();

        return to_array($result);
    }

    // get organisers
    public function get_organizers($search = null)
    {
        $query = DB::table('users');
        $query->select('name', 'id', 'email')
                ->where('role_id', 3);
        if(!empty($search))
        {
            $query
            ->where(function ($query) use($search) {
                $query->where('name','LIKE',"%{$search}%")
                      ->orWhere('email','LIKE',"%{$search}%");
            });
        }
        $result = $query->limit(10)->get();

        return to_array($result);
    }

    // get customers
    public function get_customers($search = null)
    {
        $query = DB::table('users');
        $query->select('name', 'id')
                ->where('role_id', 2);
        if(!empty($search))
        {
            $query
            ->where(function ($query) use($search) {
                $query->where('name','LIKE',"%{$search}%")
                      ->orWhere('email','LIKE',"%{$search}%");
            });
        }
        $result = $query->limit(10)->get();
        return to_array($result);
    }

    // search customers
    public function search_customers($email = null)
    {
        $query = DB::table('users');
        $query->select('name', 'id', 'email')
                ->where('role_id', 2)
                ->where('email', $email);

        $result = $query->get();
        return to_array($result);
    }

    //get selected tags from tag_event table when organiser editing his event
    public function selected_event_tags($event_id = null)
    {
        $event = Event::where('id', $event_id)->first();

        return $event->tags;

    }

    /**
     * The tags that belong to the event.
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }


    /**
     * =====================GEt events for particular organiser start=====================================
     */

    // get my evenst of particular organiser
    public function get_my_events($params = [])
    {
        return Event::select('events.*')
            ->from('events')
            ->with(['gates','collection_points','categories'])
            ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
            ->selectRaw("(SELECT CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
            ->selectRaw("(SELECT COUNT(BK.id) FROM bookings BK WHERE BK.event_id = events.id  ) count_bookings")
            ->where(['user_id' => $params['organiser_id'] ])
            ->with('categories')
            ->orderBy('events.id','desc')
            ->paginate(9);
    }

    // organiser can disable own event
    public function disable_event($params = [], $data = [])
    {
        return Event::where(['id' => $params['event_id'], 'user_id' => $params['organiser_id']])
            ->update($data);
    }

    // only admin can delete event
    public function delete_event($params = [])
    {
        return Event::where(['id' => $params['event_id']])
            ->delete();
    }



    // get all my evenst of particular organiser
    public function get_all_myevents($params = [])
    {
        $result = Event::select('events.*')
                    ->from('events')
                    ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
                    ->selectRaw("(SELECT CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
                    ->where(['user_id' => $params['organiser_id'] ])
                    ->with('categories')
                    ->get();

        return to_array($result);
    }

    /**
     *  top selling event
     */
    public function top_selling_events($user_id = null)
    {
        $query  = Event::query();

                $query->select(
                    'events.*',
                    DB::raw("(select sum(quantity) from bookings where bookings.event_id = events.id ) as total_bookings ")
                )
                ->whereDate('end_date', '>=', Carbon::today()->toDateString());

                if(!empty($user_id))
                {
                    $query->where(['user_id' => $user_id]);
                }

        $result =  $query->orderBy('total_bookings', 'DESC')
                ->limit(10)
                ->get();

        return to_array($result);
    }

    /**
     *  top selling event
     */
    public function top_selling_events_agent($user_id = null)
    {
        $query  = Event::query();
                $agentIds = AgentTicket::where('agent_id',$user_id)->pluck('id')->toArray();
                $ids = '';
        if($agentIds){
            $ids = implode(',',$agentIds);
            $query->select(
                'events.*',
                DB::raw("(select sum(quantity) from bookings where bookings.event_id = events.id and agent_id IN (".$ids.")) as total_bookings ")
            )
            ->whereDate('end_date', '>=', Carbon::today()->toDateString());

            if(!empty($user_id))
            {
                $ticket_ids = AgentTicket::where('agent_id',$user_id)->pluck('ticket_id')->toArray();
                $event_ids = Ticket::whereIn('id',$ticket_ids)->pluck('event_id')->toArray();
                $query->whereIn('id', $event_ids);
            }

            $result =  $query->orderBy('total_bookings', 'DESC')
                ->limit(10)
                ->get();


            return to_array($result);
        }
    }
    /**
     *  total event count
     */

    public function total_event($user_id = null)
    {
        if(!empty($user_id))
        {
            return Event::where(['status' => 1, 'user_id' => $user_id])->count();

        }
        return Event::count();
    }

    // get all my evenst for admin
    public function get_all_events($params = [], $user_id = null)
    {
        $query = Event::query();

        if(!empty($user_id))
        {

            $query->where(['user_id' => $user_id]);

        }

        $result = $query->get();

        return to_array($result);
    }


    // get all my evenst for admin
    public function get_all_events_agent($params = [], $user_id = null)
    {
        $ticket_ids = AgentTicket::where('agent_id',$user_id)->pluck('ticket_id')->toArray();
        $event_ids = Ticket::whereIn('id',$ticket_ids)->pluck('event_id')->toArray();
        $query = Event::query();

        if(!empty($user_id))
        {

            $query->whereIn('id',$event_ids);

        }

        $result = $query->get();
        return to_array($result);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }


    public function venues()
    {
        return $this->belongsToMany(Venue::class);
    }

    public function setWebhookParameters($data)
    {
        if (!is_null(@$data['store_timing'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['store_timing']
                ]
            ];
        }elseif(!is_null(@$data['store_event_details'])){
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['store_event_details']
                ]
            ];
        }elseif(!is_null(@$data['store_ticket'])){
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['store_ticket']
                ]
            ];
        }elseif(!is_null(@$data['store_seo'])){
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['store_seo']
                ]
            ];
        }elseif (!is_null(@$data['store_venue'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['store_venue']
                ]
            ];
        }elseif (!is_null(@$data['store_location'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['store_location']
                ]
            ];
        }elseif (!is_null(@$data['store_media'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['store_media']
                ]
            ];
        }elseif (!is_null(@$data['add_tags'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['add_tags']
                ]
            ];
        }elseif (!is_null(@$data['store_tags'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['store_tags']
                ]
            ];
        }elseif (!is_null(@$data['delete_tickets'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['delete_tickets']
                ]
            ];
        }elseif (!is_null(@$data['upload_seatchart'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['upload_seatchart']
                ]
            ];
        }elseif (!is_null(@$data['disable_enable_seatchart'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['disable_enable_seatchart']
                ]
            ];
        }elseif (!is_null(@$data['save_seat'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['save_seat']
                ]
            ];
        }elseif (!is_null(@$data['publish_event'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['publish_event']
                ]
            ];
        }elseif (!is_null(@$data['deletedId'])) {
            $info = [
                'notification_type'=>'event_delete',
                'payload'=>[
                    $data['deletedId']
                ]
            ];
        }elseif (!is_null(@$data['disable_seat'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['disable_seat']
                ]
            ];
        }elseif (!is_null(@$data['enable_seat'])) {
            $info = [
                'notification_type'=>$data['action'],
                'payload'=>[
                    $data['enable_seat']
                ]
            ];
        }
        return $info;
    }


     /**
      * ============================= End particular organiser events ===================================================
      */

      public function country() {
        return $this->hasOne(Country::class,'id','country_id');
      }


      /**
     * The taxes that belong to the tickes.
     */
    // public function delivery_charges()
    // {
    //     return $this->belongsToMany(DeliveryCharge::class,'event_delivery_charges');
    // }


    public function get_admin_delivery_charges($event_id){
        $event = Event::find($event_id);
        $delivery_charge = 0;
        $delivery_charges = $event->delivery_charges;

        if($delivery_charges->count() == 0){
            return $delivery_charge;
        } else {

            foreach($delivery_charges as  $delivery_c) {
                if($delivery_c->net_price == 'including')
                    {
                        if($delivery_c->rate)
                            $delivery_charge = $delivery_charge - $delivery_c->rate;

                    }


                    // in case of excluding
                    if($delivery_c->net_price == 'excluding')
                    {
                        if($delivery_c->rate)
                            $delivery_charge = $delivery_charge + $delivery_c->rate;
                    }
              }

        }
        return $delivery_charge;
    }
    public function delivery_charges(){
        return $this->hasMany(EventDeliveryCharge::class,'event_id','id');
    }
    public function get_delivery_charges($event_id){
        return Event::find($event_id)?->delivery_charge;
    }


    public function collection_points(){
        return $this->hasMany(EventCollectionPoint::class,'event_id','id');
    }

    public function gates(){
        return $this->hasMany(EventGate::class,'event_id','id');
    }


    public function categories() {

        return $this->belongsToMany(Category::class, 'event_categories', 'event_id', 'category_id');
    }

    /**
     * Get the reviews for the event .
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    

    public function seatchart(){
        return $this->hasMany(EventSeatChart::class,'event_id','id');
    }

    public function register_countries(){
        return $this->hasOne(RegisterCountry::class,'currency','currency');   
    }

    public function isEventEnded(){
        if(Carbon::parse($this->start_date)->lte(Carbon::today())) return true; else return false;
    }

}
