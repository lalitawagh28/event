<?php

namespace Classiebit\Eventmie\Models;

use App\Models\TaxTicket;
use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $guarded = [];

    public function get_taxes()
    {
        $result = Tax::where(['status' => 1, 'admin_tax' => 0])->get();
        return to_array($result);
    }

    public function get_admin_taxes()
    {
        return Tax::where(['status' => 1, 'admin_tax' => 1])->get();
    }

    public function setWebhookParameters($data)
    {
        if (is_null(@$data['deletedId']) && is_null(@$data['updatedOrderId'])) {
            $info = [
                'payload' => [
                    'title' => $data['title'],
                    'rate_type' => $data['rate_type'],
                    'rate' => $data['rate'],
                    'net_price' => $data['net_price'],
                    'status' => $data['status'],
                    'admin_tax' => $data['admin_tax']
                ]
            ];
            if (!is_null(@$data['updatedId'])) {
                $info['updatedId'] = $data['updatedId'];
                $info['notification-type'] = 'Tax-update';
            } else {
                $info['notification-type'] = 'tax-add';
            }
        } elseif (!is_null(@$data['updatedOrderId'])) {
            $info['updatedOrderId'] = $data['updatedOrderId'];
            $info['notification-type'] = 'tax-order-update';
        } else {
            $info['deletedId'] = $data['deletedId'];
            $info['notification-type'] = count($data['deletedId']) > 1 ? 'taxes-bulk-delete' : 'taxes-delete';
        }
        return $info;
    }

    public function getTicketTax()
    {
        return $this->hasMany(TaxTicket::class, 'tax_id', 'id');
    }
}
