<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends \TCG\Voyager\Models\Post
{
    protected $guarded = [];
    protected $translatable = [];

    // posts with limit for welcome page
    public function index()
    {
        $result = Post::where(['status' => 'PUBLISHED'])
            ->limit(3)->orderBy('updated_at', 'DESC')->get();

        return to_array($result);
    }

    // particular post view
    public function view($slug = null)
    {
        return Post::where(['slug' => $slug])->first();
    }

    // get posts
    public function get_posts()
    {
        return Post::where(['status' => 'PUBLISHED'])->orderBy('updated_at', 'DESC')->paginate(9);
    }

    public function setWebhookParameters($data)
    {
        if (is_null(@$data['deletedId']) && is_null(@$data['updatedOrderId'])) {
            $info = [
                'payload' => [
                    'category_id' => $data['category_id'],
                    'title' => $data['title'],
                    'seo_title' => $data['title'],
                    'excerpt' => $data['excerpt'],
                    'body' => $data['body'],
                    'image' => $data['image'],
                    'slug' => $data['slug'],
                    'meta_description' => $data['meta_description'],
                    'meta_keywords' => $data['meta_keywords'],
                    'status' => $data['status']
                ]
            ];
            if (!is_null(@$data['updatedId'])) {
                $info['updatedId'] = $data['updatedId'];
                $info['notification-type'] = 'Post-update';
            } else {
                $info['notification-type'] = 'Post-add';
            }
        } elseif (!is_null(@$data['updatedOrderId'])) {
            $info['updatedOrderId'] = $data['updatedOrderId'];
            $info['notification-type'] = 'Post-order-update';
        } else {
            $info['deletedId'] = $data['deletedId'];
            $info['notification-type'] = count($data['deletedId']) > 1 ? 'Posts-bulk-delete' : 'Posts-delete';
        }
        return $info;
    }
}
