<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    public function setWebhookParameters($data)
    {
        if (is_null(@$data['deletedId'])) {
            $info = [
                'payload' => [
                    'title' => $data['title'],
                    'url' => $data['url'],
                    'icon_class' => $data['icon_class'],
                    'target' => $data['target'],
                ]
            ];
            if (!is_null(@$data['updatedId'])) {
                $info['updatedId'] = $data['updatedId'];
                if ($data['menu_id'] == 2) {
                    $info['notification-type'] = 'header-menu-update';
                } else {
                    $info['notification-type'] = 'footer-menu-update';
                }
            } else {
                if ($data['menu_id'] == 2) {
                    $info['notification-type'] = 'header-menu-add';
                } else {
                    $info['notification-type'] = 'footer-menu-add';
                }
            }
        } else {
            $info['deletedId'] = $data['deletedId'];
            if ($data['menu_id'] == 2) {
                $info['notification-type'] = 'header-menu-delete';
            } else {
                $info['notification-type'] = 'footer-menu-delete';
            }
        }
        return $info;
    }

}
