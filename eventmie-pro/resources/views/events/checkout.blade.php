@extends('eventmie::layouts.app')

@section('title', $event->title)
@section('meta_title', $event->meta_title)
@section('meta_keywords', $event->meta_keywords)
@section('meta_description', $event->meta_description)
@section('meta_image', '/storage/'.$event['thumbnail'])
@section('meta_url', url()->current())

@section('content')
<!--SCHEDULE-->
<section>
<div class="checkout_wrapper">
    <div class="ticket_banner">
         <div class="banner_img">
             <img src="{{ $event->checkout_banner }}" class="checkout_ban_img">
         </div>
         <div class="ticket_banner_content">
            <h4 class="event_title">{{ $event->title }}  {{  $event->guest_star ?  "| ".$event->guest_star : ''}}</h4>
             <span class="event_subtitle">{{ $event->categories->count() ? join(",",collect($event->categories)->map(function($v){ return $v->name; })->toArray()) . "| "  : "" }}  
                <i class="fa fa-calendar" aria-hidden="true"></i> 
                <time-zone  
                                                                :datetime="{{ json_encode($event->start_date . ' ' . $event->start_time, JSON_HEX_APOS) }}"
                                                                :format="{{ json_encode('YYYY-MM-DD HH:mm:ss', JSON_HEX_APOS) }}"
                                                                :timezone="{{ json_encode( $event->time_zone, JSON_HEX_APOS) }}"
                                                                :display_format="{{ json_encode('DD MMM YYYY', JSON_HEX_APOS) }}"
                                                                :show_zonename="{{ json_encode(false, JSON_HEX_APOS) }}"></time-zone> 
                | 
                <i class="fa fa-clock-o" aria-hidden="true"></i>  
                {{-- {{ \Carbon\Carbon::parse($event->start_date . " " . $event->start_time)->format("H:i") }} GMT  --}}
                <time-zone  
                                                                :datetime="{{ json_encode($event->start_date . ' ' . $event->start_time, JSON_HEX_APOS) }}"
                                                                :format="{{ json_encode('YYYY-MM-DD HH:mm:ss', JSON_HEX_APOS) }}"
                                                                :timezone="{{ json_encode( $event->time_zone, JSON_HEX_APOS) }}"
                                                                :display_format="{{ json_encode('hh:mm', JSON_HEX_APOS) }}"
                                                                :show_zonename="{{ json_encode(true, JSON_HEX_APOS) }}"></time-zone>
                | <i class="fa fa-map-marker" aria-hidden="true"></i> {{  $event->event_address }}</span>
         </div>
     </div>
     @php
     $wallet_url = (config('app.env') == 'production') ? "https://events.dhigna.com/update-password" : "https://eventsuat.dhigna.com/update-password";
     $wallet_url = (config('app.env') == 'preprod') ? "https://dhigna-events-preprod.azurewebsites.net/update-password" : $wallet_url;
     $seatchart = null;
     $is_worldpay = setting('apps.worldpay_status') == 'active' ? 1 : 0;
     if($event->seat_image){
        $seatchart = Storage::disk('azure')->temporaryUrl($event->seat_image, now()->addMinutes(5));
     }
     


     @endphp
     <checkout-component
         :event="{{ json_encode($event, JSON_HEX_APOS) }}"
         :user="{{ json_encode($user_data, JSON_HEX_APOS) }}"
         :tickets="{{ json_encode($tickets, JSON_HEX_APOS) }}"
         :wallets="{{ json_encode($wallets, JSON_HEX_APOS) }}"
         :max_ticket_qty="{{ json_encode($max_ticket_qty, JSON_HEX_APOS) }}"
         :currency="{{ json_encode($currency, JSON_HEX_APOS) }}"
         :login_user_id="{{ json_encode(\Auth::id(), JSON_HEX_APOS) }}"
         :is_customer="{{ Auth::id() ? (Auth::user()->hasRole('customer') ? 1 : 0) : 1 }}"
         :is_organiser="{{ Auth::id() ? (Auth::user()->hasRole('organiser') ? 1 : 0) : 0 }}"
         :is_admin="{{ Auth::id() ? (Auth::user()->hasRole('admin') ? 1 : 0) : 0 }}"
         :is_pos="{{ Auth::id() ? (Auth::user()->hasRole('pos') ? 1 : 0) : 0 }}"
         :is_paypal="{{ $is_paypal }}"
         :is_offline_payment_organizer="{{ setting('booking.offline_payment_organizer') ? 1 : 0 }}"
         :is_offline_payment_customer="{{ setting('booking.offline_payment_customer') ? 1 : 0}}"
         :wallet_url="{{ json_encode($wallet_url, JSON_HEX_APOS) }}"
         :booked_tickets="{{ json_encode($booked_tickets, JSON_HEX_APOS) }}"
         :worldpay="{{$is_worldpay }}"
         :captcha_img="{{ json_encode(captcha_src('flat')) }}"
         :seatchart = {{ json_encode($seatchart, JSON_HEX_APOS) }}
     ></checkout-component>

</div>
</section>
<!--SCHEDULE END-->

@endsection

@section('javascript')
<script type="text/javascript">


    var stripe_publishable_key   = {!! json_encode(setting('apps.stripe_public_key')) !!};

    var stripe_secret_key        = {!! json_encode( $extra['stripe_secret_key']  )  !!};

    var is_stripe                = {!! json_encode( $extra['is_stripe']) !!};

    var is_authorize_net         = {!! json_encode( $extra['is_authorize_net']) !!};

    var is_bitpay                = {!! json_encode( $extra['is_bitpay']) !!};

    var is_stripe_direct         = {!! json_encode( $extra['is_stripe_direct']) !!};

    var is_twilio                = {!! json_encode( $extra['is_twilio']) !!};

    var default_payment_method   = {!! json_encode( $extra['default_payment_method']) !!};

    var sale_tickets             = {!! json_encode( $extra['sale_tickets']) !!};

    var is_pay_stack              = {!! json_encode( $extra['is_pay_stack']) !!};

    var is_razorpay              = {!! json_encode( $extra['is_razorpay']) !!};

    var is_paytm                 = {!! json_encode( $extra['is_paytm']) !!};

</script>
<script src="https://cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>
<script type="text/javascript" src="{{ asset('js/events_show_v1.9.js') }}?ver={{ filemtime(public_path('js/events_show_v1.9.js')) }}"></script>
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.23.0/axios.min.js" integrity="sha512-Idr7xVNnMWCsgBQscTSCivBNWWH30oo/tzYORviOCrLKmBaRxRflm2miNhTFJNVmXvCtzgms5nlJF4az2hiGnA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@stop
