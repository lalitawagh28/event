<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">


<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-KRXCXW4K');</script>

<!-- End Google Tag Manager -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-SES0TNCMX6"></script>
<script>   window.dataLayer = window.dataLayer || [];   function gtag(){dataLayer.push(arguments);}   gtag('js', new Date());   gtag('config', 'G-SES0TNCMX6'); </script>

    @include('eventmie::layouts.meta')

    @include('eventmie::layouts.favicon')

    @include('eventmie::layouts.include_css')

    @yield('stylesheet')
    @livewireStyles

</head>

<body class="home" {!! is_rtl() ? 'dir="rtl"' : '' !!}>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="
    https://www.googletagmanager.com/ns.html?id=GTM-KRXCXW4K"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
        your browser</a> to improve your experience.</p>
    <![endif]-->

    {{-- Ziggy directive --}}
    @routes

    {{-- Main wrapper --}}
    <div class="lgx-container" id="eventmie_app">

        @include('eventmie::layouts.header')

        @php
            $no_breadcrumb = ['eventmie.welcome', 'eventmie.events_show', 'eventmie.events_checkout', 'eventmie.events_index', 'eventmie.login', 'eventmie.register', 'eventmie.register_show', 'eventmie.password.request', 'eventmie.password.reset', 'event-list', 'event-details', 'tickets_booking', 'profile', 'deposit_form', 'deposit_form1', 'deposit_verification', 'deposit-card_details', 'payment_success', 'eventmie.profile', 'eventmie.resetPin', 'eventmie.page', 'eventmie.mybookings_index', 'eventmie.mybookings_order_bookings_show', 'ticketTransfer', 'ticketTransferAcceptPayment', 'ticketTransferAccept','events.getEventInfo','payments'];
        @endphp
        @if (!in_array(Route::currentRouteName(), $no_breadcrumb))
            @include('eventmie::layouts.breadcrumb')
        @endif

        <section class="main-wrapper">

            {{-- page content --}}
            @yield('content')

            {{-- set progress bar --}}
            <vue-progress-bar></vue-progress-bar>
        </section>

        @include('eventmie::layouts.footer')

    </div>
    <!--Main wrapper end-->

    @include('eventmie::layouts.include_js')

    {{-- Page specific javascript --}}
    @yield('javascript')
    @livewireScripts
    {{-- <script
      type="text/javascript"
      src="https://d3mkw6s8thqya7.cloudfront.net/integration-plugin.js"
      id="aisensy-wa-widget"
      widget-id="7BwcSH"
    >
    </script> --}}

    <script type="text/javascript" id="zsiqchat">var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode: "siqdd697af85c1e3089be79f0cac1ca801d82c1b9ddcc17f9fcb98f370a3a32dec6", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src=
    "https://salesiq.zohopublic.eu/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);</script>
    <script>
        $(document).ready(function() {
            function toggleSlider() {
                var $slideMenu = $(".slide-menu");
                $slideMenu.animate({
                    left: $slideMenu.css('left') === '0px' ? '-372px' : '0'
                });
            }

            $("#toggleSlider").on("click", function() {
                toggleSlider();
            });
        });

        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });

        function searchEvent(the) {
            let event = $(the).val();
            $.ajax({
                type: 'POST',
                url: "{{ route('searchEvent') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'event': event
                },
                success: function(data) {
                    console.log(data);
                    if (data.status == false) {
                       $('.categorymsg').css('display','block');
                    } else {
                        window.location.href = "/events?search="+data.event;
                    }

                },

                error: function(msg) {
                    console.log(msg);
                    var errors = msg.responseJSON;
                }
            });
        }
    </script>
</body>

</html>
