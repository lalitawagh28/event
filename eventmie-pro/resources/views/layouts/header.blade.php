<header>


    <div class="slide-menu topburger-popup">
        <div class="burger-box">
            <div class="popup-logo"><img src="/images/logo.png"></div>
            <ul>
                <li><a href="#"><img src="images/Banking-picon.png"> Banking</a></li>
                <li><a href="#"><img src="images/Events-picon.png"> Events</a></li>
                <li><a href="#"><img src="images/Shops-picon.png"> Shops</a></li>
                <li><a href="#"><img src="images/Wallet-picon.png"> Wallet</a></li>
                <li><a href="#"><img src="images/Directory-picon.png"> Directory</a></li>
                <li><a href="#"><img src="images/Store-picon.png"> Marketplace</a></li>
            </ul>
        </div>
    </div>

    <div class="lgx-header">
        <div id="navbar_vue" class="lgx-header-position lgx-header-position-white lgx-header-position-fixed">
            <div class="lgx-container-fluid">
                <!-- GDPR -->
                <cookie-law theme="gdpr" button-text="@lang('eventmie-pro::em.accept')">
                    <div slot="message">
                        <gdpr-message></gdpr-message>
                    </div>
                </cookie-law>
                <!-- GDPR -->

                <!-- Vue Alert message -->
                @if ($errors->any())
                    <alert-message :errors="{{ json_encode($errors->all(), JSON_HEX_APOS) }}"></alert-message>
                @endif

                @if (session('status'))
                    <alert-message :message="'{{ session('status') }}'"></alert-message>
                @endif
                <!-- Vue Alert message -->
                @php
                    $msg = 'Event is not found';
                @endphp



                <nav class="navbar navbar-default lgx-navbar navbar-expand-md 11111">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar" aria-expanded="false" aria-controls="navbar"
                            onclick="document.getElementById('navbar').classList.toggle('in')">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        {{--
                        <div class="burger-top" id="toggleSlider">
                            <a href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
                        </div> --}}
                        <div class="lgx-logo">
                            <a href="{{ config('app.url') }}" class="lgx-scroll">
                                <img src="/storage/{{ setting('site.logo') }}" alt="{{ setting('site.site_name') }}" />
                                <span class="brand-name">{{ setting('site.site_name') }}</span><br>
                                <span class="brand-slogan">{{ setting('site.site_slogan') }}</span>
                            </a>
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav lgx-nav navbar-right ml-auto">
                            @php $categoriesMenu = categoriesMenu() @endphp
                            @if (!empty($categoriesMenu))
                                <li>
                                    <a class="dropdown-toggle active" href="#" data-toggle="dropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false">Categories </a>
                                    <ul class="dropdown-menu multi-level">
                                        @foreach ($categoriesMenu as $val)
                                            <li>
                                                <a
                                                    href="{{ route('eventmie.events_index', ['category' => urlencode($val->name)]) }}">
                                                    {{ $val->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                        {{-- <li>Music Concert</li>
                                    <li>Sports</li>
                                    <li>Entertaiment</li>
                                    <li>Business & Seminar</li>
                                    <li>Food & Drinks</li> --}}
                                    </ul>
                                </li>
                            @endif
                            <li>
                                <div class="relative countery-top Search-sports mr-4">
                                    <input type="text" id="hs-leading-icon" name="hs-leading-icon"
                                        class="form-input pl-10 ti-form-input ltr:pl-11 rtl:pr-11 focus:z-10"
                                        placeholder="Search for Events, Sports...." onchange="searchEvent(this)">
                                    <div
                                        class="search_box absolute inset-y-0 ltr:left-0 rtl:right-0 flex items-center pointer-events-none z-20 ltr:pl-4 rtl:pr-4">
                                        <i class="fas fa-search"></i>
                                    </div>

                                </div>
                                <div class="categorymsg" style="display: none;color:red;">
                                    {{$msg}}
                                </div>


                            </li>
                            <li>
                                <div class="relative countery-top">
                                    {{-- <input type="text" id="hs-leading-icon" name="hs-leading-icon"
                                        class="form-input pl-10 ti-form-input ltr:pl-11 rtl:pr-11 focus:z-10"
                                        placeholder="United Kingdom"> --}}
                                    <div
                                        class="absolute inset-y-0 ltr:left-0 rtl:right-0 flex items-center pointer-events-none z-20 ltr:pl-4 rtl:pr-4">
                                        <img src="/flags/GB.png" style="margin-top:10px;">
                                    </div>
                                </div>

                            </li>
                            <!-- Authentication Links -->
                            @guest
                                @include('eventmie::layouts.guest_header')
                            @else
                                @include('eventmie::layouts.member_header')
                            @endguest



                        </ul>
                    </div><!--/.nav-collapse -->
                </nav>
            </div>
            <!-- //.CONTAINER -->
        </div>
    </div>
</header>
