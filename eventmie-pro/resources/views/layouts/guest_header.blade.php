<li id="login_user" >
    <a  class="lgx-scroll" href="{{ route('eventmie.login') }}"> @lang('eventmie-pro::em.login')</a>
</li>
<li id="register_user">
    <a class="dropdown-toggle active" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" v-pre> Registration</a>
    <ul class="dropdown-menu multi-level">
        <li>
            <a class="lgx-scroll" href="{{ route('eventmie.register_show') }}">@lang('eventmie-pro::em.register_customer')</a></li>
        </li>
        <li>
            <a class="lgx-scroll" href="{{ route('admin.register.personal_detail') }}">@lang('eventmie-pro::em.register_organizer')</a></li>
        </li>
        <li>
            <a class="lgx-scroll" href="{{ route('agent.register.personal_detail') }}">@lang('eventmie-pro::em.register_agent')</a></li>
        </li>
    </uL>
</li>
