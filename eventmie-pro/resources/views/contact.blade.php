@extends('eventmie::layouts.app')

@section('title')
    @lang('eventmie-pro::em.contact')
@endsection
@section('stylesheet')
<style>
.input-placeholder {
    position: relative;
  }
  .input-placeholder input {
    padding: 10px;
    font-size: 25px;
  }
  .input-placeholder input:valid + .placeholder {
    display: none;
  }
  .placeholder1 {
    position: absolute;
    pointer-events: none;
    top: 10px;
    height: 25px;
    font-size: 14px;
    left: 20px;
    margin: auto;
    color: #999;
  }
  .placeholder {
    position: absolute;
    pointer-events: none;
    top: 0;
    bottom: 0;
    height: 25px;
    font-size: 14px;
    left: 20px;
    margin: auto;
    color: #999;
  }
  
  .placeholder span,.placeholder1 span {
    color: red;
  }
  </style>
@endsection
@section('content')

    <main>
        <div class="lgx-page-wrapper">
            <!--News-->
            <section>
                <div class="container">
                    <div class="row">

                        <div class="col-12 offset-sm-2 col-sm-8 offset-lg-3 col-lg-6">
                            @if (\Session::has('msg'))
                                <div class="alert alert-success">
                                    {{ \Session::get('msg') }}
                                </div>
                            @endif
                            <form method="POST" class="lgx-contactform" novalidate action="{{route('eventmie.store_contact')}}">
                                @csrf
                                @honeypot

                                <div class="form-group">
                                    <div class="input-placeholder">
                                        <input type="text" name="name" class="form-control lgxname" id="lgxname" required>
                                        <div class="placeholder">
                                            @lang('eventmie-pro::em.name') <span>*</span>
                                        </div>
                                    </div>

                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-placeholder">
                                        <input type="email" name="email" class="form-control lgxemail" id="lgxemail" required >
                                        <div class="placeholder">
                                            @lang('eventmie-pro::em.email') <span>*</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-placeholder">
                                        <input type="text" name="title" class="form-control lgxsubject" id="lgxsubject" required>
                                        <div class="placeholder">
                                            @lang('eventmie-pro::em.title') <span>*</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('title'))
                                        <div class="alert alert-danger">{{ $errors->first('title') }}</div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-placeholder">
                                        <textarea class="form-control lgxmessage" name="message" id="lgxmessage" rows="5" required ></textarea>
                                        <div class="placeholder1">
                                            @lang('eventmie-pro::em.message') <span>*</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('message'))
                                        <div class="alert alert-danger">{{ $errors->first('message') }}</div>
                                    @endif
                                </div>


                                <button type="submit"  value="contact-form" class="lgx-btn lgxsend lgx-send btn-block"><span><i class="fas fa-paper-plane"></i> @lang('eventmie-pro::em.send_message')</span></button>
                            </form>

                        </div> <!--//.COL-->
                    </div>
                </div><!-- //.CONTAINER -->
            </section>
            <!--News END-->
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <g-component
                        :lat="{{ json_encode(setting('contact.google_map_lat'), JSON_HEX_APOS) }}"
                        :lng="{{ json_encode(setting('contact.google_map_long'), JSON_HEX_APOS) }}"
                    >
                    </g-component>
                </div>
            </div>
        </div>
    </main>



@endsection

@section('javascript')


<script type="text/javascript" src="{{ eventmie_asset('js/events_show_v1.8.js') }}"></script>

<script type="text/javascript">
    var google_map_key = {!! json_encode( setting('apps.google_map_key')) !!};

</script>

@stop
