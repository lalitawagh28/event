<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/login-responsive.css') }}">
    <style>
        .login,
        .image {
            min-height: 100vh;
        }

        .btn {
            background: #FF5C02;
        }

        .captcha {
            background: #D9D9D9;
            padding: 5px 10px;

        }

        .ti-form-input {
            margin-right: 5px;
        }

        .reg_captcha {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .highlight {
            color: #FF5C02;
        }

        .flagcountry {
            position: absolute !important;
            left: 20px !important;
            color: black;
        }

        .home_btn {

            float: right;
            margin-top: 15px;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
</head>

<body>

    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>
            <div class="col-md-6 bg-light">
                <a href="/" class="btn lgx-btn w-30 home_btn"><i class="fas fa-home"></i> Home</a>

                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-10 mx-auto">
                                <h5 class="">Register Your account</h5>
                                <p>Fill the details below to register your account</p>
                                <form method="POST" action="{{ route('eventmie.register') }}">
                                    @csrf
                                    @honeypot

                                    <div class="form-group mb-3">
                                        <div class="">
                                            <div class="form-group mb-3">
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">First Name <span
                                                            style="color:red">*</span></label>
                                                    <input id="inputEmail" type="text" placeholder="Your First Name"
                                                        required="" name="first_name" autofocus=""
                                                        class="form-control" value="{{ old('first_name') }}">
                                                    @error('first_name')
                                                        <span role="alert" style="color:red">
                                                            <small>{{ $message }}</small>
                                                        </span><br>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Last Name <span
                                                            style="color:red">*</span></label>
                                                    <input id="inputEmail" type="text" placeholder="Your Last Name"
                                                        required="" name="last_name" class="form-control"
                                                        value="{{ old('last_name') }}">
                                                    @error('last_name')
                                                        <span role="alert" style="color:red">
                                                            <small>{{ $message }}</small>
                                                        </span><br>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Email <span
                                                            style="color:red">*</span></label>
                                                    <input id="inputEmail" type="email" placeholder="Your Email"
                                                        required="" name="email" class="form-control"
                                                        value="{{ old('email') }}">
                                                    @error('email')
                                                        <span role="alert" style="color:red">
                                                            <small>{{ $message }}</small>
                                                        </span><br>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Mobile
                                                        Number <span style="color:red">*</span></label>

                                                    <div class="relative countery-top form-inline">
                                                        <span class="flagcountry"><img src="/flags/GB.png"> +44</span>

                                                        <input id="phone" type="text"
                                                            placeholder="Your Mobile Number" required=""
                                                            autofocus="" class="form-control"
                                                            style="width:100%;padding-left: 75px;" name="phone"
                                                            value="{{ old('phone') }}">
                                                    </div>
                                                    @error('phone')
                                                        <span role="alert" style="color:red">
                                                            <small>{{ $message }}</small>
                                                        </span><br>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Pin <span
                                                            style="color:red">*</span> <span class="text-sm" style="font-size:11px">{{ __("eventmie-pro::em.pin_message")}}</span></label>
                                                    <input id="inputEmail" type="password"
                                                        placeholder="Enter 6 digit pin" required="" autofocus=""
                                                        class="form-control" name="password">
                                                    @error('password')
                                                        <span role="alert" style="color:red">
                                                            <small>{{ $message }}</small>
                                                        </span><br>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="input-label" class="ti-form-label">Confirm Pin <span
                                                            style="color:red">*</span></label>
                                                    <input id="inputEmail" type="password"
                                                        placeholder="Confirm your 6 digit pin" required=""
                                                        autofocus="" class="form-control"
                                                        name="password_confirmation">
                                                    @error('password_confirmation')
                                                        <span role="alert" style="color:red">
                                                            <small>{{ $message }}</small>
                                                        </span><br>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-3">

                                                    <label for="input-label" class="ti-form-label">Captcha <span
                                                            style="color:red">*</span></label>
                                                    <div class="reg_captcha">
                                                        <div class="captcha form-inline captcha_code">
                                                            <span>{!! captcha_img('flat') !!}</span>
                                                            <button id="refresh_captcha_btn"
                                                                name="refresh_captcha_btn" type="button"
                                                                class="btn btn-primary ml-1"
                                                                data-url="{{ route('refresh_captcha') }}"><i
                                                                    class="fas fa-sync-alt"></i></button>
                                                        </div>


                                                        <div class="captcha_input">

                                                        </div>
                                                    </div>
                                                    <input id="inputEmail" type="text" placeholder="Enter captcha"
                                                        required="" autofocus="" class="form-control mb-4"
                                                        name="captcha" value="{{ old('captcha') }}">
                                                    @error('captcha')
                                                        <span role="alert"
                                                            style="color:red"><small>{{ $message }}</small></span><br>
                                                    @enderror
                                                </div>
                                                <label class="width-adjust" for="checkBox">

                                                    <input type="checkbox" id="checkBox" name="accept"
                                                        id="accept" checked value="1" hidden>
                                                    By Signing, you're agree to our <span class="highlight"><a
                                                            href="{{ route('eventmie.page', ['page' => 'terms']) }}">Terms
                                                            & Conditions</a></span> and <span class="highlight"><a
                                                            href="{{ route('eventmie.page', ['page' => 'privacy']) }}">Privacy
                                                            Policy</a></span>
                                                </label>

                                            </div>

                                        </div>
                                        <button type="submit"
                                            class="btn btn btn-yellow btn-block  mb-2 rounded-pill shadow-sm">Register</button>
                                        <div class=" text-center  justify-content-between  ">
                                            <p> Already you have an account? <a
                                                    href="{{ route('eventmie.login') }}">Login</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>

    {{-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
            </script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
            </script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {

            $('#phone').on('input', function() {
                var phoneNumber = $(this).val();
                var startingDigit = phoneNumber.charAt(0);

                if (startingDigit === '7' && phoneNumber.length > 10) {
                    $(this).val(phoneNumber.slice(0, 10));
                } else if (startingDigit === '0' && phoneNumber.length > 11) {
                    $(this).val(phoneNumber.slice(0, 11));
                }
            });
        });

        $("#refresh_captcha_btn").click(function() {
            $.ajax({
                type: 'GET',
                url: '/refresh_captcha',
                success: function(data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        });

        function setInputFilter(textbox, inputFilter, errMsg) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "focusout"].forEach(function(event) {
                textbox.addEventListener(event, function(e) {
                    const key = e.key; // const {key} = event; ES6+
                    if (["keyup", "mouseup", "input", "focusout"].indexOf(e.type) >= 0) {
                        const pattern = /^0(0+)$/;
                        //const pattern = /^(0[1-9][0-9]{1,9})|^([1-9][0-9]{1,9})$/;
                        if (pattern.test(this.value)) {
                            if (this.oldValue) {
                                this.value = this.oldValue;
                            }
                            return;
                        }
                    }
                    if (inputFilter(this.value)) {
                        // Accepted value.
                        if (["keydown", "mousedown", "focusout"].indexOf(e.type) >= 0) {
                            this.classList.remove("input-error");
                            this.setCustomValidity("");

                        }
                    }
                    if (key === "Backspace" || key === "Delete") {
                        if (this.value.length == 1 && this.oldValue == this.value) {
                            this.oldValue = '';
                            this.value = '';
                        }
                    }
                    if (this.value[0] == '0' && this.value.length > 11) {
                        this.setCustomValidity(errMsg);
                        this.reportValidity();
                        this.value = this.oldValue;
                    }
                    if (this.value[0] != '0' && this.value.length > 10) {
                        this.setCustomValidity(errMsg);
                        this.reportValidity();
                        this.value = this.oldValue;
                    }
                    this.oldValue = this.value;
                });
            });
        }
        setInputFilter(document.getElementById("phone"), function(value) {
            return /[0-9]{1,11}$/.test(value); // Allow digits and '.' only, using a RegExp.
        }, "Only 10 to 11 digits  are allowed");
    </script>

</body>

</html>
