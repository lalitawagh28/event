<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/login-responsive.css') }}">

    <style>
        .login,
        .image {
            min-height: 100vh;
        }

        .btn {
            background: #FF5C02;
        }

        .captcha {
            background: #D9D9D9;
            padding: 5px 10px;

        }

        .ti-form-input {
            margin-right: 5px;
        }

        .reg_captcha {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .highlight {
            color: #FF5C02;
        }

        .width-adjust {
            max-width: 300px;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .reset {
            color: color: #FF5C02;
            ;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>
            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-10 mx-auto">
                                <div class="reset mb-4">
                                    <h5>Reset Pin</h5>
                                </div>
                                <form method="POST" action="{{ route('eventmie.password.reset_post') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <div class="form-group mb-3">
                                        <div class="form-group mb-3">
                                            <div class="form-group mb-3">
                                                <input id="inputEmail" name="email" type="email" placeholder="Email"
                                                    required="" autofocus="" class="form-control"
                                                    value="{{ old('email') }}">
                                                @error('email')
                                                    <span role="alert" style="color:red">
                                                        <strong>{{ $message }}</strong>
                                                    </span><br>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-3">
                                                <input id="inputEmail" type="text" name="phone" placeholder="Phone"
                                                    required="" autofocus="" class="form-control"
                                                    value="{{ old('phone') }}">
                                                @error('phone')
                                                    <span role="alert" style="color:red">
                                                        <strong>{{ $message }}</strong>
                                                    </span><br>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-3">
                                                <input id="inputEmail" type="password" name="password" placeholder="{{ __("eventmie-pro::em.pin_message1")}}"
                                                    required="" autofocus="" class="form-control">
                                                @error('password')
                                                    <span role="alert" style="color:red">
                                                        <strong>{{ $message }}</strong>
                                                    </span><br>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-3">
                                                <input id="inputEmail" name="password_confirmation" type="password"
                                                    placeholder=" Confirm Pin" required="" autofocus=""
                                                    class="form-control">
                                                @error('password_confirmation')
                                                    <span role="alert" style="color:red">
                                                        <strong>{{ $message }}</strong>
                                                    </span><br>
                                                @enderror
                                            </div>
                                        </div>
                                        <button type="submit"
                                            class="btn btn btn-yellow btn-block text-white mb-2  shadow-sm">Reset
                                            Pin</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
</body>

</html>
