<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/login-responsive.css') }}">
    <style>
        .login,
        .image {
            min-height: 100vh;
        }

        .btn {
            background: #FF5C02;
        }

        .back-btn {
            display: flex;
            justify-content: space-between;
        }

        .btn-click {

            margin-left: 60px;

        }

        .text {
            color: aliceblue
        }

        .forgot-btn {
            margin-right: 60px;
        }
    </style>
</head>

<body>

    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>
            <div class="col-md-6 bg-light">

                <div class="login d-flex align-items-center py-5">

                    <div class="container">

                        <div class="row">

                            <div class="col-lg-10 col-xl-10 mx-auto">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <h5 class="">Forgot PIN</h5>
                                <p>Enter your registered email address, we will send you a link to reset
                                    PIN.
                                </p>
                                <br>
                                <form method="POST" action="{{ route('eventmie.password.email') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group mb-3">
                                        <div class="form-group mb-3">
                                            <label for="input-label" class="ti-form-label">Email <span
                                                    style="color:red">*</span></label>
                                            <input id="inputEmail" type="email" placeholder="Your Email"
                                                name="email" required="" autofocus="" class="form-control">
                                            @error('email')
                                                <span role="alert" style="color:red">
                                                    {{ $message }}
                                                </span><br>
                                            @enderror
                                        </div>
                                        <button type="submit"
                                            class=" text btn btn btn-yellow btn-block  mb-2 ">Submit</button>
                                        <div class="  account text-center d-flex justify-content-between mt-4 ">
                                            <p> Already have an account? <a href="{{ route('eventmie.login') }}">
                                                    Login</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>

</body>

</html>
