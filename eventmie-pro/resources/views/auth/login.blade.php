<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/login-responsive.css') }}">
    <style>
        .login,
        .image {
            min-height: 100vh;
        }

        .btn {
            background: #FF5C02;
        }

        .home_btn {

            float: right;
            margin-top: 15px;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
</head>

<body>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>
            <div class="col-md-6 bg-light">
                <a href="/" class="btn lgx-btn w-30 home_btn"><i class="fas fa-user-plus"></i> Home</a>
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-10 mx-auto">
                                <h5 class="">Welcome</h5>
                                <p>Login to continue</p>
                                <br>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>
                                                    <span role="alert">
                                                        <strong>{{ $error }}</strong>
                                                    </span>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form method="POST" action="{{ route('eventmie.login_post') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group mb-3">
                                        <p>Email</p>
                                        <input id="inputEmail" type="email" placeholder="Your Email" name="email"
                                            required="" autofocus="" class="form-control">
                                    </div>
                                    <div class="form-group mb-3">
                                        <p>Pin <span class="text-sm" style="font-size:11px">{{ __("eventmie-pro::em.enter_pin_message")}}</span></p>
                                        <input id="inputPassword" type="password" name="password" placeholder="Your Pin"
                                            required="" class="form-control"><br>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input id="customCheck1" type="checkbox"  class="custom-control-input">
                                        <label for="customCheck1" class="custom-control-label">Remember me</label>
                                        <a href="{{ route('eventmie.password.request') }}"
                                            class="ml-auto float-right">Forgot Pin?</a>
                                    </div>
                                    <button type="submit"
                                        class="btn btn btn-yellow btn-block text-uppercase mb-2 rounded-pill shadow-sm">Login</button>
                                    <div class="  account text-center d-flex justify-content-between mt-4 ">
                                        <p> Didn't have an account? <a href="{{ route('eventmie.register_show') }}">
                                                Signup</a></p>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
