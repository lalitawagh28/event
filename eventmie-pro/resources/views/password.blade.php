@extends('eventmie::auth.authapp')

@section('title')
    Reset Pin
@endsection

@section('authcontent')

<div class="lgx-registration-form">
    <form method="POST" action="{{ route('updatePassword') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        <input id="email" type="email" class="wpcf7-form-control form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="@lang('eventmie-pro::em.email')">
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <input id="oldpassword" type="password" class="wpcf7-form-control form-control{{ $errors->has('oldpassword') ? ' is-invalid' : '' }}" name="oldpassword" required placeholder="Old Pin">
        @if ($errors->has('oldpassword'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('oldpassword') }}</strong>
            </span>
        @endif

        <input id="password" type="password" class="wpcf7-form-control form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="@lang('eventmie-pro::em.new') Pin">
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

        <input id="password-confirm" type="password" class="wpcf7-form-control form-control" name="password_confirmation" required placeholder="Confirm Pin">
        
        <input id="phone" type="text" class="wpcf7-form-control form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" required placeholder="Phone">
        @if ($errors->has('phone'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif

        <button type="submit" class="lgx-btn lgx-btn-white btn-block">Reset Pin</button>
        
    </form>
</div>    
    
@endsection
