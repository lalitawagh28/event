<?php

use App\Http\Controllers\api\SensyController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ReviewsController;
use App\Http\Controllers\ManageReviewsController;
use App\Models\Event;
use App\Models\Booking;

use App\Http\Middleware\SubOrganizer;
use Classiebit\Eventmie\Middleware\Authenticate;
use App\Http\Controllers\DesignController;
use App\Http\Controllers\Eventmie\Voyager\AdminController;
use App\Http\Controllers\Eventmie\BookingsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WorldPayController;
use App\Http\Middleware\CheckApproved;
use App\Http\Middleware\VerificationStepMiddleware;
use Classiebit\Eventmie\Facades\Eventmie;
use App\Http\Middleware\IsReportManager;
use App\Http\Controllers\WalletTransferController;
use App\Http\Controllers\WalletPayoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::post('webhooks/event-customers', 'App\Webhooks\EventsWebhook')->name('webhooks.event-customers');
    Route::get('/worldpay-checkout', [WorldPayController::class, 'showResponse'])->name('worldpay.checkout');
    Route::post('webhook/worldpay', [WorldPayController::class, 'finish_booking_worldpay'])->name('worldpay.book');
    Route::post('webhook/aisensy', [WorldPayController::class, 'finish_booking_ai_sensy'])->name('worldpay.book');
    Route::get('/search/address', [SensyController::class, 'search']);
    //webhook worldpay
    Route::post('webhooks/web/worldpay', 'App\Webhooks\WorldPayWebhook')->name('webhooks_web_worldpay');
});

Route::get('/get-wallet', 'App\Http\Controllers\WalletPayController@getWallets')->name('get-wallet');
Route::get('/refresh_captcha', [Controller::class, 'refresh_captcha'])->name('refresh_captcha');
Route::get('/refresh_captcha_guest', [Controller::class, 'refresh_captcha_guest'])->name('refresh_captcha_guest');
Route::get('/updateAttendeeticket', "\App\Http\Controllers\UserController@updateAttendeeticket")->name('updateAttendeeticket');

Route::get('/event/{id}/{page}', 'App\Http\Controllers\Controller@getEventInfo')->name('events.getEventInfo');
Route::post('/exportVoucherSale', 'App\Http\Controllers\Eventmie\Organizer\ReportsController@exportVoucherSale')->name('events.exportVoucherSale');
Route::post('/exportEventSale', 'App\Http\Controllers\Eventmie\Organizer\ReportsController@exportEventSale')->name('events.exportEventSale');
Route::post('/exportAgentSale', 'App\Http\Controllers\Eventmie\Organizer\ReportsController@exportAgentSale')->name('events.exportAgentSale');
Route::post('/exportCommisionSale', 'App\Http\Controllers\Eventmie\Organizer\ReportsController@exportCommisionSale')->name('events.exportCommisionSale');



Route::get('/', function () {
    if (!file_exists(storage_path() . "/installed")) {
        header('location:license');
        die;
    }

    return view('welcome');
});

Route::post('/ticket-transfer', 'App\Http\Controllers\Controller@ticketTransfer')->name('ticketTransfer');
Route::post('/ticket-transfer-stripe-payment', 'App\Http\Controllers\Controller@ticketTransferStripePayment')->name('ticketTransferStripePayment');
Route::post('/ticket-transfer-wallet-payment', 'App\Http\Controllers\Controller@ticketTransferWalletPayment')->name('ticketTransferWalletPayment');
Route::get('/ticket-transfer-accept-payment/{array}', 'App\Http\Controllers\Controller@ticketTransferAcceptPayment')->name('ticketTransferAcceptPayment');
Route::get('/ticket-transfer-accept/{array}', 'App\Http\Controllers\Controller@ticketTransferAccept')->name('ticketTransferAccept');
Route::post('/ticket-transfer-request', 'App\Http\Controllers\Controller@ticketTransferRequest')->name('ticketTransferRequest');
Route::get('/update-password', 'App\Http\Controllers\Controller@updatePasswordView')->name('updatePasswordView');
Route::post('/update-password', 'App\Http\Controllers\Controller@updatePassword')->name('updatePassword');

Route::post('/search', 'App\Http\Controllers\Controller@searchEvent')->name('searchEvent');


Route::get('/license', 'App\Http\Controllers\LicenseController@index');
Route::get('/52cab7070ba5124895a63a3703f66893232', function () {
    header('location:install');
    die;
});

Route::bind('event', function ($value) {
    return \App\Models\Event::orWhere(['slug' =>  $value, 'short_url' => $value])->firstOrFail();
});

// Route::post('/payment-status-update/{id}', "App\Http\Controllers\Eventmie\OBookingsController@booking_update")->name('payment_status_update');

Route::get('/invoice/download/bookings/{booking}', 'App\Http\Controllers\Eventmie\DownloadsController@downloadInvoice')->name('invoice');

/* set local timezone */
Route::post('/set/local_timezone', function (\Illuminate\Http\Request $request) {

    session(['local_timezone' => $request->local_timezone]);

    return response()->json(['success' => 'success'], 200);
})->name('eventmie.local_timezone');

Route::group([
    'prefix' => config('eventmie.route.prefix') . '/' . config('eventmie.route.admin_prefix')
], function () {
    $approve_controller     = 'App\Http\Controllers\Eventmie\Voyager\ApprovalController';
    $country_controller     = 'App\Http\Controllers\Eventmie\Voyager\CountryController';
    Route::get('/approval_user', "$approve_controller@index")->name('admin.approval');
    Route::get('/approve_user/{id}', "$approve_controller@approve_user")->name('admin.approve_user');

    Route::get('/country', "$country_controller@index")->name('admin.country');
    Route::get('/enable_country/{id}', "$country_controller@enable_country")->name('admin.enable_country');
    Route::get('/disable_country/{id}', "$country_controller@disable_country")->name('admin.disable_country');
});
Route::group([
    'prefix' => config('eventmie.route.prefix'),
], function () {

    /* Voyager */
    Route::group([
        'namespace' => 'App\Http\Controllers\Eventmie\Voyager',
        'prefix' => config('eventmie.route.prefix') . '/' . config('eventmie.route.admin_prefix'),
    ], function () {

        $controller     = 'BookingsController';

        /* Override Voyager bulk bookings Routes */
        Route::get('/bookings/bulk', "$controller@bulk_bookings")->name('voyager.bookings.bulk_bookings');

        /* Override Voyager bulk bookings Routes */
        Route::get('/bookings/bulk/edit/{id}', "$controller@bulk_bookings_edit")->name('voyager.bookings.bulk_edit');

        /* Override Voyager bulk bookings Routes */
        Route::put('/bookings/bulk/update/{id}', "$controller@bulk_bookings_update")->name('voyager.bookings.bulk_update');

        /* Override Voyager bulk bookings Routes */
        Route::get('/bookings/bulk/delete/{id}', "$controller@bulk_bookings_delete")->name('voyager.bookings.bulk_delete');

        /* Override Voyager bulk bookings Routes */
        Route::get('/bookings/bulk/show/{id}', "$controller@bulk_bookings_show")->name('voyager.bookings.bulk_show');

        /* Override Voyager bulk bookings Routes */
        Route::get('/bookings/bulk/zip/{ticket_id}/{bulk_code}', "\App\Http\Controllers\Eventmie\DownloadsController@create_bulk_zip")->name('voyager.bookings.bulk_zip');

        /* Override Voyager bulk bookings Routes */
        Route::get('/bookings/bulk/export/{ticket_id}/{bulk_code}', "$controller@bulk_export_attendees")->name('voyager.bookings.bulk_export');
    });
    /*Events  Coupon*/

    /*Events  Coupon*/
    Route::group([
        'prefix' => config('eventmie.route.prefix') . '/' . config('eventmie.route.admin_prefix'),
        'middleware' => 'auth'
    ], function () {
        $controller = "App\Http\Controllers\CouponController";
        Route::get('/coupon', "$controller@index")->name('coupon.index');
        Route::get('/coupon-create', "$controller@create")->name('coupon.create');
        Route::post('/coupon-store', "$controller@store")->name('coupon.store');
        Route::get('/coupon-edit/{id}', "$controller@edit")->name('coupon.edit');
        Route::put('/coupon-update/{id}', "$controller@update")->name('coupon.update');
        Route::delete('/coupon/{id}', "$controller@destroy")->name('coupon.destroy');
        Route::post('/apply/', "$controller@applyCoupon")->name('apply_couponcodes');
    });

    Route::group([
        'prefix' => config('eventmie.route.prefix') . '/' . config('eventmie.route.admin_prefix'),
    ], function () {
        $controller = "App\Http\Controllers\CouponController";
        Route::post('/apply_vouhcer/', "$controller@applyVoucher")->name('apply_vouchercodes');
    });
});

Route::group([
    'middleware' => ['sub-organizer'],
], function () {

    // reviews
    Route::resource('reviews', ReviewsController::class);
    Route::get('/manage_reviews', [ManageReviewsController::class, 'index'])->name('manage_reviews.index');
    Route::post('/manage_reviews', [ManageReviewsController::class, 'update'])->name('manage_reviews.update');

    /* Glists */
    Route::prefix('/myglists')->group(function () {
        $controller = 'App\Http\Controllers\GlistsController';


        Route::get('/', "$controller@index")->name('myglists_index');

        Route::post('/create/glist', "$controller@create_glist")->name('create_glist');

        // for dropdown myglists where orgnizer add guest and choose glist from dropdwon
        Route::post('/dropdown', "$controller@get_myglist")->name('get_myglist');

        // for pagination gilist where show all glist
        Route::get('/pagination', "$controller@pagination_myglists")->name('pagination_myglist');

        Route::post('/add/glist', "$controller@add_to_glist")->name('add_to_glist');

        Route::get('/export/guest/email/{glist}', "$controller@export_emails")->name('export_emails');

        Route::post('/delete/glist', "$controller@delete_glist")->name('delete_glist');

        Route::post('/send/bluk/email', "$controller@send_bluk_email")->name('send_bluk_email');

        Route::post('/send/bluk/email/all', "$controller@send_bluk_email_to_all")->name('send_bluk_email_to_all');

        /* Manage Guests */
        $controller = 'App\Http\Controllers\GuestsController';
        Route::get('/guest-{glist}', "$controller@index")->name('myguests_index');
        Route::get('/get/guest', "$controller@get_myguests")->name('get_myguests');
        Route::post('/add/guest', "$controller@add_guest")->name('add_guest');
        Route::post('/delete/guest', "$controller@delete_guest")->name('delete_guest');
        Route::post('/edit/guest', "$controller@edit_guest")->name('edit_guest');
    });

    /* Separate sub_organizers */
    Route::prefix('/sub_organizers')->group(function () {
        $controller = 'App\Http\Controllers\SubOrganizerController';

        Route::get('/', "$controller@index")->name('sub_organizer.index');
        Route::get('/get_sub_organizers', "$controller@get_sub_organizers")->name('get_sub_organizers');

        Route::post('/edit_sub_organizer', "$controller@edit_sub_organizer")->name('edit_sub_organizer');
    });


    if (file_exists(storage_path() . "/installed")) {
        Eventmie::routes();
    }

    Route::get('email/verify',  'App\Http\Controllers\Eventmie\Auth\VerificationController@show')->name('verification.notice')->withoutMiddleware([SubOrganizer::class]);
    Route::middleware([Authenticate::class])->get('email/verify/{id}',  'App\Http\Controllers\Eventmie\Auth\VerificationController@verify')->name('verification.verify')->withoutMiddleware([SubOrganizer::class]);
    Route::get('email/resend',  'App\Http\Controllers\Eventmie\Auth\VerificationController@resend')->name('verification.resend')->withoutMiddleware([SubOrganizer::class]);


    /* Clone Event */
    Route::prefix('/clone')->group(function () {
        $controller = 'App\Http\Controllers\CloneEventController';
        Route::get('/events/{event}', "$controller@clone_event")->name('clone_event');
    });

    Route::group([
        'prefix' => config('eventmie.route.prefix'),
    ], function () {

        /* My Events (organiser) */
        Route::prefix('/myevents')->group(function () {

            $controller = 'App\Http\Controllers\Eventmie\MyEventsController';

            // make sub organizers by organizers
            Route::post('/save/sub/organizers', "$controller@save_sub_organizers")->name('save_sub_organizers');

            //create users by organizers
            Route::post('/organizer/create/user', "$controller@organizer_create_user")->name('organizer_create_user');


            //get sub-organizers users by organizers
            Route::post('get/sub-organizers', "$controller@get_organizer_users")->name('get_organizer_users');

            //delete seatchart
            Route::post('delete/seatchart', "$controller@delete_seatchart")->name('delete_seatchart');
            Route::post('delete/invoice_logo', "$controller@delete_invoice_logo")->name('delete_invoice_logo');
            Route::post('delete/ticket_logo', "$controller@delete_ticket_logo")->name('delete_ticket_logo');
            Route::post('delete/ticket_bg', "$controller@delete_ticket_bg")->name('delete_ticket_bg');
            Route::post('delete/mobile_trending', "$controller@delete_mobile_trending")->name('delete_mobile_trending');
            Route::post('delete/mobile_card', "$controller@delete_mobile_card")->name('delete_mobile_card');
            Route::post('delete/mobile_detail', "$controller@delete_mobile_detail")->name('delete_mobile_detail');
            Route::post('delete/mobile_poster', "$controller@delete_mobile_poster")->name('delete_mobile_poster');
            Route::post('delete/thumbnail', "$controller@delete_thumbnail")->name('delete_thumbnail');
            Route::post('delete/poster', "$controller@delete_poster")->name('delete_poster');




            Route::get('myevents_delivery_charges', "$controller@myevents_delivery_charges")->name('myevents_delivery_charges');
        });

        /* Voyager Events Controller*/

        /* Voyager */
        Route::group([
            'namespace' => 'App\Http\Controllers\Eventmie\Voyager',
            'prefix' => config('eventmie.route.prefix') . '/' . config('eventmie.route.admin_prefix'),
        ], function () {
            $controller     = 'EventsController';

            /* Override Voyager Events Routes */
            Route::get('/events', "$controller@index")->name('voyager.events.index');
        });

        Route::group([
            'namespace' => 'App\Http\Controllers\Eventmie\Organizer',
            'prefix' => config('eventmie.route.prefix') . '/' . config('eventmie.route.admin_prefix'),
        ], function () {
            $reports_controller = 'ReportsController';

            Route::get('/sales-report-commission', "$reports_controller@salesReportCommission")->name('voyager.sales_report_commission');
            Route::post('/sales-report-commission', "$reports_controller@getSalesReportCommissionData")->name('voyager.get_sales_report_commission_data');
            Route::get('/sales-report-by-agent', "$reports_controller@salesReportByAgent")->name('voyager.sales_report_by_agent');
            Route::post('/sales-report-by-agent', "$reports_controller@getSalesReportByAgentData")->name('voyager.get_sales_report_by_agent_data');
            Route::get('/sales-report-by-voucher', "$reports_controller@salesReportByVoucher")->name('voyager.sales_report_by_voucher');
            Route::post('/sales-report-by-voucher', "$reports_controller@getSalesReportByVoucherData")->name('voyager.get_sales_report_by_voucher_data');
            Route::get('/sales-report-by-event', "$reports_controller@salesReportByEvent")->name('voyager.sales_report_by_event');
            Route::post('/sales-report-by-event', "$reports_controller@getSalesReportByEventData")->name('voyager.get_sales_report_by_event_data');
        });

        /* Voyager UserController */
        Route::group([
            'namespace' => 'App\Http\Controllers\Eventmie\Voyager',
            'prefix' => config('eventmie.route.prefix') . '/' . config('eventmie.route.admin_prefix'),
        ], function () {
            $controller     = 'VoyagerUserController';

            /* Override Voyager Events Routes */
            Route::post('/users', "$controller@store")->name('voyager.users.store');
            Route::put('/users/{id}', "$controller@update")->name('voyager.users.update');
        });
    });

    /*Guest Controller For Checkout Guest*/
    Route::group([
        'prefix' => 'guest',
        'as'    => 'guest.',
        'middleware' => ['guest', 'common'],
    ], function () {
        $controller = 'App\Http\Controllers\GuestController';

        Route::Post('/register', "$controller@registerGuest")->name('register');
        Route::Post('/checkout-register', "$controller@registerCheckout")->name('checkout-register');
        Route::Post('/checkout-login', "$controller@loginCheckout")->name('checkout-login');
    });




    /*Organiser events */
    Route::group([
        'prefix' => 'events',

    ], function () {
        $controller = 'App\Http\Controllers\OrganiserController';

        Route::get('/{event}/{name}', "$controller@show")->name('organiser_show');
    });

    /*Organiser events */
    Route::group([
        'prefix' => 'promocodes',

    ], function () {
        $controller = 'App\Http\Controllers\PromocodesController';

        Route::get('/get', "$controller@get_promocodes")->name('get_promocodes');
        Route::get('/get_currency', "$controller@get_currency")->name('get_currency');

        Route::get('/get/ticket/{ticketd_id}', "$controller@get_ticket_promocodes")->name('get_ticket_promocodes');
        Route::get('/get/event/{event_id}', "$controller@get_event_promocodes")->name('get_event_promocodes');

        Route::post('/apply/', "$controller@apply_promocodes")->name('apply_promocodes');
        Route::post('/apply_event_promocodes/', "$controller@apply_event_promocodes")->name('apply_event_promocodes');
    });



    /* Private Event */
    Route::prefix('/private')->group(function () {
        $controller = 'App\Http\Controllers\PrivateEventController';
        Route::post('/events', "$controller@save_password")->name('private_event');
        Route::post('/verify_event_password', "$controller@verify_event_password")->name('verify_event_password');
    });

    /* Stripe Direct Checkout */
    Route::prefix('/stripe')->group(function () {
        $controller = 'App\Http\Controllers\StripeDirectController';

        Route::get('/response', "$controller@stripeResponse")->name('stripe_response');

        Route::get('/checkout', "$controller@redirectStripeCheckout")->name('stripe_checkout');

        //--- redirect after extra auhentication stripe 3d after3DAuthentication
        Route::get('/extra/authentication/{id}', "$controller@after3DAuthentication")->name('direct_after3DAuthentication');
    });

    /* Connect Stripe Account to Direct Checkout */
    Route::prefix('/connet')->group(function () {
        $controller = 'App\Http\Controllers\StripeConnectController';

        Route::get('/stripe', "$controller@createStripeAccount")->name('connect_stripe');

        Route::get('/stripe/response', "$controller@stripeAccountResponse")->name('response_connect_stripe');
    });

    /* Create Attendee On Checkout Page */
    Route::prefix('/attendee')->group(function () {
        $controller = 'App\Http\Controllers\AttendeeController';

        Route::post('/add/attendee', "$controller@add_attendee")->name('add_attendee');
    });

    /* My Bookings (customers) */
    Route::prefix('/mybookings')->group(function () {
        $controller = '\App\Http\Controllers\Eventmie\MyBookingsController';

        // API
        Route::get('/customer/event', "$controller@get_customer_events")->name('customer_events');
    });

    /* Download Ticket */
    Route::prefix('/download')->group(function () {
        $controller = '\App\Http\Controllers\Eventmie\DownloadsController';

        Route::post('/ticket/', "$controller@getQrCode")->name('get_qrcode');
    });

    Route::prefix('/seatschart/')->group(function () {

        $controller = 'App\Http\Controllers\SeatChartController';

        Route::post('upload', "$controller@upload_seatchart")->name('upload_seatchart');
        Route::post('event_seat_upload', "$controller@upload_event_seatchart")->name('upload_event_seatchart');
        Route::post('delete_event_seat', "$controller@delete_event_seat")->name('delete_event_seat');
        Route::post('save_event_seats', "$controller@save_event_seats")->name('save_event_seats');
        Route::post('disable_enable_seatchart', "$controller@disable_enable_seatchart")->name('disable_enable_seatchart');
    });

    Route::prefix('/seats/')->group(function () {

        $controller = 'App\Http\Controllers\SeatsController';

        Route::post('save', "$controller@save_seats")->name('save_seats');

        Route::post('delete', "$controller@delete_seat")->name('delete_seat');

        Route::post('disable', "$controller@disable_seat")->name('disable_seat');
        Route::post('enable', "$controller@enable_seat")->name('enable_seat');
    });

    /* Pos sub-organizers */
    Route::group([
        'prefix' => 'pos-bookings',
        'as'    => 'pos.',
        'middleware' => ['pos'],
    ], function () {
        $controller = 'App\Http\Controllers\PosController';

        Route::get('/index', "$controller@index")->name('index');

        Route::get('/booking/{id}', "$controller@show")->name('show');

        // API
        Route::get('/api/bookings', "$controller@bookings")->name('bookings');
        Route::post('/api/edit_bookings', "$controller@edit_bookings")->name('edit_bookings');
        Route::get('/api/events', "$controller@events")->name('events');
    });

    /* Pos sub-organizers */
    Route::group([
        'prefix' => 'scanner-bookings',
        'as'    => 'scanner.',
        'middleware' => ['scanner'],
    ], function () {
        $controller = 'App\Http\Controllers\ScannerController';

        Route::get('/index', "$controller@index")->name('index');

        Route::get('/booking/{id}', "$controller@show")->name('show');

        // API
        Route::get('/api/bookings', "$controller@bookings")->name('bookings');
        Route::post('/api/edit_bookings', "$controller@edit_bookings")->name('edit_bookings');
        Route::get('/api/events', "$controller@events")->name('events');
    });



    //--- redirect after extra auhentication stripe 3d after3DAuthentication
    Route::get('extra/authentication/{id}', '\App\Http\Controllers\Eventmie\BookingsController@after3DAuthentication')->name('after3DAuthentication');

    Route::get('/pay/bitpay_response', '\App\Http\Controllers\Eventmie\BookingsController@bitpayPaymentResponse')->name('bitpay_response');


    //RazorPay routes start
    Route::post('/payment/paystack', '\App\Http\Controllers\Eventmie\BookingsController@redirectToGateway')->name('payment_paystack');
    Route::get('/paystack/payment/callback', '\App\Http\Controllers\Eventmie\BookingsController@handleGatewayCallback');
    Route::any('/checkout/razorpay/callback', '\App\Http\Controllers\Eventmie\BookingsController@razorpay_callback')->name('razorpay_callback');
    Route::any('/razorpay/payment', '\App\Http\Controllers\Eventmie\BookingsController@razorpay_view')->name('razorpay_view');

    //Paytm routes start
    Route::any('/checkout/paytm/callback', '\App\Http\Controllers\Eventmie\BookingsController@paytm_callback')->name('paytm_callback');
});


Route::group([
    'middleware' => ['auth', VerificationStepMiddleware::class, CheckApproved::class],
    'namespace' => 'App\Http\Controllers\Eventmie\Organizer',
    'prefix' => config('eventmie.route.prefix') . '/organizer',
], function () {
    $controller     = 'DashboardController';
    $events_controller = 'EventsController';
    $tickets_controller = 'TicketsController';
    $bookings_controller = 'BookingsController';
    $user_bookings_controller = 'UserBookingsController';
    $reports_controller = 'ReportsController';
    $agents_controller = 'AgentsController';
    $promocodes_controller = 'PromocodesController';
    /* Override Voyager bulk bookings Routes */
    Route::get('/dashboard', "$controller@index")->name('voyager.organizer.dashboard');
    Route::get('/events', "$events_controller@index")->name('voyager.organizer.events');
    Route::get('/ticket_category', "$tickets_controller@index")->name('voyager.organizer.ticket_category');
    Route::get('/bookings', "$bookings_controller@index")->name('voyager.organizer.bookings');
    Route::get('/agents_avaliable', "$agents_controller@available")->name('voyager.organizer.agents_available');
    Route::get('/submit_tickets/{id}', "$agents_controller@submitTickets")->name('voyager.organizer.submit_tickets');
    Route::get('/bulk_allocate_tickets', "$agents_controller@bulkAllocateTickets")->name('voyager.organizer.allocate_tickets_bulk');
    Route::get('/edit_submit_tickets/{id}', "$agents_controller@editSubmitTickets")->name('voyager.organizer.edit_submit_tickets');
    Route::get('/promocodes_create/{table}', "$promocodes_controller@create_promcode")->name('voyager.organizer.promocodes.create');
    Route::get('/promocodes_edit/{id}', "$promocodes_controller@edit")->name('voyager.organizer.promocodes.edit');
    Route::get('/promocodes_show/{id}', "$promocodes_controller@show")->name('voyager.organizer.promocodes.show');
    Route::post('/promocodes_store', "$promocodes_controller@store")->name('voyager.organizer.promocodes.store');
    Route::put('/promocodes_update/{id}', "$promocodes_controller@update")->name('voyager.organizer.promocodes.update');
    Route::delete('/promocodes_bulk_delete/{id?}', "$promocodes_controller@destroy")->name('voyager.organizer.promocodes.bulk_delete');
    Route::delete('/promocodes_delete/{id}', "$promocodes_controller@delete")->name('voyager.organizer.promocodes.delete');
    Route::get('/promocodes', "$promocodes_controller@index")->name('voyager.organizer.promocodes.index');
    Route::get('/agents_allocated', "$agents_controller@index")->name('voyager.organizer.agents_allocated');
    Route::get('agent-register', "$agents_controller@agent_register")->name('agent_create.register.personal_detail');
    Route::post('personal-detail', "$agents_controller@agent_store")->name('agent_create.register.store');
    Route::post('personal-detail', "$agents_controller@agent_store")->name('agent_create.register.store');
    Route::get('mobile-verification/{id}', "$agents_controller@mobileVerificationCreate")->name('agent_create.register.mobileVerification');
    Route::get('email-verification/{id}', "$agents_controller@emailVerificationCreate")->name('agent_create.register.emailVerification');
    Route::post('mobile-otp-verification/{id}', "$agents_controller@mobileVerificationStore")->name('agent_create.register.mobileOtpVerification');
    Route::post('email-otp-verification/{id}',  "$agents_controller@emailVerificationStore")->name('agent_create.register.emailOtpVerification');
    Route::post('change-mobile-number/{id}',  "$agents_controller@changePhoneNumber")->name('agent_create.register.changeMobileNumber');
    Route::post('change-email-address/{id}',  "$agents_controller@changeEmailAddress")->name('agent_create.register.changeEmailAddress');
    Route::post('resend-mobile-otp/{id}',  "$agents_controller@resendMobileOnetimePassword")->name('agent_create.register.resendMobileOtp');
    Route::post('resend-email-otp/{id}',  "$agents_controller@resendEmailOnetimePassword")->name('agent_create.register.resendEmailOtp');
    Route::post('get-tickets/{id}/{agent_ticket_id?}',  "$agents_controller@getTickets")->name('voyager.organizer.gettickets');
    Route::post('ticket_store',  "$agents_controller@ticketStore")->name('voyager.organizer.ticket_store');
    Route::post('ticket_update/{id}',  "$agents_controller@ticketUpdate")->name('voyager.organizer.ticket_update');
    Route::post('bulk_ticket_update',  "$agents_controller@bulkTicketUpdate")->name('voyager.organizer.bulk_ticket_update');
    Route::get('download_agent_allocated_template/{id}/{agent_id}',  "$agents_controller@donwloadTemplate")->name('voyager.organizer.download_template');
    Route::post('upload_event_allocation', "$agents_controller@uploadEventAllocation")->name('voyager.organizer.upload_event_allocation');
    Route::get('/user_bookings', "$user_bookings_controller@index")->name('voyager.organizer.user_bookings');
    Route::get('/reports', "$reports_controller@index")->name('voyager.organizer.reports');
    Route::get('/agent_reports', "$reports_controller@agent")->name('voyager.organizer.agent_reports');

    Route::get('/delete/{slug}', "$events_controller@delete_event")->name('voyager.organizer.delete_event');
    Route::get('/sales-report-by-event', "$reports_controller@salesReportByEvent")->name('voyager.organizer.sales_report_by_event');
    Route::post('/sales-report-by-event', "$reports_controller@getSalesReportByEventData")->name('voyager.organizer.get_sales_report_by_event_data');
    Route::get('/sales-report-by-voucher', "$reports_controller@salesReportByVoucher")->name('voyager.organizer.sales_report_by_voucher');
    Route::post('/sales-report-by-voucher', "$reports_controller@getSalesReportByVoucherData")->name('voyager.organizer.get_sales_report_by_voucher_data');
    Route::post('/get-voucher-code-by-event', "$reports_controller@getVoucherCodeByEvent")->name('voyager.organizer.get_voucher_code_by_event');
    Route::get('/sales-report-by-agent', "$reports_controller@salesReportByAgent")->name('voyager.organizer.sales_report_by_agent');
    Route::post('/sales-report-by-agent', "$reports_controller@getSalesReportByAgentData")->name('voyager.organizer.get_sales_report_by_agent_data');
    Route::post('/get-agent-by-event', "$reports_controller@getAgentByEvent")->name('voyager.organizer.get_agents_by_event');
    Route::get('/sales-report-commission', "$reports_controller@salesReportCommission")->name('voyager.organizer.sales_report_commission');
    Route::post('/sales-report-commission', "$reports_controller@getSalesReportCommissionData")->name('voyager.organizer.get_sales_report_commission_data');
});

Route::group([
    'middleware' => ['auth', IsReportManager::class],
    'namespace' => 'App\Http\Controllers\Eventmie\ReportManager',
    'prefix' => config('eventmie.route.prefix') . '/reports-manager',
], function () {
    $controller     = 'DashboardController';
    $reports_controller = 'ReportsController';
    $user_bookings_controller = 'UserBookingsController';

    /* Override Voyager bulk bookings Routes */
    Route::get('/dashboard', "$controller@index")->name('voyager.report-manager.dashboard');
    Route::get('/reports', "$reports_controller@index")->name('voyager.report-manager.reports');
    Route::get('/agent_reports', "$reports_controller@agent")->name('voyager.report-manager.agent_reports');

    Route::get('/sales-report-by-event', "$reports_controller@salesReportByEvent")->name('voyager.report-manager.sales_report_by_event');
    Route::post('/sales-report-by-event', "$reports_controller@getSalesReportByEventData")->name('voyager.report-manager.get_sales_report_by_event_data');
    Route::get('/sales-report-by-voucher', "$reports_controller@salesReportByVoucher")->name('voyager.report-manager.sales_report_by_voucher');
    Route::post('/sales-report-by-voucher', "$reports_controller@getSalesReportByVoucherData")->name('voyager.report-manager.get_sales_report_by_voucher_data');
    Route::post('/get-voucher-code-by-event', "$reports_controller@getVoucherCodeByEvent")->name('voyager.report-manager.get_voucher_code_by_event');
    Route::get('/sales-report-by-agent', "$reports_controller@salesReportByAgent")->name('voyager.report-manager.sales_report_by_agent');
    Route::post('/sales-report-by-agent', "$reports_controller@getSalesReportByAgentData")->name('voyager.report-manager.get_sales_report_by_agent_data');
    Route::post('/get-agent-by-event', "$reports_controller@getAgentByEvent")->name('voyager.report-manager.get_agents_by_event');
    Route::get('/sales-report-commission', "$reports_controller@salesReportCommission")->name('voyager.report-manager.sales_report_commission');
    Route::post('/sales-report-commission', "$reports_controller@getSalesReportCommissionData")->name('voyager.report-manager.get_sales_report_commission_data');
    Route::get('/user_bookings', "$user_bookings_controller@index")->name('voyager.report-manager.user_bookings');

});

Route::group([
    'middleware' => ['auth', VerificationStepMiddleware::class, CheckApproved::class],
    'namespace' => 'App\Http\Controllers\Eventmie\Agents',
    'prefix' => config('eventmie.route.prefix') . '/agents',
], function () {
    $controller     = 'DashboardController';
    $events_controller = 'EventsController';
    $tickets_controller = 'TicketsController';
    $reports_controller = 'ReportsController';
    $commission_controller = 'CommissionController';
    $user_bookings_controller = 'UserBookingsController';
    /* Override Voyager bulk bookings Routes */
    Route::get('/dashboard', "$controller@index")->name('voyager.agents.dashboard');
    Route::get('/events', "$events_controller@index")->name('voyager.agents.events');
    Route::get('/ticket_category', "$tickets_controller@index")->name('voyager.agents.ticket_category');
    Route::get('/reports', "$reports_controller@index")->name('voyager.agents.reports');
    Route::get('/commission', "$commission_controller@index")->name('voyager.agents.commission');
    Route::get('/user_bookings', "$user_bookings_controller@index")->name('voyager.agents.user_bookings');
    Route::get('/edit_submit_tickets/{id}', "$commission_controller@editSubmitTickets")->name('voyager.agents.edit_submit_tickets');
    Route::post('ticket_update/{id}',  "$commission_controller@ticketUpdate")->name('voyager.agents.ticket_update');
    Route::get('/export_attendees_agent/{slug}', "$events_controller@export_attendees_agent")->name('export_attendees_agent');
    Route::get('/sales-report-by-event', "$reports_controller@salesReportByEvent")->name('voyager.agents.sales_report_by_event');
    Route::post('/sales-report-by-event', "$reports_controller@getSalesReportByEventData")->name('voyager.agents.get_sales_report_by_event_data');
    Route::get('/sales-report-by-voucher', "$reports_controller@salesReportByVoucher")->name('voyager.agents.sales_report_by_voucher');
    Route::post('/sales-report-by-voucher', "$reports_controller@getSalesReportByVoucherData")->name('voyager.agents.get_sales_report_by_voucher_data');
    Route::get('/sales-report-commission', "$reports_controller@salesReportCommission")->name('voyager.agents.sales_report_commission');
    Route::post('/sales-report-commission', "$reports_controller@getSalesReportByCommissionData")->name('voyager.agents.get_sales_report_by_commission_data');
    Route::post('/get-voucher-code-by-event', "$reports_controller@getVoucherCodeByEvent")->name('voyager.agents.get_voucher_code_by_event');
    Route::post('/exportEventSale', "$reports_controller@exportEventSale")->name('events.agents.exportEventSale');
    Route::post('/exportVoucherSale', "$reports_controller@exportVoucherSale")->name('events.agents.exportVoucherSale');
    Route::post('/exportCommisionSale', "$reports_controller@exportCommisionSale")->name('events.agents.exportCommisionSale');
});

Route::group([
    'middleware' => ['auth'],
    'namespace' => 'App\Http\Controllers\Eventmie\Voyager',
    'prefix' => config('eventmie.route.prefix') . '/verification',
], function () {
    Route::get('mobile-verification', 'RegistrationController@mobileVerificationUpdate')->name('admin.register.mobileVerificationUpdate');
    Route::post('mobile-otp-verification', 'RegistrationController@mobileVerificationUpdateStore')->name('admin.register.mobileOtpVerificationUpdate');
});

Route::group([
    'middleware' => ['auth'],
    'namespace' => 'App\Http\Controllers\Eventmie\Voyager',
    'prefix' => config('eventmie.route.prefix') .  '/' . config('eventmie.route.admin_prefix'),
], function () {
    $delivery_charges_controller = 'DeliveryChargesController';
    Route::get('/get/event/{event_id}', "$delivery_charges_controller@get_event_delivery_charges")->name('get_event_delivery_charges');
    Route::get('event_details/{id}', 'AdminController@getEventDetails')->name('admin.getEventDetails');
    Route::get('transaction_details/{id}', 'AdminController@getTransactionDetails')->name('admin.getTransactionDetails');
    Route::get('/delivery_charges_create/{table}', "$delivery_charges_controller@create_delivery_charges")->name('voyager.delivery-charges.create');
    Route::get('/delivery_charges_edit/{id}', "$delivery_charges_controller@edit")->name('voyager.delivery-charges.edit');
    Route::get('/delivery_charges_show/{id}', "$delivery_charges_controller@show")->name('voyager.delivery-charges.show');
    Route::post('/delivery_charges_store', "$delivery_charges_controller@store")->name('voyager.delivery-charges.store');
    Route::put('/delivery_charges_update/{id}', "$delivery_charges_controller@update")->name('voyager.delivery-charges.update');
    Route::delete('/delivery_charges_bulk_delete/{id?}', "$delivery_charges_controller@destroy")->name('voyager.delivery-charges.delete.index');
    Route::delete('/delivery_charges_delete/{id}', "$delivery_charges_controller@delete")->name('voyager.delivery-charges.delete');
    Route::get('/delivery_charges', "$delivery_charges_controller@index")->name('voyager.delivery-charges.index');
});


Route::group([
    'middleware' => ['auth'],
    'namespace' => 'App\Http\Controllers\Eventmie\Voyager',
    'prefix' => config('eventmie.route.prefix') .  '/' . config('eventmie.route.admin_prefix'),
], function () {
    $sales_report_controller = 'SalesReportController';
    Route::post('/get-voucher-code-by-event', "$sales_report_controller@getVoucherCodeByEvent")->name('voyager.admin.get_voucher_code_by_event');
    Route::post('/get-agent-by-event', "$sales_report_controller@getAgentByEvent")->name('voyager.admin.get_agents_by_event');
});



Route::group([
    'namespace' => 'App\Http\Controllers\Eventmie\Voyager',
    'prefix' => config('eventmie.route.prefix') . '/' . config('eventmie.route.admin_prefix'),
], function () {
    Route::get('agent-register', 'RegistrationController@agent_register')->name('agent.register.personal_detail');
    Route::get('organizer-register', 'RegistrationController@index')->name('admin.register.personal_detail');
    Route::post('personal-detail', 'RegistrationController@store')->name('admin.register.store');
    Route::post('agent-personal-detail', 'RegistrationController@agent_store')->name('admin.register.agent_store');
    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::get('mobile-verification', 'RegistrationController@mobileVerificationCreate')->name('admin.register.mobileVerification');
        Route::get('email-verification', 'RegistrationController@emailVerificationCreate')->name('admin.register.emailVerification');

        Route::post('mobile-otp-verification', 'RegistrationController@mobileVerificationStore')->name('admin.register.mobileOtpVerification');
        Route::post('email-otp-verification', 'RegistrationController@emailVerificationStore')->name('admin.register.emailOtpVerification');
        Route::post('change-mobile-number', 'RegistrationController@changePhoneNumber')->name('admin.register.changeMobileNumber');
        Route::post('change-email-address', 'RegistrationController@changeEmailAddress')->name('admin.register.changeEmailAddress');
        Route::post('resend-mobile-otp', 'RegistrationController@resendMobileOnetimePassword')->name('admin.register.resendMobileOtp');
        Route::post('resend-email-otp', 'RegistrationController@resendEmailOnetimePassword')->name('admin.register.resendEmailOtp');
    });

    Route::get('/signup', 'RegistrationController@signup')->name('admin.signup');
});
Route::get('/admin/approve', [AdminController::class, 'approve'])->name('approval');
Route::get('/design/personal-detail', [DesignController::class, 'index'])->name('personal-detail');
Route::get('/design/mobile-verification', [DesignController::class, 'mobileVerification'])->name('mobile-verification');
Route::get('/design/email-verification', [DesignController::class, 'emailVerification'])->name('email-verification');
Route::get('/design/signup', [DesignController::class, 'signup'])->name('signup');
Route::get('/design/organiser-event', [DesignController::class, 'organiserEvent'])->name('organiser-event');
Route::get('/design/event-form', [DesignController::class, 'eventForm'])->name('event-form');
Route::get('/design/event-details', [DesignController::class, 'eventDetail'])->name('event-details');
Route::get('/design/event-list', [DesignController::class, 'eventList'])->name('event-list');
Route::get('/design/tickets_booking', [DesignController::class, 'ticketsBooking'])->name('tickets_booking');
Route::get('/design/profile', [DesignController::class, 'profile'])->name('profile');
Route::get('/design/deposit_form', [DesignController::class, 'depositForm'])->name('deposit_form');
Route::get('/design/deposit_form1', [DesignController::class, 'depositForm1'])->name('deposit_form1');
Route::get('/design/deposit_verification', [DesignController::class, 'depositVerification'])->name('deposit_verification');
Route::get('/design/deposit_card_details', [DesignController::class, 'depositCardDetails'])->name('deposit_card_details');
Route::get('/design/payment_success', [DesignController::class, 'paymentSuccess'])->name('payment_success');
Route::get('/design/login', [DesignController::class, 'login'])->name('eventLogin');
Route::get('/design/registration', [DesignController::class, 'registration'])->name('eventRegistration');
Route::get('/design/faq_mobile', [DesignController::class, 'faqMobile'])->name('design.faq_mobile');
Route::get('/design/terms_mobile', [DesignController::class, 'termsMobile'])->name('terms_mobile');
Route::get('/design/privacy_mobile', [DesignController::class, 'privacyMobile'])->name('terms_mobile');
Route::get('/design/payments', [DesignController::class, 'payments'])->name('payments');
Route::get('/design/payouts-ac-create', [DesignController::class, 'payoutsAcCreate'])->name('payouts-ac-create');
Route::get('/design/transfer-ac-create', [DesignController::class, 'transferAcCreate'])->name('transfer-ac-create');
Route::get('/design/withdraw-ac-create', [DesignController::class, 'withdrawAcCreate'])->name('withdraw-ac-create');

Route::group([
    'middleware' => ['auth'],
    'namespace' => 'App\Http\Controllers',
    'prefix' => config('eventmie.route.prefix') . '/wallet',
], function () {
    Route::get('deposit', 'WalletPayController@deposit_form')->name('wallet.deposit.form');
    Route::post('get-exchange-rate', 'WalletPayController@get_exchange_rate')->name('wallet.get_exchange_rate');
    Route::post('deposit-store', 'WalletPayController@deposit_store')->name('wallet.deposit_store');


    Route::get('/transfer',[WalletTransferController::class,'transferData']);
    Route::post('/storeFormData', [WalletTransferController::class, 'storeData'])->name('storeFormData');
    Route::post('/verifyTransaction', 'WalletTransferController@verifyotp')->name('verifyTransaction');
    Route::post('/resendotp', 'WalletTransferController@resendOtp')->name('resendOtp');
    Route::post('/exchangeRate',[WalletTransferController::class,'transferGetExchangeRate'] )->name('exchangeRate');



});

