<?php

use App\Http\Controllers\api\ApiController;
use App\Http\Controllers\api\BookingController;
use App\Http\Controllers\api\StripeBookingController;
use App\Http\Controllers\api\StripeController;
use App\Http\Controllers\api\WishlistController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Models\Event;
use App\Http\Controllers\api\TransactionController;
use App\Http\Controllers\api\TransferController;
use App\Http\Controllers\api\SensyController;
use App\Http\Controllers\api\SubCategoryBookingController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/registers', [UserController::class, 'register']);

Route::post('/login', [UserController::class, 'login']);

Route::post('/checkout', function (Request $request) {
    return response()->json(['status' => true, 'data' => json_decode($request->data)]);
});

Route::middleware(['auth:api'])->group(function () {

    Route::get('get/events', function () {
        $events = Event::with('tickets')->paginate(5);
        return response()->json(['status' => true, 'events' => $events]);
    });

    Route::get('user', function (Request $request) {
        return json_decode($request->user());
    });

    // Route::post('/checkout', function(Request $request){

    //     return response()->json(['status' => true, 'data' => $request->all()]);
    // });

    // Route::get('send/message', [MessagesController::class, 'sendMessage']);
    // Route::get('get/messages', [MessagesController::class, 'getMessages']);


    Route::controller(ApiController::class)->group(function () {
        Route::post('events', 'filterEvents');
        Route::get('/categories', 'getCategoryList');
        Route::get('/venues', 'getVenueList');
        Route::post('/get-event', 'getEvent');
        Route::get('/events/filter',  'dashboardApis');


        Route::get('/upcoming-tickets', 'getUpcomingBookings');
        Route::get('/past-tickets', 'getPastBookings');
        Route::get('/show-bookings', 'showBooking');
        Route::get('/all-bookings', 'myBookings');

        Route::post('/get-my-orders', 'myOrderBookings');
        Route::get('/get-my-booking-by-order/{id}', 'myBookingsByOrder');
        Route::get('/get-delivery-charges/{id}', 'getDeliveryCharges');
        Route::get('addresses/search/{postcode}', 'search');
        Route::get('/get-collection-points/{id}', 'getCollectionPoints');

        Route::post('/download-ticket', 'downloadTicket');

        Route::post('/download-invoice', 'downloadInvoice');
    });

    Route::controller(BookingController::class)->group(function () {
        Route::post('/show-booking-info', 'showBooking')->name('checkout');
        Route::post('/show-ticket-details', 'ticketDetail')->name('ticketDetails');
        Route::post('/book-tickets', 'book_tickets')->name('book-tickets');
        Route::post('/validate-booking', 'preBookingValidate')->name('validate-booking');
        Route::post('/stripe-finish-booking', 'stripeFinishBooking')->name('stripe.finish-booking');
    });

    Route::controller(WishlistController::class)->group(function () {
        Route::get('get/wishlist', 'getWishlist');
        Route::post('wishlist', 'add_remove_wishlist');
    });

    Route::post('/payment-intent', [StripeController::class, 'createPaymentIntent']);

    Route::apiResource('transactions', TransactionController::class);
});

Route::prefix('/v1')->group(function () {

    Route::controller(ApiController::class)->group(function () {
        Route::post('user-validate',  'validateUser');
        Route::post('user-register',  'registerUser');
        Route::post('forgot-password','cmdbForgot');
    });
});

Route::controller(TransferController::class)->middleware(['auth:api'])->group(function () {
    Route::get('get/users', 'getWalletUser');
    Route::get('get/transfer-request/free', 'getTransferRequest');
    Route::post('transfer-request', 'InitiateTransferRequest');
    Route::get('accept-transfer-request/{array}', 'AcceptRequest');
    Route::post('ticket-transfer-stripe-payment', 'ticketTransferStripePayment');
    Route::post('ticket-transfer-wallet-payment', 'ticketTransferWalletPayment');
    Route::get('ticket-transfer-accept-payment/{array}', 'ticketTransferAcceptPayment');

    //Ticket Transfer
    Route::get('get/transfer-requests', [TransferController::class, 'getTransferTicket']);
});

Route::controller(SensyController::class)->group(function () {
    Route::get('/search/address', 'search');
    Route::post('initiate/checkout', 'initiateCheckout');
    Route::post('apply_voucher', 'applyVoucher');
    Route::get('get/logs-sensy', 'getSensyLog');
    Route::get('get/sensy-status', 'getStatus');
    Route::post('validate-voucher-code', 'validateVoucherCode');
});

Route::post('/book-tickets-sensy', [BookingController::class,'book_tickets']);
Route::get('/contact-Us',[ApiController::class,'contactUs']);

Route::controller(SubCategoryBookingController::class)->middleware(['auth:api'])->group(function () {
    Route::group(['prefix' => 'sub-category'], function () {
        Route::post('/book-tickets', 'book_tickets')->name('book-tickets');
        Route::post('/validate-booking', 'preBookingValidate')->name('validate-booking');
        Route::post('/stripe-finish-booking', 'stripeFinishBooking')->name('stripe.finish-booking');
    });
});
